package com.squareup.moshi;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Map.Entry;

final class t extends AbstractMap implements Serializable {
   // $FF: synthetic field
   static final boolean g;
   private static final Comparator h;
   Comparator a;
   t.f[] b;
   final t.f c;
   int d;
   int e;
   int f;
   private t.c i;
   private t.d j;

   static {
      boolean var0;
      if(!t.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      g = var0;
      h = new Comparator() {
         public int a(Comparable var1, Comparable var2) {
            return var1.compareTo(var2);
         }

         // $FF: synthetic method
         public int compare(Object var1, Object var2) {
            return this.a((Comparable)var1, (Comparable)var2);
         }
      };
   }

   t() {
      this((Comparator)null);
   }

   t(Comparator var1) {
      this.d = 0;
      this.e = 0;
      if(var1 == null) {
         var1 = h;
      }

      this.a = var1;
      this.c = new t.f();
      this.b = new t.f[16];
      this.f = this.b.length / 2 + this.b.length / 4;
   }

   private static int a(int var0) {
      var0 ^= var0 >>> 20 ^ var0 >>> 12;
      return var0 >>> 4 ^ var0 >>> 7 ^ var0;
   }

   private void a() {
      this.b = a(this.b);
      this.f = this.b.length / 2 + this.b.length / 4;
   }

   private void a(t.f var1) {
      byte var4 = 0;
      t.f var8 = var1.b;
      t.f var6 = var1.c;
      t.f var5 = var6.b;
      t.f var7 = var6.c;
      var1.c = var5;
      if(var5 != null) {
         var5.a = var1;
      }

      this.a(var1, var6);
      var6.b = var1;
      var1.a = var6;
      int var2;
      if(var8 != null) {
         var2 = var8.i;
      } else {
         var2 = 0;
      }

      int var3;
      if(var5 != null) {
         var3 = var5.i;
      } else {
         var3 = 0;
      }

      var1.i = Math.max(var2, var3) + 1;
      var3 = var1.i;
      var2 = var4;
      if(var7 != null) {
         var2 = var7.i;
      }

      var6.i = Math.max(var3, var2) + 1;
   }

   private void a(t.f var1, t.f var2) {
      t.f var5 = var1.a;
      var1.a = null;
      if(var2 != null) {
         var2.a = var5;
      }

      if(var5 != null) {
         if(var5.b == var1) {
            var5.b = var2;
         } else {
            if(!g && var5.c != var1) {
               throw new AssertionError();
            }

            var5.c = var2;
         }
      } else {
         int var3 = var1.g;
         int var4 = this.b.length;
         this.b[var3 & var4 - 1] = var2;
      }

   }

   private boolean a(Object var1, Object var2) {
      boolean var3;
      if(var1 != var2 && (var1 == null || !var1.equals(var2))) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   static t.f[] a(t.f[] var0) {
      int var4 = var0.length;
      t.f[] var6 = new t.f[var4 * 2];
      t.b var9 = new t.b();
      t.a var7 = new t.a();
      t.a var8 = new t.a();

      label50:
      for(int var1 = 0; var1 < var4; ++var1) {
         t.f var5 = var0[var1];
         if(var5 != null) {
            var9.a(var5);
            int var3 = 0;
            int var2 = 0;

            while(true) {
               t.f var10 = var9.a();
               if(var10 == null) {
                  var7.a(var2);
                  var8.a(var3);
                  var9.a(var5);

                  while(true) {
                     var5 = var9.a();
                     if(var5 == null) {
                        if(var2 > 0) {
                           var5 = var7.a();
                        } else {
                           var5 = null;
                        }

                        var6[var1] = var5;
                        if(var3 > 0) {
                           var5 = var8.a();
                        } else {
                           var5 = null;
                        }

                        var6[var1 + var4] = var5;
                        continue label50;
                     }

                     if((var5.g & var4) == 0) {
                        var7.a(var5);
                     } else {
                        var8.a(var5);
                     }
                  }
               }

               if((var10.g & var4) == 0) {
                  ++var2;
               } else {
                  ++var3;
               }
            }
         }
      }

      return var6;
   }

   private void b(t.f var1) {
      byte var4 = 0;
      t.f var5 = var1.b;
      t.f var6 = var1.c;
      t.f var7 = var5.b;
      t.f var8 = var5.c;
      var1.b = var8;
      if(var8 != null) {
         var8.a = var1;
      }

      this.a(var1, var5);
      var5.c = var1;
      var1.a = var5;
      int var2;
      if(var6 != null) {
         var2 = var6.i;
      } else {
         var2 = 0;
      }

      int var3;
      if(var8 != null) {
         var3 = var8.i;
      } else {
         var3 = 0;
      }

      var1.i = Math.max(var2, var3) + 1;
      var3 = var1.i;
      var2 = var4;
      if(var7 != null) {
         var2 = var7.i;
      }

      var5.i = Math.max(var3, var2) + 1;
   }

   private void b(t.f var1, boolean var2) {
      for(; var1 != null; var1 = var1.a) {
         t.f var7 = var1.b;
         t.f var6 = var1.c;
         int var3;
         if(var7 != null) {
            var3 = var7.i;
         } else {
            var3 = 0;
         }

         int var4;
         if(var6 != null) {
            var4 = var6.i;
         } else {
            var4 = 0;
         }

         int var5 = var3 - var4;
         t.f var8;
         if(var5 == -2) {
            var7 = var6.b;
            var8 = var6.c;
            if(var8 != null) {
               var3 = var8.i;
            } else {
               var3 = 0;
            }

            if(var7 != null) {
               var4 = var7.i;
            } else {
               var4 = 0;
            }

            var3 = var4 - var3;
            if(var3 == -1 || var3 == 0 && !var2) {
               this.a(var1);
            } else {
               if(!g && var3 != 1) {
                  throw new AssertionError();
               }

               this.b(var6);
               this.a(var1);
            }

            if(var2) {
               break;
            }
         } else if(var5 == 2) {
            var6 = var7.b;
            var8 = var7.c;
            if(var8 != null) {
               var3 = var8.i;
            } else {
               var3 = 0;
            }

            if(var6 != null) {
               var4 = var6.i;
            } else {
               var4 = 0;
            }

            var3 = var4 - var3;
            if(var3 != 1 && (var3 != 0 || var2)) {
               if(!g && var3 != -1) {
                  throw new AssertionError();
               }

               this.a(var7);
               this.b(var1);
            } else {
               this.b(var1);
            }

            if(var2) {
               break;
            }
         } else if(var5 == 0) {
            var1.i = var3 + 1;
            if(var2) {
               break;
            }
         } else {
            if(!g && var5 != -1 && var5 != 1) {
               throw new AssertionError();
            }

            var1.i = Math.max(var3, var4) + 1;
            if(!var2) {
               break;
            }
         }
      }

   }

   private Object writeReplace() throws ObjectStreamException {
      return new LinkedHashMap(this);
   }

   t.f a(Object var1) {
      Object var3 = null;
      t.f var2 = (t.f)var3;
      if(var1 != null) {
         try {
            var2 = this.a(var1, false);
         } catch (ClassCastException var4) {
            var2 = (t.f)var3;
         }
      }

      return var2;
   }

   t.f a(Object var1, boolean var2) {
      Object var9 = null;
      Comparator var11 = this.a;
      t.f[] var10 = this.b;
      int var4 = a(var1.hashCode());
      int var5 = var4 & var10.length - 1;
      t.f var6 = var10[var5];
      int var3;
      t.f var7;
      if(var6 != null) {
         Comparable var8;
         if(var11 == h) {
            var8 = (Comparable)var1;
         } else {
            var8 = null;
         }

         while(true) {
            if(var8 != null) {
               var3 = var8.compareTo(var6.f);
            } else {
               var3 = var11.compare(var1, var6.f);
            }

            if(var3 == 0) {
               return var6;
            }

            if(var3 < 0) {
               var7 = var6.b;
            } else {
               var7 = var6.c;
            }

            if(var7 == null) {
               var7 = var6;
               break;
            }

            var6 = var7;
         }
      } else {
         var3 = 0;
         var7 = var6;
      }

      var6 = (t.f)var9;
      if(var2) {
         var6 = this.c;
         t.f var12;
         if(var7 == null) {
            if(var11 == h && !(var1 instanceof Comparable)) {
               throw new ClassCastException(var1.getClass().getName() + " is not Comparable");
            }

            var12 = new t.f(var7, var1, var4, var6, var6.e);
            var10[var5] = var12;
         } else {
            var12 = new t.f(var7, var1, var4, var6, var6.e);
            if(var3 < 0) {
               var7.b = var12;
            } else {
               var7.c = var12;
            }

            this.b(var7, true);
         }

         var3 = this.d;
         this.d = var3 + 1;
         if(var3 > this.f) {
            this.a();
         }

         ++this.e;
         var6 = var12;
      }

      return var6;
   }

   t.f a(Entry var1) {
      t.f var3 = this.a(var1.getKey());
      boolean var2;
      if(var3 != null && this.a(var3.h, var1.getValue())) {
         var2 = true;
      } else {
         var2 = false;
      }

      t.f var4;
      if(var2) {
         var4 = var3;
      } else {
         var4 = null;
      }

      return var4;
   }

   void a(t.f var1, boolean var2) {
      int var4 = 0;
      if(var2) {
         var1.e.d = var1.d;
         var1.d.e = var1.e;
         var1.e = null;
         var1.d = null;
      }

      t.f var6 = var1.b;
      t.f var7 = var1.c;
      t.f var5 = var1.a;
      if(var6 != null && var7 != null) {
         if(var6.i > var7.i) {
            var5 = var6.b();
         } else {
            var5 = var7.a();
         }

         this.a(var5, false);
         var6 = var1.b;
         int var3;
         if(var6 != null) {
            var3 = var6.i;
            var5.b = var6;
            var6.a = var5;
            var1.b = null;
         } else {
            var3 = 0;
         }

         var6 = var1.c;
         if(var6 != null) {
            var4 = var6.i;
            var5.c = var6;
            var6.a = var5;
            var1.c = null;
         }

         var5.i = Math.max(var3, var4) + 1;
         this.a(var1, var5);
      } else {
         if(var6 != null) {
            this.a(var1, var6);
            var1.b = null;
         } else if(var7 != null) {
            this.a(var1, var7);
            var1.c = null;
         } else {
            this.a((t.f)var1, (t.f)null);
         }

         this.b(var5, false);
         --this.d;
         ++this.e;
      }

   }

   t.f b(Object var1) {
      t.f var2 = this.a(var1);
      if(var2 != null) {
         this.a(var2, true);
      }

      return var2;
   }

   public void clear() {
      Arrays.fill(this.b, (Object)null);
      this.d = 0;
      ++this.e;
      t.f var3 = this.c;

      t.f var2;
      for(t.f var1 = var3.d; var1 != var3; var1 = var2) {
         var2 = var1.d;
         var1.e = null;
         var1.d = null;
      }

      var3.e = var3;
      var3.d = var3;
   }

   public boolean containsKey(Object var1) {
      boolean var2;
      if(this.a(var1) != null) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public Set entrySet() {
      t.c var1 = this.i;
      if(var1 == null) {
         var1 = new t.c();
         this.i = var1;
      }

      return var1;
   }

   public Object get(Object var1) {
      t.f var2 = this.a(var1);
      if(var2 != null) {
         var1 = var2.h;
      } else {
         var1 = null;
      }

      return var1;
   }

   public Set keySet() {
      t.d var1 = this.j;
      if(var1 == null) {
         var1 = new t.d();
         this.j = var1;
      }

      return var1;
   }

   public Object put(Object var1, Object var2) {
      if(var1 == null) {
         throw new NullPointerException("key == null");
      } else {
         t.f var3 = this.a(var1, true);
         var1 = var3.h;
         var3.h = var2;
         return var1;
      }
   }

   public Object remove(Object var1) {
      t.f var2 = this.b(var1);
      if(var2 != null) {
         var1 = var2.h;
      } else {
         var1 = null;
      }

      return var1;
   }

   public int size() {
      return this.d;
   }

   static final class a {
      private t.f a;
      private int b;
      private int c;
      private int d;

      t.f a() {
         t.f var1 = this.a;
         if(var1.a != null) {
            throw new IllegalStateException();
         } else {
            return var1;
         }
      }

      void a(int var1) {
         this.b = Integer.highestOneBit(var1) * 2 - 1 - var1;
         this.d = 0;
         this.c = 0;
         this.a = null;
      }

      void a(t.f var1) {
         var1.c = null;
         var1.a = null;
         var1.b = null;
         var1.i = 1;
         if(this.b > 0 && (this.d & 1) == 0) {
            ++this.d;
            --this.b;
            ++this.c;
         }

         var1.a = this.a;
         this.a = var1;
         ++this.d;
         if(this.b > 0 && (this.d & 1) == 0) {
            ++this.d;
            --this.b;
            ++this.c;
         }

         for(int var2 = 4; (this.d & var2 - 1) == var2 - 1; var2 *= 2) {
            t.f var3;
            if(this.c == 0) {
               var1 = this.a;
               t.f var4 = var1.a;
               var3 = var4.a;
               var4.a = var3.a;
               this.a = var4;
               var4.b = var3;
               var4.c = var1;
               var4.i = var1.i + 1;
               var3.a = var4;
               var1.a = var4;
            } else if(this.c == 1) {
               var3 = this.a;
               var1 = var3.a;
               this.a = var1;
               var1.c = var3;
               var1.i = var3.i + 1;
               var3.a = var1;
               this.c = 0;
            } else if(this.c == 2) {
               this.c = 0;
            }
         }

      }
   }

   static class b {
      private t.f a;

      public t.f a() {
         t.f var1 = null;
         t.f var3 = this.a;
         if(var3 != null) {
            t.f var2 = var3.a;
            var3.a = null;

            t.f var4;
            for(var1 = var3.c; var1 != null; var1 = var4) {
               var1.a = var2;
               var4 = var1.b;
               var2 = var1;
            }

            this.a = var2;
            var1 = var3;
         }

         return var1;
      }

      void a(t.f var1) {
         t.f var2;
         t.f var3;
         for(var2 = null; var1 != null; var1 = var3) {
            var1.a = var2;
            var3 = var1.b;
            var2 = var1;
         }

         this.a = var2;
      }
   }

   final class c extends AbstractSet {
      public void clear() {
         t.this.clear();
      }

      public boolean contains(Object var1) {
         boolean var2;
         if(var1 instanceof Entry && t.this.a((Entry)var1) != null) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Iterator iterator() {
         return new t.e() {
            public Entry a() {
               return this.b();
            }

            // $FF: synthetic method
            public Object next() {
               return this.a();
            }
         };
      }

      public boolean remove(Object var1) {
         boolean var2 = false;
         if(var1 instanceof Entry) {
            t.f var3 = t.this.a((Entry)var1);
            if(var3 != null) {
               t.this.a(var3, true);
               var2 = true;
            }
         }

         return var2;
      }

      public int size() {
         return t.this.d;
      }
   }

   final class d extends AbstractSet {
      public void clear() {
         t.this.clear();
      }

      public boolean contains(Object var1) {
         return t.this.containsKey(var1);
      }

      public Iterator iterator() {
         return new t.e() {
            public Object next() {
               return this.b().f;
            }
         };
      }

      public boolean remove(Object var1) {
         boolean var2;
         if(t.this.b(var1) != null) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public int size() {
         return t.this.d;
      }
   }

   abstract class e implements Iterator {
      t.f b;
      t.f c;
      int d;

      e() {
         this.b = t.this.c.d;
         this.c = null;
         this.d = t.this.e;
      }

      final t.f b() {
         t.f var1 = this.b;
         if(var1 == t.this.c) {
            throw new NoSuchElementException();
         } else if(t.this.e != this.d) {
            throw new ConcurrentModificationException();
         } else {
            this.b = var1.d;
            this.c = var1;
            return var1;
         }
      }

      public final boolean hasNext() {
         boolean var1;
         if(this.b != t.this.c) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public final void remove() {
         if(this.c == null) {
            throw new IllegalStateException();
         } else {
            t.this.a(this.c, true);
            this.c = null;
            this.d = t.this.e;
         }
      }
   }

   static final class f implements Entry {
      t.f a;
      t.f b;
      t.f c;
      t.f d;
      t.f e;
      final Object f;
      final int g;
      Object h;
      int i;

      f() {
         this.f = null;
         this.g = -1;
         this.e = this;
         this.d = this;
      }

      f(t.f var1, Object var2, int var3, t.f var4, t.f var5) {
         this.a = var1;
         this.f = var2;
         this.g = var3;
         this.i = 1;
         this.d = var4;
         this.e = var5;
         var5.d = this;
         var4.e = this;
      }

      public t.f a() {
         t.f var1 = this.b;

         t.f var2;
         t.f var3;
         for(var2 = this; var1 != null; var1 = var3) {
            var3 = var1.b;
            var2 = var1;
         }

         return var2;
      }

      public t.f b() {
         t.f var1 = this.c;

         t.f var2;
         t.f var3;
         for(var2 = this; var1 != null; var1 = var3) {
            var3 = var1.c;
            var2 = var1;
         }

         return var2;
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof Entry) {
            Entry var4 = (Entry)var1;
            if(this.f == null) {
               var2 = var3;
               if(var4.getKey() != null) {
                  return var2;
               }
            } else {
               var2 = var3;
               if(!this.f.equals(var4.getKey())) {
                  return var2;
               }
            }

            if(this.h == null) {
               var2 = var3;
               if(var4.getValue() != null) {
                  return var2;
               }
            } else {
               var2 = var3;
               if(!this.h.equals(var4.getValue())) {
                  return var2;
               }
            }

            var2 = true;
         }

         return var2;
      }

      public Object getKey() {
         return this.f;
      }

      public Object getValue() {
         return this.h;
      }

      public int hashCode() {
         int var2 = 0;
         int var1;
         if(this.f == null) {
            var1 = 0;
         } else {
            var1 = this.f.hashCode();
         }

         if(this.h != null) {
            var2 = this.h.hashCode();
         }

         return var1 ^ var2;
      }

      public Object setValue(Object var1) {
         Object var2 = this.h;
         this.h = var1;
         return var2;
      }

      public String toString() {
         return this.f + "=" + this.h;
      }
   }
}
