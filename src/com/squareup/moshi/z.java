package com.squareup.moshi;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

final class z {
   public static final Set a = Collections.emptySet();

   public static Set a(AnnotatedElement var0) {
      return a(var0.getAnnotations());
   }

   public static Set a(Annotation[] var0) {
      int var2 = var0.length;
      LinkedHashSet var3 = null;

      LinkedHashSet var4;
      for(int var1 = 0; var1 < var2; var3 = var4) {
         Annotation var5 = var0[var1];
         var4 = var3;
         if(var5.annotationType().isAnnotationPresent(j.class)) {
            var4 = var3;
            if(var3 == null) {
               var4 = new LinkedHashSet();
            }

            var4.add(var5);
         }

         ++var1;
      }

      Set var6;
      if(var3 != null) {
         var6 = Collections.unmodifiableSet(var3);
      } else {
         var6 = a;
      }

      return var6;
   }

   public static boolean b(Annotation[] var0) {
      boolean var4 = false;
      int var2 = var0.length;
      int var1 = 0;

      boolean var3;
      while(true) {
         var3 = var4;
         if(var1 >= var2) {
            break;
         }

         if(var0[var1].annotationType().getSimpleName().equals("Nullable")) {
            var3 = true;
            break;
         }

         ++var1;
      }

      return var3;
   }
}
