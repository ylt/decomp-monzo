package com.squareup.moshi;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Set;
import javax.annotation.Nullable;

public abstract class i {
   @Nullable
   public final Object a(c.e var1) throws IOException {
      return this.a(k.a(var1));
   }

   @Nullable
   public abstract Object a(k var1) throws IOException;

   @Nullable
   public final Object a(@Nullable Object var1) {
      o var3 = new o(var1);

      try {
         var1 = this.a((k)var3);
         return var1;
      } catch (IOException var2) {
         throw new AssertionError(var2);
      }
   }

   public abstract void a(p var1, @Nullable Object var2) throws IOException;

   public final i c() {
      return new i() {
         @Nullable
         public Object a(k var1) throws IOException {
            return i.this.a(var1);
         }

         public void a(p var1, @Nullable Object var2) throws IOException {
            boolean var3 = var1.h();
            var1.c(true);

            try {
               i.this.a(var1, var2);
            } finally {
               var1.c(var3);
            }

         }

         public String toString() {
            return i.this + ".serializeNulls()";
         }
      };
   }

   public final i d() {
      return new i() {
         @Nullable
         public Object a(k var1) throws IOException {
            Object var2;
            if(var1.h() == k.b.i) {
               var2 = var1.l();
            } else {
               var2 = i.this.a(var1);
            }

            return var2;
         }

         public void a(p var1, @Nullable Object var2) throws IOException {
            if(var2 == null) {
               var1.e();
            } else {
               i.this.a(var1, var2);
            }

         }

         public String toString() {
            return i.this + ".nullSafe()";
         }
      };
   }

   public final i e() {
      return new i() {
         @Nullable
         public Object a(k var1) throws IOException {
            boolean var2 = var1.a();
            var1.a(true);

            Object var3;
            try {
               var3 = i.this.a(var1);
            } finally {
               var1.a(var2);
            }

            return var3;
         }

         public void a(p var1, @Nullable Object var2) throws IOException {
            boolean var3 = var1.g();
            var1.b(true);

            try {
               i.this.a(var1, var2);
            } finally {
               var1.b(var3);
            }

         }

         public String toString() {
            return i.this + ".lenient()";
         }
      };
   }

   public final i f() {
      return new i() {
         @Nullable
         public Object a(k var1) throws IOException {
            boolean var2 = var1.b();
            var1.b(true);

            Object var3;
            try {
               var3 = i.this.a(var1);
            } finally {
               var1.b(var2);
            }

            return var3;
         }

         public void a(p var1, @Nullable Object var2) throws IOException {
            i.this.a(var1, var2);
         }

         public String toString() {
            return i.this + ".failOnUnknown()";
         }
      };
   }

   public interface a {
      @Nullable
      i a(Type var1, Set var2, v var3);
   }
}
