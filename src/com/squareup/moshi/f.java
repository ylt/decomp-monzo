package com.squareup.moshi;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;

abstract class f extends i {
   public static final i.a a = new i.a() {
      @Nullable
      public i a(Type var1, Set var2, v var3) {
         Object var4 = null;
         Class var5 = y.e(var1);
         i var6;
         if(!var2.isEmpty()) {
            var6 = (i)var4;
         } else if(var5 != List.class && var5 != Collection.class) {
            var6 = (i)var4;
            if(var5 == Set.class) {
               var6 = f.b(var1, var3).d();
            }
         } else {
            var6 = f.a(var1, var3).d();
         }

         return var6;
      }
   };
   private final i b;

   private f(i var1) {
      this.b = var1;
   }

   // $FF: synthetic method
   f(i var1, Object var2) {
      this(var1);
   }

   static i a(Type var0, v var1) {
      return new f(var1.a(y.a(var0, Collection.class))) {
         // $FF: synthetic method
         public Object a(k var1) throws IOException {
            return super.b(var1);
         }

         Collection a() {
            return new ArrayList();
         }
      };
   }

   static i b(Type var0, v var1) {
      return new f(var1.a(y.a(var0, Collection.class))) {
         // $FF: synthetic method
         public Object a(k var1) throws IOException {
            return super.b(var1);
         }

         // $FF: synthetic method
         Collection a() {
            return this.b();
         }

         Set b() {
            return new LinkedHashSet();
         }
      };
   }

   // $FF: synthetic method
   public Object a(k var1) throws IOException {
      return this.b(var1);
   }

   abstract Collection a();

   public void a(p var1, Collection var2) throws IOException {
      var1.a();
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         Object var4 = var3.next();
         this.b.a(var1, var4);
      }

      var1.b();
   }

   public Collection b(k var1) throws IOException {
      Collection var2 = this.a();
      var1.c();

      while(var1.g()) {
         var2.add(this.b.a(var1));
      }

      var1.d();
      return var2;
   }

   public String toString() {
      return this.b + ".collection()";
   }
}
