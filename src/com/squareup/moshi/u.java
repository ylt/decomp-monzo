package com.squareup.moshi;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import javax.annotation.Nullable;

final class u extends i {
   public static final i.a a = new i.a() {
      @Nullable
      public i a(Type var1, Set var2, v var3) {
         Object var4 = null;
         i var7;
         if(!var2.isEmpty()) {
            var7 = (i)var4;
         } else {
            Class var5 = y.e(var1);
            var7 = (i)var4;
            if(var5 == Map.class) {
               Type[] var6 = y.b(var1, var5);
               var7 = (new u(var3, var6[0], var6[1])).d();
            }
         }

         return var7;
      }
   };
   private final i b;
   private final i c;

   u(v var1, Type var2, Type var3) {
      this.b = var1.a(var2);
      this.c = var1.a(var3);
   }

   // $FF: synthetic method
   public Object a(k var1) throws IOException {
      return this.b(var1);
   }

   public void a(p var1, Map var2) throws IOException {
      var1.c();
      Iterator var3 = var2.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var4 = (Entry)var3.next();
         if(var4.getKey() == null) {
            throw new JsonDataException("Map key is null at " + var1.j());
         }

         var1.i();
         this.b.a(var1, var4.getKey());
         this.c.a(var1, var4.getValue());
      }

      var1.d();
   }

   public Map b(k var1) throws IOException {
      t var4 = new t();
      var1.e();

      Object var2;
      Object var3;
      Object var5;
      do {
         if(!var1.g()) {
            var1.f();
            return var4;
         }

         var1.s();
         var3 = this.b.a(var1);
         var2 = this.c.a(var1);
         var5 = var4.put(var3, var2);
      } while(var5 == null);

      throw new JsonDataException("Map key '" + var3 + "' has multiple values at path " + var1.r() + ": " + var5 + " and " + var2);
   }

   public String toString() {
      return "JsonAdapter(" + this.b + "=" + this.c + ")";
   }
}
