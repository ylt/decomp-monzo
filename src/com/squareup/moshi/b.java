package com.squareup.moshi;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;

final class b implements i.a {
   private final List a;
   private final List b;

   b(List var1, List var2) {
      this.a = var1;
      this.b = var2;
   }

   static b.a a(Object var0, Method var1) {
      var1.setAccessible(true);
      final Type var7 = var1.getGenericReturnType();
      Type[] var3 = var1.getGenericParameterTypes();
      Annotation[][] var4 = var1.getParameterAnnotations();
      b.a var8;
      if(var3.length >= 2 && var3[0] == p.class && var7 == Void.TYPE && a(2, var3)) {
         Set var9 = z.a(var4[1]);
         var8 = new b.a(var3[1], var9, var0, var1, var3.length, 2, true) {
            public void a(v var1, p var2, @Nullable Object var3) throws IOException, InvocationTargetException {
               this.a(var2, var3);
            }
         };
      } else {
         if(var3.length != 1 || var7 == Void.TYPE) {
            throw new IllegalArgumentException("Unexpected signature for " + var1 + ".\n" + "@ToJson method signatures may have one of the following structures:\n" + "    <any access modifier> void toJson(JsonWriter writer, T value) throws <any>;\n" + "    <any access modifier> void toJson(JsonWriter writer," + " JsonAdapter<any> delegate, <any more delegates>) throws <any>;\n" + "    <any access modifier> void toJson(JsonWriter writer, T value" + " JsonAdapter<any> delegate, <any more delegates>) throws <any>;\n" + "    <any access modifier> R toJson(T value) throws <any>;\n");
         }

         final Set var6 = z.a((AnnotatedElement)var1);
         Set var5 = z.a(var4[0]);
         boolean var2 = z.b(var4[0]);
         var8 = new b.a(var3[0], var5, var0, var1, var3.length, 1, var2) {
            private i c;

            public void a(v var1, i.a var2) {
               super.a(var1, var2);
               this.c = var1.a(var7, var6);
            }

            public void a(v var1, p var2, @Nullable Object var3) throws IOException, InvocationTargetException {
               Object var4 = this.a(var3);
               this.c.a(var2, var4);
            }
         };
      }

      return var8;
   }

   @Nullable
   private static b.a a(List var0, Type var1, Set var2) {
      int var4 = var0.size();
      int var3 = 0;

      b.a var6;
      while(true) {
         if(var3 >= var4) {
            var6 = null;
            break;
         }

         b.a var5 = (b.a)var0.get(var3);
         if(var5.d.equals(var1) && var5.e.equals(var2)) {
            var6 = var5;
            break;
         }

         ++var3;
      }

      return var6;
   }

   public static b a(Object var0) {
      ArrayList var5 = new ArrayList();
      ArrayList var4 = new ArrayList();

      for(Class var3 = var0.getClass(); var3 != Object.class; var3 = var3.getSuperclass()) {
         Method[] var6 = var3.getDeclaredMethods();
         int var2 = var6.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            Method var8 = var6[var1];
            b.a var7;
            if(var8.isAnnotationPresent(x.class)) {
               var7 = a(var0, var8);
               b.a var9 = a((List)var5, (Type)var7.d, (Set)var7.e);
               if(var9 != null) {
                  throw new IllegalArgumentException("Conflicting @ToJson methods:\n    " + var9.g + "\n" + "    " + var7.g);
               }

               var5.add(var7);
            }

            if(var8.isAnnotationPresent(g.class)) {
               b.a var10 = b(var0, var8);
               var7 = a((List)var4, (Type)var10.d, (Set)var10.e);
               if(var7 != null) {
                  throw new IllegalArgumentException("Conflicting @FromJson methods:\n    " + var7.g + "\n" + "    " + var10.g);
               }

               var4.add(var10);
            }
         }
      }

      if(var5.isEmpty() && var4.isEmpty()) {
         throw new IllegalArgumentException("Expected at least one @ToJson or @FromJson method on " + var0.getClass().getName());
      } else {
         return new b(var5, var4);
      }
   }

   private static boolean a(int var0, Type[] var1) {
      int var2 = var1.length;

      boolean var3;
      while(true) {
         if(var0 >= var2) {
            var3 = true;
            break;
         }

         if(!(var1[var0] instanceof ParameterizedType)) {
            var3 = false;
            break;
         }

         if(((ParameterizedType)var1[var0]).getRawType() != i.class) {
            var3 = false;
            break;
         }

         ++var0;
      }

      return var3;
   }

   static b.a b(Object var0, Method var1) {
      var1.setAccessible(true);
      Type var4 = var1.getGenericReturnType();
      Set var6 = z.a((AnnotatedElement)var1);
      final Type[] var3 = var1.getGenericParameterTypes();
      Annotation[][] var5 = var1.getParameterAnnotations();
      b.a var8;
      if(var3.length >= 1 && var3[0] == k.class && var4 != Void.TYPE && a(1, var3)) {
         var8 = new b.a(var4, var6, var0, var1, var3.length, 1, true) {
            public Object a(v var1, k var2) throws IOException, InvocationTargetException {
               return this.a(var2);
            }
         };
      } else {
         if(var3.length != 1 || var4 == Void.TYPE) {
            throw new IllegalArgumentException("Unexpected signature for " + var1 + ".\n" + "@FromJson method signatures may have one of the following structures:\n" + "    <any access modifier> R fromJson(JsonReader jsonReader) throws <any>;\n" + "    <any access modifier> R fromJson(JsonReader jsonReader," + " JsonAdapter<any> delegate, <any more delegates>) throws <any>;\n" + "    <any access modifier> R fromJson(T value) throws <any>;\n");
         }

         final Set var7 = z.a(var5[0]);
         boolean var2 = z.b(var5[0]);
         var8 = new b.a(var4, var6, var0, var1, var3.length, 1, var2) {
            i a;

            public Object a(v var1, k var2) throws IOException, InvocationTargetException {
               return this.a(this.a.a(var2));
            }

            public void a(v var1, i.a var2) {
               super.a(var1, var2);
               this.a = var1.a(var3[0], var7);
            }
         };
      }

      return var8;
   }

   @Nullable
   public i a(final Type var1, final Set var2, final v var3) {
      final i var4 = null;
      final b.a var5 = a(this.a, var1, var2);
      final b.a var6 = a(this.b, var1, var2);
      i var8;
      if(var5 == null && var6 == null) {
         var8 = var4;
      } else {
         if(var5 != null && var6 != null) {
            var4 = null;
         } else {
            try {
               var4 = var3.a(this, var1, var2);
            } catch (IllegalArgumentException var7) {
               String var9;
               if(var5 == null) {
                  var9 = "@ToJson";
               } else {
                  var9 = "@FromJson";
               }

               throw new IllegalArgumentException("No " + var9 + " adapter for " + var1 + " annotated " + var2);
            }
         }

         if(var5 != null) {
            var5.a((v)var3, (i.a)this);
         }

         if(var6 != null) {
            var6.a((v)var3, (i.a)this);
         }

         var8 = new i() {
            @Nullable
            public Object a(k var1x) throws IOException {
               Object var4x;
               if(var6 == null) {
                  var4x = var4.a(var1x);
               } else if(!var6.j && var1x.h() == k.b.i) {
                  var1x.l();
                  var4x = null;
               } else {
                  Object var5x;
                  try {
                     var5x = var6.a(var3, var1x);
                  } catch (InvocationTargetException var3x) {
                     Throwable var2x = var3x.getCause();
                     if(var2x instanceof IOException) {
                        throw (IOException)var2x;
                     }

                     throw new JsonDataException(var2x + " at " + var1x.r(), var2x);
                  }

                  var4x = var5x;
               }

               return var4x;
            }

            public void a(p var1x, @Nullable Object var2x) throws IOException {
               if(var5 == null) {
                  var4.a(var1x, var2x);
               } else if(!var5.j && var2x == null) {
                  var1x.e();
               } else {
                  try {
                     var5.a(var3, var1x, var2x);
                  } catch (InvocationTargetException var3x) {
                     Throwable var4x = var3x.getCause();
                     if(var4x instanceof IOException) {
                        throw (IOException)var4x;
                     }

                     throw new JsonDataException(var4x + " at " + var1x.j(), var4x);
                  }
               }

            }

            public String toString() {
               return "JsonAdapter" + var2 + "(" + var1 + ")";
            }
         };
      }

      return var8;
   }

   abstract static class a {
      final Type d;
      final Set e;
      final Object f;
      final Method g;
      final int h;
      final i[] i;
      final boolean j;

      a(Type var1, Set var2, Object var3, Method var4, int var5, int var6, boolean var7) {
         this.d = y.d(var1);
         this.e = var2;
         this.f = var3;
         this.g = var4;
         this.h = var6;
         this.i = new i[var5 - var6];
         this.j = var7;
      }

      @Nullable
      public Object a(v var1, k var2) throws IOException, InvocationTargetException {
         throw new AssertionError();
      }

      @Nullable
      protected Object a(@Nullable Object var1) throws InvocationTargetException {
         Object[] var2 = new Object[this.i.length + 1];
         var2[0] = var1;
         System.arraycopy(this.i, 0, var2, 1, this.i.length);

         try {
            var1 = this.g.invoke(this.f, var2);
            return var1;
         } catch (IllegalAccessException var3) {
            throw new AssertionError();
         }
      }

      protected Object a(@Nullable Object var1, @Nullable Object var2) throws InvocationTargetException {
         Object[] var3 = new Object[this.i.length + 2];
         var3[0] = var1;
         var3[1] = var2;
         System.arraycopy(this.i, 0, var3, 2, this.i.length);

         try {
            var1 = this.g.invoke(this.f, var3);
            return var1;
         } catch (IllegalAccessException var4) {
            throw new AssertionError();
         }
      }

      public void a(v var1, i.a var2) {
         if(this.i.length > 0) {
            Type[] var8 = this.g.getGenericParameterTypes();
            Annotation[][] var9 = this.g.getParameterAnnotations();
            int var3 = this.h;

            for(int var4 = var8.length; var3 < var4; ++var3) {
               Type var6 = ((ParameterizedType)var8[var3]).getActualTypeArguments()[0];
               Set var10 = z.a(var9[var3]);
               i[] var7 = this.i;
               int var5 = this.h;
               i var11;
               if(y.a(this.d, var6) && this.e.equals(var10)) {
                  var11 = var1.a(var2, var6, var10);
               } else {
                  var11 = var1.a(var6, var10);
               }

               var7[var3 - var5] = var11;
            }
         }

      }

      public void a(v var1, p var2, @Nullable Object var3) throws IOException, InvocationTargetException {
         throw new AssertionError();
      }
   }
}
