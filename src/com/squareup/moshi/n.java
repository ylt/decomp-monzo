package com.squareup.moshi;

import java.io.IOException;
import javax.annotation.Nullable;

final class n extends p {
   private static final String[] i = new String[128];
   private final c.d j;
   private String k = ":";
   private String l;

   static {
      for(int var0 = 0; var0 <= 31; ++var0) {
         i[var0] = String.format("\\u%04x", new Object[]{Integer.valueOf(var0)});
      }

      i[34] = "\\\"";
      i[92] = "\\\\";
      i[9] = "\\t";
      i[8] = "\\b";
      i[10] = "\\n";
      i[13] = "\\r";
      i[12] = "\\f";
   }

   n(c.d var1) {
      if(var1 == null) {
         throw new NullPointerException("sink == null");
      } else {
         this.j = var1;
         this.a(6);
      }
   }

   private p a(int var1, int var2, String var3) throws IOException {
      int var4 = this.f();
      if(var4 != var2 && var4 != var1) {
         throw new IllegalStateException("Nesting problem.");
      } else if(this.l != null) {
         throw new IllegalStateException("Dangling name: " + this.l);
      } else {
         --this.a;
         this.c[this.a] = null;
         int[] var5 = this.d;
         var1 = this.a - 1;
         ++var5[var1];
         if(var4 == var2) {
            this.l();
         }

         this.j.b(var3);
         return this;
      }
   }

   private p a(int var1, String var2) throws IOException {
      this.n();
      this.a(var1);
      this.d[this.a - 1] = 0;
      this.j.b(var2);
      return this;
   }

   static void a(c.d var0, String var1) throws IOException {
      int var4 = 0;
      String[] var9 = i;
      var0.i(34);
      int var5 = var1.length();

      int var3;
      for(int var2 = 0; var2 < var5; var4 = var3) {
         label37: {
            char var6 = var1.charAt(var2);
            String var7;
            if(var6 < 128) {
               String var8 = var9[var6];
               var7 = var8;
               if(var8 == null) {
                  var3 = var4;
                  break label37;
               }
            } else if(var6 == 8232) {
               var7 = "\\u2028";
            } else {
               var3 = var4;
               if(var6 != 8233) {
                  break label37;
               }

               var7 = "\\u2029";
            }

            if(var4 < var2) {
               var0.b(var1, var4, var2);
            }

            var0.b(var7);
            var3 = var2 + 1;
         }

         ++var2;
      }

      if(var4 < var5) {
         var0.b(var1, var4, var5);
      }

      var0.i(34);
   }

   private void k() throws IOException {
      if(this.l != null) {
         this.m();
         a(this.j, this.l);
         this.l = null;
      }

   }

   private void l() throws IOException {
      if(this.e != null) {
         this.j.i(10);
         int var1 = 1;

         for(int var2 = this.a; var1 < var2; ++var1) {
            this.j.b(this.e);
         }
      }

   }

   private void m() throws IOException {
      int var1 = this.f();
      if(var1 == 5) {
         this.j.i(44);
      } else if(var1 != 3) {
         throw new IllegalStateException("Nesting problem.");
      }

      this.l();
      this.b(4);
   }

   private void n() throws IOException {
      switch(this.f()) {
      case 1:
         this.b(2);
         this.l();
         break;
      case 2:
         this.j.i(44);
         this.l();
         break;
      case 3:
      case 5:
      default:
         throw new IllegalStateException("Nesting problem.");
      case 4:
         this.j.b(this.k);
         this.b(5);
         break;
      case 7:
         if(!this.f) {
            throw new IllegalStateException("JSON must have only one top-level value.");
         }
      case 6:
         this.b(7);
      }

   }

   public p a() throws IOException {
      this.k();
      return this.a(1, "[");
   }

   public p a(double var1) throws IOException {
      if(this.f || !Double.isNaN(var1) && !Double.isInfinite(var1)) {
         Object var4;
         if(this.h) {
            var4 = this.a(Double.toString(var1));
         } else {
            this.k();
            this.n();
            this.j.b(Double.toString(var1));
            int[] var5 = this.d;
            int var3 = this.a - 1;
            ++var5[var3];
            var4 = this;
         }

         return (p)var4;
      } else {
         throw new IllegalArgumentException("Numeric values must be finite, but was " + var1);
      }
   }

   public p a(long var1) throws IOException {
      Object var4;
      if(this.h) {
         var4 = this.a(Long.toString(var1));
      } else {
         this.k();
         this.n();
         this.j.b(Long.toString(var1));
         int[] var5 = this.d;
         int var3 = this.a - 1;
         ++var5[var3];
         var4 = this;
      }

      return (p)var4;
   }

   public p a(@Nullable Number var1) throws IOException {
      Object var4;
      if(var1 == null) {
         var4 = this.e();
      } else {
         String var3 = var1.toString();
         if(!this.f && (var3.equals("-Infinity") || var3.equals("Infinity") || var3.equals("NaN"))) {
            throw new IllegalArgumentException("Numeric values must be finite, but was " + var1);
         }

         if(this.h) {
            var4 = this.a(var3);
         } else {
            this.k();
            this.n();
            this.j.b(var3);
            int[] var5 = this.d;
            int var2 = this.a - 1;
            ++var5[var2];
            var4 = this;
         }
      }

      return (p)var4;
   }

   public p a(String var1) throws IOException {
      if(var1 == null) {
         throw new NullPointerException("name == null");
      } else if(this.a == 0) {
         throw new IllegalStateException("JsonWriter is closed.");
      } else if(this.l != null) {
         throw new IllegalStateException("Nesting problem.");
      } else {
         this.l = var1;
         this.c[this.a - 1] = var1;
         this.h = false;
         return this;
      }
   }

   public p a(boolean var1) throws IOException {
      this.k();
      this.n();
      c.d var4 = this.j;
      String var3;
      if(var1) {
         var3 = "true";
      } else {
         var3 = "false";
      }

      var4.b(var3);
      int[] var5 = this.d;
      int var2 = this.a - 1;
      ++var5[var2];
      return this;
   }

   public p b() throws IOException {
      return this.a(1, 2, "]");
   }

   public p b(String var1) throws IOException {
      Object var3;
      if(var1 == null) {
         var3 = this.e();
      } else if(this.h) {
         var3 = this.a(var1);
      } else {
         this.k();
         this.n();
         a(this.j, var1);
         int[] var4 = this.d;
         int var2 = this.a - 1;
         ++var4[var2];
         var3 = this;
      }

      return (p)var3;
   }

   public p c() throws IOException {
      this.k();
      return this.a(3, "{");
   }

   public void close() throws IOException {
      this.j.close();
      int var1 = this.a;
      if(var1 <= 1 && (var1 != 1 || this.b[var1 - 1] == 7)) {
         this.a = 0;
      } else {
         throw new IOException("Incomplete document");
      }
   }

   public p d() throws IOException {
      this.h = false;
      return this.a(3, 5, "}");
   }

   public p e() throws IOException {
      if(this.l != null) {
         if(!this.g) {
            this.l = null;
            return this;
         }

         this.k();
      }

      this.n();
      this.j.b("null");
      int[] var2 = this.d;
      int var1 = this.a - 1;
      ++var2[var1];
      return this;
   }

   public void flush() throws IOException {
      if(this.a == 0) {
         throw new IllegalStateException("JsonWriter is closed.");
      } else {
         this.j.flush();
      }
   }
}
