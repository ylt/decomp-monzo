package com.squareup.moshi;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import javax.annotation.Nullable;

public abstract class p implements Closeable, Flushable {
   int a = 0;
   final int[] b = new int[32];
   final String[] c = new String[32];
   final int[] d = new int[32];
   String e;
   boolean f;
   boolean g;
   boolean h;

   public static p a(c.d var0) {
      return new n(var0);
   }

   public abstract p a() throws IOException;

   public abstract p a(double var1) throws IOException;

   public abstract p a(long var1) throws IOException;

   public abstract p a(@Nullable Number var1) throws IOException;

   public abstract p a(String var1) throws IOException;

   public abstract p a(boolean var1) throws IOException;

   final void a(int var1) {
      if(this.a == this.b.length) {
         throw new JsonDataException("Nesting too deep at " + this.j() + ": circular reference?");
      } else {
         int[] var3 = this.b;
         int var2 = this.a;
         this.a = var2 + 1;
         var3[var2] = var1;
      }
   }

   public abstract p b() throws IOException;

   public abstract p b(@Nullable String var1) throws IOException;

   final void b(int var1) {
      this.b[this.a - 1] = var1;
   }

   public final void b(boolean var1) {
      this.f = var1;
   }

   public abstract p c() throws IOException;

   public final void c(boolean var1) {
      this.g = var1;
   }

   public abstract p d() throws IOException;

   public abstract p e() throws IOException;

   final int f() {
      if(this.a == 0) {
         throw new IllegalStateException("JsonWriter is closed.");
      } else {
         return this.b[this.a - 1];
      }
   }

   public final boolean g() {
      return this.f;
   }

   public final boolean h() {
      return this.g;
   }

   final void i() throws IOException {
      int var1 = this.f();
      if(var1 != 5 && var1 != 3) {
         throw new IllegalStateException("Nesting problem.");
      } else {
         this.h = true;
      }
   }

   public final String j() {
      return l.a(this.a, this.b, this.c, this.d);
   }
}
