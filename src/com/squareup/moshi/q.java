package com.squareup.moshi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 1},
   d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002:\u0002\u001e\u001fB9\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u001c\u0010\u0005\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00028\u0000\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00070\u0006\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u0015\u0010\u0012\u001a\u00028\u00002\u0006\u0010\u0013\u001a\u00020\u0014H\u0016¢\u0006\u0002\u0010\u0015J\u001f\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00018\u0000H\u0016¢\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u00020\u001dH\u0016R'\u0010\u0005\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00028\u0000\u0012\u0006\u0012\u0004\u0018\u00010\b\u0018\u00010\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011¨\u0006 "},
   d2 = {"Lcom/squareup/moshi/KotlinJsonAdapter;", "T", "Lcom/squareup/moshi/JsonAdapter;", "constructor", "Lkotlin/reflect/KFunction;", "bindings", "", "Lcom/squareup/moshi/KotlinJsonAdapter$Binding;", "", "options", "Lcom/squareup/moshi/JsonReader$Options;", "(Lkotlin/reflect/KFunction;Ljava/util/List;Lcom/squareup/moshi/JsonReader$Options;)V", "getBindings", "()Ljava/util/List;", "getConstructor", "()Lkotlin/reflect/KFunction;", "getOptions", "()Lcom/squareup/moshi/JsonReader$Options;", "fromJson", "reader", "Lcom/squareup/moshi/JsonReader;", "(Lcom/squareup/moshi/JsonReader;)Ljava/lang/Object;", "toJson", "", "writer", "Lcom/squareup/moshi/JsonWriter;", "value", "(Lcom/squareup/moshi/JsonWriter;Ljava/lang/Object;)V", "toString", "", "Binding", "IndexedParameterMap", "moshi-kotlin"},
   k = 1,
   mv = {1, 1, 5}
)
public final class q extends i {
   private final kotlin.reflect.f a;
   private final List b;
   private final k.a c;

   public q(kotlin.reflect.f var1, List var2, k.a var3) {
      kotlin.d.b.l.b(var1, "constructor");
      kotlin.d.b.l.b(var2, "bindings");
      kotlin.d.b.l.b(var3, "options");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public Object a(k var1) {
      kotlin.d.b.l.b(var1, "reader");
      int var3 = this.a.h().size();
      int var2 = this.b.size();
      Object[] var5 = new Object[var2];
      int var4 = var2 - 1;
      if(var4 <= 0) {
         var2 = 0;

         while(true) {
            var5[var2] = a.a;
            if(var2 == var4) {
               break;
            }

            ++var2;
         }
      }

      Object[] var6 = (Object[])var5;
      var1.e();

      while(var1.g()) {
         var2 = var1.a(this.c);
         q.a var8;
         if(var2 != -1) {
            var8 = (q.a)this.b.get(var2);
         } else {
            var8 = null;
         }

         if(var8 == null) {
            var1.i();
            var1.p();
         } else {
            if(var6[var2] != a.a) {
               throw (Throwable)(new JsonDataException("Multiple values for " + ((kotlin.reflect.k)this.a.h().get(var2)).b() + " at " + var1.r()));
            }

            var6[var2] = var8.b().a(var1);
         }
      }

      var1.f();
      kotlin.f.c var9 = kotlin.f.d.b(0, var3);
      var2 = var9.a();
      var4 = var9.b();
      if(var2 <= var4) {
         while(true) {
            if(var6[var2] == a.a && !((kotlin.reflect.k)this.a.h().get(var2)).e()) {
               if(!((kotlin.reflect.k)this.a.h().get(var2)).c().b()) {
                  throw (Throwable)(new JsonDataException("Required value " + ((kotlin.reflect.k)this.a.h().get(var2)).b() + " missing at " + var1.r()));
               }

               var6[var2] = null;
            }

            if(var2 == var4) {
               break;
            }

            ++var2;
         }
      }

      Object var7 = this.a.a((Map)(new q.b(this.a.h(), var6)));
      var9 = kotlin.f.d.b(var3, this.b.size());
      var2 = var9.a();
      var3 = var9.b();
      if(var2 <= var3) {
         while(true) {
            Object var10 = this.b.get(var2);
            if(var10 == null) {
               kotlin.d.b.l.a();
            }

            ((q.a)var10).a(var7, var6[var2]);
            if(var2 == var3) {
               break;
            }

            ++var2;
         }
      }

      return var7;
   }

   public void a(p var1, Object var2) {
      kotlin.d.b.l.b(var1, "writer");
      if(var2 == null) {
         throw (Throwable)(new NullPointerException("value == null"));
      } else {
         var1.c();
         Iterator var3 = this.b.iterator();

         while(var3.hasNext()) {
            q.a var4 = (q.a)var3.next();
            if(var4 != null) {
               var1.a(var4.a());
               var4.b().a(var1, var4.a(var2));
            }
         }

         var1.d();
      }
   }

   public String toString() {
      return "KotlinJsonAdapter(" + this.a.i() + ")";
   }

   @Metadata(
      bv = {1, 0, 1},
      d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000*\u0004\b\u0001\u0010\u0001*\u0004\b\u0002\u0010\u00022\u00020\u0003B9\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00020\u0007\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\u0002\u0010\fJ\t\u0010\u0015\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00020\u0007HÆ\u0003J\u0015\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\tHÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u000bHÆ\u0003JQ\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00002\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00020\u00072\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bHÆ\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÖ\u0003J\u0013\u0010\u001d\u001a\u00028\u00022\u0006\u0010\u001e\u001a\u00028\u0001¢\u0006\u0002\u0010\u001fJ\t\u0010 \u001a\u00020!HÖ\u0001J\u001b\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00028\u00012\u0006\u0010\u001e\u001a\u00028\u0002¢\u0006\u0002\u0010%J\t\u0010&\u001a\u00020\u0005HÖ\u0001R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u001d\u0010\b\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006'"},
      d2 = {"Lcom/squareup/moshi/KotlinJsonAdapter$Binding;", "K", "P", "", "name", "", "adapter", "Lcom/squareup/moshi/JsonAdapter;", "property", "Lkotlin/reflect/KProperty1;", "parameter", "Lkotlin/reflect/KParameter;", "(Ljava/lang/String;Lcom/squareup/moshi/JsonAdapter;Lkotlin/reflect/KProperty1;Lkotlin/reflect/KParameter;)V", "getAdapter", "()Lcom/squareup/moshi/JsonAdapter;", "getName", "()Ljava/lang/String;", "getParameter", "()Lkotlin/reflect/KParameter;", "getProperty", "()Lkotlin/reflect/KProperty1;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "get", "value", "(Ljava/lang/Object;)Ljava/lang/Object;", "hashCode", "", "set", "", "result", "(Ljava/lang/Object;Ljava/lang/Object;)V", "toString", "moshi-kotlin"},
      k = 1,
      mv = {1, 1, 5}
   )
   public static final class a {
      private final String a;
      private final i b;
      private final kotlin.reflect.n c;
      private final kotlin.reflect.k d;

      public a(String var1, i var2, kotlin.reflect.n var3, kotlin.reflect.k var4) {
         kotlin.d.b.l.b(var1, "name");
         kotlin.d.b.l.b(var2, "adapter");
         kotlin.d.b.l.b(var3, "property");
         super();
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      public final Object a(Object var1) {
         return this.c.b(var1);
      }

      public final String a() {
         return this.a;
      }

      public final void a(Object var1, Object var2) {
         if(var2 != a.a) {
            kotlin.reflect.n var3 = this.c;
            if(var3 == null) {
               throw new TypeCastException("null cannot be cast to non-null type kotlin.reflect.KMutableProperty1<K, P>");
            }

            ((kotlin.reflect.i)var3).a(var1, var2);
         }

      }

      public final i b() {
         return this.b;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label32: {
               if(var1 instanceof q.a) {
                  q.a var3 = (q.a)var1;
                  if(kotlin.d.b.l.a(this.a, var3.a) && kotlin.d.b.l.a(this.b, var3.b) && kotlin.d.b.l.a(this.c, var3.c) && kotlin.d.b.l.a(this.d, var3.d)) {
                     break label32;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var4 = 0;
         String var5 = this.a;
         int var1;
         if(var5 != null) {
            var1 = var5.hashCode();
         } else {
            var1 = 0;
         }

         i var6 = this.b;
         int var2;
         if(var6 != null) {
            var2 = var6.hashCode();
         } else {
            var2 = 0;
         }

         kotlin.reflect.n var7 = this.c;
         int var3;
         if(var7 != null) {
            var3 = var7.hashCode();
         } else {
            var3 = 0;
         }

         kotlin.reflect.k var8 = this.d;
         if(var8 != null) {
            var4 = var8.hashCode();
         }

         return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
      }

      public String toString() {
         return "Binding(name=" + this.a + ", adapter=" + this.b + ", property=" + this.c + ", parameter=" + this.d + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 1},
      d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010&\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001B#\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016J\u0013\u0010\u0016\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0015\u001a\u00020\u0002H\u0096\u0002R(\u0010\t\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u000b0\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0007¢\u0006\n\n\u0002\u0010\u0012\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0017"},
      d2 = {"Lcom/squareup/moshi/KotlinJsonAdapter$IndexedParameterMap;", "Lkotlin/collections/AbstractMap;", "Lkotlin/reflect/KParameter;", "", "parameterKeys", "", "parameterValues", "", "(Ljava/util/List;[Ljava/lang/Object;)V", "entries", "", "", "getEntries", "()Ljava/util/Set;", "getParameterKeys", "()Ljava/util/List;", "getParameterValues", "()[Ljava/lang/Object;", "[Ljava/lang/Object;", "containsKey", "", "key", "get", "moshi-kotlin"},
      k = 1,
      mv = {1, 1, 5}
   )
   public static final class b extends kotlin.a.c {
      private final List b;
      private final Object[] c;

      public b(List var1, Object[] var2) {
         kotlin.d.b.l.b(var1, "parameterKeys");
         kotlin.d.b.l.b(var2, "parameterValues");
         super();
         this.b = var1;
         this.c = var2;
      }

      public Object a(kotlin.reflect.k var1, Object var2) {
         return super.getOrDefault(var1, var2);
      }

      public Set a() {
         Iterable var3 = (Iterable)this.b;
         Collection var2 = (Collection)(new ArrayList(kotlin.a.m.a(var3, 10)));
         Iterator var6 = var3.iterator();

         for(int var1 = 0; var6.hasNext(); ++var1) {
            var2.add(new SimpleEntry((kotlin.reflect.k)var6.next(), this.c[var1]));
         }

         var3 = (Iterable)((List)var2);
         var2 = (Collection)(new LinkedHashSet());
         Iterator var4 = var3.iterator();

         while(var4.hasNext()) {
            Object var7 = var4.next();
            boolean var5;
            if(((SimpleEntry)var7).getValue() != a.a) {
               var5 = true;
            } else {
               var5 = false;
            }

            if(var5) {
               var2.add(var7);
            }
         }

         return (Set)var2;
      }

      public boolean a(kotlin.reflect.k var1) {
         kotlin.d.b.l.b(var1, "key");
         boolean var2;
         if(this.c[var1.a()] != a.a) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Object b(kotlin.reflect.k var1) {
         kotlin.d.b.l.b(var1, "key");
         Object var2 = this.c[var1.a()];
         if(var2 == a.a) {
            var2 = null;
         }

         return var2;
      }

      public final boolean containsKey(Object var1) {
         boolean var2;
         if(var1 instanceof kotlin.reflect.k) {
            var2 = this.a((kotlin.reflect.k)var1);
         } else {
            var2 = false;
         }

         return var2;
      }

      public final Object get(Object var1) {
         if(var1 instanceof kotlin.reflect.k) {
            var1 = this.b((kotlin.reflect.k)var1);
         } else {
            var1 = null;
         }

         return var1;
      }

      public final Object getOrDefault(Object var1, Object var2) {
         Object var3 = var2;
         if(var1 instanceof kotlin.reflect.k) {
            var3 = this.a((kotlin.reflect.k)var1, var2);
         }

         return var3;
      }
   }
}
