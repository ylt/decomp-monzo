package com.squareup.moshi;

import java.io.EOFException;
import java.io.IOException;
import java.math.BigDecimal;
import javax.annotation.Nullable;

final class m extends k {
   private static final c.f g = c.f.a("'\\");
   private static final c.f h = c.f.a("\"\\");
   private static final c.f i = c.f.a("{}[]:, \n\t\r\f/\\;#=");
   private static final c.f j = c.f.a("\n\r");
   private final c.e k;
   private final c.c l;
   private int m = 0;
   private long n;
   private int o;
   @Nullable
   private String p;

   m(c.e var1) {
      if(var1 == null) {
         throw new NullPointerException("source == null");
      } else {
         this.k = var1;
         this.l = var1.b();
         this.a(6);
      }
   }

   private char A() throws IOException {
      char var1 = 0;
      if(!this.k.b(1L)) {
         throw this.a((String)"Unterminated escape sequence");
      } else {
         byte var2 = this.l.i();
         switch(var2) {
         case 10:
         case 34:
         case 39:
         case 47:
         case 92:
            var1 = (char)var2;
            break;
         case 98:
            var1 = 8;
            break;
         case 102:
            var1 = 12;
            break;
         case 110:
            var1 = 10;
            break;
         case 114:
            var1 = 13;
            break;
         case 116:
            var1 = 9;
            break;
         case 117:
            if(!this.k.b(4L)) {
               throw new EOFException("Unterminated escape sequence at path " + this.r());
            }

            for(int var5 = 0; var5 < 4; ++var5) {
               byte var4 = this.l.c((long)var5);
               char var3 = (char)(var1 << 4);
               if(var4 >= 48 && var4 <= 57) {
                  var1 = (char)(var3 + (var4 - 48));
               } else if(var4 >= 97 && var4 <= 102) {
                  var1 = (char)(var3 + var4 - 97 + 10);
               } else {
                  if(var4 < 65 || var4 > 70) {
                     throw this.a((String)("\\u" + this.l.e(4L)));
                  }

                  var1 = (char)(var3 + var4 - 65 + 10);
               }
            }

            this.l.i(4L);
            break;
         default:
            if(!this.e) {
               throw this.a((String)("Invalid escape sequence: \\" + (char)var2));
            }

            var1 = (char)var2;
         }

         return var1;
      }
   }

   private int a(String var1, k.a var2) {
      int var4 = var2.a.length;
      int var3 = 0;

      while(true) {
         if(var3 >= var4) {
            var3 = -1;
            break;
         }

         if(var1.equals(var2.a[var3])) {
            this.m = 0;
            this.c[this.a - 1] = var1;
            break;
         }

         ++var3;
      }

      return var3;
   }

   private String a(c.f var1) throws IOException {
      StringBuilder var4 = null;

      while(true) {
         long var2 = this.k.b(var1);
         if(var2 == -1L) {
            throw this.a((String)"Unterminated string");
         }

         if(this.l.c(var2) != 92) {
            String var6;
            if(var4 == null) {
               var6 = this.l.e(var2);
               this.l.i();
            } else {
               var4.append(this.l.e(var2));
               this.l.i();
               var6 = var4.toString();
            }

            return var6;
         }

         StringBuilder var5 = var4;
         if(var4 == null) {
            var5 = new StringBuilder();
         }

         var5.append(this.l.e(var2));
         this.l.i();
         var5.append(this.A());
         var4 = var5;
      }
   }

   private int b(String var1, k.a var2) {
      int var4 = var2.a.length;
      int var3 = 0;

      while(true) {
         if(var3 >= var4) {
            var3 = -1;
            break;
         }

         if(var1.equals(var2.a[var3])) {
            this.m = 0;
            int[] var5 = this.d;
            var4 = this.a - 1;
            ++var5[var4];
            break;
         }

         ++var3;
      }

      return var3;
   }

   private void b(c.f var1) throws IOException {
      while(true) {
         long var2 = this.k.b(var1);
         if(var2 == -1L) {
            throw this.a((String)"Unterminated string");
         }

         if(this.l.c(var2) != 92) {
            this.l.i(var2 + 1L);
            return;
         }

         this.l.i(var2 + 1L);
         this.A();
      }
   }

   private boolean b(int var1) throws IOException {
      boolean var2;
      switch(var1) {
      case 35:
      case 47:
      case 59:
      case 61:
      case 92:
         this.y();
      case 9:
      case 10:
      case 12:
      case 13:
      case 32:
      case 44:
      case 58:
      case 91:
      case 93:
      case 123:
      case 125:
         var2 = false;
         break;
      default:
         var2 = true;
      }

      return var2;
   }

   private boolean b(String var1) throws IOException {
      boolean var4 = false;

      boolean var3;
      label22:
      while(true) {
         var3 = var4;
         if(!this.k.b((long)var1.length())) {
            break;
         }

         for(int var2 = 0; var2 < var1.length(); ++var2) {
            if(this.l.c((long)var2) != var1.charAt(var2)) {
               this.l.i();
               continue label22;
            }
         }

         var3 = true;
         break;
      }

      return var3;
   }

   private int c(boolean var1) throws IOException {
      int var2 = 0;

      byte var6;
      while(true) {
         if(!this.k.b((long)(var2 + 1))) {
            if(var1) {
               throw new EOFException("End of input");
            }

            var6 = -1;
            break;
         }

         c.c var5 = this.l;
         int var3 = var2 + 1;
         byte var4 = var5.c((long)var2);
         if(var4 != 10 && var4 != 32 && var4 != 13) {
            if(var4 == 9) {
               var2 = var3;
            } else {
               this.l.i((long)(var3 - 1));
               if(var4 == 47) {
                  if(!this.k.b(2L)) {
                     var6 = var4;
                     break;
                  }

                  this.y();
                  switch(this.l.c(1L)) {
                  case 42:
                     this.l.i();
                     this.l.i();
                     if(!this.b("*/")) {
                        throw this.a((String)"Unterminated comment");
                     }

                     this.l.i();
                     this.l.i();
                     var2 = 0;
                     break;
                  case 47:
                     this.l.i();
                     this.l.i();
                     this.z();
                     var2 = 0;
                     break;
                  default:
                     var6 = var4;
                     return var6;
                  }
               } else {
                  var6 = var4;
                  if(var4 != 35) {
                     break;
                  }

                  this.y();
                  this.z();
                  var2 = 0;
               }
            }
         } else {
            var2 = var3;
         }
      }

      return var6;
   }

   private int t() throws IOException {
      int var1 = 4;
      int var2 = this.b[this.a - 1];
      if(var2 == 1) {
         this.b[this.a - 1] = 2;
      } else {
         int var3;
         if(var2 == 2) {
            var3 = this.c(true);
            this.l.i();
            switch(var3) {
            case 44:
               break;
            case 59:
               this.y();
               break;
            case 93:
               this.m = 4;
               return var1;
            default:
               throw this.a((String)"Unterminated array");
            }
         } else {
            if(var2 == 3 || var2 == 5) {
               this.b[this.a - 1] = 4;
               if(var2 == 5) {
                  var1 = this.c(true);
                  this.l.i();
                  switch(var1) {
                  case 44:
                     break;
                  case 59:
                     this.y();
                     break;
                  case 125:
                     this.m = 2;
                     var1 = 2;
                     return var1;
                  default:
                     throw this.a((String)"Unterminated object");
                  }
               }

               var1 = this.c(true);
               switch(var1) {
               case 34:
                  this.l.i();
                  var1 = 13;
                  this.m = 13;
                  return var1;
               case 39:
                  this.l.i();
                  this.y();
                  var1 = 12;
                  this.m = 12;
                  return var1;
               case 125:
                  if(var2 == 5) {
                     throw this.a((String)"Expected name");
                  }

                  this.l.i();
                  this.m = 2;
                  var1 = 2;
                  return var1;
               default:
                  this.y();
                  if(!this.b((char)var1)) {
                     throw this.a((String)"Expected name");
                  }

                  var1 = 14;
                  this.m = 14;
                  return var1;
               }
            }

            if(var2 == 4) {
               this.b[this.a - 1] = 5;
               var3 = this.c(true);
               this.l.i();
               switch(var3) {
               case 58:
                  break;
               case 59:
               case 60:
               default:
                  throw this.a((String)"Expected ':'");
               case 61:
                  this.y();
                  if(this.k.b(1L) && this.l.c(0L) == 62) {
                     this.l.i();
                  }
               }
            } else if(var2 == 6) {
               this.b[this.a - 1] = 7;
            } else if(var2 == 7) {
               if(this.c(false) == -1) {
                  var1 = 18;
                  this.m = 18;
                  return var1;
               }

               this.y();
            } else if(var2 == 8) {
               throw new IllegalStateException("JsonReader is closed");
            }
         }
      }

      switch(this.c(true)) {
      case 34:
         this.l.i();
         var1 = 9;
         this.m = 9;
         break;
      case 39:
         this.y();
         this.l.i();
         var1 = 8;
         this.m = 8;
         break;
      case 91:
         this.l.i();
         var1 = 3;
         this.m = 3;
         break;
      case 93:
         if(var2 == 1) {
            this.l.i();
            this.m = 4;
            break;
         }
      case 44:
      case 59:
         if(var2 != 1 && var2 != 2) {
            throw this.a((String)"Unexpected value");
         }

         this.y();
         this.m = 7;
         var1 = 7;
         break;
      case 123:
         this.l.i();
         this.m = 1;
         var1 = 1;
         break;
      default:
         var1 = this.u();
         if(var1 == 0) {
            var2 = this.v();
            var1 = var2;
            if(var2 == 0) {
               if(!this.b(this.l.c(0L))) {
                  throw this.a((String)"Expected value");
               }

               this.y();
               var1 = 10;
               this.m = 10;
            }
         }
      }

      return var1;
   }

   private int u() throws IOException {
      byte var1 = this.l.c(0L);
      String var5;
      String var6;
      byte var7;
      if(var1 != 116 && var1 != 84) {
         if(var1 != 102 && var1 != 70) {
            if(var1 != 110 && var1 != 78) {
               var7 = 0;
               return var7;
            }

            var6 = "null";
            var5 = "NULL";
            var7 = 7;
         } else {
            var6 = "false";
            var5 = "FALSE";
            var7 = 6;
         }
      } else {
         var6 = "true";
         var5 = "TRUE";
         var7 = 5;
      }

      int var3 = var6.length();
      int var2 = 1;

      while(true) {
         if(var2 >= var3) {
            if(this.k.b((long)(var3 + 1)) && this.b(this.l.c((long)var3))) {
               var7 = 0;
            } else {
               this.l.i((long)var3);
               this.m = var7;
            }
            break;
         }

         if(!this.k.b((long)(var2 + 1))) {
            var7 = 0;
            break;
         }

         byte var4 = this.l.c((long)var2);
         if(var4 != var6.charAt(var2) && var4 != var5.charAt(var2)) {
            var7 = 0;
            break;
         }

         ++var2;
      }

      return var7;
   }

   private int v() throws IOException {
      long var8 = 0L;
      boolean var2 = false;
      boolean var3 = true;
      byte var5 = 0;
      int var4 = 0;

      byte var12;
      while(true) {
         boolean var6;
         boolean var7;
         long var10;
         label131: {
            if(this.k.b((long)(var4 + 1))) {
               byte var1 = this.l.c((long)var4);
               switch(var1) {
               case 43:
                  if(var5 != 5) {
                     var12 = 0;
                     return var12;
                  }

                  var12 = 6;
                  var7 = var3;
                  var6 = var2;
                  var10 = var8;
                  break label131;
               case 45:
                  if(var5 == 0) {
                     var6 = true;
                     var12 = 1;
                     var10 = var8;
                     var7 = var3;
                  } else {
                     if(var5 != 5) {
                        var12 = 0;
                        return var12;
                     }

                     var12 = 6;
                     var7 = var3;
                     var6 = var2;
                     var10 = var8;
                  }
                  break label131;
               case 46:
                  if(var5 != 2) {
                     var12 = 0;
                     return var12;
                  }

                  var12 = 3;
                  var7 = var3;
                  var6 = var2;
                  var10 = var8;
                  break label131;
               case 69:
               case 101:
                  if(var5 != 2 && var5 != 4) {
                     var12 = 0;
                     return var12;
                  }

                  var12 = 5;
                  var7 = var3;
                  var6 = var2;
                  var10 = var8;
                  break label131;
               default:
                  if(var1 >= 48 && var1 <= 57) {
                     if(var5 != 1 && var5 != 0) {
                        if(var5 == 2) {
                           if(var8 == 0L) {
                              var12 = 0;
                              return var12;
                           }

                           var10 = 10L * var8 - (long)(var1 - 48);
                           boolean var13;
                           if(var8 <= -922337203685477580L && (var8 != -922337203685477580L || var10 >= var8)) {
                              var13 = false;
                           } else {
                              var13 = true;
                           }

                           var7 = var3 & var13;
                           var12 = var5;
                           var6 = var2;
                        } else if(var5 == 3) {
                           var12 = 4;
                           var7 = var3;
                           var6 = var2;
                           var10 = var8;
                        } else {
                           if(var5 != 5) {
                              var12 = var5;
                              var7 = var3;
                              var6 = var2;
                              var10 = var8;
                              if(var5 != 6) {
                                 break label131;
                              }
                           }

                           var12 = 7;
                           var7 = var3;
                           var6 = var2;
                           var10 = var8;
                        }
                     } else {
                        var10 = (long)(-(var1 - 48));
                        var12 = 2;
                        var7 = var3;
                        var6 = var2;
                     }
                     break label131;
                  }

                  if(this.b(var1)) {
                     var12 = 0;
                     return var12;
                  }
               }
            }

            if(var5 == 2 && var3 && (var8 != Long.MIN_VALUE || var2) && (var8 != 0L || !var2)) {
               if(!var2) {
                  var8 = -var8;
               }

               this.n = var8;
               this.l.i((long)var4);
               var12 = 16;
               this.m = 16;
               break;
            }

            if(var5 != 2 && var5 != 4 && var5 != 7) {
               var12 = 0;
               break;
            }

            this.o = var4;
            var12 = 17;
            this.m = 17;
            break;
         }

         ++var4;
         var5 = var12;
         var3 = var7;
         var2 = var6;
         var8 = var10;
      }

      return var12;
   }

   private String w() throws IOException {
      long var1 = this.k.b(i);
      String var3;
      if(var1 != -1L) {
         var3 = this.l.e(var1);
      } else {
         var3 = this.l.r();
      }

      return var3;
   }

   private void x() throws IOException {
      long var1 = this.k.b(i);
      c.c var3 = this.l;
      if(var1 == -1L) {
         var1 = this.l.a();
      }

      var3.i(var1);
   }

   private void y() throws IOException {
      if(!this.e) {
         throw this.a((String)"Use JsonReader.setLenient(true) to accept malformed JSON");
      }
   }

   private void z() throws IOException {
      long var1 = this.k.b(j);
      c.c var3 = this.l;
      if(var1 != -1L) {
         ++var1;
      } else {
         var1 = this.l.a();
      }

      var3.i(var1);
   }

   public int a(k.a var1) throws IOException {
      int var3 = this.m;
      int var2 = var3;
      if(var3 == 0) {
         var2 = this.t();
      }

      if(var2 >= 12 && var2 <= 15) {
         if(var2 == 15) {
            var2 = this.a(this.p, var1);
         } else {
            var2 = this.k.a(var1.b);
            if(var2 != -1) {
               this.m = 0;
               this.c[this.a - 1] = var1.a[var2];
            } else {
               String var5 = this.c[this.a - 1];
               String var4 = this.i();
               var3 = this.a(var4, var1);
               var2 = var3;
               if(var3 == -1) {
                  this.m = 15;
                  this.p = var4;
                  this.c[this.a - 1] = var5;
                  var2 = var3;
               }
            }
         }
      } else {
         var2 = -1;
      }

      return var2;
   }

   public int b(k.a var1) throws IOException {
      int var3 = this.m;
      int var2 = var3;
      if(var3 == 0) {
         var2 = this.t();
      }

      if(var2 >= 8 && var2 <= 11) {
         if(var2 == 11) {
            var2 = this.b(this.p, var1);
         } else {
            var2 = this.k.a(var1.b);
            int[] var5;
            if(var2 != -1) {
               this.m = 0;
               var5 = this.d;
               var3 = this.a - 1;
               ++var5[var3];
            } else {
               String var4 = this.j();
               var3 = this.b(var4, var1);
               var2 = var3;
               if(var3 == -1) {
                  this.m = 11;
                  this.p = var4;
                  var5 = this.d;
                  var2 = this.a - 1;
                  --var5[var2];
                  var2 = var3;
               }
            }
         }
      } else {
         var2 = -1;
      }

      return var2;
   }

   public void c() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      if(var1 == 3) {
         this.a(1);
         this.d[this.a - 1] = 0;
         this.m = 0;
      } else {
         throw new JsonDataException("Expected BEGIN_ARRAY but was " + this.h() + " at path " + this.r());
      }
   }

   public void close() throws IOException {
      this.m = 0;
      this.b[0] = 8;
      this.a = 1;
      this.l.v();
      this.k.close();
   }

   public void d() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      if(var1 == 4) {
         --this.a;
         int[] var3 = this.d;
         var1 = this.a - 1;
         ++var3[var1];
         this.m = 0;
      } else {
         throw new JsonDataException("Expected END_ARRAY but was " + this.h() + " at path " + this.r());
      }
   }

   public void e() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      if(var1 == 1) {
         this.a(3);
         this.m = 0;
      } else {
         throw new JsonDataException("Expected BEGIN_OBJECT but was " + this.h() + " at path " + this.r());
      }
   }

   public void f() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      if(var1 == 2) {
         --this.a;
         this.c[this.a] = null;
         int[] var3 = this.d;
         var1 = this.a - 1;
         ++var3[var1];
         this.m = 0;
      } else {
         throw new JsonDataException("Expected END_OBJECT but was " + this.h() + " at path " + this.r());
      }
   }

   public boolean g() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      boolean var3;
      if(var1 != 2 && var1 != 4) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public k.b h() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      k.b var3;
      switch(var1) {
      case 1:
         var3 = k.b.c;
         break;
      case 2:
         var3 = k.b.d;
         break;
      case 3:
         var3 = k.b.a;
         break;
      case 4:
         var3 = k.b.b;
         break;
      case 5:
      case 6:
         var3 = k.b.h;
         break;
      case 7:
         var3 = k.b.i;
         break;
      case 8:
      case 9:
      case 10:
      case 11:
         var3 = k.b.f;
         break;
      case 12:
      case 13:
      case 14:
      case 15:
         var3 = k.b.e;
         break;
      case 16:
      case 17:
         var3 = k.b.g;
         break;
      case 18:
         var3 = k.b.j;
         break;
      default:
         throw new AssertionError();
      }

      return var3;
   }

   public String i() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      String var3;
      if(var1 == 14) {
         var3 = this.w();
      } else if(var1 == 13) {
         var3 = this.a(h);
      } else if(var1 == 12) {
         var3 = this.a(g);
      } else {
         if(var1 != 15) {
            throw new JsonDataException("Expected a name but was " + this.h() + " at path " + this.r());
         }

         var3 = this.p;
      }

      this.m = 0;
      this.c[this.a - 1] = var3;
      return var3;
   }

   public String j() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      String var3;
      if(var1 == 10) {
         var3 = this.w();
      } else if(var1 == 9) {
         var3 = this.a(h);
      } else if(var1 == 8) {
         var3 = this.a(g);
      } else if(var1 == 11) {
         var3 = this.p;
         this.p = null;
      } else if(var1 == 16) {
         var3 = Long.toString(this.n);
      } else {
         if(var1 != 17) {
            throw new JsonDataException("Expected a string but was " + this.h() + " at path " + this.r());
         }

         var3 = this.l.e((long)this.o);
      }

      this.m = 0;
      int[] var4 = this.d;
      var1 = this.a - 1;
      ++var4[var1];
      return var3;
   }

   public boolean k() throws IOException {
      boolean var3 = false;
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      int[] var4;
      if(var1 == 5) {
         this.m = 0;
         var4 = this.d;
         var1 = this.a - 1;
         ++var4[var1];
         var3 = true;
      } else {
         if(var1 != 6) {
            throw new JsonDataException("Expected a boolean but was " + this.h() + " at path " + this.r());
         }

         this.m = 0;
         var4 = this.d;
         var1 = this.a - 1;
         ++var4[var1];
      }

      return var3;
   }

   @Nullable
   public Object l() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      if(var1 == 7) {
         this.m = 0;
         int[] var3 = this.d;
         var1 = this.a - 1;
         ++var3[var1];
         return null;
      } else {
         throw new JsonDataException("Expected null but was " + this.h() + " at path " + this.r());
      }
   }

   public double m() throws IOException {
      int var4 = this.m;
      int var3 = var4;
      if(var4 == 0) {
         var3 = this.t();
      }

      double var1;
      int[] var5;
      if(var3 == 16) {
         this.m = 0;
         var5 = this.d;
         var3 = this.a - 1;
         ++var5[var3];
         var1 = (double)this.n;
      } else {
         if(var3 == 17) {
            this.p = this.l.e((long)this.o);
         } else if(var3 == 9) {
            this.p = this.a(h);
         } else if(var3 == 8) {
            this.p = this.a(g);
         } else if(var3 == 10) {
            this.p = this.w();
         } else if(var3 != 11) {
            throw new JsonDataException("Expected a double but was " + this.h() + " at path " + this.r());
         }

         this.m = 11;

         try {
            var1 = Double.parseDouble(this.p);
         } catch (NumberFormatException var6) {
            throw new JsonDataException("Expected a double but was " + this.p + " at path " + this.r());
         }

         if(!this.e && (Double.isNaN(var1) || Double.isInfinite(var1))) {
            throw new JsonEncodingException("JSON forbids NaN and infinities: " + var1 + " at path " + this.r());
         }

         this.p = null;
         this.m = 0;
         var5 = this.d;
         var3 = this.a - 1;
         ++var5[var3];
      }

      return var1;
   }

   public long n() throws IOException {
      int var2 = this.m;
      int var1 = var2;
      if(var2 == 0) {
         var1 = this.t();
      }

      long var3;
      int[] var9;
      if(var1 == 16) {
         this.m = 0;
         var9 = this.d;
         var1 = this.a - 1;
         ++var9[var1];
         var3 = this.n;
      } else {
         if(var1 == 17) {
            this.p = this.l.e((long)this.o);
         } else if(var1 != 9 && var1 != 8) {
            if(var1 != 11) {
               throw new JsonDataException("Expected a long but was " + this.h() + " at path " + this.r());
            }
         } else {
            label66: {
               String var5;
               if(var1 == 9) {
                  var5 = this.a(h);
               } else {
                  var5 = this.a(g);
               }

               this.p = var5;

               try {
                  var3 = Long.parseLong(this.p);
                  this.m = 0;
                  var9 = this.d;
                  var1 = this.a - 1;
               } catch (NumberFormatException var8) {
                  break label66;
               }

               ++var9[var1];
               return var3;
            }
         }

         this.m = 11;

         label40: {
            try {
               BigDecimal var10 = new BigDecimal(this.p);
               var3 = var10.longValueExact();
               break label40;
            } catch (NumberFormatException var6) {
               ;
            } catch (ArithmeticException var7) {
               ;
            }

            throw new JsonDataException("Expected a long but was " + this.p + " at path " + this.r());
         }

         this.p = null;
         this.m = 0;
         var9 = this.d;
         var1 = this.a - 1;
         ++var9[var1];
      }

      return var3;
   }

   public int o() throws IOException {
      int var4 = this.m;
      int var3 = var4;
      if(var4 == 0) {
         var3 = this.t();
      }

      int[] var5;
      if(var3 == 16) {
         var3 = (int)this.n;
         if(this.n != (long)var3) {
            throw new JsonDataException("Expected an int but was " + this.n + " at path " + this.r());
         }

         this.m = 0;
         var5 = this.d;
         var4 = this.a - 1;
         ++var5[var4];
      } else {
         if(var3 == 17) {
            this.p = this.l.e((long)this.o);
         } else if(var3 != 9 && var3 != 8) {
            if(var3 != 11) {
               throw new JsonDataException("Expected an int but was " + this.h() + " at path " + this.r());
            }
         } else {
            label65: {
               String var8;
               if(var3 == 9) {
                  var8 = this.a(h);
               } else {
                  var8 = this.a(g);
               }

               this.p = var8;

               try {
                  var3 = Integer.parseInt(this.p);
                  this.m = 0;
                  var5 = this.d;
                  var4 = this.a - 1;
               } catch (NumberFormatException var7) {
                  break label65;
               }

               ++var5[var4];
               return var3;
            }
         }

         this.m = 11;

         double var1;
         try {
            var1 = Double.parseDouble(this.p);
         } catch (NumberFormatException var6) {
            throw new JsonDataException("Expected an int but was " + this.p + " at path " + this.r());
         }

         var3 = (int)var1;
         if((double)var3 != var1) {
            throw new JsonDataException("Expected an int but was " + this.p + " at path " + this.r());
         }

         this.p = null;
         this.m = 0;
         var5 = this.d;
         var4 = this.a - 1;
         ++var5[var4];
      }

      return var3;
   }

   public void p() throws IOException {
      if(this.f) {
         throw new JsonDataException("Cannot skip unexpected " + this.h() + " at " + this.r());
      } else {
         int var2 = 0;

         int var1;
         do {
            var1 = this.m;
            int var3 = var1;
            if(var1 == 0) {
               var3 = this.t();
            }

            if(var3 == 3) {
               this.a(1);
               var1 = var2 + 1;
            } else if(var3 == 1) {
               this.a(3);
               var1 = var2 + 1;
            } else if(var3 == 4) {
               --this.a;
               var1 = var2 - 1;
            } else if(var3 == 2) {
               --this.a;
               var1 = var2 - 1;
            } else if(var3 != 14 && var3 != 10) {
               if(var3 != 9 && var3 != 13) {
                  if(var3 != 8 && var3 != 12) {
                     var1 = var2;
                     if(var3 == 17) {
                        this.l.i((long)this.o);
                        var1 = var2;
                     }
                  } else {
                     this.b(g);
                     var1 = var2;
                  }
               } else {
                  this.b(h);
                  var1 = var2;
               }
            } else {
               this.x();
               var1 = var2;
            }

            this.m = 0;
            var2 = var1;
         } while(var1 != 0);

         int[] var4 = this.d;
         var1 = this.a - 1;
         ++var4[var1];
         this.c[this.a - 1] = "null";
      }
   }

   void s() throws IOException {
      if(this.g()) {
         this.p = this.i();
         this.m = 11;
      }

   }

   public String toString() {
      return "JsonReader(" + this.k + ")";
   }
}
