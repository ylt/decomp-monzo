package com.squareup.moshi;

final class l {
   static String a(int var0, int[] var1, String[] var2, int[] var3) {
      StringBuilder var5 = (new StringBuilder()).append('$');

      for(int var4 = 0; var4 < var0; ++var4) {
         switch(var1[var4]) {
         case 1:
         case 2:
            var5.append('[').append(var3[var4]).append(']');
            break;
         case 3:
         case 4:
         case 5:
            var5.append('.');
            if(var2[var4] != null) {
               var5.append(var2[var4]);
            }
         }
      }

      return var5.toString();
   }
}
