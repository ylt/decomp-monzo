package com.squareup.moshi;

import javax.annotation.Nullable;

public final class JsonDataException extends RuntimeException {
   public JsonDataException() {
   }

   public JsonDataException(@Nullable String var1) {
      super(var1);
   }

   public JsonDataException(@Nullable String var1, @Nullable Throwable var2) {
      super(var1, var2);
   }
}
