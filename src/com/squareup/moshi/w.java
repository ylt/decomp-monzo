package com.squareup.moshi;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

final class w {
   public static final i.a a = new i.a() {
      public i a(Type var1, Set var2, v var3) {
         Object var4 = null;
         i var5;
         if(!var2.isEmpty()) {
            var5 = (i)var4;
         } else if(var1 == Boolean.TYPE) {
            var5 = w.b;
         } else if(var1 == Byte.TYPE) {
            var5 = w.c;
         } else if(var1 == Character.TYPE) {
            var5 = w.d;
         } else if(var1 == Double.TYPE) {
            var5 = w.e;
         } else if(var1 == Float.TYPE) {
            var5 = w.f;
         } else if(var1 == Integer.TYPE) {
            var5 = w.g;
         } else if(var1 == Long.TYPE) {
            var5 = w.h;
         } else if(var1 == Short.TYPE) {
            var5 = w.i;
         } else if(var1 == Boolean.class) {
            var5 = w.b.d();
         } else if(var1 == Byte.class) {
            var5 = w.c.d();
         } else if(var1 == Character.class) {
            var5 = w.d.d();
         } else if(var1 == Double.class) {
            var5 = w.e.d();
         } else if(var1 == Float.class) {
            var5 = w.f.d();
         } else if(var1 == Integer.class) {
            var5 = w.g.d();
         } else if(var1 == Long.class) {
            var5 = w.h.d();
         } else if(var1 == Short.class) {
            var5 = w.i.d();
         } else if(var1 == String.class) {
            var5 = w.j.d();
         } else if(var1 == Object.class) {
            var5 = (new w.b(var3)).d();
         } else {
            Class var6 = y.e(var1);
            var5 = (i)var4;
            if(var6.isEnum()) {
               var5 = (new w.a(var6)).d();
            }
         }

         return var5;
      }
   };
   static final i b = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Boolean var2) throws IOException {
         var1.a(var2.booleanValue());
      }

      public Boolean b(k var1) throws IOException {
         return Boolean.valueOf(var1.k());
      }

      public String toString() {
         return "JsonAdapter(Boolean)";
      }
   };
   static final i c = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Byte var2) throws IOException {
         var1.a((long)(var2.intValue() & 255));
      }

      public Byte b(k var1) throws IOException {
         return Byte.valueOf((byte)w.a(var1, "a byte", -128, 255));
      }

      public String toString() {
         return "JsonAdapter(Byte)";
      }
   };
   static final i d = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Character var2) throws IOException {
         var1.b(var2.toString());
      }

      public Character b(k var1) throws IOException {
         String var2 = var1.j();
         if(var2.length() > 1) {
            throw new JsonDataException(String.format("Expected %s but was %s at path %s", new Object[]{"a char", '"' + var2 + '"', var1.r()}));
         } else {
            return Character.valueOf(var2.charAt(0));
         }
      }

      public String toString() {
         return "JsonAdapter(Character)";
      }
   };
   static final i e = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Double var2) throws IOException {
         var1.a(var2.doubleValue());
      }

      public Double b(k var1) throws IOException {
         return Double.valueOf(var1.m());
      }

      public String toString() {
         return "JsonAdapter(Double)";
      }
   };
   static final i f = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Float var2) throws IOException {
         if(var2 == null) {
            throw new NullPointerException();
         } else {
            var1.a((Number)var2);
         }
      }

      public Float b(k var1) throws IOException {
         float var2 = (float)var1.m();
         if(!var1.a() && Float.isInfinite(var2)) {
            throw new JsonDataException("JSON forbids NaN and infinities: " + var2 + " at path " + var1.r());
         } else {
            return Float.valueOf(var2);
         }
      }

      public String toString() {
         return "JsonAdapter(Float)";
      }
   };
   static final i g = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Integer var2) throws IOException {
         var1.a((long)var2.intValue());
      }

      public Integer b(k var1) throws IOException {
         return Integer.valueOf(var1.o());
      }

      public String toString() {
         return "JsonAdapter(Integer)";
      }
   };
   static final i h = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Long var2) throws IOException {
         var1.a(var2.longValue());
      }

      public Long b(k var1) throws IOException {
         return Long.valueOf(var1.n());
      }

      public String toString() {
         return "JsonAdapter(Long)";
      }
   };
   static final i i = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Short var2) throws IOException {
         var1.a((long)var2.intValue());
      }

      public Short b(k var1) throws IOException {
         return Short.valueOf((short)w.a(var1, "a short", -32768, 32767));
      }

      public String toString() {
         return "JsonAdapter(Short)";
      }
   };
   static final i j = new i() {
      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, String var2) throws IOException {
         var1.b(var2);
      }

      public String b(k var1) throws IOException {
         return var1.j();
      }

      public String toString() {
         return "JsonAdapter(String)";
      }
   };

   static int a(k var0, String var1, int var2, int var3) throws IOException {
      int var4 = var0.o();
      if(var4 >= var2 && var4 <= var3) {
         return var4;
      } else {
         throw new JsonDataException(String.format("Expected %s but was %s at path %s", new Object[]{var1, Integer.valueOf(var4), var0.r()}));
      }
   }

   static final class a extends i {
      private final Class a;
      private final String[] b;
      private final Enum[] c;
      private final k.a d;

      a(Class param1) {
         // $FF: Couldn't be decompiled
      }

      // $FF: synthetic method
      public Object a(k var1) throws IOException {
         return this.b(var1);
      }

      public void a(p var1, Enum var2) throws IOException {
         var1.b(this.b[var2.ordinal()]);
      }

      public Enum b(k var1) throws IOException {
         int var2 = var1.b(this.d);
         if(var2 != -1) {
            return this.c[var2];
         } else {
            String var3 = var1.j();
            throw new JsonDataException("Expected one of " + Arrays.asList(this.b) + " but was " + var3 + " at path " + var1.r());
         }
      }

      public String toString() {
         return "JsonAdapter(" + this.a.getName() + ")";
      }
   }

   static final class b extends i {
      private final v a;

      b(v var1) {
         this.a = var1;
      }

      private Class a(Class var1) {
         Class var2;
         if(Map.class.isAssignableFrom(var1)) {
            var2 = Map.class;
         } else {
            var2 = var1;
            if(Collection.class.isAssignableFrom(var1)) {
               var2 = Collection.class;
            }
         }

         return var2;
      }

      public Object a(k var1) throws IOException {
         return var1.q();
      }

      public void a(p var1, Object var2) throws IOException {
         Class var3 = var2.getClass();
         if(var3 == Object.class) {
            var1.c();
            var1.d();
         } else {
            this.a.a(this.a(var3), z.a).a(var1, var2);
         }

      }

      public String toString() {
         return "JsonAdapter(Object)";
      }
   }
}
