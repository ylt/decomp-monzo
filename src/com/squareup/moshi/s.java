package com.squareup.moshi;

import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 1},
   d1 = {"\u0000\f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\"\u0016\u0010\u0000\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0003"},
   d2 = {"KOTLIN_METADATA", "Ljava/lang/Class;", "", "moshi-kotlin"},
   k = 2,
   mv = {1, 1, 5}
)
public final class s {
   private static final Class a;

   static {
      Class var0 = Class.forName("kotlin.Metadata");
      if(var0 == null) {
         throw new TypeCastException("null cannot be cast to non-null type java.lang.Class<out kotlin.Annotation>");
      } else {
         a = (Class)var0;
      }
   }

   // $FF: synthetic method
   public static final Class a() {
      return a;
   }
}
