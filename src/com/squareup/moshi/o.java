package com.squareup.moshi;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nullable;

final class o extends k {
   private static final Object g = new Object();
   private final Object[] h = new Object[32];

   o(Object var1) {
      this.b[this.a] = 7;
      Object[] var3 = this.h;
      int var2 = this.a;
      this.a = var2 + 1;
      var3[var2] = var1;
   }

   @Nullable
   private Object a(Class var1, k.b var2) throws IOException {
      Object var4 = null;
      Object var3;
      if(this.a != 0) {
         var3 = this.h[this.a - 1];
      } else {
         var3 = null;
      }

      label24: {
         Object var5;
         if(var1.isInstance(var3)) {
            var5 = var1.cast(var3);
         } else {
            if(var3 != null) {
               break label24;
            }

            var5 = var4;
            if(var2 != k.b.i) {
               break label24;
            }
         }

         return var5;
      }

      if(var3 == g) {
         throw new IllegalStateException("JsonReader is closed");
      } else {
         throw this.a(var3, var2);
      }
   }

   private String a(Entry var1) {
      Object var2 = var1.getKey();
      if(var2 instanceof String) {
         return (String)var2;
      } else {
         throw this.a(var2, k.b.e);
      }
   }

   private void a(Object var1) {
      if(this.a == this.h.length) {
         throw new JsonDataException("Nesting too deep at " + this.r());
      } else {
         Object[] var3 = this.h;
         int var2 = this.a;
         this.a = var2 + 1;
         var3[var2] = var1;
      }
   }

   private void t() {
      --this.a;
      this.h[this.a] = null;
      this.b[this.a] = 0;
      if(this.a > 0) {
         int[] var2 = this.d;
         int var1 = this.a - 1;
         ++var2[var1];
         Object var3 = this.h[this.a - 1];
         if(var3 instanceof Iterator && ((Iterator)var3).hasNext()) {
            this.a(((Iterator)var3).next());
         }
      }

   }

   public int a(k.a var1) throws IOException {
      Entry var5 = (Entry)this.a(Entry.class, k.b.e);
      String var4 = this.a(var5);
      int var2 = 0;
      int var3 = var1.a.length;

      while(true) {
         if(var2 >= var3) {
            var2 = -1;
            break;
         }

         if(var1.a[var2].equals(var4)) {
            this.h[this.a - 1] = var5.getValue();
            this.c[this.a - 2] = var4;
            break;
         }

         ++var2;
      }

      return var2;
   }

   public int b(k.a var1) throws IOException {
      String var4 = (String)this.a(String.class, k.b.f);
      int var2 = 0;
      int var3 = var1.a.length;

      while(true) {
         if(var2 >= var3) {
            var2 = -1;
            break;
         }

         if(var1.a[var2].equals(var4)) {
            this.t();
            break;
         }

         ++var2;
      }

      return var2;
   }

   public void c() throws IOException {
      ListIterator var1 = ((List)this.a(List.class, k.b.a)).listIterator();
      this.h[this.a - 1] = var1;
      this.b[this.a - 1] = 1;
      this.d[this.a - 1] = 0;
      if(var1.hasNext()) {
         this.a(var1.next());
      }

   }

   public void close() throws IOException {
      Arrays.fill(this.h, 0, this.a, (Object)null);
      this.h[0] = g;
      this.b[0] = 8;
      this.a = 1;
   }

   public void d() throws IOException {
      ListIterator var1 = (ListIterator)this.a(ListIterator.class, k.b.b);
      if(var1.hasNext()) {
         throw this.a(var1, k.b.b);
      } else {
         this.t();
      }
   }

   public void e() throws IOException {
      Iterator var1 = ((Map)this.a(Map.class, k.b.c)).entrySet().iterator();
      this.h[this.a - 1] = var1;
      this.b[this.a - 1] = 3;
      if(var1.hasNext()) {
         this.a(var1.next());
      }

   }

   public void f() throws IOException {
      Iterator var1 = (Iterator)this.a(Iterator.class, k.b.d);
      if(!(var1 instanceof ListIterator) && !var1.hasNext()) {
         this.c[this.a - 1] = null;
         this.t();
      } else {
         throw this.a(var1, k.b.d);
      }
   }

   public boolean g() throws IOException {
      boolean var1 = true;
      if(this.a != 0) {
         Object var2 = this.h[this.a - 1];
         if(var2 instanceof Iterator && !((Iterator)var2).hasNext()) {
            var1 = false;
         } else {
            var1 = true;
         }
      }

      return var1;
   }

   public k.b h() throws IOException {
      k.b var1;
      if(this.a == 0) {
         var1 = k.b.j;
      } else {
         Object var2 = this.h[this.a - 1];
         if(var2 instanceof ListIterator) {
            var1 = k.b.b;
         } else if(var2 instanceof Iterator) {
            var1 = k.b.d;
         } else if(var2 instanceof List) {
            var1 = k.b.a;
         } else if(var2 instanceof Map) {
            var1 = k.b.c;
         } else if(var2 instanceof Entry) {
            var1 = k.b.e;
         } else if(var2 instanceof String) {
            var1 = k.b.f;
         } else if(var2 instanceof Boolean) {
            var1 = k.b.h;
         } else if(var2 instanceof Number) {
            var1 = k.b.g;
         } else {
            if(var2 != null) {
               if(var2 == g) {
                  throw new IllegalStateException("JsonReader is closed");
               }

               throw this.a(var2, "a JSON value");
            }

            var1 = k.b.i;
         }
      }

      return var1;
   }

   public String i() throws IOException {
      Entry var2 = (Entry)this.a(Entry.class, k.b.e);
      String var1 = this.a(var2);
      this.h[this.a - 1] = var2.getValue();
      this.c[this.a - 2] = var1;
      return var1;
   }

   public String j() throws IOException {
      String var1 = (String)this.a(String.class, k.b.f);
      this.t();
      return var1;
   }

   public boolean k() throws IOException {
      Boolean var1 = (Boolean)this.a(Boolean.class, k.b.h);
      this.t();
      return var1.booleanValue();
   }

   @Nullable
   public Object l() throws IOException {
      this.a(Void.class, k.b.i);
      this.t();
      return null;
   }

   public double m() throws IOException {
      Object var3 = this.a(Object.class, k.b.g);
      double var1;
      if(var3 instanceof Number) {
         var1 = ((Number)var3).doubleValue();
      } else {
         if(!(var3 instanceof String)) {
            throw this.a(var3, k.b.g);
         }

         try {
            var1 = Double.parseDouble((String)var3);
         } catch (NumberFormatException var5) {
            throw this.a(var3, k.b.g);
         }
      }

      if(this.e || !Double.isNaN(var1) && !Double.isInfinite(var1)) {
         this.t();
         return var1;
      } else {
         throw new JsonEncodingException("JSON forbids NaN and infinities: " + var1 + " at path " + this.r());
      }
   }

   public long n() throws IOException {
      Object var3 = this.a(Object.class, k.b.g);
      long var1;
      if(var3 instanceof Number) {
         var1 = ((Number)var3).longValue();
      } else {
         if(!(var3 instanceof String)) {
            throw this.a(var3, k.b.g);
         }

         try {
            var1 = Long.parseLong((String)var3);
         } catch (NumberFormatException var6) {
            try {
               BigDecimal var4 = new BigDecimal((String)var3);
               var1 = var4.longValueExact();
            } catch (NumberFormatException var5) {
               throw this.a(var3, k.b.g);
            }
         }
      }

      this.t();
      return var1;
   }

   public int o() throws IOException {
      Object var2 = this.a(Object.class, k.b.g);
      int var1;
      if(var2 instanceof Number) {
         var1 = ((Number)var2).intValue();
      } else {
         if(!(var2 instanceof String)) {
            throw this.a(var2, k.b.g);
         }

         try {
            var1 = Integer.parseInt((String)var2);
         } catch (NumberFormatException var5) {
            try {
               BigDecimal var3 = new BigDecimal((String)var2);
               var1 = var3.intValueExact();
            } catch (NumberFormatException var4) {
               throw this.a(var2, k.b.g);
            }
         }
      }

      this.t();
      return var1;
   }

   public void p() throws IOException {
      if(this.f) {
         throw new JsonDataException("Cannot skip unexpected " + this.h() + " at " + this.r());
      } else {
         if(this.a > 1) {
            this.c[this.a - 2] = "null";
         }

         Object var1;
         if(this.a != 0) {
            var1 = this.h[this.a - 1];
         } else {
            var1 = null;
         }

         if(var1 instanceof Entry) {
            Entry var2 = (Entry)this.h[this.a - 1];
            this.h[this.a - 1] = var2.getValue();
         } else if(this.a > 0) {
            this.t();
         }

      }
   }

   void s() throws IOException {
      if(this.g()) {
         this.a((Object)this.i());
      }

   }
}
