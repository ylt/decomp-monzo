package com.squareup.moshi;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 1},
   d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"},
   d2 = {"Lcom/squareup/moshi/ABSENT_VALUE;", "", "()V", "moshi-kotlin"},
   k = 1,
   mv = {1, 1, 5}
)
final class a {
   public static final a a;

   static {
      new a();
   }

   private a() {
      a = (a)this;
   }
}
