package com.squareup.moshi;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Set;
import javax.annotation.Nullable;

final class c extends i {
   public static final i.a a = new i.a() {
      @Nullable
      public i a(Type var1, Set var2, v var3) {
         Object var4 = null;
         Type var5 = y.h(var1);
         i var6;
         if(var5 == null) {
            var6 = (i)var4;
         } else {
            var6 = (i)var4;
            if(var2.isEmpty()) {
               var6 = (new c(y.e(var5), var3.a(var5))).d();
            }
         }

         return var6;
      }
   };
   private final Class b;
   private final i c;

   c(Class var1, i var2) {
      this.b = var1;
      this.c = var2;
   }

   public Object a(k var1) throws IOException {
      ArrayList var3 = new ArrayList();
      var1.c();

      while(var1.g()) {
         var3.add(this.c.a(var1));
      }

      var1.d();
      Object var4 = Array.newInstance(this.b, var3.size());

      for(int var2 = 0; var2 < var3.size(); ++var2) {
         Array.set(var4, var2, var3.get(var2));
      }

      return var4;
   }

   public void a(p var1, Object var2) throws IOException {
      var1.a();
      int var3 = 0;

      for(int var4 = Array.getLength(var2); var3 < var4; ++var3) {
         this.c.a(var1, Array.get(var2, var3));
      }

      var1.b();
   }

   public String toString() {
      return this.c + ".array()";
   }
}
