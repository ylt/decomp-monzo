package com.squareup.moshi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.ab;
import kotlin.d.b.ac;

@Metadata(
   bv = {1, 0, 1},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0010\u001b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J0\u0010\u0003\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000e\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0016¨\u0006\f"},
   d2 = {"Lcom/squareup/moshi/KotlinJsonAdapterFactory;", "Lcom/squareup/moshi/JsonAdapter$Factory;", "()V", "create", "Lcom/squareup/moshi/JsonAdapter;", "type", "Ljava/lang/reflect/Type;", "annotations", "", "", "moshi", "Lcom/squareup/moshi/Moshi;", "moshi-kotlin"},
   k = 1,
   mv = {1, 1, 5}
)
public final class r implements i.a {
   public i a(Type var1, Set var2, v var3) {
      kotlin.d.b.l.b(var2, "annotations");
      kotlin.d.b.l.b(var3, "moshi");
      i var13;
      if(!var2.isEmpty()) {
         var13 = null;
      } else {
         Class var14 = y.e(var1);
         if(var14.isEnum()) {
            var13 = null;
         } else if(!var14.isAnnotationPresent(s.a())) {
            var13 = null;
         } else if(e.a(var14)) {
            var13 = null;
         } else {
            kotlin.reflect.f var6 = kotlin.reflect.full.a.a(kotlin.d.a.a(var14));
            if(var6 != null) {
               Iterable var15 = (Iterable)var6.h();
               Map var8 = (Map)(new LinkedHashMap(kotlin.f.d.c(ab.a(kotlin.a.m.a(var15, 10)), 16)));
               Iterator var16 = var15.iterator();

               while(var16.hasNext()) {
                  Object var5 = var16.next();
                  var8.put(((kotlin.reflect.k)var5).b(), var5);
               }

               kotlin.reflect.jvm.a.a((kotlin.reflect.b)var6, true);
               LinkedHashMap var7 = new LinkedHashMap();
               Iterator var10 = kotlin.reflect.full.a.b(kotlin.d.a.a(var14)).iterator();

               while(true) {
                  kotlin.reflect.n var9;
                  kotlin.reflect.k var11;
                  List var18;
                  Iterator var25;
                  String var29;
                  do {
                     int var4;
                     do {
                        if(!var10.hasNext()) {
                           ArrayList var27 = new ArrayList();
                           Iterator var19 = var6.h().iterator();

                           while(var19.hasNext()) {
                              kotlin.reflect.k var31 = (kotlin.reflect.k)var19.next();
                              var8 = (Map)var7;
                              String var34 = var31.b();
                              q.a var36 = (q.a)ac.c(var8).remove(var34);
                              if(var36 == null && !var31.e()) {
                                 throw (Throwable)(new IllegalArgumentException("No property for required constructor " + var31));
                              }

                              ((Collection)var27).add(var36);
                           }

                           Collection var33 = (Collection)var27;
                           Collection var21 = var7.values();
                           kotlin.d.b.l.a(var21, "bindingsByName.values");
                           kotlin.a.m.a(var33, (Iterable)var21);
                           Iterable var35 = (Iterable)var27;
                           var21 = (Collection)(new ArrayList(kotlin.a.m.a(var35, 10)));

                           for(var25 = var35.iterator(); var25.hasNext(); var21.add(var29)) {
                              q.a var37 = (q.a)var25.next();
                              if(var37 != null) {
                                 var29 = var37.a();
                                 if(var29 != null) {
                                    continue;
                                 }
                              }

                              var29 = "\u0000";
                           }

                           var33 = (Collection)((Collection)((List)var21));
                           Object[] var38 = var33.toArray(new String[var33.size()]);
                           if(var38 == null) {
                              throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                           }

                           String[] var39 = (String[])((Object[])var38);
                           k.a var40 = k.a.a((String[])Arrays.copyOf(var39, var39.length));
                           var18 = (List)var27;
                           kotlin.d.b.l.a(var40, "options");
                           var13 = (new q(var6, var18, var40)).d();
                           return var13;
                        }

                        var9 = (kotlin.reflect.n)var10.next();
                        Field var17 = kotlin.reflect.jvm.c.a((kotlin.reflect.l)var9);
                        if(var17 != null) {
                           var4 = var17.getModifiers();
                        } else {
                           var4 = 0;
                        }
                     } while(Modifier.isTransient(var4));

                     var11 = (kotlin.reflect.k)var8.get(var9.b());
                  } while(!(var9 instanceof kotlin.reflect.i) && var11 == null);

                  kotlin.reflect.jvm.a.a((kotlin.reflect.b)var9, true);
                  var18 = var9.j();
                  var25 = ((Iterable)((kotlin.reflect.a)var9).j()).iterator();

                  Object var20;
                  do {
                     if(!var25.hasNext()) {
                        var20 = null;
                        break;
                     }

                     var20 = var25.next();
                  } while(!((Annotation)var20 instanceof h));

                  h var28;
                  label122: {
                     var28 = (h)((h)var20);
                     List var22 = var18;
                     if(var11 != null) {
                        var18 = kotlin.a.m.b((Collection)var18, (Iterable)var11.j());
                        var22 = var18;
                        if(var28 == null) {
                           var25 = ((Iterable)((kotlin.reflect.a)var11).j()).iterator();

                           do {
                              if(!var25.hasNext()) {
                                 var20 = null;
                                 break;
                              }

                              var20 = var25.next();
                           } while(!((Annotation)var20 instanceof h));

                           var28 = (h)((h)var20);
                           break label122;
                        }
                     }

                     var18 = var22;
                  }

                  label127: {
                     if(var28 != null) {
                        var29 = var28.a();
                        if(var29 != null) {
                           break label127;
                        }
                     }

                     var29 = var9.b();
                  }

                  Type var30 = kotlin.reflect.jvm.c.a(var9.i());
                  Collection var23 = (Collection)var18;
                  if(var23 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.util.Collection<T>");
                  }

                  var23 = (Collection)var23;
                  Object[] var24 = var23.toArray(new Annotation[var23.size()]);
                  if(var24 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                  }

                  i var26 = var3.a(var30, z.a((Annotation[])((Object[])var24)));
                  Map var32 = (Map)var7;
                  String var12 = var9.b();
                  kotlin.d.b.l.a(var26, "adapter");
                  if(var9 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type kotlin.reflect.KProperty1<kotlin.Any, kotlin.Any?>");
                  }

                  var32.put(var12, new q.a(var29, var26, (kotlin.reflect.n)var9, var11));
               }
            } else {
               var13 = null;
            }
         }
      }

      return var13;
   }
}
