package com.squareup.moshi;

import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

abstract class d {
   public static d a(final Class var0) {
      d var14;
      d var15;
      try {
         final Constructor var19 = var0.getDeclaredConstructor(new Class[0]);
         var19.setAccessible(true);
         var15 = new d() {
            public Object a() throws IllegalAccessException, InvocationTargetException, InstantiationException {
               return var19.newInstance((Object[])null);
            }

            public String toString() {
               return var0.getName();
            }
         };
      } catch (NoSuchMethodException var13) {
         label62: {
            try {
               Class var16 = Class.forName("sun.misc.Unsafe");
               Field var17 = var16.getDeclaredField("theUnsafe");
               var17.setAccessible(true);
               final Object var18 = var17.get((Object)null);
               final Method var4 = var16.getMethod("allocateInstance", new Class[]{Class.class});
               var15 = new d() {
                  public Object a() throws InvocationTargetException, IllegalAccessException {
                     return var4.invoke(var18, new Object[]{var0});
                  }

                  public String toString() {
                     return var0.getName();
                  }
               };
               break label62;
            } catch (IllegalAccessException var9) {
               throw new AssertionError();
            } catch (ClassNotFoundException var10) {
               ;
            } catch (NoSuchMethodException var11) {
               ;
            } catch (NoSuchFieldException var12) {
               ;
            }

            final Method var2;
            try {
               var2 = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", new Class[]{Class.class});
               var2.setAccessible(true);
               final int var1 = ((Integer)var2.invoke((Object)null, new Object[]{Object.class})).intValue();
               final Method var3 = ObjectStreamClass.class.getDeclaredMethod("newInstance", new Class[]{Class.class, Integer.TYPE});
               var3.setAccessible(true);
               var15 = new d() {
                  public Object a() throws InvocationTargetException, IllegalAccessException {
                     return var3.invoke((Object)null, new Object[]{var0, Integer.valueOf(var1)});
                  }

                  public String toString() {
                     return var0.getName();
                  }
               };
            } catch (IllegalAccessException var6) {
               throw new AssertionError();
            } catch (InvocationTargetException var7) {
               throw new RuntimeException(var7);
            } catch (NoSuchMethodException var8) {
               try {
                  var2 = ObjectInputStream.class.getDeclaredMethod("newInstance", new Class[]{Class.class, Class.class});
                  var2.setAccessible(true);
                  var15 = new d() {
                     public Object a() throws InvocationTargetException, IllegalAccessException {
                        return var2.invoke((Object)null, new Object[]{var0, Object.class});
                     }

                     public String toString() {
                        return var0.getName();
                     }
                  };
               } catch (Exception var5) {
                  throw new IllegalArgumentException("cannot construct instances of " + var0.getName());
               }

               var14 = var15;
               return var14;
            }

            var14 = var15;
            return var14;
         }

         var14 = var15;
         return var14;
      }

      var14 = var15;
      return var14;
   }

   abstract Object a() throws InvocationTargetException, IllegalAccessException, InstantiationException;
}
