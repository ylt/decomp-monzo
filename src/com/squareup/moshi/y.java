package com.squareup.moshi;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import javax.annotation.Nullable;

public final class y {
   static final Type[] a = new Type[0];

   static int a(@Nullable Object var0) {
      int var1;
      if(var0 != null) {
         var1 = var0.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   private static int a(Object[] var0, Object var1) {
      for(int var2 = 0; var2 < var0.length; ++var2) {
         if(var1.equals(var0[var2])) {
            return var2;
         }
      }

      throw new NoSuchElementException();
   }

   @Nullable
   private static Class a(TypeVariable var0) {
      GenericDeclaration var1 = var0.getGenericDeclaration();
      Class var2;
      if(var1 instanceof Class) {
         var2 = (Class)var1;
      } else {
         var2 = null;
      }

      return var2;
   }

   public static GenericArrayType a(Type var0) {
      return new y.a(var0);
   }

   public static Type a(Type var0, Class var1) {
      Type var2 = b(var0, var1, Collection.class);
      var0 = var2;
      if(var2 instanceof WildcardType) {
         var0 = ((WildcardType)var2).getUpperBounds()[0];
      }

      Object var3;
      if(var0 instanceof ParameterizedType) {
         var3 = ((ParameterizedType)var0).getActualTypeArguments()[0];
      } else {
         var3 = Object.class;
      }

      return (Type)var3;
   }

   static Type a(Type var0, Class var1, Class var2) {
      if(var2 != var1) {
         if(var2.isInterface()) {
            Class[] var5 = var1.getInterfaces();
            int var3 = 0;

            for(int var4 = var5.length; var3 < var4; ++var3) {
               if(var5[var3] == var2) {
                  var0 = var1.getGenericInterfaces()[var3];
                  return (Type)var0;
               }

               if(var2.isAssignableFrom(var5[var3])) {
                  var0 = a(var1.getGenericInterfaces()[var3], var5[var3], var2);
                  return (Type)var0;
               }
            }
         }

         if(!var1.isInterface()) {
            while(var1 != Object.class) {
               Class var6 = var1.getSuperclass();
               if(var6 == var2) {
                  var0 = var1.getGenericSuperclass();
                  return (Type)var0;
               }

               if(var2.isAssignableFrom(var6)) {
                  var0 = a(var1.getGenericSuperclass(), var6, var2);
                  return (Type)var0;
               }

               var1 = var6;
            }
         }

         var0 = var2;
      }

      return (Type)var0;
   }

   static Type a(Type var0, Class var1, Type var2) {
      Object var7 = var2;

      Object var11;
      while(true) {
         if(!(var7 instanceof TypeVariable)) {
            if(var7 instanceof Class && ((Class)var7).isArray()) {
               var11 = (Class)var7;
               Class var18 = ((Class)var11).getComponentType();
               var0 = a(var0, var1, (Type)var18);
               if(var18 != var0) {
                  var11 = a(var0);
               }
               break;
            }

            if(var7 instanceof GenericArrayType) {
               var11 = (GenericArrayType)var7;
               Type var14 = ((GenericArrayType)var11).getGenericComponentType();
               var0 = a(var0, var1, var14);
               if(var14 != var0) {
                  var11 = a(var0);
               }
            } else if(var7 instanceof ParameterizedType) {
               ParameterizedType var8 = (ParameterizedType)var7;
               var2 = var8.getOwnerType();
               Type var9 = a(var0, var1, var2);
               boolean var3;
               if(var9 != var2) {
                  var3 = true;
               } else {
                  var3 = false;
               }

               Type[] var16 = var8.getActualTypeArguments();
               int var6 = var16.length;

               boolean var4;
               for(int var5 = 0; var5 < var6; var3 = var4) {
                  Type var10 = a(var0, var1, var16[var5]);
                  Type[] var12 = var16;
                  var4 = var3;
                  if(var10 != var16[var5]) {
                     var12 = var16;
                     var4 = var3;
                     if(!var3) {
                        var12 = (Type[])var16.clone();
                        var4 = true;
                     }

                     var12[var5] = var10;
                  }

                  ++var5;
                  var16 = var12;
               }

               var11 = var8;
               if(var3) {
                  var11 = new y.b(var9, var8.getRawType(), var16);
               }
            } else {
               var11 = var7;
               if(var7 instanceof WildcardType) {
                  WildcardType var17 = (WildcardType)var7;
                  Type[] var19 = var17.getLowerBounds();
                  Type[] var15 = var17.getUpperBounds();
                  if(var19.length == 1) {
                     var0 = a(var0, var1, var19[0]);
                     var11 = var17;
                     if(var0 != var19[0]) {
                        var11 = c(var0);
                     }
                  } else {
                     var11 = var17;
                     if(var15.length == 1) {
                        var0 = a(var0, var1, var15[0]);
                        var11 = var17;
                        if(var0 != var15[0]) {
                           var11 = b(var0);
                        }
                     }
                  }
               }
            }
            break;
         }

         TypeVariable var13 = (TypeVariable)var7;
         var11 = a(var0, var1, var13);
         if(var11 == var13) {
            break;
         }

         var7 = var11;
      }

      return (Type)var11;
   }

   static Type a(Type var0, Class var1, TypeVariable var2) {
      Class var4 = a(var2);
      Object var5;
      if(var4 == null) {
         var5 = var2;
      } else {
         Type var6 = a(var0, var1, var4);
         var5 = var2;
         if(var6 instanceof ParameterizedType) {
            int var3 = a((Object[])var4.getTypeParameters(), (Object)var2);
            var5 = ((ParameterizedType)var6).getActualTypeArguments()[var3];
         }
      }

      return (Type)var5;
   }

   static boolean a(@Nullable Object var0, @Nullable Object var1) {
      boolean var2;
      if(var0 != var1 && (var0 == null || !var0.equals(var1))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public static boolean a(@Nullable Type var0, @Nullable Type var1) {
      boolean var5 = true;
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(var0 == var1) {
         var2 = true;
      } else if(var0 instanceof Class) {
         if(var1 instanceof GenericArrayType) {
            var2 = a((Type)((Class)var0).getComponentType(), (Type)((GenericArrayType)var1).getGenericComponentType());
         } else {
            var2 = var0.equals(var1);
         }
      } else if(var0 instanceof ParameterizedType) {
         var2 = var4;
         if(var1 instanceof ParameterizedType) {
            ParameterizedType var6 = (ParameterizedType)var0;
            ParameterizedType var7 = (ParameterizedType)var1;
            Type[] var8;
            if(var6 instanceof y.b) {
               var8 = ((y.b)var6).a;
            } else {
               var8 = var6.getActualTypeArguments();
            }

            Type[] var10;
            if(var7 instanceof y.b) {
               var10 = ((y.b)var7).a;
            } else {
               var10 = var7.getActualTypeArguments();
            }

            if(a((Object)var6.getOwnerType(), (Object)var7.getOwnerType()) && var6.getRawType().equals(var7.getRawType()) && Arrays.equals(var8, var10)) {
               var2 = true;
            } else {
               var2 = false;
            }
         }
      } else if(var0 instanceof GenericArrayType) {
         if(var1 instanceof Class) {
            var2 = a((Type)((Class)var1).getComponentType(), (Type)((GenericArrayType)var0).getGenericComponentType());
         } else {
            var2 = var4;
            if(var1 instanceof GenericArrayType) {
               GenericArrayType var9 = (GenericArrayType)var0;
               GenericArrayType var13 = (GenericArrayType)var1;
               var2 = a(var9.getGenericComponentType(), var13.getGenericComponentType());
            }
         }
      } else if(var0 instanceof WildcardType) {
         var2 = var4;
         if(var1 instanceof WildcardType) {
            WildcardType var11 = (WildcardType)var0;
            WildcardType var14 = (WildcardType)var1;
            if(Arrays.equals(var11.getUpperBounds(), var14.getUpperBounds()) && Arrays.equals(var11.getLowerBounds(), var14.getLowerBounds())) {
               var2 = var3;
            } else {
               var2 = false;
            }
         }
      } else {
         var2 = var4;
         if(var0 instanceof TypeVariable) {
            var2 = var4;
            if(var1 instanceof TypeVariable) {
               TypeVariable var12 = (TypeVariable)var0;
               TypeVariable var15 = (TypeVariable)var1;
               if(var12.getGenericDeclaration() == var15.getGenericDeclaration() && var12.getName().equals(var15.getName())) {
                  var2 = var5;
               } else {
                  var2 = false;
               }
            }
         }
      }

      return var2;
   }

   static Type b(Type var0, Class var1, Class var2) {
      if(!var2.isAssignableFrom(var1)) {
         throw new IllegalArgumentException();
      } else {
         return a(var0, var1, a(var0, var1, var2));
      }
   }

   public static WildcardType b(Type var0) {
      Type[] var1 = a;
      return new y.c(new Type[]{var0}, var1);
   }

   static Type[] b(Type var0, Class var1) {
      Type[] var2;
      if(var0 == Properties.class) {
         var2 = new Type[]{String.class, String.class};
      } else {
         var0 = b(var0, var1, Map.class);
         if(var0 instanceof ParameterizedType) {
            var2 = ((ParameterizedType)var0).getActualTypeArguments();
         } else {
            var2 = new Type[]{Object.class, Object.class};
         }
      }

      return var2;
   }

   public static WildcardType c(Type var0) {
      return new y.c(new Type[]{Object.class}, new Type[]{var0});
   }

   static Type d(Type var0) {
      Object var1;
      if(var0 instanceof Class) {
         Class var2 = (Class)var0;
         var1 = var2;
         if(var2.isArray()) {
            var1 = new y.a(d(var2.getComponentType()));
         }
      } else if(var0 instanceof ParameterizedType) {
         var1 = var0;
         if(!(var0 instanceof y.b)) {
            ParameterizedType var3 = (ParameterizedType)var0;
            var1 = new y.b(var3.getOwnerType(), var3.getRawType(), var3.getActualTypeArguments());
         }
      } else if(var0 instanceof GenericArrayType) {
         var1 = var0;
         if(!(var0 instanceof y.a)) {
            var1 = new y.a(((GenericArrayType)var0).getGenericComponentType());
         }
      } else {
         var1 = var0;
         if(var0 instanceof WildcardType) {
            var1 = var0;
            if(!(var0 instanceof y.c)) {
               WildcardType var4 = (WildcardType)var0;
               var1 = new y.c(var4.getUpperBounds(), var4.getLowerBounds());
            }
         }
      }

      return (Type)var1;
   }

   public static Class e(Type var0) {
      Class var2;
      if(var0 instanceof Class) {
         var2 = (Class)var0;
      } else if(var0 instanceof ParameterizedType) {
         var2 = (Class)((ParameterizedType)var0).getRawType();
      } else if(var0 instanceof GenericArrayType) {
         var2 = Array.newInstance(e(((GenericArrayType)var0).getGenericComponentType()), 0).getClass();
      } else if(var0 instanceof TypeVariable) {
         var2 = Object.class;
      } else {
         if(!(var0 instanceof WildcardType)) {
            String var1;
            if(var0 == null) {
               var1 = "null";
            } else {
               var1 = var0.getClass().getName();
            }

            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + var0 + "> is of type " + var1);
         }

         var2 = e(((WildcardType)var0).getUpperBounds()[0]);
      }

      return var2;
   }

   static String f(Type var0) {
      String var1;
      if(var0 instanceof Class) {
         var1 = ((Class)var0).getName();
      } else {
         var1 = var0.toString();
      }

      return var1;
   }

   static Type g(Type var0) {
      Class var1 = e(var0);
      return a(var0, var1, var1.getGenericSuperclass());
   }

   static Type h(Type var0) {
      Object var1;
      if(var0 instanceof GenericArrayType) {
         var1 = ((GenericArrayType)var0).getGenericComponentType();
      } else if(var0 instanceof Class) {
         var1 = ((Class)var0).getComponentType();
      } else {
         var1 = null;
      }

      return (Type)var1;
   }

   static boolean i(Type var0) {
      boolean var1;
      if(var0 != Boolean.class && var0 != Byte.class && var0 != Character.class && var0 != Double.class && var0 != Float.class && var0 != Integer.class && var0 != Long.class && var0 != Short.class && var0 != String.class && var0 != Object.class) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   static void j(Type var0) {
      if(var0 instanceof Class && ((Class)var0).isPrimitive()) {
         throw new IllegalArgumentException();
      }
   }

   private static final class a implements GenericArrayType {
      private final Type a;

      a(Type var1) {
         this.a = y.d(var1);
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof GenericArrayType && y.a((Type)this, (Type)((GenericArrayType)var1))) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type getGenericComponentType() {
         return this.a;
      }

      public int hashCode() {
         return this.a.hashCode();
      }

      public String toString() {
         return y.f(this.a) + "[]";
      }
   }

   private static final class b implements ParameterizedType {
      final Type[] a;
      @Nullable
      private final Type b;
      private final Type c;

      b(@Nullable Type var1, Type var2, Type... var3) {
         boolean var5 = true;
         byte var6 = 0;
         super();
         if(var2 instanceof Class) {
            boolean var4;
            if(var1 == null) {
               var4 = true;
            } else {
               var4 = false;
            }

            if(((Class)var2).getEnclosingClass() != null) {
               var5 = false;
            }

            if(var4 != var5) {
               throw new IllegalArgumentException("unexpected owner type for " + var2 + ": " + var1);
            }
         }

         if(var1 == null) {
            var1 = null;
         } else {
            var1 = y.d(var1);
         }

         this.b = var1;
         this.c = y.d(var2);
         this.a = (Type[])var3.clone();

         for(int var7 = var6; var7 < this.a.length; ++var7) {
            if(this.a[var7] == null) {
               throw new NullPointerException();
            }

            y.j(this.a[var7]);
            this.a[var7] = y.d(this.a[var7]);
         }

      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof ParameterizedType && y.a((Type)this, (Type)((ParameterizedType)var1))) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type[] getActualTypeArguments() {
         return (Type[])this.a.clone();
      }

      @Nullable
      public Type getOwnerType() {
         return this.b;
      }

      public Type getRawType() {
         return this.c;
      }

      public int hashCode() {
         return Arrays.hashCode(this.a) ^ this.c.hashCode() ^ y.a((Object)this.b);
      }

      public String toString() {
         StringBuilder var2 = new StringBuilder((this.a.length + 1) * 30);
         var2.append(y.f(this.c));
         String var3;
         if(this.a.length == 0) {
            var3 = var2.toString();
         } else {
            var2.append("<").append(y.f(this.a[0]));

            for(int var1 = 1; var1 < this.a.length; ++var1) {
               var2.append(", ").append(y.f(this.a[var1]));
            }

            var3 = var2.append(">").toString();
         }

         return var3;
      }
   }

   private static final class c implements WildcardType {
      private final Type a;
      @Nullable
      private final Type b;

      c(Type[] var1, Type[] var2) {
         if(var2.length > 1) {
            throw new IllegalArgumentException();
         } else if(var1.length != 1) {
            throw new IllegalArgumentException();
         } else {
            if(var2.length == 1) {
               if(var2[0] == null) {
                  throw new NullPointerException();
               }

               y.j(var2[0]);
               if(var1[0] != Object.class) {
                  throw new IllegalArgumentException();
               }

               this.b = y.d(var2[0]);
               this.a = Object.class;
            } else {
               if(var1[0] == null) {
                  throw new NullPointerException();
               }

               y.j(var1[0]);
               this.b = null;
               this.a = y.d(var1[0]);
            }

         }
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof WildcardType && y.a((Type)this, (Type)((WildcardType)var1))) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type[] getLowerBounds() {
         Type[] var1;
         if(this.b != null) {
            var1 = new Type[]{this.b};
         } else {
            var1 = y.a;
         }

         return var1;
      }

      public Type[] getUpperBounds() {
         return new Type[]{this.a};
      }

      public int hashCode() {
         int var1;
         if(this.b != null) {
            var1 = this.b.hashCode() + 31;
         } else {
            var1 = 1;
         }

         return var1 ^ this.a.hashCode() + 31;
      }

      public String toString() {
         String var1;
         if(this.b != null) {
            var1 = "? super " + y.f(this.b);
         } else if(this.a == Object.class) {
            var1 = "?";
         } else {
            var1 = "? extends " + y.f(this.a);
         }

         return var1;
      }
   }
}
