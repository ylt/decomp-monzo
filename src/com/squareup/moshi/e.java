package com.squareup.moshi;

import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.Nullable;

final class e extends i {
   public static final i.a a = new i.a() {
      private void a(v var1, Type var2, Map var3) {
         Class var8 = y.e(var2);
         boolean var6 = e.a(var8);
         Field[] var9 = var8.getDeclaredFields();
         int var5 = var9.length;

         for(int var4 = 0; var4 < var5; ++var4) {
            Field var10 = var9[var4];
            if(this.a(var6, var10.getModifiers())) {
               i var11 = var1.a(y.a(var2, var8, var10.getGenericType()), z.a((AnnotatedElement)var10));
               var10.setAccessible(true);
               h var7 = (h)var10.getAnnotation(h.class);
               String var13;
               if(var7 != null) {
                  var13 = var7.a();
               } else {
                  var13 = var10.getName();
               }

               e.a var12 = new e.a(var13, var10, var11);
               e.a var14 = (e.a)var3.put(var13, var12);
               if(var14 != null) {
                  throw new IllegalArgumentException("Conflicting fields:\n    " + var14.b + "\n" + "    " + var12.b);
               }
            }
         }

      }

      private boolean a(boolean var1, int var2) {
         boolean var4 = false;
         boolean var3 = var4;
         if(!Modifier.isStatic(var2)) {
            if(Modifier.isTransient(var2)) {
               var3 = var4;
            } else {
               if(!Modifier.isPublic(var2) && !Modifier.isProtected(var2)) {
                  var3 = var4;
                  if(var1) {
                     return var3;
                  }
               }

               var3 = true;
            }
         }

         return var3;
      }

      @Nullable
      public i a(Type var1, Set var2, v var3) {
         Object var5 = null;
         Class var6 = y.e(var1);
         i var4 = (i)var5;
         if(!var6.isInterface()) {
            if(var6.isEnum()) {
               var4 = (i)var5;
            } else {
               if(e.a(var6) && !y.i(var6)) {
                  throw new IllegalArgumentException("Platform " + var1 + " annotated " + var2 + " requires explicit JsonAdapter to be registered");
               }

               var4 = (i)var5;
               if(var2.isEmpty()) {
                  if(var6.getEnclosingClass() != null && !Modifier.isStatic(var6.getModifiers())) {
                     if(var6.getSimpleName().isEmpty()) {
                        throw new IllegalArgumentException("Cannot serialize anonymous class " + var6.getName());
                     }

                     throw new IllegalArgumentException("Cannot serialize non-static nested class " + var6.getName());
                  }

                  if(Modifier.isAbstract(var6.getModifiers())) {
                     throw new IllegalArgumentException("Cannot serialize abstract class " + var6.getName());
                  }

                  d var7 = d.a(var6);

                  TreeMap var8;
                  for(var8 = new TreeMap(); var1 != Object.class; var1 = y.g(var1)) {
                     this.a((v)var3, (Type)var1, (Map)var8);
                  }

                  var4 = (new e(var7, var8)).d();
               }
            }
         }

         return var4;
      }
   };
   private final d b;
   private final e.a[] c;
   private final k.a d;

   e(d var1, Map var2) {
      this.b = var1;
      this.c = (e.a[])var2.values().toArray(new e.a[var2.size()]);
      this.d = k.a.a((String[])var2.keySet().toArray(new String[var2.size()]));
   }

   static boolean a(Class var0) {
      String var2 = var0.getName();
      boolean var1;
      if(!var2.startsWith("android.") && !var2.startsWith("java.") && !var2.startsWith("javax.") && !var2.startsWith("kotlin.") && !var2.startsWith("scala.")) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public Object a(k param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void a(p param1, Object param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public String toString() {
      return "JsonAdapter(" + this.b + ")";
   }

   static class a {
      final String a;
      final Field b;
      final i c;

      a(String var1, Field var2, i var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      void a(k var1, Object var2) throws IOException, IllegalAccessException {
         Object var3 = this.c.a(var1);
         this.b.set(var2, var3);
      }

      void a(p var1, Object var2) throws IllegalAccessException, IOException {
         var2 = this.b.get(var2);
         this.c.a(var1, var2);
      }
   }
}
