package com.squareup.moshi;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

public abstract class k implements Closeable {
   int a = 0;
   final int[] b = new int[32];
   final String[] c = new String[32];
   final int[] d = new int[32];
   boolean e;
   boolean f;

   public static k a(c.e var0) {
      return new m(var0);
   }

   public abstract int a(k.a var1) throws IOException;

   final JsonDataException a(@Nullable Object var1, Object var2) {
      JsonDataException var3;
      if(var1 == null) {
         var3 = new JsonDataException("Expected " + var2 + " but was null at path " + this.r());
      } else {
         var3 = new JsonDataException("Expected " + var2 + " but was " + var1 + ", a " + var1.getClass().getName() + ", at path " + this.r());
      }

      return var3;
   }

   final JsonEncodingException a(String var1) throws JsonEncodingException {
      throw new JsonEncodingException(var1 + " at path " + this.r());
   }

   final void a(int var1) {
      if(this.a == this.b.length) {
         throw new JsonDataException("Nesting too deep at " + this.r());
      } else {
         int[] var3 = this.b;
         int var2 = this.a;
         this.a = var2 + 1;
         var3[var2] = var1;
      }
   }

   public final void a(boolean var1) {
      this.e = var1;
   }

   public final boolean a() {
      return this.e;
   }

   public abstract int b(k.a var1) throws IOException;

   public final void b(boolean var1) {
      this.f = var1;
   }

   public final boolean b() {
      return this.f;
   }

   public abstract void c() throws IOException;

   public abstract void d() throws IOException;

   public abstract void e() throws IOException;

   public abstract void f() throws IOException;

   public abstract boolean g() throws IOException;

   public abstract k.b h() throws IOException;

   public abstract String i() throws IOException;

   public abstract String j() throws IOException;

   public abstract boolean k() throws IOException;

   @Nullable
   public abstract Object l() throws IOException;

   public abstract double m() throws IOException;

   public abstract long n() throws IOException;

   public abstract int o() throws IOException;

   public abstract void p() throws IOException;

   @Nullable
   public final Object q() throws IOException {
      Object var1;
      switch(null.a[this.h().ordinal()]) {
      case 1:
         var1 = new ArrayList();
         this.c();

         while(this.g()) {
            ((List)var1).add(this.q());
         }

         this.d();
         break;
      case 2:
         var1 = new t();
         this.e();

         while(this.g()) {
            String var3 = this.i();
            Object var4 = this.q();
            Object var2 = ((Map)var1).put(var3, var4);
            if(var2 != null) {
               throw new JsonDataException("Map key '" + var3 + "' has multiple values at path " + this.r() + ": " + var2 + " and " + var4);
            }
         }

         this.f();
         break;
      case 3:
         var1 = this.j();
         break;
      case 4:
         var1 = Double.valueOf(this.m());
         break;
      case 5:
         var1 = Boolean.valueOf(this.k());
         break;
      case 6:
         var1 = this.l();
         break;
      default:
         throw new IllegalStateException("Expected a value but was " + this.h() + " at path " + this.r());
      }

      return var1;
   }

   public final String r() {
      return l.a(this.a, this.b, this.c, this.d);
   }

   abstract void s() throws IOException;

   public static final class a {
      final String[] a;
      final c.m b;

      private a(String[] var1, c.m var2) {
         this.a = var1;
         this.b = var2;
      }

      public static k.a a(String... param0) {
         // $FF: Couldn't be decompiled
      }
   }

   public static enum b {
      a,
      b,
      c,
      d,
      e,
      f,
      g,
      h,
      i,
      j;
   }
}
