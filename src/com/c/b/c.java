package com.c.b;

final class c extends b {
   private final Object a;

   c(Object var1) {
      this.a = var1;
   }

   public b a(com.c.a.a var1) {
      var1.a(this.a);
      return this;
   }

   public Object a() {
      return this.a;
   }

   public Object a(Object var1) {
      return this.a;
   }

   public boolean b() {
      return true;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else if(!(var1 instanceof c)) {
         var2 = false;
      } else {
         c var3 = (c)var1;
         var2 = this.a.equals(var3.a);
      }

      return var2;
   }

   public int hashCode() {
      return this.a.hashCode();
   }

   public String toString() {
      return "Some {value=" + this.a + '}';
   }
}
