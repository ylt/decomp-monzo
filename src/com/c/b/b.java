package com.c.b;

import java.io.Serializable;

public abstract class b implements Serializable {
   private static final a a = new a();

   public static b b(Object var0) {
      if(var0 == null) {
         var0 = c();
      } else {
         var0 = new c(var0);
      }

      return (b)var0;
   }

   public static b c() {
      return a;
   }

   public abstract b a(com.c.a.a var1);

   public abstract Object a();

   public abstract Object a(Object var1);

   public abstract boolean b();

   public final boolean d() {
      boolean var1;
      if(!this.b()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
