package com.intercom.input.gallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class GalleryInputFullScreenActivity extends android.support.v7.app.e implements j {
   private Class a;
   private Bundle b;

   public static Intent a(Context var0, Class var1, Bundle var2) {
      return (new Intent(var0, GalleryInputFullScreenActivity.class)).putExtra("fragment_class", var1).putExtra("fragment_args", var2);
   }

   public void onBackPressed() {
      super.onBackPressed();
      View var1 = this.getCurrentFocus();
      if(var1 != null) {
         ((InputMethodManager)this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
      }

      this.overridePendingTransition(o.a.intercom_composer_stay, o.a.intercom_composer_slide_down);
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(o.f.intercom_composer_activity_input_full_screen);
      com.intercom.composer.h.a(this.getWindow(), o.b.intercom_composer_status_bar);
      Intent var2 = this.getIntent();
      this.a = b.a(var2.getSerializableExtra("fragment_class"));
      this.b = var2.getBundleExtra("fragment_args");
   }

   public void onGalleryOutputReceived(c var1) {
      Intent var2 = new Intent();
      var2.putExtra("gallery_image", var1);
      this.setResult(-1, var2);
      this.onBackPressed();
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2;
      if(var1.getItemId() == 16908332) {
         this.onBackPressed();
         var2 = true;
      } else {
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }

   protected void onPostCreate(Bundle var1) {
      super.onPostCreate(var1);
      android.support.v4.app.n var3 = this.getSupportFragmentManager();
      String var4 = g.class.getName();
      if(var3.a(var4) == null) {
         g var2 = (g)b.a(this.a);
         if(this.b == null) {
            var1 = new Bundle();
         } else {
            var1 = new Bundle(this.b);
         }

         var1.putAll(g.createArguments(true));
         var2.setArguments(var1);
         var2.setGalleryListener(this);
         var3.a().b(o.d.expanded_fragment, var2, var4).c();
      }

   }
}
