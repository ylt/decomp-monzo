package com.intercom.input.gallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class GalleryLightBoxActivity extends android.support.v7.app.e {
   c a;
   android.support.v4.app.n b;
   private Class c;

   public static Intent a(Context var0, c var1, Class var2) {
      return (new Intent(var0, GalleryLightBoxActivity.class)).putExtra("gallery_image", var1).putExtra("fragment_class", var2);
   }

   public void onBackPressed() {
      super.onBackPressed();
      this.overridePendingTransition(o.a.intercom_composer_stay, o.a.intercom_composer_slide_down);
   }

   protected void onCreate(Bundle var1) {
      Window var2 = this.getWindow();
      super.onCreate(var1);
      this.setContentView(o.f.intercom_composer_activity_gallery_lightbox);
      com.intercom.composer.h.a(var2, o.b.intercom_composer_status_bar);
      Intent var3 = this.getIntent();
      this.a = (c)var3.getParcelableExtra("gallery_image");
      this.c = b.a(var3.getSerializableExtra("fragment_class"));
      this.b = this.getSupportFragmentManager();
      if(this.b.a(o.d.fragment_container) == null) {
         i var4 = (i)b.a(this.c);
         var4.setArguments(i.createArgs(this.a));
         this.b.a().b(o.d.fragment_container, var4).c();
      }

   }
}
