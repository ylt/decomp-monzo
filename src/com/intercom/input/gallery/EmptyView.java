package com.intercom.input.gallery;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EmptyView extends LinearLayout {
   private TextView a;
   private TextView b;
   private Button c;
   private String d;
   private String e;
   private String f;
   private int g;
   private int h;
   private int i;
   private int j;

   public EmptyView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public EmptyView(Context var1, AttributeSet var2) {
      super(var1, var2);
      LayoutInflater.from(var1).inflate(o.f.intercom_composer_empty_view, this, true);
      this.setBackgroundResource(o.b.intercom_composer_white);
      TypedArray var7 = var1.obtainStyledAttributes(var2, o.h.intercom_composer_empty_view, 0, 0);

      try {
         this.d = var7.getString(o.h.intercom_composer_empty_view_intercom_composer_titleText);
         this.e = var7.getString(o.h.intercom_composer_empty_view_intercom_composer_subtitleText);
         this.f = var7.getString(o.h.intercom_composer_empty_view_intercom_composer_actionButtonText);
         this.g = var7.getDimensionPixelSize(o.h.intercom_composer_empty_view_intercom_composer_internalPaddingTop, 0);
         this.h = var7.getDimensionPixelSize(o.h.intercom_composer_empty_view_intercom_composer_internalPaddingBottom, 0);
         this.i = var7.getDimensionPixelSize(o.h.intercom_composer_empty_view_intercom_composer_internalPaddingLeft, 0);
         this.j = var7.getDimensionPixelSize(o.h.intercom_composer_empty_view_intercom_composer_internalPaddingRight, 0);
      } catch (RuntimeException var5) {
         this.d = "";
         this.e = "";
         this.f = "";
         this.g = 0;
         this.h = 0;
         this.i = 0;
         this.j = 0;
      } finally {
         var7.recycle();
      }

   }

   protected void onFinishInflate() {
      super.onFinishInflate();
      this.a = (TextView)this.findViewById(o.d.empty_text_title);
      this.b = (TextView)this.findViewById(o.d.empty_text_subtitle);
      this.c = (Button)this.findViewById(o.d.empty_action_button);
      this.a.setText(this.d);
      this.b.setText(this.e);
      if(!TextUtils.isEmpty(this.f)) {
         this.c.setText(this.f);
      } else {
         this.c.setVisibility(8);
      }

      ((LinearLayout)this.findViewById(o.d.empty_view_layout)).setPadding(this.i, this.g, this.j, this.h);
   }

   public void setActionButtonClickListener(OnClickListener var1) {
      this.c.setOnClickListener(var1);
   }

   public void setActionButtonVisibility(int var1) {
      this.c.setVisibility(var1);
   }

   public void setSubtitle(int var1) {
      this.b.setText(var1);
   }

   public void setSubtitle(CharSequence var1) {
      this.b.setText(var1);
   }

   public void setThemeColor(int var1) {
      this.c.setBackgroundColor(var1);
   }

   public void setTitle(int var1) {
      this.a.setText(var1);
   }

   public void setTitle(CharSequence var1) {
      this.a.setText(var1);
   }
}
