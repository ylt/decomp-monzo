package com.intercom.input.gallery;

public class d extends com.intercom.composer.b.b {
   private final j a;
   private final f b;
   private final com.intercom.composer.d c;

   public d(String var1, com.intercom.composer.b.a var2, j var3, f var4, com.intercom.composer.d var5) {
      super(var1, var2);
      this.a = var3;
      this.b = var4;
      this.c = var5;
   }

   public g a() {
      g var1 = (g)this.c.create();
      var1.setArguments(g.createArguments(false));
      var1.setGalleryListener(this.a);
      var1.setGalleryExpandedListener(this.b);
      return var1;
   }

   // $FF: synthetic method
   public com.intercom.composer.b.c createFragment() {
      return this.a();
   }
}
