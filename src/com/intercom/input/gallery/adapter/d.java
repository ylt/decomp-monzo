package com.intercom.input.gallery.adapter;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.intercom.input.gallery.o;

class d extends w {
   private final ImageView a;

   d(View var1, final e var2) {
      super(var1);
      this.a = (ImageView)var1.findViewById(o.d.thumbnail);
      var1.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            var2.onImageClicked(d.this);
         }
      });
   }

   public ImageView a() {
      return this.a;
   }
}
