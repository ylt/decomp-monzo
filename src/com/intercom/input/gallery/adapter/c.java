package com.intercom.input.gallery.adapter;

import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.intercom.input.gallery.o;
import java.util.List;

public class c extends android.support.v7.widget.RecyclerView.a {
   private final LayoutInflater a;
   private final boolean b;
   private final List c;
   private final e d;
   private final com.intercom.composer.e e;

   public c(LayoutInflater var1, List var2, boolean var3, e var4, com.intercom.composer.e var5) {
      this.a = var1;
      this.c = var2;
      this.b = var3;
      this.d = var4;
      this.e = var5;
   }

   public d a(ViewGroup var1, int var2) {
      if(this.b) {
         var2 = o.f.intercom_composer_expanded_image_list_item;
      } else {
         var2 = o.f.intercom_composer_image_list_item;
      }

      return new d(this.a.inflate(var2, var1, false), this.d);
   }

   public com.intercom.input.gallery.c a(int var1) {
      return (com.intercom.input.gallery.c)this.c.get(var1);
   }

   public void a(d var1) {
      super.onViewRecycled(var1);
      this.e.clear(var1.a());
   }

   public void a(d var1, int var2) {
      this.e.loadImageIntoView((com.intercom.input.gallery.c)this.c.get(var2), var1.a());
   }

   public int getItemCount() {
      return this.c.size();
   }

   // $FF: synthetic method
   public void onBindViewHolder(w var1, int var2) {
      this.a((d)var1, var2);
   }

   // $FF: synthetic method
   public w onCreateViewHolder(ViewGroup var1, int var2) {
      return this.a(var1, var2);
   }

   // $FF: synthetic method
   public void onViewRecycled(w var1) {
      this.a((d)var1);
   }
}
