package com.intercom.input.gallery.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.m;

public class a extends m {
   private final LinearLayoutManager a;
   private final b b;
   private int c = Integer.MIN_VALUE;

   public a(LinearLayoutManager var1, b var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(int var1) {
      this.c = var1;
   }

   public void onScrolled(RecyclerView var1, int var2, int var3) {
      super.onScrolled(var1, var2, var3);
      var2 = var1.getChildCount();
      var3 = this.a.F();
      if(var3 - var2 <= this.a.n() && var3 < this.c) {
         this.b.onLoadMore();
      }

   }
}
