package com.intercom.input.gallery.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.intercom.input.gallery.o;

public class SquareLayout extends FrameLayout {
   private int a;

   public SquareLayout(Context var1) {
      super(var1);
   }

   public SquareLayout(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.a(var1, var2);
   }

   public SquareLayout(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.a(var1, var2);
   }

   @TargetApi(21)
   public SquareLayout(Context var1, AttributeSet var2, int var3, int var4) {
      super(var1, var2, var3, var4);
      this.a(var1, var2);
   }

   private void a(Context var1, AttributeSet var2) {
      TypedArray var5 = var1.getTheme().obtainStyledAttributes(var2, o.h.intercom_composer_square_layout, 0, 0);

      try {
         this.a = var5.getInteger(o.h.intercom_composer_square_layout_intercom_composer_measure_type, 0);
      } finally {
         var5.recycle();
      }

   }

   protected void onMeasure(int var1, int var2) {
      if(this.a == 0) {
         super.onMeasure(var1, var1);
      } else {
         super.onMeasure(var2, var2);
      }

   }
}
