package com.intercom.input.gallery;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import java.util.ArrayList;
import java.util.List;

public abstract class g extends com.intercom.composer.b.c implements com.intercom.input.gallery.adapter.b, com.intercom.input.gallery.adapter.e, h {
   private static final String ARG_EXPANDED = "expanded";
   public static final int GALLERY_FULL_SCREEN_REQUEST_CODE = 14;
   FrameLayout contentLayout;
   final e.a dataListener = new e.a() {
      public void a() {
         if(g.this.isAdded()) {
            g.this.showErrorScreen();
         }

      }

      public void a(List var1) {
         g.this.galleryImages.clear();
         g.this.galleryImages.addAll(var1);
         g.this.endlessRecyclerScrollListener.a(g.this.dataSource.getCount());
         g.this.recyclerAdapter.notifyDataSetChanged();
         if(g.this.isAdded()) {
            g.this.showEmptyOrPermissionScreen(0);
         }

      }
   };
   e dataSource;
   EmptyView emptyLayout;
   com.intercom.input.gallery.adapter.a endlessRecyclerScrollListener;
   boolean expanded;
   private final OnClickListener expanderClickListener = new OnClickListener() {
      public void onClick(View var1) {
         if(g.this.galleryInputExpandedListener != null) {
            g.this.galleryInputExpandedListener.onInputExpanded();
         }

         Intent var3 = GalleryInputFullScreenActivity.a(g.this.getActivity(), g.this.getClass(), g.this.getArguments());
         Bundle var2 = android.support.v4.app.b.a(g.this.getActivity(), o.a.intercom_composer_slide_up, o.a.intercom_composer_stay).a();
         g.this.startActivityForResult(var3, 14, var2);
      }
   };
   final List galleryImages = new ArrayList();
   f galleryInputExpandedListener;
   j galleryOutputListener;
   private com.intercom.composer.e imageLoader;
   g.a injector;
   LinearLayoutManager layoutManager;
   com.intercom.input.gallery.adapter.c recyclerAdapter;
   RecyclerView recyclerView;

   public static Bundle createArguments(boolean var0) {
      Bundle var1 = new Bundle();
      var1.putBoolean("expanded", var0);
      return var1;
   }

   private void setUpAppBar(g.a var1, ViewGroup var2) {
      Toolbar var3 = var1.getToolbar(var2);
      var2.addView(var3);
      ((android.support.v7.app.e)this.getActivity()).setSupportActionBar(var3);
      android.support.v7.app.a var4 = ((android.support.v7.app.e)this.getActivity()).getSupportActionBar();
      if(var4 != null) {
         var4.b(true);
         var4.a(true);
         var4.c(false);
      }

   }

   private void setUpPreviewViews(g.a var1, ViewGroup var2) {
      ImageButton var3 = (ImageButton)var1.getExpanderButton(this.contentLayout);
      if(var3 != null) {
         this.contentLayout.addView(var3);
         var3.setOnClickListener(this.expanderClickListener);
      }

      View var4 = var1.getSearchView(this.contentLayout);
      if(var4 != null) {
         var4.setOnClickListener(this.expanderClickListener);
         var2.addView(var4, 0);
      }

   }

   private void showPermissionPermanentlyDeniedDialog() {
      (new android.support.v7.app.d.a(this.getActivity())).a(o.g.intercom_photo_access_denied).b(o.g.intercom_go_to_device_settings).a(o.g.intercom_app_settings, new android.content.DialogInterface.OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            Uri var3 = Uri.fromParts("package", g.this.getActivity().getPackageName(), (String)null);
            g.this.startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", var3));
         }
      }).b(o.g.intercom_not_now, (android.content.DialogInterface.OnClickListener)null).c();
   }

   void checkPermissionAndFetchImages(boolean var1) {
      int var2 = this.dataSource.getPermissionStatus();
      switch(var2) {
      case 1:
      case 3:
         this.showEmptyOrPermissionScreen(var2);
         if(var1) {
            this.dataSource.requestPermission();
         }
         break;
      case 2:
         this.showEmptyOrPermissionScreen(var2);
         if(var1) {
            this.showPermissionPermanentlyDeniedDialog();
         }
         break;
      default:
         this.fetchImagesIfNotFetched();
      }

   }

   void fetchImagesIfNotFetched() {
      if(this.galleryImages.isEmpty()) {
         this.dataSource.getImages(0, (String)null);
      }

   }

   protected abstract g.a getInjector(g var1);

   int getLayoutRes() {
      int var1;
      if(this.expanded) {
         var1 = o.f.intercom_composer_fragment_composer_gallery_expanded;
      } else {
         var1 = o.f.intercom_composer_fragment_composer_gallery;
      }

      return var1;
   }

   void launchLightBoxActivity(c var1) {
      android.support.v4.app.j var2 = this.getActivity();
      this.startActivityForResult(GalleryLightBoxActivity.a(var2, var1, this.getInjector(this).getLightBoxFragmentClass(this)), 14, android.support.v4.app.b.a(var2, o.a.intercom_composer_slide_up, o.a.intercom_composer_stay).a());
   }

   public void onActivityResult(int var1, int var2, Intent var3) {
      if(var1 == 14 && var2 == -1) {
         if(this.galleryOutputListener != null) {
            c var4 = (c)var3.getParcelableExtra("gallery_image");
            this.galleryOutputListener.onGalleryOutputReceived(var4);
         }
      } else {
         super.onActivityResult(var1, var2, var3);
      }

   }

   public void onAttach(Context var1) {
      super.onAttach(var1);
      if(var1 instanceof j) {
         this.galleryOutputListener = (j)var1;
      }

      if(var1 instanceof f) {
         this.galleryInputExpandedListener = (f)var1;
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      var1 = this.getArguments();
      if(var1 != null) {
         this.expanded = var1.getBoolean("expanded", false);
      }

      if(this.expanded) {
         int var2 = this.getResources().getInteger(o.e.intercom_composer_expanded_column_count);
         this.layoutManager = new GridLayoutManager(this.getContext(), var2);
      } else {
         this.layoutManager = new LinearLayoutManager(this.getContext(), 0, false);
      }

      this.injector = this.getInjector(this);
      this.dataSource = this.injector.getDataSource(this);
      this.dataSource.setListener(this.dataListener);
      this.imageLoader = this.injector.getImageLoader(this);
      this.endlessRecyclerScrollListener = new com.intercom.input.gallery.adapter.a(this.layoutManager, this);
      this.recyclerAdapter = new com.intercom.input.gallery.adapter.c(LayoutInflater.from(this.getContext()), this.galleryImages, this.expanded, this, this.imageLoader);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      View var5 = var1.inflate(this.getLayoutRes(), var2, false);
      ViewGroup var4 = (ViewGroup)var5.findViewById(o.d.gallery_root_view);
      this.recyclerView = (RecyclerView)var5.findViewById(o.d.gallery_recycler_view);
      this.emptyLayout = (EmptyView)var5.findViewById(o.d.gallery_empty_view);
      this.contentLayout = (FrameLayout)var5.findViewById(o.d.gallery_content_layout);
      if(this.expanded) {
         this.setUpAppBar(this.injector, var4);
         this.recyclerView.a(new k(o.c.intercom_composer_toolbar_height));
      } else {
         this.setUpPreviewViews(this.injector, var4);
      }

      this.emptyLayout.setActionButtonClickListener(new OnClickListener() {
         public void onClick(View var1) {
            switch(g.this.dataSource.getPermissionStatus()) {
            case 1:
            case 3:
               g.this.dataSource.requestPermission();
               break;
            case 2:
               g.this.showPermissionPermanentlyDeniedDialog();
            }

         }
      });
      this.emptyLayout.setThemeColor(this.injector.getThemeColor(this.getContext()));
      return var5;
   }

   public void onDestroyView() {
      super.onDestroyView();
      this.recyclerView.b(this.endlessRecyclerScrollListener);
      this.recyclerView.setLayoutManager((android.support.v7.widget.RecyclerView.h)null);
   }

   public void onImageClicked(w var1) {
      int var2 = var1.getAdapterPosition();
      if(var2 > -1 && var2 < this.recyclerAdapter.getItemCount()) {
         this.launchLightBoxActivity(this.recyclerAdapter.a(var2));
      }

   }

   public void onInputDeselected() {
   }

   public void onInputReselected() {
   }

   public void onInputSelected() {
      this.checkPermissionAndFetchImages(true);
   }

   public void onLoadMore() {
      if(!this.galleryImages.isEmpty() && !this.dataSource.isLoading()) {
         this.dataSource.getImages(this.galleryImages.size(), (String)null);
      }

   }

   public void onRequestPermissionsResult(int var1, String[] var2, int[] var3) {
      this.checkPermissionAndFetchImages(false);
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      this.recyclerView.setLayoutManager(this.layoutManager);
      this.recyclerView.setAdapter(this.recyclerAdapter);
      this.recyclerView.a(this.endlessRecyclerScrollListener);
      if(this.expanded) {
         this.onInputSelected();
      }

      this.endlessRecyclerScrollListener.a(this.dataSource.getCount());
   }

   protected void passDataOnViewCreated(Bundle var1) {
   }

   public void setGalleryExpandedListener(f var1) {
      this.galleryInputExpandedListener = var1;
   }

   public void setGalleryListener(j var1) {
      this.galleryOutputListener = var1;
   }

   void showEmptyOrPermissionScreen(int var1) {
      switch(var1) {
      case 0:
         if(this.galleryImages.isEmpty()) {
            this.emptyLayout.setVisibility(0);
            this.emptyLayout.setActionButtonVisibility(8);
            this.emptyLayout.setTitle(this.injector.getEmptyViewTitle(this.getResources()));
            this.emptyLayout.setSubtitle(this.injector.getEmptyViewSubtitle(this.getResources()));
            this.contentLayout.setVisibility(8);
         } else {
            this.emptyLayout.setVisibility(8);
            this.contentLayout.setVisibility(0);
         }
         break;
      case 1:
      case 2:
         this.emptyLayout.setVisibility(0);
         this.emptyLayout.setTitle(o.g.intercom_photo_access_denied);
         this.emptyLayout.setSubtitle(o.g.intercom_allow_storage_access);
         this.emptyLayout.setActionButtonVisibility(0);
         this.contentLayout.setVisibility(8);
         break;
      case 3:
         this.emptyLayout.setVisibility(0);
         this.emptyLayout.setTitle(o.g.intercom_access_photos);
         this.emptyLayout.setSubtitle(o.g.intercom_storage_access_request);
         this.emptyLayout.setActionButtonVisibility(8);
         this.contentLayout.setVisibility(8);
      }

   }

   void showErrorScreen() {
      this.emptyLayout.setVisibility(0);
      this.emptyLayout.setActionButtonVisibility(8);
      this.emptyLayout.setTitle(this.injector.getErrorViewTitle(this.getResources()));
      this.emptyLayout.setSubtitle(this.injector.getErrorViewSubtitle(this.getResources()));
      this.contentLayout.setVisibility(8);
   }

   public interface a {
      e getDataSource(g var1);

      String getEmptyViewSubtitle(Resources var1);

      String getEmptyViewTitle(Resources var1);

      String getErrorViewSubtitle(Resources var1);

      String getErrorViewTitle(Resources var1);

      View getExpanderButton(ViewGroup var1);

      com.intercom.composer.e getImageLoader(g var1);

      Class getLightBoxFragmentClass(g var1);

      View getSearchView(ViewGroup var1);

      int getThemeColor(Context var1);

      Toolbar getToolbar(ViewGroup var1);
   }
}
