package com.intercom.input.gallery;

import java.io.Serializable;

public class b {
   public static Class a(Serializable var0) {
      return (Class)var0;
   }

   public static Object a(Class var0) {
      try {
         Object var1 = var0.newInstance();
         return var1;
      } catch (InstantiationException var2) {
         throw new RuntimeException("Could not instantiate " + var0, var2);
      } catch (IllegalAccessException var3) {
         throw new RuntimeException("Could not instantiate" + var0 + ", make sure the class is public and static", var3);
      }
   }
}
