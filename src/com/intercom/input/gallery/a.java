package com.intercom.input.gallery;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;

public class a extends Drawable {
   private final Paint a = new Paint();

   public a(int var1) {
      this.a.setAntiAlias(true);
      this.a.setColor(var1);
      this.a.setStyle(Style.FILL);
   }

   public void draw(Canvas var1) {
      Rect var2 = this.getBounds();
      var1.drawCircle((float)((var2.left + var2.right) / 2), (float)((var2.top + var2.bottom) / 2), (float)((var2.right - var2.left) / 2), this.a);
   }

   public int getOpacity() {
      return -3;
   }

   public void setAlpha(int var1) {
      this.a.setAlpha(var1);
   }

   public void setColorFilter(ColorFilter var1) {
      this.a.setColorFilter(var1);
   }
}
