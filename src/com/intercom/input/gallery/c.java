package com.intercom.input.gallery;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.File;

public class c implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public c a(Parcel var1) {
         return new c(var1);
      }

      public c[] a(int var1) {
         return new c[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return this.a(var1);
      }
   };
   private final String a;
   private final String b;
   private final String c;
   private final String d;
   private final String e;
   private final int f;
   private final int g;
   private final int h;
   private final boolean i;

   protected c(Parcel var1) {
      boolean var2 = true;
      super();
      this.a = var1.readString();
      this.b = var1.readString();
      this.c = var1.readString();
      this.d = var1.readString();
      this.e = var1.readString();
      this.f = var1.readInt();
      this.g = var1.readInt();
      this.h = var1.readInt();
      if(var1.readInt() != 1) {
         var2 = false;
      }

      this.i = var2;
   }

   c(String var1, String var2, String var3, String var4, String var5, int var6, int var7, int var8, boolean var9) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
      this.i = var9;
   }

   public String a() {
      return this.c;
   }

   public String b() {
      return this.a;
   }

   public String c() {
      return this.b;
   }

   public String d() {
      return this.d;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.e;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               c var4 = (c)var1;
               var2 = var3;
               if(this.f == var4.f) {
                  var2 = var3;
                  if(this.g == var4.g) {
                     var2 = var3;
                     if(this.h == var4.h) {
                        var2 = var3;
                        if(this.i == var4.i) {
                           var2 = var3;
                           if(this.a.equals(var4.a)) {
                              var2 = var3;
                              if(this.b.equals(var4.b)) {
                                 var2 = var3;
                                 if(this.c.equals(var4.c)) {
                                    var2 = var3;
                                    if(this.d.equals(var4.d)) {
                                       var2 = this.e.equals(var4.e);
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public int f() {
      return this.f;
   }

   public int g() {
      return this.g;
   }

   public int h() {
      return this.h;
   }

   public int hashCode() {
      int var2 = this.a.hashCode();
      int var6 = this.b.hashCode();
      int var3 = this.c.hashCode();
      int var8 = this.d.hashCode();
      int var9 = this.e.hashCode();
      int var5 = this.f;
      int var7 = this.g;
      int var4 = this.h;
      byte var1;
      if(this.i) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      return var1 + (((((((var2 * 31 + var6) * 31 + var3) * 31 + var8) * 31 + var9) * 31 + var5) * 31 + var7) * 31 + var4) * 31;
   }

   public boolean i() {
      return this.i;
   }

   public File j() {
      return new File(this.c);
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.a);
      var1.writeString(this.b);
      var1.writeString(this.c);
      var1.writeString(this.d);
      var1.writeString(this.e);
      var1.writeInt(this.f);
      var1.writeInt(this.g);
      var1.writeInt(this.h);
      byte var3;
      if(this.i) {
         var3 = 1;
      } else {
         var3 = 0;
      }

      var1.writeInt(var3);
   }

   public static final class a {
      private String a;
      private String b;
      private String c;
      private String d;
      private String e;
      private int f;
      private int g;
      private int h;
      private boolean i;

      private static String f(String var0) {
         String var1 = var0;
         if(var0 == null) {
            var1 = "";
         }

         return var1;
      }

      public c.a a(int var1) {
         this.f = var1;
         return this;
      }

      public c.a a(String var1) {
         this.a = var1;
         return this;
      }

      public c.a a(boolean var1) {
         this.i = var1;
         return this;
      }

      public c a() {
         return new c(f(this.a), f(this.b), f(this.c), f(this.d), f(this.e), this.f, this.g, this.h, this.i);
      }

      public c.a b(int var1) {
         this.g = var1;
         return this;
      }

      public c.a b(String var1) {
         this.b = var1;
         return this;
      }

      public c.a c(int var1) {
         this.h = var1;
         return this;
      }

      public c.a c(String var1) {
         this.c = var1;
         return this;
      }

      public c.a d(String var1) {
         this.d = var1;
         return this;
      }

      public c.a e(String var1) {
         this.e = var1;
         return this;
      }
   }
}
