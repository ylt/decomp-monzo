package com.intercom.input.gallery;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.BitmapFactory.Options;
import android.os.Build.VERSION;
import android.provider.MediaStore.Images.Media;
import java.util.ArrayList;
import java.util.List;

public class m implements e {
   private e.a a;
   private n b;
   private Context c;
   private h d;
   private boolean e;

   m(Context var1, n var2, h var3) {
      this.c = var1;
      this.d = var3;
      this.b = var2;
   }

   private Point a(Cursor var1, String var2) {
      int var3;
      int var4;
      if(VERSION.SDK_INT >= 16) {
         var4 = var1.getInt(var1.getColumnIndexOrThrow("height"));
         var3 = var1.getInt(var1.getColumnIndexOrThrow("width"));
      } else {
         Options var5 = new Options();
         var5.inJustDecodeBounds = true;
         BitmapFactory.decodeFile(var2, var5);
         var3 = var5.outWidth;
         var4 = var5.outHeight;
      }

      return new Point(var3, var4);
   }

   public static e a(g var0) {
      android.support.v4.app.j var1 = var0.getActivity();
      return new m(var1, n.a((Activity)var1), var0);
   }

   List a(Cursor var1) {
      ArrayList var4 = new ArrayList(var1.getCount());
      if(var1.moveToFirst()) {
         do {
            String var6 = var1.getString(var1.getColumnIndexOrThrow("_data"));
            String var3 = var1.getString(var1.getColumnIndexOrThrow("mime_type"));
            String var5 = var1.getString(var1.getColumnIndexOrThrow("title"));
            int var2 = var1.getInt(var1.getColumnIndexOrThrow("_size"));
            if(var6 != null && var5 != null && var3 != null) {
               Point var7 = this.a(var1, var6);
               var4.add((new c.a()).a(var5).c(var6).b(var3).a(var7.x).b(var7.y).c(var2).a());
            }
         } while(var1.moveToNext());
      }

      var1.close();
      return var4;
   }

   public int getCount() {
      int var1 = 0;
      if(this.getPermissionStatus() == 0) {
         Cursor var2 = this.c.getContentResolver().query(Media.EXTERNAL_CONTENT_URI, (String[])null, (String)null, (String[])null, (String)null);
         if(var2 != null) {
            var1 = var2.getCount();
            var2.close();
         } else {
            var1 = 0;
         }
      }

      return var1;
   }

   public void getImages(int var1, String var2) {
      this.e = true;
      String[] var3;
      if(VERSION.SDK_INT >= 16) {
         var3 = new String[]{"_data", "date_added", "mime_type", "title", "height", "width", "_size"};
      } else {
         var3 = new String[]{"_data", "date_added", "mime_type", "title", "_size"};
      }

      Cursor var4 = this.c.getContentResolver().query(Media.EXTERNAL_CONTENT_URI, var3, (String)null, (String[])null, "date_added DESC LIMIT 50 OFFSET " + var1);
      this.e = false;
      if(var4 == null) {
         this.a.a();
      } else {
         this.a.a(this.a(var4));
      }

   }

   public int getPermissionStatus() {
      int var1;
      if(VERSION.SDK_INT >= 23) {
         var1 = this.b.a("android.permission.READ_EXTERNAL_STORAGE");
      } else {
         var1 = 0;
      }

      return var1;
   }

   public boolean isLoading() {
      return this.e;
   }

   public void requestPermission() {
      this.b.a(true);
      if(VERSION.SDK_INT >= 23) {
         this.d.requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1);
      }

   }

   public void setListener(e.a var1) {
      this.a = var1;
   }
}
