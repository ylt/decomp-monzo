package com.intercom.input.gallery;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.t;
import android.view.View;

public class k extends android.support.v7.widget.RecyclerView.g {
   private final int a;

   public k(int var1) {
      this.a = var1;
   }

   public void getItemOffsets(Rect var1, View var2, RecyclerView var3, t var4) {
      int var6 = var3.getResources().getInteger(o.e.intercom_composer_expanded_column_count);
      int var5 = var3.f(var2);
      if(var5 >= 0 && var5 < var6) {
         var1.set(0, var3.getResources().getDimensionPixelOffset(this.a), 0, 0);
      }

   }
}
