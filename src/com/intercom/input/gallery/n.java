package com.intercom.input.gallery;

import android.app.Activity;
import android.content.SharedPreferences;

class n {
   private final SharedPreferences a;
   private final Activity b;

   n(Activity var1, SharedPreferences var2) {
      this.b = var1;
      this.a = var2;
   }

   static n a(Activity var0) {
      return new n(var0, var0.getSharedPreferences("intercom_composer_permission_status_prefs", 0));
   }

   int a(String var1) {
      byte var2 = 0;
      if(android.support.v4.content.a.b(this.b, var1) != 0) {
         if(android.support.v4.app.a.a(this.b, var1)) {
            var2 = 1;
         } else if(this.a.getBoolean("asked_for_permission", false)) {
            var2 = 2;
         } else {
            var2 = 3;
         }
      }

      return var2;
   }

   void a(boolean var1) {
      this.a.edit().putBoolean("asked_for_permission", var1).apply();
   }
}
