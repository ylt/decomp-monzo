package com.intercom.input.gallery;

import java.util.List;

public interface e {
   int getCount();

   void getImages(int var1, String var2);

   int getPermissionStatus();

   boolean isLoading();

   void requestPermission();

   void setListener(e.a var1);

   public interface a {
      void a();

      void a(List var1);
   }
}
