package com.intercom.input.gallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.List;

public class AnnotatedImageView extends AppCompatImageView {
   private Path a = new Path();
   private Paint b;
   private Canvas c;
   private Bitmap d;
   private final List e = new ArrayList();
   private final List f = new ArrayList();
   private int g = -10092544;
   private boolean h = false;
   private Rect i = new Rect();
   private AnnotatedImageView.a j;
   private final Runnable k = new Runnable() {
      public void run() {
         AnnotatedImageView.this.d();
      }
   };

   public AnnotatedImageView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.c();
   }

   private void a(Canvas var1) {
      int var3 = this.e.size();

      for(int var2 = 0; var2 < var3; ++var2) {
         var1.drawPath((Path)this.e.get(var2), (Paint)this.f.get(var2));
      }

   }

   private void c() {
      this.b = new Paint();
      this.b.setColor(this.g);
      this.b.setAntiAlias(true);
      this.b.setStrokeWidth(32.0F);
      this.b.setStyle(Style.STROKE);
      this.b.setStrokeJoin(Join.ROUND);
      this.b.setStrokeCap(Cap.ROUND);
   }

   private void d() {
      if(this.h) {
         Drawable var5 = this.getDrawable();
         if(var5 != null) {
            int var2 = this.getHeight();
            int var1 = this.getWidth();
            int var3 = var5.getIntrinsicHeight();
            int var4 = var5.getIntrinsicWidth();
            if(var2 * var4 <= var1 * var3) {
               var3 = var4 * var2 / var3;
               var1 = (var1 - var3) / 2;
               this.i.set(var1, 0, var3 + var1, var2);
            } else {
               var3 = var3 * var1 / var4;
               var2 = (var2 - var3) / 2;
               this.i.set(0, var2, var1, var3 + var2);
            }

            this.d = Bitmap.createBitmap(this.i.width(), this.i.height(), Config.ARGB_8888);
            this.c = new Canvas(this.d);
            this.c.translate((float)(-this.i.left), (float)(-this.i.top));
         }
      }

   }

   private void e() {
      if(this.j != null) {
         this.j.a(this.e.size());
      }

   }

   public void a() {
      this.e.clear();
      this.f.clear();
      if(this.c != null) {
         this.c.drawColor(0, Mode.CLEAR);
      }

      this.invalidate();
      this.e();
   }

   public void b() {
      if(!this.e.isEmpty()) {
         this.e.remove(this.e.size() - 1);
         this.f.remove(this.f.size() - 1);
         this.invalidate();
         this.e();
      }

   }

   public Bitmap getAnnotationsBitmap() {
      this.a(this.c);
      return this.d;
   }

   public int getPathCount() {
      return this.e.size();
   }

   protected void onDraw(Canvas var1) {
      super.onDraw(var1);
      var1.clipRect(this.i);
      this.a(var1);
      var1.drawPath(this.a, this.b);
   }

   protected void onSizeChanged(int var1, int var2, int var3, int var4) {
      super.onSizeChanged(var1, var2, var3, var4);
      if(var1 > 0 && var2 > 0) {
         this.post(this.k);
      }

   }

   public boolean onTouchEvent(MotionEvent var1) {
      boolean var4 = false;
      if(this.h) {
         float var2 = var1.getX();
         float var3 = var1.getY();
         switch(var1.getAction()) {
         case 0:
            this.a.moveTo(var2, var3);
            break;
         case 1:
            this.a.lineTo(var2, var3);
            this.e.add(this.a);
            this.f.add(this.b);
            this.e();
            this.a = new Path();
            this.c();
            break;
         case 2:
            this.a.lineTo(var2, var3);
            break;
         default:
            return var4;
         }

         this.invalidate();
         var4 = true;
      }

      return var4;
   }

   public void setAnnotationEnabled(boolean var1) {
      this.h = var1;
   }

   public void setColor(int var1) {
      this.g = var1;
      this.b.setColor(this.g);
   }

   public void setImageBitmap(Bitmap var1) {
      super.setImageBitmap(var1);
      this.post(this.k);
   }

   public void setImageDrawable(Drawable var1) {
      super.setImageDrawable(var1);
      this.post(this.k);
   }

   public void setListener(AnnotatedImageView.a var1) {
      this.j = var1;
   }

   public interface a {
      void a(int var1);
   }
}
