package com.intercom.input.gallery;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import java.util.List;

public abstract class i extends Fragment implements OnClickListener {
   private static final int COLOR_PICKER_DIAMETER_DP = 48;
   private static final int COLOR_PICKER_PADDING_DP = 12;
   private static final float HIGHLIGHTED_COLOR_SCALE = 2.5F;
   private static final float INITIAL_UNDO_BUTTON_SCALE = 0.9F;
   private static final int INTRO_ANIMATION_DURATION_MS = 800;
   private static final int INTRO_ANIMATION_STAGGER_DELAY_MS = 50;
   private static final float UNSELECTED_COLOR_ALPHA = 0.3F;
   private AnnotatedImageView annotatedImageView;
   private List annotationColors;
   c galleryImage;
   private com.intercom.composer.e imageLoader;
   private boolean isAnnotationEnabled;
   private View selectedColorView;

   public static Bundle createArgs(c var0) {
      Bundle var1 = new Bundle();
      var1.putParcelable("gallery_image", var0);
      return var1;
   }

   private int dpToPx(int var1) {
      return (int)((float)var1 * this.getResources().getDisplayMetrics().density);
   }

   private void setupAnnotations(View var1, boolean var2, List var3) {
      byte var4 = 8;
      final View var5 = var1.findViewById(o.d.lightbox_undo_button);
      var5.setVisibility(8);
      if(var2) {
         this.annotatedImageView.setAnnotationEnabled(true);
         var5.setOnClickListener(new OnClickListener() {
            public void onClick(View var1) {
               i.this.annotatedImageView.b();
            }
         });
         var5.setAlpha(0.0F);
         var5.setScaleX(0.9F);
         var5.setScaleY(0.9F);
         this.annotatedImageView.setListener(new AnnotatedImageView.a() {
            public void a(int var1) {
               View var2 = var5;
               byte var3;
               if(var1 > 0) {
                  var3 = 0;
               } else {
                  var3 = 8;
               }

               var2.setVisibility(var3);
            }
         });
         this.setupColorPickers(var1, var3, this.annotatedImageView, var5);
      }

      var1 = var1.findViewById(o.d.lightbox_annotation_controls_space);
      if(var2) {
         var4 = 0;
      }

      var1.setVisibility(var4);
   }

   private void setupColorPickers(View var1, List var2, final AnnotatedImageView var3, View var4) {
      ViewGroup var11 = (ViewGroup)var1.findViewById(o.d.lightbox_button_layout);
      int var9 = var11.indexOfChild(var4);
      int var7 = this.dpToPx(48);
      int var8 = this.dpToPx(12);
      final View[] var16 = new View[var2.size()];
      final OvershootInterpolator var15 = new OvershootInterpolator();
      final android.support.v4.view.b.b var12 = new android.support.v4.view.b.b();

      for(int var6 = 0; var6 < var2.size(); ++var6) {
         ImageView var14 = new ImageView(var1.getContext());
         var16[var6] = var14;
         var14.setPadding(var8, var8, var8, var8);
         var14.setLayoutParams(new LayoutParams(var7, var7));
         final int var10 = ((Integer)var2.get(var6)).intValue();
         var14.setOnTouchListener(new OnTouchListener(new Rect()) {
            // $FF: synthetic field
            final Rect f;

            {
               this.f = var7;
            }

            private boolean a(View var1, int var2, int var3x) {
               var1.getHitRect(this.f);
               this.f.offset(-this.f.left, -this.f.top);
               return this.f.contains(var2, var3x);
            }

            public boolean onTouch(View var1, MotionEvent var2) {
               float var3x = 1.0F;
               boolean var6;
               ViewPropertyAnimator var9;
               switch(android.support.v4.view.h.a(var2)) {
               case 0:
                  var1.animate().scaleX(2.5F).scaleY(2.5F).alpha(1.0F).setDuration(200L).setInterpolator(var15).start();
                  var6 = true;
                  break;
               case 1:
                  var1.animate().scaleX(1.0F).scaleY(1.0F).setDuration(200L).setInterpolator(var15).start();
                  if(this.a(var1, (int)var2.getX(), (int)var2.getY())) {
                     var3.setColor(var10);
                     i.this.selectedColorView = var1;
                     var1.performClick();
                  }

                  View[] var8 = var16;
                  int var5 = var8.length;

                  for(int var4 = 0; var4 < var5; ++var4) {
                     View var7 = var8[var4];
                     var9 = var7.animate();
                     if(var7 == i.this.selectedColorView) {
                        var3x = 1.0F;
                     } else {
                        var3x = 0.3F;
                     }

                     var9.alpha(var3x).setInterpolator(var12).setDuration(200L).start();
                  }

                  var6 = true;
                  break;
               case 2:
               default:
                  var6 = false;
                  break;
               case 3:
                  var9 = var1.animate().scaleX(1.0F).scaleY(1.0F);
                  if(var1 != i.this.selectedColorView) {
                     var3x = 0.3F;
                  }

                  var9.alpha(var3x).setInterpolator(var15).setDuration(200L).start();
                  var6 = true;
               }

               return var6;
            }
         });
         var14.setImageDrawable(new a(var10));
         var14.setAlpha(0.0F);
         ViewPropertyAnimator var13 = var14.animate();
         float var5;
         if(var6 == 0) {
            var5 = 1.0F;
         } else {
            var5 = 0.3F;
         }

         var13.alpha(var5).setDuration(800L).setStartDelay((long)(var6 * 50)).start();
         var11.addView(var14, var9 + var6);
      }

      var3.setColor(((Integer)var2.get(0)).intValue());
      this.selectedColorView = var16[0];
   }

   protected abstract i.a getInjector(i var1);

   public void onClick(View var1) {
      if(var1.getId() == o.d.lightbox_send_button) {
         if(this.isAnnotationEnabled && this.annotatedImageView.getPathCount() > 0) {
            this.galleryImage = (new l(this.getActivity().getCacheDir(), this.imageLoader)).a(this.annotatedImageView, this.galleryImage);
         }

         Intent var2 = new Intent();
         var2.putExtra("gallery_image", this.galleryImage);
         this.getActivity().setResult(-1, var2);
      }

      this.getActivity().onBackPressed();
   }

   public void onConfigurationChanged(Configuration var1) {
      super.onConfigurationChanged(var1);
      if(this.annotatedImageView != null) {
         this.annotatedImageView.a();
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.galleryImage = (c)this.getArguments().getParcelable("gallery_image");
      i.a var3 = this.getInjector(this);
      this.imageLoader = var3.getImageLoader(this);
      this.annotationColors = var3.getAnnotationColors(this);
      boolean var2;
      if(var3.isAnnotationEnabled(this) && !this.annotationColors.isEmpty()) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.isAnnotationEnabled = var2;
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(o.f.intercom_composer_gallery_lightbox_fragment, var2, false);
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      this.annotatedImageView = (AnnotatedImageView)var1.findViewById(o.d.lightbox_image);
      this.imageLoader.loadImageIntoView(this.galleryImage, this.annotatedImageView);
      var1.findViewById(o.d.lightbox_send_button).setOnClickListener(this);
      var1.findViewById(o.d.lightbox_close_button).setOnClickListener(this);
      this.setupAnnotations(var1, this.isAnnotationEnabled, this.annotationColors);
   }

   public interface a {
      List getAnnotationColors(i var1);

      com.intercom.composer.e getImageLoader(i var1);

      boolean isAnnotationEnabled(i var1);
   }
}
