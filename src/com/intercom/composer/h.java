package com.intercom.composer;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.view.Window;

public class h {
   @TargetApi(19)
   public static void a(Window var0, int var1) {
      if(VERSION.SDK_INT > 19) {
         var0.getDecorView().setSystemUiVisibility(1280);
         c(var0, var1);
      }

   }

   @TargetApi(21)
   private static void b(Window var0, int var1) {
      if(VERSION.SDK_INT >= 21) {
         var0.clearFlags(67108864);
         var0.addFlags(Integer.MIN_VALUE);
         var0.setStatusBarColor(var1);
      }

   }

   @TargetApi(21)
   private static void c(Window var0, int var1) {
      b(var0, android.support.v4.content.a.c(var0.getContext(), var1));
   }
}
