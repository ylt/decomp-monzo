package com.intercom.composer;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.app.n;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.w;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ComposerView extends LinearLayout implements a, com.intercom.composer.b.b.b, com.intercom.composer.b.b.f, com.intercom.composer.b.c.a.a, com.intercom.composer.d.a {
   RecyclerView a;
   LinearLayout b;
   ViewPager c;
   ImageView d;
   View e;
   View f;
   View g;
   com.intercom.composer.b.b.e h;
   com.intercom.composer.c.a i;
   com.intercom.composer.a.b j;
   f k;
   com.intercom.composer.a.f l;
   com.intercom.composer.a.a m;
   final com.intercom.composer.d.b n;
   private final List o;
   private final LinearLayoutManager p;
   private final com.intercom.composer.c.d q;
   private com.intercom.composer.b.c.a.a r;

   public ComposerView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public ComposerView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public ComposerView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.o = new ArrayList();
      this.m = com.intercom.composer.a.a.e;
      this.setOrientation(1);
      inflate(var1, g.f.intercom_composer_view_layout, this);
      this.f = this.findViewById(g.e.composer_upper_border);
      this.g = this.findViewById(g.e.composer_lower_border);
      this.b = (LinearLayout)this.findViewById(g.e.composer_edit_text_layout);
      this.a = (RecyclerView)this.findViewById(g.e.composer_input_icons_recycler_view);
      this.c = (ViewPager)this.findViewById(g.e.composer_view_pager);
      this.d = (ImageView)this.findViewById(g.e.send_button);
      this.e = this.findViewById(g.e.send_button_fading_background);
      this.q = new com.intercom.composer.c.d(var1);
      this.i = new com.intercom.composer.c.a((Activity)var1, this.q, this.b, this.c);
      this.j = new com.intercom.composer.a.b(this.b);
      this.p = new LinearLayoutManager(var1, 0, false);
      this.a.setLayoutManager(this.p);
      this.a.a(new com.intercom.composer.b.b.c(var1));
      this.n = new com.intercom.composer.d.b(this);
   }

   private void a(View var1) {
      ViewParent var2 = var1.getParent();
      if(var2 instanceof ViewGroup) {
         ((ViewGroup)var2).removeView(var1);
      }

   }

   private void a(EditText var1) {
      var1.requestFocus();
      ((InputMethodManager)var1.getContext().getSystemService("input_method")).showSoftInput(var1, 0);
      if(var1.getContext().getResources().getBoolean(g.a.intercom_composer_keyboard_takes_full_screen_in_landscape) && this.q.a() == 2) {
         this.i.b();
      }

   }

   private void d() {
      if(this.e()) {
         this.b.setVisibility(0);
         this.d.setVisibility(0);
         this.e.setVisibility(0);
      } else {
         this.b.setVisibility(8);
         this.d.setVisibility(8);
         this.e.setVisibility(8);
      }

   }

   private boolean e() {
      Iterator var2 = this.o.iterator();

      boolean var1;
      while(true) {
         if(var2.hasNext()) {
            if(!((com.intercom.composer.b.b)var2.next() instanceof com.intercom.composer.b.c.b)) {
               continue;
            }

            var1 = true;
            break;
         }

         var1 = false;
         break;
      }

      return var1;
   }

   EditText a(com.intercom.composer.b.c.b var1) {
      boolean var4 = false;
      EditText var5 = var1.getEditText();
      List var6 = var1.getOptions();
      this.b.removeAllViews();
      this.a((View)var5);
      LayoutParams var8 = new LayoutParams(0, -2, 1.0F);
      this.b.addView(var5, var8);
      this.d.setOnClickListener(new com.intercom.composer.b.c.a.b(this, var5));
      var5.addTextChangedListener(this.n);
      if(!TextUtils.isEmpty(var5.getText())) {
         var4 = true;
      }

      this.a(var4);
      if(var6 != null) {
         Iterator var10 = var6.iterator();

         while(var10.hasNext()) {
            com.intercom.composer.b.c.b.c var9 = (com.intercom.composer.b.c.b.c)var10.next();
            int var2 = this.getResources().getDimensionPixelSize(g.c.intercom_composer_editable_text_input_option_padding);
            int var3 = this.getResources().getDimensionPixelSize(g.c.intercom_composer_editable_text_input_option_padding_bottom);
            ImageView var7 = new ImageView(this.getContext());
            var7.setImageResource(var9.a());
            var7.setPadding(var2, var2, var2, var3);
            var7.setOnClickListener(new com.intercom.composer.b.c.b.b(var9));
            this.b.addView(var7);
         }
      }

      return var5;
   }

   public void a(int var1, int var2) {
      this.b.setBackgroundResource(var1);
      this.a.setBackgroundResource(var1);
      this.e.setBackgroundResource(var1);
      this.f.setBackgroundResource(var2);
      this.g.setBackgroundResource(var2);
   }

   void a(Context var1, int var2) {
      Drawable var3 = android.support.v4.content.a.a(var1, g.d.intercom_composer_send_background);
      var3.setColorFilter(var2, Mode.SRC_ATOP);
      if(VERSION.SDK_INT >= 16) {
         this.d.setBackground(var3);
      } else {
         this.d.setBackgroundDrawable(var3);
      }

   }

   public void a(w var1) {
      int var2 = var1.getAdapterPosition();
      if(var2 >= 0 && var2 < this.o.size()) {
         this.a((com.intercom.composer.b.b)this.o.get(var2), true, true);
      }

   }

   public void a(com.intercom.composer.a.a var1) {
      this.m = var1;
   }

   public void a(com.intercom.composer.b.b var1, int var2, boolean var3, boolean var4) {
      if(var1 instanceof com.intercom.composer.b.c.b) {
         EditText var5 = this.a((com.intercom.composer.b.c.b)var1);
         this.j.a(var4);
         if(var3) {
            this.a(var5);
         }

         if(!TextUtils.isEmpty(var5.getText())) {
            var3 = true;
         } else {
            var3 = false;
         }

         this.a(var3);
      } else {
         this.i.a();
         this.b.clearFocus();
         this.j.a();
         this.a(false);
      }

      this.a(var1.getBackgroundColor(), var1.getBorderColor());
      this.c.a(var2, false);
   }

   public void a(CharSequence var1) {
      if(this.r != null) {
         this.r.a(var1);
      }

   }

   public void a(boolean var1) {
      if(this.l != null) {
         this.l.a(var1, this.m);
      }

   }

   public boolean a() {
      com.intercom.composer.b.b var1 = this.getSelectedInput();
      if(var1 != null && !var1.equals(this.o.get(0))) {
         this.a((com.intercom.composer.b.b)this.o.get(0), false, false);
      } else {
         this.h.b();
      }

      return this.i.b();
   }

   public boolean a(com.intercom.composer.b.b var1, boolean var2, boolean var3) {
      if(this.k != null) {
         this.k.onInputSelected(var1);
      }

      return this.h.a(var1, var2, var3);
   }

   public void b() {
      this.c();
      this.i.c();
   }

   void c() {
      int var2 = this.b.getChildCount();
      if(var2 > 0) {
         for(int var1 = 0; var1 < var2; ++var1) {
            View var3 = this.b.getChildAt(var1);
            if(var3 instanceof EditText) {
               ((EditText)var3).removeTextChangedListener(this.n);
            }
         }
      }

   }

   public List getInputs() {
      return this.o;
   }

   public com.intercom.composer.b.b getSelectedInput() {
      return this.h.a();
   }

   public int getTextInputHeight() {
      return this.b.getHeight();
   }

   public void setComposerPagerAdapter(com.intercom.composer.pager.a var1) {
      this.c.setAdapter(var1);
      this.c.setOffscreenPageLimit(this.o.size());
      com.intercom.composer.a.h var2 = new com.intercom.composer.a.h(this.o, var1, this.h, this.p, this);
      com.intercom.composer.a.e var3 = new com.intercom.composer.a.e(this.o, var1, this.h, this);
      this.l = new com.intercom.composer.a.f(this.e, this.d, var2, var3);
   }

   public void setEditTextLayoutAnimationListener(com.intercom.composer.a.c var1) {
      this.j.a(var1);
   }

   public void setFragmentManager(n var1) {
      this.h = new com.intercom.composer.b.b.e(LayoutInflater.from(this.getContext()), this.o, this, this, var1);
      this.a.setAdapter(this.h);
   }

   public void setInputSelectedListener(f var1) {
      this.k = var1;
   }

   public void setInputs(List var1) {
      if(this.h == null) {
         throw new IllegalStateException("Fragment manager should be set!");
      } else {
         this.o.clear();
         this.o.addAll(var1);
         this.d();
         this.h.notifyDataSetChanged();
      }
   }

   public void setOnSendButtonClickListener(com.intercom.composer.b.c.a.a var1) {
      this.r = var1;
   }

   public void setSendButtonVisibility(int var1) {
      this.d.setVisibility(var1);
   }
}
