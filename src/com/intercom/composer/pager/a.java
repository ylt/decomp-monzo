package com.intercom.composer.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.n;
import android.support.v4.app.r;
import android.view.ViewGroup;
import com.intercom.composer.b.b;
import java.util.List;

public class a extends r {
   private final List a;

   public a(n var1, List var2) {
      super(var1);
      this.a = var2;
   }

   public Fragment a(int var1) {
      return ((b)this.a.get(var1)).createFragment();
   }

   public Object a(ViewGroup var1, int var2) {
      Fragment var3 = (Fragment)super.a(var1, var2);
      ((b)this.a.get(var2)).setFragmentTag(var3.getTag());
      return var3;
   }

   public int b() {
      return this.a.size();
   }
}
