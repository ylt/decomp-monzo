package com.intercom.composer.pager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class NonSwipeableViewPager extends ViewPager {
   public NonSwipeableViewPager(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public boolean onInterceptTouchEvent(MotionEvent var1) {
      return false;
   }

   public boolean onTouchEvent(MotionEvent var1) {
      return false;
   }
}
