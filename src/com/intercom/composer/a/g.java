package com.intercom.composer.a;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import java.util.Iterator;
import java.util.List;

abstract class g extends AnimatorListenerAdapter {
   protected final List a;
   final com.intercom.composer.pager.a b;
   final android.support.v7.widget.RecyclerView.a c;
   final com.intercom.composer.a d;
   boolean e;

   g(List var1, com.intercom.composer.pager.a var2, android.support.v7.widget.RecyclerView.a var3, com.intercom.composer.a var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   boolean a() {
      Iterator var2 = this.a.iterator();

      boolean var1;
      while(true) {
         if(var2.hasNext()) {
            if(!((com.intercom.composer.b.b)var2.next() instanceof com.intercom.composer.b.a.b)) {
               continue;
            }

            var1 = true;
            break;
         }

         var1 = false;
         break;
      }

      return var1;
   }

   public void onAnimationCancel(Animator var1) {
      super.onAnimationCancel(var1);
      this.e = true;
   }
}
