package com.intercom.composer.a;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;
import android.widget.ImageView;

public class f {
   final View a;
   final ImageView b;
   AnimatorSet c;
   final AnimatorUpdateListener d = new AnimatorUpdateListener() {
      public void onAnimationUpdate(ValueAnimator var1) {
         f.this.a.setAlpha(((Float)var1.getAnimatedValue()).floatValue());
      }
   };
   private final h e;
   private final e f;
   private ObjectAnimator g;
   private ObjectAnimator h;

   public f(View var1, ImageView var2, h var3, e var4) {
      this.a = var1;
      this.b = var2;
      this.e = var3;
      this.f = var4;
      this.g = ObjectAnimator.ofPropertyValuesHolder(var2, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.0F, 1.0F}), PropertyValuesHolder.ofFloat("scaleX", new float[]{0.6F, 1.0F}), PropertyValuesHolder.ofFloat("scaleY", new float[]{0.6F, 1.0F})});
      this.g.setStartDelay(50L);
      this.h = ObjectAnimator.ofPropertyValuesHolder(var2, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{1.0F, 0.0F}), PropertyValuesHolder.ofFloat("scaleX", new float[]{1.0F, 0.6F}), PropertyValuesHolder.ofFloat("scaleY", new float[]{1.0F, 0.6F})});
   }

   public void a(boolean var1, a var2) {
      boolean var4;
      if(!var1 || var2 != a.a && var2 != a.c && var2 != a.e) {
         if(var1 || var2 != a.b && var2 != a.d && var2 != a.e) {
            return;
         }

         var4 = false;
      } else {
         var4 = true;
      }

      if(this.c != null) {
         this.c.cancel();
      }

      this.c = new AnimatorSet();
      float var3;
      if(var4) {
         var3 = 1.0F;
      } else {
         var3 = 0.0F;
      }

      ObjectAnimator var5 = ObjectAnimator.ofFloat(this.a, "alpha", new float[]{var3});
      var5.addUpdateListener(this.d);
      AnimatorSet var6 = this.c;
      ObjectAnimator var7;
      if(var4) {
         var7 = this.g;
      } else {
         var7 = this.h;
      }

      var6.playTogether(new Animator[]{var5, var7});
      this.c.setDuration(100L);
      AnimatorSet var9 = this.c;
      Object var8;
      if(var1) {
         var8 = this.e;
      } else {
         var8 = this.f;
      }

      var9.addListener((AnimatorListener)var8);
      this.c.start();
   }
}
