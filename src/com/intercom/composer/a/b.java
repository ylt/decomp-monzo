package com.intercom.composer.a;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;

public class b {
   a a;
   final AnimatorListener b;
   final AnimatorListener c;
   final AnimatorUpdateListener d;
   private final View e;
   private c f;
   private ObjectAnimator g;

   public b(View var1) {
      this.a = a.d;
      this.b = new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            super.onAnimationEnd(var1);
            b.this.a = a.d;
            if(b.this.f != null) {
               b.this.f.a(a.d);
            }

         }

         public void onAnimationStart(Animator var1) {
            super.onAnimationStart(var1);
            b.this.a = a.b;
         }
      };
      this.c = new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            super.onAnimationEnd(var1);
            b.this.a = a.c;
            if(b.this.f != null) {
               b.this.f.a(a.c);
            }

         }

         public void onAnimationStart(Animator var1) {
            super.onAnimationStart(var1);
            b.this.a = a.a;
         }
      };
      this.d = new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1) {
            ((LayoutParams)b.this.e.getLayoutParams()).bottomMargin = ((Float)var1.getAnimatedValue()).intValue();
            b.this.e.requestLayout();
         }
      };
      this.e = var1;
   }

   public void a() {
      if(this.a == a.b && this.g != null) {
         this.g.cancel();
      }

      if(this.a == a.d) {
         this.g = ObjectAnimator.ofFloat(this.e, "layout_marginBottom", new float[]{0.0F, (float)(-this.e.getHeight())});
         this.g.setDuration(350L);
         this.g.setInterpolator(new android.support.v4.view.b.b());
         this.g.addUpdateListener(this.d);
         this.g.addListener(this.c);
         this.g.start();
      }

   }

   public void a(c var1) {
      this.f = var1;
   }

   public void a(boolean var1) {
      if(this.a == a.a && this.g != null) {
         this.g.cancel();
      }

      if(this.a == a.c) {
         this.g = ObjectAnimator.ofFloat(this.e, "layout_marginBottom", new float[]{(float)(-this.e.getHeight()), 0.0F});
         ObjectAnimator var4 = this.g;
         long var2;
         if(var1) {
            var2 = 350L;
         } else {
            var2 = 0L;
         }

         var4.setDuration(var2);
         this.g.setInterpolator(new android.support.v4.view.b.b());
         this.g.addUpdateListener(this.d);
         this.g.addListener(this.b);
         this.g.start();
      }

   }
}
