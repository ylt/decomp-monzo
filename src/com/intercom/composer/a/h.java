package com.intercom.composer.a;

import android.animation.Animator;
import android.support.v7.widget.LinearLayoutManager;
import java.util.List;

public class h extends g {
   private final LinearLayoutManager f;

   public h(List var1, com.intercom.composer.pager.a var2, android.support.v7.widget.RecyclerView.a var3, LinearLayoutManager var4, com.intercom.composer.a var5) {
      super(var1, var2, var3, var5);
      this.f = var4;
   }

   public void onAnimationEnd(Animator var1) {
      super.onAnimationEnd(var1);
      if(!this.e && !this.a()) {
         this.d.a(a.d);
         this.a.add(new com.intercom.composer.b.a.b());
         this.b.c();
         int var2 = this.a.size() - 1;
         this.c.notifyItemInserted(var2);
         if(this.f.p() == var2 - 1) {
            this.f.e(var2);
         }
      }

   }

   public void onAnimationStart(Animator var1) {
      super.onAnimationStart(var1);
      this.d.a(a.b);
      this.d.setSendButtonVisibility(0);
   }
}
