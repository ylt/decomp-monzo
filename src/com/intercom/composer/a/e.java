package com.intercom.composer.a;

import android.animation.Animator;
import java.util.List;

public class e extends g {
   public e(List var1, com.intercom.composer.pager.a var2, android.support.v7.widget.RecyclerView.a var3, com.intercom.composer.a var4) {
      super(var1, var2, var3, var4);
   }

   public void onAnimationEnd(Animator var1) {
      super.onAnimationEnd(var1);
      this.d.setSendButtonVisibility(8);
      if(!this.e && this.a()) {
         this.d.a(a.c);
         this.a.remove(this.a.size() - 1);
         this.b.c();
         this.c.notifyItemRemoved(this.a.size());
      }

   }

   public void onAnimationStart(Animator var1) {
      super.onAnimationStart(var1);
      this.d.a(a.a);
   }
}
