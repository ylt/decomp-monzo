package com.intercom.composer.b;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

public abstract class c extends Fragment {
   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setRetainInstance(true);
   }

   public abstract void onInputDeselected();

   public abstract void onInputReselected();

   public abstract void onInputSelected();

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      this.passDataOnViewCreated(this.getArguments());
   }

   public void passData(Bundle var1) {
      if(this.getView() != null) {
         this.passDataOnViewCreated(var1);
      } else {
         this.setArguments(var1);
      }

   }

   protected abstract void passDataOnViewCreated(Bundle var1);

   public void setArguments(Bundle var1) {
      Bundle var3 = this.getArguments();
      Bundle var2 = var3;
      if(var3 == null) {
         var2 = new Bundle();
      }

      var2.putAll(var1);
      super.setArguments(var2);
   }
}
