package com.intercom.composer.b.b;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.intercom.composer.g;

class d extends w implements OnClickListener {
   final ImageView a;
   final b b;

   d(View var1, b var2) {
      super(var1);
      this.b = var2;
      this.a = (ImageView)var1.findViewById(g.e.input_icon_image_view);
      this.a.setOnClickListener(this);
      var1.setOnClickListener(this);
   }

   void a(com.intercom.composer.b.b var1, boolean var2) {
      this.a.setImageDrawable(var1.getIconDrawable(this.a.getContext()));
      this.a.setSelected(var2);
   }

   public void onClick(View var1) {
      this.b.a(this);
   }
}
