package com.intercom.composer.b.b;

import android.support.v4.app.n;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.intercom.composer.g;
import java.util.ArrayList;
import java.util.List;

public class e extends android.support.v7.widget.RecyclerView.a {
   private final f a;
   private final LayoutInflater b;
   private List c = new ArrayList();
   private com.intercom.composer.b.b d;
   private final b e;
   private final n f;

   public e(LayoutInflater var1, List var2, f var3, b var4, n var5) {
      this.c = var2;
      this.a = var3;
      this.b = var1;
      this.e = var4;
      this.f = var5;
   }

   private void a(com.intercom.composer.b.b var1) {
      com.intercom.composer.b.c var3;
      if(var1 == this.d) {
         var3 = var1.findFragment(this.f);
         if(var3 != null) {
            var3.onInputReselected();
         }
      } else {
         if(this.d != null) {
            com.intercom.composer.b.c var2 = this.d.findFragment(this.f);
            if(var2 != null) {
               var2.onInputDeselected();
            }
         }

         var3 = var1.findFragment(this.f);
         if(var3 != null) {
            var3.onInputSelected();
         }
      }

   }

   public com.intercom.composer.b.b a() {
      return this.d;
   }

   public boolean a(com.intercom.composer.b.b var1, boolean var2, boolean var3) {
      boolean var4 = false;
      if(this.c.indexOf(var1) != -1) {
         this.a(var1);
         if(var1 != this.d) {
            this.d = var1;
            this.notifyDataSetChanged();
            this.a.a(var1, this.c.indexOf(var1), var2, var3);
            var4 = true;
         }
      }

      return var4;
   }

   public void b() {
      this.d = null;
      this.notifyDataSetChanged();
   }

   public int getItemCount() {
      return this.c.size();
   }

   public int getItemViewType(int var1) {
      byte var2;
      if((com.intercom.composer.b.b)this.c.get(var1) instanceof com.intercom.composer.b.a.b) {
         var2 = 2;
      } else {
         var2 = 1;
      }

      return var2;
   }

   public void onBindViewHolder(w var1, int var2) {
      com.intercom.composer.b.b var4 = (com.intercom.composer.b.b)this.c.get(var2);
      if(var1 instanceof d) {
         d var5 = (d)var1;
         boolean var3;
         if(this.d != null && var4.getUniqueIdentifier().equals(this.d.getUniqueIdentifier())) {
            var3 = true;
         } else {
            var3 = false;
         }

         var5.a(var4, var3);
      }

   }

   public w onCreateViewHolder(ViewGroup var1, int var2) {
      Object var3;
      if(var2 == 2) {
         var3 = new a(this.b.inflate(g.f.intercom_composer_empty_view_layout, var1, false));
      } else {
         var3 = new d(this.b.inflate(g.f.intercom_composer_input_icon_view_layout, var1, false), this.e);
      }

      return (w)var3;
   }
}
