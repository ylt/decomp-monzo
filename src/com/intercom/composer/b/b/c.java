package com.intercom.composer.b.b;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.g;
import android.support.v7.widget.RecyclerView.t;
import android.view.View;

public class c extends g {
   final int a;

   public c(Context var1) {
      this.a = var1.getResources().getDimensionPixelSize(com.intercom.composer.g.c.intercom_composer_icon_bar_left_spacing);
   }

   public void getItemOffsets(Rect var1, View var2, RecyclerView var3, t var4) {
      if(var3.g(var2) == 0) {
         var1.set(this.a, 0, 0, 0);
      }

   }
}
