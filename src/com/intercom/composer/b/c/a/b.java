package com.intercom.composer.b.c.a;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class b implements OnClickListener {
   private final a a;
   private final EditText b;

   public b(a var1, EditText var2) {
      this.a = var1;
      this.b = var2;
   }

   void a() {
      this.b.getText().clear();
   }

   public void onClick(View var1) {
      this.a.a(this.b.getText());
      this.a();
   }
}
