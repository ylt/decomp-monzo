package com.intercom.composer.b.c;

import android.widget.EditText;
import java.util.List;

public abstract class b extends com.intercom.composer.b.b {
   private EditText editText;
   private final List options;
   private a sendTextCallback;

   public b(String var1, com.intercom.composer.b.a var2, a var3) {
      this(var1, var2, var3, (List)null);
   }

   public b(String var1, com.intercom.composer.b.a var2, a var3, List var4) {
      super(var1, var2);
      this.sendTextCallback = var3;
      this.options = var4;
   }

   protected abstract EditText createEditText();

   public EditText getEditText() {
      if(this.editText == null) {
         this.editText = this.createEditText();
      }

      return this.editText;
   }

   public List getOptions() {
      return this.options;
   }

   public void sendTextBack(CharSequence var1) {
      this.sendTextCallback.textToBeSent(this, var1);
   }
}
