package com.intercom.composer.b.a;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.intercom.composer.g;
import com.intercom.composer.b.c;

public class a extends c {
   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(g.f.intercom_composer_fragment_empty, var2, false);
   }

   public void onInputDeselected() {
   }

   public void onInputReselected() {
   }

   public void onInputSelected() {
   }

   protected void passDataOnViewCreated(Bundle var1) {
   }
}
