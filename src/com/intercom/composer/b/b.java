package com.intercom.composer.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.n;
import android.text.TextUtils;
import com.intercom.composer.g;

public abstract class b {
   private String fragmentTag;
   private final a iconProvider;
   private final String uniqueIdentifier;

   protected b(String var1, a var2) {
      this.uniqueIdentifier = var1;
      this.iconProvider = var2;
      if(TextUtils.isEmpty(var1)) {
         throw new IllegalArgumentException("Inputs must have a non-empty unique identifier.");
      }
   }

   public abstract c createFragment();

   public c findFragment(n var1) {
      return (c)var1.a(this.fragmentTag);
   }

   public int getBackgroundColor() {
      return g.b.intercom_composer_white;
   }

   public int getBorderColor() {
      return g.b.intercom_composer_border;
   }

   public Drawable getIconDrawable(Context var1) {
      return this.iconProvider.getIconDrawable(this.uniqueIdentifier, var1);
   }

   public String getUniqueIdentifier() {
      return this.uniqueIdentifier;
   }

   public void setFragmentTag(String var1) {
      this.fragmentTag = var1;
   }
}
