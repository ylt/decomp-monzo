package com.intercom.composer;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Iterator;
import java.util.List;

public class b extends Fragment {
   c a;
   ComposerView b;
   String c;
   com.intercom.composer.b.b d;
   private f e;
   private boolean f;
   private int g;
   private Runnable h = new Runnable() {
      public void run() {
         b.this.a();
      }
   };

   public static b a(String var0, boolean var1, int var2) {
      b var3 = new b();
      Bundle var4 = new Bundle();
      var4.putString("initial_input_identifier", var0);
      var4.putBoolean("show_keyboard_for_initial_input", var1);
      var4.putInt("theme_color", var2);
      var3.setArguments(var4);
      return var3;
   }

   private boolean a(com.intercom.composer.b.b var1) {
      boolean var2;
      if(!(var1 instanceof com.intercom.composer.b.c.b)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private boolean b(com.intercom.composer.b.b var1) {
      boolean var2;
      if(var1 != null) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public com.intercom.composer.b.b a(String var1) {
      com.intercom.composer.b.b var4;
      if(TextUtils.isEmpty(var1)) {
         var4 = null;
      } else {
         Iterator var3 = this.a.getInputs().iterator();

         while(true) {
            if(!var3.hasNext()) {
               var4 = null;
               break;
            }

            com.intercom.composer.b.b var2 = (com.intercom.composer.b.b)var3.next();
            if(var2.getUniqueIdentifier().equals(var1)) {
               var4 = var2;
               break;
            }
         }
      }

      return var4;
   }

   void a() {
      if(this.d == null || !this.b.a(this.d, false, true)) {
         List var3 = this.a.getInputs();
         if(!var3.isEmpty()) {
            com.intercom.composer.b.b var2 = this.a(this.c);
            com.intercom.composer.b.b var1 = var2;
            if(var2 == null) {
               var1 = (com.intercom.composer.b.b)var3.get(0);
            }

            this.b.a(var1, this.f, true);
         }
      }

   }

   public void a(c var1) {
      this.a = var1;
   }

   public void a(f var1) {
      this.e = var1;
   }

   public void a(String var1, boolean var2) {
      com.intercom.composer.b.b var3 = this.a(var1);
      if(var3 != null) {
         this.b.a(var3, var2, true);
      }

   }

   public boolean b() {
      return this.b.a();
   }

   public com.intercom.composer.b.b c() {
      return this.b.getSelectedInput();
   }

   public boolean d() {
      com.intercom.composer.b.b var2 = this.c();
      boolean var1;
      if(this.b(var2) && this.a(var2)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onAttach(Context var1) {
      super.onAttach(var1);
      if(var1 instanceof c) {
         this.a = (c)var1;
      }

      if(var1 instanceof f) {
         this.e = (f)var1;
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setRetainInstance(true);
      this.c = this.getArguments().getString("initial_input_identifier");
      this.f = this.getArguments().getBoolean("show_keyboard_for_initial_input");
      this.g = this.getArguments().getInt("theme_color");
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      this.b = (ComposerView)var1.inflate(g.f.intercom_composer_layout, var2, false);
      this.b.a(this.getContext(), this.g);
      this.b.setFragmentManager(this.getChildFragmentManager());
      this.b.setInputs(this.a.getInputs());
      this.b.setOnSendButtonClickListener(new com.intercom.composer.b.c.a.a() {
         public void a(CharSequence var1) {
            com.intercom.composer.b.b var2 = b.this.b.getSelectedInput();
            if(var2 instanceof com.intercom.composer.b.c.b) {
               ((com.intercom.composer.b.c.b)var2).sendTextBack(var1);
            }

         }
      });
      if(this.e != null) {
         this.b.setInputSelectedListener(this.e);
      }

      com.intercom.composer.pager.a var4 = new com.intercom.composer.pager.a(this.getChildFragmentManager(), this.b.getInputs());
      this.b.setComposerPagerAdapter(var4);
      this.b.setEditTextLayoutAnimationListener(new com.intercom.composer.a.c(this.getActivity()));
      this.b.post(this.h);
      return this.b;
   }

   public void onDestroy() {
      if(this.b != null) {
         this.b.b();
      }

      super.onDestroy();
   }

   public void onDestroyView() {
      this.d = this.b.getSelectedInput();
      super.onDestroyView();
   }
}
