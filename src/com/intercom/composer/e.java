package com.intercom.composer;

import android.graphics.Bitmap;
import android.widget.ImageView;

public interface e {
   void clear(ImageView var1);

   Bitmap getBitmapFromView(ImageView var1);

   void loadImageIntoView(com.intercom.input.gallery.c var1, ImageView var2);
}
