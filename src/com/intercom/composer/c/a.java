package com.intercom.composer.c;

import android.app.Activity;
import android.view.View;
import android.view.Window;

public class a implements c {
   private final b a;
   private final Window b;
   private final View c;
   private final View d;
   private final d e;

   public a(Activity var1, d var2, View var3, View var4) {
      this(new b(var1, var2), var2, var1.getWindow(), var3, var4);
   }

   a(b var1, d var2, Window var3, View var4, View var5) {
      if(var5 == null) {
         throw new IllegalArgumentException("behindKeyboardView can not be null!");
      } else if(var4 == null) {
         throw new IllegalArgumentException("editText can not be null!");
      } else {
         this.c = var4;
         this.d = var5;
         this.b = var3;
         this.b.setSoftInputMode(19);
         this.e = var2;
         this.a = var1;
         this.a.a((c)this);
      }
   }

   private boolean d() {
      boolean var1;
      if(this.d.getLayoutParams().height != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void a() {
      int var1 = this.e.a();
      if(this.a.a()) {
         this.d.getLayoutParams().height = this.a.a(var1);
         this.d.requestLayout();
         this.b.setSoftInputMode(32);
         if(this.a.a()) {
            this.a.a(this.c);
         }
      } else if(!this.d()) {
         this.d.getLayoutParams().height = this.a.a(var1);
         this.d.requestLayout();
         this.b.setSoftInputMode(32);
      }

   }

   public void a(boolean var1, int var2) {
      if(var1) {
         this.b.setSoftInputMode(16);
         if(this.d()) {
            this.d.getLayoutParams().height = 0;
            this.d.requestLayout();
         }
      } else if(this.d()) {
         this.b.setSoftInputMode(32);
      } else {
         this.b.setSoftInputMode(16);
      }

   }

   public boolean b() {
      boolean var1 = false;
      if(this.d()) {
         this.d.getLayoutParams().height = 0;
         this.d.requestLayout();
         this.b.setSoftInputMode(16);
         var1 = true;
      }

      return var1;
   }

   public void c() {
      this.a.b();
   }
}
