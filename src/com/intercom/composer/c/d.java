package com.intercom.composer.c;

import android.content.Context;

public class d {
   private final Context a;

   public d(Context var1) {
      this.a = var1;
   }

   public int a() {
      return this.a.getResources().getConfiguration().orientation;
   }
}
