package com.intercom.composer.c;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.InputMethodManager;
import com.intercom.composer.g;

class b implements OnGlobalLayoutListener, c {
   boolean a;
   int b;
   private final Window c;
   private final WindowManager d;
   private final InputMethodManager e;
   private final SharedPreferences f;
   private final d g;
   private c h;

   b(Activity var1, d var2) {
      this(var1.getWindow(), (WindowManager)var1.getSystemService("window"), (InputMethodManager)var1.getSystemService("input_method"), var1.getSharedPreferences("keyboard", 0), var2);
   }

   b(Window var1, WindowManager var2, InputMethodManager var3, SharedPreferences var4, d var5) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.c.getDecorView().getRootView().getViewTreeObserver().addOnGlobalLayoutListener(this);
   }

   private int c() {
      View var3 = this.c.getDecorView();
      Rect var4 = new Rect();
      var3.getWindowVisibleDisplayFrame(var4);
      int var2 = var3.getRootView().getHeight() - var4.bottom;
      int var1 = var2;
      if(VERSION.SDK_INT >= 18) {
         var1 = var2 - this.d();
      }

      return var1;
   }

   @TargetApi(18)
   private int d() {
      DisplayMetrics var3 = new DisplayMetrics();
      Display var4 = this.d.getDefaultDisplay();
      var4.getMetrics(var3);
      int var1 = var3.heightPixels;
      var4.getRealMetrics(var3);
      int var2 = var3.heightPixels;
      if(var2 > var1) {
         var1 = var2 - var1;
      } else {
         var1 = 0;
      }

      return var1;
   }

   int a(int var1) {
      Resources var2 = this.c.getContext().getResources();
      if(var1 == 1) {
         var1 = var2.getDimensionPixelSize(g.c.intercom_composer_keyboard_portrait_height);
         var1 = this.f.getInt("keyboard_height_portrait", var1);
      } else {
         var1 = var2.getDimensionPixelSize(g.c.intercom_composer_keyboard_landscape_height);
      }

      return var1;
   }

   void a(View var1) {
      this.e.hideSoftInputFromWindow(var1.getWindowToken(), 0);
   }

   void a(c var1) {
      this.h = var1;
   }

   public void a(boolean var1, int var2) {
      if(var1 && this.g.a() == 1) {
         this.f.edit().putInt("keyboard_height_portrait", var2).apply();
      }

   }

   boolean a() {
      boolean var1;
      if(this.c() > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   void b() {
      if(this.c != null) {
         ViewTreeObserver var1 = this.c.getDecorView().getRootView().getViewTreeObserver();
         if(VERSION.SDK_INT >= 16) {
            var1.removeOnGlobalLayoutListener(this);
         } else {
            var1.removeGlobalOnLayoutListener(this);
         }
      }

   }

   public void onGlobalLayout() {
      int var2 = this.c();
      boolean var3;
      if(var2 > 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      int var1 = this.g.a();
      if(var3 != this.a || var1 != this.b) {
         this.a = var3;
         this.b = var1;
         this.a(var3, var2);
         if(this.h != null) {
            this.h.a(var3, var2);
         }
      }

   }
}
