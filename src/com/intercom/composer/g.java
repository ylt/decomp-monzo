package com.intercom.composer;

public final class g {
   public static final class a {
      public static final int intercom_composer_keyboard_takes_full_screen_in_landscape = 2131623937;
   }

   public static final class b {
      public static final int intercom_composer_blue = 2131689599;
      public static final int intercom_composer_border = 2131689600;
      public static final int intercom_composer_semi_transparent_black = 2131689606;
      public static final int intercom_composer_white = 2131689610;
   }

   public static final class c {
      public static final int intercom_composer_editable_text_input_option_padding = 2131427554;
      public static final int intercom_composer_editable_text_input_option_padding_bottom = 2131427555;
      public static final int intercom_composer_icon_bar_height = 2131427558;
      public static final int intercom_composer_icon_bar_left_spacing = 2131427559;
      public static final int intercom_composer_keyboard_landscape_height = 2131427560;
      public static final int intercom_composer_keyboard_portrait_height = 2131427561;
      public static final int intercom_composer_send_button_fading_background_width = 2131427563;
   }

   public static final class d {
      public static final int intercom_composer_ic_send = 2130837910;
      public static final int intercom_composer_send_background = 2130837912;
   }

   public static final class e {
      public static final int composer_edit_text_layout = 2131821481;
      public static final int composer_input_icons_recycler_view = 2131821482;
      public static final int composer_lower_border = 2131821485;
      public static final int composer_upper_border = 2131821480;
      public static final int composer_view_pager = 2131821486;
      public static final int input_icon_image_view = 2131821479;
      public static final int send_button = 2131821484;
      public static final int send_button_fading_background = 2131821483;
   }

   public static final class f {
      public static final int intercom_composer_empty_view_layout = 2131034309;
      public static final int intercom_composer_fragment_empty = 2131034313;
      public static final int intercom_composer_input_icon_view_layout = 2131034317;
      public static final int intercom_composer_layout = 2131034318;
      public static final int intercom_composer_view_layout = 2131034320;
   }
}
