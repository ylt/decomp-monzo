package com.intercom.composer.d;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

public class b implements TextWatcher {
   private final a a;

   public b(a var1) {
      this.a = var1;
   }

   public void afterTextChanged(Editable var1) {
      a var3 = this.a;
      boolean var2;
      if(!TextUtils.isEmpty(var1.toString().trim())) {
         var2 = true;
      } else {
         var2 = false;
      }

      var3.a(var2);
   }

   public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
   }

   public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
   }
}
