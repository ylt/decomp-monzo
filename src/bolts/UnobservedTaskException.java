package bolts;

public class UnobservedTaskException extends RuntimeException {
   public UnobservedTaskException(Throwable var1) {
      super(var1);
   }
}
