package bolts;

import android.content.Intent;
import android.os.Bundle;

public final class c {
   public static Bundle a(Intent var0) {
      return var0.getBundleExtra("al_applink_data");
   }

   public static Bundle b(Intent var0) {
      Bundle var1 = a(var0);
      if(var1 == null) {
         var1 = null;
      } else {
         var1 = var1.getBundle("extras");
      }

      return var1;
   }
}
