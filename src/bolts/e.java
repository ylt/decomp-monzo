package bolts;

import java.util.Locale;

public class e {
   private final g a;

   public boolean a() {
      return this.a.a();
   }

   public String toString() {
      return String.format(Locale.US, "%s@%s[cancellationRequested=%s]", new Object[]{this.getClass().getName(), Integer.toHexString(this.hashCode()), Boolean.toString(this.a.a())});
   }
}
