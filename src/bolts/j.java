package bolts;

public class j {
   private final i a = new i();

   public i a() {
      return this.a;
   }

   public boolean a(Exception var1) {
      return this.a.b(var1);
   }

   public boolean a(Object var1) {
      return this.a.b(var1);
   }

   public void b(Exception var1) {
      if(!this.a(var1)) {
         throw new IllegalStateException("Cannot set the error on a completed task.");
      }
   }

   public void b(Object var1) {
      if(!this.a(var1)) {
         throw new IllegalStateException("Cannot set the result of a completed task.");
      }
   }

   public boolean b() {
      return this.a.i();
   }

   public void c() {
      if(!this.b()) {
         throw new IllegalStateException("Cannot cancel a completed task.");
      }
   }
}
