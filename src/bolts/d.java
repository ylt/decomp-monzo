package bolts;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

final class d {
   private static final d a = new d();
   private final ExecutorService b;
   private final ScheduledExecutorService c;
   private final Executor d;

   private d() {
      ExecutorService var1;
      if(!c()) {
         var1 = Executors.newCachedThreadPool();
      } else {
         var1 = a.a();
      }

      this.b = var1;
      this.c = Executors.newSingleThreadScheduledExecutor();
      this.d = new d.a();
   }

   public static ExecutorService a() {
      return a.b;
   }

   static Executor b() {
      return a.d;
   }

   private static boolean c() {
      String var1 = System.getProperty("java.runtime.name");
      boolean var0;
      if(var1 == null) {
         var0 = false;
      } else {
         var0 = var1.toLowerCase(Locale.US).contains("android");
      }

      return var0;
   }

   private static class a implements Executor {
      private ThreadLocal a;

      private a() {
         this.a = new ThreadLocal();
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      private int a() {
         Integer var3 = (Integer)this.a.get();
         Integer var2 = var3;
         if(var3 == null) {
            var2 = Integer.valueOf(0);
         }

         int var1 = var2.intValue() + 1;
         this.a.set(Integer.valueOf(var1));
         return var1;
      }

      private int b() {
         Integer var3 = (Integer)this.a.get();
         Integer var2 = var3;
         if(var3 == null) {
            var2 = Integer.valueOf(0);
         }

         int var1 = var2.intValue() - 1;
         if(var1 == 0) {
            this.a.remove();
         } else {
            this.a.set(Integer.valueOf(var1));
         }

         return var1;
      }

      public void execute(Runnable param1) {
         // $FF: Couldn't be decompiled
      }
   }
}
