package bolts;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Looper;
import android.os.Build.VERSION;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

final class a {
   static final int a;
   static final int b;
   private static final a c = new a();
   private static final int e = Runtime.getRuntime().availableProcessors();
   private final Executor d = new a.a();

   static {
      a = e + 1;
      b = e * 2 + 1;
   }

   public static ExecutorService a() {
      ThreadPoolExecutor var0 = new ThreadPoolExecutor(a, b, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue());
      a(var0, true);
      return var0;
   }

   @SuppressLint({"NewApi"})
   public static void a(ThreadPoolExecutor var0, boolean var1) {
      if(VERSION.SDK_INT >= 9) {
         var0.allowCoreThreadTimeOut(var1);
      }

   }

   public static Executor b() {
      return c.d;
   }

   private static class a implements Executor {
      private a() {
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      public void execute(Runnable var1) {
         (new Handler(Looper.getMainLooper())).post(var1);
      }
   }
}
