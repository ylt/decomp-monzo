package bolts;

import java.io.Closeable;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ScheduledFuture;

public class g implements Closeable {
   private final Object a;
   private final List b;
   private ScheduledFuture c;
   private boolean d;
   private boolean e;

   private void b() {
      if(this.e) {
         throw new IllegalStateException("Object already closed");
      }
   }

   private void c() {
      if(this.c != null) {
         this.c.cancel(true);
         this.c = null;
      }

   }

   void a(f param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean a() {
      // $FF: Couldn't be decompiled
   }

   public void close() {
      // $FF: Couldn't be decompiled
   }

   public String toString() {
      return String.format(Locale.US, "%s@%s[cancellationRequested=%s]", new Object[]{this.getClass().getName(), Integer.toHexString(this.hashCode()), Boolean.toString(this.a())});
   }
}
