package bolts;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

public class AggregateException extends Exception {
   private List a;

   public void printStackTrace(PrintStream var1) {
      super.printStackTrace(var1);
      Iterator var3 = this.a.iterator();
      int var2 = -1;

      while(var3.hasNext()) {
         Throwable var4 = (Throwable)var3.next();
         var1.append("\n");
         var1.append("  Inner throwable #");
         ++var2;
         var1.append(Integer.toString(var2));
         var1.append(": ");
         var4.printStackTrace(var1);
         var1.append("\n");
      }

   }

   public void printStackTrace(PrintWriter var1) {
      super.printStackTrace(var1);
      Iterator var4 = this.a.iterator();
      int var2 = -1;

      while(var4.hasNext()) {
         Throwable var3 = (Throwable)var4.next();
         var1.append("\n");
         var1.append("  Inner throwable #");
         ++var2;
         var1.append(Integer.toString(var2));
         var1.append(": ");
         var3.printStackTrace(var1);
         var1.append("\n");
      }

   }
}
