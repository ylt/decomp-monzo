package bolts;

import android.net.Uri;
import java.util.Collections;
import java.util.List;

public class b {
   private Uri a;
   private List b;
   private Uri c;

   public b(Uri var1, List var2, Uri var3) {
      this.a = var1;
      List var4 = var2;
      if(var2 == null) {
         var4 = Collections.emptyList();
      }

      this.b = var4;
      this.c = var3;
   }

   public static class a {
      private final Uri a;
      private final String b;
      private final String c;
      private final String d;

      public a(String var1, String var2, Uri var3, String var4) {
         this.b = var1;
         this.c = var2;
         this.a = var3;
         this.d = var4;
      }
   }
}
