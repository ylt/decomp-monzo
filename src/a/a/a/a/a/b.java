package a.a.a.a.a;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.g;
import android.support.v7.widget.RecyclerView.t;
import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import java.util.HashMap;
import java.util.Map;

public class b extends g {
   private Map a;
   private a b;
   private boolean c;

   public b(a var1) {
      this(var1, false);
   }

   public b(a var1, boolean var2) {
      this.b = var1;
      this.a = new HashMap();
      this.c = var2;
   }

   private int a(RecyclerView var1, View var2, View var3, int var4, int var5) {
      int var8 = this.a(var3);
      int var7 = (int)var2.getY() - var8;
      int var6 = var7;
      if(var5 == 0) {
         var5 = var1.getChildCount();
         long var9 = this.b.a(var4);

         for(var4 = 1; var4 < var5; ++var4) {
            var6 = var1.f(var1.getChildAt(var4));
            if(var6 != -1 && this.b.a(var6) != var9) {
               var6 = (int)var1.getChildAt(var4).getY() - (var8 + this.a(var1, var6).itemView.getHeight());
               if(var6 < 0) {
                  return var6;
               }
               break;
            }
         }

         var6 = Math.max(0, var7);
      }

      return var6;
   }

   private int a(View var1) {
      int var2;
      if(this.c) {
         var2 = 0;
      } else {
         var2 = var1.getHeight();
      }

      return var2;
   }

   private w a(RecyclerView var1, int var2) {
      long var4 = this.b.a(var2);
      w var8;
      if(this.a.containsKey(Long.valueOf(var4))) {
         var8 = (w)this.a.get(Long.valueOf(var4));
      } else {
         w var6 = this.b.a(var1);
         View var7 = var6.itemView;
         this.b.a(var6, var2);
         var2 = MeasureSpec.makeMeasureSpec(var1.getMeasuredWidth(), 1073741824);
         int var3 = MeasureSpec.makeMeasureSpec(var1.getMeasuredHeight(), 0);
         var7.measure(ViewGroup.getChildMeasureSpec(var2, var1.getPaddingLeft() + var1.getPaddingRight(), var7.getLayoutParams().width), ViewGroup.getChildMeasureSpec(var3, var1.getPaddingTop() + var1.getPaddingBottom(), var7.getLayoutParams().height));
         var7.layout(0, 0, var7.getMeasuredWidth(), var7.getMeasuredHeight());
         this.a.put(Long.valueOf(var4), var6);
         var8 = var6;
      }

      return var8;
   }

   private boolean a(int var1) {
      boolean var2 = true;
      if(var1 != 0 && this.b.a(var1 - 1) == this.b.a(var1)) {
         var2 = false;
      }

      return var2;
   }

   private boolean b(int var1) {
      boolean var2;
      if(this.b.a(var1) != -1L) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void getItemOffsets(Rect var1, View var2, RecyclerView var3, t var4) {
      int var5 = var3.f(var2);
      if(var5 != -1 && this.b(var5) && this.a(var5)) {
         var5 = this.a(this.a(var3, var5).itemView);
      } else {
         var5 = 0;
      }

      var1.set(0, var5, 0, 0);
   }

   public void onDrawOver(Canvas var1, RecyclerView var2, t var3) {
      int var5 = var2.getChildCount();
      long var10 = -1L;

      long var8;
      for(int var4 = 0; var4 < var5; var10 = var8) {
         View var15 = var2.getChildAt(var4);
         int var7 = var2.f(var15);
         var8 = var10;
         if(var7 != -1) {
            var8 = var10;
            if(this.b(var7)) {
               long var12 = this.b.a(var7);
               var8 = var10;
               if(var12 != var10) {
                  View var14 = this.a(var2, var7).itemView;
                  var1.save();
                  int var6 = var15.getLeft();
                  var7 = this.a(var2, var15, var14, var7, var4);
                  var1.translate((float)var6, (float)var7);
                  var14.setTranslationX((float)var6);
                  var14.setTranslationY((float)var7);
                  var14.draw(var1);
                  var1.restore();
                  var8 = var12;
               }
            }
         }

         ++var4;
      }

   }
}
