.class public final Ld/a/a;
.super Ljava/lang/Object;
.source "Timber.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/a/a$a;,
        Ld/a/a$b;
    }
.end annotation


# static fields
.field static volatile a:[Ld/a/a$b;

.field private static final b:[Ld/a/a$b;

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ld/a/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ld/a/a$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    new-array v0, v0, [Ld/a/a$b;

    sput-object v0, Ld/a/a;->b:[Ld/a/a$b;

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ld/a/a;->c:Ljava/util/List;

    .line 209
    sget-object v0, Ld/a/a;->b:[Ld/a/a$b;

    sput-object v0, Ld/a/a;->a:[Ld/a/a$b;

    .line 212
    new-instance v0, Ld/a/a$1;

    invoke-direct {v0}, Ld/a/a$1;-><init>()V

    sput-object v0, Ld/a/a;->d:Ld/a/a$b;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static a(Ld/a/a$b;)V
    .locals 3

    .prologue
    .line 144
    if-nez p0, :cond_0

    .line 145
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "tree == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_0
    sget-object v0, Ld/a/a;->d:Ld/a/a$b;

    if-ne p0, v0, :cond_1

    .line 148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot plant Timber into itself."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_1
    sget-object v1, Ld/a/a;->c:Ljava/util/List;

    monitor-enter v1

    .line 151
    :try_start_0
    sget-object v0, Ld/a/a;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    sget-object v0, Ld/a/a;->c:Ljava/util/List;

    sget-object v2, Ld/a/a;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ld/a/a$b;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ld/a/a$b;

    sput-object v0, Ld/a/a;->a:[Ld/a/a$b;

    .line 153
    monitor-exit v1

    .line 154
    return-void

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ld/a/a;->d:Ld/a/a$b;

    invoke-virtual {v0, p0, p1}, Ld/a/a$b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 91
    sget-object v0, Ld/a/a;->d:Ld/a/a$b;

    invoke-virtual {v0, p0}, Ld/a/a$b;->a(Ljava/lang/Throwable;)V

    .line 92
    return-void
.end method

.method public static varargs a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Ld/a/a;->d:Ld/a/a$b;

    invoke-virtual {v0, p0, p1, p2}, Ld/a/a$b;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    return-void
.end method

.method public static varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Ld/a/a;->d:Ld/a/a$b;

    invoke-virtual {v0, p0, p1}, Ld/a/a$b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    return-void
.end method

.method public static varargs c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Ld/a/a;->d:Ld/a/a$b;

    invoke-virtual {v0, p0, p1}, Ld/a/a$b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    return-void
.end method

.method public static varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 81
    sget-object v0, Ld/a/a;->d:Ld/a/a$b;

    invoke-virtual {v0, p0, p1}, Ld/a/a$b;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    return-void
.end method
