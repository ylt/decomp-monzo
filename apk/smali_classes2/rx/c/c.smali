.class public final Lrx/c/c;
.super Ljava/lang/Object;
.source "RxJavaHooks.java"


# static fields
.field static volatile a:Lrx/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/b",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field static volatile b:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/d$a;",
            "Lrx/d$a;",
            ">;"
        }
    .end annotation
.end field

.field static volatile c:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/h$a;",
            "Lrx/h$a;",
            ">;"
        }
    .end annotation
.end field

.field static volatile d:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/a$a;",
            "Lrx/a$a;",
            ">;"
        }
    .end annotation
.end field

.field static volatile e:Lrx/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/d",
            "<",
            "Lrx/d;",
            "Lrx/d$a;",
            "Lrx/d$a;",
            ">;"
        }
    .end annotation
.end field

.field static volatile f:Lrx/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/d",
            "<",
            "Lrx/h;",
            "Lrx/h$a;",
            "Lrx/h$a;",
            ">;"
        }
    .end annotation
.end field

.field static volatile g:Lrx/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/d",
            "<",
            "Lrx/a;",
            "Lrx/a$a;",
            "Lrx/a$a;",
            ">;"
        }
    .end annotation
.end field

.field static volatile h:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/a/a;",
            "Lrx/a/a;",
            ">;"
        }
    .end annotation
.end field

.field static volatile i:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/k;",
            "Lrx/k;",
            ">;"
        }
    .end annotation
.end field

.field static volatile j:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/k;",
            "Lrx/k;",
            ">;"
        }
    .end annotation
.end field

.field static volatile k:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field static volatile l:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field static volatile m:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field static volatile n:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/d$b;",
            "Lrx/d$b;",
            ">;"
        }
    .end annotation
.end field

.field static volatile o:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/d$b;",
            "Lrx/d$b;",
            ">;"
        }
    .end annotation
.end field

.field static volatile p:Lrx/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/c",
            "<",
            "Lrx/a$b;",
            "Lrx/a$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 87
    invoke-static {}, Lrx/c/c;->a()V

    .line 88
    return-void
.end method

.method public static a(Lrx/a$a;)Lrx/a$a;
    .locals 1

    .prologue
    .line 363
    sget-object v0, Lrx/c/c;->d:Lrx/a/c;

    .line 364
    if-eqz v0, :cond_0

    .line 365
    invoke-interface {v0, p0}, Lrx/a/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/a$a;

    .line 367
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lrx/a/a;)Lrx/a/a;
    .locals 1

    .prologue
    .line 416
    sget-object v0, Lrx/c/c;->h:Lrx/a/c;

    .line 417
    if-eqz v0, :cond_0

    .line 418
    invoke-interface {v0, p0}, Lrx/a/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/a/a;

    .line 420
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lrx/d$a;)Lrx/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/d$a",
            "<TT;>;)",
            "Lrx/d$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 335
    sget-object v0, Lrx/c/c;->b:Lrx/a/c;

    .line 336
    if-eqz v0, :cond_0

    .line 337
    invoke-interface {v0, p0}, Lrx/a/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/d$a;

    .line 339
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lrx/d;Lrx/d$a;)Lrx/d$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/d",
            "<TT;>;",
            "Lrx/d$a",
            "<TT;>;)",
            "Lrx/d$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 432
    sget-object v0, Lrx/c/c;->e:Lrx/a/d;

    .line 433
    if-eqz v0, :cond_0

    .line 434
    invoke-interface {v0, p0, p1}, Lrx/a/d;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/d$a;

    .line 436
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public static a(Lrx/d$b;)Lrx/d$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/d$b",
            "<TR;TT;>;)",
            "Lrx/d$b",
            "<TR;TT;>;"
        }
    .end annotation

    .prologue
    .line 532
    sget-object v0, Lrx/c/c;->o:Lrx/a/c;

    .line 533
    if-eqz v0, :cond_0

    .line 534
    invoke-interface {v0, p0}, Lrx/a/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/d$b;

    .line 536
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lrx/h$a;)Lrx/h$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/h$a",
            "<TT;>;)",
            "Lrx/h$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 350
    sget-object v0, Lrx/c/c;->c:Lrx/a/c;

    .line 351
    if-eqz v0, :cond_0

    .line 352
    invoke-interface {v0, p0}, Lrx/a/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/h$a;

    .line 354
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lrx/k;)Lrx/k;
    .locals 1

    .prologue
    .line 445
    sget-object v0, Lrx/c/c;->i:Lrx/a/c;

    .line 446
    if-eqz v0, :cond_0

    .line 447
    invoke-interface {v0, p0}, Lrx/a/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/k;

    .line 449
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method static a()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lrx/c/c$1;

    invoke-direct {v0}, Lrx/c/c$1;-><init>()V

    sput-object v0, Lrx/c/c;->a:Lrx/a/b;

    .line 108
    new-instance v0, Lrx/c/c$9;

    invoke-direct {v0}, Lrx/c/c$9;-><init>()V

    sput-object v0, Lrx/c/c;->e:Lrx/a/d;

    .line 115
    new-instance v0, Lrx/c/c$10;

    invoke-direct {v0}, Lrx/c/c$10;-><init>()V

    sput-object v0, Lrx/c/c;->i:Lrx/a/c;

    .line 122
    new-instance v0, Lrx/c/c$11;

    invoke-direct {v0}, Lrx/c/c$11;-><init>()V

    sput-object v0, Lrx/c/c;->f:Lrx/a/d;

    .line 137
    new-instance v0, Lrx/c/c$12;

    invoke-direct {v0}, Lrx/c/c$12;-><init>()V

    sput-object v0, Lrx/c/c;->j:Lrx/a/c;

    .line 144
    new-instance v0, Lrx/c/c$13;

    invoke-direct {v0}, Lrx/c/c$13;-><init>()V

    sput-object v0, Lrx/c/c;->g:Lrx/a/d;

    .line 151
    new-instance v0, Lrx/c/c$14;

    invoke-direct {v0}, Lrx/c/c$14;-><init>()V

    sput-object v0, Lrx/c/c;->h:Lrx/a/c;

    .line 158
    new-instance v0, Lrx/c/c$15;

    invoke-direct {v0}, Lrx/c/c$15;-><init>()V

    sput-object v0, Lrx/c/c;->k:Lrx/a/c;

    .line 165
    new-instance v0, Lrx/c/c$16;

    invoke-direct {v0}, Lrx/c/c$16;-><init>()V

    sput-object v0, Lrx/c/c;->n:Lrx/a/c;

    .line 172
    new-instance v0, Lrx/c/c$2;

    invoke-direct {v0}, Lrx/c/c$2;-><init>()V

    sput-object v0, Lrx/c/c;->l:Lrx/a/c;

    .line 179
    new-instance v0, Lrx/c/c$3;

    invoke-direct {v0}, Lrx/c/c$3;-><init>()V

    sput-object v0, Lrx/c/c;->o:Lrx/a/c;

    .line 186
    new-instance v0, Lrx/c/c$4;

    invoke-direct {v0}, Lrx/c/c$4;-><init>()V

    sput-object v0, Lrx/c/c;->m:Lrx/a/c;

    .line 193
    new-instance v0, Lrx/c/c$5;

    invoke-direct {v0}, Lrx/c/c$5;-><init>()V

    sput-object v0, Lrx/c/c;->p:Lrx/a/c;

    .line 200
    invoke-static {}, Lrx/c/c;->b()V

    .line 201
    return-void
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 301
    sget-object v0, Lrx/c/c;->a:Lrx/a/b;

    .line 302
    if-eqz v0, :cond_0

    .line 304
    :try_start_0
    invoke-interface {v0, p0}, Lrx/a/b;->call(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v0

    .line 312
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The onError handler threw an Exception. It shouldn\'t. => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 313
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 315
    invoke-static {v0}, Lrx/c/c;->b(Ljava/lang/Throwable;)V

    .line 318
    :cond_0
    invoke-static {p0}, Lrx/c/c;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static b()V
    .locals 1

    .prologue
    .line 205
    new-instance v0, Lrx/c/c$6;

    invoke-direct {v0}, Lrx/c/c$6;-><init>()V

    sput-object v0, Lrx/c/c;->b:Lrx/a/c;

    .line 212
    new-instance v0, Lrx/c/c$7;

    invoke-direct {v0}, Lrx/c/c$7;-><init>()V

    sput-object v0, Lrx/c/c;->c:Lrx/a/c;

    .line 219
    new-instance v0, Lrx/c/c$8;

    invoke-direct {v0}, Lrx/c/c$8;-><init>()V

    sput-object v0, Lrx/c/c;->d:Lrx/a/c;

    .line 225
    return-void
.end method

.method static b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 322
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    .line 324
    invoke-interface {v1, v0, p0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 325
    return-void
.end method

.method public static c(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 458
    sget-object v0, Lrx/c/c;->k:Lrx/a/c;

    .line 459
    if-eqz v0, :cond_0

    .line 460
    invoke-interface {v0, p0}, Lrx/a/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 462
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method
