.class public final enum Lrx/b/d/b;
.super Ljava/lang/Enum;
.source "Unsubscribed.java"

# interfaces
.implements Lrx/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lrx/b/d/b;",
        ">;",
        "Lrx/k;"
    }
.end annotation


# static fields
.field public static final enum a:Lrx/b/d/b;

.field private static final synthetic b:[Lrx/b/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    new-instance v0, Lrx/b/d/b;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lrx/b/d/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lrx/b/d/b;->a:Lrx/b/d/b;

    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Lrx/b/d/b;

    sget-object v1, Lrx/b/d/b;->a:Lrx/b/d/b;

    aput-object v1, v0, v2

    sput-object v0, Lrx/b/d/b;->b:[Lrx/b/d/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lrx/b/d/b;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lrx/b/d/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lrx/b/d/b;

    return-object v0
.end method

.method public static values()[Lrx/b/d/b;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lrx/b/d/b;->b:[Lrx/b/d/b;

    invoke-virtual {v0}, [Lrx/b/d/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lrx/b/d/b;

    return-object v0
.end method


# virtual methods
.method public isUnsubscribed()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    return v0
.end method

.method public unsubscribe()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method
