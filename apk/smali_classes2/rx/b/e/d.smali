.class public final Lrx/b/e/d;
.super Lrx/d;
.source "ScalarSynchronousObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/b/e/d$b;,
        Lrx/b/e/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/d",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final c:Z


# instance fields
.field final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "rx.just.strong-mode"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lrx/b/e/d;->c:Z

    .line 49
    return-void
.end method


# virtual methods
.method public b(Lrx/g;)Lrx/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/g;",
            ")",
            "Lrx/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 97
    instance-of v0, p1, Lrx/b/c/a;

    if-eqz v0, :cond_0

    .line 98
    check-cast p1, Lrx/b/c/a;

    .line 99
    new-instance v0, Lrx/b/e/d$1;

    invoke-direct {v0, p0, p1}, Lrx/b/e/d$1;-><init>(Lrx/b/e/d;Lrx/b/c/a;)V

    .line 125
    :goto_0
    new-instance v1, Lrx/b/e/d$a;

    iget-object v2, p0, Lrx/b/e/d;->b:Ljava/lang/Object;

    invoke-direct {v1, v2, v0}, Lrx/b/e/d$a;-><init>(Ljava/lang/Object;Lrx/a/c;)V

    invoke-static {v1}, Lrx/b/e/d;->b(Lrx/d$a;)Lrx/d;

    move-result-object v0

    return-object v0

    .line 106
    :cond_0
    new-instance v0, Lrx/b/e/d$2;

    invoke-direct {v0, p0, p1}, Lrx/b/e/d$2;-><init>(Lrx/b/e/d;Lrx/g;)V

    goto :goto_0
.end method
