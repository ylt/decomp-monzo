.class final Lrx/b/c/c$b;
.super Ljava/util/concurrent/atomic/AtomicBoolean;
.source "ScheduledAction.java"

# interfaces
.implements Lrx/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lrx/b/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation


# instance fields
.field final a:Lrx/b/c/c;

.field final b:Lrx/b/e/e;


# direct methods
.method public constructor <init>(Lrx/b/c/c;Lrx/b/e/e;)V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 184
    iput-object p1, p0, Lrx/b/c/c$b;->a:Lrx/b/c/c;

    .line 185
    iput-object p2, p0, Lrx/b/c/c$b;->b:Lrx/b/e/e;

    .line 186
    return-void
.end method


# virtual methods
.method public isUnsubscribed()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lrx/b/c/c$b;->a:Lrx/b/c/c;

    invoke-virtual {v0}, Lrx/b/c/c;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public unsubscribe()V
    .locals 2

    .prologue
    .line 195
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lrx/b/c/c$b;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lrx/b/c/c$b;->b:Lrx/b/e/e;

    iget-object v1, p0, Lrx/b/c/c$b;->a:Lrx/b/c/c;

    invoke-virtual {v0, v1}, Lrx/b/e/e;->b(Lrx/k;)V

    .line 198
    :cond_0
    return-void
.end method
