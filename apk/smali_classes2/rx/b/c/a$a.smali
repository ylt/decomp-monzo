.class final Lrx/b/c/a$a;
.super Lrx/g$a;
.source "EventLoopsScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lrx/b/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:Lrx/b/e/e;

.field private final b:Lrx/d/a;

.field private final c:Lrx/b/e/e;

.field private final d:Lrx/b/c/a$c;


# direct methods
.method constructor <init>(Lrx/b/c/a$c;)V
    .locals 4

    .prologue
    .line 145
    invoke-direct {p0}, Lrx/g$a;-><init>()V

    .line 140
    new-instance v0, Lrx/b/e/e;

    invoke-direct {v0}, Lrx/b/e/e;-><init>()V

    iput-object v0, p0, Lrx/b/c/a$a;->a:Lrx/b/e/e;

    .line 141
    new-instance v0, Lrx/d/a;

    invoke-direct {v0}, Lrx/d/a;-><init>()V

    iput-object v0, p0, Lrx/b/c/a$a;->b:Lrx/d/a;

    .line 142
    new-instance v0, Lrx/b/e/e;

    const/4 v1, 0x2

    new-array v1, v1, [Lrx/k;

    const/4 v2, 0x0

    iget-object v3, p0, Lrx/b/c/a$a;->a:Lrx/b/e/e;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lrx/b/c/a$a;->b:Lrx/d/a;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lrx/b/e/e;-><init>([Lrx/k;)V

    iput-object v0, p0, Lrx/b/c/a$a;->c:Lrx/b/e/e;

    .line 146
    iput-object p1, p0, Lrx/b/c/a$a;->d:Lrx/b/c/a$c;

    .line 148
    return-void
.end method


# virtual methods
.method public a(Lrx/a/a;)Lrx/k;
    .locals 6

    .prologue
    .line 162
    invoke-virtual {p0}, Lrx/b/c/a$a;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    invoke-static {}, Lrx/d/c;->a()Lrx/k;

    move-result-object v0

    .line 166
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lrx/b/c/a$a;->d:Lrx/b/c/a$c;

    new-instance v1, Lrx/b/c/a$a$1;

    invoke-direct {v1, p0, p1}, Lrx/b/c/a$a$1;-><init>(Lrx/b/c/a$a;Lrx/a/a;)V

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lrx/b/c/a$a;->a:Lrx/b/e/e;

    invoke-virtual/range {v0 .. v5}, Lrx/b/c/a$c;->a(Lrx/a/a;JLjava/util/concurrent/TimeUnit;Lrx/b/e/e;)Lrx/b/c/c;

    move-result-object v0

    goto :goto_0
.end method

.method public isUnsubscribed()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lrx/b/c/a$a;->c:Lrx/b/e/e;

    invoke-virtual {v0}, Lrx/b/e/e;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public unsubscribe()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lrx/b/c/a$a;->c:Lrx/b/e/e;

    invoke-virtual {v0}, Lrx/b/e/e;->unsubscribe()V

    .line 153
    return-void
.end method
