.class public final Lrx/b/c/a;
.super Lrx/g;
.source "EventLoopsScheduler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/b/c/a$c;,
        Lrx/b/c/a$a;,
        Lrx/b/c/a$b;
    }
.end annotation


# static fields
.field static final a:I

.field static final b:Lrx/b/c/a$c;

.field static final c:Lrx/b/c/a$b;


# instance fields
.field final d:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lrx/b/c/a$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    const-string v0, "rx.scheduler.max-computation-threads"

    invoke-static {v0, v2}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 36
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    .line 38
    if-lez v0, :cond_0

    if-le v0, v1, :cond_1

    :cond_0
    move v0, v1

    .line 43
    :cond_1
    sput v0, Lrx/b/c/a;->a:I

    .line 48
    new-instance v0, Lrx/b/c/a$c;

    sget-object v1, Lrx/b/e/c;->a:Ljava/util/concurrent/ThreadFactory;

    invoke-direct {v0, v1}, Lrx/b/c/a$c;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    sput-object v0, Lrx/b/c/a;->b:Lrx/b/c/a$c;

    .line 49
    sget-object v0, Lrx/b/c/a;->b:Lrx/b/c/a$c;

    invoke-virtual {v0}, Lrx/b/c/a$c;->unsubscribe()V

    .line 53
    new-instance v0, Lrx/b/c/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2}, Lrx/b/c/a$b;-><init>(Ljava/util/concurrent/ThreadFactory;I)V

    sput-object v0, Lrx/b/c/a;->c:Lrx/b/c/a$b;

    return-void
.end method


# virtual methods
.method public a()Lrx/g$a;
    .locals 2

    .prologue
    .line 103
    new-instance v1, Lrx/b/c/a$a;

    iget-object v0, p0, Lrx/b/c/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/b/c/a$b;

    invoke-virtual {v0}, Lrx/b/c/a$b;->a()Lrx/b/c/a$c;

    move-result-object v0

    invoke-direct {v1, v0}, Lrx/b/c/a$a;-><init>(Lrx/b/c/a$c;)V

    return-object v1
.end method

.method public a(Lrx/a/a;)Lrx/k;
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lrx/b/c/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/b/c/a$b;

    invoke-virtual {v0}, Lrx/b/c/a$b;->a()Lrx/b/c/a$c;

    move-result-object v0

    .line 136
    const-wide/16 v2, -0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, v2, v3, v1}, Lrx/b/c/a$c;->b(Lrx/a/a;JLjava/util/concurrent/TimeUnit;)Lrx/b/c/c;

    move-result-object v0

    return-object v0
.end method
