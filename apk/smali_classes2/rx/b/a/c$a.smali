.class abstract Lrx/b/a/c$a;
.super Ljava/util/concurrent/atomic/AtomicLong;
.source "OnSubscribeCreate.java"

# interfaces
.implements Lrx/c;
.implements Lrx/f;
.implements Lrx/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lrx/b/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicLong;",
        "Lrx/c",
        "<TT;>;",
        "Lrx/f;",
        "Lrx/k;"
    }
.end annotation


# instance fields
.field final a:Lrx/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/j",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lrx/d/b;


# direct methods
.method public constructor <init>(Lrx/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    .line 87
    iput-object p1, p0, Lrx/b/a/c$a;->a:Lrx/j;

    .line 88
    new-instance v0, Lrx/d/b;

    invoke-direct {v0}, Lrx/d/b;-><init>()V

    iput-object v0, p0, Lrx/b/a/c$a;->b:Lrx/d/b;

    .line 89
    return-void
.end method


# virtual methods
.method a()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method b()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public final isUnsubscribed()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lrx/b/a/c$a;->b:Lrx/d/b;

    invoke-virtual {v0}, Lrx/d/b;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public onCompleted()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lrx/b/a/c$a;->a:Lrx/j;

    invoke-virtual {v0}, Lrx/j;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 97
    :cond_0
    :try_start_0
    iget-object v0, p0, Lrx/b/a/c$a;->a:Lrx/j;

    invoke-virtual {v0}, Lrx/j;->onCompleted()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    iget-object v0, p0, Lrx/b/a/c$a;->b:Lrx/d/b;

    invoke-virtual {v0}, Lrx/d/b;->unsubscribe()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lrx/b/a/c$a;->b:Lrx/d/b;

    invoke-virtual {v1}, Lrx/d/b;->unsubscribe()V

    throw v0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lrx/b/a/c$a;->a:Lrx/j;

    invoke-virtual {v0}, Lrx/j;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    :goto_0
    return-void

    .line 109
    :cond_0
    :try_start_0
    iget-object v0, p0, Lrx/b/a/c$a;->a:Lrx/j;

    invoke-virtual {v0, p1}, Lrx/j;->onError(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    iget-object v0, p0, Lrx/b/a/c$a;->b:Lrx/d/b;

    invoke-virtual {v0}, Lrx/d/b;->unsubscribe()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lrx/b/a/c$a;->b:Lrx/d/b;

    invoke-virtual {v1}, Lrx/d/b;->unsubscribe()V

    throw v0
.end method

.method public final request(J)V
    .locals 1

    .prologue
    .line 132
    invoke-static {p1, p2}, Lrx/b/a/a;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-static {p0, p1, p2}, Lrx/b/a/a;->a(Ljava/util/concurrent/atomic/AtomicLong;J)J

    .line 134
    invoke-virtual {p0}, Lrx/b/a/c$a;->b()V

    .line 136
    :cond_0
    return-void
.end method

.method public final unsubscribe()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lrx/b/a/c$a;->b:Lrx/d/b;

    invoke-virtual {v0}, Lrx/d/b;->unsubscribe()V

    .line 118
    invoke-virtual {p0}, Lrx/b/a/c$a;->a()V

    .line 119
    return-void
.end method
