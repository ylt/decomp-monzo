.class public final Lrx/b/a/c;
.super Ljava/lang/Object;
.source "OnSubscribeCreate.java"

# interfaces
.implements Lrx/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/b/a/c$1;,
        Lrx/b/a/c$e;,
        Lrx/b/a/c$b;,
        Lrx/b/a/c$d;,
        Lrx/b/a/c$c;,
        Lrx/b/a/c$f;,
        Lrx/b/a/c$g;,
        Lrx/b/a/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/d$a",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/a/b",
            "<",
            "Lrx/c",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field final b:Lrx/c$a;


# virtual methods
.method public a(Lrx/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    sget-object v0, Lrx/b/a/c$1;->a:[I

    iget-object v1, p0, Lrx/b/a/c;->b:Lrx/c$a;

    invoke-virtual {v1}, Lrx/c$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 65
    new-instance v0, Lrx/b/a/c$b;

    sget v1, Lrx/b/e/b;->a:I

    invoke-direct {v0, p1, v1}, Lrx/b/a/c$b;-><init>(Lrx/j;I)V

    .line 70
    :goto_0
    invoke-virtual {p1, v0}, Lrx/j;->add(Lrx/k;)V

    .line 71
    invoke-virtual {p1, v0}, Lrx/j;->setProducer(Lrx/f;)V

    .line 72
    iget-object v1, p0, Lrx/b/a/c;->a:Lrx/a/b;

    invoke-interface {v1, v0}, Lrx/a/b;->call(Ljava/lang/Object;)V

    .line 74
    return-void

    .line 49
    :pswitch_0
    new-instance v0, Lrx/b/a/c$g;

    invoke-direct {v0, p1}, Lrx/b/a/c$g;-><init>(Lrx/j;)V

    goto :goto_0

    .line 53
    :pswitch_1
    new-instance v0, Lrx/b/a/c$d;

    invoke-direct {v0, p1}, Lrx/b/a/c$d;-><init>(Lrx/j;)V

    goto :goto_0

    .line 57
    :pswitch_2
    new-instance v0, Lrx/b/a/c$c;

    invoke-direct {v0, p1}, Lrx/b/a/c$c;-><init>(Lrx/j;)V

    goto :goto_0

    .line 61
    :pswitch_3
    new-instance v0, Lrx/b/a/c$e;

    invoke-direct {v0, p1}, Lrx/b/a/c$e;-><init>(Lrx/j;)V

    goto :goto_0

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lrx/j;

    invoke-virtual {p0, p1}, Lrx/b/a/c;->a(Lrx/j;)V

    return-void
.end method
