.class public final Lrx/b/a/e;
.super Ljava/lang/Object;
.source "OperatorSubscribeOn.java"

# interfaces
.implements Lrx/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/b/a/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/d$a",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lrx/g;

.field final b:Lrx/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/d",
            "<TT;>;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lrx/d;Lrx/g;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/d",
            "<TT;>;",
            "Lrx/g;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p2, p0, Lrx/b/a/e;->a:Lrx/g;

    .line 38
    iput-object p1, p0, Lrx/b/a/e;->b:Lrx/d;

    .line 39
    iput-boolean p3, p0, Lrx/b/a/e;->c:Z

    .line 40
    return-void
.end method


# virtual methods
.method public a(Lrx/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lrx/b/a/e;->a:Lrx/g;

    invoke-virtual {v0}, Lrx/g;->a()Lrx/g$a;

    move-result-object v0

    .line 46
    new-instance v1, Lrx/b/a/e$a;

    iget-boolean v2, p0, Lrx/b/a/e;->c:Z

    iget-object v3, p0, Lrx/b/a/e;->b:Lrx/d;

    invoke-direct {v1, p1, v2, v0, v3}, Lrx/b/a/e$a;-><init>(Lrx/j;ZLrx/g$a;Lrx/d;)V

    .line 47
    invoke-virtual {p1, v1}, Lrx/j;->add(Lrx/k;)V

    .line 48
    invoke-virtual {p1, v0}, Lrx/j;->add(Lrx/k;)V

    .line 50
    invoke-virtual {v0, v1}, Lrx/g$a;->a(Lrx/a/a;)Lrx/k;

    .line 51
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lrx/j;

    invoke-virtual {p0, p1}, Lrx/b/a/e;->a(Lrx/j;)V

    return-void
.end method
