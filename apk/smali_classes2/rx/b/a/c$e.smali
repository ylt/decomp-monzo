.class final Lrx/b/a/c$e;
.super Lrx/b/a/c$a;
.source "OnSubscribeCreate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lrx/b/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/b/a/c$a",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/lang/Throwable;

.field volatile e:Z

.field final f:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Lrx/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 417
    invoke-direct {p0, p1}, Lrx/b/a/c$a;-><init>(Lrx/j;)V

    .line 418
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lrx/b/a/c$e;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 419
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lrx/b/a/c$e;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 420
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Lrx/b/a/c$e;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 449
    iget-object v0, p0, Lrx/b/a/c$e;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 451
    :cond_0
    return-void
.end method

.method b()V
    .locals 0

    .prologue
    .line 443
    invoke-virtual {p0}, Lrx/b/a/c$e;->c()V

    .line 444
    return-void
.end method

.method c()V
    .locals 15

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    const/4 v14, 0x0

    const/4 v1, 0x1

    .line 454
    iget-object v0, p0, Lrx/b/a/c$e;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    :goto_0
    return-void

    .line 459
    :cond_0
    iget-object v8, p0, Lrx/b/a/c$e;->a:Lrx/j;

    .line 460
    iget-object v9, p0, Lrx/b/a/c$e;->c:Ljava/util/concurrent/atomic/AtomicReference;

    move v0, v1

    .line 463
    :cond_1
    invoke-virtual {p0}, Lrx/b/a/c$e;->get()J

    move-result-wide v10

    move-wide v4, v6

    .line 466
    :goto_1
    cmp-long v2, v4, v10

    if-eqz v2, :cond_6

    .line 467
    invoke-virtual {v8}, Lrx/j;->isUnsubscribed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 468
    invoke-virtual {v9, v14}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    goto :goto_0

    .line 472
    :cond_2
    iget-boolean v12, p0, Lrx/b/a/c$e;->e:Z

    .line 474
    invoke-virtual {v9, v14}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .line 476
    if-nez v13, :cond_3

    move v2, v1

    .line 478
    :goto_2
    if-eqz v12, :cond_5

    if-eqz v2, :cond_5

    .line 479
    iget-object v0, p0, Lrx/b/a/c$e;->d:Ljava/lang/Throwable;

    .line 480
    if-eqz v0, :cond_4

    .line 481
    invoke-super {p0, v0}, Lrx/b/a/c$a;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    move v2, v3

    .line 476
    goto :goto_2

    .line 483
    :cond_4
    invoke-super {p0}, Lrx/b/a/c$a;->onCompleted()V

    goto :goto_0

    .line 488
    :cond_5
    if-eqz v2, :cond_7

    .line 497
    :cond_6
    cmp-long v2, v4, v10

    if-nez v2, :cond_b

    .line 498
    invoke-virtual {v8}, Lrx/j;->isUnsubscribed()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 499
    invoke-virtual {v9, v14}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    goto :goto_0

    .line 492
    :cond_7
    invoke-static {v13}, Lrx/b/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v8, v2}, Lrx/j;->onNext(Ljava/lang/Object;)V

    .line 494
    const-wide/16 v12, 0x1

    add-long/2addr v4, v12

    .line 495
    goto :goto_1

    .line 503
    :cond_8
    iget-boolean v10, p0, Lrx/b/a/c$e;->e:Z

    .line 505
    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_9

    move v2, v1

    .line 507
    :goto_3
    if-eqz v10, :cond_b

    if-eqz v2, :cond_b

    .line 508
    iget-object v0, p0, Lrx/b/a/c$e;->d:Ljava/lang/Throwable;

    .line 509
    if-eqz v0, :cond_a

    .line 510
    invoke-super {p0, v0}, Lrx/b/a/c$a;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_9
    move v2, v3

    .line 505
    goto :goto_3

    .line 512
    :cond_a
    invoke-super {p0}, Lrx/b/a/c$a;->onCompleted()V

    goto :goto_0

    .line 518
    :cond_b
    cmp-long v2, v4, v6

    if-eqz v2, :cond_c

    .line 519
    invoke-static {p0, v4, v5}, Lrx/b/a/a;->b(Ljava/util/concurrent/atomic/AtomicLong;J)J

    .line 522
    :cond_c
    iget-object v2, p0, Lrx/b/a/c$e;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    neg-int v0, v0

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v0

    .line 523
    if-nez v0, :cond_1

    goto :goto_0
.end method

.method public onCompleted()V
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrx/b/a/c$e;->e:Z

    .line 438
    invoke-virtual {p0}, Lrx/b/a/c$e;->c()V

    .line 439
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 430
    iput-object p1, p0, Lrx/b/a/c$e;->d:Ljava/lang/Throwable;

    .line 431
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrx/b/a/c$e;->e:Z

    .line 432
    invoke-virtual {p0}, Lrx/b/a/c$e;->c()V

    .line 433
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 424
    iget-object v0, p0, Lrx/b/a/c$e;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p1}, Lrx/b/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 425
    invoke-virtual {p0}, Lrx/b/a/c$e;->c()V

    .line 426
    return-void
.end method
