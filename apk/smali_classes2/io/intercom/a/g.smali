.class public abstract Lio/intercom/a/g;
.super Ljava/lang/Object;
.source "ForwardingSink.java"

# interfaces
.implements Lio/intercom/a/r;


# instance fields
.field private final delegate:Lio/intercom/a/r;


# direct methods
.method public constructor <init>(Lio/intercom/a/r;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "delegate == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iput-object p1, p0, Lio/intercom/a/g;->delegate:Lio/intercom/a/r;

    .line 27
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lio/intercom/a/g;->delegate:Lio/intercom/a/r;

    invoke-interface {v0}, Lio/intercom/a/r;->close()V

    .line 48
    return-void
.end method

.method public final delegate()Lio/intercom/a/r;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lio/intercom/a/g;->delegate:Lio/intercom/a/r;

    return-object v0
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lio/intercom/a/g;->delegate:Lio/intercom/a/r;

    invoke-interface {v0}, Lio/intercom/a/r;->flush()V

    .line 40
    return-void
.end method

.method public timeout()Lio/intercom/a/t;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lio/intercom/a/g;->delegate:Lio/intercom/a/r;

    invoke-interface {v0}, Lio/intercom/a/r;->timeout()Lio/intercom/a/t;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/a/g;->delegate:Lio/intercom/a/r;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Lio/intercom/a/c;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lio/intercom/a/g;->delegate:Lio/intercom/a/r;

    invoke-interface {v0, p1, p2, p3}, Lio/intercom/a/r;->write(Lio/intercom/a/c;J)V

    .line 36
    return-void
.end method
