.class public Lio/intercom/a/i;
.super Lio/intercom/a/t;
.source "ForwardingTimeout.java"


# instance fields
.field private a:Lio/intercom/a/t;


# direct methods
.method public constructor <init>(Lio/intercom/a/t;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Lio/intercom/a/t;-><init>()V

    .line 26
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "delegate == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    iput-object p1, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Lio/intercom/a/t;)Lio/intercom/a/i;
    .locals 2

    .prologue
    .line 36
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "delegate == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    .line 38
    return-object p0
.end method

.method public final a()Lio/intercom/a/t;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    return-object v0
.end method

.method public clearDeadline()Lio/intercom/a/t;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    invoke-virtual {v0}, Lio/intercom/a/t;->clearDeadline()Lio/intercom/a/t;

    move-result-object v0

    return-object v0
.end method

.method public clearTimeout()Lio/intercom/a/t;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    invoke-virtual {v0}, Lio/intercom/a/t;->clearTimeout()Lio/intercom/a/t;

    move-result-object v0

    return-object v0
.end method

.method public deadlineNanoTime()J
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    invoke-virtual {v0}, Lio/intercom/a/t;->deadlineNanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public deadlineNanoTime(J)Lio/intercom/a/t;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    invoke-virtual {v0, p1, p2}, Lio/intercom/a/t;->deadlineNanoTime(J)Lio/intercom/a/t;

    move-result-object v0

    return-object v0
.end method

.method public hasDeadline()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    invoke-virtual {v0}, Lio/intercom/a/t;->hasDeadline()Z

    move-result v0

    return v0
.end method

.method public throwIfReached()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    invoke-virtual {v0}, Lio/intercom/a/t;->throwIfReached()V

    .line 71
    return-void
.end method

.method public timeout(JLjava/util/concurrent/TimeUnit;)Lio/intercom/a/t;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    invoke-virtual {v0, p1, p2, p3}, Lio/intercom/a/t;->timeout(JLjava/util/concurrent/TimeUnit;)Lio/intercom/a/t;

    move-result-object v0

    return-object v0
.end method

.method public timeoutNanos()J
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lio/intercom/a/i;->a:Lio/intercom/a/t;

    invoke-virtual {v0}, Lio/intercom/a/t;->timeoutNanos()J

    move-result-wide v0

    return-wide v0
.end method
