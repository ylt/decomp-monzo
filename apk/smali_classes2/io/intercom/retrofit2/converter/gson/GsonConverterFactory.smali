.class public final Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;
.super Lio/intercom/retrofit2/Converter$Factory;
.source "GsonConverterFactory.java"


# instance fields
.field private final gson:Lio/intercom/com/google/gson/e;


# direct methods
.method private constructor <init>(Lio/intercom/com/google/gson/e;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lio/intercom/retrofit2/Converter$Factory;-><init>()V

    .line 58
    iput-object p1, p0, Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;->gson:Lio/intercom/com/google/gson/e;

    .line 59
    return-void
.end method

.method public static create()Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lio/intercom/com/google/gson/e;

    invoke-direct {v0}, Lio/intercom/com/google/gson/e;-><init>()V

    invoke-static {v0}, Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;->create(Lio/intercom/com/google/gson/e;)Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    return-object v0
.end method

.method public static create(Lio/intercom/com/google/gson/e;)Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;
    .locals 2

    .prologue
    .line 51
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "gson == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    new-instance v0, Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;

    invoke-direct {v0, p0}, Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;-><init>(Lio/intercom/com/google/gson/e;)V

    return-object v0
.end method


# virtual methods
.method public requestBodyConverter(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;Lio/intercom/retrofit2/Retrofit;)Lio/intercom/retrofit2/Converter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lio/intercom/retrofit2/Retrofit;",
            ")",
            "Lio/intercom/retrofit2/Converter",
            "<*",
            "Lio/intercom/okhttp3/RequestBody;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;->gson:Lio/intercom/com/google/gson/e;

    invoke-static {p1}, Lio/intercom/com/google/gson/c/a;->a(Ljava/lang/reflect/Type;)Lio/intercom/com/google/gson/c/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/google/gson/e;->a(Lio/intercom/com/google/gson/c/a;)Lio/intercom/com/google/gson/q;

    move-result-object v0

    .line 72
    new-instance v1, Lio/intercom/retrofit2/converter/gson/GsonRequestBodyConverter;

    iget-object v2, p0, Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;->gson:Lio/intercom/com/google/gson/e;

    invoke-direct {v1, v2, v0}, Lio/intercom/retrofit2/converter/gson/GsonRequestBodyConverter;-><init>(Lio/intercom/com/google/gson/e;Lio/intercom/com/google/gson/q;)V

    return-object v1
.end method

.method public responseBodyConverter(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lio/intercom/retrofit2/Retrofit;)Lio/intercom/retrofit2/Converter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lio/intercom/retrofit2/Retrofit;",
            ")",
            "Lio/intercom/retrofit2/Converter",
            "<",
            "Lio/intercom/okhttp3/ResponseBody;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;->gson:Lio/intercom/com/google/gson/e;

    invoke-static {p1}, Lio/intercom/com/google/gson/c/a;->a(Ljava/lang/reflect/Type;)Lio/intercom/com/google/gson/c/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/google/gson/e;->a(Lio/intercom/com/google/gson/c/a;)Lio/intercom/com/google/gson/q;

    move-result-object v0

    .line 65
    new-instance v1, Lio/intercom/retrofit2/converter/gson/GsonResponseBodyConverter;

    iget-object v2, p0, Lio/intercom/retrofit2/converter/gson/GsonConverterFactory;->gson:Lio/intercom/com/google/gson/e;

    invoke-direct {v1, v2, v0}, Lio/intercom/retrofit2/converter/gson/GsonResponseBodyConverter;-><init>(Lio/intercom/com/google/gson/e;Lio/intercom/com/google/gson/q;)V

    return-object v1
.end method
