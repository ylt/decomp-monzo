.class public abstract Lio/intercom/com/google/gson/q;
.super Ljava/lang/Object;
.source "TypeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lio/intercom/com/google/gson/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lio/intercom/com/google/gson/j;"
        }
    .end annotation

    .prologue
    .line 233
    :try_start_0
    new-instance v0, Lio/intercom/com/google/gson/b/a/f;

    invoke-direct {v0}, Lio/intercom/com/google/gson/b/a/f;-><init>()V

    .line 234
    invoke-virtual {p0, v0, p1}, Lio/intercom/com/google/gson/q;->a(Lio/intercom/com/google/gson/stream/c;Ljava/lang/Object;)V

    .line 235
    invoke-virtual {v0}, Lio/intercom/com/google/gson/b/a/f;->a()Lio/intercom/com/google/gson/j;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    new-instance v1, Lio/intercom/com/google/gson/JsonIOException;

    invoke-direct {v1, v0}, Lio/intercom/com/google/gson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()Lio/intercom/com/google/gson/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/intercom/com/google/gson/q",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 186
    new-instance v0, Lio/intercom/com/google/gson/q$1;

    invoke-direct {v0, p0}, Lio/intercom/com/google/gson/q$1;-><init>(Lio/intercom/com/google/gson/q;)V

    return-object v0
.end method

.method public abstract a(Lio/intercom/com/google/gson/stream/c;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/google/gson/stream/c;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract b(Lio/intercom/com/google/gson/stream/a;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/google/gson/stream/a;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
