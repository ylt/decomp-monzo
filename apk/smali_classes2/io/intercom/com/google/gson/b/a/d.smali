.class public final Lio/intercom/com/google/gson/b/a/d;
.super Ljava/lang/Object;
.source "JsonAdapterAnnotationTypeAdapterFactory.java"

# interfaces
.implements Lio/intercom/com/google/gson/r;


# instance fields
.field private final a:Lio/intercom/com/google/gson/b/c;


# direct methods
.method public constructor <init>(Lio/intercom/com/google/gson/b/c;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lio/intercom/com/google/gson/b/a/d;->a:Lio/intercom/com/google/gson/b/c;

    .line 39
    return-void
.end method


# virtual methods
.method a(Lio/intercom/com/google/gson/b/c;Lio/intercom/com/google/gson/e;Lio/intercom/com/google/gson/c/a;Lio/intercom/com/google/gson/a/b;)Lio/intercom/com/google/gson/q;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/google/gson/b/c;",
            "Lio/intercom/com/google/gson/e;",
            "Lio/intercom/com/google/gson/c/a",
            "<*>;",
            "Lio/intercom/com/google/gson/a/b;",
            ")",
            "Lio/intercom/com/google/gson/q",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 55
    invoke-interface {p4}, Lio/intercom/com/google/gson/a/b;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lio/intercom/com/google/gson/c/a;->b(Ljava/lang/Class;)Lio/intercom/com/google/gson/c/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/b/c;->a(Lio/intercom/com/google/gson/c/a;)Lio/intercom/com/google/gson/b/h;

    move-result-object v0

    invoke-interface {v0}, Lio/intercom/com/google/gson/b/h;->a()Ljava/lang/Object;

    move-result-object v0

    .line 58
    instance-of v1, v0, Lio/intercom/com/google/gson/q;

    if-eqz v1, :cond_1

    .line 59
    check-cast v0, Lio/intercom/com/google/gson/q;

    .line 77
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {p4}, Lio/intercom/com/google/gson/a/b;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    invoke-virtual {v0}, Lio/intercom/com/google/gson/q;->a()Lio/intercom/com/google/gson/q;

    move-result-object v0

    .line 81
    :cond_0
    return-object v0

    .line 60
    :cond_1
    instance-of v1, v0, Lio/intercom/com/google/gson/r;

    if-eqz v1, :cond_2

    .line 61
    check-cast v0, Lio/intercom/com/google/gson/r;

    invoke-interface {v0, p2, p3}, Lio/intercom/com/google/gson/r;->a(Lio/intercom/com/google/gson/e;Lio/intercom/com/google/gson/c/a;)Lio/intercom/com/google/gson/q;

    move-result-object v0

    goto :goto_0

    .line 62
    :cond_2
    instance-of v1, v0, Lio/intercom/com/google/gson/o;

    if-nez v1, :cond_3

    instance-of v1, v0, Lio/intercom/com/google/gson/i;

    if-eqz v1, :cond_6

    .line 63
    :cond_3
    instance-of v1, v0, Lio/intercom/com/google/gson/o;

    if-eqz v1, :cond_4

    move-object v1, v0

    check-cast v1, Lio/intercom/com/google/gson/o;

    .line 66
    :goto_1
    instance-of v2, v0, Lio/intercom/com/google/gson/i;

    if-eqz v2, :cond_5

    check-cast v0, Lio/intercom/com/google/gson/i;

    move-object v2, v0

    .line 69
    :goto_2
    new-instance v0, Lio/intercom/com/google/gson/b/a/l;

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lio/intercom/com/google/gson/b/a/l;-><init>(Lio/intercom/com/google/gson/o;Lio/intercom/com/google/gson/i;Lio/intercom/com/google/gson/e;Lio/intercom/com/google/gson/c/a;Lio/intercom/com/google/gson/r;)V

    goto :goto_0

    :cond_4
    move-object v1, v5

    .line 63
    goto :goto_1

    :cond_5
    move-object v2, v5

    .line 66
    goto :goto_2

    .line 71
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid attempt to bind an instance of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 72
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " as a @JsonAdapter for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lio/intercom/com/google/gson/c/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory,"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " JsonSerializer or JsonDeserializer."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public a(Lio/intercom/com/google/gson/e;Lio/intercom/com/google/gson/c/a;)Lio/intercom/com/google/gson/q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/intercom/com/google/gson/e;",
            "Lio/intercom/com/google/gson/c/a",
            "<TT;>;)",
            "Lio/intercom/com/google/gson/q",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p2}, Lio/intercom/com/google/gson/c/a;->a()Ljava/lang/Class;

    move-result-object v0

    .line 45
    const-class v1, Lio/intercom/com/google/gson/a/b;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lio/intercom/com/google/gson/a/b;

    .line 46
    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x0

    .line 49
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lio/intercom/com/google/gson/b/a/d;->a:Lio/intercom/com/google/gson/b/c;

    invoke-virtual {p0, v1, p1, p2, v0}, Lio/intercom/com/google/gson/b/a/d;->a(Lio/intercom/com/google/gson/b/c;Lio/intercom/com/google/gson/e;Lio/intercom/com/google/gson/c/a;Lio/intercom/com/google/gson/a/b;)Lio/intercom/com/google/gson/q;

    move-result-object v0

    goto :goto_0
.end method
