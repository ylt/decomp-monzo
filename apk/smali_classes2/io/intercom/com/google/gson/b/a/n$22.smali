.class final Lio/intercom/com/google/gson/b/a/n$22;
.super Lio/intercom/com/google/gson/q;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/com/google/gson/b/a/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/intercom/com/google/gson/q",
        "<",
        "Lio/intercom/com/google/gson/j;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 697
    invoke-direct {p0}, Lio/intercom/com/google/gson/q;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lio/intercom/com/google/gson/stream/a;)Lio/intercom/com/google/gson/j;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 699
    sget-object v0, Lio/intercom/com/google/gson/b/a/n$29;->a:[I

    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->f()Lio/intercom/com/google/gson/stream/b;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/com/google/gson/stream/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 731
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 701
    :pswitch_0
    new-instance v0, Lio/intercom/com/google/gson/m;

    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/com/google/gson/m;-><init>(Ljava/lang/String;)V

    .line 725
    :goto_0
    return-object v0

    .line 703
    :pswitch_1
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->h()Ljava/lang/String;

    move-result-object v1

    .line 704
    new-instance v0, Lio/intercom/com/google/gson/m;

    new-instance v2, Lio/intercom/com/google/gson/b/f;

    invoke-direct {v2, v1}, Lio/intercom/com/google/gson/b/f;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lio/intercom/com/google/gson/m;-><init>(Ljava/lang/Number;)V

    goto :goto_0

    .line 706
    :pswitch_2
    new-instance v0, Lio/intercom/com/google/gson/m;

    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->i()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/com/google/gson/m;-><init>(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 708
    :pswitch_3
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->j()V

    .line 709
    sget-object v0, Lio/intercom/com/google/gson/k;->a:Lio/intercom/com/google/gson/k;

    goto :goto_0

    .line 711
    :pswitch_4
    new-instance v0, Lio/intercom/com/google/gson/g;

    invoke-direct {v0}, Lio/intercom/com/google/gson/g;-><init>()V

    .line 712
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->a()V

    .line 713
    :goto_1
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 714
    invoke-virtual {p0, p1}, Lio/intercom/com/google/gson/b/a/n$22;->a(Lio/intercom/com/google/gson/stream/a;)Lio/intercom/com/google/gson/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/google/gson/g;->a(Lio/intercom/com/google/gson/j;)V

    goto :goto_1

    .line 716
    :cond_0
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->b()V

    goto :goto_0

    .line 719
    :pswitch_5
    new-instance v0, Lio/intercom/com/google/gson/l;

    invoke-direct {v0}, Lio/intercom/com/google/gson/l;-><init>()V

    .line 720
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->c()V

    .line 721
    :goto_2
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 722
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lio/intercom/com/google/gson/b/a/n$22;->a(Lio/intercom/com/google/gson/stream/a;)Lio/intercom/com/google/gson/j;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/intercom/com/google/gson/l;->a(Ljava/lang/String;Lio/intercom/com/google/gson/j;)V

    goto :goto_2

    .line 724
    :cond_1
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->d()V

    goto :goto_0

    .line 699
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lio/intercom/com/google/gson/stream/c;Lio/intercom/com/google/gson/j;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 736
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lio/intercom/com/google/gson/j;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 737
    :cond_0
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/c;->f()Lio/intercom/com/google/gson/stream/c;

    .line 766
    :goto_0
    return-void

    .line 738
    :cond_1
    invoke-virtual {p2}, Lio/intercom/com/google/gson/j;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 739
    invoke-virtual {p2}, Lio/intercom/com/google/gson/j;->m()Lio/intercom/com/google/gson/m;

    move-result-object v0

    .line 740
    invoke-virtual {v0}, Lio/intercom/com/google/gson/m;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 741
    invoke-virtual {v0}, Lio/intercom/com/google/gson/m;->a()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->a(Ljava/lang/Number;)Lio/intercom/com/google/gson/stream/c;

    goto :goto_0

    .line 742
    :cond_2
    invoke-virtual {v0}, Lio/intercom/com/google/gson/m;->o()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 743
    invoke-virtual {v0}, Lio/intercom/com/google/gson/m;->f()Z

    move-result v0

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->a(Z)Lio/intercom/com/google/gson/stream/c;

    goto :goto_0

    .line 745
    :cond_3
    invoke-virtual {v0}, Lio/intercom/com/google/gson/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->b(Ljava/lang/String;)Lio/intercom/com/google/gson/stream/c;

    goto :goto_0

    .line 748
    :cond_4
    invoke-virtual {p2}, Lio/intercom/com/google/gson/j;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 749
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/c;->b()Lio/intercom/com/google/gson/stream/c;

    .line 750
    invoke-virtual {p2}, Lio/intercom/com/google/gson/j;->l()Lio/intercom/com/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/com/google/gson/g;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/google/gson/j;

    .line 751
    invoke-virtual {p0, p1, v0}, Lio/intercom/com/google/gson/b/a/n$22;->a(Lio/intercom/com/google/gson/stream/c;Lio/intercom/com/google/gson/j;)V

    goto :goto_1

    .line 753
    :cond_5
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/c;->c()Lio/intercom/com/google/gson/stream/c;

    goto :goto_0

    .line 755
    :cond_6
    invoke-virtual {p2}, Lio/intercom/com/google/gson/j;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 756
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/c;->d()Lio/intercom/com/google/gson/stream/c;

    .line 757
    invoke-virtual {p2}, Lio/intercom/com/google/gson/j;->k()Lio/intercom/com/google/gson/l;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/com/google/gson/l;->o()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 758
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lio/intercom/com/google/gson/stream/c;->a(Ljava/lang/String;)Lio/intercom/com/google/gson/stream/c;

    .line 759
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/google/gson/j;

    invoke-virtual {p0, p1, v0}, Lio/intercom/com/google/gson/b/a/n$22;->a(Lio/intercom/com/google/gson/stream/c;Lio/intercom/com/google/gson/j;)V

    goto :goto_2

    .line 761
    :cond_7
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/c;->e()Lio/intercom/com/google/gson/stream/c;

    goto/16 :goto_0

    .line 764
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t write "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic a(Lio/intercom/com/google/gson/stream/c;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 697
    check-cast p2, Lio/intercom/com/google/gson/j;

    invoke-virtual {p0, p1, p2}, Lio/intercom/com/google/gson/b/a/n$22;->a(Lio/intercom/com/google/gson/stream/c;Lio/intercom/com/google/gson/j;)V

    return-void
.end method

.method public synthetic b(Lio/intercom/com/google/gson/stream/a;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 697
    invoke-virtual {p0, p1}, Lio/intercom/com/google/gson/b/a/n$22;->a(Lio/intercom/com/google/gson/stream/a;)Lio/intercom/com/google/gson/j;

    move-result-object v0

    return-object v0
.end method
