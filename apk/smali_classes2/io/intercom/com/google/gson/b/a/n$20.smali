.class final Lio/intercom/com/google/gson/b/a/n$20;
.super Lio/intercom/com/google/gson/q;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/com/google/gson/b/a/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/intercom/com/google/gson/q",
        "<",
        "Ljava/util/Calendar;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 592
    invoke-direct {p0}, Lio/intercom/com/google/gson/q;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lio/intercom/com/google/gson/stream/a;)Ljava/util/Calendar;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 602
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->f()Lio/intercom/com/google/gson/stream/b;

    move-result-object v0

    sget-object v1, Lio/intercom/com/google/gson/stream/b;->i:Lio/intercom/com/google/gson/stream/b;

    if-ne v0, v1, :cond_0

    .line 603
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->j()V

    .line 604
    const/4 v0, 0x0

    .line 631
    :goto_0
    return-object v0

    .line 606
    :cond_0
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->c()V

    move v5, v6

    move v4, v6

    move v3, v6

    move v2, v6

    move v1, v6

    .line 613
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->f()Lio/intercom/com/google/gson/stream/b;

    move-result-object v0

    sget-object v7, Lio/intercom/com/google/gson/stream/b;->d:Lio/intercom/com/google/gson/stream/b;

    if-eq v0, v7, :cond_7

    .line 614
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->g()Ljava/lang/String;

    move-result-object v7

    .line 615
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->m()I

    move-result v0

    .line 616
    const-string v8, "year"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v1, v0

    .line 617
    goto :goto_1

    .line 618
    :cond_2
    const-string v8, "month"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v2, v0

    .line 619
    goto :goto_1

    .line 620
    :cond_3
    const-string v8, "dayOfMonth"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    move v3, v0

    .line 621
    goto :goto_1

    .line 622
    :cond_4
    const-string v8, "hourOfDay"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    move v4, v0

    .line 623
    goto :goto_1

    .line 624
    :cond_5
    const-string v8, "minute"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    move v5, v0

    .line 625
    goto :goto_1

    .line 626
    :cond_6
    const-string v8, "second"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v6, v0

    .line 627
    goto :goto_1

    .line 630
    :cond_7
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/a;->d()V

    .line 631
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V

    goto :goto_0
.end method

.method public bridge synthetic a(Lio/intercom/com/google/gson/stream/c;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 592
    check-cast p2, Ljava/util/Calendar;

    invoke-virtual {p0, p1, p2}, Lio/intercom/com/google/gson/b/a/n$20;->a(Lio/intercom/com/google/gson/stream/c;Ljava/util/Calendar;)V

    return-void
.end method

.method public a(Lio/intercom/com/google/gson/stream/c;Ljava/util/Calendar;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 636
    if-nez p2, :cond_0

    .line 637
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/c;->f()Lio/intercom/com/google/gson/stream/c;

    .line 654
    :goto_0
    return-void

    .line 640
    :cond_0
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/c;->d()Lio/intercom/com/google/gson/stream/c;

    .line 641
    const-string v0, "year"

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->a(Ljava/lang/String;)Lio/intercom/com/google/gson/stream/c;

    .line 642
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lio/intercom/com/google/gson/stream/c;->a(J)Lio/intercom/com/google/gson/stream/c;

    .line 643
    const-string v0, "month"

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->a(Ljava/lang/String;)Lio/intercom/com/google/gson/stream/c;

    .line 644
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lio/intercom/com/google/gson/stream/c;->a(J)Lio/intercom/com/google/gson/stream/c;

    .line 645
    const-string v0, "dayOfMonth"

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->a(Ljava/lang/String;)Lio/intercom/com/google/gson/stream/c;

    .line 646
    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lio/intercom/com/google/gson/stream/c;->a(J)Lio/intercom/com/google/gson/stream/c;

    .line 647
    const-string v0, "hourOfDay"

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->a(Ljava/lang/String;)Lio/intercom/com/google/gson/stream/c;

    .line 648
    const/16 v0, 0xb

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lio/intercom/com/google/gson/stream/c;->a(J)Lio/intercom/com/google/gson/stream/c;

    .line 649
    const-string v0, "minute"

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->a(Ljava/lang/String;)Lio/intercom/com/google/gson/stream/c;

    .line 650
    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lio/intercom/com/google/gson/stream/c;->a(J)Lio/intercom/com/google/gson/stream/c;

    .line 651
    const-string v0, "second"

    invoke-virtual {p1, v0}, Lio/intercom/com/google/gson/stream/c;->a(Ljava/lang/String;)Lio/intercom/com/google/gson/stream/c;

    .line 652
    const/16 v0, 0xd

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lio/intercom/com/google/gson/stream/c;->a(J)Lio/intercom/com/google/gson/stream/c;

    .line 653
    invoke-virtual {p1}, Lio/intercom/com/google/gson/stream/c;->e()Lio/intercom/com/google/gson/stream/c;

    goto :goto_0
.end method

.method public synthetic b(Lio/intercom/com/google/gson/stream/a;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 592
    invoke-virtual {p0, p1}, Lio/intercom/com/google/gson/b/a/n$20;->a(Lio/intercom/com/google/gson/stream/a;)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method
