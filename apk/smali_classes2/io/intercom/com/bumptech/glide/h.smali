.class public Lio/intercom/com/bumptech/glide/h;
.super Ljava/lang/Object;
.source "RequestBuilder.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TranscodeType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field protected static final a:Lio/intercom/com/bumptech/glide/f/f;


# instance fields
.field protected b:Lio/intercom/com/bumptech/glide/f/f;

.field private final c:Lio/intercom/com/bumptech/glide/e;

.field private final d:Lio/intercom/com/bumptech/glide/i;

.field private final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TTranscodeType;>;"
        }
    .end annotation
.end field

.field private final f:Lio/intercom/com/bumptech/glide/f/f;

.field private final g:Lio/intercom/com/bumptech/glide/c;

.field private h:Lio/intercom/com/bumptech/glide/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/j",
            "<*-TTranscodeType;>;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/Object;

.field private j:Lio/intercom/com/bumptech/glide/f/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/f/e",
            "<TTranscodeType;>;"
        }
    .end annotation
.end field

.field private k:Lio/intercom/com/bumptech/glide/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation
.end field

.field private l:Ljava/lang/Float;

.field private m:Z

.field private n:Z

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lio/intercom/com/bumptech/glide/f/f;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/f/f;-><init>()V

    sget-object v1, Lio/intercom/com/bumptech/glide/load/engine/h;->c:Lio/intercom/com/bumptech/glide/load/engine/h;

    .line 37
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->b(Lio/intercom/com/bumptech/glide/load/engine/h;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    sget-object v1, Lio/intercom/com/bumptech/glide/g;->d:Lio/intercom/com/bumptech/glide/g;

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->a(Lio/intercom/com/bumptech/glide/g;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 38
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->a(Z)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    sput-object v0, Lio/intercom/com/bumptech/glide/h;->a:Lio/intercom/com/bumptech/glide/f/f;

    .line 36
    return-void
.end method

.method protected constructor <init>(Lio/intercom/com/bumptech/glide/c;Lio/intercom/com/bumptech/glide/i;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/c;",
            "Lio/intercom/com/bumptech/glide/i;",
            "Ljava/lang/Class",
            "<TTranscodeType;>;)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/intercom/com/bumptech/glide/h;->m:Z

    .line 64
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/h;->g:Lio/intercom/com/bumptech/glide/c;

    .line 65
    iput-object p2, p0, Lio/intercom/com/bumptech/glide/h;->d:Lio/intercom/com/bumptech/glide/i;

    .line 66
    invoke-virtual {p1}, Lio/intercom/com/bumptech/glide/c;->e()Lio/intercom/com/bumptech/glide/e;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/h;->c:Lio/intercom/com/bumptech/glide/e;

    .line 67
    iput-object p3, p0, Lio/intercom/com/bumptech/glide/h;->e:Ljava/lang/Class;

    .line 68
    invoke-virtual {p2}, Lio/intercom/com/bumptech/glide/i;->f()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/h;->f:Lio/intercom/com/bumptech/glide/f/f;

    .line 69
    invoke-virtual {p2, p3}, Lio/intercom/com/bumptech/glide/i;->b(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/j;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/h;->h:Lio/intercom/com/bumptech/glide/j;

    .line 70
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->f:Lio/intercom/com/bumptech/glide/f/f;

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 71
    return-void
.end method

.method private a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/f;Lio/intercom/com/bumptech/glide/f/c;Lio/intercom/com/bumptech/glide/j;Lio/intercom/com/bumptech/glide/g;II)Lio/intercom/com/bumptech/glide/f/b;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<TTranscodeType;>;",
            "Lio/intercom/com/bumptech/glide/f/f;",
            "Lio/intercom/com/bumptech/glide/f/c;",
            "Lio/intercom/com/bumptech/glide/j",
            "<*-TTranscodeType;>;",
            "Lio/intercom/com/bumptech/glide/g;",
            "II)",
            "Lio/intercom/com/bumptech/glide/f/b;"
        }
    .end annotation

    .prologue
    .line 655
    invoke-virtual {p2}, Lio/intercom/com/bumptech/glide/f/f;->i()Lio/intercom/com/bumptech/glide/f/f;

    .line 657
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->c:Lio/intercom/com/bumptech/glide/e;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->i:Ljava/lang/Object;

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/h;->e:Ljava/lang/Class;

    iget-object v8, p0, Lio/intercom/com/bumptech/glide/h;->j:Lio/intercom/com/bumptech/glide/f/e;

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/h;->c:Lio/intercom/com/bumptech/glide/e;

    .line 668
    invoke-virtual {v3}, Lio/intercom/com/bumptech/glide/e;->c()Lio/intercom/com/bumptech/glide/load/engine/i;

    move-result-object v10

    .line 669
    invoke-virtual/range {p4 .. p4}, Lio/intercom/com/bumptech/glide/j;->b()Lio/intercom/com/bumptech/glide/f/b/e;

    move-result-object v11

    move-object v3, p2

    move/from16 v4, p6

    move/from16 v5, p7

    move-object/from16 v6, p5

    move-object v7, p1

    move-object v9, p3

    .line 657
    invoke-static/range {v0 .. v11}, Lio/intercom/com/bumptech/glide/f/h;->a(Lio/intercom/com/bumptech/glide/e;Ljava/lang/Object;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/f/f;IILio/intercom/com/bumptech/glide/g;Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/e;Lio/intercom/com/bumptech/glide/f/c;Lio/intercom/com/bumptech/glide/load/engine/i;Lio/intercom/com/bumptech/glide/f/b/e;)Lio/intercom/com/bumptech/glide/f/h;

    move-result-object v0

    return-object v0
.end method

.method private a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/i;Lio/intercom/com/bumptech/glide/j;Lio/intercom/com/bumptech/glide/g;II)Lio/intercom/com/bumptech/glide/f/b;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<TTranscodeType;>;",
            "Lio/intercom/com/bumptech/glide/f/i;",
            "Lio/intercom/com/bumptech/glide/j",
            "<*-TTranscodeType;>;",
            "Lio/intercom/com/bumptech/glide/g;",
            "II)",
            "Lio/intercom/com/bumptech/glide/f/b;"
        }
    .end annotation

    .prologue
    .line 594
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    if-eqz v1, :cond_2

    .line 596
    iget-boolean v1, p0, Lio/intercom/com/bumptech/glide/h;->o:Z

    if-eqz v1, :cond_0

    .line 597
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 601
    :cond_0
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/h;->h:Lio/intercom/com/bumptech/glide/j;

    .line 606
    iget-object v2, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    iget-boolean v2, v2, Lio/intercom/com/bumptech/glide/h;->m:Z

    if-eqz v2, :cond_5

    move-object/from16 v9, p3

    .line 610
    :goto_0
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->y()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 611
    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->z()Lio/intercom/com/bumptech/glide/g;

    move-result-object v1

    move-object v10, v1

    .line 613
    :goto_1
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->A()I

    move-result v2

    .line 614
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->C()I

    move-result v1

    .line 615
    invoke-static/range {p5 .. p6}, Lio/intercom/com/bumptech/glide/h/i;->a(II)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    iget-object v3, v3, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 616
    invoke-virtual {v3}, Lio/intercom/com/bumptech/glide/f/f;->B()Z

    move-result v3

    if-nez v3, :cond_4

    .line 617
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->A()I

    move-result v2

    .line 618
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->C()I

    move-result v1

    move v11, v1

    move v12, v2

    .line 621
    :goto_2
    new-instance v4, Lio/intercom/com/bumptech/glide/f/i;

    invoke-direct {v4, p2}, Lio/intercom/com/bumptech/glide/f/i;-><init>(Lio/intercom/com/bumptech/glide/f/c;)V

    .line 622
    iget-object v3, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/f;Lio/intercom/com/bumptech/glide/f/c;Lio/intercom/com/bumptech/glide/j;Lio/intercom/com/bumptech/glide/g;II)Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v1

    .line 624
    const/4 v2, 0x1

    iput-boolean v2, p0, Lio/intercom/com/bumptech/glide/h;->o:Z

    .line 626
    iget-object v2, p0, Lio/intercom/com/bumptech/glide/h;->k:Lio/intercom/com/bumptech/glide/h;

    move-object v3, p1

    move-object v5, v9

    move-object v6, v10

    move v7, v12

    move v8, v11

    invoke-direct/range {v2 .. v8}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/i;Lio/intercom/com/bumptech/glide/j;Lio/intercom/com/bumptech/glide/g;II)Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v2

    .line 628
    const/4 v3, 0x0

    iput-boolean v3, p0, Lio/intercom/com/bumptech/glide/h;->o:Z

    .line 629
    invoke-virtual {v4, v1, v2}, Lio/intercom/com/bumptech/glide/f/i;->a(Lio/intercom/com/bumptech/glide/f/b;Lio/intercom/com/bumptech/glide/f/b;)V

    .line 646
    :goto_3
    return-object v4

    .line 611
    :cond_1
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/g;)Lio/intercom/com/bumptech/glide/g;

    move-result-object v1

    move-object v10, v1

    goto :goto_1

    .line 631
    :cond_2
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->l:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 633
    new-instance v4, Lio/intercom/com/bumptech/glide/f/i;

    invoke-direct {v4, p2}, Lio/intercom/com/bumptech/glide/f/i;-><init>(Lio/intercom/com/bumptech/glide/f/c;)V

    .line 634
    iget-object v3, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/f;Lio/intercom/com/bumptech/glide/f/c;Lio/intercom/com/bumptech/glide/j;Lio/intercom/com/bumptech/glide/g;II)Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v9

    .line 636
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->a()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v1

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/h;->l:Ljava/lang/Float;

    .line 637
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Lio/intercom/com/bumptech/glide/f/f;->a(F)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v3

    .line 640
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/g;)Lio/intercom/com/bumptech/glide/g;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v5, p3

    move/from16 v7, p5

    move/from16 v8, p6

    .line 639
    invoke-direct/range {v1 .. v8}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/f;Lio/intercom/com/bumptech/glide/f/c;Lio/intercom/com/bumptech/glide/j;Lio/intercom/com/bumptech/glide/g;II)Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v1

    .line 642
    invoke-virtual {v4, v9, v1}, Lio/intercom/com/bumptech/glide/f/i;->a(Lio/intercom/com/bumptech/glide/f/b;Lio/intercom/com/bumptech/glide/f/b;)V

    goto :goto_3

    .line 646
    :cond_3
    iget-object v3, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/f;Lio/intercom/com/bumptech/glide/f/c;Lio/intercom/com/bumptech/glide/j;Lio/intercom/com/bumptech/glide/g;II)Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v4

    goto :goto_3

    :cond_4
    move v11, v1

    move v12, v2

    goto/16 :goto_2

    :cond_5
    move-object v9, v1

    goto/16 :goto_0
.end method

.method private a(Lio/intercom/com/bumptech/glide/g;)Lio/intercom/com/bumptech/glide/g;
    .locals 3

    .prologue
    .line 572
    sget-object v0, Lio/intercom/com/bumptech/glide/h$2;->b:[I

    invoke-virtual {p1}, Lio/intercom/com/bumptech/glide/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 581
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown priority: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v2}, Lio/intercom/com/bumptech/glide/f/f;->z()Lio/intercom/com/bumptech/glide/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 574
    :pswitch_0
    sget-object v0, Lio/intercom/com/bumptech/glide/g;->c:Lio/intercom/com/bumptech/glide/g;

    .line 579
    :goto_0
    return-object v0

    .line 576
    :pswitch_1
    sget-object v0, Lio/intercom/com/bumptech/glide/g;->b:Lio/intercom/com/bumptech/glide/g;

    goto :goto_0

    .line 579
    :pswitch_2
    sget-object v0, Lio/intercom/com/bumptech/glide/g;->a:Lio/intercom/com/bumptech/glide/g;

    goto :goto_0

    .line 572
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private b(Lio/intercom/com/bumptech/glide/f/a/h;)Lio/intercom/com/bumptech/glide/f/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<TTranscodeType;>;)",
            "Lio/intercom/com/bumptech/glide/f/b;"
        }
    .end annotation

    .prologue
    .line 586
    const/4 v2, 0x0

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/h;->h:Lio/intercom/com/bumptech/glide/j;

    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->z()Lio/intercom/com/bumptech/glide/g;

    move-result-object v4

    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 587
    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->A()I

    move-result v5

    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->C()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    .line 586
    invoke-direct/range {v0 .. v6}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/i;Lio/intercom/com/bumptech/glide/j;Lio/intercom/com/bumptech/glide/g;II)Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 200
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/h;->i:Ljava/lang/Object;

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/intercom/com/bumptech/glide/h;->n:Z

    .line 202
    return-object p0
.end method


# virtual methods
.method public a(Landroid/widget/ImageView;)Lio/intercom/com/bumptech/glide/f/a/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            ")",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 397
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/i;->a()V

    .line 398
    invoke-static {p1}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 401
    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    invoke-virtual {p1}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 403
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->a()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 406
    :cond_0
    sget-object v0, Lio/intercom/com/bumptech/glide/h$2;->a:[I

    invoke-virtual {p1}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 428
    :cond_1
    :goto_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->c:Lio/intercom/com/bumptech/glide/e;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->e:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, Lio/intercom/com/bumptech/glide/e;->a(Landroid/widget/ImageView;Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/f/a/h;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;)Lio/intercom/com/bumptech/glide/f/a/h;

    move-result-object v0

    return-object v0

    .line 408
    :pswitch_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->e()Lio/intercom/com/bumptech/glide/f/f;

    goto :goto_0

    .line 411
    :pswitch_1
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->g()Lio/intercom/com/bumptech/glide/f/f;

    goto :goto_0

    .line 416
    :pswitch_2
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->f()Lio/intercom/com/bumptech/glide/f/f;

    goto :goto_0

    .line 419
    :pswitch_3
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->g()Lio/intercom/com/bumptech/glide/f/f;

    goto :goto_0

    .line 406
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lio/intercom/com/bumptech/glide/f/a/h;)Lio/intercom/com/bumptech/glide/f/a/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Y::",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<TTranscodeType;>;>(TY;)TY;"
        }
    .end annotation

    .prologue
    .line 352
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/i;->a()V

    .line 353
    invoke-static {p1}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    iget-boolean v0, p0, Lio/intercom/com/bumptech/glide/h;->n:Z

    if-nez v0, :cond_0

    .line 355
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must call #load() before calling #into()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :cond_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->i()Lio/intercom/com/bumptech/glide/f/f;

    .line 359
    invoke-direct {p0, p1}, Lio/intercom/com/bumptech/glide/h;->b(Lio/intercom/com/bumptech/glide/f/a/h;)Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v1

    .line 361
    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/f/a/h;->getRequest()Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v2

    .line 365
    invoke-interface {v1, v2}, Lio/intercom/com/bumptech/glide/f/b;->a(Lio/intercom/com/bumptech/glide/f/b;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 366
    invoke-static {v2}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/f/b;

    invoke-interface {v0}, Lio/intercom/com/bumptech/glide/f/b;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 367
    invoke-static {v2}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/f/b;

    invoke-interface {v0}, Lio/intercom/com/bumptech/glide/f/b;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 368
    :cond_1
    invoke-interface {v1}, Lio/intercom/com/bumptech/glide/f/b;->i()V

    .line 372
    invoke-static {v2}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/f/b;

    invoke-interface {v0}, Lio/intercom/com/bumptech/glide/f/b;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 373
    invoke-interface {v2}, Lio/intercom/com/bumptech/glide/f/b;->a()V

    .line 382
    :cond_2
    :goto_0
    return-object p1

    .line 378
    :cond_3
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->d:Lio/intercom/com/bumptech/glide/i;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/i;->a(Lio/intercom/com/bumptech/glide/f/a/h;)V

    .line 379
    invoke-interface {p1, v1}, Lio/intercom/com/bumptech/glide/f/a/h;->setRequest(Lio/intercom/com/bumptech/glide/f/b;)V

    .line 380
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->d:Lio/intercom/com/bumptech/glide/i;

    invoke-virtual {v0, p1, v1}, Lio/intercom/com/bumptech/glide/i;->a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/b;)V

    goto :goto_0
.end method

.method public a(II)Lio/intercom/com/bumptech/glide/f/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lio/intercom/com/bumptech/glide/f/a",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 480
    new-instance v0, Lio/intercom/com/bumptech/glide/f/d;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->c:Lio/intercom/com/bumptech/glide/e;

    .line 481
    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/e;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lio/intercom/com/bumptech/glide/f/d;-><init>(Landroid/os/Handler;II)V

    .line 483
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/i;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->c:Lio/intercom/com/bumptech/glide/e;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/e;->b()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lio/intercom/com/bumptech/glide/h$1;

    invoke-direct {v2, p0, v0}, Lio/intercom/com/bumptech/glide/h$1;-><init>(Lio/intercom/com/bumptech/glide/h;Lio/intercom/com/bumptech/glide/f/d;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 496
    :goto_0
    return-object v0

    .line 493
    :cond_0
    invoke-virtual {p0, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;)Lio/intercom/com/bumptech/glide/f/a/h;

    goto :goto_0
.end method

.method protected a()Lio/intercom/com/bumptech/glide/f/f;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->f:Lio/intercom/com/bumptech/glide/f/f;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 95
    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->a()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    goto :goto_0
.end method

.method public a(Lio/intercom/com/bumptech/glide/f/e;)Lio/intercom/com/bumptech/glide/h;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/e",
            "<TTranscodeType;>;)",
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 125
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/h;->j:Lio/intercom/com/bumptech/glide/f/e;

    .line 127
    return-object p0
.end method

.method public a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/f;",
            ")",
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {p1}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-virtual {p0}, Lio/intercom/com/bumptech/glide/h;->a()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/f/f;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 90
    return-object p0
.end method

.method public a(Lio/intercom/com/bumptech/glide/j;)Lio/intercom/com/bumptech/glide/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/j",
            "<*-TTranscodeType;>;)",
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 109
    invoke-static {p1}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/j;

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/h;->h:Lio/intercom/com/bumptech/glide/j;

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/com/bumptech/glide/h;->m:Z

    .line 111
    return-object p0
.end method

.method public a(Ljava/io/File;)Lio/intercom/com/bumptech/glide/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lio/intercom/com/bumptech/glide/h;->b(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 196
    invoke-direct {p0, p1}, Lio/intercom/com/bumptech/glide/h;->b(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/intercom/com/bumptech/glide/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lio/intercom/com/bumptech/glide/h;->b(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lio/intercom/com/bumptech/glide/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/intercom/com/bumptech/glide/h",
            "<TTranscodeType;>;"
        }
    .end annotation

    .prologue
    .line 335
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/h;

    .line 336
    iget-object v1, v0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->a()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v1

    iput-object v1, v0, Lio/intercom/com/bumptech/glide/h;->b:Lio/intercom/com/bumptech/glide/f/f;

    .line 337
    iget-object v1, v0, Lio/intercom/com/bumptech/glide/h;->h:Lio/intercom/com/bumptech/glide/j;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/j;->a()Lio/intercom/com/bumptech/glide/j;

    move-result-object v1

    iput-object v1, v0, Lio/intercom/com/bumptech/glide/h;->h:Lio/intercom/com/bumptech/glide/j;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    return-object v0

    .line 339
    :catch_0
    move-exception v0

    .line 340
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/intercom/com/bumptech/glide/h;->b()Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    return-object v0
.end method
