.class public final Lio/intercom/com/bumptech/glide/d;
.super Ljava/lang/Object;
.source "GlideBuilder.java"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lio/intercom/com/bumptech/glide/j",
            "<**>;>;"
        }
    .end annotation
.end field

.field private b:Lio/intercom/com/bumptech/glide/load/engine/i;

.field private c:Lio/intercom/com/bumptech/glide/load/engine/a/e;

.field private d:Lio/intercom/com/bumptech/glide/load/engine/a/b;

.field private e:Lio/intercom/com/bumptech/glide/load/engine/b/h;

.field private f:Lio/intercom/com/bumptech/glide/load/engine/c/a;

.field private g:Lio/intercom/com/bumptech/glide/load/engine/c/a;

.field private h:Lio/intercom/com/bumptech/glide/load/engine/b/a$a;

.field private i:Lio/intercom/com/bumptech/glide/load/engine/b/i;

.field private j:Lio/intercom/com/bumptech/glide/c/d;

.field private k:I

.field private l:Lio/intercom/com/bumptech/glide/f/f;

.field private m:Lio/intercom/com/bumptech/glide/c/l$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->a:Ljava/util/Map;

    .line 42
    const/4 v0, 0x4

    iput v0, p0, Lio/intercom/com/bumptech/glide/d;->k:I

    .line 43
    new-instance v0, Lio/intercom/com/bumptech/glide/f/f;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/f/f;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->l:Lio/intercom/com/bumptech/glide/f/f;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lio/intercom/com/bumptech/glide/c;
    .locals 11

    .prologue
    .line 294
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->f:Lio/intercom/com/bumptech/glide/load/engine/c/a;

    if-nez v0, :cond_0

    .line 295
    invoke-static {}, Lio/intercom/com/bumptech/glide/load/engine/c/a;->b()Lio/intercom/com/bumptech/glide/load/engine/c/a;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->f:Lio/intercom/com/bumptech/glide/load/engine/c/a;

    .line 298
    :cond_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->g:Lio/intercom/com/bumptech/glide/load/engine/c/a;

    if-nez v0, :cond_1

    .line 299
    invoke-static {}, Lio/intercom/com/bumptech/glide/load/engine/c/a;->a()Lio/intercom/com/bumptech/glide/load/engine/c/a;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->g:Lio/intercom/com/bumptech/glide/load/engine/c/a;

    .line 302
    :cond_1
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->i:Lio/intercom/com/bumptech/glide/load/engine/b/i;

    if-nez v0, :cond_2

    .line 303
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/b/i$a;

    invoke-direct {v0, p1}, Lio/intercom/com/bumptech/glide/load/engine/b/i$a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/engine/b/i$a;->a()Lio/intercom/com/bumptech/glide/load/engine/b/i;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->i:Lio/intercom/com/bumptech/glide/load/engine/b/i;

    .line 306
    :cond_2
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->j:Lio/intercom/com/bumptech/glide/c/d;

    if-nez v0, :cond_3

    .line 307
    new-instance v0, Lio/intercom/com/bumptech/glide/c/f;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/c/f;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->j:Lio/intercom/com/bumptech/glide/c/d;

    .line 310
    :cond_3
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->c:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    if-nez v0, :cond_4

    .line 311
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->i:Lio/intercom/com/bumptech/glide/load/engine/b/i;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/engine/b/i;->b()I

    move-result v0

    .line 312
    if-lez v0, :cond_9

    .line 313
    new-instance v1, Lio/intercom/com/bumptech/glide/load/engine/a/k;

    invoke-direct {v1, v0}, Lio/intercom/com/bumptech/glide/load/engine/a/k;-><init>(I)V

    iput-object v1, p0, Lio/intercom/com/bumptech/glide/d;->c:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    .line 319
    :cond_4
    :goto_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->d:Lio/intercom/com/bumptech/glide/load/engine/a/b;

    if-nez v0, :cond_5

    .line 320
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/a/j;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/d;->i:Lio/intercom/com/bumptech/glide/load/engine/b/i;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/load/engine/b/i;->c()I

    move-result v1

    invoke-direct {v0, v1}, Lio/intercom/com/bumptech/glide/load/engine/a/j;-><init>(I)V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->d:Lio/intercom/com/bumptech/glide/load/engine/a/b;

    .line 323
    :cond_5
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->e:Lio/intercom/com/bumptech/glide/load/engine/b/h;

    if-nez v0, :cond_6

    .line 324
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/b/g;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/d;->i:Lio/intercom/com/bumptech/glide/load/engine/b/i;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/load/engine/b/i;->a()I

    move-result v1

    invoke-direct {v0, v1}, Lio/intercom/com/bumptech/glide/load/engine/b/g;-><init>(I)V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->e:Lio/intercom/com/bumptech/glide/load/engine/b/h;

    .line 327
    :cond_6
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->h:Lio/intercom/com/bumptech/glide/load/engine/b/a$a;

    if-nez v0, :cond_7

    .line 328
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/b/f;

    invoke-direct {v0, p1}, Lio/intercom/com/bumptech/glide/load/engine/b/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->h:Lio/intercom/com/bumptech/glide/load/engine/b/a$a;

    .line 331
    :cond_7
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->b:Lio/intercom/com/bumptech/glide/load/engine/i;

    if-nez v0, :cond_8

    .line 332
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/i;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/d;->e:Lio/intercom/com/bumptech/glide/load/engine/b/h;

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/d;->h:Lio/intercom/com/bumptech/glide/load/engine/b/a$a;

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/d;->g:Lio/intercom/com/bumptech/glide/load/engine/c/a;

    iget-object v4, p0, Lio/intercom/com/bumptech/glide/d;->f:Lio/intercom/com/bumptech/glide/load/engine/c/a;

    .line 333
    invoke-static {}, Lio/intercom/com/bumptech/glide/load/engine/c/a;->c()Lio/intercom/com/bumptech/glide/load/engine/c/a;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lio/intercom/com/bumptech/glide/load/engine/i;-><init>(Lio/intercom/com/bumptech/glide/load/engine/b/h;Lio/intercom/com/bumptech/glide/load/engine/b/a$a;Lio/intercom/com/bumptech/glide/load/engine/c/a;Lio/intercom/com/bumptech/glide/load/engine/c/a;Lio/intercom/com/bumptech/glide/load/engine/c/a;)V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->b:Lio/intercom/com/bumptech/glide/load/engine/i;

    .line 336
    :cond_8
    new-instance v6, Lio/intercom/com/bumptech/glide/c/l;

    iget-object v0, p0, Lio/intercom/com/bumptech/glide/d;->m:Lio/intercom/com/bumptech/glide/c/l$a;

    invoke-direct {v6, v0}, Lio/intercom/com/bumptech/glide/c/l;-><init>(Lio/intercom/com/bumptech/glide/c/l$a;)V

    .line 339
    new-instance v0, Lio/intercom/com/bumptech/glide/c;

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/d;->b:Lio/intercom/com/bumptech/glide/load/engine/i;

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/d;->e:Lio/intercom/com/bumptech/glide/load/engine/b/h;

    iget-object v4, p0, Lio/intercom/com/bumptech/glide/d;->c:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    iget-object v5, p0, Lio/intercom/com/bumptech/glide/d;->d:Lio/intercom/com/bumptech/glide/load/engine/a/b;

    iget-object v7, p0, Lio/intercom/com/bumptech/glide/d;->j:Lio/intercom/com/bumptech/glide/c/d;

    iget v8, p0, Lio/intercom/com/bumptech/glide/d;->k:I

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/d;->l:Lio/intercom/com/bumptech/glide/f/f;

    .line 348
    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/f/f;->i()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v9

    iget-object v10, p0, Lio/intercom/com/bumptech/glide/d;->a:Ljava/util/Map;

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lio/intercom/com/bumptech/glide/c;-><init>(Landroid/content/Context;Lio/intercom/com/bumptech/glide/load/engine/i;Lio/intercom/com/bumptech/glide/load/engine/b/h;Lio/intercom/com/bumptech/glide/load/engine/a/e;Lio/intercom/com/bumptech/glide/load/engine/a/b;Lio/intercom/com/bumptech/glide/c/l;Lio/intercom/com/bumptech/glide/c/d;ILio/intercom/com/bumptech/glide/f/f;Ljava/util/Map;)V

    .line 339
    return-object v0

    .line 315
    :cond_9
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/a/f;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/engine/a/f;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/d;->c:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    goto :goto_0
.end method

.method a(Lio/intercom/com/bumptech/glide/c/l$a;)Lio/intercom/com/bumptech/glide/d;
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/d;->m:Lio/intercom/com/bumptech/glide/c/l$a;

    .line 284
    return-object p0
.end method
