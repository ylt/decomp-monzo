.class public final Lio/intercom/com/bumptech/glide/h/a/a;
.super Ljava/lang/Object;
.source "FactoryPools.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/com/bumptech/glide/h/a/a$b;,
        Lio/intercom/com/bumptech/glide/h/a/a$c;,
        Lio/intercom/com/bumptech/glide/h/a/a$d;,
        Lio/intercom/com/bumptech/glide/h/a/a$a;
    }
.end annotation


# static fields
.field private static final a:Lio/intercom/com/bumptech/glide/h/a/a$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/h/a/a$d",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lio/intercom/com/bumptech/glide/h/a/a$1;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/h/a/a$1;-><init>()V

    sput-object v0, Lio/intercom/com/bumptech/glide/h/a/a;->a:Lio/intercom/com/bumptech/glide/h/a/a$d;

    return-void
.end method

.method public static a()Landroid/support/v4/g/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Landroid/support/v4/g/k$a",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 65
    const/16 v0, 0x14

    invoke-static {v0}, Lio/intercom/com/bumptech/glide/h/a/a;->a(I)Landroid/support/v4/g/k$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Landroid/support/v4/g/k$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "Landroid/support/v4/g/k$a",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Landroid/support/v4/g/k$c;

    invoke-direct {v0, p0}, Landroid/support/v4/g/k$c;-><init>(I)V

    new-instance v1, Lio/intercom/com/bumptech/glide/h/a/a$2;

    invoke-direct {v1}, Lio/intercom/com/bumptech/glide/h/a/a$2;-><init>()V

    new-instance v2, Lio/intercom/com/bumptech/glide/h/a/a$3;

    invoke-direct {v2}, Lio/intercom/com/bumptech/glide/h/a/a$3;-><init>()V

    invoke-static {v0, v1, v2}, Lio/intercom/com/bumptech/glide/h/a/a;->a(Landroid/support/v4/g/k$a;Lio/intercom/com/bumptech/glide/h/a/a$a;Lio/intercom/com/bumptech/glide/h/a/a$d;)Landroid/support/v4/g/k$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILio/intercom/com/bumptech/glide/h/a/a$a;)Landroid/support/v4/g/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lio/intercom/com/bumptech/glide/h/a/a$c;",
            ">(I",
            "Lio/intercom/com/bumptech/glide/h/a/a$a",
            "<TT;>;)",
            "Landroid/support/v4/g/k$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Landroid/support/v4/g/k$b;

    invoke-direct {v0, p0}, Landroid/support/v4/g/k$b;-><init>(I)V

    invoke-static {v0, p1}, Lio/intercom/com/bumptech/glide/h/a/a;->a(Landroid/support/v4/g/k$a;Lio/intercom/com/bumptech/glide/h/a/a$a;)Landroid/support/v4/g/k$a;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/support/v4/g/k$a;Lio/intercom/com/bumptech/glide/h/a/a$a;)Landroid/support/v4/g/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lio/intercom/com/bumptech/glide/h/a/a$c;",
            ">(",
            "Landroid/support/v4/g/k$a",
            "<TT;>;",
            "Lio/intercom/com/bumptech/glide/h/a/a$a",
            "<TT;>;)",
            "Landroid/support/v4/g/k$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/a/a;->b()Lio/intercom/com/bumptech/glide/h/a/a$d;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lio/intercom/com/bumptech/glide/h/a/a;->a(Landroid/support/v4/g/k$a;Lio/intercom/com/bumptech/glide/h/a/a$a;Lio/intercom/com/bumptech/glide/h/a/a$d;)Landroid/support/v4/g/k$a;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/support/v4/g/k$a;Lio/intercom/com/bumptech/glide/h/a/a$a;Lio/intercom/com/bumptech/glide/h/a/a$d;)Landroid/support/v4/g/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/support/v4/g/k$a",
            "<TT;>;",
            "Lio/intercom/com/bumptech/glide/h/a/a$a",
            "<TT;>;",
            "Lio/intercom/com/bumptech/glide/h/a/a$d",
            "<TT;>;)",
            "Landroid/support/v4/g/k$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lio/intercom/com/bumptech/glide/h/a/a$b;

    invoke-direct {v0, p0, p1, p2}, Lio/intercom/com/bumptech/glide/h/a/a$b;-><init>(Landroid/support/v4/g/k$a;Lio/intercom/com/bumptech/glide/h/a/a$a;Lio/intercom/com/bumptech/glide/h/a/a$d;)V

    return-object v0
.end method

.method public static b(ILio/intercom/com/bumptech/glide/h/a/a$a;)Landroid/support/v4/g/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lio/intercom/com/bumptech/glide/h/a/a$c;",
            ">(I",
            "Lio/intercom/com/bumptech/glide/h/a/a$a",
            "<TT;>;)",
            "Landroid/support/v4/g/k$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Landroid/support/v4/g/k$c;

    invoke-direct {v0, p0}, Landroid/support/v4/g/k$c;-><init>(I)V

    invoke-static {v0, p1}, Lio/intercom/com/bumptech/glide/h/a/a;->a(Landroid/support/v4/g/k$a;Lio/intercom/com/bumptech/glide/h/a/a$a;)Landroid/support/v4/g/k$a;

    move-result-object v0

    return-object v0
.end method

.method private static b()Lio/intercom/com/bumptech/glide/h/a/a$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/intercom/com/bumptech/glide/h/a/a$d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lio/intercom/com/bumptech/glide/h/a/a;->a:Lio/intercom/com/bumptech/glide/h/a/a$d;

    return-object v0
.end method
