.class public Lio/intercom/com/bumptech/glide/i;
.super Ljava/lang/Object;
.source "RequestManager.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/c/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/com/bumptech/glide/i$a;,
        Lio/intercom/com/bumptech/glide/i$b;
    }
.end annotation


# static fields
.field private static final c:Lio/intercom/com/bumptech/glide/f/f;

.field private static final d:Lio/intercom/com/bumptech/glide/f/f;

.field private static final e:Lio/intercom/com/bumptech/glide/f/f;


# instance fields
.field protected final a:Lio/intercom/com/bumptech/glide/c;

.field final b:Lio/intercom/com/bumptech/glide/c/h;

.field private final f:Lio/intercom/com/bumptech/glide/c/n;

.field private final g:Lio/intercom/com/bumptech/glide/c/m;

.field private final h:Lio/intercom/com/bumptech/glide/c/p;

.field private final i:Ljava/lang/Runnable;

.field private final j:Landroid/os/Handler;

.field private final k:Lio/intercom/com/bumptech/glide/c/c;

.field private l:Lio/intercom/com/bumptech/glide/f/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-class v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lio/intercom/com/bumptech/glide/f/f;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->i()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    sput-object v0, Lio/intercom/com/bumptech/glide/i;->c:Lio/intercom/com/bumptech/glide/f/f;

    .line 47
    const-class v0, Lio/intercom/com/bumptech/glide/load/resource/d/c;

    invoke-static {v0}, Lio/intercom/com/bumptech/glide/f/f;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->i()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    sput-object v0, Lio/intercom/com/bumptech/glide/i;->d:Lio/intercom/com/bumptech/glide/f/f;

    .line 48
    sget-object v0, Lio/intercom/com/bumptech/glide/load/engine/h;->c:Lio/intercom/com/bumptech/glide/load/engine/h;

    .line 49
    invoke-static {v0}, Lio/intercom/com/bumptech/glide/f/f;->a(Lio/intercom/com/bumptech/glide/load/engine/h;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    sget-object v1, Lio/intercom/com/bumptech/glide/g;->d:Lio/intercom/com/bumptech/glide/g;

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->a(Lio/intercom/com/bumptech/glide/g;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    const/4 v1, 0x1

    .line 50
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->a(Z)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    sput-object v0, Lio/intercom/com/bumptech/glide/i;->e:Lio/intercom/com/bumptech/glide/f/f;

    .line 48
    return-void
.end method

.method public constructor <init>(Lio/intercom/com/bumptech/glide/c;Lio/intercom/com/bumptech/glide/c/h;Lio/intercom/com/bumptech/glide/c/m;)V
    .locals 6

    .prologue
    .line 70
    new-instance v4, Lio/intercom/com/bumptech/glide/c/n;

    invoke-direct {v4}, Lio/intercom/com/bumptech/glide/c/n;-><init>()V

    invoke-virtual {p1}, Lio/intercom/com/bumptech/glide/c;->d()Lio/intercom/com/bumptech/glide/c/d;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lio/intercom/com/bumptech/glide/i;-><init>(Lio/intercom/com/bumptech/glide/c;Lio/intercom/com/bumptech/glide/c/h;Lio/intercom/com/bumptech/glide/c/m;Lio/intercom/com/bumptech/glide/c/n;Lio/intercom/com/bumptech/glide/c/d;)V

    .line 71
    return-void
.end method

.method constructor <init>(Lio/intercom/com/bumptech/glide/c;Lio/intercom/com/bumptech/glide/c/h;Lio/intercom/com/bumptech/glide/c/m;Lio/intercom/com/bumptech/glide/c/n;Lio/intercom/com/bumptech/glide/c/d;)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Lio/intercom/com/bumptech/glide/c/p;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/c/p;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/i;->h:Lio/intercom/com/bumptech/glide/c/p;

    .line 57
    new-instance v0, Lio/intercom/com/bumptech/glide/i$1;

    invoke-direct {v0, p0}, Lio/intercom/com/bumptech/glide/i$1;-><init>(Lio/intercom/com/bumptech/glide/i;)V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/i;->i:Ljava/lang/Runnable;

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/i;->j:Landroid/os/Handler;

    .line 81
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/i;->a:Lio/intercom/com/bumptech/glide/c;

    .line 82
    iput-object p2, p0, Lio/intercom/com/bumptech/glide/i;->b:Lio/intercom/com/bumptech/glide/c/h;

    .line 83
    iput-object p3, p0, Lio/intercom/com/bumptech/glide/i;->g:Lio/intercom/com/bumptech/glide/c/m;

    .line 84
    iput-object p4, p0, Lio/intercom/com/bumptech/glide/i;->f:Lio/intercom/com/bumptech/glide/c/n;

    .line 86
    invoke-virtual {p1}, Lio/intercom/com/bumptech/glide/c;->e()Lio/intercom/com/bumptech/glide/e;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/e;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 88
    new-instance v1, Lio/intercom/com/bumptech/glide/i$b;

    invoke-direct {v1, p4}, Lio/intercom/com/bumptech/glide/i$b;-><init>(Lio/intercom/com/bumptech/glide/c/n;)V

    .line 89
    invoke-interface {p5, v0, v1}, Lio/intercom/com/bumptech/glide/c/d;->a(Landroid/content/Context;Lio/intercom/com/bumptech/glide/c/c$a;)Lio/intercom/com/bumptech/glide/c/c;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/i;->k:Lio/intercom/com/bumptech/glide/c/c;

    .line 95
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/i;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->j:Landroid/os/Handler;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/i;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 100
    :goto_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->k:Lio/intercom/com/bumptech/glide/c/c;

    invoke-interface {p2, v0}, Lio/intercom/com/bumptech/glide/c/h;->a(Lio/intercom/com/bumptech/glide/c/i;)V

    .line 102
    invoke-virtual {p1}, Lio/intercom/com/bumptech/glide/c;->e()Lio/intercom/com/bumptech/glide/e;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/e;->a()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/com/bumptech/glide/i;->a(Lio/intercom/com/bumptech/glide/f/f;)V

    .line 104
    invoke-virtual {p1, p0}, Lio/intercom/com/bumptech/glide/c;->a(Lio/intercom/com/bumptech/glide/i;)V

    .line 105
    return-void

    .line 98
    :cond_0
    invoke-interface {p2, p0}, Lio/intercom/com/bumptech/glide/c/h;->a(Lio/intercom/com/bumptech/glide/c/i;)V

    goto :goto_0
.end method

.method private c(Lio/intercom/com/bumptech/glide/f/a/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 430
    invoke-virtual {p0, p1}, Lio/intercom/com/bumptech/glide/i;->b(Lio/intercom/com/bumptech/glide/f/a/h;)Z

    move-result v0

    .line 431
    if-nez v0, :cond_0

    .line 432
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->a:Lio/intercom/com/bumptech/glide/c;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/c;->a(Lio/intercom/com/bumptech/glide/f/a/h;)V

    .line 434
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ResourceType:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TResourceType;>;)",
            "Lio/intercom/com/bumptech/glide/h",
            "<TResourceType;>;"
        }
    .end annotation

    .prologue
    .line 387
    new-instance v0, Lio/intercom/com/bumptech/glide/h;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/i;->a:Lio/intercom/com/bumptech/glide/c;

    invoke-direct {v0, v1, p0, p1}, Lio/intercom/com/bumptech/glide/h;-><init>(Lio/intercom/com/bumptech/glide/c;Lio/intercom/com/bumptech/glide/i;Ljava/lang/Class;)V

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lio/intercom/com/bumptech/glide/h",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    invoke-virtual {p0}, Lio/intercom/com/bumptech/glide/i;->d()Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/h;->a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 200
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/i;->a()V

    .line 201
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->f:Lio/intercom/com/bumptech/glide/c/n;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c/n;->a()V

    .line 202
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 403
    new-instance v0, Lio/intercom/com/bumptech/glide/i$a;

    invoke-direct {v0, p1}, Lio/intercom/com/bumptech/glide/i$a;-><init>(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lio/intercom/com/bumptech/glide/i;->a(Lio/intercom/com/bumptech/glide/f/a/h;)V

    .line 404
    return-void
.end method

.method public a(Lio/intercom/com/bumptech/glide/f/a/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 413
    if-nez p1, :cond_0

    .line 427
    :goto_0
    return-void

    .line 417
    :cond_0
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    invoke-direct {p0, p1}, Lio/intercom/com/bumptech/glide/i;->c(Lio/intercom/com/bumptech/glide/f/a/h;)V

    goto :goto_0

    .line 420
    :cond_1
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->j:Landroid/os/Handler;

    new-instance v1, Lio/intercom/com/bumptech/glide/i$2;

    invoke-direct {v1, p0, p1}, Lio/intercom/com/bumptech/glide/i$2;-><init>(Lio/intercom/com/bumptech/glide/i;Lio/intercom/com/bumptech/glide/f/a/h;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method a(Lio/intercom/com/bumptech/glide/f/a/h;Lio/intercom/com/bumptech/glide/f/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<*>;",
            "Lio/intercom/com/bumptech/glide/f/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 453
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->h:Lio/intercom/com/bumptech/glide/c/p;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/c/p;->a(Lio/intercom/com/bumptech/glide/f/a/h;)V

    .line 454
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->f:Lio/intercom/com/bumptech/glide/c/n;

    invoke-virtual {v0, p2}, Lio/intercom/com/bumptech/glide/c/n;->a(Lio/intercom/com/bumptech/glide/f/b;)V

    .line 455
    return-void
.end method

.method protected a(Lio/intercom/com/bumptech/glide/f/f;)V
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p1}, Lio/intercom/com/bumptech/glide/f/f;->a()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/f/f;->j()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/i;->l:Lio/intercom/com/bumptech/glide/f/f;

    .line 109
    return-void
.end method

.method b(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lio/intercom/com/bumptech/glide/j",
            "<*TT;>;"
        }
    .end annotation

    .prologue
    .line 463
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->a:Lio/intercom/com/bumptech/glide/c;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c;->e()Lio/intercom/com/bumptech/glide/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/e;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/j;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 233
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/i;->a()V

    .line 234
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->f:Lio/intercom/com/bumptech/glide/c/n;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c/n;->b()V

    .line 235
    return-void
.end method

.method b(Lio/intercom/com/bumptech/glide/f/a/h;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/f/a/h",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 437
    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/f/a/h;->getRequest()Lio/intercom/com/bumptech/glide/f/b;

    move-result-object v1

    .line 439
    if-nez v1, :cond_0

    .line 448
    :goto_0
    return v0

    .line 443
    :cond_0
    iget-object v2, p0, Lio/intercom/com/bumptech/glide/i;->f:Lio/intercom/com/bumptech/glide/c/n;

    invoke-virtual {v2, v1}, Lio/intercom/com/bumptech/glide/c/n;->b(Lio/intercom/com/bumptech/glide/f/b;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 444
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/i;->h:Lio/intercom/com/bumptech/glide/c/p;

    invoke-virtual {v1, p1}, Lio/intercom/com/bumptech/glide/c/p;->b(Lio/intercom/com/bumptech/glide/f/a/h;)V

    .line 445
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Lio/intercom/com/bumptech/glide/f/a/h;->setRequest(Lio/intercom/com/bumptech/glide/f/b;)V

    goto :goto_0

    .line 448
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lio/intercom/com/bumptech/glide/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/intercom/com/bumptech/glide/h",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    const-class v0, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lio/intercom/com/bumptech/glide/i;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    sget-object v1, Lio/intercom/com/bumptech/glide/i;->c:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/intercom/com/bumptech/glide/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/intercom/com/bumptech/glide/h",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327
    const-class v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lio/intercom/com/bumptech/glide/i;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    return-object v0
.end method

.method public e()Lio/intercom/com/bumptech/glide/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/intercom/com/bumptech/glide/h",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 352
    const-class v0, Ljava/io/File;

    invoke-virtual {p0, v0}, Lio/intercom/com/bumptech/glide/i;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    sget-object v1, Lio/intercom/com/bumptech/glide/i;->e:Lio/intercom/com/bumptech/glide/f/f;

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    return-object v0
.end method

.method f()Lio/intercom/com/bumptech/glide/f/f;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->l:Lio/intercom/com/bumptech/glide/f/f;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->h:Lio/intercom/com/bumptech/glide/c/p;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c/p;->onDestroy()V

    .line 278
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->h:Lio/intercom/com/bumptech/glide/c/p;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c/p;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/f/a/h;

    .line 279
    invoke-virtual {p0, v0}, Lio/intercom/com/bumptech/glide/i;->a(Lio/intercom/com/bumptech/glide/f/a/h;)V

    goto :goto_0

    .line 281
    :cond_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->h:Lio/intercom/com/bumptech/glide/c/p;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c/p;->b()V

    .line 282
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->f:Lio/intercom/com/bumptech/glide/c/n;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c/n;->c()V

    .line 283
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->b:Lio/intercom/com/bumptech/glide/c/h;

    invoke-interface {v0, p0}, Lio/intercom/com/bumptech/glide/c/h;->b(Lio/intercom/com/bumptech/glide/c/i;)V

    .line 284
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->b:Lio/intercom/com/bumptech/glide/c/h;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/i;->k:Lio/intercom/com/bumptech/glide/c/c;

    invoke-interface {v0, v1}, Lio/intercom/com/bumptech/glide/c/h;->b(Lio/intercom/com/bumptech/glide/c/i;)V

    .line 285
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->j:Landroid/os/Handler;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/i;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 286
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->a:Lio/intercom/com/bumptech/glide/c;

    invoke-virtual {v0, p0}, Lio/intercom/com/bumptech/glide/c;->b(Lio/intercom/com/bumptech/glide/i;)V

    .line 287
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0}, Lio/intercom/com/bumptech/glide/i;->b()V

    .line 258
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->h:Lio/intercom/com/bumptech/glide/c/p;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c/p;->onStart()V

    .line 259
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lio/intercom/com/bumptech/glide/i;->a()V

    .line 268
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/i;->h:Lio/intercom/com/bumptech/glide/c/p;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/c/p;->onStop()V

    .line 269
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 468
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{tracker="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/i;->f:Lio/intercom/com/bumptech/glide/c/n;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", treeNode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/i;->g:Lio/intercom/com/bumptech/glide/c/m;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
