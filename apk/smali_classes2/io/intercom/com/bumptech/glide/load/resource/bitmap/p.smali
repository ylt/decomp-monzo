.class public Lio/intercom/com/bumptech/glide/load/resource/bitmap/p;
.super Ljava/lang/Object;
.source "StreamBitmapDecoder.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/com/bumptech/glide/load/resource/bitmap/p$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/j",
        "<",
        "Ljava/io/InputStream;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;

.field private final b:Lio/intercom/com/bumptech/glide/load/engine/a/b;


# direct methods
.method public constructor <init>(Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;Lio/intercom/com/bumptech/glide/load/engine/a/b;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;

    .line 24
    iput-object p2, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p;->b:Lio/intercom/com/bumptech/glide/load/engine/a/b;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "II",
            "Lio/intercom/com/bumptech/glide/load/i;",
            ")",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    instance-of v0, p1, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    if-eqz v0, :cond_1

    .line 40
    check-cast p1, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    .line 41
    const/4 v0, 0x0

    move v6, v0

    .line 51
    :goto_0
    invoke-static {p1}, Lio/intercom/com/bumptech/glide/h/c;->a(Ljava/io/InputStream;)Lio/intercom/com/bumptech/glide/h/c;

    move-result-object v7

    .line 57
    new-instance v1, Lio/intercom/com/bumptech/glide/h/f;

    invoke-direct {v1, v7}, Lio/intercom/com/bumptech/glide/h/f;-><init>(Ljava/io/InputStream;)V

    .line 58
    new-instance v5, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p$a;

    invoke-direct {v5, p1, v7}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p$a;-><init>(Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;Lio/intercom/com/bumptech/glide/h/c;)V

    .line 60
    :try_start_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;->a(Ljava/io/InputStream;IILio/intercom/com/bumptech/glide/load/i;Lio/intercom/com/bumptech/glide/load/resource/bitmap/l$a;)Lio/intercom/com/bumptech/glide/load/engine/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 62
    invoke-virtual {v7}, Lio/intercom/com/bumptech/glide/h/c;->b()V

    .line 63
    if-eqz v6, :cond_0

    .line 64
    invoke-virtual {p1}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;->b()V

    .line 60
    :cond_0
    return-object v0

    .line 43
    :cond_1
    new-instance v1, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p;->b:Lio/intercom/com/bumptech/glide/load/engine/a/b;

    invoke-direct {v1, p1, v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;-><init>(Ljava/io/InputStream;Lio/intercom/com/bumptech/glide/load/engine/a/b;)V

    .line 44
    const/4 v0, 0x1

    move v6, v0

    move-object p1, v1

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Lio/intercom/com/bumptech/glide/h/c;->b()V

    .line 63
    if-eqz v6, :cond_2

    .line 64
    invoke-virtual {p1}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;->b()V

    :cond_2
    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {p0, p1, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p;->a(Ljava/io/InputStream;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/InputStream;Lio/intercom/com/bumptech/glide/load/i;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;->a(Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lio/intercom/com/bumptech/glide/load/i;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {p0, p1, p2}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/p;->a(Ljava/io/InputStream;Lio/intercom/com/bumptech/glide/load/i;)Z

    move-result v0

    return v0
.end method
