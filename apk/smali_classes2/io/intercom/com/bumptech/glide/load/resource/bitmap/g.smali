.class public Lio/intercom/com/bumptech/glide/load/resource/bitmap/g;
.super Ljava/lang/Object;
.source "ByteBufferBitmapDecoder.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/j",
        "<",
        "Ljava/nio/ByteBuffer;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;


# direct methods
.method public constructor <init>(Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/g;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;

    .line 20
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p1, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, p1, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/g;->a(Ljava/nio/ByteBuffer;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/nio/ByteBuffer;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "Lio/intercom/com/bumptech/glide/load/i;",
            ")",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-static {p1}, Lio/intercom/com/bumptech/glide/h/a;->b(Ljava/nio/ByteBuffer;)Ljava/io/InputStream;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/g;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;

    invoke-virtual {v1, v0, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;->a(Ljava/io/InputStream;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lio/intercom/com/bumptech/glide/load/i;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p1, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, p1, p2}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/g;->a(Ljava/nio/ByteBuffer;Lio/intercom/com/bumptech/glide/load/i;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/nio/ByteBuffer;Lio/intercom/com/bumptech/glide/load/i;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/g;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/l;->a(Ljava/nio/ByteBuffer;)Z

    move-result v0

    return v0
.end method
