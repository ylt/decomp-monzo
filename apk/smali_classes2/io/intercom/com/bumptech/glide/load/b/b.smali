.class public Lio/intercom/com/bumptech/glide/load/b/b;
.super Ljava/lang/Object;
.source "ByteArrayLoader.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/b/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/com/bumptech/glide/load/b/b$d;,
        Lio/intercom/com/bumptech/glide/load/b/b$a;,
        Lio/intercom/com/bumptech/glide/load/b/b$c;,
        Lio/intercom/com/bumptech/glide/load/b/b$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Data:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/b/m",
        "<[BTData;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/intercom/com/bumptech/glide/load/b/b$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/load/b/b$b",
            "<TData;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/intercom/com/bumptech/glide/load/b/b$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/load/b/b$b",
            "<TData;>;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/load/b/b;->a:Lio/intercom/com/bumptech/glide/load/b/b$b;

    .line 25
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/b/m$a;
    .locals 1

    .prologue
    .line 20
    check-cast p1, [B

    invoke-virtual {p0, p1, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/b/b;->a([BIILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/b/m$a;

    move-result-object v0

    return-object v0
.end method

.method public a([BIILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/b/m$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Lio/intercom/com/bumptech/glide/load/i;",
            ")",
            "Lio/intercom/com/bumptech/glide/load/b/m$a",
            "<TData;>;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lio/intercom/com/bumptech/glide/load/b/m$a;

    invoke-static {}, Lio/intercom/com/bumptech/glide/g/a;->a()Lio/intercom/com/bumptech/glide/g/a;

    move-result-object v1

    new-instance v2, Lio/intercom/com/bumptech/glide/load/b/b$c;

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/load/b/b;->a:Lio/intercom/com/bumptech/glide/load/b/b$b;

    invoke-direct {v2, p1, v3}, Lio/intercom/com/bumptech/glide/load/b/b$c;-><init>([BLio/intercom/com/bumptech/glide/load/b/b$b;)V

    invoke-direct {v0, v1, v2}, Lio/intercom/com/bumptech/glide/load/b/m$a;-><init>(Lio/intercom/com/bumptech/glide/load/g;Lio/intercom/com/bumptech/glide/load/a/b;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 20
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lio/intercom/com/bumptech/glide/load/b/b;->a([B)Z

    move-result v0

    return v0
.end method

.method public a([B)Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method
