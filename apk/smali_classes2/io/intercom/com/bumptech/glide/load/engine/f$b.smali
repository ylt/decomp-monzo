.class final Lio/intercom/com/bumptech/glide/load/engine/f$b;
.super Ljava/lang/Object;
.source "DecodeJob.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/engine/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/com/bumptech/glide/load/engine/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Z:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/engine/g$a",
        "<TZ;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lio/intercom/com/bumptech/glide/load/engine/f;

.field private final b:Lio/intercom/com/bumptech/glide/load/a;


# direct methods
.method constructor <init>(Lio/intercom/com/bumptech/glide/load/engine/f;Lio/intercom/com/bumptech/glide/load/a;)V
    .locals 0

    .prologue
    .line 509
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 510
    iput-object p2, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->b:Lio/intercom/com/bumptech/glide/load/a;

    .line 511
    return-void
.end method

.method private b(Lio/intercom/com/bumptech/glide/load/engine/r;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<TZ;>;)",
            "Ljava/lang/Class",
            "<TZ;>;"
        }
    .end annotation

    .prologue
    .line 563
    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/load/engine/r;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lio/intercom/com/bumptech/glide/load/engine/r;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<TZ;>;)",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<TZ;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 515
    invoke-direct {p0, p1}, Lio/intercom/com/bumptech/glide/load/engine/f$b;->b(Lio/intercom/com/bumptech/glide/load/engine/r;)Ljava/lang/Class;

    move-result-object v6

    .line 518
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->b:Lio/intercom/com/bumptech/glide/load/a;

    sget-object v2, Lio/intercom/com/bumptech/glide/load/a;->d:Lio/intercom/com/bumptech/glide/load/a;

    if-eq v0, v2, :cond_7

    .line 519
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v0, v0, Lio/intercom/com/bumptech/glide/load/engine/f;->a:Lio/intercom/com/bumptech/glide/load/engine/e;

    invoke-virtual {v0, v6}, Lio/intercom/com/bumptech/glide/load/engine/e;->c(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/load/l;

    move-result-object v5

    .line 520
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    invoke-static {v0}, Lio/intercom/com/bumptech/glide/load/engine/f;->b(Lio/intercom/com/bumptech/glide/load/engine/f;)Lio/intercom/com/bumptech/glide/e;

    move-result-object v0

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget v2, v2, Lio/intercom/com/bumptech/glide/load/engine/f;->d:I

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget v3, v3, Lio/intercom/com/bumptech/glide/load/engine/f;->e:I

    invoke-interface {v5, v0, p1, v2, v3}, Lio/intercom/com/bumptech/glide/load/l;->transform(Landroid/content/Context;Lio/intercom/com/bumptech/glide/load/engine/r;II)Lio/intercom/com/bumptech/glide/load/engine/r;

    move-result-object v0

    move-object v8, v0

    .line 523
    :goto_0
    invoke-virtual {p1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/load/engine/r;->e()V

    .line 529
    :cond_0
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v0, v0, Lio/intercom/com/bumptech/glide/load/engine/f;->a:Lio/intercom/com/bumptech/glide/load/engine/e;

    invoke-virtual {v0, v8}, Lio/intercom/com/bumptech/glide/load/engine/e;->a(Lio/intercom/com/bumptech/glide/load/engine/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v0, v0, Lio/intercom/com/bumptech/glide/load/engine/f;->a:Lio/intercom/com/bumptech/glide/load/engine/e;

    invoke-virtual {v0, v8}, Lio/intercom/com/bumptech/glide/load/engine/e;->b(Lio/intercom/com/bumptech/glide/load/engine/r;)Lio/intercom/com/bumptech/glide/load/k;

    move-result-object v1

    .line 531
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v0, v0, Lio/intercom/com/bumptech/glide/load/engine/f;->g:Lio/intercom/com/bumptech/glide/load/i;

    invoke-interface {v1, v0}, Lio/intercom/com/bumptech/glide/load/k;->a(Lio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/c;

    move-result-object v0

    move-object v9, v1

    .line 538
    :goto_1
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/load/engine/f;->a:Lio/intercom/com/bumptech/glide/load/engine/e;

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v2, v2, Lio/intercom/com/bumptech/glide/load/engine/f;->h:Lio/intercom/com/bumptech/glide/load/g;

    invoke-virtual {v1, v2}, Lio/intercom/com/bumptech/glide/load/engine/e;->a(Lio/intercom/com/bumptech/glide/load/g;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 539
    :goto_2
    iget-object v2, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v2, v2, Lio/intercom/com/bumptech/glide/load/engine/f;->f:Lio/intercom/com/bumptech/glide/load/engine/h;

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->b:Lio/intercom/com/bumptech/glide/load/a;

    invoke-virtual {v2, v1, v3, v0}, Lio/intercom/com/bumptech/glide/load/engine/h;->a(ZLio/intercom/com/bumptech/glide/load/a;Lio/intercom/com/bumptech/glide/load/c;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 541
    if-nez v9, :cond_3

    .line 542
    new-instance v0, Lio/intercom/com/bumptech/glide/Registry$NoResultEncoderAvailableException;

    invoke-interface {v8}, Lio/intercom/com/bumptech/glide/load/engine/r;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/com/bumptech/glide/Registry$NoResultEncoderAvailableException;-><init>(Ljava/lang/Class;)V

    throw v0

    .line 534
    :cond_1
    sget-object v0, Lio/intercom/com/bumptech/glide/load/c;->c:Lio/intercom/com/bumptech/glide/load/c;

    move-object v9, v1

    goto :goto_1

    .line 538
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 545
    :cond_3
    sget-object v1, Lio/intercom/com/bumptech/glide/load/c;->a:Lio/intercom/com/bumptech/glide/load/c;

    if-ne v0, v1, :cond_5

    .line 546
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/b;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/load/engine/f;->h:Lio/intercom/com/bumptech/glide/load/g;

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v2, v2, Lio/intercom/com/bumptech/glide/load/engine/f;->c:Lio/intercom/com/bumptech/glide/load/g;

    invoke-direct {v0, v1, v2}, Lio/intercom/com/bumptech/glide/load/engine/b;-><init>(Lio/intercom/com/bumptech/glide/load/g;Lio/intercom/com/bumptech/glide/load/g;)V

    .line 554
    :goto_3
    invoke-static {v8}, Lio/intercom/com/bumptech/glide/load/engine/q;->a(Lio/intercom/com/bumptech/glide/load/engine/r;)Lio/intercom/com/bumptech/glide/load/engine/q;

    move-result-object v8

    .line 555
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/load/engine/f;->b:Lio/intercom/com/bumptech/glide/load/engine/f$c;

    invoke-virtual {v1, v0, v9, v8}, Lio/intercom/com/bumptech/glide/load/engine/f$c;->a(Lio/intercom/com/bumptech/glide/load/g;Lio/intercom/com/bumptech/glide/load/k;Lio/intercom/com/bumptech/glide/load/engine/q;)V

    .line 558
    :cond_4
    return-object v8

    .line 547
    :cond_5
    sget-object v1, Lio/intercom/com/bumptech/glide/load/c;->b:Lio/intercom/com/bumptech/glide/load/c;

    if-ne v0, v1, :cond_6

    .line 548
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/t;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v1, v1, Lio/intercom/com/bumptech/glide/load/engine/f;->h:Lio/intercom/com/bumptech/glide/load/g;

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v2, v2, Lio/intercom/com/bumptech/glide/load/engine/f;->c:Lio/intercom/com/bumptech/glide/load/g;

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget v3, v3, Lio/intercom/com/bumptech/glide/load/engine/f;->d:I

    iget-object v4, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget v4, v4, Lio/intercom/com/bumptech/glide/load/engine/f;->e:I

    iget-object v7, p0, Lio/intercom/com/bumptech/glide/load/engine/f$b;->a:Lio/intercom/com/bumptech/glide/load/engine/f;

    iget-object v7, v7, Lio/intercom/com/bumptech/glide/load/engine/f;->g:Lio/intercom/com/bumptech/glide/load/i;

    invoke-direct/range {v0 .. v7}, Lio/intercom/com/bumptech/glide/load/engine/t;-><init>(Lio/intercom/com/bumptech/glide/load/g;Lio/intercom/com/bumptech/glide/load/g;IILio/intercom/com/bumptech/glide/load/l;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/i;)V

    goto :goto_3

    .line 551
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown strategy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object v8, p1

    move-object v5, v1

    goto/16 :goto_0
.end method
