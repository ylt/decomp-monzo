.class public Lio/intercom/com/bumptech/glide/load/b/f;
.super Ljava/lang/Object;
.source "FileLoader.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/b/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/com/bumptech/glide/load/b/f$b;,
        Lio/intercom/com/bumptech/glide/load/b/f$e;,
        Lio/intercom/com/bumptech/glide/load/b/f$a;,
        Lio/intercom/com/bumptech/glide/load/b/f$c;,
        Lio/intercom/com/bumptech/glide/load/b/f$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Data:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/b/m",
        "<",
        "Ljava/io/File;",
        "TData;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/intercom/com/bumptech/glide/load/b/f$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/load/b/f$d",
            "<TData;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/intercom/com/bumptech/glide/load/b/f$d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/load/b/f$d",
            "<TData;>;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/load/b/f;->a:Lio/intercom/com/bumptech/glide/load/b/f$d;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Ljava/io/File;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/b/m$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "II",
            "Lio/intercom/com/bumptech/glide/load/i;",
            ")",
            "Lio/intercom/com/bumptech/glide/load/b/m$a",
            "<TData;>;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lio/intercom/com/bumptech/glide/load/b/m$a;

    new-instance v1, Lio/intercom/com/bumptech/glide/g/b;

    invoke-direct {v1, p1}, Lio/intercom/com/bumptech/glide/g/b;-><init>(Ljava/lang/Object;)V

    new-instance v2, Lio/intercom/com/bumptech/glide/load/b/f$c;

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/load/b/f;->a:Lio/intercom/com/bumptech/glide/load/b/f$d;

    invoke-direct {v2, p1, v3}, Lio/intercom/com/bumptech/glide/load/b/f$c;-><init>(Ljava/io/File;Lio/intercom/com/bumptech/glide/load/b/f$d;)V

    invoke-direct {v0, v1, v2}, Lio/intercom/com/bumptech/glide/load/b/m$a;-><init>(Lio/intercom/com/bumptech/glide/load/g;Lio/intercom/com/bumptech/glide/load/a/b;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/b/m$a;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/b/f;->a(Ljava/io/File;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/b/m$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/File;)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lio/intercom/com/bumptech/glide/load/b/f;->a(Ljava/io/File;)Z

    move-result v0

    return v0
.end method
