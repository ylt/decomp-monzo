.class Lio/intercom/com/bumptech/glide/load/engine/f$c;
.super Ljava/lang/Object;
.source "DecodeJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/com/bumptech/glide/load/engine/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Z:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Lio/intercom/com/bumptech/glide/load/g;

.field private b:Lio/intercom/com/bumptech/glide/load/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/load/k",
            "<TZ;>;"
        }
    .end annotation
.end field

.field private c:Lio/intercom/com/bumptech/glide/load/engine/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/load/engine/q",
            "<TZ;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(Lio/intercom/com/bumptech/glide/load/engine/f$d;Lio/intercom/com/bumptech/glide/load/i;)V
    .locals 5

    .prologue
    .line 625
    const-string v0, "DecodeJob.encode"

    invoke-static {v0}, Landroid/support/v4/os/e;->a(Ljava/lang/String;)V

    .line 627
    :try_start_0
    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/load/engine/f$d;->a()Lio/intercom/com/bumptech/glide/load/engine/b/a;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->a:Lio/intercom/com/bumptech/glide/load/g;

    new-instance v2, Lio/intercom/com/bumptech/glide/load/engine/c;

    iget-object v3, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->b:Lio/intercom/com/bumptech/glide/load/k;

    iget-object v4, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->c:Lio/intercom/com/bumptech/glide/load/engine/q;

    invoke-direct {v2, v3, v4, p2}, Lio/intercom/com/bumptech/glide/load/engine/c;-><init>(Lio/intercom/com/bumptech/glide/load/d;Ljava/lang/Object;Lio/intercom/com/bumptech/glide/load/i;)V

    invoke-interface {v0, v1, v2}, Lio/intercom/com/bumptech/glide/load/engine/b/a;->a(Lio/intercom/com/bumptech/glide/load/g;Lio/intercom/com/bumptech/glide/load/engine/b/a$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 630
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->c:Lio/intercom/com/bumptech/glide/load/engine/q;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/engine/q;->a()V

    .line 631
    invoke-static {}, Landroid/support/v4/os/e;->a()V

    .line 633
    return-void

    .line 630
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->c:Lio/intercom/com/bumptech/glide/load/engine/q;

    invoke-virtual {v1}, Lio/intercom/com/bumptech/glide/load/engine/q;->a()V

    .line 631
    invoke-static {}, Landroid/support/v4/os/e;->a()V

    throw v0
.end method

.method a(Lio/intercom/com/bumptech/glide/load/g;Lio/intercom/com/bumptech/glide/load/k;Lio/intercom/com/bumptech/glide/load/engine/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/intercom/com/bumptech/glide/load/g;",
            "Lio/intercom/com/bumptech/glide/load/k",
            "<TX;>;",
            "Lio/intercom/com/bumptech/glide/load/engine/q",
            "<TX;>;)V"
        }
    .end annotation

    .prologue
    .line 619
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->a:Lio/intercom/com/bumptech/glide/load/g;

    .line 620
    iput-object p2, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->b:Lio/intercom/com/bumptech/glide/load/k;

    .line 621
    iput-object p3, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->c:Lio/intercom/com/bumptech/glide/load/engine/q;

    .line 622
    return-void
.end method

.method a()Z
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->c:Lio/intercom/com/bumptech/glide/load/engine/q;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 640
    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->a:Lio/intercom/com/bumptech/glide/load/g;

    .line 641
    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->b:Lio/intercom/com/bumptech/glide/load/k;

    .line 642
    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/engine/f$c;->c:Lio/intercom/com/bumptech/glide/load/engine/q;

    .line 643
    return-void
.end method
