.class public Lio/intercom/com/bumptech/glide/load/resource/bitmap/a;
.super Ljava/lang/Object;
.source "BitmapDrawableDecoder.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DataType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/j",
        "<TDataType;",
        "Landroid/graphics/drawable/BitmapDrawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lio/intercom/com/bumptech/glide/load/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/load/j",
            "<TDataType;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/res/Resources;

.field private final c:Lio/intercom/com/bumptech/glide/load/engine/a/e;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lio/intercom/com/bumptech/glide/load/engine/a/e;Lio/intercom/com/bumptech/glide/load/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lio/intercom/com/bumptech/glide/load/engine/a/e;",
            "Lio/intercom/com/bumptech/glide/load/j",
            "<TDataType;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/a;->b:Landroid/content/res/Resources;

    .line 33
    invoke-static {p2}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/load/engine/a/e;

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/a;->c:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    .line 34
    invoke-static {p3}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/load/j;

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/a;->a:Lio/intercom/com/bumptech/glide/load/j;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDataType;II",
            "Lio/intercom/com/bumptech/glide/load/i;",
            ")",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/a;->a:Lio/intercom/com/bumptech/glide/load/j;

    invoke-interface {v0, p1, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/j;->a(Ljava/lang/Object;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/a;->b:Landroid/content/res/Resources;

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/a;->c:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    invoke-interface {v0}, Lio/intercom/com/bumptech/glide/load/engine/r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v1, v2, v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/o;->a(Landroid/content/res/Resources;Lio/intercom/com/bumptech/glide/load/engine/a/e;Landroid/graphics/Bitmap;)Lio/intercom/com/bumptech/glide/load/resource/bitmap/o;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Lio/intercom/com/bumptech/glide/load/i;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDataType;",
            "Lio/intercom/com/bumptech/glide/load/i;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/a;->a:Lio/intercom/com/bumptech/glide/load/j;

    invoke-interface {v0, p1, p2}, Lio/intercom/com/bumptech/glide/load/j;->a(Ljava/lang/Object;Lio/intercom/com/bumptech/glide/load/i;)Z

    move-result v0

    return v0
.end method
