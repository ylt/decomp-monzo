.class public final Lio/intercom/com/bumptech/glide/load/a/i;
.super Ljava/lang/Object;
.source "InputStreamRewinder.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/a/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/com/bumptech/glide/load/a/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/a/c",
        "<",
        "Ljava/io/InputStream;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;Lio/intercom/com/bumptech/glide/load/engine/a/b;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    invoke-direct {v0, p1, p2}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;-><init>(Ljava/io/InputStream;Lio/intercom/com/bumptech/glide/load/engine/a/b;)V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/a/i;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    .line 20
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/a/i;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    const/high16 v1, 0x500000

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;->mark(I)V

    .line 21
    return-void
.end method


# virtual methods
.method public synthetic a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0}, Lio/intercom/com/bumptech/glide/load/a/i;->c()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/a/i;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;->b()V

    .line 32
    return-void
.end method

.method public c()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/a/i;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;->reset()V

    .line 26
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/a/i;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;

    return-object v0
.end method
