.class public Lio/intercom/com/bumptech/glide/load/resource/d/e;
.super Lio/intercom/com/bumptech/glide/load/resource/b/a;
.source "GifDrawableResource.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/engine/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/intercom/com/bumptech/glide/load/resource/b/a",
        "<",
        "Lio/intercom/com/bumptech/glide/load/resource/d/c;",
        ">;",
        "Lio/intercom/com/bumptech/glide/load/engine/o;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lio/intercom/com/bumptech/glide/load/resource/d/c;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lio/intercom/com/bumptech/glide/load/resource/b/a;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 13
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/d/e;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lio/intercom/com/bumptech/glide/load/resource/d/c;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/resource/d/c;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->prepareToDraw()V

    .line 34
    return-void
.end method

.method public b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lio/intercom/com/bumptech/glide/load/resource/d/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    const-class v0, Lio/intercom/com/bumptech/glide/load/resource/d/c;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/d/e;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lio/intercom/com/bumptech/glide/load/resource/d/c;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/resource/d/c;->a()I

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/d/e;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lio/intercom/com/bumptech/glide/load/resource/d/c;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/resource/d/c;->stop()V

    .line 28
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/d/e;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lio/intercom/com/bumptech/glide/load/resource/d/c;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/load/resource/d/c;->g()V

    .line 29
    return-void
.end method
