.class public Lio/intercom/com/bumptech/glide/load/resource/e/b;
.super Ljava/lang/Object;
.source "BitmapDrawableTranscoder.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/resource/e/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/resource/e/d",
        "<",
        "Landroid/graphics/Bitmap;",
        "Landroid/graphics/drawable/BitmapDrawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lio/intercom/com/bumptech/glide/load/engine/a/e;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lio/intercom/com/bumptech/glide/load/engine/a/e;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/e/b;->a:Landroid/content/res/Resources;

    .line 27
    invoke-static {p2}, Lio/intercom/com/bumptech/glide/h/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/com/bumptech/glide/load/engine/a/e;

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/load/resource/e/b;->b:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lio/intercom/com/bumptech/glide/load/engine/r;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/resource/e/b;->a:Landroid/content/res/Resources;

    iget-object v2, p0, Lio/intercom/com/bumptech/glide/load/resource/e/b;->b:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/load/engine/r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v1, v2, v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/o;->a(Landroid/content/res/Resources;Lio/intercom/com/bumptech/glide/load/engine/a/e;Landroid/graphics/Bitmap;)Lio/intercom/com/bumptech/glide/load/resource/bitmap/o;

    move-result-object v0

    return-object v0
.end method
