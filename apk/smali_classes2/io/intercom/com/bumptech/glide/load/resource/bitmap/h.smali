.class public Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;
.super Lio/intercom/com/bumptech/glide/load/resource/bitmap/f;
.source "CenterCrop.java"


# static fields
.field private static final ID:Ljava/lang/String; = "io.intercom.com.bumptech.glide.load.resource.bitmap.CenterCrop"

.field private static final ID_BYTES:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    const-string v0, "io.intercom.com.bumptech.glide.load.resource.bitmap.CenterCrop"

    sget-object v1, Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    sput-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;->ID_BYTES:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/f;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(Lio/intercom/com/bumptech/glide/load/engine/a/e;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 44
    instance-of v0, p1, Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 49
    const-string v0, "io.intercom.com.bumptech.glide.load.resource.bitmap.CenterCrop"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method protected transform(Lio/intercom/com/bumptech/glide/load/engine/a/e;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 39
    invoke-static {p1, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/q;->a(Lio/intercom/com/bumptech/glide/load/engine/a/e;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public updateDiskCacheKey(Ljava/security/MessageDigest;)V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;->ID_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 55
    return-void
.end method
