.class public final Lio/intercom/com/bumptech/glide/load/resource/d/h;
.super Ljava/lang/Object;
.source "GifFrameResourceDecoder.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/j",
        "<",
        "Lio/intercom/com/bumptech/glide/b/a;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lio/intercom/com/bumptech/glide/load/engine/a/e;


# direct methods
.method public constructor <init>(Lio/intercom/com/bumptech/glide/load/engine/a/e;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/load/resource/d/h;->a:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Lio/intercom/com/bumptech/glide/b/a;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/b/a;",
            "II",
            "Lio/intercom/com/bumptech/glide/load/i;",
            ")",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/b/a;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/resource/d/h;->a:Lio/intercom/com/bumptech/glide/load/engine/a/e;

    invoke-static {v0, v1}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/e;->a(Landroid/graphics/Bitmap;Lio/intercom/com/bumptech/glide/load/engine/a/e;)Lio/intercom/com/bumptech/glide/load/resource/bitmap/e;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p1, Lio/intercom/com/bumptech/glide/b/a;

    invoke-virtual {p0, p1, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/resource/d/h;->a(Lio/intercom/com/bumptech/glide/b/a;IILio/intercom/com/bumptech/glide/load/i;)Lio/intercom/com/bumptech/glide/load/engine/r;

    move-result-object v0

    return-object v0
.end method

.method public a(Lio/intercom/com/bumptech/glide/b/a;Lio/intercom/com/bumptech/glide/load/i;)Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lio/intercom/com/bumptech/glide/load/i;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p1, Lio/intercom/com/bumptech/glide/b/a;

    invoke-virtual {p0, p1, p2}, Lio/intercom/com/bumptech/glide/load/resource/d/h;->a(Lio/intercom/com/bumptech/glide/b/a;Lio/intercom/com/bumptech/glide/load/i;)Z

    move-result v0

    return v0
.end method
