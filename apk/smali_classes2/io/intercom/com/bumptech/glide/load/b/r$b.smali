.class public Lio/intercom/com/bumptech/glide/load/b/r$b;
.super Ljava/lang/Object;
.source "ResourceLoader.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/load/b/n;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/com/bumptech/glide/load/b/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/load/b/n",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/io/InputStream;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/load/b/r$b;->a:Landroid/content/res/Resources;

    .line 65
    return-void
.end method


# virtual methods
.method public a(Lio/intercom/com/bumptech/glide/load/b/q;)Lio/intercom/com/bumptech/glide/load/b/m;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/load/b/q;",
            ")",
            "Lio/intercom/com/bumptech/glide/load/b/m",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lio/intercom/com/bumptech/glide/load/b/r;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/load/b/r$b;->a:Landroid/content/res/Resources;

    const-class v2, Landroid/net/Uri;

    const-class v3, Ljava/io/InputStream;

    invoke-virtual {p1, v2, v3}, Lio/intercom/com/bumptech/glide/load/b/q;->a(Ljava/lang/Class;Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/load/b/m;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/intercom/com/bumptech/glide/load/b/r;-><init>(Landroid/content/res/Resources;Lio/intercom/com/bumptech/glide/load/b/m;)V

    return-object v0
.end method
