.class public abstract Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;
.super Ljava/lang/Object;
.source "DownsampleStrategy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$g;,
        Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$c;,
        Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$f;,
        Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$b;,
        Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$a;,
        Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$d;,
        Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$e;
    }
.end annotation


# static fields
.field public static final a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

.field public static final b:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

.field public static final c:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

.field public static final d:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

.field public static final e:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

.field public static final f:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

.field public static final g:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$e;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$e;-><init>()V

    sput-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;->a:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

    .line 36
    new-instance v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$d;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$d;-><init>()V

    sput-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;->b:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

    .line 42
    new-instance v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$a;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$a;-><init>()V

    sput-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;->c:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

    .line 48
    new-instance v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$b;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$b;-><init>()V

    sput-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;->d:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

    .line 57
    new-instance v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$c;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$c;-><init>()V

    sput-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;->e:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

    .line 62
    new-instance v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$f;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$f;-><init>()V

    sput-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;->f:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

    .line 67
    sget-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;->b:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

    sput-object v0, Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;->g:Lio/intercom/com/bumptech/glide/load/resource/bitmap/k;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(IIII)F
.end method

.method public abstract b(IIII)Lio/intercom/com/bumptech/glide/load/resource/bitmap/k$g;
.end method
