.class public Lio/intercom/com/bumptech/glide/Registry;
.super Ljava/lang/Object;
.source "Registry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/com/bumptech/glide/Registry$NoImageHeaderParserException;,
        Lio/intercom/com/bumptech/glide/Registry$MissingComponentException;,
        Lio/intercom/com/bumptech/glide/Registry$NoSourceEncoderAvailableException;,
        Lio/intercom/com/bumptech/glide/Registry$NoResultEncoderAvailableException;,
        Lio/intercom/com/bumptech/glide/Registry$NoModelLoaderAvailableException;
    }
.end annotation


# instance fields
.field private final a:Lio/intercom/com/bumptech/glide/load/b/o;

.field private final b:Lio/intercom/com/bumptech/glide/e/a;

.field private final c:Lio/intercom/com/bumptech/glide/e/e;

.field private final d:Lio/intercom/com/bumptech/glide/e/f;

.field private final e:Lio/intercom/com/bumptech/glide/load/a/d;

.field private final f:Lio/intercom/com/bumptech/glide/load/resource/e/e;

.field private final g:Lio/intercom/com/bumptech/glide/e/b;

.field private final h:Lio/intercom/com/bumptech/glide/e/d;

.field private final i:Lio/intercom/com/bumptech/glide/e/c;

.field private final j:Landroid/support/v4/g/k$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/g/k$a",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Exception;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lio/intercom/com/bumptech/glide/e/d;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/e/d;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->h:Lio/intercom/com/bumptech/glide/e/d;

    .line 45
    new-instance v0, Lio/intercom/com/bumptech/glide/e/c;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/e/c;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->i:Lio/intercom/com/bumptech/glide/e/c;

    .line 46
    invoke-static {}, Lio/intercom/com/bumptech/glide/h/a/a;->a()Landroid/support/v4/g/k$a;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->j:Landroid/support/v4/g/k$a;

    .line 49
    new-instance v0, Lio/intercom/com/bumptech/glide/load/b/o;

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/Registry;->j:Landroid/support/v4/g/k$a;

    invoke-direct {v0, v1}, Lio/intercom/com/bumptech/glide/load/b/o;-><init>(Landroid/support/v4/g/k$a;)V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->a:Lio/intercom/com/bumptech/glide/load/b/o;

    .line 50
    new-instance v0, Lio/intercom/com/bumptech/glide/e/a;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/e/a;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->b:Lio/intercom/com/bumptech/glide/e/a;

    .line 51
    new-instance v0, Lio/intercom/com/bumptech/glide/e/e;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/e/e;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->c:Lio/intercom/com/bumptech/glide/e/e;

    .line 52
    new-instance v0, Lio/intercom/com/bumptech/glide/e/f;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/e/f;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->d:Lio/intercom/com/bumptech/glide/e/f;

    .line 53
    new-instance v0, Lio/intercom/com/bumptech/glide/load/a/d;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/a/d;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->e:Lio/intercom/com/bumptech/glide/load/a/d;

    .line 54
    new-instance v0, Lio/intercom/com/bumptech/glide/load/resource/e/e;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/load/resource/e/e;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->f:Lio/intercom/com/bumptech/glide/load/resource/e/e;

    .line 55
    new-instance v0, Lio/intercom/com/bumptech/glide/e/b;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/e/b;-><init>()V

    iput-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->g:Lio/intercom/com/bumptech/glide/e/b;

    .line 56
    return-void
.end method

.method private c(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            "TResource:",
            "Ljava/lang/Object;",
            "Transcode:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TData;>;",
            "Ljava/lang/Class",
            "<TTResource;>;",
            "Ljava/lang/Class",
            "<TTranscode;>;)",
            "Ljava/util/List",
            "<",
            "Lio/intercom/com/bumptech/glide/load/engine/g",
            "<TData;TTResource;TTranscode;>;>;"
        }
    .end annotation

    .prologue
    .line 294
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 295
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->c:Lio/intercom/com/bumptech/glide/e/e;

    .line 296
    invoke-virtual {v0, p1, p2}, Lio/intercom/com/bumptech/glide/e/e;->b(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 298
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 299
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->f:Lio/intercom/com/bumptech/glide/load/resource/e/e;

    .line 300
    invoke-virtual {v0, v2, p3}, Lio/intercom/com/bumptech/glide/load/resource/e/e;->b(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 302
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    .line 304
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->c:Lio/intercom/com/bumptech/glide/e/e;

    .line 305
    invoke-virtual {v0, p1, v2}, Lio/intercom/com/bumptech/glide/e/e;->a(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 306
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->f:Lio/intercom/com/bumptech/glide/load/resource/e/e;

    .line 307
    invoke-virtual {v0, v2, v3}, Lio/intercom/com/bumptech/glide/load/resource/e/e;->a(Ljava/lang/Class;Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/load/resource/e/d;

    move-result-object v5

    .line 308
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/g;

    iget-object v6, p0, Lio/intercom/com/bumptech/glide/Registry;->j:Landroid/support/v4/g/k$a;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lio/intercom/com/bumptech/glide/load/engine/g;-><init>(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Ljava/util/List;Lio/intercom/com/bumptech/glide/load/resource/e/d;Landroid/support/v4/g/k$a;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 312
    :cond_1
    return-object v7
.end method


# virtual methods
.method public a(Lio/intercom/com/bumptech/glide/load/a/c$a;)Lio/intercom/com/bumptech/glide/Registry;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->e:Lio/intercom/com/bumptech/glide/load/a/d;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/load/a/d;->a(Lio/intercom/com/bumptech/glide/load/a/c$a;)V

    .line 157
    return-object p0
.end method

.method public a(Lio/intercom/com/bumptech/glide/load/e;)Lio/intercom/com/bumptech/glide/Registry;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->g:Lio/intercom/com/bumptech/glide/e/b;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/e/b;->a(Lio/intercom/com/bumptech/glide/load/e;)V

    .line 182
    return-object p0
.end method

.method public a(Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/d;)Lio/intercom/com/bumptech/glide/Registry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TData;>;",
            "Lio/intercom/com/bumptech/glide/load/d",
            "<TData;>;)",
            "Lio/intercom/com/bumptech/glide/Registry;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->b:Lio/intercom/com/bumptech/glide/e/a;

    invoke-virtual {v0, p1, p2}, Lio/intercom/com/bumptech/glide/e/a;->a(Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/d;)V

    .line 73
    return-object p0
.end method

.method public a(Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/k;)Lio/intercom/com/bumptech/glide/Registry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResource:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTResource;>;",
            "Lio/intercom/com/bumptech/glide/load/k",
            "<TTResource;>;)",
            "Lio/intercom/com/bumptech/glide/Registry;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->d:Lio/intercom/com/bumptech/glide/e/f;

    invoke-virtual {v0, p1, p2}, Lio/intercom/com/bumptech/glide/e/f;->a(Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/k;)V

    .line 148
    return-object p0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/b/n;)Lio/intercom/com/bumptech/glide/Registry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Model:",
            "Ljava/lang/Object;",
            "Data:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TModel;>;",
            "Ljava/lang/Class",
            "<TData;>;",
            "Lio/intercom/com/bumptech/glide/load/b/n",
            "<TModel;TData;>;)",
            "Lio/intercom/com/bumptech/glide/Registry;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->a:Lio/intercom/com/bumptech/glide/load/b/o;

    invoke-virtual {v0, p1, p2, p3}, Lio/intercom/com/bumptech/glide/load/b/o;->a(Ljava/lang/Class;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/b/n;)V

    .line 210
    return-object p0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/j;)Lio/intercom/com/bumptech/glide/Registry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            "TResource:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TData;>;",
            "Ljava/lang/Class",
            "<TTResource;>;",
            "Lio/intercom/com/bumptech/glide/load/j",
            "<TData;TTResource;>;)",
            "Lio/intercom/com/bumptech/glide/Registry;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->c:Lio/intercom/com/bumptech/glide/e/e;

    invoke-virtual {v0, p3, p1, p2}, Lio/intercom/com/bumptech/glide/e/e;->a(Lio/intercom/com/bumptech/glide/load/j;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 100
    return-object p0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/resource/e/d;)Lio/intercom/com/bumptech/glide/Registry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TResource:",
            "Ljava/lang/Object;",
            "Transcode:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTResource;>;",
            "Ljava/lang/Class",
            "<TTranscode;>;",
            "Lio/intercom/com/bumptech/glide/load/resource/e/d",
            "<TTResource;TTranscode;>;)",
            "Lio/intercom/com/bumptech/glide/Registry;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->f:Lio/intercom/com/bumptech/glide/load/resource/e/e;

    invoke-virtual {v0, p1, p2, p3}, Lio/intercom/com/bumptech/glide/load/resource/e/e;->a(Ljava/lang/Class;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/resource/e/d;)V

    .line 173
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/load/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            ">(TX;)",
            "Lio/intercom/com/bumptech/glide/load/d",
            "<TX;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/intercom/com/bumptech/glide/Registry$NoSourceEncoderAvailableException;
        }
    .end annotation

    .prologue
    .line 355
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->b:Lio/intercom/com/bumptech/glide/e/a;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/e/a;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/load/d;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_0

    .line 357
    return-object v0

    .line 359
    :cond_0
    new-instance v0, Lio/intercom/com/bumptech/glide/Registry$NoSourceEncoderAvailableException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/com/bumptech/glide/Registry$NoSourceEncoderAvailableException;-><init>(Ljava/lang/Class;)V

    throw v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/load/engine/p;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            "TResource:",
            "Ljava/lang/Object;",
            "Transcode:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TData;>;",
            "Ljava/lang/Class",
            "<TTResource;>;",
            "Ljava/lang/Class",
            "<TTranscode;>;)",
            "Lio/intercom/com/bumptech/glide/load/engine/p",
            "<TData;TTResource;TTranscode;>;"
        }
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->i:Lio/intercom/com/bumptech/glide/e/c;

    .line 274
    invoke-virtual {v0, p1, p2, p3}, Lio/intercom/com/bumptech/glide/e/c;->b(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/load/engine/p;

    move-result-object v0

    .line 275
    if-nez v0, :cond_0

    iget-object v1, p0, Lio/intercom/com/bumptech/glide/Registry;->i:Lio/intercom/com/bumptech/glide/e/c;

    invoke-virtual {v1, p1, p2, p3}, Lio/intercom/com/bumptech/glide/e/c;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 277
    invoke-direct {p0, p1, p2, p3}, Lio/intercom/com/bumptech/glide/Registry;->c(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 280
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    const/4 v0, 0x0

    .line 286
    :goto_0
    iget-object v1, p0, Lio/intercom/com/bumptech/glide/Registry;->i:Lio/intercom/com/bumptech/glide/e/c;

    invoke-virtual {v1, p1, p2, p3, v0}, Lio/intercom/com/bumptech/glide/e/c;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/engine/p;)V

    .line 288
    :cond_0
    return-object v0

    .line 283
    :cond_1
    new-instance v0, Lio/intercom/com/bumptech/glide/load/engine/p;

    iget-object v5, p0, Lio/intercom/com/bumptech/glide/Registry;->j:Landroid/support/v4/g/k$a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lio/intercom/com/bumptech/glide/load/engine/p;-><init>(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Ljava/util/List;Landroid/support/v4/g/k$a;)V

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lio/intercom/com/bumptech/glide/load/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->g:Lio/intercom/com/bumptech/glide/e/b;

    invoke-virtual {v0}, Lio/intercom/com/bumptech/glide/e/b;->a()Ljava/util/List;

    move-result-object v0

    .line 376
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    new-instance v0, Lio/intercom/com/bumptech/glide/Registry$NoImageHeaderParserException;

    invoke-direct {v0}, Lio/intercom/com/bumptech/glide/Registry$NoImageHeaderParserException;-><init>()V

    throw v0

    .line 379
    :cond_0
    return-object v0
.end method

.method public a(Lio/intercom/com/bumptech/glide/load/engine/r;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 341
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->d:Lio/intercom/com/bumptech/glide/e/f;

    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/load/engine/r;->b()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/e/f;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/load/k;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/Class;Ljava/lang/Class;Lio/intercom/com/bumptech/glide/load/j;)Lio/intercom/com/bumptech/glide/Registry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            "TResource:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TData;>;",
            "Ljava/lang/Class",
            "<TTResource;>;",
            "Lio/intercom/com/bumptech/glide/load/j",
            "<TData;TTResource;>;)",
            "Lio/intercom/com/bumptech/glide/Registry;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->c:Lio/intercom/com/bumptech/glide/e/e;

    invoke-virtual {v0, p3, p1, p2}, Lio/intercom/com/bumptech/glide/e/e;->b(Lio/intercom/com/bumptech/glide/load/j;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 127
    return-object p0
.end method

.method public b(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/load/a/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            ">(TX;)",
            "Lio/intercom/com/bumptech/glide/load/a/c",
            "<TX;>;"
        }
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->e:Lio/intercom/com/bumptech/glide/load/a/d;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/load/a/d;->a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/load/a/c;

    move-result-object v0

    return-object v0
.end method

.method public b(Lio/intercom/com/bumptech/glide/load/engine/r;)Lio/intercom/com/bumptech/glide/load/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/intercom/com/bumptech/glide/load/engine/r",
            "<TX;>;)",
            "Lio/intercom/com/bumptech/glide/load/k",
            "<TX;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/intercom/com/bumptech/glide/Registry$NoResultEncoderAvailableException;
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->d:Lio/intercom/com/bumptech/glide/e/f;

    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/load/engine/r;->b()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/e/f;->a(Ljava/lang/Class;)Lio/intercom/com/bumptech/glide/load/k;

    move-result-object v0

    .line 347
    if-eqz v0, :cond_0

    .line 348
    return-object v0

    .line 350
    :cond_0
    new-instance v0, Lio/intercom/com/bumptech/glide/Registry$NoResultEncoderAvailableException;

    invoke-interface {p1}, Lio/intercom/com/bumptech/glide/load/engine/r;->b()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/com/bumptech/glide/Registry$NoResultEncoderAvailableException;-><init>(Ljava/lang/Class;)V

    throw v0
.end method

.method public b(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Model:",
            "Ljava/lang/Object;",
            "TResource:",
            "Ljava/lang/Object;",
            "Transcode:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TModel;>;",
            "Ljava/lang/Class",
            "<TTResource;>;",
            "Ljava/lang/Class",
            "<TTranscode;>;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 317
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->h:Lio/intercom/com/bumptech/glide/e/d;

    invoke-virtual {v0, p1, p2}, Lio/intercom/com/bumptech/glide/e/d;->a(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 319
    if-nez v0, :cond_3

    .line 320
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 321
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->a:Lio/intercom/com/bumptech/glide/load/b/o;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/load/b/o;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 322
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 323
    iget-object v3, p0, Lio/intercom/com/bumptech/glide/Registry;->c:Lio/intercom/com/bumptech/glide/e/e;

    .line 324
    invoke-virtual {v3, v0, p2}, Lio/intercom/com/bumptech/glide/e/e;->b(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 325
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 326
    iget-object v4, p0, Lio/intercom/com/bumptech/glide/Registry;->f:Lio/intercom/com/bumptech/glide/load/resource/e/e;

    .line 327
    invoke-virtual {v4, v0, p3}, Lio/intercom/com/bumptech/glide/load/resource/e/e;->b(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 328
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 329
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 333
    :cond_2
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->h:Lio/intercom/com/bumptech/glide/e/d;

    .line 334
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 333
    invoke-virtual {v0, p1, p2, v2}, Lio/intercom/com/bumptech/glide/e/d;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/List;)V

    move-object v0, v1

    .line 337
    :cond_3
    return-object v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Model:",
            "Ljava/lang/Object;",
            ">(TModel;)",
            "Ljava/util/List",
            "<",
            "Lio/intercom/com/bumptech/glide/load/b/m",
            "<TModel;*>;>;"
        }
    .end annotation

    .prologue
    .line 367
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/Registry;->a:Lio/intercom/com/bumptech/glide/load/b/o;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/load/b/o;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 368
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 369
    new-instance v0, Lio/intercom/com/bumptech/glide/Registry$NoModelLoaderAvailableException;

    invoke-direct {v0, p1}, Lio/intercom/com/bumptech/glide/Registry$NoModelLoaderAvailableException;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 371
    :cond_0
    return-object v0
.end method
