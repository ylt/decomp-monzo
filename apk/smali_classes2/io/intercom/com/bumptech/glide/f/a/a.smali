.class public abstract Lio/intercom/com/bumptech/glide/f/a/a;
.super Ljava/lang/Object;
.source "BaseTarget.java"

# interfaces
.implements Lio/intercom/com/bumptech/glide/f/a/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Z:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/intercom/com/bumptech/glide/f/a/h",
        "<TZ;>;"
    }
.end annotation


# instance fields
.field private request:Lio/intercom/com/bumptech/glide/f/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRequest()Lio/intercom/com/bumptech/glide/f/b;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lio/intercom/com/bumptech/glide/f/a/a;->request:Lio/intercom/com/bumptech/glide/f/b;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public onLoadCleared(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public onLoadFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public onLoadStarted(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public setRequest(Lio/intercom/com/bumptech/glide/f/b;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lio/intercom/com/bumptech/glide/f/a/a;->request:Lio/intercom/com/bumptech/glide/f/b;

    .line 28
    return-void
.end method
