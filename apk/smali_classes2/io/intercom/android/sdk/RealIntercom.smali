.class Lio/intercom/android/sdk/RealIntercom;
.super Lio/intercom/android/sdk/Intercom;
.source "RealIntercom.java"


# instance fields
.field private final activityFinisher:Lio/intercom/android/sdk/utilities/ActivityFinisher;

.field private final apiProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/api/Api;",
            ">;"
        }
    .end annotation
.end field

.field private final appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private final metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

.field private final nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

.field private final overlayManager:Lio/intercom/android/sdk/overlay/OverlayManager;

.field private final resetManager:Lio/intercom/android/sdk/ResetManager;

.field private final store:Lio/intercom/android/sdk/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;"
        }
    .end annotation
.end field

.field private final superDeDuper:Lio/intercom/android/sdk/api/DeDuper;

.field private final systemNotificationManager:Lio/intercom/android/sdk/push/SystemNotificationManager;

.field private final twig:Lio/intercom/android/sdk/twig/Twig;

.field private final unreadCountTracker:Lio/intercom/android/sdk/store/UnreadCountTracker;

.field private final userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

.field private final userUpdateBatcher:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/api/UserUpdateBatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/api/DeDuper;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/nexus/NexusClient;Lio/intercom/android/sdk/store/Store;Lio/intercom/android/sdk/store/UnreadCountTracker;Lio/intercom/android/sdk/metrics/MetricTracker;Landroid/content/Context;Lio/intercom/android/sdk/overlay/OverlayManager;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/push/SystemNotificationManager;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/ResetManager;Lio/intercom/android/sdk/twig/Twig;Lio/intercom/android/sdk/utilities/ActivityFinisher;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/api/DeDuper;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/api/Api;",
            ">;",
            "Lio/intercom/android/sdk/nexus/NexusClient;",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;",
            "Lio/intercom/android/sdk/store/UnreadCountTracker;",
            "Lio/intercom/android/sdk/metrics/MetricTracker;",
            "Landroid/content/Context;",
            "Lio/intercom/android/sdk/overlay/OverlayManager;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Lio/intercom/android/sdk/identity/UserIdentity;",
            "Lio/intercom/android/sdk/push/SystemNotificationManager;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/api/UserUpdateBatcher;",
            ">;",
            "Lio/intercom/android/sdk/ResetManager;",
            "Lio/intercom/android/sdk/twig/Twig;",
            "Lio/intercom/android/sdk/utilities/ActivityFinisher;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Lio/intercom/android/sdk/Intercom;-><init>()V

    .line 90
    iput-object p1, p0, Lio/intercom/android/sdk/RealIntercom;->superDeDuper:Lio/intercom/android/sdk/api/DeDuper;

    .line 91
    iput-object p2, p0, Lio/intercom/android/sdk/RealIntercom;->apiProvider:Lio/intercom/android/sdk/Provider;

    .line 92
    iput-object p3, p0, Lio/intercom/android/sdk/RealIntercom;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    .line 93
    iput-object p4, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    .line 94
    iput-object p5, p0, Lio/intercom/android/sdk/RealIntercom;->unreadCountTracker:Lio/intercom/android/sdk/store/UnreadCountTracker;

    .line 95
    iput-object p6, p0, Lio/intercom/android/sdk/RealIntercom;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    .line 96
    iput-object p7, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    .line 97
    iput-object p8, p0, Lio/intercom/android/sdk/RealIntercom;->overlayManager:Lio/intercom/android/sdk/overlay/OverlayManager;

    .line 98
    iput-object p9, p0, Lio/intercom/android/sdk/RealIntercom;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 99
    iput-object p10, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 100
    iput-object p11, p0, Lio/intercom/android/sdk/RealIntercom;->systemNotificationManager:Lio/intercom/android/sdk/push/SystemNotificationManager;

    .line 101
    iput-object p12, p0, Lio/intercom/android/sdk/RealIntercom;->userUpdateBatcher:Lio/intercom/android/sdk/Provider;

    .line 102
    move-object/from16 v0, p13

    iput-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->resetManager:Lio/intercom/android/sdk/ResetManager;

    .line 103
    move-object/from16 v0, p14

    iput-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 104
    move-object/from16 v0, p15

    iput-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->activityFinisher:Lio/intercom/android/sdk/utilities/ActivityFinisher;

    .line 106
    sget-object v1, Lio/intercom/android/sdk/RealIntercom$3;->$SwitchMap$io$intercom$android$sdk$IntercomPushManager$IntercomPushIntegrationType:[I

    invoke-static {}, Lio/intercom/android/sdk/IntercomPushManager;->getInstalledModuleType()Lio/intercom/android/sdk/IntercomPushManager$IntercomPushIntegrationType;

    move-result-object v2

    invoke-virtual {v2}, Lio/intercom/android/sdk/IntercomPushManager$IntercomPushIntegrationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 131
    invoke-virtual {p11}, Lio/intercom/android/sdk/push/SystemNotificationManager;->deleteNotificationChannels()V

    .line 132
    const-string v1, "No push integration detected"

    move-object/from16 v0, p14

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    .line 134
    :goto_0
    return-void

    .line 108
    :pswitch_0
    new-instance v1, Lio/intercom/android/sdk/exceptions/IntercomIntegrationException;

    const-string v2, "Both Intercom FCM and GCM modules were included. Please include only one of these dependencies in your project."

    invoke-direct {v1, v2}, Lio/intercom/android/sdk/exceptions/IntercomIntegrationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 112
    :pswitch_1
    const-string v1, "Enabling FCM for cloud messaging"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    move-object/from16 v0, p14

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    invoke-virtual {p11}, Lio/intercom/android/sdk/push/SystemNotificationManager;->setUpNotificationChannelsIfSupported()V

    goto :goto_0

    .line 117
    :pswitch_2
    const-string v1, "Enabling GCM for cloud messaging"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    move-object/from16 v0, p14

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    invoke-virtual {p11}, Lio/intercom/android/sdk/push/SystemNotificationManager;->setUpNotificationChannelsIfSupported()V

    .line 119
    invoke-direct {p0}, Lio/intercom/android/sdk/RealIntercom;->setGcmSenderId()V

    .line 120
    new-instance v1, Lio/intercom/android/sdk/RealIntercom$1;

    invoke-direct {v1, p0, p7}, Lio/intercom/android/sdk/RealIntercom$1;-><init>(Lio/intercom/android/sdk/RealIntercom;Landroid/content/Context;)V

    invoke-static {v1}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/RealIntercom;)Lio/intercom/android/sdk/overlay/OverlayManager;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->overlayManager:Lio/intercom/android/sdk/overlay/OverlayManager;

    return-object v0
.end method

.method static create(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;)Lio/intercom/android/sdk/Intercom;
    .locals 16

    .prologue
    .line 71
    invoke-static/range {p0 .. p2}, Lio/intercom/android/sdk/Injector;->initWithAppCredentials(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v7

    .line 74
    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getUserIdentity()Lio/intercom/android/sdk/identity/UserIdentity;

    move-result-object v10

    .line 75
    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getStore()Lio/intercom/android/sdk/store/Store;

    move-result-object v4

    .line 76
    new-instance v0, Lio/intercom/android/sdk/RealIntercom;

    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getDeDuper()Lio/intercom/android/sdk/api/DeDuper;

    move-result-object v1

    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getApiProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v2

    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getNexusClient()Lio/intercom/android/sdk/nexus/NexusClient;

    move-result-object v3

    new-instance v5, Lio/intercom/android/sdk/store/UnreadCountTracker;

    invoke-direct {v5, v4}, Lio/intercom/android/sdk/store/UnreadCountTracker;-><init>(Lio/intercom/android/sdk/store/Store;)V

    .line 77
    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getMetricTracker()Lio/intercom/android/sdk/metrics/MetricTracker;

    move-result-object v6

    .line 78
    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getOverlayManager()Lio/intercom/android/sdk/overlay/OverlayManager;

    move-result-object v8

    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getAppConfigProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v9

    .line 79
    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getSystemNotificationManager()Lio/intercom/android/sdk/push/SystemNotificationManager;

    move-result-object v11

    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getUserUpdateBatcherProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v12

    .line 80
    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getResetManager()Lio/intercom/android/sdk/ResetManager;

    move-result-object v13

    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v14

    invoke-virtual {v7}, Lio/intercom/android/sdk/Injector;->getActivityFinisher()Lio/intercom/android/sdk/utilities/ActivityFinisher;

    move-result-object v15

    move-object/from16 v7, p0

    invoke-direct/range {v0 .. v15}, Lio/intercom/android/sdk/RealIntercom;-><init>(Lio/intercom/android/sdk/api/DeDuper;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/nexus/NexusClient;Lio/intercom/android/sdk/store/Store;Lio/intercom/android/sdk/store/UnreadCountTracker;Lio/intercom/android/sdk/metrics/MetricTracker;Landroid/content/Context;Lio/intercom/android/sdk/overlay/OverlayManager;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/push/SystemNotificationManager;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/ResetManager;Lio/intercom/android/sdk/twig/Twig;Lio/intercom/android/sdk/utilities/ActivityFinisher;)V

    .line 76
    return-object v0
.end method

.method private logErrorAndOpenInbox(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 330
    invoke-virtual {p0}, Lio/intercom/android/sdk/RealIntercom;->displayConversationsList()V

    .line 331
    return-void
.end method

.method private logEventWithValidation(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 276
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "The event name is null or empty. We can\'t log an event with this string."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userUpdateBatcher:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/api/UserUpdateBatcher;

    invoke-virtual {v0}, Lio/intercom/android/sdk/api/UserUpdateBatcher;->submitPendingUpdate()V

    .line 281
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->apiProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/api/Api;

    invoke-virtual {v0, p1, p2}, Lio/intercom/android/sdk/api/Api;->logEvent(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method private noUserRegistered()Z
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->identityExists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->isSoftReset()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openIntercomChatPush(Ljava/lang/String;Landroid/app/TaskStackBuilder;)V
    .locals 3

    .prologue
    .line 366
    const-string v0, "multiple_notifications"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    invoke-static {v0}, Lio/intercom/android/sdk/activities/IntercomMessengerActivity;->openInbox(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 372
    :goto_0
    if-eqz p2, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    .line 373
    invoke-virtual {p2, v0}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    .line 374
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/app/TaskStackBuilder;->getIntents()[Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivities([Landroid/content/Intent;)V

    .line 378
    :goto_1
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/metrics/MetricTracker;->viewedPushNotification(Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->systemNotificationManager:Lio/intercom/android/sdk/push/SystemNotificationManager;

    invoke-virtual {v0}, Lio/intercom/android/sdk/push/SystemNotificationManager;->clear()V

    .line 381
    return-void

    .line 369
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    sget-object v1, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->NULL:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    invoke-static {v0, p1, v1}, Lio/intercom/android/sdk/activities/IntercomMessengerActivity;->openConversation(Landroid/content/Context;Ljava/lang/String;Lio/intercom/android/sdk/models/LastParticipatingAdmin;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 376
    :cond_1
    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private performUpdate(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 246
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->isUnidentified()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    invoke-static {p1}, Lio/intercom/android/sdk/utilities/AttributeSanitiser;->anonymousSanitisation(Ljava/util/Map;)V

    .line 250
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->superDeDuper:Lio/intercom/android/sdk/api/DeDuper;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/api/DeDuper;->shouldUpdateUser(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->superDeDuper:Lio/intercom/android/sdk/api/DeDuper;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/api/DeDuper;->update(Ljava/util/Map;)V

    .line 252
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->APP_IS_BACKGROUNDED:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 253
    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->userUpdateBatcher:Lio/intercom/android/sdk/Provider;

    invoke-interface {v1}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/intercom/android/sdk/api/UserUpdateBatcher;

    new-instance v2, Lio/intercom/android/sdk/api/UserUpdateRequest;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v3, v0, p1, v3}, Lio/intercom/android/sdk/api/UserUpdateRequest;-><init>(ZZLjava/util/Map;Z)V

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/api/UserUpdateBatcher;->updateUser(Lio/intercom/android/sdk/api/UserUpdateRequest;)V

    .line 255
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "dupe"

    const-string v2, "updated user"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "dupe"

    const-string v2, "dropped dupe"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setGcmSenderId()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_gcm_sender_id:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 139
    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    invoke-static {v1, v0}, Lio/intercom/android/sdk/IntercomPushManager;->cacheSenderId(Landroid/content/Context;Ljava/lang/String;)V

    .line 141
    :cond_0
    return-void
.end method

.method private softRegister()V
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    invoke-static {v0}, Lio/intercom/android/sdk/user/DeviceData;->getDeviceToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->apiProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/api/Api;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/api/Api;->setDeviceToken(Ljava/lang/String;)V

    .line 205
    :cond_0
    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getRealTimeConfig()Lio/intercom/android/sdk/nexus/NexusConfig;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lio/intercom/android/sdk/nexus/NexusClient;->connect(Lio/intercom/android/sdk/nexus/NexusConfig;Z)V

    .line 206
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->softRestart()V

    .line 207
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->resetManager:Lio/intercom/android/sdk/ResetManager;

    invoke-virtual {v0}, Lio/intercom/android/sdk/ResetManager;->clear()V

    .line 211
    new-instance v0, Lio/intercom/android/sdk/RealIntercom$2;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/RealIntercom$2;-><init>(Lio/intercom/android/sdk/RealIntercom;)V

    .line 216
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 217
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_1
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private userIsRegistered(Lio/intercom/android/sdk/identity/Registration;)Z
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->isSoftReset()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/identity/UserIdentity;->isSameUser(Lio/intercom/android/sdk/identity/Registration;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addUnreadConversationCountListener(Lio/intercom/android/sdk/UnreadConversationCountListener;)V
    .locals 1

    .prologue
    .line 394
    if-eqz p1, :cond_0

    .line 395
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->unreadCountTracker:Lio/intercom/android/sdk/store/UnreadCountTracker;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/store/UnreadCountTracker;->addListener(Lio/intercom/android/sdk/UnreadConversationCountListener;)V

    .line 397
    :cond_0
    return-void
.end method

.method public displayConversationsList()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    sget-object v1, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;->CUSTOM:Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/metrics/MetricTracker;->openedMessengerConversationList(Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;)V

    .line 317
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    invoke-static {v1}, Lio/intercom/android/sdk/activities/IntercomMessengerActivity;->openInbox(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 318
    return-void
.end method

.method public displayMessageComposer()V
    .locals 1

    .prologue
    .line 296
    const-string v0, ""

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/RealIntercom;->displayMessageComposer(Ljava/lang/String;)V

    .line 297
    return-void
.end method

.method public displayMessageComposer(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 300
    invoke-direct {p0}, Lio/intercom/android/sdk/RealIntercom;->noUserRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    const-string v0, "The messenger was opened but there was no user registered on this device. Please call registerUnidentifiedUser() or registerIdentifiedUser(Registration)."

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/RealIntercom;->logErrorAndOpenInbox(Ljava/lang/String;)V

    .line 313
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->isReceivedFromServer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 304
    const-string v0, "It appears your app has not received a successful response from Intercom. Please check you are using the correct Android app ID and API Key from the Intercom settings."

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/RealIntercom;->logErrorAndOpenInbox(Ljava/lang/String;)V

    goto :goto_0

    .line 306
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->isInboundMessages()Z

    move-result v0

    if-nez v0, :cond_2

    .line 307
    const-string v0, "It appears your app is not on a plan that allows message composing. As a fallback we are calling displayConversationsList()"

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/RealIntercom;->logErrorAndOpenInbox(Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    sget-object v1, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;->CUSTOM:Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/metrics/MetricTracker;->openedMessengerNewConversation(Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;)V

    .line 311
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Lio/intercom/android/sdk/activities/IntercomMessengerActivity;->openComposer(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public displayMessenger()V
    .locals 5

    .prologue
    .line 286
    invoke-direct {p0}, Lio/intercom/android/sdk/RealIntercom;->noUserRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "The messenger was opened but there was no user registered on this device.Please call registerUnidentifiedUser() or registerIdentifiedUser(Registration)."

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/RealIntercom;->logErrorAndOpenInbox(Ljava/lang/String;)V

    .line 293
    :goto_0
    return-void

    .line 290
    :cond_0
    new-instance v0, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour;

    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v2, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v3, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;->CUSTOM:Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;

    iget-object v4, p0, Lio/intercom/android/sdk/RealIntercom;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-direct {v0, v1, v2, v3, v4}, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour;-><init>(Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/store/Store;Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;Lio/intercom/android/sdk/metrics/MetricTracker;)V

    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    .line 291
    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour;->openMessenger(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public getUnreadConversationCount()I
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    invoke-virtual {v0}, Lio/intercom/android/sdk/store/Store;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/State;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/State;->unreadConversationIds()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public handlePushMessage()V
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/RealIntercom;->handlePushMessage(Landroid/app/TaskStackBuilder;)V

    .line 347
    return-void
.end method

.method public handlePushMessage(Landroid/app/TaskStackBuilder;)V
    .locals 3

    .prologue
    .line 350
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->context:Landroid/content/Context;

    const-string v1, "INTERCOM_SDK_PUSH_PREFS"

    const/4 v2, 0x0

    .line 351
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 353
    const-string v1, "intercom_push_notification_path"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 355
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "No Uri found"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    .line 361
    :goto_0
    return-void

    .line 358
    :cond_0
    invoke-direct {p0, v1, p1}, Lio/intercom/android/sdk/RealIntercom;->openIntercomChatPush(Ljava/lang/String;Landroid/app/TaskStackBuilder;)V

    .line 359
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public hideMessenger()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->activityFinisher:Lio/intercom/android/sdk/utilities/ActivityFinisher;

    invoke-virtual {v0}, Lio/intercom/android/sdk/utilities/ActivityFinisher;->finishActivities()V

    .line 343
    return-void
.end method

.method public logEvent(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 262
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lio/intercom/android/sdk/RealIntercom;->logEventWithValidation(Ljava/lang/String;Ljava/util/Map;)V

    .line 263
    return-void
.end method

.method public logEvent(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 266
    if-nez p2, :cond_1

    .line 267
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "The metadata provided is null, logging event with no metadata"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 268
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 272
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lio/intercom/android/sdk/RealIntercom;->logEventWithValidation(Ljava/lang/String;Ljava/util/Map;)V

    .line 273
    return-void

    .line 269
    :cond_1
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "The metadata provided is empty, logging event with no metadata"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public registerIdentifiedUser(Lio/intercom/android/sdk/identity/Registration;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    if-nez p1, :cond_0

    .line 161
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "The registration object you passed to is null. An example successful call is registerIdentifiedUser(Registration.create().withEmail(email));"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    :goto_0
    return-void

    .line 166
    :cond_0
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/RealIntercom;->userIsRegistered(Lio/intercom/android/sdk/identity/Registration;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    invoke-direct {p0}, Lio/intercom/android/sdk/RealIntercom;->softRegister()V

    goto :goto_0

    .line 169
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->resetManager:Lio/intercom/android/sdk/ResetManager;

    invoke-virtual {v0}, Lio/intercom/android/sdk/ResetManager;->hardReset()V

    .line 170
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/identity/UserIdentity;->canRegisterIdentifiedUser(Lio/intercom/android/sdk/identity/Registration;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 171
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/identity/UserIdentity;->registerIdentifiedUser(Lio/intercom/android/sdk/identity/Registration;)V

    .line 172
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    invoke-virtual {v0}, Lio/intercom/android/sdk/nexus/NexusClient;->disconnect()V

    .line 174
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v3, Lio/intercom/android/sdk/store/Selectors;->SESSION_STARTED_SINCE_LAST_BACKGROUNDED:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 175
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v2, Lio/intercom/android/sdk/store/Selectors;->APP_IS_BACKGROUNDED:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 177
    invoke-virtual {p1}, Lio/intercom/android/sdk/identity/Registration;->getAttributes()Lio/intercom/android/sdk/UserAttributes;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 178
    new-instance v2, Lio/intercom/android/sdk/api/UserUpdateRequest;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 179
    invoke-virtual {p1}, Lio/intercom/android/sdk/identity/Registration;->getAttributes()Lio/intercom/android/sdk/UserAttributes;

    move-result-object v4

    invoke-virtual {v4}, Lio/intercom/android/sdk/UserAttributes;->toMap()Ljava/util/Map;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4, v1}, Lio/intercom/android/sdk/api/UserUpdateRequest;-><init>(ZZLjava/util/Map;Z)V

    move-object v1, v2

    .line 184
    :goto_2
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userUpdateBatcher:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/api/UserUpdateBatcher;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/api/UserUpdateBatcher;->updateUser(Lio/intercom/android/sdk/api/UserUpdateRequest;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 174
    goto :goto_1

    .line 181
    :cond_3
    new-instance v2, Lio/intercom/android/sdk/api/UserUpdateRequest;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v3, v0, v1}, Lio/intercom/android/sdk/api/UserUpdateRequest;-><init>(ZZZ)V

    move-object v1, v2

    goto :goto_2

    .line 185
    :cond_4
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/identity/UserIdentity;->registrationHasAttributes(Lio/intercom/android/sdk/identity/Registration;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 186
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "We already have a registered user. Updating this user with the attributes provided."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    invoke-virtual {p1}, Lio/intercom/android/sdk/identity/Registration;->getAttributes()Lio/intercom/android/sdk/UserAttributes;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/RealIntercom;->updateUser(Lio/intercom/android/sdk/UserAttributes;)V

    goto/16 :goto_0

    .line 190
    :cond_5
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "Failed to register user. We already have a registered user. If you are attempting to register a new user, call reset() before this."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public registerUnidentifiedUser()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 144
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->canRegisterUnidentifiedUser()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->resetManager:Lio/intercom/android/sdk/ResetManager;

    invoke-virtual {v0}, Lio/intercom/android/sdk/ResetManager;->hardReset()V

    .line 146
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->registerUnidentifiedUser()V

    .line 147
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    invoke-virtual {v0}, Lio/intercom/android/sdk/nexus/NexusClient;->disconnect()V

    .line 148
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->APP_IS_BACKGROUNDED:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 149
    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v4, Lio/intercom/android/sdk/store/Selectors;->SESSION_STARTED_SINCE_LAST_BACKGROUNDED:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v1, v4}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 150
    iget-object v1, p0, Lio/intercom/android/sdk/RealIntercom;->userUpdateBatcher:Lio/intercom/android/sdk/Provider;

    invoke-interface {v1}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/intercom/android/sdk/api/UserUpdateBatcher;

    new-instance v4, Lio/intercom/android/sdk/api/UserUpdateRequest;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v4, v3, v0, v2}, Lio/intercom/android/sdk/api/UserUpdateRequest;-><init>(ZZZ)V

    invoke-virtual {v1, v4}, Lio/intercom/android/sdk/api/UserUpdateBatcher;->updateUser(Lio/intercom/android/sdk/api/UserUpdateRequest;)V

    .line 157
    :goto_1
    return-void

    :cond_0
    move v1, v3

    .line 149
    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "Failed to register user. We already have a registered user. If you are attempting to register a new user, call reset() before this. If you are attempting to register an identified user call: registerIdentifiedUser(Registration)"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public removeUnreadConversationCountListener(Lio/intercom/android/sdk/UnreadConversationCountListener;)V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->unreadCountTracker:Lio/intercom/android/sdk/store/UnreadCountTracker;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/store/UnreadCountTracker;->removeListener(Lio/intercom/android/sdk/UnreadConversationCountListener;)V

    .line 401
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->isSoftReset()Z

    move-result v0

    if-nez v0, :cond_0

    .line 385
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->resetManager:Lio/intercom/android/sdk/ResetManager;

    invoke-virtual {v0}, Lio/intercom/android/sdk/ResetManager;->softReset()V

    .line 387
    :cond_0
    return-void
.end method

.method public setBottomPadding(I)V
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {p1}, Lio/intercom/android/sdk/actions/Actions;->setBottomPadding(I)Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 322
    return-void
.end method

.method public setInAppMessageVisibility(Lio/intercom/android/sdk/Intercom$Visibility;)V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {p1}, Lio/intercom/android/sdk/actions/Actions;->setInAppNotificationVisibility(Lio/intercom/android/sdk/Intercom$Visibility;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 335
    return-void
.end method

.method public setLauncherVisibility(Lio/intercom/android/sdk/Intercom$Visibility;)V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {p1}, Lio/intercom/android/sdk/actions/Actions;->setLauncherVisibility(Lio/intercom/android/sdk/Intercom$Visibility;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 339
    return-void
.end method

.method public setUserHash(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 224
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "The user hash you sent us to verify was either null or empty, we will not be able to authenticate your requests without a valid user hash."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->getHmac()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "The user hash set matches the existing user identity hash value"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 230
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/identity/UserIdentity;->softUserIdentityHmacDiffers(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->resetManager:Lio/intercom/android/sdk/ResetManager;

    invoke-virtual {v0}, Lio/intercom/android/sdk/ResetManager;->hardReset()V

    .line 233
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/identity/UserIdentity;->setUserHash(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateUser(Lio/intercom/android/sdk/UserAttributes;)V
    .locals 3

    .prologue
    .line 238
    if-nez p1, :cond_0

    .line 239
    iget-object v0, p0, Lio/intercom/android/sdk/RealIntercom;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "updateUser method failed: the UserAttributes object provided is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-virtual {p1}, Lio/intercom/android/sdk/UserAttributes;->toMap()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/RealIntercom;->performUpdate(Ljava/util/Map;)V

    goto :goto_0
.end method
