.class Lio/intercom/android/sdk/profile/TeammateProfilePresenter;
.super Ljava/lang/Object;
.source "TeammateProfilePresenter.java"


# static fields
.field private static final FADE_DURATION_MS:I = 0x96


# instance fields
.field private final activeStatePresenter:Lio/intercom/android/sdk/views/ActiveStatePresenter;

.field private final activeStateView:Landroid/view/View;

.field private final appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final avatarHolder:Landroid/widget/LinearLayout;

.field private final avatarStroke:Landroid/graphics/drawable/Drawable;

.field private final avatarView1:Landroid/widget/ImageView;

.field private final avatarView2:Landroid/widget/ImageView;

.field private final avatarView3:Landroid/widget/ImageView;

.field private final backgroundImageView:Landroid/widget/ImageView;

.field private final bioView:Landroid/widget/TextView;

.field private final borderSize:I

.field private final bottomSpacer:Landroid/widget/Space;

.field private final groupConversationBanner:Landroid/widget/LinearLayout;

.field lastAdminLocation:Lio/intercom/android/sdk/models/Location;

.field private final linkedInButton:Landroid/widget/ImageButton;

.field final locationView:Landroid/widget/TextView;

.field private final profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

.field private final requestManager:Lio/intercom/com/bumptech/glide/i;

.field private final roleView:Landroid/widget/TextView;

.field private final rootLayout:Landroid/view/ViewGroup;

.field private final socialLayout:Landroid/widget/LinearLayout;

.field private final subtitleView:Landroid/widget/TextView;

.field private final timeUpdate:Ljava/lang/Runnable;

.field private final titleView:Landroid/widget/TextView;

.field private final titleViewNameOnly:Landroid/widget/TextView;

.field private final toolbarBehavior:Lio/intercom/android/sdk/profile/ProfileToolbarBehavior;

.field private final topSpacer:Landroid/widget/Space;

.field private final twitterButton:Landroid/widget/ImageButton;


# direct methods
.method constructor <init>(Landroid/support/design/widget/CoordinatorLayout;ILio/intercom/android/sdk/profile/ProfilePresenter;Lio/intercom/android/sdk/Provider;Lio/intercom/com/bumptech/glide/i;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "I",
            "Lio/intercom/android/sdk/profile/ProfilePresenter;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Lio/intercom/com/bumptech/glide/i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    new-instance v0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter$3;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter$3;-><init>(Lio/intercom/android/sdk/profile/TeammateProfilePresenter;)V

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->timeUpdate:Ljava/lang/Runnable;

    .line 91
    iput-object p3, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    .line 92
    iput-object p4, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 93
    iput-object p5, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 95
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_teammate_profile_container_view:I

    invoke-virtual {p1, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    .line 96
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 98
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_avatar_spacer:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Space;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->topSpacer:Landroid/widget/Space;

    .line 99
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_bottom_spacer:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Space;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->bottomSpacer:Landroid/widget/Space;

    .line 100
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_title:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->titleView:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_title_name_only:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->titleViewNameOnly:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_subtitle:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->subtitleView:Landroid/widget/TextView;

    .line 103
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_role:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->roleView:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_location:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->locationView:Landroid/widget/TextView;

    .line 105
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_bio:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->bioView:Landroid/widget/TextView;

    .line 106
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_teammate_avatar1:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView1:Landroid/widget/ImageView;

    .line 107
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_teammate_avatar2:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView2:Landroid/widget/ImageView;

    .line 108
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_teammate_avatar3:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView3:Landroid/widget/ImageView;

    .line 109
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_collapsing_teammate_active_state:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->activeStateView:Landroid/view/View;

    .line 110
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->twitter_button:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->twitterButton:Landroid/widget/ImageButton;

    .line 111
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    sget v1, Lio/intercom/android/sdk/R$id;->linkedin_button:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->linkedInButton:Landroid/widget/ImageButton;

    .line 112
    sget v0, Lio/intercom/android/sdk/R$id;->collapsing_background_image:I

    invoke-virtual {p1, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->backgroundImageView:Landroid/widget/ImageView;

    .line 113
    sget v0, Lio/intercom/android/sdk/R$id;->social_button_layout:I

    invoke-virtual {p1, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->socialLayout:Landroid/widget/LinearLayout;

    .line 114
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_group_conversations_banner:I

    invoke-virtual {p1, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->groupConversationBanner:Landroid/widget/LinearLayout;

    .line 115
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_group_avatar_holder:I

    invoke-virtual {p1, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarHolder:Landroid/widget/LinearLayout;

    .line 116
    new-instance v0, Lio/intercom/android/sdk/profile/TeammateProfileToolbarBehavior;

    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    invoke-direct {v0, v1}, Lio/intercom/android/sdk/profile/TeammateProfileToolbarBehavior;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->toolbarBehavior:Lio/intercom/android/sdk/profile/ProfileToolbarBehavior;

    .line 117
    new-instance v0, Lio/intercom/android/sdk/views/ActiveStatePresenter;

    invoke-direct {v0}, Lio/intercom/android/sdk/views/ActiveStatePresenter;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->activeStatePresenter:Lio/intercom/android/sdk/views/ActiveStatePresenter;

    .line 119
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    .line 120
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$drawable;->intercom_solid_circle:I

    invoke-static {v0, v1}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarStroke:Landroid/graphics/drawable/Drawable;

    .line 121
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarStroke:Landroid/graphics/drawable/Drawable;

    invoke-interface {p4}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 123
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView1:Landroid/widget/ImageView;

    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarStroke:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lio/intercom/android/sdk/utilities/BackgroundUtils;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 124
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView2:Landroid/widget/ImageView;

    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarStroke:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lio/intercom/android/sdk/utilities/BackgroundUtils;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 125
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView3:Landroid/widget/ImageView;

    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarStroke:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lio/intercom/android/sdk/utilities/BackgroundUtils;->setBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 126
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView1:Landroid/widget/ImageView;

    iget v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v3, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v4, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 127
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView2:Landroid/widget/ImageView;

    iget v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v3, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v4, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 128
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView3:Landroid/widget/ImageView;

    iget v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v3, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    iget v4, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->borderSize:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 129
    return-void
.end method

.method private setGroupConversationParticipants(Ljava/util/List;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Participant;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 266
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 267
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->groupConversationBanner:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 272
    :goto_0
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 274
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 276
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0, v4}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v5

    .line 277
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$dimen;->intercom_group_conversations_banner_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 278
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lio/intercom/android/sdk/R$dimen;->intercom_teammate_avatar_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 279
    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p2, v0

    add-int v1, v6, v5

    div-int/2addr v0, v1

    .line 281
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_4

    .line 282
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    :goto_1
    move v2, v3

    .line 285
    :goto_2
    if-ge v2, v1, :cond_3

    .line 286
    add-int/lit8 v0, v1, -0x1

    if-ne v2, v0, :cond_2

    .line 287
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 288
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 289
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 290
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    const/4 v7, -0x1

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 292
    const/16 v7, 0x11

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 294
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v1

    add-int/lit8 v7, v7, 0x1

    .line 295
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "+"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 296
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v7, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 285
    :cond_0
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 269
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->groupConversationBanner:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 299
    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Participant;

    .line 300
    sget-object v7, Lio/intercom/android/sdk/models/Participant;->NULL:Lio/intercom/android/sdk/models/Participant;

    if-eq v0, v7, :cond_0

    .line 301
    new-instance v7, Landroid/widget/ImageView;

    invoke-direct {v7, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 302
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 303
    invoke-virtual {v8, v3, v3, v5, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 304
    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 305
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Participant;->getAvatar()Lio/intercom/android/sdk/models/Avatar;

    move-result-object v8

    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 306
    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    iget-object v9, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 305
    invoke-static {v8, v7, v0, v9}, Lio/intercom/android/sdk/utilities/AvatarUtils;->loadAvatarIntoView(Lio/intercom/android/sdk/models/Avatar;Landroid/widget/ImageView;Lio/intercom/android/sdk/identity/AppConfig;Lio/intercom/com/bumptech/glide/i;)V

    .line 307
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_3

    .line 311
    :cond_3
    return-void

    :cond_4
    move v1, v0

    goto/16 :goto_1
.end method

.method private setSocialAccounts(Lio/intercom/android/sdk/models/SocialAccount;Lio/intercom/android/sdk/models/SocialAccount;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 140
    sget-object v0, Lio/intercom/android/sdk/models/SocialAccount;->NULL:Lio/intercom/android/sdk/models/SocialAccount;

    if-ne p1, v0, :cond_0

    .line 141
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->twitterButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 153
    :goto_0
    sget-object v0, Lio/intercom/android/sdk/models/SocialAccount;->NULL:Lio/intercom/android/sdk/models/SocialAccount;

    if-ne p2, v0, :cond_1

    .line 154
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->linkedInButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 166
    :goto_1
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->linkedInButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->twitterButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 167
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->socialLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 171
    :goto_2
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->twitterButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->twitterButton:Landroid/widget/ImageButton;

    new-instance v1, Lio/intercom/android/sdk/profile/TeammateProfilePresenter$1;

    invoke-direct {v1, p0, p1, p3}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter$1;-><init>(Lio/intercom/android/sdk/profile/TeammateProfilePresenter;Lio/intercom/android/sdk/models/SocialAccount;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->linkedInButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->linkedInButton:Landroid/widget/ImageButton;

    new-instance v1, Lio/intercom/android/sdk/profile/TeammateProfilePresenter$2;

    invoke-direct {v1, p0, p2, p3}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter$2;-><init>(Lio/intercom/android/sdk/profile/TeammateProfilePresenter;Lio/intercom/android/sdk/models/SocialAccount;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 169
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->socialLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method private setToolbarBackground(Lio/intercom/android/sdk/models/Avatar;I)V
    .locals 3

    .prologue
    .line 174
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Avatar;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 176
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 177
    invoke-virtual {v1, v0}, Lio/intercom/com/bumptech/glide/i;->a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v1

    .line 178
    invoke-static {}, Lio/intercom/com/bumptech/glide/load/resource/b/b;->c()Lio/intercom/com/bumptech/glide/load/resource/b/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/j;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v1

    new-instance v2, Lio/intercom/com/bumptech/glide/f/f;

    invoke-direct {v2}, Lio/intercom/com/bumptech/glide/f/f;-><init>()V

    .line 180
    invoke-static {v0}, Lio/intercom/android/sdk/utilities/ImageUtils;->getDiskCacheStrategy(Ljava/lang/String;)Lio/intercom/com/bumptech/glide/load/engine/h;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/intercom/com/bumptech/glide/f/f;->b(Lio/intercom/com/bumptech/glide/load/engine/h;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    iget-object v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    .line 181
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v2, p2}, Lio/intercom/com/bumptech/glide/f/f;->a(II)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    .line 179
    invoke-virtual {v1, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->backgroundImageView:Landroid/widget/ImageView;

    .line 182
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Landroid/widget/ImageView;)Lio/intercom/com/bumptech/glide/f/a/h;

    .line 184
    :cond_0
    return-void
.end method


# virtual methods
.method getRootLayout()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method getToolbarBehavior()Lio/intercom/android/sdk/profile/ProfileToolbarBehavior;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->toolbarBehavior:Lio/intercom/android/sdk/profile/ProfileToolbarBehavior;

    return-object v0
.end method

.method setPresence(Lio/intercom/android/sdk/models/LastParticipatingAdmin;Ljava/util/List;Ljava/lang/CharSequence;ILandroid/support/design/widget/AppBarLayout;Lio/intercom/android/sdk/profile/ProfilePresenter$ProfileState;Landroid/view/View;Landroid/view/View;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/models/LastParticipatingAdmin;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Participant;",
            ">;",
            "Ljava/lang/CharSequence;",
            "I",
            "Landroid/support/design/widget/AppBarLayout;",
            "Lio/intercom/android/sdk/profile/ProfilePresenter$ProfileState;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->toolbarBehavior:Lio/intercom/android/sdk/profile/ProfileToolbarBehavior;

    invoke-virtual {p5, v0}, Landroid/support/design/widget/AppBarLayout;->b(Landroid/support/design/widget/AppBarLayout$b;)V

    .line 203
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 205
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->isDidShowUnknown()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 206
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 207
    const/4 v0, 0x0

    invoke-virtual {p7, v0}, Landroid/view/View;->setAlpha(F)V

    .line 210
    sget-object v0, Lio/intercom/android/sdk/profile/ProfilePresenter$ProfileState;->EXPANDED:Lio/intercom/android/sdk/profile/ProfilePresenter$ProfileState;

    if-ne p6, v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p5, v0, v1}, Landroid/support/design/widget/AppBarLayout;->a(ZZ)V

    .line 211
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/profile/ProfilePresenter;->setDidShowUnknown(Z)V

    .line 217
    :cond_0
    :goto_1
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 219
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->titleView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getFirstName()Ljava/lang/String;

    move-result-object v2

    .line 220
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    .line 219
    invoke-static {v2, v3, v1}, Lio/intercom/android/sdk/utilities/GroupConversationTextFormatter;->groupConversationTitle(Ljava/lang/String;ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->titleViewNameOnly:Landroid/widget/TextView;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    invoke-virtual {p0, p3}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->setTeammateSubtitle(Ljava/lang/CharSequence;)V

    .line 224
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 225
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getAvatar()Lio/intercom/android/sdk/models/Avatar;

    move-result-object v3

    iget-object v4, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView1:Landroid/widget/ImageView;

    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 226
    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    iget-object v5, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 225
    invoke-static {v3, v4, v0, v5}, Lio/intercom/android/sdk/utilities/AvatarUtils;->loadAvatarIntoView(Lio/intercom/android/sdk/models/Avatar;Landroid/widget/ImageView;Lio/intercom/android/sdk/identity/AppConfig;Lio/intercom/com/bumptech/glide/i;)V

    .line 228
    if-lez v2, :cond_1

    .line 229
    add-int/lit8 v0, v2, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Participant;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Participant;->getAvatar()Lio/intercom/android/sdk/models/Avatar;

    move-result-object v3

    iget-object v4, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView2:Landroid/widget/ImageView;

    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 230
    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    iget-object v5, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 229
    invoke-static {v3, v4, v0, v5}, Lio/intercom/android/sdk/utilities/AvatarUtils;->loadAvatarIntoView(Lio/intercom/android/sdk/models/Avatar;Landroid/widget/ImageView;Lio/intercom/android/sdk/identity/AppConfig;Lio/intercom/com/bumptech/glide/i;)V

    .line 232
    :cond_1
    const/4 v0, 0x1

    if-le v2, v0, :cond_2

    .line 233
    add-int/lit8 v0, v2, -0x2

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Participant;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Participant;->getAvatar()Lio/intercom/android/sdk/models/Avatar;

    move-result-object v2

    iget-object v3, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->avatarView3:Landroid/widget/ImageView;

    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 234
    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    iget-object v4, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 233
    invoke-static {v2, v3, v0, v4}, Lio/intercom/android/sdk/utilities/AvatarUtils;->loadAvatarIntoView(Lio/intercom/android/sdk/models/Avatar;Landroid/widget/ImageView;Lio/intercom/android/sdk/identity/AppConfig;Lio/intercom/com/bumptech/glide/i;)V

    .line 237
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->activeStateView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->activeStatePresenter:Lio/intercom/android/sdk/views/ActiveStatePresenter;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->isActive()Z

    move-result v3

    iget-object v4, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->activeStateView:Landroid/view/View;

    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v2, v3, v4, v0}, Lio/intercom/android/sdk/views/ActiveStatePresenter;->presentStateDot(ZLandroid/view/View;Lio/intercom/android/sdk/identity/AppConfig;)V

    .line 239
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->roleView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getJobTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 240
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getLocation()Lio/intercom/android/sdk/models/Location;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->lastAdminLocation:Lio/intercom/android/sdk/models/Location;

    .line 241
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->timeUpdate:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 242
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->bioView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getIntro()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 243
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getTwitter()Lio/intercom/android/sdk/models/SocialAccount;

    move-result-object v0

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getLinkedIn()Lio/intercom/android/sdk/models/SocialAccount;

    move-result-object v2

    invoke-direct {p0, v0, v2, v1}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->setSocialAccounts(Lio/intercom/android/sdk/models/SocialAccount;Lio/intercom/android/sdk/models/SocialAccount;Landroid/content/Context;)V

    .line 244
    if-nez p4, :cond_3

    .line 245
    invoke-static {v1}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->getScreenDimensions(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget p4, v0, Landroid/graphics/Point;->x:I

    .line 247
    :cond_3
    invoke-direct {p0, p2, p4}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->setGroupConversationParticipants(Ljava/util/List;I)V

    .line 249
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 250
    iget-object v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/view/ViewGroup;->measure(II)V

    .line 251
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 253
    invoke-virtual {p8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 254
    invoke-virtual {p8}, Landroid/view/View;->requestLayout()V

    .line 255
    iget-object v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->backgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 256
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 257
    iget-object v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    sget v3, Lio/intercom/android/sdk/R$dimen;->intercom_toolbar_height:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 258
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestLayout()V

    .line 260
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    iget-object v2, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->toolbarBehavior:Lio/intercom/android/sdk/profile/ProfileToolbarBehavior;

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/profile/ProfilePresenter;->applyOffsetChangedListener(Landroid/support/design/widget/AppBarLayout$b;)V

    .line 262
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getAvatar()Lio/intercom/android/sdk/models/Avatar;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->setToolbarBackground(Lio/intercom/android/sdk/models/Avatar;I)V

    .line 263
    return-void

    .line 210
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 213
    :cond_5
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->rootLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 214
    invoke-virtual {p7}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_1
.end method

.method setTeammateSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    return-void
.end method

.method setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 314
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 322
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 318
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->topSpacer:Landroid/widget/Space;

    invoke-virtual {v0, v1}, Landroid/widget/Space;->setVisibility(I)V

    goto :goto_0
.end method

.method stopUpdatingTime()V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->locationView:Landroid/widget/TextView;

    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeammateProfilePresenter;->timeUpdate:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 330
    return-void
.end method
