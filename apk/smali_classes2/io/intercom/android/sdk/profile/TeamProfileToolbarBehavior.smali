.class Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;
.super Lio/intercom/android/sdk/profile/ProfileToolbarBehavior;
.source "TeamProfileToolbarBehavior.java"


# static fields
.field private static final CHANGE_IN_SCALE:F = 0.3f


# instance fields
.field private avatarMini1:Landroid/widget/ImageView;

.field private avatarMini2:Landroid/widget/ImageView;

.field private avatarMini3:Landroid/widget/ImageView;

.field private bio:Landroid/widget/TextView;

.field private officeHours:Landroid/widget/TextView;

.field private officeHoursBanner:Landroid/view/View;

.field private final rootLayout:Landroid/view/ViewGroup;

.field private separator:Landroid/view/View;

.field private teamProfiles:Landroid/widget/LinearLayout;

.field private final translate16dp:F


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/profile/ProfileToolbarBehavior;-><init>(Landroid/content/Context;)V

    .line 34
    iput-object p1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->rootLayout:Landroid/view/ViewGroup;

    .line 35
    const/high16 v0, 0x41800000    # 16.0f

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->translate16dp:F

    .line 36
    return-void
.end method

.method private updateCollapsedAvatars(Landroid/view/ViewGroup;F)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini1:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 40
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_collapsing_team_avatar1:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini1:Landroid/widget/ImageView;

    .line 42
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini2:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 43
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_collapsing_team_avatar2:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini2:Landroid/widget/ImageView;

    .line 45
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini3:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    .line 46
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_collapsing_team_avatar3:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini3:Landroid/widget/ImageView;

    .line 49
    :cond_2
    const v0, 0x3f333333    # 0.7f

    const v1, 0x3b449ba6    # 0.003f

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    .line 50
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini1:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 51
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini2:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 52
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini3:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 53
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini1:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 54
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini2:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 55
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->avatarMini3:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 56
    return-void
.end method

.method private updateCollapsedBio(Landroid/view/ViewGroup;Landroid/view/View;F)V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->bio:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 70
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_collapsing_team_bio:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->bio:Landroid/widget/TextView;

    .line 73
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->bio:Landroid/widget/TextView;

    const v1, 0x3f333333    # 0.7f

    const/4 v2, 0x0

    invoke-virtual {p0, v0, p2, v1, v2}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->setAlphaAsPercentageOfScroll(Landroid/view/View;Landroid/view/View;FZ)V

    .line 75
    iget v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->translate16dp:F

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    mul-float/2addr v0, p3

    .line 76
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->bio:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 77
    return-void
.end method

.method private updateOfficeHours(Landroid/view/ViewGroup;Landroid/view/View;F)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 80
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->officeHours:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 81
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_collapsing_office_hours:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->officeHours:Landroid/widget/TextView;

    .line 83
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->separator:Landroid/view/View;

    if-nez v0, :cond_1

    .line 84
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_team_profile_separator:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->separator:Landroid/view/View;

    .line 86
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->officeHoursBanner:Landroid/view/View;

    if-nez v0, :cond_2

    .line 87
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_office_hours_banner:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->officeHoursBanner:Landroid/view/View;

    .line 90
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->officeHours:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, p2, v1, v2}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->setAlphaAsPercentageOfScroll(Landroid/view/View;Landroid/view/View;FZ)V

    .line 91
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->separator:Landroid/view/View;

    const v1, 0x3dcccccd    # 0.1f

    invoke-virtual {p0, v0, p2, v1, v2}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->setAlphaAsPercentageOfScroll(Landroid/view/View;Landroid/view/View;FZ)V

    .line 92
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->officeHoursBanner:Landroid/view/View;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {p0, v0, p2, v1, v2}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->setAlphaAsPercentageOfScroll(Landroid/view/View;Landroid/view/View;FZ)V

    .line 94
    iget v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->translate16dp:F

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    mul-float/2addr v0, p3

    .line 95
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->officeHours:Landroid/widget/TextView;

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 96
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->separator:Landroid/view/View;

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 97
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->officeHoursBanner:Landroid/view/View;

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 98
    return-void
.end method

.method private updateTeamProfile(Landroid/view/ViewGroup;Landroid/view/View;F)V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->teamProfiles:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 60
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_team_profiles_layout:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->teamProfiles:Landroid/widget/LinearLayout;

    .line 62
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->teamProfiles:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-virtual {p0, v0, p2, v1, v2}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->setAlphaAsPercentageOfScroll(Landroid/view/View;Landroid/view/View;FZ)V

    .line 64
    iget v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->translate16dp:F

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    mul-float/2addr v0, p3

    .line 65
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->teamProfiles:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    .line 66
    return-void
.end method


# virtual methods
.method public onOffsetChanged(Landroid/support/design/widget/AppBarLayout;I)V
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->toolbarHeight:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-virtual {p0, p1}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->getScrollPercentage(Landroid/view/View;)F

    move-result v0

    .line 106
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->rootLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->getBottom()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 108
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->rootLayout:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->updateCollapsedAvatars(Landroid/view/ViewGroup;F)V

    .line 109
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->rootLayout:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, p1, v0}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->updateTeamProfile(Landroid/view/ViewGroup;Landroid/view/View;F)V

    .line 110
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->rootLayout:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, p1, v0}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->updateCollapsedBio(Landroid/view/ViewGroup;Landroid/view/View;F)V

    .line 111
    iget-object v1, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->rootLayout:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, p1, v0}, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->updateOfficeHours(Landroid/view/ViewGroup;Landroid/view/View;F)V

    .line 113
    iget-object v0, p0, Lio/intercom/android/sdk/profile/TeamProfileToolbarBehavior;->rootLayout:Landroid/view/ViewGroup;

    invoke-static {v0}, Lio/intercom/android/sdk/utilities/ViewUtils;->requestLayoutIfPossible(Landroid/view/View;)V

    goto :goto_0
.end method
