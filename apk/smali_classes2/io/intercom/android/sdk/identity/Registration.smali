.class public Lio/intercom/android/sdk/identity/Registration;
.super Ljava/lang/Object;
.source "Registration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/identity/Registration$Validity;
    }
.end annotation


# instance fields
.field private attributes:Lio/intercom/android/sdk/UserAttributes;

.field private email:Ljava/lang/String;

.field private final twig:Lio/intercom/android/sdk/twig/Twig;

.field private userId:Ljava/lang/String;

.field private validity:Lio/intercom/android/sdk/identity/Registration$Validity;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->email:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->userId:Ljava/lang/String;

    .line 44
    sget-object v0, Lio/intercom/android/sdk/identity/Registration$Validity;->NOT_SET:Lio/intercom/android/sdk/identity/Registration$Validity;

    iput-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->validity:Lio/intercom/android/sdk/identity/Registration$Validity;

    return-void
.end method

.method public static create()Lio/intercom/android/sdk/identity/Registration;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lio/intercom/android/sdk/identity/Registration;

    invoke-direct {v0}, Lio/intercom/android/sdk/identity/Registration;-><init>()V

    return-object v0
.end method

.method private updateState(Z)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->validity:Lio/intercom/android/sdk/identity/Registration$Validity;

    sget-object v1, Lio/intercom/android/sdk/identity/Registration$Validity;->NOT_SET:Lio/intercom/android/sdk/identity/Registration$Validity;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->validity:Lio/intercom/android/sdk/identity/Registration$Validity;

    sget-object v1, Lio/intercom/android/sdk/identity/Registration$Validity;->VALID:Lio/intercom/android/sdk/identity/Registration$Validity;

    if-ne v0, v1, :cond_1

    .line 130
    :cond_0
    if-eqz p1, :cond_2

    sget-object v0, Lio/intercom/android/sdk/identity/Registration$Validity;->VALID:Lio/intercom/android/sdk/identity/Registration$Validity;

    :goto_0
    iput-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->validity:Lio/intercom/android/sdk/identity/Registration$Validity;

    .line 132
    :cond_1
    return-void

    .line 130
    :cond_2
    sget-object v0, Lio/intercom/android/sdk/identity/Registration$Validity;->INVALID:Lio/intercom/android/sdk/identity/Registration$Validity;

    goto :goto_0
.end method


# virtual methods
.method public getAttributes()Lio/intercom/android/sdk/UserAttributes;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->attributes:Lio/intercom/android/sdk/UserAttributes;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->userId:Ljava/lang/String;

    return-object v0
.end method

.method isValidRegistration()Z
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lio/intercom/android/sdk/identity/Registration$Validity;->VALID:Lio/intercom/android/sdk/identity/Registration$Validity;

    iget-object v1, p0, Lio/intercom/android/sdk/identity/Registration;->validity:Lio/intercom/android/sdk/identity/Registration$Validity;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/identity/Registration$Validity;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public withEmail(Ljava/lang/String;)Lio/intercom/android/sdk/identity/Registration;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 63
    :goto_0
    if-eqz v0, :cond_1

    .line 64
    iput-object p1, p0, Lio/intercom/android/sdk/identity/Registration;->email:Ljava/lang/String;

    .line 68
    :goto_1
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/identity/Registration;->updateState(Z)V

    .line 69
    return-object p0

    :cond_0
    move v0, v1

    .line 62
    goto :goto_0

    .line 66
    :cond_1
    iget-object v2, p0, Lio/intercom/android/sdk/identity/Registration;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v3, "Email cannot be null or empty"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public withUserAttributes(Lio/intercom/android/sdk/UserAttributes;)Lio/intercom/android/sdk/identity/Registration;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 98
    if-nez p1, :cond_0

    .line 99
    sget-object v0, Lio/intercom/android/sdk/identity/Registration$Validity;->INVALID:Lio/intercom/android/sdk/identity/Registration$Validity;

    iput-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->validity:Lio/intercom/android/sdk/identity/Registration$Validity;

    .line 100
    iget-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "Registration.withUserAttributes method failed: the attributes Map provided is null"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    :goto_0
    return-object p0

    .line 102
    :cond_0
    invoke-virtual {p1}, Lio/intercom/android/sdk/UserAttributes;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    sget-object v0, Lio/intercom/android/sdk/identity/Registration$Validity;->INVALID:Lio/intercom/android/sdk/identity/Registration$Validity;

    iput-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->validity:Lio/intercom/android/sdk/identity/Registration$Validity;

    .line 104
    iget-object v0, p0, Lio/intercom/android/sdk/identity/Registration;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "Registration.withUserAttributes method failed: the attributes Map provided is empty"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    :cond_1
    iput-object p1, p0, Lio/intercom/android/sdk/identity/Registration;->attributes:Lio/intercom/android/sdk/UserAttributes;

    goto :goto_0
.end method

.method public withUserId(Ljava/lang/String;)Lio/intercom/android/sdk/identity/Registration;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 80
    :goto_0
    if-eqz v0, :cond_1

    .line 81
    iput-object p1, p0, Lio/intercom/android/sdk/identity/Registration;->userId:Ljava/lang/String;

    .line 85
    :goto_1
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/identity/Registration;->updateState(Z)V

    .line 86
    return-object p0

    :cond_0
    move v0, v1

    .line 79
    goto :goto_0

    .line 83
    :cond_1
    iget-object v2, p0, Lio/intercom/android/sdk/identity/Registration;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v3, "UserId cannot be null or empty"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
