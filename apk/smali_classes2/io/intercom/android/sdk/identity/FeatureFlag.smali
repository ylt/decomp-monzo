.class public interface abstract annotation Lio/intercom/android/sdk/identity/FeatureFlag;
.super Ljava/lang/Object;
.source "FeatureFlag.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation


# static fields
.field public static final CONVERSATION_WEB_VIEW:Ljava/lang/String; = "conversation-web-view"

.field public static final IMAGE_ANNOTATION:Ljava/lang/String; = "image-annotation"

.field public static final SCREENSHOT_SHARING:Ljava/lang/String; = "screenshot-sharing"
