.class public Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;
.super Ljava/lang/Object;
.source "ScreenshotMiddleware.java"

# interfaces
.implements Lio/intercom/android/sdk/store/Store$Middleware;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/android/sdk/store/Store$Middleware",
        "<",
        "Lio/intercom/android/sdk/state/State;",
        ">;"
    }
.end annotation


# instance fields
.field private final appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private dialogDismiss:Ljava/lang/Runnable;

.field private observerSubscription:Lio/intercom/android/sdk/store/Store$Subscription;


# direct methods
.method public constructor <init>(Lio/intercom/android/sdk/Provider;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 37
    iput-object p2, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->context:Landroid/content/Context;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->dialogDismiss:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$002(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->dialogDismiss:Ljava/lang/Runnable;

    return-object p1
.end method

.method private showShareDialog(Lio/intercom/android/sdk/store/Store;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    sget-object v0, Lio/intercom/android/sdk/store/Selectors;->OVERLAY:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {p1, v0}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/OverlayState;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->resumedHostActivity()Landroid/app/Activity;

    move-result-object v1

    .line 84
    if-nez v1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getName()Ljava/lang/String;

    move-result-object v0

    .line 88
    new-instance v2, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;

    invoke-direct {v2, p0, v1, v0}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;-><init>(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private startDetector(Lio/intercom/android/sdk/store/Store;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->observerSubscription:Lio/intercom/android/sdk/store/Store$Subscription;

    invoke-static {v0}, Lio/intercom/android/sdk/utilities/StoreUtils;->safeUnsubscribe(Lio/intercom/android/sdk/store/Store$Subscription;)V

    .line 69
    iget-object v0, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 71
    new-instance v1, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;

    invoke-direct {v1, p1, v0}, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;-><init>(Lio/intercom/android/sdk/store/Store;Landroid/content/ContentResolver;)V

    .line 73
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 75
    new-instance v2, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$1;

    invoke-direct {v2, p0, v0, v1}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$1;-><init>(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;Landroid/content/ContentResolver;Landroid/database/ContentObserver;)V

    iput-object v2, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->observerSubscription:Lio/intercom/android/sdk/store/Store$Subscription;

    .line 80
    return-void
.end method


# virtual methods
.method public dispatch(Lio/intercom/android/sdk/store/Store;Lio/intercom/android/sdk/actions/Action;Lio/intercom/android/sdk/store/Store$NextDispatcher;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;",
            "Lio/intercom/android/sdk/actions/Action",
            "<*>;",
            "Lio/intercom/android/sdk/store/Store$NextDispatcher;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-interface {p3, p2}, Lio/intercom/android/sdk/store/Store$NextDispatcher;->dispatch(Lio/intercom/android/sdk/actions/Action;)V

    .line 43
    iget-object v0, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    const-string v1, "screenshot-sharing"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/identity/AppConfig;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    sget-object v0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$3;->$SwitchMap$io$intercom$android$sdk$actions$Action$Type:[I

    invoke-virtual {p2}, Lio/intercom/android/sdk/actions/Action;->type()Lio/intercom/android/sdk/actions/Action$Type;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/actions/Action$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 49
    :pswitch_0
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->startDetector(Lio/intercom/android/sdk/store/Store;)V

    goto :goto_0

    .line 52
    :pswitch_1
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->showShareDialog(Lio/intercom/android/sdk/store/Store;)V

    goto :goto_0

    .line 55
    :pswitch_2
    invoke-virtual {p1}, Lio/intercom/android/sdk/store/Store;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/State;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/State;->lastScreenshot()Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->dialogDismiss:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->dialogDismiss:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 60
    :pswitch_3
    iget-object v0, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->observerSubscription:Lio/intercom/android/sdk/store/Store$Subscription;

    invoke-static {v0}, Lio/intercom/android/sdk/utilities/StoreUtils;->safeUnsubscribe(Lio/intercom/android/sdk/store/Store$Subscription;)V

    goto :goto_0

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
