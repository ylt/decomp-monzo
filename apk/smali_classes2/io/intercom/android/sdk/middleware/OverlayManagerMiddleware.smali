.class public Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;
.super Ljava/lang/Object;
.source "OverlayManagerMiddleware.java"

# interfaces
.implements Lio/intercom/android/sdk/store/Store$Middleware;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/android/sdk/store/Store$Middleware",
        "<",
        "Lio/intercom/android/sdk/state/State;",
        ">;"
    }
.end annotation


# instance fields
.field private final managerProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/overlay/OverlayManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/intercom/android/sdk/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/overlay/OverlayManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->managerProvider:Lio/intercom/android/sdk/Provider;

    .line 24
    return-void
.end method

.method private manager()Lio/intercom/android/sdk/overlay/OverlayManager;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->managerProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/OverlayManager;

    return-object v0
.end method

.method private removeOverlays(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 75
    if-eqz p1, :cond_0

    .line 76
    invoke-direct {p0}, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->manager()Lio/intercom/android/sdk/overlay/OverlayManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/overlay/OverlayManager;->removeOverlaysIfPresent(Landroid/app/Activity;)V

    .line 78
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatch(Lio/intercom/android/sdk/store/Store;Lio/intercom/android/sdk/actions/Action;Lio/intercom/android/sdk/store/Store$NextDispatcher;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;",
            "Lio/intercom/android/sdk/actions/Action",
            "<*>;",
            "Lio/intercom/android/sdk/store/Store$NextDispatcher;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lio/intercom/android/sdk/store/Selectors;->OVERLAY:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {p1, v0}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/OverlayState;

    .line 28
    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->pausedHostActivity()Landroid/app/Activity;

    move-result-object v1

    .line 29
    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->resumedHostActivity()Landroid/app/Activity;

    move-result-object v0

    .line 31
    sget-object v2, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware$1;->$SwitchMap$io$intercom$android$sdk$actions$Action$Type:[I

    invoke-virtual {p2}, Lio/intercom/android/sdk/actions/Action;->type()Lio/intercom/android/sdk/actions/Action$Type;

    move-result-object v3

    invoke-virtual {v3}, Lio/intercom/android/sdk/actions/Action$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 71
    :cond_0
    :goto_0
    invoke-interface {p3, p2}, Lio/intercom/android/sdk/store/Store$NextDispatcher;->dispatch(Lio/intercom/android/sdk/actions/Action;)V

    .line 72
    return-void

    .line 35
    :pswitch_0
    invoke-virtual {p2}, Lio/intercom/android/sdk/actions/Action;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 36
    if-eq v0, v1, :cond_0

    .line 37
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->removeOverlays(Landroid/app/Activity;)V

    goto :goto_0

    .line 43
    :pswitch_1
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->removeOverlays(Landroid/app/Activity;)V

    goto :goto_0

    .line 48
    :pswitch_2
    invoke-virtual {p2}, Lio/intercom/android/sdk/actions/Action;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 49
    if-ne v0, v1, :cond_0

    .line 50
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->removeOverlays(Landroid/app/Activity;)V

    goto :goto_0

    .line 56
    :pswitch_3
    invoke-direct {p0}, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->manager()Lio/intercom/android/sdk/overlay/OverlayManager;

    move-result-object v2

    invoke-virtual {v2}, Lio/intercom/android/sdk/overlay/OverlayManager;->cancelAnimations()V

    .line 57
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->removeOverlays(Landroid/app/Activity;)V

    .line 58
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->removeOverlays(Landroid/app/Activity;)V

    goto :goto_0

    .line 63
    :pswitch_4
    invoke-virtual {p2}, Lio/intercom/android/sdk/actions/Action;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/UsersResponse;

    .line 64
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/UsersResponse;->getUnreadConversations()Lio/intercom/android/sdk/models/ConversationList;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/ConversationList;->getConversations()Ljava/util/List;

    move-result-object v0

    .line 65
    invoke-direct {p0}, Lio/intercom/android/sdk/middleware/OverlayManagerMiddleware;->manager()Lio/intercom/android/sdk/overlay/OverlayManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/overlay/OverlayManager;->sendUnreadConversationsReceivedMetric(Ljava/util/List;)V

    goto :goto_0

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
