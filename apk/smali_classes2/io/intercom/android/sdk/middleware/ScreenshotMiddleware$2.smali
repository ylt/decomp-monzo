.class Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;
.super Ljava/lang/Object;
.source "ScreenshotMiddleware.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->showShareDialog(Lio/intercom/android/sdk/store/Store;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$appName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;->this$0:Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;

    iput-object p2, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;->val$appName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 90
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;->val$activity:Landroid/app/Activity;

    sget v2, Landroid/support/v7/a/a$i;->Theme_AppCompat_Light_Dialog_Alert:I

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const-string v1, "Share screenshot?"

    .line 91
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Edit and share this screenshot with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;->val$appName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Share"

    new-instance v2, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2$1;

    invoke-direct {v2, p0}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2$1;-><init>(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;)V

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;->this$0:Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;

    invoke-static {v1}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->access$000(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;)Ljava/lang/Runnable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;->this$0:Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;

    invoke-static {v1}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->access$000(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 105
    :cond_0
    iget-object v1, p0, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;->this$0:Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;

    new-instance v2, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2$2;

    invoke-direct {v2, p0, v0}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2$2;-><init>(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware$2;Landroid/app/AlertDialog;)V

    invoke-static {v1, v2}, Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;->access$002(Lio/intercom/android/sdk/middleware/ScreenshotMiddleware;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 110
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 111
    return-void
.end method
