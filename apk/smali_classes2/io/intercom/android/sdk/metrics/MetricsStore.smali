.class public Lio/intercom/android/sdk/metrics/MetricsStore;
.super Ljava/lang/Object;
.source "MetricsStore.java"


# static fields
.field private static final DELIMITER:Ljava/lang/String; = "~"

.field private static final EXECUTOR:Ljava/util/concurrent/Executor;

.field private static final METRICS_DISK_CACHE:Ljava/lang/String; = "intercomMetricsDiskCache"

.field private static final OPS_METRICS_DISK_CACHE:Ljava/lang/String; = "intercomOpsMetricsDiskCache"


# instance fields
.field final apiProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/api/Api;",
            ">;"
        }
    .end annotation
.end field

.field private final appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field final context:Landroid/content/Context;

.field private final executor:Ljava/util/concurrent/Executor;

.field final gson:Lio/intercom/com/google/gson/e;

.field final installerPackageName:Ljava/lang/String;

.field final isDebugBuild:Z

.field final metrics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/metrics/MetricObject;",
            ">;"
        }
    .end annotation
.end field

.field final opsMetrics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;",
            ">;"
        }
    .end annotation
.end field

.field private final twig:Lio/intercom/android/sdk/twig/Twig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lio/intercom/android/sdk/metrics/MetricsStore;->EXECUTOR:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/api/Api;",
            ">;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    sget-object v0, Lio/intercom/android/sdk/metrics/MetricsStore;->EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-direct {p0, p1, p2, p3, v0}, Lio/intercom/android/sdk/metrics/MetricsStore;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/Provider;Ljava/util/concurrent/Executor;)V

    .line 49
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/Provider;Ljava/util/concurrent/Executor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/api/Api;",
            ">;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->metrics:Ljava/util/List;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->opsMetrics:Ljava/util/List;

    .line 41
    new-instance v0, Lio/intercom/com/google/gson/e;

    invoke-direct {v0}, Lio/intercom/com/google/gson/e;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->gson:Lio/intercom/com/google/gson/e;

    .line 45
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 53
    iput-object p1, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->context:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->apiProvider:Lio/intercom/android/sdk/Provider;

    .line 55
    iput-object p3, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 56
    iput-object p4, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->executor:Ljava/util/concurrent/Executor;

    .line 57
    invoke-static {p1}, Lio/intercom/android/sdk/metrics/AppTypeDetector;->isDebugBuild(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->isDebugBuild:Z

    .line 58
    invoke-static {p1}, Lio/intercom/android/sdk/metrics/AppTypeDetector;->getInstallerPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->installerPackageName:Ljava/lang/String;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/metrics/MetricsStore;)Lio/intercom/android/sdk/twig/Twig;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->twig:Lio/intercom/android/sdk/twig/Twig;

    return-object v0
.end method

.method static synthetic access$100(Lio/intercom/android/sdk/metrics/MetricsStore;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method private getMetrics([Ljava/lang/String;Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 177
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 179
    :try_start_0
    const-class v3, Lio/intercom/android/sdk/metrics/MetricObject;

    if-ne p2, v3, :cond_0

    .line 180
    iget-object v3, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->metrics:Ljava/util/List;

    iget-object v4, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->gson:Lio/intercom/com/google/gson/e;

    const-class v5, Lio/intercom/android/sdk/metrics/MetricObject;

    invoke-virtual {v4, v2, v5}, Lio/intercom/com/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_0
    iget-object v3, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->opsMetrics:Ljava/util/List;

    iget-object v4, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->gson:Lio/intercom/com/google/gson/e;

    const-class v5, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;

    invoke-virtual {v4, v2, v5}, Lio/intercom/com/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 184
    :catch_0
    move-exception v3

    .line 185
    iget-object v3, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->twig:Lio/intercom/android/sdk/twig/Twig;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not parse metric: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    goto :goto_1

    .line 188
    :cond_1
    return-void
.end method

.method private isDisabled()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->isMetricsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadObjectsAsJson(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    const/4 v1, 0x0

    .line 195
    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    invoke-static {v0}, Lio/intercom/a/l;->a(Ljava/io/InputStream;)Lio/intercom/a/s;

    move-result-object v0

    invoke-static {v0}, Lio/intercom/a/l;->a(Lio/intercom/a/s;)Lio/intercom/a/e;

    move-result-object v1

    .line 196
    invoke-interface {v1}, Lio/intercom/a/e;->r()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 198
    if-eqz v1, :cond_0

    .line 199
    invoke-interface {v1}, Lio/intercom/a/e;->close()V

    .line 202
    :cond_0
    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 198
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 199
    invoke-interface {v1}, Lio/intercom/a/e;->close()V

    :cond_1
    throw v0
.end method


# virtual methods
.method cleanUpMetrics(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/metrics/MetricObject;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->metrics:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 133
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->opsMetrics:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 134
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->context:Landroid/content/Context;

    const-string v1, "intercomMetricsDiskCache"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 135
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->context:Landroid/content/Context;

    const-string v1, "intercomOpsMetricsDiskCache"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 136
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->metrics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->metrics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/metrics/MetricObject;

    .line 138
    const-string v2, "intercomMetricsDiskCache"

    invoke-virtual {p0, v0, v2}, Lio/intercom/android/sdk/metrics/MetricsStore;->persistMetric(Lio/intercom/android/sdk/metrics/MetricInterface;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->opsMetrics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->opsMetrics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;

    .line 143
    const-string v2, "intercomOpsMetricsDiskCache"

    invoke-virtual {p0, v0, v2}, Lio/intercom/android/sdk/metrics/MetricsStore;->persistMetric(Lio/intercom/android/sdk/metrics/MetricInterface;Ljava/lang/String;)V

    goto :goto_1

    .line 146
    :cond_1
    return-void
.end method

.method public loadAndSendCachedMetrics()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lio/intercom/android/sdk/metrics/MetricsStore$3;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/metrics/MetricsStore$3;-><init>(Lio/intercom/android/sdk/metrics/MetricsStore;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 102
    return-void
.end method

.method persistMetric(Lio/intercom/android/sdk/metrics/MetricInterface;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 152
    const/4 v1, 0x0

    .line 154
    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->context:Landroid/content/Context;

    const v2, 0x8000

    invoke-virtual {v0, p2, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    invoke-static {v0}, Lio/intercom/a/l;->a(Ljava/io/OutputStream;)Lio/intercom/a/r;

    move-result-object v0

    invoke-static {v0}, Lio/intercom/a/l;->a(Lio/intercom/a/r;)Lio/intercom/a/d;

    move-result-object v1

    .line 155
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->gson:Lio/intercom/com/google/gson/e;

    invoke-interface {p1, v0}, Lio/intercom/android/sdk/metrics/MetricInterface;->toJson(Lio/intercom/com/google/gson/e;)Ljava/lang/String;

    move-result-object v0

    .line 156
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/intercom/a/d;->b(Ljava/lang/String;)Lio/intercom/a/d;

    .line 157
    iget-object v2, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->twig:Lio/intercom/android/sdk/twig/Twig;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Persisted metric: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    .line 158
    invoke-interface {v1}, Lio/intercom/a/d;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    if-eqz v1, :cond_0

    .line 161
    :try_start_1
    invoke-interface {v1}, Lio/intercom/a/d;->close()V

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 160
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 161
    invoke-interface {v1}, Lio/intercom/a/d;->close()V

    :cond_1
    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 164
    :catch_0
    move-exception v0

    .line 165
    iget-object v1, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->twig:Lio/intercom/android/sdk/twig/Twig;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t persist metric to disk: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    goto :goto_0
.end method

.method readMetricsFromDisk()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    const-string v0, "intercomMetricsDiskCache"

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/metrics/MetricsStore;->loadObjectsAsJson(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-class v1, Lio/intercom/android/sdk/metrics/MetricObject;

    invoke-direct {p0, v0, v1}, Lio/intercom/android/sdk/metrics/MetricsStore;->getMetrics([Ljava/lang/String;Ljava/lang/Class;)V

    .line 172
    const-string v0, "intercomOpsMetricsDiskCache"

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/metrics/MetricsStore;->loadObjectsAsJson(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-class v1, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;

    invoke-direct {p0, v0, v1}, Lio/intercom/android/sdk/metrics/MetricsStore;->getMetrics([Ljava/lang/String;Ljava/lang/Class;)V

    .line 173
    return-void
.end method

.method public sendMetrics()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lio/intercom/android/sdk/metrics/MetricsStore$4;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/metrics/MetricsStore$4;-><init>(Lio/intercom/android/sdk/metrics/MetricsStore;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 128
    return-void
.end method

.method public track(Lio/intercom/android/sdk/metrics/MetricObject;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Lio/intercom/android/sdk/metrics/MetricsStore;->isDisabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "Metrics have been remotely disabled"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    .line 72
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lio/intercom/android/sdk/metrics/MetricsStore$1;

    invoke-direct {v1, p0, p1}, Lio/intercom/android/sdk/metrics/MetricsStore$1;-><init>(Lio/intercom/android/sdk/metrics/MetricsStore;Lio/intercom/android/sdk/metrics/MetricObject;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public track(Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Lio/intercom/android/sdk/metrics/MetricsStore;->isDisabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "Metrics have been remotely disabled"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    .line 85
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lio/intercom/android/sdk/metrics/MetricsStore$2;

    invoke-direct {v1, p0, p1}, Lio/intercom/android/sdk/metrics/MetricsStore$2;-><init>(Lio/intercom/android/sdk/metrics/MetricsStore;Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
