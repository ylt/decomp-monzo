.class Lio/intercom/android/sdk/metrics/MetricsStore$2;
.super Ljava/lang/Object;
.source "MetricsStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/metrics/MetricsStore;->track(Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

.field final synthetic val$opsMetric:Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/metrics/MetricsStore;Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lio/intercom/android/sdk/metrics/MetricsStore$2;->this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

    iput-object p2, p0, Lio/intercom/android/sdk/metrics/MetricsStore$2;->val$opsMetric:Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore$2;->this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

    iget-object v0, v0, Lio/intercom/android/sdk/metrics/MetricsStore;->opsMetrics:Ljava/util/List;

    iget-object v1, p0, Lio/intercom/android/sdk/metrics/MetricsStore$2;->val$opsMetric:Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore$2;->this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

    iget-object v1, p0, Lio/intercom/android/sdk/metrics/MetricsStore$2;->val$opsMetric:Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;

    const-string v2, "intercomOpsMetricsDiskCache"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/MetricsStore;->persistMetric(Lio/intercom/android/sdk/metrics/MetricInterface;Ljava/lang/String;)V

    .line 83
    return-void
.end method
