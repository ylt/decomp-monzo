.class Lio/intercom/android/sdk/metrics/MetricsStore$1;
.super Ljava/lang/Object;
.source "MetricsStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/metrics/MetricsStore;->track(Lio/intercom/android/sdk/metrics/MetricObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

.field final synthetic val$metric:Lio/intercom/android/sdk/metrics/MetricObject;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/metrics/MetricsStore;Lio/intercom/android/sdk/metrics/MetricObject;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lio/intercom/android/sdk/metrics/MetricsStore$1;->this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

    iput-object p2, p0, Lio/intercom/android/sdk/metrics/MetricsStore$1;->val$metric:Lio/intercom/android/sdk/metrics/MetricObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore$1;->this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

    iget-object v0, v0, Lio/intercom/android/sdk/metrics/MetricsStore;->metrics:Ljava/util/List;

    iget-object v1, p0, Lio/intercom/android/sdk/metrics/MetricsStore$1;->val$metric:Lio/intercom/android/sdk/metrics/MetricObject;

    iget-object v2, p0, Lio/intercom/android/sdk/metrics/MetricsStore$1;->this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

    iget-object v2, v2, Lio/intercom/android/sdk/metrics/MetricsStore;->installerPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/metrics/MetricObject;->addInstallerPackageName(Ljava/lang/String;)Lio/intercom/android/sdk/metrics/MetricObject;

    move-result-object v1

    iget-object v2, p0, Lio/intercom/android/sdk/metrics/MetricsStore$1;->this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

    iget-boolean v2, v2, Lio/intercom/android/sdk/metrics/MetricsStore;->isDebugBuild:Z

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/metrics/MetricObject;->addIsDebugBuild(Z)Lio/intercom/android/sdk/metrics/MetricObject;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/MetricsStore$1;->this$0:Lio/intercom/android/sdk/metrics/MetricsStore;

    iget-object v1, p0, Lio/intercom/android/sdk/metrics/MetricsStore$1;->val$metric:Lio/intercom/android/sdk/metrics/MetricObject;

    const-string v2, "intercomMetricsDiskCache"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/MetricsStore;->persistMetric(Lio/intercom/android/sdk/metrics/MetricInterface;Ljava/lang/String;)V

    .line 70
    return-void
.end method
