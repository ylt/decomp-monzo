.class public Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;
.super Ljava/lang/Object;
.source "OpsMetricObject.java"

# interfaces
.implements Lio/intercom/android/sdk/metrics/MetricInterface;


# instance fields
.field private final name:Ljava/lang/String;

.field private final type:Ljava/lang/String;

.field private final value:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->type:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->name:Ljava/lang/String;

    .line 15
    iput-wide p3, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->value:J

    .line 16
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 20
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 27
    :cond_0
    :goto_0
    return v1

    .line 21
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 23
    check-cast p1, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;

    .line 25
    iget-wide v2, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->value:J

    iget-wide v4, p1, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->value:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 26
    iget-object v2, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->type:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->type:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 27
    :cond_2
    iget-object v2, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->name:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v0, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->name:Ljava/lang/String;

    iget-object v1, p1, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_3
    :goto_1
    move v1, v0

    goto :goto_0

    .line 26
    :cond_4
    iget-object v2, p1, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->type:Ljava/lang/String;

    if-eqz v2, :cond_2

    goto :goto_0

    .line 27
    :cond_5
    iget-object v2, p1, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->name:Ljava/lang/String;

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 32
    iget-object v0, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->type:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->type:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 33
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->name:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 34
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->value:J

    iget-wide v4, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->value:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 35
    return v0

    :cond_1
    move v0, v1

    .line 32
    goto :goto_0
.end method

.method public toJson(Lio/intercom/com/google/gson/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p1, p0}, Lio/intercom/com/google/gson/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v2, 0x27

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OpsMetricObject{type=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/metrics/ops/OpsMetricObject;->value:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
