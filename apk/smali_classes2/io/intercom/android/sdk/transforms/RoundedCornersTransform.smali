.class public Lio/intercom/android/sdk/transforms/RoundedCornersTransform;
.super Lio/intercom/com/bumptech/glide/load/resource/bitmap/f;
.source "RoundedCornersTransform.java"


# static fields
.field private static final VERSION:I = 0x1


# instance fields
.field private final id:Ljava/lang/String;

.field private final idBytes:[B

.field private final radius:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/f;-><init>()V

    .line 22
    iput p1, p0, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;->radius:I

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "io.intercom.android.sdk.transforms.RoundedCornersTransform.(radius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;->id:Ljava/lang/String;

    .line 24
    iget-object v0, p0, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;->id:Ljava/lang/String;

    sget-object v1, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;->idBytes:[B

    .line 25
    return-void
.end method


# virtual methods
.method protected transform(Lio/intercom/com/bumptech/glide/load/engine/a/e;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lio/intercom/android/sdk/transforms/RoundedCornersTransform$1;

    invoke-direct {v0, p0, p1}, Lio/intercom/android/sdk/transforms/RoundedCornersTransform$1;-><init>(Lio/intercom/android/sdk/transforms/RoundedCornersTransform;Lio/intercom/com/bumptech/glide/load/engine/a/e;)V

    .line 33
    iget v1, p0, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;->radius:I

    invoke-static {p2, v0, v1}, Lio/intercom/android/sdk/commons/utilities/BitmapUtils;->transformRoundCorners(Landroid/graphics/Bitmap;Lio/intercom/android/sdk/commons/utilities/BitmapUtils$BitmapCache;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public updateDiskCacheKey(Ljava/security/MessageDigest;)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;->idBytes:[B

    invoke-virtual {p1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 38
    return-void
.end method
