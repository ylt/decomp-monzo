.class public final Lio/intercom/android/sdk/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ALT:I = 0x7f110090

.field public static final CTRL:I = 0x7f110091

.field public static final FUNCTION:I = 0x7f110092

.field public static final META:I = 0x7f110093

.field public static final SHIFT:I = 0x7f110094

.field public static final SYM:I = 0x7f110095

.field public static final action0:I = 0x7f110442

.field public static final action_bar:I = 0x7f1100eb

.field public static final action_bar_activity_content:I = 0x7f110000

.field public static final action_bar_container:I = 0x7f1100ea

.field public static final action_bar_root:I = 0x7f1100e6

.field public static final action_bar_spinner:I = 0x7f110001

.field public static final action_bar_subtitle:I = 0x7f1100ca

.field public static final action_bar_title:I = 0x7f1100c9

.field public static final action_button:I = 0x7f1103c0

.field public static final action_container:I = 0x7f11043f

.field public static final action_context_bar:I = 0x7f1100ec

.field public static final action_divider:I = 0x7f110446

.field public static final action_image:I = 0x7f110440

.field public static final action_menu_divider:I = 0x7f110002

.field public static final action_menu_presenter:I = 0x7f110003

.field public static final action_mode_bar:I = 0x7f1100e8

.field public static final action_mode_bar_stub:I = 0x7f1100e7

.field public static final action_mode_close_button:I = 0x7f1100cb

.field public static final action_text:I = 0x7f110441

.field public static final actions:I = 0x7f11044e

.field public static final activity_chooser_view_content:I = 0x7f1100cc

.field public static final add:I = 0x7f110059

.field public static final alertTitle:I = 0x7f1100df

.field public static final all:I = 0x7f11003b

.field public static final always:I = 0x7f110096

.field public static final app_bar_layout:I = 0x7f110228

.field public static final async:I = 0x7f110085

.field public static final attribution:I = 0x7f1103e0

.field public static final author:I = 0x7f110366

.field public static final auto:I = 0x7f11005c

.field public static final avatar:I = 0x7f1103cf

.field public static final avatarView:I = 0x7f1103de

.field public static final avatar_view:I = 0x7f110365

.field public static final beginning:I = 0x7f110082

.field public static final blocking:I = 0x7f110086

.field public static final bottom:I = 0x7f11002f

.field public static final buttonPanel:I = 0x7f1100d2

.field public static final cancel_action:I = 0x7f110443

.field public static final cellLayout:I = 0x7f1103af

.field public static final cell_content:I = 0x7f1103e8

.field public static final center:I = 0x7f110065

.field public static final center_horizontal:I = 0x7f110066

.field public static final center_vertical:I = 0x7f110067

.field public static final chat_avatar_container:I = 0x7f1103d6

.field public static final chat_full_body:I = 0x7f1103d2

.field public static final chat_overlay_overflow_fade:I = 0x7f1103d5

.field public static final chathead_avatar:I = 0x7f1103d7

.field public static final chathead_root:I = 0x7f1103d1

.field public static final chathead_text_body:I = 0x7f1103d8

.field public static final chathead_text_container:I = 0x7f1103d3

.field public static final chathead_text_header:I = 0x7f1103d4

.field public static final checkbox:I = 0x7f1100e2

.field public static final chronometer:I = 0x7f11044b

.field public static final clear_search:I = 0x7f1103ce

.field public static final clip_horizontal:I = 0x7f110068

.field public static final clip_vertical:I = 0x7f110069

.field public static final collapseActionView:I = 0x7f110097

.field public static final collapsing_background_image:I = 0x7f110381

.field public static final compose_action_button:I = 0x7f1103cc

.field public static final composer_container:I = 0x7f1103a6

.field public static final composer_edit_text_layout:I = 0x7f1103a9

.field public static final composer_holder:I = 0x7f1103c7

.field public static final composer_holder_shadow:I = 0x7f1103c9

.field public static final composer_input_icons_recycler_view:I = 0x7f1103aa

.field public static final composer_input_view:I = 0x7f1103c4

.field public static final composer_lower_border:I = 0x7f1103ad

.field public static final composer_upper_border:I = 0x7f1103a8

.field public static final composer_view_pager:I = 0x7f1103ae

.field public static final container:I = 0x7f1102ae

.field public static final contentPanel:I = 0x7f1100d5

.field public static final conversation_coordinator:I = 0x7f11037a

.field public static final conversation_coordinator_layout:I = 0x7f1103b0

.field public static final conversation_fragment:I = 0x7f110373

.field public static final conversation_fragment_root:I = 0x7f1103c5

.field public static final conversation_list:I = 0x7f1103b3

.field public static final conversation_web_view_layout:I = 0x7f1103b4

.field public static final conversations_list_root:I = 0x7f1103ca

.field public static final coordinator:I = 0x7f1102af

.field public static final custom:I = 0x7f1100dc

.field public static final customPanel:I = 0x7f1100db

.field public static final decor_content_parent:I = 0x7f1100e9

.field public static final default_activity_button:I = 0x7f1100cf

.field public static final description:I = 0x7f11012a

.field public static final design_bottom_sheet:I = 0x7f1102b1

.field public static final design_menu_item_action_area:I = 0x7f1102b8

.field public static final design_menu_item_action_area_stub:I = 0x7f1102b7

.field public static final design_menu_item_text:I = 0x7f1102b6

.field public static final design_navigation_view:I = 0x7f1102b5

.field public static final disableHome:I = 0x7f110049

.field public static final dismiss:I = 0x7f110361

.field public static final dot1:I = 0x7f11037d

.field public static final dot2:I = 0x7f11037e

.field public static final dot3:I = 0x7f11037f

.field public static final edit_query:I = 0x7f1100ed

.field public static final empty_action_button:I = 0x7f11039a

.field public static final empty_text_subtitle:I = 0x7f110399

.field public static final empty_text_title:I = 0x7f110398

.field public static final empty_view_layout:I = 0x7f110397

.field public static final end:I = 0x7f110030

.field public static final end_padder:I = 0x7f110451

.field public static final enterAlways:I = 0x7f11004f

.field public static final enterAlwaysCollapsed:I = 0x7f110050

.field public static final error_layout_article:I = 0x7f1103bf

.field public static final error_layout_conversation:I = 0x7f1103c1

.field public static final error_layout_inbox:I = 0x7f1103c2

.field public static final event_name:I = 0x7f1103e1

.field public static final exitUntilCollapsed:I = 0x7f110051

.field public static final expand_activities_button:I = 0x7f1100cd

.field public static final expand_arrow:I = 0x7f1103ea

.field public static final expanded_fragment:I = 0x7f110395

.field public static final expanded_menu:I = 0x7f1100e1

.field public static final fill:I = 0x7f110040

.field public static final fill_horizontal:I = 0x7f11006a

.field public static final fill_vertical:I = 0x7f11006b

.field public static final fixed:I = 0x7f11009e

.field public static final forever:I = 0x7f110087

.field public static final fragment_container:I = 0x7f110394

.field public static final full_image:I = 0x7f110370

.field public static final gallery_content_layout:I = 0x7f11039d

.field public static final gallery_empty_view:I = 0x7f11039f

.field public static final gallery_expand_button:I = 0x7f1103c3

.field public static final gallery_recycler_view:I = 0x7f11039e

.field public static final gallery_root_view:I = 0x7f11039c

.field public static final ghost_view:I = 0x7f110015

.field public static final height:I = 0x7f1100c7

.field public static final help_center_web_view:I = 0x7f11036e

.field public static final home:I = 0x7f110016

.field public static final homeAsUp:I = 0x7f11004a

.field public static final icon:I = 0x7f1100d1

.field public static final icon_group:I = 0x7f11044f

.field public static final ifRoom:I = 0x7f110098

.field public static final image:I = 0x7f1100ce

.field public static final inbox_fragment:I = 0x7f110372

.field public static final inbox_recycler_view:I = 0x7f1103cb

.field public static final info:I = 0x7f11044c

.field public static final input_icon_image_view:I = 0x7f1103a7

.field public static final input_text:I = 0x7f110396

.field public static final intercom_author_avatar:I = 0x7f1103e2

.field public static final intercom_avatar_spacer:I = 0x7f110389

.field public static final intercom_bottom_spacer:I = 0x7f110390

.field public static final intercom_bubble:I = 0x7f1103dd

.field public static final intercom_collapsing_bio:I = 0x7f11038c

.field public static final intercom_collapsing_location:I = 0x7f11038b

.field public static final intercom_collapsing_office_hours:I = 0x7f1103f4

.field public static final intercom_collapsing_role:I = 0x7f11038a

.field public static final intercom_collapsing_subtitle:I = 0x7f110388

.field public static final intercom_collapsing_team_avatar1:I = 0x7f1103ed

.field public static final intercom_collapsing_team_avatar2:I = 0x7f1103ef

.field public static final intercom_collapsing_team_avatar3:I = 0x7f1103f1

.field public static final intercom_collapsing_team_bio:I = 0x7f1103f3

.field public static final intercom_collapsing_team_name_1:I = 0x7f1103ee

.field public static final intercom_collapsing_team_name_2:I = 0x7f1103f0

.field public static final intercom_collapsing_team_name_3:I = 0x7f1103f2

.field public static final intercom_collapsing_teammate_active_state:I = 0x7f110385

.field public static final intercom_collapsing_teammate_avatar1:I = 0x7f110384

.field public static final intercom_collapsing_teammate_avatar2:I = 0x7f110383

.field public static final intercom_collapsing_teammate_avatar3:I = 0x7f110382

.field public static final intercom_collapsing_title:I = 0x7f110386

.field public static final intercom_collapsing_title_name_only:I = 0x7f110387

.field public static final intercom_container_card_title:I = 0x7f1103e7

.field public static final intercom_container_fade_view:I = 0x7f1103e9

.field public static final intercom_conversation_indicator:I = 0x7f1103e5

.field public static final intercom_group_avatar_holder:I = 0x7f110393

.field public static final intercom_group_conversations_banner:I = 0x7f110391

.field public static final intercom_group_conversations_banner_title:I = 0x7f110392

.field public static final intercom_left_item_layout:I = 0x7f1103f7

.field public static final intercom_link:I = 0x7f1103b5

.field public static final intercom_message_summary:I = 0x7f1103e6

.field public static final intercom_office_hours_banner:I = 0x7f1103f6

.field public static final intercom_overlay_root:I = 0x7f110017

.field public static final intercom_rating_options_layout:I = 0x7f1103b8

.field public static final intercom_rating_tell_us_more_button:I = 0x7f1103b9

.field public static final intercom_team_profile:I = 0x7f1103eb

.field public static final intercom_team_profile_separator:I = 0x7f1103f5

.field public static final intercom_team_profiles_layout:I = 0x7f1103ec

.field public static final intercom_teammate_profile_container_view:I = 0x7f110380

.field public static final intercom_time_stamp:I = 0x7f1103e3

.field public static final intercom_toolbar:I = 0x7f110375

.field public static final intercom_toolbar_avatar:I = 0x7f1103f9

.field public static final intercom_toolbar_avatar_active_state:I = 0x7f1103fa

.field public static final intercom_toolbar_close:I = 0x7f1103fb

.field public static final intercom_toolbar_divider:I = 0x7f1103fe

.field public static final intercom_toolbar_inbox:I = 0x7f1103f8

.field public static final intercom_toolbar_subtitle:I = 0x7f1103fd

.field public static final intercom_toolbar_title:I = 0x7f1103fc

.field public static final intercom_user_name:I = 0x7f1103e4

.field public static final intercom_you_rated_image_view:I = 0x7f1103bb

.field public static final intercom_you_rated_layout:I = 0x7f1103ba

.field public static final italic:I = 0x7f110088

.field public static final item_touch_helper_previous_elevation:I = 0x7f110018

.field public static final largeLabel:I = 0x7f1102ad

.field public static final launcher_badge_count:I = 0x7f1103be

.field public static final launcher_icon:I = 0x7f1103bd

.field public static final launcher_root:I = 0x7f1103bc

.field public static final left:I = 0x7f110031

.field public static final lightbox_annotation_controls_space:I = 0x7f1103a3

.field public static final lightbox_button_layout:I = 0x7f1103a0

.field public static final lightbox_close_button:I = 0x7f1103a1

.field public static final lightbox_image:I = 0x7f1103a5

.field public static final lightbox_send_button:I = 0x7f1103a4

.field public static final lightbox_undo_button:I = 0x7f1103a2

.field public static final line1:I = 0x7f110019

.field public static final line3:I = 0x7f11001a

.field public static final link_composer_container:I = 0x7f110368

.field public static final link_container:I = 0x7f110364

.field public static final link_root:I = 0x7f110360

.field public static final link_title_bar:I = 0x7f11036b

.field public static final link_view:I = 0x7f110362

.field public static final linkedin_button:I = 0x7f11038f

.field public static final listMode:I = 0x7f110046

.field public static final list_item:I = 0x7f1100d0

.field public static final loading_view:I = 0x7f11036d

.field public static final masked:I = 0x7f1104c7

.field public static final media_actions:I = 0x7f110445

.field public static final message:I = 0x7f110473

.field public static final messenger_container:I = 0x7f110371

.field public static final metadata:I = 0x7f1103df

.field public static final middle:I = 0x7f110083

.field public static final mini:I = 0x7f110084

.field public static final multiply:I = 0x7f110054

.field public static final navigation_header_container:I = 0x7f1102b4

.field public static final never:I = 0x7f110099

.field public static final none:I = 0x7f11003e

.field public static final normal:I = 0x7f110047

.field public static final note_composer_container:I = 0x7f110377

.field public static final note_layout:I = 0x7f110374

.field public static final note_touch_target:I = 0x7f110378

.field public static final note_view:I = 0x7f110376

.field public static final notification_background:I = 0x7f11044d

.field public static final notification_main_column:I = 0x7f110448

.field public static final notification_main_column_container:I = 0x7f110447

.field public static final notification_pill:I = 0x7f1103d0

.field public static final notification_root:I = 0x7f1103d9

.field public static final parallax:I = 0x7f11006c

.field public static final parentPanel:I = 0x7f1100d4

.field public static final parent_matrix:I = 0x7f11001c

.field public static final pill:I = 0x7f1103c8

.field public static final pin:I = 0x7f11006d

.field public static final post_container:I = 0x7f110379

.field public static final post_touch_target:I = 0x7f11037c

.field public static final post_view:I = 0x7f11037b

.field public static final preview_avatar:I = 0x7f1103da

.field public static final preview_name:I = 0x7f1103db

.field public static final preview_summary:I = 0x7f1103dc

.field public static final profile_toolbar:I = 0x7f1103b2

.field public static final profile_toolbar_coordinator:I = 0x7f1103b1

.field public static final progressBar:I = 0x7f1101eb

.field public static final progress_bar:I = 0x7f1102a1

.field public static final progress_circular:I = 0x7f11001d

.field public static final progress_horizontal:I = 0x7f11001e

.field public static final radio:I = 0x7f1100e4

.field public static final rate_your_conversation_text_view:I = 0x7f1103b7

.field public static final reaction_input_view:I = 0x7f11036a

.field public static final reaction_text:I = 0x7f110369

.field public static final right:I = 0x7f110032

.field public static final right_icon:I = 0x7f110450

.field public static final right_side:I = 0x7f110449

.field public static final root_view:I = 0x7f11036f

.field public static final save_image_matrix:I = 0x7f11001f

.field public static final save_non_transition_alpha:I = 0x7f110020

.field public static final save_scale_type:I = 0x7f110021

.field public static final screen:I = 0x7f110055

.field public static final scroll:I = 0x7f110052

.field public static final scrollIndicatorDown:I = 0x7f1100da

.field public static final scrollIndicatorUp:I = 0x7f1100d6

.field public static final scrollView:I = 0x7f1100d7

.field public static final scroll_view:I = 0x7f110363

.field public static final scrollable:I = 0x7f11009f

.field public static final search_badge:I = 0x7f1100ef

.field public static final search_bar:I = 0x7f1100ee

.field public static final search_button:I = 0x7f1100f0

.field public static final search_close_btn:I = 0x7f1100f5

.field public static final search_edit_frame:I = 0x7f1100f1

.field public static final search_go_btn:I = 0x7f1100f7

.field public static final search_mag_icon:I = 0x7f1100f2

.field public static final search_plate:I = 0x7f1100f3

.field public static final search_src_text:I = 0x7f1100f4

.field public static final search_voice_btn:I = 0x7f1100f8

.field public static final select_dialog_listview:I = 0x7f1100f9

.field public static final send_button:I = 0x7f1103ac

.field public static final send_button_fading_background:I = 0x7f1103ab

.field public static final shortcut:I = 0x7f1100e3

.field public static final showCustom:I = 0x7f11004b

.field public static final showHome:I = 0x7f11004c

.field public static final showTitle:I = 0x7f11004d

.field public static final smallLabel:I = 0x7f1102ac

.field public static final snackbar_action:I = 0x7f1102b3

.field public static final snackbar_text:I = 0x7f1102b2

.field public static final snap:I = 0x7f110053

.field public static final social_button_layout:I = 0x7f11038d

.field public static final spacer:I = 0x7f1100d3

.field public static final split_action_bar:I = 0x7f110022

.field public static final src_atop:I = 0x7f110056

.field public static final src_in:I = 0x7f110057

.field public static final src_over:I = 0x7f110058

.field public static final start:I = 0x7f110033

.field public static final status_bar_latest_event_content:I = 0x7f110444

.field public static final submenuarrow:I = 0x7f1100e5

.field public static final submit_area:I = 0x7f1100f6

.field public static final tabMode:I = 0x7f110048

.field public static final text:I = 0x7f110023

.field public static final text2:I = 0x7f110024

.field public static final textSpacerNoButtons:I = 0x7f1100d9

.field public static final textSpacerNoTitle:I = 0x7f1100d8

.field public static final text_input_password_toggle:I = 0x7f1102b9

.field public static final textinput_counter:I = 0x7f110025

.field public static final textinput_error:I = 0x7f110026

.field public static final thumbnail:I = 0x7f11039b

.field public static final time:I = 0x7f11044a

.field public static final title:I = 0x7f110027

.field public static final titleDividerNoCustom:I = 0x7f1100e0

.field public static final title_bar_text:I = 0x7f11036c

.field public static final title_template:I = 0x7f1100de

.field public static final toolbar:I = 0x7f1100fe

.field public static final toolbar_shadow:I = 0x7f1103b6

.field public static final toolbar_title:I = 0x7f1103cd

.field public static final top:I = 0x7f110034

.field public static final topPanel:I = 0x7f1100dd

.field public static final touch_outside:I = 0x7f1102b0

.field public static final transition_current_scene:I = 0x7f110028

.field public static final transition_layout_save:I = 0x7f110029

.field public static final transition_position:I = 0x7f11002a

.field public static final transition_scene_layoutid_cache:I = 0x7f11002b

.field public static final transition_transform:I = 0x7f11002c

.field public static final twitter_button:I = 0x7f11038e

.field public static final uniform:I = 0x7f11005a

.field public static final up:I = 0x7f11002d

.field public static final updated:I = 0x7f110367

.field public static final useLogo:I = 0x7f11004e

.field public static final view_offset_helper:I = 0x7f11002e

.field public static final visible:I = 0x7f1104c6

.field public static final wallpaper:I = 0x7f1103c6

.field public static final width:I = 0x7f1100c8

.field public static final withText:I = 0x7f11009a

.field public static final wrap_content:I = 0x7f11005b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
