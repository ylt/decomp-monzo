.class public Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;
.super Lio/intercom/android/sdk/activities/IntercomBaseActivity;
.source "IntercomHelpCenterActivity.java"


# static fields
.field private static final ENTRANCE_ANIMATION_TIME_MS:I = 0x12c

.field private static final EXIT_ANIMATION_TIME_MS:I = 0x96

.field public static final LINK_TRANSITION_KEY:Ljava/lang/String; = "link_background"

.field private static final PARCEL_CONVERSATION_ID:Ljava/lang/String; = "parcel_conversation_id"

.field private static final PARCEL_HELP_CENTER_URL:Ljava/lang/String; = "parcel_help_center_url"


# instance fields
.field conversationId:Ljava/lang/String;

.field loadingView:Landroid/widget/ProgressBar;

.field metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

.field titleBarEnabled:Z

.field webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lio/intercom/android/sdk/activities/IntercomBaseActivity;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->titleBarEnabled:Z

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->conversationId:Ljava/lang/String;

    return-void
.end method

.method public static buildIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    const-string v1, "parcel_help_center_url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const-string v1, "parcel_conversation_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 118
    return-object v0
.end method

.method private enterTransition()Landroid/transition/Transition;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 123
    new-instance v0, Landroid/transition/ChangeBounds;

    invoke-direct {v0}, Landroid/transition/ChangeBounds;-><init>()V

    .line 124
    new-instance v1, Landroid/support/v4/view/b/b;

    invoke-direct {v1}, Landroid/support/v4/view/b/b;-><init>()V

    invoke-virtual {v0, v1}, Landroid/transition/ChangeBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 125
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/transition/ChangeBounds;->setDuration(J)Landroid/transition/Transition;

    .line 126
    return-object v0
.end method

.method private fadeOutView(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 102
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 103
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    .line 104
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 106
    return-void
.end method

.method private returnTransition()Landroid/transition/Transition;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 131
    new-instance v0, Landroid/transition/ChangeBounds;

    invoke-direct {v0}, Landroid/transition/ChangeBounds;-><init>()V

    .line 132
    new-instance v1, Landroid/support/v4/view/b/b;

    invoke-direct {v1}, Landroid/support/v4/view/b/b;-><init>()V

    invoke-virtual {v0, v1}, Landroid/transition/ChangeBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 133
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/transition/ChangeBounds;->setDuration(J)Landroid/transition/Transition;

    .line 134
    return-object v0
.end method


# virtual methods
.method closeHelpCenter()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->titleBarEnabled:Z

    .line 97
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->fadeOutView(Landroid/view/View;)V

    .line 98
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->supportFinishAfterTransition()V

    .line 99
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->conversationId:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/MetricTracker;->closedHelpCenter(Ljava/lang/String;I)V

    .line 110
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->closeHelpCenter()V

    .line 111
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v6, 0x15

    const/4 v5, 0x1

    .line 40
    invoke-super {p0, p1}, Lio/intercom/android/sdk/activities/IntercomBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    sget v0, Lio/intercom/android/sdk/R$layout;->intercom_activity_help_center:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->setContentView(I)V

    .line 43
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/intercom/android/sdk/Injector;->getMetricTracker()Lio/intercom/android/sdk/metrics/MetricTracker;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    .line 45
    invoke-virtual {v0}, Lio/intercom/android/sdk/Injector;->getAppConfigProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v0

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    .line 47
    const-string v1, ""

    .line 48
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 49
    if-eqz v2, :cond_0

    .line 50
    const-string v1, "parcel_help_center_url"

    const-string v3, ""

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51
    const-string v3, "parcel_conversation_id"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->conversationId:Ljava/lang/String;

    :cond_0
    move-object v2, v1

    .line 54
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->closeHelpCenter()V

    .line 58
    :cond_1
    sget v1, Lio/intercom/android/sdk/R$id;->loading_view:I

    invoke-virtual {p0, v1}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->loadingView:Landroid/widget/ProgressBar;

    .line 59
    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->loadingView:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 61
    sget v0, Lio/intercom/android/sdk/R$id;->help_center_web_view:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    .line 62
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 63
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    new-instance v1, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;-><init>(Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 74
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_2

    .line 75
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v5}, Landroid/webkit/WebView;->setClipToOutline(Z)V

    .line 77
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 79
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 81
    sget v0, Lio/intercom/android/sdk/R$id;->dismiss:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$2;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$2;-><init>(Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_3

    .line 89
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->enterTransition()Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementEnterTransition(Landroid/transition/Transition;)V

    .line 90
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->returnTransition()Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementReturnTransition(Landroid/transition/Transition;)V

    .line 91
    sget v0, Lio/intercom/android/sdk/R$id;->link_view:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "link_background"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 93
    :cond_3
    return-void
.end method
