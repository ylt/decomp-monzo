.class Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;
.super Landroid/webkit/WebViewClient;
.source "IntercomHelpCenterActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;->this$0:Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;->this$0:Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;

    iget-object v0, v0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 66
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    .line 67
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 69
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;->this$0:Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;

    iget-object v0, v0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->loadingView:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;->this$0:Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;

    iget-object v0, v0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;->this$0:Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;

    iget-object v1, v1, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/metrics/MetricTracker;->viewedHelpCenter(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity$1;->this$0:Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;

    iget-object v0, v0, Lio/intercom/android/sdk/activities/IntercomHelpCenterActivity;->webView:Landroid/webkit/WebView;

    const-string v1, "javascript:Intercom(\'shutdown\')"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 72
    return-void
.end method
