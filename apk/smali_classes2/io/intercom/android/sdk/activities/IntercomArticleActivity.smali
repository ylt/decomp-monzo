.class public Lio/intercom/android/sdk/activities/IntercomArticleActivity;
.super Lio/intercom/android/sdk/activities/IntercomBaseActivity;
.source "IntercomArticleActivity.java"


# static fields
.field private static final ENTRANCE_ANIMATION_TIME_MS:I = 0x12c

.field private static final EXIT_ANIMATION_TIME_MS:I = 0x96

.field public static final LINK_TRANSITION_KEY:Ljava/lang/String; = "link_background"

.field private static final PARCEL_CONVERSATION_ID:Ljava/lang/String; = "parcel_conversation_id"

.field private static final PARCEL_LINK_ID:Ljava/lang/String; = "parcel_link_id"

.field private static final PARCEL_PART_ID:Ljava/lang/String; = "parcel_part_id"


# instance fields
.field api:Lio/intercom/android/sdk/api/Api;

.field private final apiCallback:Lio/intercom/android/sdk/api/BaseCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/api/BaseCallback",
            "<",
            "Lio/intercom/android/sdk/models/LinkResponse$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private author:Landroid/widget/TextView;

.field private avatar:Landroid/widget/ImageView;

.field private avatarSize:I

.field private composerLayout:Landroid/view/View;

.field conversationId:Ljava/lang/String;

.field private description:Landroid/widget/TextView;

.field private intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

.field private linkContainer:Landroid/widget/LinearLayout;

.field linkId:Ljava/lang/String;

.field linkView:Landroid/widget/FrameLayout;

.field loadingView:Landroid/widget/ProgressBar;

.field metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

.field private noteHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

.field private partId:Ljava/lang/String;

.field private reactionComposer:Lio/intercom/android/sdk/conversation/ReactionInputView;

.field private requestManager:Lio/intercom/com/bumptech/glide/i;

.field scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

.field private timeFormatter:Lio/intercom/android/sdk/utilities/TimeFormatter;

.field private title:Landroid/widget/TextView;

.field titleBar:Landroid/widget/FrameLayout;

.field titleBarEnabled:Z

.field titleBarText:Landroid/widget/TextView;

.field titleSize:I

.field private updated:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lio/intercom/android/sdk/activities/IntercomBaseActivity;-><init>()V

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->conversationId:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->partId:Ljava/lang/String;

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->linkId:Ljava/lang/String;

    .line 75
    iput v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleSize:I

    .line 76
    iput-boolean v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleBarEnabled:Z

    .line 178
    new-instance v0, Lio/intercom/android/sdk/activities/IntercomArticleActivity$3;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity$3;-><init>(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)V

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->apiCallback:Lio/intercom/android/sdk/api/BaseCallback;

    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)Lio/intercom/android/sdk/api/BaseCallback;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->apiCallback:Lio/intercom/android/sdk/api/BaseCallback;

    return-object v0
.end method

.method static synthetic access$100(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)Lio/intercom/android/sdk/views/IntercomErrorView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    return-object v0
.end method

.method static synthetic access$200(Lio/intercom/android/sdk/activities/IntercomArticleActivity;Lio/intercom/android/sdk/models/Link;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->updateContent(Lio/intercom/android/sdk/models/Link;)V

    return-void
.end method

.method static synthetic access$300(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->partId:Ljava/lang/String;

    return-object v0
.end method

.method public static buildIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->buildIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static buildIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 324
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lio/intercom/android/sdk/activities/IntercomArticleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 325
    const-string v1, "parcel_link_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    if-eqz p2, :cond_0

    .line 327
    const-string v1, "parcel_part_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    :cond_0
    const-string v1, "parcel_conversation_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 331
    return-object v0
.end method

.method private enterTransition()Landroid/transition/Transition;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 336
    new-instance v0, Landroid/transition/ChangeBounds;

    invoke-direct {v0}, Landroid/transition/ChangeBounds;-><init>()V

    .line 337
    new-instance v1, Landroid/support/v4/view/b/b;

    invoke-direct {v1}, Landroid/support/v4/view/b/b;-><init>()V

    invoke-virtual {v0, v1}, Landroid/transition/ChangeBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 338
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/transition/ChangeBounds;->setDuration(J)Landroid/transition/Transition;

    .line 339
    return-object v0
.end method

.method private fadeOutView(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 308
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 309
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    .line 310
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 312
    return-void
.end method

.method private returnTransition()Landroid/transition/Transition;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 344
    new-instance v0, Landroid/transition/ChangeBounds;

    invoke-direct {v0}, Landroid/transition/ChangeBounds;-><init>()V

    .line 345
    new-instance v1, Landroid/support/v4/view/b/b;

    invoke-direct {v1}, Landroid/support/v4/view/b/b;-><init>()V

    invoke-virtual {v0, v1}, Landroid/transition/ChangeBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 346
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/transition/ChangeBounds;->setDuration(J)Landroid/transition/Transition;

    .line 347
    return-object v0
.end method

.method private setAuthorSpannable(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 291
    new-instance v0, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Written by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 292
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 293
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v2, v1, v2

    .line 294
    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v0, v3, v2, v1, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 296
    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->author:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    return-void
.end method

.method private updateContent(Lio/intercom/android/sdk/models/Link;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x12c

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 195
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getAuthor()Lio/intercom/android/sdk/blocks/models/Author;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/Author;->getAvatar()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lio/intercom/android/sdk/models/Avatar;->create(Ljava/lang/String;Ljava/lang/String;)Lio/intercom/android/sdk/models/Avatar;

    move-result-object v1

    .line 196
    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->avatar:Landroid/widget/ImageView;

    iget v3, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->avatarSize:I

    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    iget-object v4, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-static {v1, v2, v3, v0, v4}, Lio/intercom/android/sdk/utilities/AvatarUtils;->createAvatar(Lio/intercom/android/sdk/models/Avatar;Landroid/widget/ImageView;ILio/intercom/android/sdk/identity/AppConfig;Lio/intercom/com/bumptech/glide/i;)V

    .line 198
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 200
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->description:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 206
    :goto_0
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->title:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleBarText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getAuthor()Lio/intercom/android/sdk/blocks/models/Author;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/Author;->getFirstName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->setAuthorSpannable(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->updated:Landroid/widget/TextView;

    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->timeFormatter:Lio/intercom/android/sdk/utilities/TimeFormatter;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getUpdatedAt()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lio/intercom/android/sdk/utilities/TimeFormatter;->getUpdated(J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->noteHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 215
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getBlocks()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->createLayoutFromBlocks(Lio/intercom/android/sdk/blocks/BlocksViewHolder;Ljava/util/List;Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->linkContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->linkContainer:Landroid/widget/LinearLayout;

    invoke-static {v2, v0, p0}, Lio/intercom/android/sdk/utilities/BlockUtils;->getBlockView(Landroid/view/ViewGroup;Landroid/widget/LinearLayout;Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 218
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleSize:I

    .line 220
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    new-instance v1, Lio/intercom/android/sdk/activities/IntercomArticleActivity$4;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity$4;-><init>(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)V

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/ContentAwareScrollView;->setListener(Lio/intercom/android/sdk/views/ContentAwareScrollView$Listener;)V

    .line 231
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    invoke-virtual {v0, v7}, Lio/intercom/android/sdk/views/ContentAwareScrollView;->setAlpha(F)V

    .line 232
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    invoke-virtual {v0}, Lio/intercom/android/sdk/views/ContentAwareScrollView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 233
    invoke-virtual {v0, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 234
    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 235
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 237
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getReactionReply()Lio/intercom/android/sdk/models/ReactionReply;

    move-result-object v0

    invoke-static {v0}, Lio/intercom/android/sdk/models/ReactionReply;->isNull(Lio/intercom/android/sdk/models/ReactionReply;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getReactionReply()Lio/intercom/android/sdk/models/ReactionReply;

    move-result-object v0

    .line 240
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lio/intercom/android/sdk/R$dimen;->intercom_link_reaction_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 241
    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    iget-object v3, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    invoke-virtual {v3}, Lio/intercom/android/sdk/views/ContentAwareScrollView;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    .line 242
    invoke-virtual {v4}, Lio/intercom/android/sdk/views/ContentAwareScrollView;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    .line 243
    invoke-virtual {v5}, Lio/intercom/android/sdk/views/ContentAwareScrollView;->getPaddingRight()I

    move-result v5

    .line 241
    invoke-virtual {v2, v3, v4, v5, v1}, Lio/intercom/android/sdk/views/ContentAwareScrollView;->setPadding(IIII)V

    .line 246
    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->reactionComposer:Lio/intercom/android/sdk/conversation/ReactionInputView;

    iget-object v3, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-virtual {v2, v0, v3}, Lio/intercom/android/sdk/conversation/ReactionInputView;->preloadReactionImages(Lio/intercom/android/sdk/models/ReactionReply;Lio/intercom/com/bumptech/glide/i;)V

    .line 247
    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->reactionComposer:Lio/intercom/android/sdk/conversation/ReactionInputView;

    new-instance v3, Lio/intercom/android/sdk/activities/IntercomArticleActivity$5;

    invoke-direct {v3, p0, p1}, Lio/intercom/android/sdk/activities/IntercomArticleActivity$5;-><init>(Lio/intercom/android/sdk/activities/IntercomArticleActivity;Lio/intercom/android/sdk/models/Link;)V

    iget-object v4, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-virtual {v2, v0, v6, v3, v4}, Lio/intercom/android/sdk/conversation/ReactionInputView;->setUpReactions(Lio/intercom/android/sdk/models/ReactionReply;ZLio/intercom/android/sdk/conversation/ReactionListener;Lio/intercom/com/bumptech/glide/i;)V

    .line 256
    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->composerLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 257
    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->composerLayout:Landroid/view/View;

    iget-object v3, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->composerLayout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getY()F

    move-result v3

    int-to-float v1, v1

    add-float/2addr v1, v3

    invoke-virtual {v2, v1}, Landroid/view/View;->setY(F)V

    .line 258
    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->composerLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v2, v8}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    .line 259
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 260
    invoke-virtual {v1, v7}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 261
    invoke-virtual {v1, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lio/intercom/android/sdk/activities/IntercomArticleActivity$6;

    invoke-direct {v2, p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity$6;-><init>(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)V

    .line 262
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 267
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 268
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/ReactionReply;->getReactionIndex()Ljava/lang/Integer;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    .line 270
    sget v0, Lio/intercom/android/sdk/R$id;->reaction_text:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_article_response:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 274
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    invoke-virtual {v0}, Lio/intercom/android/sdk/views/ContentAwareScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 275
    new-instance v1, Lio/intercom/android/sdk/activities/IntercomArticleActivity$7;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity$7;-><init>(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 287
    return-void

    .line 202
    :cond_1
    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->description:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->description:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method


# virtual methods
.method closeLink()V
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleBarEnabled:Z

    .line 301
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->fadeOutView(Landroid/view/View;)V

    .line 302
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->composerLayout:Landroid/view/View;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->fadeOutView(Landroid/view/View;)V

    .line 303
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleBar:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->fadeOutView(Landroid/view/View;)V

    .line 304
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->supportFinishAfterTransition()V

    .line 305
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 315
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    iget-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->conversationId:Ljava/lang/String;

    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->linkId:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lio/intercom/android/sdk/metrics/MetricTracker;->closedArticle(Ljava/lang/String;Ljava/lang/String;I)V

    .line 316
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->closeLink()V

    .line 317
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 100
    invoke-super {p0, p1}, Lio/intercom/android/sdk/activities/IntercomBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 101
    sget v0, Lio/intercom/android/sdk/R$layout;->intercom_activity_article:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->setContentView(I)V

    .line 103
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    .line 105
    const-string v1, "parcel_conversation_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->conversationId:Ljava/lang/String;

    .line 106
    const-string v1, "parcel_part_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->partId:Ljava/lang/String;

    .line 107
    const-string v1, "parcel_link_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->linkId:Ljava/lang/String;

    .line 110
    :cond_0
    invoke-static {p0}, Lio/intercom/com/bumptech/glide/c;->a(Landroid/support/v4/app/j;)Lio/intercom/com/bumptech/glide/i;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 112
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getAppConfigProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 115
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getApi()Lio/intercom/android/sdk/api/Api;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->api:Lio/intercom/android/sdk/api/Api;

    .line 116
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->api:Lio/intercom/android/sdk/api/Api;

    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->linkId:Ljava/lang/String;

    iget-object v3, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->apiCallback:Lio/intercom/android/sdk/api/BaseCallback;

    invoke-virtual {v0, v2, v3}, Lio/intercom/android/sdk/api/Api;->getLink(Ljava/lang/String;Lio/intercom/retrofit2/Callback;)V

    .line 118
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getMetricTracker()Lio/intercom/android/sdk/metrics/MetricTracker;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    .line 120
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v8

    .line 122
    new-instance v0, Lio/intercom/android/sdk/utilities/TimeFormatter;

    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getTimeProvider()Lio/intercom/android/sdk/commons/utilities/TimeProvider;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lio/intercom/android/sdk/utilities/TimeFormatter;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/commons/utilities/TimeProvider;)V

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->timeFormatter:Lio/intercom/android/sdk/utilities/TimeFormatter;

    .line 124
    sget v0, Lio/intercom/android/sdk/R$id;->link_view:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->linkView:Landroid/widget/FrameLayout;

    .line 126
    sget v0, Lio/intercom/android/sdk/R$id;->link_title_bar:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleBar:Landroid/widget/FrameLayout;

    .line 127
    sget v0, Lio/intercom/android/sdk/R$id;->title_bar_text:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleBarText:Landroid/widget/TextView;

    .line 128
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->titleBarText:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    sget v0, Lio/intercom/android/sdk/R$id;->loading_view:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->loadingView:Landroid/widget/ProgressBar;

    .line 131
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->loadingView:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v8, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 133
    sget v0, Lio/intercom/android/sdk/R$id;->title:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->title:Landroid/widget/TextView;

    .line 134
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->title:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 136
    sget v0, Lio/intercom/android/sdk/R$id;->description:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->description:Landroid/widget/TextView;

    .line 138
    sget v0, Lio/intercom/android/sdk/R$id;->reaction_input_view:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/conversation/ReactionInputView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->reactionComposer:Lio/intercom/android/sdk/conversation/ReactionInputView;

    .line 139
    sget v0, Lio/intercom/android/sdk/R$id;->link_composer_container:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->composerLayout:Landroid/view/View;

    .line 141
    sget v0, Lio/intercom/android/sdk/R$id;->author:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->author:Landroid/widget/TextView;

    .line 142
    sget v0, Lio/intercom/android/sdk/R$id;->updated:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->updated:Landroid/widget/TextView;

    .line 144
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$dimen;->intercom_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->avatarSize:I

    .line 145
    sget v0, Lio/intercom/android/sdk/R$id;->avatar_view:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->avatar:Landroid/widget/ImageView;

    .line 147
    new-instance v0, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;

    new-instance v1, Lio/intercom/android/sdk/blocks/UploadingImageCache;

    invoke-direct {v1}, Lio/intercom/android/sdk/blocks/UploadingImageCache;-><init>()V

    iget-object v2, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->api:Lio/intercom/android/sdk/api/Api;

    iget-object v3, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v4, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->conversationId:Ljava/lang/String;

    new-instance v5, Lio/intercom/android/sdk/blocks/LightboxOpeningImageClickListener;

    iget-object v6, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->api:Lio/intercom/android/sdk/api/Api;

    invoke-direct {v5, v6}, Lio/intercom/android/sdk/blocks/LightboxOpeningImageClickListener;-><init>(Lio/intercom/android/sdk/api/Api;)V

    new-instance v6, Lio/intercom/android/sdk/blocks/LinkOpeningButtonClickListener;

    iget-object v7, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->api:Lio/intercom/android/sdk/api/Api;

    invoke-direct {v6, v7}, Lio/intercom/android/sdk/blocks/LinkOpeningButtonClickListener;-><init>(Lio/intercom/android/sdk/api/Api;)V

    iget-object v7, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-direct/range {v0 .. v7}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;-><init>(Lio/intercom/android/sdk/blocks/UploadingImageCache;Lio/intercom/android/sdk/api/Api;Lio/intercom/android/sdk/Provider;Ljava/lang/String;Lio/intercom/android/sdk/blocks/ImageClickListener;Lio/intercom/android/sdk/blocks/ButtonClickListener;Lio/intercom/com/bumptech/glide/i;)V

    .line 150
    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->getNoteHolder()Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->noteHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 152
    sget v0, Lio/intercom/android/sdk/R$id;->error_layout_article:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/views/IntercomErrorView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    .line 153
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    invoke-virtual {v0, v8}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonTextColor(I)V

    .line 154
    iget-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    new-instance v1, Lio/intercom/android/sdk/activities/IntercomArticleActivity$1;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity$1;-><init>(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)V

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    sget v0, Lio/intercom/android/sdk/R$id;->link_container:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->linkContainer:Landroid/widget/LinearLayout;

    .line 162
    sget v0, Lio/intercom/android/sdk/R$id;->scroll_view:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/views/ContentAwareScrollView;

    iput-object v0, p0, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->scrollView:Lio/intercom/android/sdk/views/ContentAwareScrollView;

    .line 164
    sget v0, Lio/intercom/android/sdk/R$id;->dismiss:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lio/intercom/android/sdk/activities/IntercomArticleActivity$2;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity$2;-><init>(Lio/intercom/android/sdk/activities/IntercomArticleActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 172
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->enterTransition()Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementEnterTransition(Landroid/transition/Transition;)V

    .line 173
    invoke-virtual {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->returnTransition()Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSharedElementReturnTransition(Landroid/transition/Transition;)V

    .line 174
    sget v0, Lio/intercom/android/sdk/R$id;->link_view:I

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "link_background"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 176
    :cond_1
    return-void
.end method
