.class public final Lio/intercom/android/sdk/actions/Action;
.super Ljava/lang/Object;
.source "Action.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/actions/Action$Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final type:Lio/intercom/android/sdk/actions/Action$Type;

.field private final value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/actions/Action$Type;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/actions/Action$Type;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lio/intercom/android/sdk/actions/Action;->type:Lio/intercom/android/sdk/actions/Action$Type;

    .line 10
    iput-object p2, p0, Lio/intercom/android/sdk/actions/Action;->value:Ljava/lang/Object;

    .line 11
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 27
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    .line 33
    :cond_0
    :goto_0
    return v0

    .line 28
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 30
    check-cast p1, Lio/intercom/android/sdk/actions/Action;

    .line 32
    iget-object v1, p0, Lio/intercom/android/sdk/actions/Action;->type:Lio/intercom/android/sdk/actions/Action$Type;

    iget-object v2, p1, Lio/intercom/android/sdk/actions/Action;->type:Lio/intercom/android/sdk/actions/Action$Type;

    if-ne v1, v2, :cond_0

    .line 33
    iget-object v0, p0, Lio/intercom/android/sdk/actions/Action;->value:Ljava/lang/Object;

    iget-object v1, p1, Lio/intercom/android/sdk/actions/Action;->value:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lio/intercom/android/sdk/actions/Action;->type:Lio/intercom/android/sdk/actions/Action$Type;

    invoke-virtual {v0}, Lio/intercom/android/sdk/actions/Action$Type;->hashCode()I

    move-result v0

    .line 38
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lio/intercom/android/sdk/actions/Action;->value:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lio/intercom/android/sdk/actions/Action;->type:Lio/intercom/android/sdk/actions/Action$Type;

    invoke-virtual {v1}, Lio/intercom/android/sdk/actions/Action$Type;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/actions/Action;->value:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Lio/intercom/android/sdk/actions/Action$Type;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lio/intercom/android/sdk/actions/Action;->type:Lio/intercom/android/sdk/actions/Action$Type;

    return-object v0
.end method

.method public value()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lio/intercom/android/sdk/actions/Action;->value:Ljava/lang/Object;

    return-object v0
.end method
