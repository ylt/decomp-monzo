.class public Lio/intercom/android/sdk/gcm/IntercomGcmRefreshService;
.super Lcom/google/android/gms/iid/InstanceIDListenerService;
.source "IntercomGcmRefreshService.java"


# instance fields
.field private implementation:Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/google/android/gms/iid/InstanceIDListenerService;-><init>()V

    .line 9
    new-instance v0, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;

    invoke-direct {v0}, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/gcm/IntercomGcmRefreshService;->implementation:Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;

    return-void
.end method


# virtual methods
.method public onTokenRefresh()V
    .locals 2

    .prologue
    .line 12
    iget-object v1, p0, Lio/intercom/android/sdk/gcm/IntercomGcmRefreshService;->implementation:Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;

    invoke-virtual {p0}, Lio/intercom/android/sdk/gcm/IntercomGcmRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;->registerToken(Landroid/app/Application;)V

    .line 13
    return-void
.end method
