.class public final Lio/intercom/android/sdk/gcm/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/gcm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0xa

.field public static final ActionBar_backgroundSplit:I = 0xc

.field public static final ActionBar_backgroundStacked:I = 0xb

.field public static final ActionBar_contentInsetEnd:I = 0x15

.field public static final ActionBar_contentInsetEndWithActions:I = 0x19

.field public static final ActionBar_contentInsetLeft:I = 0x16

.field public static final ActionBar_contentInsetRight:I = 0x17

.field public static final ActionBar_contentInsetStart:I = 0x14

.field public static final ActionBar_contentInsetStartWithNavigation:I = 0x18

.field public static final ActionBar_customNavigationLayout:I = 0xd

.field public static final ActionBar_displayOptions:I = 0x3

.field public static final ActionBar_divider:I = 0x9

.field public static final ActionBar_elevation:I = 0x1a

.field public static final ActionBar_height:I = 0x0

.field public static final ActionBar_hideOnContentScroll:I = 0x13

.field public static final ActionBar_homeAsUpIndicator:I = 0x1c

.field public static final ActionBar_homeLayout:I = 0xe

.field public static final ActionBar_icon:I = 0x7

.field public static final ActionBar_indeterminateProgressStyle:I = 0x10

.field public static final ActionBar_itemPadding:I = 0x12

.field public static final ActionBar_logo:I = 0x8

.field public static final ActionBar_navigationMode:I = 0x2

.field public static final ActionBar_popupTheme:I = 0x1b

.field public static final ActionBar_progressBarPadding:I = 0x11

.field public static final ActionBar_progressBarStyle:I = 0xf

.field public static final ActionBar_subtitle:I = 0x4

.field public static final ActionBar_subtitleTextStyle:I = 0x6

.field public static final ActionBar_title:I = 0x1

.field public static final ActionBar_titleTextStyle:I = 0x5

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x3

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_closeItemLayout:I = 0x5

.field public static final ActionMode_height:I = 0x0

.field public static final ActionMode_subtitleTextStyle:I = 0x2

.field public static final ActionMode_titleTextStyle:I = 0x1

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final AlertDialog:[I

.field public static final AlertDialog_android_layout:I = 0x0

.field public static final AlertDialog_buttonPanelSideLayout:I = 0x1

.field public static final AlertDialog_listItemLayout:I = 0x5

.field public static final AlertDialog_listLayout:I = 0x2

.field public static final AlertDialog_multiChoiceItemLayout:I = 0x3

.field public static final AlertDialog_showTitle:I = 0x6

.field public static final AlertDialog_singleChoiceItemLayout:I = 0x4

.field public static final AppBarLayout:[I

.field public static final AppBarLayoutStates:[I

.field public static final AppBarLayoutStates_state_collapsed:I = 0x0

.field public static final AppBarLayoutStates_state_collapsible:I = 0x1

.field public static final AppBarLayout_Layout:[I

.field public static final AppBarLayout_Layout_layout_scrollFlags:I = 0x0

.field public static final AppBarLayout_Layout_layout_scrollInterpolator:I = 0x1

.field public static final AppBarLayout_android_background:I = 0x0

.field public static final AppBarLayout_android_keyboardNavigationCluster:I = 0x2

.field public static final AppBarLayout_android_touchscreenBlocksFocus:I = 0x1

.field public static final AppBarLayout_elevation:I = 0x3

.field public static final AppBarLayout_expanded:I = 0x4

.field public static final AppCompatImageView:[I

.field public static final AppCompatImageView_android_src:I = 0x0

.field public static final AppCompatImageView_srcCompat:I = 0x1

.field public static final AppCompatImageView_tint:I = 0x2

.field public static final AppCompatImageView_tintMode:I = 0x3

.field public static final AppCompatSeekBar:[I

.field public static final AppCompatSeekBar_android_thumb:I = 0x0

.field public static final AppCompatSeekBar_tickMark:I = 0x1

.field public static final AppCompatSeekBar_tickMarkTint:I = 0x2

.field public static final AppCompatSeekBar_tickMarkTintMode:I = 0x3

.field public static final AppCompatTextHelper:[I

.field public static final AppCompatTextHelper_android_drawableBottom:I = 0x2

.field public static final AppCompatTextHelper_android_drawableEnd:I = 0x6

.field public static final AppCompatTextHelper_android_drawableLeft:I = 0x3

.field public static final AppCompatTextHelper_android_drawableRight:I = 0x4

.field public static final AppCompatTextHelper_android_drawableStart:I = 0x5

.field public static final AppCompatTextHelper_android_drawableTop:I = 0x1

.field public static final AppCompatTextHelper_android_textAppearance:I = 0x0

.field public static final AppCompatTextView:[I

.field public static final AppCompatTextView_android_textAppearance:I = 0x0

.field public static final AppCompatTextView_autoSizeMaxTextSize:I = 0x6

.field public static final AppCompatTextView_autoSizeMinTextSize:I = 0x5

.field public static final AppCompatTextView_autoSizePresetSizes:I = 0x4

.field public static final AppCompatTextView_autoSizeStepGranularity:I = 0x3

.field public static final AppCompatTextView_autoSizeTextType:I = 0x2

.field public static final AppCompatTextView_fontFamily:I = 0x7

.field public static final AppCompatTextView_textAllCaps:I = 0x1

.field public static final AppCompatTheme:[I

.field public static final AppCompatTheme_actionBarDivider:I = 0x17

.field public static final AppCompatTheme_actionBarItemBackground:I = 0x18

.field public static final AppCompatTheme_actionBarPopupTheme:I = 0x11

.field public static final AppCompatTheme_actionBarSize:I = 0x16

.field public static final AppCompatTheme_actionBarSplitStyle:I = 0x13

.field public static final AppCompatTheme_actionBarStyle:I = 0x12

.field public static final AppCompatTheme_actionBarTabBarStyle:I = 0xd

.field public static final AppCompatTheme_actionBarTabStyle:I = 0xc

.field public static final AppCompatTheme_actionBarTabTextStyle:I = 0xe

.field public static final AppCompatTheme_actionBarTheme:I = 0x14

.field public static final AppCompatTheme_actionBarWidgetTheme:I = 0x15

.field public static final AppCompatTheme_actionButtonStyle:I = 0x32

.field public static final AppCompatTheme_actionDropDownStyle:I = 0x2e

.field public static final AppCompatTheme_actionMenuTextAppearance:I = 0x19

.field public static final AppCompatTheme_actionMenuTextColor:I = 0x1a

.field public static final AppCompatTheme_actionModeBackground:I = 0x1d

.field public static final AppCompatTheme_actionModeCloseButtonStyle:I = 0x1c

.field public static final AppCompatTheme_actionModeCloseDrawable:I = 0x1f

.field public static final AppCompatTheme_actionModeCopyDrawable:I = 0x21

.field public static final AppCompatTheme_actionModeCutDrawable:I = 0x20

.field public static final AppCompatTheme_actionModeFindDrawable:I = 0x25

.field public static final AppCompatTheme_actionModePasteDrawable:I = 0x22

.field public static final AppCompatTheme_actionModePopupWindowStyle:I = 0x27

.field public static final AppCompatTheme_actionModeSelectAllDrawable:I = 0x23

.field public static final AppCompatTheme_actionModeShareDrawable:I = 0x24

.field public static final AppCompatTheme_actionModeSplitBackground:I = 0x1e

.field public static final AppCompatTheme_actionModeStyle:I = 0x1b

.field public static final AppCompatTheme_actionModeWebSearchDrawable:I = 0x26

.field public static final AppCompatTheme_actionOverflowButtonStyle:I = 0xf

.field public static final AppCompatTheme_actionOverflowMenuStyle:I = 0x10

.field public static final AppCompatTheme_activityChooserViewStyle:I = 0x3a

.field public static final AppCompatTheme_alertDialogButtonGroupStyle:I = 0x5f

.field public static final AppCompatTheme_alertDialogCenterButtons:I = 0x60

.field public static final AppCompatTheme_alertDialogStyle:I = 0x5e

.field public static final AppCompatTheme_alertDialogTheme:I = 0x61

.field public static final AppCompatTheme_android_windowAnimationStyle:I = 0x1

.field public static final AppCompatTheme_android_windowIsFloating:I = 0x0

.field public static final AppCompatTheme_autoCompleteTextViewStyle:I = 0x66

.field public static final AppCompatTheme_borderlessButtonStyle:I = 0x37

.field public static final AppCompatTheme_buttonBarButtonStyle:I = 0x34

.field public static final AppCompatTheme_buttonBarNegativeButtonStyle:I = 0x64

.field public static final AppCompatTheme_buttonBarNeutralButtonStyle:I = 0x65

.field public static final AppCompatTheme_buttonBarPositiveButtonStyle:I = 0x63

.field public static final AppCompatTheme_buttonBarStyle:I = 0x33

.field public static final AppCompatTheme_buttonStyle:I = 0x67

.field public static final AppCompatTheme_buttonStyleSmall:I = 0x68

.field public static final AppCompatTheme_checkboxStyle:I = 0x69

.field public static final AppCompatTheme_checkedTextViewStyle:I = 0x6a

.field public static final AppCompatTheme_colorAccent:I = 0x56

.field public static final AppCompatTheme_colorBackgroundFloating:I = 0x5d

.field public static final AppCompatTheme_colorButtonNormal:I = 0x5a

.field public static final AppCompatTheme_colorControlActivated:I = 0x58

.field public static final AppCompatTheme_colorControlHighlight:I = 0x59

.field public static final AppCompatTheme_colorControlNormal:I = 0x57

.field public static final AppCompatTheme_colorError:I = 0x76

.field public static final AppCompatTheme_colorPrimary:I = 0x54

.field public static final AppCompatTheme_colorPrimaryDark:I = 0x55

.field public static final AppCompatTheme_colorSwitchThumbNormal:I = 0x5b

.field public static final AppCompatTheme_controlBackground:I = 0x5c

.field public static final AppCompatTheme_dialogPreferredPadding:I = 0x2c

.field public static final AppCompatTheme_dialogTheme:I = 0x2b

.field public static final AppCompatTheme_dividerHorizontal:I = 0x39

.field public static final AppCompatTheme_dividerVertical:I = 0x38

.field public static final AppCompatTheme_dropDownListViewStyle:I = 0x4b

.field public static final AppCompatTheme_dropdownListPreferredItemHeight:I = 0x2f

.field public static final AppCompatTheme_editTextBackground:I = 0x40

.field public static final AppCompatTheme_editTextColor:I = 0x3f

.field public static final AppCompatTheme_editTextStyle:I = 0x6b

.field public static final AppCompatTheme_homeAsUpIndicator:I = 0x31

.field public static final AppCompatTheme_imageButtonStyle:I = 0x41

.field public static final AppCompatTheme_listChoiceBackgroundIndicator:I = 0x53

.field public static final AppCompatTheme_listDividerAlertDialog:I = 0x2d

.field public static final AppCompatTheme_listMenuViewStyle:I = 0x73

.field public static final AppCompatTheme_listPopupWindowStyle:I = 0x4c

.field public static final AppCompatTheme_listPreferredItemHeight:I = 0x46

.field public static final AppCompatTheme_listPreferredItemHeightLarge:I = 0x48

.field public static final AppCompatTheme_listPreferredItemHeightSmall:I = 0x47

.field public static final AppCompatTheme_listPreferredItemPaddingLeft:I = 0x49

.field public static final AppCompatTheme_listPreferredItemPaddingRight:I = 0x4a

.field public static final AppCompatTheme_panelBackground:I = 0x50

.field public static final AppCompatTheme_panelMenuListTheme:I = 0x52

.field public static final AppCompatTheme_panelMenuListWidth:I = 0x51

.field public static final AppCompatTheme_popupMenuStyle:I = 0x3d

.field public static final AppCompatTheme_popupWindowStyle:I = 0x3e

.field public static final AppCompatTheme_radioButtonStyle:I = 0x6c

.field public static final AppCompatTheme_ratingBarStyle:I = 0x6d

.field public static final AppCompatTheme_ratingBarStyleIndicator:I = 0x6e

.field public static final AppCompatTheme_ratingBarStyleSmall:I = 0x6f

.field public static final AppCompatTheme_searchViewStyle:I = 0x45

.field public static final AppCompatTheme_seekBarStyle:I = 0x70

.field public static final AppCompatTheme_selectableItemBackground:I = 0x35

.field public static final AppCompatTheme_selectableItemBackgroundBorderless:I = 0x36

.field public static final AppCompatTheme_spinnerDropDownItemStyle:I = 0x30

.field public static final AppCompatTheme_spinnerStyle:I = 0x71

.field public static final AppCompatTheme_switchStyle:I = 0x72

.field public static final AppCompatTheme_textAppearanceLargePopupMenu:I = 0x28

.field public static final AppCompatTheme_textAppearanceListItem:I = 0x4d

.field public static final AppCompatTheme_textAppearanceListItemSecondary:I = 0x4e

.field public static final AppCompatTheme_textAppearanceListItemSmall:I = 0x4f

.field public static final AppCompatTheme_textAppearancePopupMenuHeader:I = 0x2a

.field public static final AppCompatTheme_textAppearanceSearchResultSubtitle:I = 0x43

.field public static final AppCompatTheme_textAppearanceSearchResultTitle:I = 0x42

.field public static final AppCompatTheme_textAppearanceSmallPopupMenu:I = 0x29

.field public static final AppCompatTheme_textColorAlertDialogListItem:I = 0x62

.field public static final AppCompatTheme_textColorSearchUrl:I = 0x44

.field public static final AppCompatTheme_toolbarNavigationButtonStyle:I = 0x3c

.field public static final AppCompatTheme_toolbarStyle:I = 0x3b

.field public static final AppCompatTheme_tooltipForegroundColor:I = 0x75

.field public static final AppCompatTheme_tooltipFrameBackground:I = 0x74

.field public static final AppCompatTheme_windowActionBar:I = 0x2

.field public static final AppCompatTheme_windowActionBarOverlay:I = 0x4

.field public static final AppCompatTheme_windowActionModeOverlay:I = 0x5

.field public static final AppCompatTheme_windowFixedHeightMajor:I = 0x9

.field public static final AppCompatTheme_windowFixedHeightMinor:I = 0x7

.field public static final AppCompatTheme_windowFixedWidthMajor:I = 0x6

.field public static final AppCompatTheme_windowFixedWidthMinor:I = 0x8

.field public static final AppCompatTheme_windowMinWidthMajor:I = 0xa

.field public static final AppCompatTheme_windowMinWidthMinor:I = 0xb

.field public static final AppCompatTheme_windowNoTitle:I = 0x3

.field public static final AuthorAvatarView:[I

.field public static final AuthorAvatarView_activeStateSize:I = 0x1

.field public static final AuthorAvatarView_avatarSize:I = 0x0

.field public static final BottomNavigationView:[I

.field public static final BottomNavigationView_elevation:I = 0x0

.field public static final BottomNavigationView_itemBackground:I = 0x4

.field public static final BottomNavigationView_itemIconTint:I = 0x2

.field public static final BottomNavigationView_itemTextColor:I = 0x3

.field public static final BottomNavigationView_menu:I = 0x1

.field public static final BottomSheetBehavior_Layout:[I

.field public static final BottomSheetBehavior_Layout_behavior_hideable:I = 0x1

.field public static final BottomSheetBehavior_Layout_behavior_peekHeight:I = 0x0

.field public static final BottomSheetBehavior_Layout_behavior_skipCollapsed:I = 0x2

.field public static final ButtonBarLayout:[I

.field public static final ButtonBarLayout_allowStacking:I = 0x0

.field public static final CollapsingToolbarLayout:[I

.field public static final CollapsingToolbarLayout_Layout:[I

.field public static final CollapsingToolbarLayout_Layout_layout_collapseMode:I = 0x0

.field public static final CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier:I = 0x1

.field public static final CollapsingToolbarLayout_collapsedTitleGravity:I = 0xd

.field public static final CollapsingToolbarLayout_collapsedTitleTextAppearance:I = 0x7

.field public static final CollapsingToolbarLayout_contentScrim:I = 0x8

.field public static final CollapsingToolbarLayout_expandedTitleGravity:I = 0xe

.field public static final CollapsingToolbarLayout_expandedTitleMargin:I = 0x1

.field public static final CollapsingToolbarLayout_expandedTitleMarginBottom:I = 0x5

.field public static final CollapsingToolbarLayout_expandedTitleMarginEnd:I = 0x4

.field public static final CollapsingToolbarLayout_expandedTitleMarginStart:I = 0x2

.field public static final CollapsingToolbarLayout_expandedTitleMarginTop:I = 0x3

.field public static final CollapsingToolbarLayout_expandedTitleTextAppearance:I = 0x6

.field public static final CollapsingToolbarLayout_scrimAnimationDuration:I = 0xc

.field public static final CollapsingToolbarLayout_scrimVisibleHeightTrigger:I = 0xb

.field public static final CollapsingToolbarLayout_statusBarScrim:I = 0x9

.field public static final CollapsingToolbarLayout_title:I = 0x0

.field public static final CollapsingToolbarLayout_titleEnabled:I = 0xf

.field public static final CollapsingToolbarLayout_toolbarId:I = 0xa

.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x2

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final CompoundButton:[I

.field public static final CompoundButton_android_button:I = 0x0

.field public static final CompoundButton_buttonTint:I = 0x1

.field public static final CompoundButton_buttonTintMode:I = 0x2

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x6

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x3

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final DesignTheme:[I

.field public static final DesignTheme_bottomSheetDialogTheme:I = 0x0

.field public static final DesignTheme_bottomSheetStyle:I = 0x1

.field public static final DesignTheme_textColorError:I = 0x2

.field public static final DrawerArrowToggle:[I

.field public static final DrawerArrowToggle_arrowHeadLength:I = 0x4

.field public static final DrawerArrowToggle_arrowShaftLength:I = 0x5

.field public static final DrawerArrowToggle_barLength:I = 0x6

.field public static final DrawerArrowToggle_color:I = 0x0

.field public static final DrawerArrowToggle_drawableSize:I = 0x2

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x3

.field public static final DrawerArrowToggle_spinBars:I = 0x1

.field public static final DrawerArrowToggle_thickness:I = 0x7

.field public static final ExpandableLayout:[I

.field public static final ExpandableLayout_intercomCanExpand:I = 0x0

.field public static final FloatingActionButton:[I

.field public static final FloatingActionButton_Behavior_Layout:[I

.field public static final FloatingActionButton_Behavior_Layout_behavior_autoHide:I = 0x0

.field public static final FloatingActionButton_backgroundTint:I = 0x6

.field public static final FloatingActionButton_backgroundTintMode:I = 0x7

.field public static final FloatingActionButton_borderWidth:I = 0x4

.field public static final FloatingActionButton_elevation:I = 0x0

.field public static final FloatingActionButton_fabSize:I = 0x2

.field public static final FloatingActionButton_pressedTranslationZ:I = 0x3

.field public static final FloatingActionButton_rippleColor:I = 0x1

.field public static final FloatingActionButton_useCompatPadding:I = 0x5

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_font:I = 0x1

.field public static final FontFamilyFont_fontStyle:I = 0x0

.field public static final FontFamilyFont_fontWeight:I = 0x2

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x3

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x4

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x5

.field public static final FontFamily_fontProviderPackage:I = 0x1

.field public static final FontFamily_fontProviderQuery:I = 0x2

.field public static final ForegroundLinearLayout:[I

.field public static final ForegroundLinearLayout_android_foreground:I = 0x0

.field public static final ForegroundLinearLayout_android_foregroundGravity:I = 0x1

.field public static final ForegroundLinearLayout_foregroundInsidePadding:I = 0x2

.field public static final LinearLayoutCompat:[I

.field public static final LinearLayoutCompat_Layout:[I

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x2

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x3

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x1

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x2

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayoutCompat_android_gravity:I = 0x0

.field public static final LinearLayoutCompat_android_orientation:I = 0x1

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4

.field public static final LinearLayoutCompat_divider:I = 0x5

.field public static final LinearLayoutCompat_dividerPadding:I = 0x8

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x6

.field public static final LinearLayoutCompat_showDividers:I = 0x7

.field public static final ListPopupWindow:[I

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x2

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x0

.field public static final LockableScrollView:[I

.field public static final LockableScrollView_intercomExpanded:I = 0x1

.field public static final LockableScrollView_intercomHeightLimit:I = 0x0

.field public static final LockableScrollView_intercomInterceptTouch:I = 0x2

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0x10

.field public static final MenuItem_actionProviderClass:I = 0x12

.field public static final MenuItem_actionViewClass:I = 0x11

.field public static final MenuItem_alphabeticModifiers:I = 0xd

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_contentDescription:I = 0x13

.field public static final MenuItem_iconTint:I = 0x15

.field public static final MenuItem_iconTintMode:I = 0x16

.field public static final MenuItem_numericModifiers:I = 0xe

.field public static final MenuItem_showAsAction:I = 0xf

.field public static final MenuItem_tooltipText:I = 0x14

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final MenuView_subMenuArrow:I = 0x8

.field public static final NavigationView:[I

.field public static final NavigationView_android_background:I = 0x0

.field public static final NavigationView_android_fitsSystemWindows:I = 0x1

.field public static final NavigationView_android_maxWidth:I = 0x2

.field public static final NavigationView_elevation:I = 0x3

.field public static final NavigationView_headerLayout:I = 0x9

.field public static final NavigationView_itemBackground:I = 0x7

.field public static final NavigationView_itemIconTint:I = 0x5

.field public static final NavigationView_itemTextAppearance:I = 0x8

.field public static final NavigationView_itemTextColor:I = 0x6

.field public static final NavigationView_menu:I = 0x4

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_android_popupAnimationStyle:I = 0x1

.field public static final PopupWindow_android_popupBackground:I = 0x0

.field public static final PopupWindow_overlapAnchor:I = 0x2

.field public static final RecycleListView:[I

.field public static final RecycleListView_paddingBottomNoButtons:I = 0x0

.field public static final RecycleListView_paddingTopNoTitle:I = 0x1

.field public static final RecyclerView:[I

.field public static final RecyclerView_android_descendantFocusability:I = 0x1

.field public static final RecyclerView_android_orientation:I = 0x0

.field public static final RecyclerView_fastScrollEnabled:I = 0x6

.field public static final RecyclerView_fastScrollHorizontalThumbDrawable:I = 0x9

.field public static final RecyclerView_fastScrollHorizontalTrackDrawable:I = 0xa

.field public static final RecyclerView_fastScrollVerticalThumbDrawable:I = 0x7

.field public static final RecyclerView_fastScrollVerticalTrackDrawable:I = 0x8

.field public static final RecyclerView_layoutManager:I = 0x2

.field public static final RecyclerView_reverseLayout:I = 0x4

.field public static final RecyclerView_spanCount:I = 0x3

.field public static final RecyclerView_stackFromEnd:I = 0x5

.field public static final ScrimInsetsFrameLayout:[I

.field public static final ScrimInsetsFrameLayout_insetForeground:I = 0x0

.field public static final ScrollingViewBehavior_Layout:[I

.field public static final ScrollingViewBehavior_Layout_behavior_overlapTop:I = 0x0

.field public static final SearchView:[I

.field public static final SearchView_android_focusable:I = 0x0

.field public static final SearchView_android_imeOptions:I = 0x3

.field public static final SearchView_android_inputType:I = 0x2

.field public static final SearchView_android_maxWidth:I = 0x1

.field public static final SearchView_closeIcon:I = 0x8

.field public static final SearchView_commitIcon:I = 0xd

.field public static final SearchView_defaultQueryHint:I = 0x7

.field public static final SearchView_goIcon:I = 0x9

.field public static final SearchView_iconifiedByDefault:I = 0x5

.field public static final SearchView_layout:I = 0x4

.field public static final SearchView_queryBackground:I = 0xf

.field public static final SearchView_queryHint:I = 0x6

.field public static final SearchView_searchHintIcon:I = 0xb

.field public static final SearchView_searchIcon:I = 0xa

.field public static final SearchView_submitBackground:I = 0x10

.field public static final SearchView_suggestionRowLayout:I = 0xe

.field public static final SearchView_voiceIcon:I = 0xc

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2

.field public static final SnackbarLayout:[I

.field public static final SnackbarLayout_android_maxWidth:I = 0x0

.field public static final SnackbarLayout_elevation:I = 0x1

.field public static final SnackbarLayout_maxActionInlineWidth:I = 0x2

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_entries:I = 0x0

.field public static final Spinner_android_popupBackground:I = 0x1

.field public static final Spinner_android_prompt:I = 0x2

.field public static final Spinner_popupTheme:I = 0x4

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_showText:I = 0xd

.field public static final SwitchCompat_splitTrack:I = 0xc

.field public static final SwitchCompat_switchMinWidth:I = 0xa

.field public static final SwitchCompat_switchPadding:I = 0xb

.field public static final SwitchCompat_switchTextAppearance:I = 0x9

.field public static final SwitchCompat_thumbTextPadding:I = 0x8

.field public static final SwitchCompat_thumbTint:I = 0x3

.field public static final SwitchCompat_thumbTintMode:I = 0x4

.field public static final SwitchCompat_track:I = 0x5

.field public static final SwitchCompat_trackTint:I = 0x6

.field public static final SwitchCompat_trackTintMode:I = 0x7

.field public static final TabItem:[I

.field public static final TabItem_android_icon:I = 0x0

.field public static final TabItem_android_layout:I = 0x1

.field public static final TabItem_android_text:I = 0x2

.field public static final TabLayout:[I

.field public static final TabLayout_tabBackground:I = 0x3

.field public static final TabLayout_tabContentStart:I = 0x2

.field public static final TabLayout_tabGravity:I = 0x5

.field public static final TabLayout_tabIndicatorColor:I = 0x0

.field public static final TabLayout_tabIndicatorHeight:I = 0x1

.field public static final TabLayout_tabMaxWidth:I = 0x7

.field public static final TabLayout_tabMinWidth:I = 0x6

.field public static final TabLayout_tabMode:I = 0x4

.field public static final TabLayout_tabPadding:I = 0xf

.field public static final TabLayout_tabPaddingBottom:I = 0xe

.field public static final TabLayout_tabPaddingEnd:I = 0xd

.field public static final TabLayout_tabPaddingStart:I = 0xb

.field public static final TabLayout_tabPaddingTop:I = 0xc

.field public static final TabLayout_tabSelectedTextColor:I = 0xa

.field public static final TabLayout_tabTextAppearance:I = 0x8

.field public static final TabLayout_tabTextColor:I = 0x9

.field public static final TextAppearance:[I

.field public static final TextAppearance_android_fontFamily:I = 0xa

.field public static final TextAppearance_android_shadowColor:I = 0x6

.field public static final TextAppearance_android_shadowDx:I = 0x7

.field public static final TextAppearance_android_shadowDy:I = 0x8

.field public static final TextAppearance_android_shadowRadius:I = 0x9

.field public static final TextAppearance_android_textColor:I = 0x3

.field public static final TextAppearance_android_textColorHint:I = 0x4

.field public static final TextAppearance_android_textColorLink:I = 0x5

.field public static final TextAppearance_android_textSize:I = 0x0

.field public static final TextAppearance_android_textStyle:I = 0x2

.field public static final TextAppearance_android_typeface:I = 0x1

.field public static final TextAppearance_fontFamily:I = 0xc

.field public static final TextAppearance_textAllCaps:I = 0xb

.field public static final TextInputLayout:[I

.field public static final TextInputLayout_android_hint:I = 0x1

.field public static final TextInputLayout_android_textColorHint:I = 0x0

.field public static final TextInputLayout_counterEnabled:I = 0x6

.field public static final TextInputLayout_counterMaxLength:I = 0x7

.field public static final TextInputLayout_counterOverflowTextAppearance:I = 0x9

.field public static final TextInputLayout_counterTextAppearance:I = 0x8

.field public static final TextInputLayout_errorEnabled:I = 0x2

.field public static final TextInputLayout_errorTextAppearance:I = 0x5

.field public static final TextInputLayout_hintAnimationEnabled:I = 0xa

.field public static final TextInputLayout_hintEnabled:I = 0x4

.field public static final TextInputLayout_hintTextAppearance:I = 0x3

.field public static final TextInputLayout_passwordToggleContentDescription:I = 0xd

.field public static final TextInputLayout_passwordToggleDrawable:I = 0xc

.field public static final TextInputLayout_passwordToggleEnabled:I = 0xb

.field public static final TextInputLayout_passwordToggleTint:I = 0xe

.field public static final TextInputLayout_passwordToggleTintMode:I = 0xf

.field public static final Toolbar:[I

.field public static final Toolbar_android_gravity:I = 0x0

.field public static final Toolbar_android_minHeight:I = 0x1

.field public static final Toolbar_buttonGravity:I = 0x15

.field public static final Toolbar_collapseContentDescription:I = 0x17

.field public static final Toolbar_collapseIcon:I = 0x16

.field public static final Toolbar_contentInsetEnd:I = 0x6

.field public static final Toolbar_contentInsetEndWithActions:I = 0xa

.field public static final Toolbar_contentInsetLeft:I = 0x7

.field public static final Toolbar_contentInsetRight:I = 0x8

.field public static final Toolbar_contentInsetStart:I = 0x5

.field public static final Toolbar_contentInsetStartWithNavigation:I = 0x9

.field public static final Toolbar_logo:I = 0x4

.field public static final Toolbar_logoDescription:I = 0x1a

.field public static final Toolbar_maxButtonHeight:I = 0x14

.field public static final Toolbar_navigationContentDescription:I = 0x19

.field public static final Toolbar_navigationIcon:I = 0x18

.field public static final Toolbar_popupTheme:I = 0xb

.field public static final Toolbar_subtitle:I = 0x3

.field public static final Toolbar_subtitleTextAppearance:I = 0xd

.field public static final Toolbar_subtitleTextColor:I = 0x1c

.field public static final Toolbar_title:I = 0x2

.field public static final Toolbar_titleMargin:I = 0xe

.field public static final Toolbar_titleMarginBottom:I = 0x12

.field public static final Toolbar_titleMarginEnd:I = 0x10

.field public static final Toolbar_titleMarginStart:I = 0xf

.field public static final Toolbar_titleMarginTop:I = 0x11

.field public static final Toolbar_titleMargins:I = 0x13

.field public static final Toolbar_titleTextAppearance:I = 0xc

.field public static final Toolbar_titleTextColor:I = 0x1b

.field public static final View:[I

.field public static final ViewBackgroundHelper:[I

.field public static final ViewBackgroundHelper_android_background:I = 0x0

.field public static final ViewBackgroundHelper_backgroundTint:I = 0x1

.field public static final ViewBackgroundHelper_backgroundTintMode:I = 0x2

.field public static final ViewStubCompat:[I

.field public static final ViewStubCompat_android_id:I = 0x0

.field public static final ViewStubCompat_android_inflatedId:I = 0x2

.field public static final ViewStubCompat_android_layout:I = 0x1

.field public static final View_android_focusable:I = 0x1

.field public static final View_android_theme:I = 0x0

.field public static final View_paddingEnd:I = 0x3

.field public static final View_paddingStart:I = 0x2

.field public static final View_theme:I = 0x4

.field public static final intercom_composer_empty_view:[I

.field public static final intercom_composer_empty_view_intercom_composer_actionButtonText:I = 0x2

.field public static final intercom_composer_empty_view_intercom_composer_internalPaddingBottom:I = 0x4

.field public static final intercom_composer_empty_view_intercom_composer_internalPaddingLeft:I = 0x5

.field public static final intercom_composer_empty_view_intercom_composer_internalPaddingRight:I = 0x6

.field public static final intercom_composer_empty_view_intercom_composer_internalPaddingTop:I = 0x3

.field public static final intercom_composer_empty_view_intercom_composer_subtitleText:I = 0x1

.field public static final intercom_composer_empty_view_intercom_composer_titleText:I

.field public static final intercom_composer_square_layout:[I

.field public static final intercom_composer_square_layout_intercom_composer_measure_type:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 2045
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ActionBar:[I

    .line 2075
    new-array v0, v4, [I

    const v1, 0x10100b3

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ActionBarLayout:[I

    .line 2077
    new-array v0, v4, [I

    const v1, 0x101013f

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ActionMenuItemView:[I

    .line 2079
    new-array v0, v3, [I

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ActionMenuView:[I

    .line 2080
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ActionMode:[I

    .line 2087
    new-array v0, v5, [I

    fill-array-data v0, :array_2

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ActivityChooserView:[I

    .line 2090
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AlertDialog:[I

    .line 2098
    new-array v0, v6, [I

    fill-array-data v0, :array_4

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AppBarLayout:[I

    .line 2104
    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AppBarLayoutStates:[I

    .line 2107
    new-array v0, v5, [I

    fill-array-data v0, :array_6

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AppBarLayout_Layout:[I

    .line 2110
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AppCompatImageView:[I

    .line 2115
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AppCompatSeekBar:[I

    .line 2120
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AppCompatTextHelper:[I

    .line 2128
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AppCompatTextView:[I

    .line 2137
    const/16 v0, 0x77

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AppCompatTheme:[I

    .line 2257
    new-array v0, v5, [I

    fill-array-data v0, :array_c

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->AuthorAvatarView:[I

    .line 2260
    new-array v0, v6, [I

    fill-array-data v0, :array_d

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->BottomNavigationView:[I

    .line 2266
    new-array v0, v2, [I

    fill-array-data v0, :array_e

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->BottomSheetBehavior_Layout:[I

    .line 2270
    new-array v0, v4, [I

    const v1, 0x7f010105

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ButtonBarLayout:[I

    .line 2272
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->CollapsingToolbarLayout:[I

    .line 2289
    new-array v0, v5, [I

    fill-array-data v0, :array_10

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->CollapsingToolbarLayout_Layout:[I

    .line 2292
    new-array v0, v2, [I

    fill-array-data v0, :array_11

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ColorStateListItem:[I

    .line 2296
    new-array v0, v2, [I

    fill-array-data v0, :array_12

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->CompoundButton:[I

    .line 2300
    new-array v0, v5, [I

    fill-array-data v0, :array_13

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->CoordinatorLayout:[I

    .line 2303
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_14

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->CoordinatorLayout_Layout:[I

    .line 2311
    new-array v0, v2, [I

    fill-array-data v0, :array_15

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->DesignTheme:[I

    .line 2315
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_16

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->DrawerArrowToggle:[I

    .line 2324
    new-array v0, v4, [I

    const v1, 0x7f01015e

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ExpandableLayout:[I

    .line 2326
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->FloatingActionButton:[I

    .line 2335
    new-array v0, v4, [I

    const v1, 0x7f010179

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->FloatingActionButton_Behavior_Layout:[I

    .line 2337
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->FontFamily:[I

    .line 2344
    new-array v0, v2, [I

    fill-array-data v0, :array_19

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->FontFamilyFont:[I

    .line 2348
    new-array v0, v2, [I

    fill-array-data v0, :array_1a

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ForegroundLinearLayout:[I

    .line 2352
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1b

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->LinearLayoutCompat:[I

    .line 2362
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1c

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->LinearLayoutCompat_Layout:[I

    .line 2367
    new-array v0, v5, [I

    fill-array-data v0, :array_1d

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ListPopupWindow:[I

    .line 2370
    new-array v0, v2, [I

    fill-array-data v0, :array_1e

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->LoadingImageView:[I

    .line 2374
    new-array v0, v2, [I

    fill-array-data v0, :array_1f

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->LockableScrollView:[I

    .line 2378
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_20

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->MenuGroup:[I

    .line 2385
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_21

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->MenuItem:[I

    .line 2409
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_22

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->MenuView:[I

    .line 2419
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_23

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->NavigationView:[I

    .line 2430
    new-array v0, v2, [I

    fill-array-data v0, :array_24

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->PopupWindow:[I

    .line 2434
    new-array v0, v4, [I

    const v1, 0x7f0101d7

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->PopupWindowBackgroundState:[I

    .line 2436
    new-array v0, v5, [I

    fill-array-data v0, :array_25

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->RecycleListView:[I

    .line 2439
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_26

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->RecyclerView:[I

    .line 2451
    new-array v0, v4, [I

    const v1, 0x7f0101e3

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ScrimInsetsFrameLayout:[I

    .line 2453
    new-array v0, v4, [I

    const v1, 0x7f0101e4

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ScrollingViewBehavior_Layout:[I

    .line 2455
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_27

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->SearchView:[I

    .line 2473
    new-array v0, v2, [I

    fill-array-data v0, :array_28

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->SignInButton:[I

    .line 2477
    new-array v0, v2, [I

    fill-array-data v0, :array_29

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->SnackbarLayout:[I

    .line 2481
    new-array v0, v6, [I

    fill-array-data v0, :array_2a

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->Spinner:[I

    .line 2487
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_2b

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->SwitchCompat:[I

    .line 2502
    new-array v0, v2, [I

    fill-array-data v0, :array_2c

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->TabItem:[I

    .line 2506
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_2d

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->TabLayout:[I

    .line 2523
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_2e

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->TextAppearance:[I

    .line 2537
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_2f

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->TextInputLayout:[I

    .line 2554
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_30

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->Toolbar:[I

    .line 2584
    new-array v0, v6, [I

    fill-array-data v0, :array_31

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->View:[I

    .line 2590
    new-array v0, v2, [I

    fill-array-data v0, :array_32

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ViewBackgroundHelper:[I

    .line 2594
    new-array v0, v2, [I

    fill-array-data v0, :array_33

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->ViewStubCompat:[I

    .line 2598
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_34

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->intercom_composer_empty_view:[I

    .line 2606
    new-array v0, v4, [I

    const v1, 0x7f010267

    aput v1, v0, v3

    sput-object v0, Lio/intercom/android/sdk/gcm/R$styleable;->intercom_composer_square_layout:[I

    return-void

    .line 2045
    :array_0
    .array-data 4
        0x7f010008
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f0100ba
    .end array-data

    .line 2080
    :array_1
    .array-data 4
        0x7f010008
        0x7f010043
        0x7f010044
        0x7f010048
        0x7f01004a
        0x7f01005a
    .end array-data

    .line 2087
    :array_2
    .array-data 4
        0x7f01005f
        0x7f010060
    .end array-data

    .line 2090
    :array_3
    .array-data 4
        0x10100f2
        0x7f010062
        0x7f010063
        0x7f010064
        0x7f010065
        0x7f010066
        0x7f010067
    .end array-data

    .line 2098
    :array_4
    .array-data 4
        0x10100d4
        0x101048f
        0x1010540
        0x7f010058
        0x7f010079
    .end array-data

    .line 2104
    :array_5
    .array-data 4
        0x7f01007a
        0x7f01007b
    .end array-data

    .line 2107
    :array_6
    .array-data 4
        0x7f01007c
        0x7f01007d
    .end array-data

    .line 2110
    :array_7
    .array-data 4
        0x1010119
        0x7f01007e
        0x7f01007f
        0x7f010080
    .end array-data

    .line 2115
    :array_8
    .array-data 4
        0x1010142
        0x7f010081
        0x7f010082
        0x7f010083
    .end array-data

    .line 2120
    :array_9
    .array-data 4
        0x1010034
        0x101016d
        0x101016e
        0x101016f
        0x1010170
        0x1010392
        0x1010393
    .end array-data

    .line 2128
    :array_a
    .array-data 4
        0x1010034
        0x7f010084
        0x7f010085
        0x7f010086
        0x7f010087
        0x7f010088
        0x7f010089
        0x7f01008a
    .end array-data

    .line 2137
    :array_b
    .array-data 4
        0x1010057
        0x10100ae
        0x7f01008b
        0x7f01008c
        0x7f01008d
        0x7f01008e
        0x7f01008f
        0x7f010090
        0x7f010091
        0x7f010092
        0x7f010093
        0x7f010094
        0x7f010095
        0x7f010096
        0x7f010097
        0x7f010098
        0x7f010099
        0x7f01009a
        0x7f01009b
        0x7f01009c
        0x7f01009d
        0x7f01009e
        0x7f01009f
        0x7f0100a0
        0x7f0100a1
        0x7f0100a2
        0x7f0100a3
        0x7f0100a4
        0x7f0100a5
        0x7f0100a6
        0x7f0100a7
        0x7f0100a8
        0x7f0100a9
        0x7f0100aa
        0x7f0100ab
        0x7f0100ac
        0x7f0100ad
        0x7f0100ae
        0x7f0100af
        0x7f0100b0
        0x7f0100b1
        0x7f0100b2
        0x7f0100b3
        0x7f0100b4
        0x7f0100b5
        0x7f0100b6
        0x7f0100b7
        0x7f0100b8
        0x7f0100b9
        0x7f0100ba
        0x7f0100bb
        0x7f0100bc
        0x7f0100bd
        0x7f0100be
        0x7f0100bf
        0x7f0100c0
        0x7f0100c1
        0x7f0100c2
        0x7f0100c3
        0x7f0100c4
        0x7f0100c5
        0x7f0100c6
        0x7f0100c7
        0x7f0100c8
        0x7f0100c9
        0x7f0100ca
        0x7f0100cb
        0x7f0100cc
        0x7f0100cd
        0x7f0100ce
        0x7f0100cf
        0x7f0100d0
        0x7f0100d1
        0x7f0100d2
        0x7f0100d3
        0x7f0100d4
        0x7f0100d5
        0x7f0100d6
        0x7f0100d7
        0x7f0100d8
        0x7f0100d9
        0x7f0100da
        0x7f0100db
        0x7f0100dc
        0x7f0100dd
        0x7f0100de
        0x7f0100df
        0x7f0100e0
        0x7f0100e1
        0x7f0100e2
        0x7f0100e3
        0x7f0100e4
        0x7f0100e5
        0x7f0100e6
        0x7f0100e7
        0x7f0100e8
        0x7f0100e9
        0x7f0100ea
        0x7f0100eb
        0x7f0100ec
        0x7f0100ed
        0x7f0100ee
        0x7f0100ef
        0x7f0100f0
        0x7f0100f1
        0x7f0100f2
        0x7f0100f3
        0x7f0100f4
        0x7f0100f5
        0x7f0100f6
        0x7f0100f7
        0x7f0100f8
        0x7f0100f9
        0x7f0100fa
        0x7f0100fb
        0x7f0100fc
        0x7f0100fd
        0x7f0100fe
        0x7f0100ff
    .end array-data

    .line 2257
    :array_c
    .array-data 4
        0x7f010100
        0x7f010101
    .end array-data

    .line 2260
    :array_d
    .array-data 4
        0x7f010058
        0x7f0101c7
        0x7f0101c8
        0x7f0101c9
        0x7f0101ca
    .end array-data

    .line 2266
    :array_e
    .array-data 4
        0x7f010102
        0x7f010103
        0x7f010104
    .end array-data

    .line 2272
    :array_f
    .array-data 4
        0x7f01003f
        0x7f010125
        0x7f010126
        0x7f010127
        0x7f010128
        0x7f010129
        0x7f01012a
        0x7f01012b
        0x7f01012c
        0x7f01012d
        0x7f01012e
        0x7f01012f
        0x7f010130
        0x7f010131
        0x7f010132
        0x7f010133
    .end array-data

    .line 2289
    :array_10
    .array-data 4
        0x7f010134
        0x7f010135
    .end array-data

    .line 2292
    :array_11
    .array-data 4
        0x10101a5
        0x101031f
        0x7f010136
    .end array-data

    .line 2296
    :array_12
    .array-data 4
        0x1010107
        0x7f010137
        0x7f010138
    .end array-data

    .line 2300
    :array_13
    .array-data 4
        0x7f010139
        0x7f01013a
    .end array-data

    .line 2303
    :array_14
    .array-data 4
        0x10100b3
        0x7f01013b
        0x7f01013c
        0x7f01013d
        0x7f01013e
        0x7f01013f
        0x7f010140
    .end array-data

    .line 2311
    :array_15
    .array-data 4
        0x7f010151
        0x7f010152
        0x7f010153
    .end array-data

    .line 2315
    :array_16
    .array-data 4
        0x7f010154
        0x7f010155
        0x7f010156
        0x7f010157
        0x7f010158
        0x7f010159
        0x7f01015a
        0x7f01015b
    .end array-data

    .line 2326
    :array_17
    .array-data 4
        0x7f010058
        0x7f010174
        0x7f010175
        0x7f010176
        0x7f010177
        0x7f010178
        0x7f010243
        0x7f010244
    .end array-data

    .line 2337
    :array_18
    .array-data 4
        0x7f01017a
        0x7f01017b
        0x7f01017c
        0x7f01017d
        0x7f01017e
        0x7f01017f
    .end array-data

    .line 2344
    :array_19
    .array-data 4
        0x7f010180
        0x7f010181
        0x7f010182
    .end array-data

    .line 2348
    :array_1a
    .array-data 4
        0x1010109
        0x1010200
        0x7f010183
    .end array-data

    .line 2352
    :array_1b
    .array-data 4
        0x10100af
        0x10100c4
        0x1010126
        0x1010127
        0x1010128
        0x7f010047
        0x7f010186
        0x7f010187
        0x7f010188
    .end array-data

    .line 2362
    :array_1c
    .array-data 4
        0x10100b3
        0x10100f4
        0x10100f5
        0x1010181
    .end array-data

    .line 2367
    :array_1d
    .array-data 4
        0x10102ac
        0x10102ad
    .end array-data

    .line 2370
    :array_1e
    .array-data 4
        0x7f010195
        0x7f010196
        0x7f010197
    .end array-data

    .line 2374
    :array_1f
    .array-data 4
        0x7f010198
        0x7f010199
        0x7f01019a
    .end array-data

    .line 2378
    :array_20
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    .line 2385
    :array_21
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f0101bb
        0x7f0101bc
        0x7f0101bd
        0x7f0101be
        0x7f0101bf
        0x7f0101c0
        0x7f0101c1
        0x7f0101c2
        0x7f0101c3
        0x7f0101c4
    .end array-data

    .line 2409
    :array_22
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x7f0101c5
        0x7f0101c6
    .end array-data

    .line 2419
    :array_23
    .array-data 4
        0x10100d4
        0x10100dd
        0x101011f
        0x7f010058
        0x7f0101c7
        0x7f0101c8
        0x7f0101c9
        0x7f0101ca
        0x7f0101cb
        0x7f0101cc
    .end array-data

    .line 2430
    :array_24
    .array-data 4
        0x1010176
        0x10102c9
        0x7f0101d6
    .end array-data

    .line 2436
    :array_25
    .array-data 4
        0x7f0101d8
        0x7f0101d9
    .end array-data

    .line 2439
    :array_26
    .array-data 4
        0x10100c4
        0x10100f1
        0x7f0101da
        0x7f0101db
        0x7f0101dc
        0x7f0101dd
        0x7f0101de
        0x7f0101df
        0x7f0101e0
        0x7f0101e1
        0x7f0101e2
    .end array-data

    .line 2455
    :array_27
    .array-data 4
        0x10100da
        0x101011f
        0x1010220
        0x1010264
        0x7f0101e5
        0x7f0101e6
        0x7f0101e7
        0x7f0101e8
        0x7f0101e9
        0x7f0101ea
        0x7f0101eb
        0x7f0101ec
        0x7f0101ed
        0x7f0101ee
        0x7f0101ef
        0x7f0101f0
        0x7f0101f1
    .end array-data

    .line 2473
    :array_28
    .array-data 4
        0x7f0101f2
        0x7f0101f3
        0x7f0101f4
    .end array-data

    .line 2477
    :array_29
    .array-data 4
        0x101011f
        0x7f010058
        0x7f0101fa
    .end array-data

    .line 2481
    :array_2a
    .array-data 4
        0x10100b2
        0x1010176
        0x101017b
        0x1010262
        0x7f010059
    .end array-data

    .line 2487
    :array_2b
    .array-data 4
        0x1010124
        0x1010125
        0x1010142
        0x7f0101fb
        0x7f0101fc
        0x7f0101fd
        0x7f0101fe
        0x7f0101ff
        0x7f010200
        0x7f010201
        0x7f010202
        0x7f010203
        0x7f010204
        0x7f010205
    .end array-data

    .line 2502
    :array_2c
    .array-data 4
        0x1010002
        0x10100f2
        0x101014f
    .end array-data

    .line 2506
    :array_2d
    .array-data 4
        0x7f010206
        0x7f010207
        0x7f010208
        0x7f010209
        0x7f01020a
        0x7f01020b
        0x7f01020c
        0x7f01020d
        0x7f01020e
        0x7f01020f
        0x7f010210
        0x7f010211
        0x7f010212
        0x7f010213
        0x7f010214
        0x7f010215
    .end array-data

    .line 2523
    :array_2e
    .array-data 4
        0x1010095
        0x1010096
        0x1010097
        0x1010098
        0x101009a
        0x101009b
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x10103ac
        0x7f010084
        0x7f01008a
    .end array-data

    .line 2537
    :array_2f
    .array-data 4
        0x101009a
        0x1010150
        0x7f01006a
        0x7f010216
        0x7f010217
        0x7f010218
        0x7f010219
        0x7f01021a
        0x7f01021b
        0x7f01021c
        0x7f01021d
        0x7f01021e
        0x7f01021f
        0x7f010220
        0x7f010221
        0x7f010222
    .end array-data

    .line 2554
    :array_30
    .array-data 4
        0x10100af
        0x1010140
        0x7f01003f
        0x7f010042
        0x7f010046
        0x7f010052
        0x7f010053
        0x7f010054
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010059
        0x7f01022b
        0x7f01022c
        0x7f01022d
        0x7f01022e
        0x7f01022f
        0x7f010230
        0x7f010231
        0x7f010232
        0x7f010233
        0x7f010234
        0x7f010235
        0x7f010236
        0x7f010237
        0x7f010238
        0x7f010239
        0x7f01023a
        0x7f01023b
    .end array-data

    .line 2584
    :array_31
    .array-data 4
        0x1010000
        0x10100da
        0x7f010240
        0x7f010241
        0x7f010242
    .end array-data

    .line 2590
    :array_32
    .array-data 4
        0x10100d4
        0x7f010243
        0x7f010244
    .end array-data

    .line 2594
    :array_33
    .array-data 4
        0x10100d0
        0x10100f2
        0x10100f3
    .end array-data

    .line 2598
    :array_34
    .array-data 4
        0x7f010260
        0x7f010261
        0x7f010262
        0x7f010263
        0x7f010264
        0x7f010265
        0x7f010266
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
