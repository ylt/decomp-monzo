.class public final Lio/intercom/android/sdk/gcm/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/gcm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f050000

.field public static final abc_action_bar_up_container:I = 0x7f050001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f050002

.field public static final abc_action_menu_item_layout:I = 0x7f050003

.field public static final abc_action_menu_layout:I = 0x7f050004

.field public static final abc_action_mode_bar:I = 0x7f050005

.field public static final abc_action_mode_close_item_material:I = 0x7f050006

.field public static final abc_activity_chooser_view:I = 0x7f050007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f050008

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f050009

.field public static final abc_alert_dialog_material:I = 0x7f05000a

.field public static final abc_alert_dialog_title_material:I = 0x7f05000b

.field public static final abc_dialog_title_material:I = 0x7f05000c

.field public static final abc_expanded_menu_layout:I = 0x7f05000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f05000e

.field public static final abc_list_menu_item_icon:I = 0x7f05000f

.field public static final abc_list_menu_item_layout:I = 0x7f050010

.field public static final abc_list_menu_item_radio:I = 0x7f050011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f050012

.field public static final abc_popup_menu_item_layout:I = 0x7f050013

.field public static final abc_screen_content_include:I = 0x7f050014

.field public static final abc_screen_simple:I = 0x7f050015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f050016

.field public static final abc_screen_toolbar:I = 0x7f050017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f050018

.field public static final abc_search_view:I = 0x7f050019

.field public static final abc_select_dialog_material:I = 0x7f05001a

.field public static final design_bottom_navigation_item:I = 0x7f050085

.field public static final design_bottom_sheet_dialog:I = 0x7f050086

.field public static final design_layout_snackbar:I = 0x7f050087

.field public static final design_layout_snackbar_include:I = 0x7f050088

.field public static final design_layout_tab_icon:I = 0x7f050089

.field public static final design_layout_tab_text:I = 0x7f05008a

.field public static final design_menu_item_action_area:I = 0x7f05008b

.field public static final design_navigation_item:I = 0x7f05008c

.field public static final design_navigation_item_header:I = 0x7f05008d

.field public static final design_navigation_item_separator:I = 0x7f05008e

.field public static final design_navigation_item_subheader:I = 0x7f05008f

.field public static final design_navigation_menu:I = 0x7f050090

.field public static final design_navigation_menu_item:I = 0x7f050091

.field public static final design_text_input_password_icon:I = 0x7f050092

.field public static final intercom_activity_article:I = 0x7f0500b3

.field public static final intercom_activity_help_center:I = 0x7f0500b4

.field public static final intercom_activity_lightbox:I = 0x7f0500b5

.field public static final intercom_activity_messenger:I = 0x7f0500b6

.field public static final intercom_activity_messenger_two_pane:I = 0x7f0500b7

.field public static final intercom_activity_note:I = 0x7f0500b8

.field public static final intercom_activity_post:I = 0x7f0500b9

.field public static final intercom_admin_is_typing:I = 0x7f0500ba

.field public static final intercom_admin_profile:I = 0x7f0500bb

.field public static final intercom_blocks_admin_layout:I = 0x7f0500bc

.field public static final intercom_blocks_container_card_layout:I = 0x7f0500bd

.field public static final intercom_blocks_container_layout:I = 0x7f0500be

.field public static final intercom_blocks_note_layout:I = 0x7f0500bf

.field public static final intercom_blocks_user_layout:I = 0x7f0500c0

.field public static final intercom_composer_activity_gallery_lightbox:I = 0x7f0500c1

.field public static final intercom_composer_activity_input_full_screen:I = 0x7f0500c2

.field public static final intercom_composer_edit_text:I = 0x7f0500c3

.field public static final intercom_composer_empty_view:I = 0x7f0500c4

.field public static final intercom_composer_empty_view_layout:I = 0x7f0500c5

.field public static final intercom_composer_expanded_image_list_item:I = 0x7f0500c6

.field public static final intercom_composer_fragment_composer_gallery:I = 0x7f0500c7

.field public static final intercom_composer_fragment_composer_gallery_expanded:I = 0x7f0500c8

.field public static final intercom_composer_fragment_empty:I = 0x7f0500c9

.field public static final intercom_composer_gallery_lightbox_fragment:I = 0x7f0500ca

.field public static final intercom_composer_holder:I = 0x7f0500cb

.field public static final intercom_composer_image_list_item:I = 0x7f0500cc

.field public static final intercom_composer_input_icon_view_layout:I = 0x7f0500cd

.field public static final intercom_composer_layout:I = 0x7f0500ce

.field public static final intercom_composer_loading_view:I = 0x7f0500cf

.field public static final intercom_composer_view_layout:I = 0x7f0500d0

.field public static final intercom_container_layout:I = 0x7f0500d1

.field public static final intercom_conversation_coordinator:I = 0x7f0500d2

.field public static final intercom_conversation_rating_block:I = 0x7f0500d3

.field public static final intercom_day_divider:I = 0x7f0500d4

.field public static final intercom_default_launcher:I = 0x7f0500d5

.field public static final intercom_error_article:I = 0x7f0500d6

.field public static final intercom_error_conversation:I = 0x7f0500d7

.field public static final intercom_error_inbox:I = 0x7f0500d8

.field public static final intercom_expander_button:I = 0x7f0500d9

.field public static final intercom_fake_composer:I = 0x7f0500da

.field public static final intercom_fragment_conversation:I = 0x7f0500db

.field public static final intercom_fragment_inbox:I = 0x7f0500dc

.field public static final intercom_gallery_input_toolbar:I = 0x7f0500dd

.field public static final intercom_gif_input_search:I = 0x7f0500de

.field public static final intercom_gif_input_toolbar:I = 0x7f0500df

.field public static final intercom_link_block:I = 0x7f0500e0

.field public static final intercom_messenger_activity_layout:I = 0x7f050148

.field public static final intercom_notification_pill:I = 0x7f0500e1

.field public static final intercom_onboarding_layout:I = 0x7f0500e2

.field public static final intercom_preview_chat_full_overlay:I = 0x7f0500e3

.field public static final intercom_preview_chat_snippet_body:I = 0x7f0500e4

.field public static final intercom_preview_chat_snippet_overlay:I = 0x7f0500e5

.field public static final intercom_preview_notification:I = 0x7f0500e6

.field public static final intercom_row_admin_part:I = 0x7f0500e7

.field public static final intercom_row_conversation_rating:I = 0x7f0500e8

.field public static final intercom_row_event:I = 0x7f0500e9

.field public static final intercom_row_inbox:I = 0x7f0500ea

.field public static final intercom_row_link:I = 0x7f0500eb

.field public static final intercom_row_link_list:I = 0x7f0500ec

.field public static final intercom_row_link_reply:I = 0x7f0500ed

.field public static final intercom_row_loading:I = 0x7f0500ee

.field public static final intercom_row_note:I = 0x7f0500ef

.field public static final intercom_row_post:I = 0x7f0500f0

.field public static final intercom_row_user_part:I = 0x7f0500f1

.field public static final intercom_team_profile:I = 0x7f0500f2

.field public static final intercom_toolbar:I = 0x7f0500f3

.field public static final notification_action:I = 0x7f050114

.field public static final notification_action_tombstone:I = 0x7f050115

.field public static final notification_media_action:I = 0x7f050116

.field public static final notification_media_cancel_action:I = 0x7f050117

.field public static final notification_template_big_media:I = 0x7f050118

.field public static final notification_template_big_media_custom:I = 0x7f050119

.field public static final notification_template_big_media_narrow:I = 0x7f05011a

.field public static final notification_template_big_media_narrow_custom:I = 0x7f05011b

.field public static final notification_template_custom_big:I = 0x7f05011c

.field public static final notification_template_icon_group:I = 0x7f05011d

.field public static final notification_template_lines_media:I = 0x7f05011e

.field public static final notification_template_media:I = 0x7f05011f

.field public static final notification_template_media_custom:I = 0x7f050120

.field public static final notification_template_part_chronometer:I = 0x7f050121

.field public static final notification_template_part_time:I = 0x7f050122

.field public static final select_dialog_item_material:I = 0x7f05012c

.field public static final select_dialog_multichoice_material:I = 0x7f05012d

.field public static final select_dialog_singlechoice_material:I = 0x7f05012e

.field public static final support_simple_spinner_dropdown_item:I = 0x7f05012f

.field public static final tooltip:I = 0x7f050132


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
