.class public final Lio/intercom/android/sdk/gcm/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/gcm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0f00f3

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0f00f4

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0f00f5

.field public static final abc_btn_colored_text_material:I = 0x7f0f00f6

.field public static final abc_color_highlight_material:I = 0x7f0f00f7

.field public static final abc_hint_foreground_material_dark:I = 0x7f0f00f8

.field public static final abc_hint_foreground_material_light:I = 0x7f0f00f9

.field public static final abc_input_method_navigation_guard:I = 0x7f0f0001

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0f00fa

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0f00fb

.field public static final abc_primary_text_material_dark:I = 0x7f0f00fc

.field public static final abc_primary_text_material_light:I = 0x7f0f00fd

.field public static final abc_search_url_text:I = 0x7f0f00fe

.field public static final abc_search_url_text_normal:I = 0x7f0f0002

.field public static final abc_search_url_text_pressed:I = 0x7f0f0003

.field public static final abc_search_url_text_selected:I = 0x7f0f0004

.field public static final abc_secondary_text_material_dark:I = 0x7f0f00ff

.field public static final abc_secondary_text_material_light:I = 0x7f0f0100

.field public static final abc_tint_btn_checkable:I = 0x7f0f0101

.field public static final abc_tint_default:I = 0x7f0f0102

.field public static final abc_tint_edittext:I = 0x7f0f0103

.field public static final abc_tint_seek_thumb:I = 0x7f0f0104

.field public static final abc_tint_spinner:I = 0x7f0f0105

.field public static final abc_tint_switch_track:I = 0x7f0f0106

.field public static final accent_material_dark:I = 0x7f0f0008

.field public static final accent_material_light:I = 0x7f0f0009

.field public static final background_floating_material_dark:I = 0x7f0f0013

.field public static final background_floating_material_light:I = 0x7f0f0014

.field public static final background_material_dark:I = 0x7f0f0015

.field public static final background_material_light:I = 0x7f0f0016

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0f001f

.field public static final bright_foreground_disabled_material_light:I = 0x7f0f0020

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0f0021

.field public static final bright_foreground_inverse_material_light:I = 0x7f0f0022

.field public static final bright_foreground_material_dark:I = 0x7f0f0023

.field public static final bright_foreground_material_light:I = 0x7f0f0024

.field public static final button_material_dark:I = 0x7f0f0025

.field public static final button_material_light:I = 0x7f0f0026

.field public static final common_google_signin_btn_text_dark:I = 0x7f0f0109

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f0f004d

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f0f004e

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0f004f

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f0f0050

.field public static final common_google_signin_btn_text_light:I = 0x7f0f010a

.field public static final common_google_signin_btn_text_light_default:I = 0x7f0f0051

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f0f0052

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0f0053

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f0f0054

.field public static final common_google_signin_btn_tint:I = 0x7f0f010b

.field public static final design_bottom_navigation_shadow_color:I = 0x7f0f005a

.field public static final design_error:I = 0x7f0f010c

.field public static final design_fab_shadow_end_color:I = 0x7f0f005b

.field public static final design_fab_shadow_mid_color:I = 0x7f0f005c

.field public static final design_fab_shadow_start_color:I = 0x7f0f005d

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0f005e

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0f005f

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0f0060

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0f0061

.field public static final design_snackbar_background_color:I = 0x7f0f0062

.field public static final design_tint_password_toggle:I = 0x7f0f010d

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0f0064

.field public static final dim_foreground_disabled_material_light:I = 0x7f0f0065

.field public static final dim_foreground_material_dark:I = 0x7f0f0066

.field public static final dim_foreground_material_light:I = 0x7f0f0067

.field public static final error_color_material:I = 0x7f0f0068

.field public static final foreground_material_dark:I = 0x7f0f006c

.field public static final foreground_material_light:I = 0x7f0f006d

.field public static final highlighted_text_material_dark:I = 0x7f0f0077

.field public static final highlighted_text_material_light:I = 0x7f0f0078

.field public static final intercom_active_state:I = 0x7f0f007a

.field public static final intercom_admin_block_background:I = 0x7f0f007b

.field public static final intercom_away_state:I = 0x7f0f007c

.field public static final intercom_black:I = 0x7f0f007d

.field public static final intercom_border_color:I = 0x7f0f007e

.field public static final intercom_composer_blue:I = 0x7f0f007f

.field public static final intercom_composer_border:I = 0x7f0f0080

.field public static final intercom_composer_empty_view_background:I = 0x7f0f0081

.field public static final intercom_composer_empty_view_subtitle:I = 0x7f0f0082

.field public static final intercom_composer_empty_view_title:I = 0x7f0f0083

.field public static final intercom_composer_full_screen_toolbar:I = 0x7f0f0084

.field public static final intercom_composer_ripple_dark:I = 0x7f0f0085

.field public static final intercom_composer_semi_transparent_black:I = 0x7f0f0086

.field public static final intercom_composer_status_bar:I = 0x7f0f0087

.field public static final intercom_composer_transparent:I = 0x7f0f0088

.field public static final intercom_composer_transparent_black_lightbox:I = 0x7f0f0089

.field public static final intercom_composer_white:I = 0x7f0f008a

.field public static final intercom_container_border:I = 0x7f0f008b

.field public static final intercom_conversation_rating_text:I = 0x7f0f008c

.field public static final intercom_disabled_button_color:I = 0x7f0f008d

.field public static final intercom_error_state_empty_avatar:I = 0x7f0f008e

.field public static final intercom_error_state_title:I = 0x7f0f008f

.field public static final intercom_full_transparent_full_black:I = 0x7f0f0090

.field public static final intercom_full_transparent_full_white:I = 0x7f0f0091

.field public static final intercom_grey_100:I = 0x7f0f0092

.field public static final intercom_grey_200:I = 0x7f0f0093

.field public static final intercom_grey_400:I = 0x7f0f0094

.field public static final intercom_grey_500:I = 0x7f0f0095

.field public static final intercom_grey_600:I = 0x7f0f0096

.field public static final intercom_grey_700:I = 0x7f0f0097

.field public static final intercom_grey_800:I = 0x7f0f0098

.field public static final intercom_image_preview_grey:I = 0x7f0f0099

.field public static final intercom_inbox_count_background:I = 0x7f0f009a

.field public static final intercom_input_default_color:I = 0x7f0f009b

.field public static final intercom_input_text_hint_color:I = 0x7f0f009c

.field public static final intercom_is_typing_grey:I = 0x7f0f009d

.field public static final intercom_link_text:I = 0x7f0f009e

.field public static final intercom_list_divider_grey:I = 0x7f0f009f

.field public static final intercom_main_blue:I = 0x7f0f00a0

.field public static final intercom_note_grey:I = 0x7f0f00a1

.field public static final intercom_note_tint:I = 0x7f0f00a2

.field public static final intercom_note_title_grey:I = 0x7f0f00a3

.field public static final intercom_reaction_shadow:I = 0x7f0f00a4

.field public static final intercom_search_bg_grey:I = 0x7f0f00a5

.field public static final intercom_search_border_grey:I = 0x7f0f00a6

.field public static final intercom_search_text_grey:I = 0x7f0f00a7

.field public static final intercom_semi_transparent:I = 0x7f0f00a8

.field public static final intercom_semi_transparent_white:I = 0x7f0f00a9

.field public static final intercom_slate_grey_two:I = 0x7f0f00aa

.field public static final intercom_toolbar_hint:I = 0x7f0f00ab

.field public static final intercom_transparent_black:I = 0x7f0f00ac

.field public static final intercom_transparent_black_lightbox:I = 0x7f0f00ad

.field public static final intercom_transparent_white:I = 0x7f0f00ae

.field public static final intercom_white:I = 0x7f0f00af

.field public static final intercom_white_alpha_20:I = 0x7f0f00b0

.field public static final material_blue_grey_800:I = 0x7f0f00b3

.field public static final material_blue_grey_900:I = 0x7f0f00b4

.field public static final material_blue_grey_950:I = 0x7f0f00b5

.field public static final material_deep_teal_200:I = 0x7f0f00b6

.field public static final material_deep_teal_500:I = 0x7f0f00b7

.field public static final material_grey_100:I = 0x7f0f00b8

.field public static final material_grey_300:I = 0x7f0f00b9

.field public static final material_grey_50:I = 0x7f0f00ba

.field public static final material_grey_600:I = 0x7f0f00bb

.field public static final material_grey_800:I = 0x7f0f00bc

.field public static final material_grey_850:I = 0x7f0f00bd

.field public static final material_grey_900:I = 0x7f0f00be

.field public static final notification_action_color_filter:I = 0x7f0f0000

.field public static final notification_icon_bg_color:I = 0x7f0f00c4

.field public static final notification_material_background_media_default_color:I = 0x7f0f00c5

.field public static final primary_dark_material_dark:I = 0x7f0f00c8

.field public static final primary_dark_material_light:I = 0x7f0f00c9

.field public static final primary_material_dark:I = 0x7f0f00ca

.field public static final primary_material_light:I = 0x7f0f00cb

.field public static final primary_text_default_material_dark:I = 0x7f0f00cc

.field public static final primary_text_default_material_light:I = 0x7f0f00cd

.field public static final primary_text_disabled_material_dark:I = 0x7f0f00ce

.field public static final primary_text_disabled_material_light:I = 0x7f0f00cf

.field public static final ripple_material_dark:I = 0x7f0f00d1

.field public static final ripple_material_light:I = 0x7f0f00d2

.field public static final secondary_text_default_material_dark:I = 0x7f0f00d3

.field public static final secondary_text_default_material_light:I = 0x7f0f00d4

.field public static final secondary_text_disabled_material_dark:I = 0x7f0f00d5

.field public static final secondary_text_disabled_material_light:I = 0x7f0f00d6

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0f00d7

.field public static final switch_thumb_disabled_material_light:I = 0x7f0f00d8

.field public static final switch_thumb_material_dark:I = 0x7f0f010f

.field public static final switch_thumb_material_light:I = 0x7f0f0110

.field public static final switch_thumb_normal_material_dark:I = 0x7f0f00d9

.field public static final switch_thumb_normal_material_light:I = 0x7f0f00da

.field public static final tooltip_background_dark:I = 0x7f0f00db

.field public static final tooltip_background_light:I = 0x7f0f00dc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
