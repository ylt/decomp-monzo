.class public Lio/intercom/android/sdk/gcm/IntercomGcmListenerService;
.super Lcom/google/android/gms/gcm/GcmListenerService;
.source "IntercomGcmListenerService.java"


# instance fields
.field private final pushClient:Lio/intercom/android/sdk/push/IntercomPushClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/android/gms/gcm/GcmListenerService;-><init>()V

    .line 10
    new-instance v0, Lio/intercom/android/sdk/push/IntercomPushClient;

    invoke-direct {v0}, Lio/intercom/android/sdk/push/IntercomPushClient;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/gcm/IntercomGcmListenerService;->pushClient:Lio/intercom/android/sdk/push/IntercomPushClient;

    return-void
.end method


# virtual methods
.method public onMessageReceived(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 13
    iget-object v0, p0, Lio/intercom/android/sdk/gcm/IntercomGcmListenerService;->pushClient:Lio/intercom/android/sdk/push/IntercomPushClient;

    invoke-virtual {p0}, Lio/intercom/android/sdk/gcm/IntercomGcmListenerService;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lio/intercom/android/sdk/push/IntercomPushClient;->handlePush(Landroid/app/Application;Landroid/os/Bundle;)V

    .line 14
    return-void
.end method
