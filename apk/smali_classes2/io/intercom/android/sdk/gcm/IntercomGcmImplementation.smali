.class public Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;
.super Ljava/lang/Object;
.source "IntercomGcmImplementation.java"

# interfaces
.implements Lio/intercom/android/sdk/IntercomPushManager$GcmImplementation;


# instance fields
.field private final pushClient:Lio/intercom/android/sdk/push/IntercomPushClient;

.field private final twig:Lio/intercom/android/sdk/twig/Twig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;

    invoke-direct {v0}, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;-><init>()V

    sput-object v0, Lio/intercom/android/sdk/IntercomPushManager;->gcmImplementation:Lio/intercom/android/sdk/IntercomPushManager$GcmImplementation;

    .line 20
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lio/intercom/android/sdk/push/IntercomPushClient;

    invoke-direct {v0}, Lio/intercom/android/sdk/push/IntercomPushClient;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;->pushClient:Lio/intercom/android/sdk/push/IntercomPushClient;

    .line 23
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;->twig:Lio/intercom/android/sdk/twig/Twig;

    return-void
.end method

.method private printTokenError()V
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "Intercom push registration failed. Please make sure the following string is in your strings.xml file: <string name=\"intercom_gcm_sender_id\">YOUR_SENDER_ID</string>"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    return-void
.end method


# virtual methods
.method public registerToken(Landroid/app/Application;)V
    .locals 4

    .prologue
    .line 26
    invoke-static {p1}, Lcom/google/android/gms/iid/InstanceID;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/iid/InstanceID;

    move-result-object v0

    .line 27
    invoke-static {p1}, Lio/intercom/android/sdk/IntercomPushManager;->getSenderId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 29
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 30
    invoke-direct {p0}, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;->printTokenError()V

    .line 39
    :goto_0
    return-void

    .line 33
    :cond_0
    :try_start_0
    const-string v2, "GCM"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/iid/InstanceID;->getToken(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;->pushClient:Lio/intercom/android/sdk/push/IntercomPushClient;

    invoke-virtual {v1, p1, v0}, Lio/intercom/android/sdk/push/IntercomPushClient;->sendTokenToIntercom(Landroid/app/Application;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    iget-object v1, p0, Lio/intercom/android/sdk/gcm/IntercomGcmImplementation;->twig:Lio/intercom/android/sdk/twig/Twig;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Upload failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
