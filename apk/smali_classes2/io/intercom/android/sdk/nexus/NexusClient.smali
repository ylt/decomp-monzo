.class public Lio/intercom/android/sdk/nexus/NexusClient;
.super Ljava/lang/Object;
.source "NexusClient.java"

# interfaces
.implements Lio/intercom/android/sdk/nexus/NexusTopicProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/nexus/NexusClient$NexusThreadFactory;
    }
.end annotation


# instance fields
.field private backgroundTaskExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final client:Lio/intercom/okhttp3/OkHttpClient;

.field private final eventPropagator:Lio/intercom/android/sdk/nexus/NexusEventPropagator;

.field private future:Ljava/util/concurrent/ScheduledFuture;

.field private presenceInterval:J

.field private final sockets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/nexus/NexusSocket;",
            ">;"
        }
    .end annotation
.end field

.field private final topics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final twig:Lio/intercom/android/sdk/twig/Twig;


# direct methods
.method public constructor <init>(Lio/intercom/android/sdk/twig/Twig;)V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lio/intercom/android/sdk/nexus/NexusEventPropagator;

    invoke-direct {v0, p1}, Lio/intercom/android/sdk/nexus/NexusEventPropagator;-><init>(Lio/intercom/android/sdk/twig/Twig;)V

    invoke-direct {p0, p1, v0}, Lio/intercom/android/sdk/nexus/NexusClient;-><init>(Lio/intercom/android/sdk/twig/Twig;Lio/intercom/android/sdk/nexus/NexusEventPropagator;)V

    .line 31
    return-void
.end method

.method constructor <init>(Lio/intercom/android/sdk/twig/Twig;Lio/intercom/android/sdk/nexus/NexusEventPropagator;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x82

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->sockets:Ljava/util/List;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    .line 34
    iput-object p1, p0, Lio/intercom/android/sdk/nexus/NexusClient;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 35
    iput-object p2, p0, Lio/intercom/android/sdk/nexus/NexusClient;->eventPropagator:Lio/intercom/android/sdk/nexus/NexusEventPropagator;

    .line 38
    new-instance v0, Lio/intercom/okhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lio/intercom/okhttp3/OkHttpClient$Builder;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 39
    invoke-virtual {v0, v2, v3, v1}, Lio/intercom/okhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lio/intercom/okhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 40
    invoke-virtual {v0, v2, v3, v1}, Lio/intercom/okhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lio/intercom/okhttp3/OkHttpClient$Builder;

    move-result-object v0

    const-wide/16 v2, 0x14

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 41
    invoke-virtual {v0, v2, v3, v1}, Lio/intercom/okhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lio/intercom/okhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lio/intercom/okhttp3/OkHttpClient$Builder;->build()Lio/intercom/okhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->client:Lio/intercom/okhttp3/OkHttpClient;

    .line 43
    return-void
.end method

.method static synthetic access$100(Lio/intercom/android/sdk/nexus/NexusClient;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lio/intercom/android/sdk/nexus/NexusClient;->schedulePresence()V

    return-void
.end method

.method private schedulePresence()V
    .locals 5

    .prologue
    .line 179
    iget-wide v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->presenceInterval:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 180
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->backgroundTaskExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lio/intercom/android/sdk/nexus/NexusClient$1;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/nexus/NexusClient$1;-><init>(Lio/intercom/android/sdk/nexus/NexusClient;)V

    iget-wide v2, p0, Lio/intercom/android/sdk/nexus/NexusClient;->presenceInterval:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->future:Ljava/util/concurrent/ScheduledFuture;

    .line 187
    :cond_0
    return-void
.end method

.method private subscribeToTopics(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    invoke-static {p1}, Lio/intercom/android/sdk/nexus/NexusEvent;->getSubscribeEvent(Ljava/util/List;)Lio/intercom/android/sdk/nexus/NexusEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/nexus/NexusClient;->fire(Lio/intercom/android/sdk/nexus/NexusEvent;)V

    .line 170
    :cond_0
    return-void
.end method

.method private unSubscribeFromTopics(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    invoke-static {p1}, Lio/intercom/android/sdk/nexus/NexusEvent;->getUnsubscribeEvent(Ljava/util/List;)Lio/intercom/android/sdk/nexus/NexusEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/nexus/NexusClient;->fire(Lio/intercom/android/sdk/nexus/NexusEvent;)V

    .line 176
    :cond_0
    return-void
.end method


# virtual methods
.method public addEventListener(Lio/intercom/android/sdk/nexus/NexusListener;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->eventPropagator:Lio/intercom/android/sdk/nexus/NexusEventPropagator;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/nexus/NexusEventPropagator;->addListener(Lio/intercom/android/sdk/nexus/NexusListener;)V

    .line 119
    return-void
.end method

.method public declared-synchronized addTopics(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 145
    iget-object v1, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 146
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/nexus/NexusClient;->subscribeToTopics(Ljava/util/List;)V

    .line 147
    iget-object v1, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    monitor-exit p0

    return-void

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearTopics()V
    .locals 1

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/nexus/NexusClient;->unSubscribeFromTopics(Ljava/util/List;)V

    .line 163
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    monitor-exit p0

    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public connect(Lio/intercom/android/sdk/nexus/NexusConfig;Z)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 46
    invoke-virtual {p1}, Lio/intercom/android/sdk/nexus/NexusConfig;->getEndpoints()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "No endpoints present"

    new-array v2, v10, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->backgroundTaskExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_2

    .line 53
    new-instance v0, Lio/intercom/android/sdk/nexus/NexusClient$NexusThreadFactory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lio/intercom/android/sdk/nexus/NexusClient$NexusThreadFactory;-><init>(Lio/intercom/android/sdk/nexus/NexusClient$1;)V

    .line 54
    invoke-virtual {p1}, Lio/intercom/android/sdk/nexus/NexusConfig;->getEndpoints()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->backgroundTaskExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 57
    :cond_2
    invoke-virtual {p1}, Lio/intercom/android/sdk/nexus/NexusConfig;->getEndpoints()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v2, "Adding socket"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    new-instance v0, Lio/intercom/android/sdk/nexus/NexusSocket;

    .line 61
    invoke-virtual {p1}, Lio/intercom/android/sdk/nexus/NexusConfig;->getConnectionTimeout()I

    move-result v2

    iget-object v4, p0, Lio/intercom/android/sdk/nexus/NexusClient;->twig:Lio/intercom/android/sdk/twig/Twig;

    iget-object v5, p0, Lio/intercom/android/sdk/nexus/NexusClient;->backgroundTaskExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v6, p0, Lio/intercom/android/sdk/nexus/NexusClient;->client:Lio/intercom/okhttp3/OkHttpClient;

    iget-object v7, p0, Lio/intercom/android/sdk/nexus/NexusClient;->eventPropagator:Lio/intercom/android/sdk/nexus/NexusEventPropagator;

    move v3, p2

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lio/intercom/android/sdk/nexus/NexusSocket;-><init>(Ljava/lang/String;IZLio/intercom/android/sdk/twig/Twig;Ljava/util/concurrent/ScheduledExecutorService;Lio/intercom/okhttp3/OkHttpClient;Lio/intercom/android/sdk/nexus/NexusListener;Lio/intercom/android/sdk/nexus/NexusTopicProvider;)V

    .line 68
    invoke-virtual {v0}, Lio/intercom/android/sdk/nexus/NexusSocket;->connect()V

    .line 69
    iget-object v1, p0, Lio/intercom/android/sdk/nexus/NexusClient;->sockets:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 72
    :cond_3
    invoke-virtual {p1}, Lio/intercom/android/sdk/nexus/NexusConfig;->getPresenceHeartbeatInterval()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->presenceInterval:J

    .line 74
    if-eqz p2, :cond_0

    .line 75
    invoke-direct {p0}, Lio/intercom/android/sdk/nexus/NexusClient;->schedulePresence()V

    goto :goto_0
.end method

.method public declared-synchronized disconnect()V
    .locals 5

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->sockets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->sockets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/nexus/NexusSocket;

    .line 82
    iget-object v2, p0, Lio/intercom/android/sdk/nexus/NexusClient;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v3, "disconnecting socket"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    invoke-virtual {v0}, Lio/intercom/android/sdk/nexus/NexusSocket;->disconnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85
    :cond_0
    :try_start_1
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->sockets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 86
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "client disconnected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->future:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    .line 90
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->future:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized fire(Lio/intercom/android/sdk/nexus/NexusEvent;)V
    .locals 3

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->eventPropagator:Lio/intercom/android/sdk/nexus/NexusEventPropagator;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/nexus/NexusEventPropagator;->cacheEvent(Lio/intercom/android/sdk/nexus/NexusEvent;)V

    .line 96
    invoke-virtual {p1}, Lio/intercom/android/sdk/nexus/NexusEvent;->toStringEncodedJsonObject()Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->sockets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/nexus/NexusSocket;

    .line 99
    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/nexus/NexusSocket;->fire(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 102
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized getTopics()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isConnected()Z
    .locals 2

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->sockets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/nexus/NexusSocket;

    .line 110
    invoke-virtual {v0}, Lio/intercom/android/sdk/nexus/NexusSocket;->isConnected()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x1

    .line 114
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized localUpdate(Lio/intercom/android/sdk/nexus/NexusEvent;)V
    .locals 1

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->eventPropagator:Lio/intercom/android/sdk/nexus/NexusEventPropagator;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/nexus/NexusEventPropagator;->notifyEvent(Lio/intercom/android/sdk/nexus/NexusEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit p0

    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public removeEventListener(Lio/intercom/android/sdk/nexus/NexusListener;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->eventPropagator:Lio/intercom/android/sdk/nexus/NexusEventPropagator;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/nexus/NexusEventPropagator;->removeListener(Lio/intercom/android/sdk/nexus/NexusListener;)V

    .line 123
    return-void
.end method

.method public declared-synchronized removeTopics(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 152
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 153
    iget-object v3, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 154
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 157
    :cond_1
    :try_start_1
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/nexus/NexusClient;->unSubscribeFromTopics(Ljava/util/List;)V

    .line 158
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setTopics(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 131
    iget-object v1, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 133
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 134
    invoke-interface {v1, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 136
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/nexus/NexusClient;->subscribeToTopics(Ljava/util/List;)V

    .line 137
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/nexus/NexusClient;->unSubscribeFromTopics(Ljava/util/List;)V

    .line 139
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 140
    iget-object v0, p0, Lio/intercom/android/sdk/nexus/NexusClient;->topics:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    monitor-exit p0

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
