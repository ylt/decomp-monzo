.class public Lio/intercom/android/sdk/utilities/BlockUtils;
.super Ljava/lang/Object;
.source "BlockUtils.java"


# static fields
.field private static final DEFAULT_MARGIN_BOTTOM_DP:I = 0xa

.field private static final LARGE_LINE_SPACING_DP:I = 0x4

.field private static final SMALL_LINE_SPACING_DP:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createLayoutParams(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 21
    return-void
.end method

.method public static getBlockView(Landroid/view/ViewGroup;Landroid/widget/LinearLayout;Landroid/content/Context;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 58
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 60
    sget v1, Lio/intercom/android/sdk/R$layout;->intercom_container_layout:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 62
    sget v0, Lio/intercom/android/sdk/R$id;->cellLayout:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 63
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 64
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 67
    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 68
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 71
    :cond_1
    invoke-virtual {v0, p1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 73
    invoke-virtual {v2, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 75
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 77
    return-object v2
.end method

.method public static setDefaultMarginBottom(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lio/intercom/android/sdk/utilities/BlockUtils;->setMarginBottom(Landroid/view/View;I)V

    .line 25
    return-void
.end method

.method public static setLargeLineSpacing(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lio/intercom/android/sdk/utilities/BlockUtils;->setLineSpacing(Landroid/widget/TextView;I)V

    .line 51
    return-void
.end method

.method public static setLayoutMarginsAndGravity(Landroid/view/View;IZ)V
    .locals 5

    .prologue
    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 39
    if-eqz p2, :cond_0

    .line 40
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 42
    :cond_0
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 43
    return-void
.end method

.method private static setLineSpacing(Landroid/widget/TextView;I)V
    .locals 2

    .prologue
    .line 54
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 55
    return-void
.end method

.method public static setMarginBottom(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 29
    int-to-float v1, p1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 30
    return-void
.end method

.method public static setMarginLeft(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 34
    int-to-float v1, p1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 35
    return-void
.end method

.method public static setSmallLineSpacing(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lio/intercom/android/sdk/utilities/BlockUtils;->setLineSpacing(Landroid/widget/TextView;I)V

    .line 47
    return-void
.end method
