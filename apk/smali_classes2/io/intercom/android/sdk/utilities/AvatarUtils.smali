.class public Lio/intercom/android/sdk/utilities/AvatarUtils;
.super Ljava/lang/Object;
.source "AvatarUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAvatar(Lio/intercom/android/sdk/models/Avatar;Landroid/widget/ImageView;ILio/intercom/android/sdk/identity/AppConfig;Lio/intercom/com/bumptech/glide/i;)V
    .locals 3

    .prologue
    .line 50
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    invoke-virtual {p0}, Lio/intercom/android/sdk/models/Avatar;->getInitials()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53
    invoke-static {v0, p3}, Lio/intercom/android/sdk/utilities/AvatarUtils;->getDefaultDrawable(Landroid/content/Context;Lio/intercom/android/sdk/identity/AppConfig;)Lio/intercom/android/sdk/views/AvatarDefaultDrawable;

    move-result-object v0

    .line 56
    :goto_0
    new-instance v1, Lio/intercom/com/bumptech/glide/f/f;

    invoke-direct {v1}, Lio/intercom/com/bumptech/glide/f/f;-><init>()V

    .line 57
    invoke-virtual {v1, v0}, Lio/intercom/com/bumptech/glide/f/f;->a(Landroid/graphics/drawable/Drawable;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v1

    .line 58
    invoke-virtual {v1, v0}, Lio/intercom/com/bumptech/glide/f/f;->b(Landroid/graphics/drawable/Drawable;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    .line 59
    invoke-virtual {p0}, Lio/intercom/android/sdk/models/Avatar;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lio/intercom/android/sdk/utilities/ImageUtils;->getDiskCacheStrategy(Ljava/lang/String;)Lio/intercom/com/bumptech/glide/load/engine/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->b(Lio/intercom/com/bumptech/glide/load/engine/h;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    new-instance v1, Lio/intercom/android/sdk/transforms/RoundTransform;

    invoke-direct {v1}, Lio/intercom/android/sdk/transforms/RoundTransform;-><init>()V

    .line 60
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->a(Lio/intercom/com/bumptech/glide/load/l;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    .line 61
    if-lez p2, :cond_0

    .line 62
    invoke-virtual {v0, p2, p2}, Lio/intercom/com/bumptech/glide/f/f;->a(II)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    .line 66
    :cond_0
    invoke-virtual {p0}, Lio/intercom/android/sdk/models/Avatar;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lio/intercom/com/bumptech/glide/i;->a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v1

    .line 67
    invoke-static {}, Lio/intercom/com/bumptech/glide/load/resource/b/b;->c()Lio/intercom/com/bumptech/glide/load/resource/b/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/j;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v1

    .line 68
    invoke-virtual {v1, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    .line 69
    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/h;->a(Landroid/widget/ImageView;)Lio/intercom/com/bumptech/glide/f/a/h;

    .line 70
    return-void

    .line 54
    :cond_1
    invoke-virtual {p0}, Lio/intercom/android/sdk/models/Avatar;->getInitials()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lio/intercom/android/sdk/utilities/AvatarUtils;->getInitialsDrawable(Ljava/lang/String;Lio/intercom/android/sdk/identity/AppConfig;)Lio/intercom/android/sdk/views/AvatarInitialsDrawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDefaultDrawable(Landroid/content/Context;Lio/intercom/android/sdk/identity/AppConfig;)Lio/intercom/android/sdk/views/AvatarDefaultDrawable;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lio/intercom/android/sdk/views/AvatarDefaultDrawable;

    invoke-virtual {p1}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColorDark()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lio/intercom/android/sdk/views/AvatarDefaultDrawable;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public static getInitialsDrawable(Ljava/lang/String;Lio/intercom/android/sdk/identity/AppConfig;)Lio/intercom/android/sdk/views/AvatarInitialsDrawable;
    .locals 3

    .prologue
    .line 73
    new-instance v0, Lio/intercom/android/sdk/views/AvatarInitialsDrawable;

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColorDark()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lio/intercom/android/sdk/views/AvatarInitialsDrawable;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static loadAvatarIntoView(Lio/intercom/android/sdk/models/Avatar;Landroid/widget/ImageView;Lio/intercom/android/sdk/identity/AppConfig;Lio/intercom/com/bumptech/glide/i;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2, p3}, Lio/intercom/android/sdk/utilities/AvatarUtils;->createAvatar(Lio/intercom/android/sdk/models/Avatar;Landroid/widget/ImageView;ILio/intercom/android/sdk/identity/AppConfig;Lio/intercom/com/bumptech/glide/i;)V

    .line 46
    return-void
.end method

.method public static preloadAvatar(Lio/intercom/android/sdk/models/Avatar;Ljava/lang/Runnable;Lio/intercom/com/bumptech/glide/i;)V
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p0}, Lio/intercom/android/sdk/models/Avatar;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 41
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-virtual {p2}, Lio/intercom/com/bumptech/glide/i;->e()Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    .line 30
    invoke-virtual {p0}, Lio/intercom/android/sdk/models/Avatar;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Ljava/lang/String;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    new-instance v1, Lio/intercom/android/sdk/utilities/AvatarUtils$1;

    invoke-direct {v1, p1}, Lio/intercom/android/sdk/utilities/AvatarUtils$1;-><init>(Ljava/lang/Runnable;)V

    .line 31
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;)Lio/intercom/com/bumptech/glide/f/a/h;

    goto :goto_0
.end method
