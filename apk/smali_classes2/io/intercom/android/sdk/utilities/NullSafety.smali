.class public Lio/intercom/android/sdk/utilities/NullSafety;
.super Ljava/lang/Object;
.source "NullSafety.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static valueOrDefault(Ljava/lang/Boolean;Z)Z
    .locals 0

    .prologue
    .line 11
    if-nez p0, :cond_0

    :goto_0
    return p1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0
.end method

.method public static valueOrEmpty(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 7
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method
