.class public Lio/intercom/android/sdk/views/StatusBarThemer;
.super Ljava/lang/Object;
.source "StatusBarThemer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static setStatusBarColor(ILandroid/view/Window;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 20
    const/16 v0, 0x15

    if-lt p2, v0, :cond_0

    .line 21
    const/high16 v0, -0x80000000

    invoke-virtual {p1, v0}, Landroid/view/Window;->addFlags(I)V

    .line 22
    invoke-virtual {p1, p0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 24
    :cond_0
    return-void
.end method

.method public static setStatusBarColor(Landroid/view/Window;Lio/intercom/android/sdk/identity/AppConfig;)V
    .locals 2

    .prologue
    .line 15
    invoke-virtual {p1}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColorDark()I

    move-result v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0, p0, v1}, Lio/intercom/android/sdk/views/StatusBarThemer;->setStatusBarColor(ILandroid/view/Window;I)V

    .line 16
    return-void
.end method
