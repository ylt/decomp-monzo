.class public Lio/intercom/android/sdk/views/holder/ConversationLoadingViewHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "ConversationLoadingViewHolder.java"


# direct methods
.method public constructor <init>(Landroid/view/View;Lio/intercom/android/sdk/identity/AppConfig;)V
    .locals 3

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 16
    sget v0, Lio/intercom/android/sdk/R$id;->progressBar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 17
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 18
    return-void
.end method
