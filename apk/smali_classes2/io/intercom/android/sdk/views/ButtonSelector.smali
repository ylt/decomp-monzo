.class public Lio/intercom/android/sdk/views/ButtonSelector;
.super Landroid/graphics/drawable/StateListDrawable;
.source "ButtonSelector.java"


# instance fields
.field private final color:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    invoke-direct {p0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 13
    iput p3, p0, Lio/intercom/android/sdk/views/ButtonSelector;->color:I

    .line 14
    new-array v0, v3, [I

    const v1, 0x101009e

    aput v1, v0, v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lio/intercom/android/sdk/views/ButtonSelector;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 15
    new-array v0, v3, [I

    const v1, 0x101009c

    aput v1, v0, v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lio/intercom/android/sdk/views/ButtonSelector;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 16
    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lio/intercom/android/sdk/views/ButtonSelector;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 17
    return-void
.end method

.method private static darken(ID)I
    .locals 5

    .prologue
    .line 39
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, p1

    double-to-int v0, v0

    .line 40
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-double v2, v1

    mul-double/2addr v2, p1

    double-to-int v1, v2

    .line 41
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, p1

    double-to-int v2, v2

    .line 43
    const/16 v3, 0xff

    invoke-static {v3, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected onStateChange([I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 21
    .line 22
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget v3, p1, v1

    .line 23
    const v4, 0x10100a7

    if-eq v3, v4, :cond_0

    const v4, 0x101009c

    if-ne v3, v4, :cond_1

    .line 25
    :cond_0
    const/4 v0, 0x1

    .line 22
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 29
    :cond_2
    if-eqz v0, :cond_3

    .line 30
    iget v0, p0, Lio/intercom/android/sdk/views/ButtonSelector;->color:I

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    invoke-static {v0, v2, v3}, Lio/intercom/android/sdk/views/ButtonSelector;->darken(ID)I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v0, v1}, Lio/intercom/android/sdk/views/ButtonSelector;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 35
    :goto_1
    invoke-super {p0, p1}, Landroid/graphics/drawable/StateListDrawable;->onStateChange([I)Z

    move-result v0

    return v0

    .line 32
    :cond_3
    iget v0, p0, Lio/intercom/android/sdk/views/ButtonSelector;->color:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v0, v1}, Lio/intercom/android/sdk/views/ButtonSelector;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_1
.end method
