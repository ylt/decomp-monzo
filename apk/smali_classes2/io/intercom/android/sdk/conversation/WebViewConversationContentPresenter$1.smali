.class Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter$1;
.super Ljava/lang/Object;
.source "WebViewConversationContentPresenter.java"

# interfaces
.implements Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->setup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollChange(Landroid/webkit/WebView;IIII)V
    .locals 2

    .prologue
    .line 123
    if-lez p3, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->access$000(Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;)Lio/intercom/android/sdk/profile/ProfilePresenter;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->access$000(Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;)Lio/intercom/android/sdk/profile/ProfilePresenter;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->closeProfile()V

    .line 126
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->access$100(Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;)Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;->updateIntercomLink(Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;)V

    .line 127
    return-void
.end method
