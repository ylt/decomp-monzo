.class Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;
.super Ljava/lang/Object;
.source "ProfileExpansionLogic.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static hasNoUserReplies(Lio/intercom/android/sdk/models/Conversation;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    .line 26
    invoke-virtual {p0}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 27
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->isAdmin()Z

    move-result v5

    if-eqz v5, :cond_1

    move v1, v3

    .line 30
    :cond_1
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->isAdmin()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    :goto_0
    return v2

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private static hasOnlyUserParts(Lio/intercom/android/sdk/models/Conversation;)Z
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 15
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->isAdmin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x0

    .line 19
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method shouldExpandProfile(Lio/intercom/android/sdk/models/Conversation;)Z
    .locals 1

    .prologue
    .line 9
    invoke-static {p1}, Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;->hasOnlyUserParts(Lio/intercom/android/sdk/models/Conversation;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;->hasNoUserReplies(Lio/intercom/android/sdk/models/Conversation;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
