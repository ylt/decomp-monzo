.class Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;
.super Ljava/lang/Object;
.source "WebViewConversationContentPresenter.java"

# interfaces
.implements Lio/intercom/android/sdk/conversation/ConversationContentPresenter;
.implements Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;


# static fields
.field private static final DIRECTION_DOWN:I = 0x1

.field private static final WEB_PAGE_URL:Ljava/lang/String; = "https://js.intercomcdn.com/mobile.html"


# instance fields
.field private final appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final appIdentity:Lio/intercom/android/sdk/identity/AppIdentity;

.field private final bus:Lio/intercom/com/a/a/b;

.field private fetchedConversation:Z

.field private final gson:Lio/intercom/com/google/gson/e;

.field private final host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

.field private hostWrapper:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

.field private final intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

.field private final jsRunner:Lio/intercom/android/sdk/conversation/JavascriptRunner;

.field private final loadingView:Landroid/view/View;

.field private final opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

.field private final parts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;"
        }
    .end annotation
.end field

.field private final profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

.field private final store:Lio/intercom/android/sdk/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

.field private final webView:Lio/intercom/android/sdk/conversation/ObservableWebView;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/ObservableWebView;Landroid/view/View;Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;Lio/intercom/android/sdk/identity/AppIdentity;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/Provider;Ljava/util/List;Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Lio/intercom/com/google/gson/e;Lio/intercom/android/sdk/conversation/JavascriptRunner;Lio/intercom/android/sdk/store/Store;Lio/intercom/com/a/a/b;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;Lio/intercom/android/sdk/profile/ProfilePresenter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/conversation/ObservableWebView;",
            "Landroid/view/View;",
            "Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;",
            "Lio/intercom/android/sdk/identity/AppIdentity;",
            "Lio/intercom/android/sdk/identity/UserIdentity;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;",
            "Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;",
            "Lio/intercom/com/google/gson/e;",
            "Lio/intercom/android/sdk/conversation/JavascriptRunner;",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;",
            "Lio/intercom/com/a/a/b;",
            "Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;",
            "Lio/intercom/android/sdk/profile/ProfilePresenter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->fetchedConversation:Z

    .line 97
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    .line 98
    iput-object p4, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->appIdentity:Lio/intercom/android/sdk/identity/AppIdentity;

    .line 99
    iput-object p5, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 100
    iput-object p6, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 101
    iput-object p8, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    .line 102
    iput-object p9, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->gson:Lio/intercom/com/google/gson/e;

    .line 103
    iput-object p10, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->jsRunner:Lio/intercom/android/sdk/conversation/JavascriptRunner;

    .line 104
    iput-object p2, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->loadingView:Landroid/view/View;

    .line 105
    iput-object p11, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->store:Lio/intercom/android/sdk/store/Store;

    .line 106
    iput-object p12, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->bus:Lio/intercom/com/a/a/b;

    .line 107
    iput-object p3, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    .line 108
    iput-object p7, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->parts:Ljava/util/List;

    .line 109
    iput-object p13, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    .line 110
    iput-object p14, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    .line 111
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;)Lio/intercom/android/sdk/profile/ProfilePresenter;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    return-object v0
.end method

.method static synthetic access$100(Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;)Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    return-object v0
.end method

.method static create(Lio/intercom/android/sdk/conversation/ObservableWebView;Landroid/view/View;Lio/intercom/android/sdk/views/IntercomLinkView;Lio/intercom/android/sdk/identity/AppIdentity;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/api/Api;Ljava/util/List;Ljava/lang/String;Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Lio/intercom/com/google/gson/e;Lio/intercom/android/sdk/conversation/JavascriptRunner;Lio/intercom/android/sdk/store/Store;Lio/intercom/com/a/a/b;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;Lio/intercom/android/sdk/profile/ProfilePresenter;)Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/conversation/ObservableWebView;",
            "Landroid/view/View;",
            "Lio/intercom/android/sdk/views/IntercomLinkView;",
            "Lio/intercom/android/sdk/identity/AppIdentity;",
            "Lio/intercom/android/sdk/identity/UserIdentity;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Lio/intercom/android/sdk/api/Api;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;",
            "Ljava/lang/String;",
            "Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;",
            "Lio/intercom/com/google/gson/e;",
            "Lio/intercom/android/sdk/conversation/JavascriptRunner;",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;",
            "Lio/intercom/com/a/a/b;",
            "Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;",
            "Lio/intercom/android/sdk/profile/ProfilePresenter;",
            ")",
            "Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    invoke-direct/range {v0 .. v5}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;-><init>(Lio/intercom/android/sdk/views/IntercomLinkView;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/api/Api;Ljava/util/List;Ljava/lang/String;)V

    .line 78
    new-instance v1, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object v4, v0

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    invoke-direct/range {v1 .. v15}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;-><init>(Lio/intercom/android/sdk/conversation/ObservableWebView;Landroid/view/View;Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;Lio/intercom/android/sdk/identity/AppIdentity;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/Provider;Ljava/util/List;Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Lio/intercom/com/google/gson/e;Lio/intercom/android/sdk/conversation/JavascriptRunner;Lio/intercom/android/sdk/store/Store;Lio/intercom/com/a/a/b;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;Lio/intercom/android/sdk/profile/ProfilePresenter;)V

    return-object v1
.end method

.method private loadBundle()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->jsRunner:Lio/intercom/android/sdk/conversation/JavascriptRunner;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/JavascriptRunner;->reset()V

    .line 148
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->jsRunner:Lio/intercom/android/sdk/conversation/JavascriptRunner;

    const-string v1, "conversationApp.sendAction = function(action) {AndroidHost.handleAction(JSON.stringify(action));};"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/JavascriptRunner;->run(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    const-string v1, "https://js.intercomcdn.com/mobile.html"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ObservableWebView;->loadUrl(Ljava/lang/String;)V

    .line 152
    return-void
.end method


# virtual methods
.method public addBottomPadding(I)V
    .locals 0

    .prologue
    .line 267
    return-void
.end method

.method public cleanup()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->jsRunner:Lio/intercom/android/sdk/conversation/JavascriptRunner;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/JavascriptRunner;->clearPendingScripts()V

    .line 263
    return-void
.end method

.method public fetchConversation(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 222
    iget-boolean v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->fetchedConversation:Z

    if-eqz v0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 225
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->fetchedConversation:Z

    .line 226
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->loadBundle()V

    .line 227
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->appIdentity:Lio/intercom/android/sdk/identity/AppIdentity;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-static {v0, v1, v2, p1}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->setConversation(Lio/intercom/android/sdk/identity/AppIdentity;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/Provider;Ljava/lang/String;)Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->sendWebViewAction(Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V

    goto :goto_0
.end method

.method public isAtBottom()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 173
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/conversation/ObservableWebView;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAdminStartedTyping(Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;)V
    .locals 3

    .prologue
    .line 213
    .line 214
    invoke-virtual {p1}, Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;->getAdminId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;->getAdminName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;->getAdminAvatarUrl()Ljava/lang/String;

    move-result-object v2

    .line 213
    invoke-static {v0, v1, v2}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->adminIsTyping(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->sendWebViewAction(Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V

    .line 215
    return-void
.end method

.method public onConversationFetched(Lio/intercom/android/sdk/models/events/ConversationEvent;)V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method public onGlobalLayout()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    invoke-virtual {v0, p0}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;->updateIntercomLink(Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;)V

    .line 190
    return-void
.end method

.method public onNewCommentEventReceived(Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;)V
    .locals 1

    .prologue
    .line 218
    invoke-static {}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->partCreated()Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->sendWebViewAction(Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V

    .line 219
    return-void
.end method

.method public onNewConversation(Lio/intercom/android/sdk/models/events/NewConversationEvent;)V
    .locals 0

    .prologue
    .line 251
    return-void
.end method

.method public onNewPartReceived()V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public onPartClicked(Lio/intercom/android/sdk/models/Part;)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public onProfileScrolled()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->scrollToTop()V

    .line 185
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    invoke-virtual {v0, p0}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;->updateIntercomLink(Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;)V

    .line 186
    return-void
.end method

.method public onReplyDelivered(Lio/intercom/android/sdk/models/events/ReplyEvent;)V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public scrollToBottom()V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    const/4 v1, 0x0

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    invoke-virtual {v2}, Lio/intercom/android/sdk/conversation/ObservableWebView;->getContentHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    invoke-virtual {v3}, Lio/intercom/android/sdk/conversation/ObservableWebView;->getScale()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/conversation/ObservableWebView;->scrollTo(II)V

    .line 170
    return-void
.end method

.method public scrollToTop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    invoke-virtual {v0, v1, v1}, Lio/intercom/android/sdk/conversation/ObservableWebView;->scrollTo(II)V

    .line 166
    return-void
.end method

.method public sendPart(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    invoke-static {p1}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->createPart(Ljava/util/List;)Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->sendWebViewAction(Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V

    .line 237
    return-void
.end method

.method sendWebViewAction(Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V
    .locals 4

    .prologue
    .line 155
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->gson:Lio/intercom/com/google/gson/e;

    invoke-virtual {v0, p1}, Lio/intercom/com/google/gson/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->jsRunner:Lio/intercom/android/sdk/conversation/JavascriptRunner;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "conversationApp.handleAction("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/conversation/JavascriptRunner;->run(Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method public setup()V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    new-instance v1, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter$1;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter$1;-><init>(Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;)V

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ObservableWebView;->setOnScrollChangeListener(Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;)V

    .line 130
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    .line 131
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->loadingView:Landroid/view/View;

    sget v2, Lio/intercom/android/sdk/R$id;->progressBar:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 132
    if-eqz v1, :cond_0

    .line 133
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 136
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ObservableWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 137
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    new-instance v1, Lio/intercom/android/sdk/conversation/ConversationWebViewClient;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->jsRunner:Lio/intercom/android/sdk/conversation/JavascriptRunner;

    const-string v3, "https://js.intercomcdn.com/mobile.html"

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->bus:Lio/intercom/com/a/a/b;

    invoke-direct {v1, v2, v3, v4}, Lio/intercom/android/sdk/conversation/ConversationWebViewClient;-><init>(Lio/intercom/android/sdk/conversation/JavascriptRunner;Ljava/lang/String;Lio/intercom/com/a/a/b;)V

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ObservableWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 139
    new-instance v0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->gson:Lio/intercom/com/google/gson/e;

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->store:Lio/intercom/android/sdk/store/Store;

    iget-object v5, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->parts:Ljava/util/List;

    iget-object v6, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    invoke-direct/range {v0 .. v6}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;-><init>(Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Landroid/webkit/WebView;Lio/intercom/com/google/gson/e;Lio/intercom/android/sdk/store/Store;Ljava/util/List;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;)V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->hostWrapper:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    .line 140
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->hostWrapper:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    const-string v2, "AndroidHost"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/conversation/ObservableWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ObservableWebView;->setBackgroundColor(I)V

    .line 143
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    invoke-virtual {v0, p0}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;->setup(Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;)V

    .line 144
    return-void
.end method

.method public showContentView()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ObservableWebView;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->loadingView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 210
    return-void
.end method

.method public showErrorView()V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->loadingView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ObservableWebView;->setVisibility(I)V

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->fetchedConversation:Z

    .line 200
    return-void
.end method

.method public showLoadingView()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->loadingView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->webView:Lio/intercom/android/sdk/conversation/ObservableWebView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ObservableWebView;->setVisibility(I)V

    .line 205
    return-void
.end method

.method public smoothScrollToTop()V
    .locals 0

    .prologue
    .line 161
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->scrollToTop()V

    .line 162
    return-void
.end method

.method public uploadImage(Ljava/util/List;Lcom/intercom/input/gallery/c;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;",
            "Lcom/intercom/input/gallery/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 240
    .line 241
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->b()Ljava/lang/String;

    move-result-object v0

    .line 242
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->a()Ljava/lang/String;

    move-result-object v1

    .line 243
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->c()Ljava/lang/String;

    move-result-object v2

    .line 244
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->f()I

    move-result v3

    .line 245
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->g()I

    move-result v4

    .line 246
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->i()Z

    move-result v5

    .line 240
    invoke-static/range {v0 .. v5}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->createUpload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->sendWebViewAction(Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V

    .line 247
    return-void
.end method
