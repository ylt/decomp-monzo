.class Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$8;
.super Ljava/lang/Object;
.source "NativeConversationContentPresenter.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->updateSendPartOpsMetric()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V
    .locals 0

    .prologue
    .line 656
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$8;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 658
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$8;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->access$500(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    move-result-object v0

    const-string v1, "finish"

    const-string v2, "time-to-render-result-send-part-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$8;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->access$600(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    invoke-static {v0, p0}, Lio/intercom/android/sdk/utilities/ViewUtils;->removeGlobalLayoutListener(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 660
    return-void
.end method
