.class Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$10;
.super Ljava/lang/Object;
.source "NativeConversationContentPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V
    .locals 0

    .prologue
    .line 714
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$10;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 716
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$10;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->access$400(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 717
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$10;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->access$600(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    sget-wide v2, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->TIMESTAMP_UPDATE_PERIOD:J

    invoke-virtual {v0, p0, v2, v3}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 718
    return-void
.end method
