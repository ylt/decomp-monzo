.class Lio/intercom/android/sdk/conversation/WebViewHostWrapper$3;
.super Ljava/lang/Object;
.source "WebViewHostWrapper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->handleAction(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

.field final synthetic val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$3;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    iput-object p2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$3;->val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$3;->val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->getValue()Ljava/util/Map;

    move-result-object v0

    const-string v1, "articleId"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 90
    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$3;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    invoke-static {v1}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$200(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 92
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$3;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    .line 93
    invoke-static {v2}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$000(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    move-result-object v2

    invoke-interface {v2}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;->getConversationId()Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-static {v1, v0, v2}, Lio/intercom/android/sdk/activities/IntercomArticleActivity;->buildIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 95
    :cond_0
    return-void
.end method
