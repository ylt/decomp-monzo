.class public Lio/intercom/android/sdk/conversation/ConversationFragment;
.super Landroid/support/v4/app/Fragment;
.source "ConversationFragment.java"

# interfaces
.implements Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;
.implements Lio/intercom/android/sdk/conversation/ConversationPartAdapter$Listener;
.implements Lio/intercom/android/sdk/store/Store$Subscriber2;
.implements Lio/intercom/android/sdk/views/IntercomToolbar$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;",
        "Lio/intercom/android/sdk/conversation/ConversationPartAdapter$Listener;",
        "Lio/intercom/android/sdk/store/Store$Subscriber2",
        "<",
        "Ljava/lang/Integer;",
        "Lio/intercom/android/sdk/models/TeamPresence;",
        ">;",
        "Lio/intercom/android/sdk/views/IntercomToolbar$Listener;"
    }
.end annotation


# static fields
.field private static final ARG_CONVERSATION_ID:Ljava/lang/String; = "conversationId"

.field private static final ARG_GROUP_PARTICIPANTS:Ljava/lang/String; = "group_participants"

.field private static final ARG_INITIAL_MESSAGE:Ljava/lang/String; = "initial_message"

.field private static final ARG_IS_READ:Ljava/lang/String; = "intercomsdk-isRead"

.field private static final ARG_IS_TWO_PANE:Ljava/lang/String; = "is_two_pane"

.field private static final ARG_LAST_PARTICIPANT:Ljava/lang/String; = "last_participant"

.field private static final COMPOSER_LIGHTBOX_REQUEST_CODE:I = 0x19

.field private static final EXTRA_GALLERY_IMAGE:Ljava/lang/String; = "gallery_image"

.field private static final IMAGE_MIME_TYPE:Ljava/lang/String; = "image"

.field private static final MAX_FILE_SIZE_BYTES:I = 0x2800000


# instance fields
.field appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private bus:Lio/intercom/com/a/a/b;

.field private canOpenProfile:Z

.field private composerHolder:Landroid/widget/FrameLayout;

.field private final composerListener:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter$Listener;

.field composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

.field private final connectivityEventListener:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor$ConnectivityEventListener;

.field contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

.field conversation:Lio/intercom/android/sdk/models/Conversation;

.field conversationId:Ljava/lang/String;

.field private currentOrientation:I

.field private final globalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private groupParticipants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Participant;",
            ">;"
        }
    .end annotation
.end field

.field private hasLoadedConversation:Z

.field private initialMessage:Ljava/lang/String;

.field private intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

.field private intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

.field private isTwoPane:Z

.field private lastActiveTime:Ljava/lang/CharSequence;

.field private lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

.field private listener:Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;

.field private metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

.field final networkConnectivityMonitor:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;

.field private nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

.field private opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

.field orientationChanged:Z

.field private final profileExpansionLogic:Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;

.field profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

.field private reactionComposer:Lio/intercom/android/sdk/conversation/ReactionInputView;

.field private requestManager:Lio/intercom/com/bumptech/glide/i;

.field rootView:Landroid/view/View;

.field shouldStayAtBottom:Z

.field soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

.field private store:Lio/intercom/android/sdk/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;"
        }
    .end annotation
.end field

.field private subscription:Lio/intercom/android/sdk/store/Store$Subscription;

.field private final twig:Lio/intercom/android/sdk/twig/Twig;

.field private userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

.field private wallpaperLoader:Lio/intercom/android/sdk/imageloader/WallpaperLoader;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 112
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 113
    new-instance v0, Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;

    invoke-direct {v0}, Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->networkConnectivityMonitor:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;

    .line 114
    new-instance v0, Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;

    invoke-direct {v0}, Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profileExpansionLogic:Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->canOpenProfile:Z

    .line 141
    iput-boolean v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->hasLoadedConversation:Z

    .line 143
    iput-boolean v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->orientationChanged:Z

    .line 146
    iput-boolean v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->shouldStayAtBottom:Z

    .line 148
    const-string v0, ""

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->initialMessage:Ljava/lang/String;

    .line 149
    iput-boolean v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->isTwoPane:Z

    .line 337
    new-instance v0, Lio/intercom/android/sdk/conversation/ConversationFragment$4;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$4;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->globalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 518
    new-instance v0, Lio/intercom/android/sdk/conversation/ConversationFragment$7;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$7;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->connectivityEventListener:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor$ConnectivityEventListener;

    .line 778
    new-instance v0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$9;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerListener:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter$Listener;

    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/conversation/ConversationFragment;)Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    return-object v0
.end method

.method private configureInputView(Lio/intercom/android/sdk/models/Conversation;)V
    .locals 7

    .prologue
    .line 460
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v6

    .line 461
    invoke-virtual {v6}, Lio/intercom/android/sdk/models/Part;->getReactionReply()Lio/intercom/android/sdk/models/ReactionReply;

    move-result-object v0

    invoke-static {v0}, Lio/intercom/android/sdk/models/ReactionReply;->isNull(Lio/intercom/android/sdk/models/ReactionReply;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->showComposer()V

    .line 463
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->requestFocus()V

    .line 464
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->reactionComposer:Lio/intercom/android/sdk/conversation/ReactionInputView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ReactionInputView;->setVisibility(I)V

    .line 471
    :goto_0
    return-void

    .line 466
    :cond_0
    new-instance v0, Lio/intercom/android/sdk/activities/ConversationReactionListener;

    sget-object v1, Lio/intercom/android/sdk/metrics/MetricTracker$ReactionLocation;->CONVERSATION:Lio/intercom/android/sdk/metrics/MetricTracker$ReactionLocation;

    .line 467
    invoke-virtual {v6}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v4

    invoke-virtual {v4}, Lio/intercom/android/sdk/Injector;->getApi()Lio/intercom/android/sdk/api/Api;

    move-result-object v4

    iget-object v5, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-direct/range {v0 .. v5}, Lio/intercom/android/sdk/activities/ConversationReactionListener;-><init>(Lio/intercom/android/sdk/metrics/MetricTracker$ReactionLocation;Ljava/lang/String;Ljava/lang/String;Lio/intercom/android/sdk/api/Api;Lio/intercom/android/sdk/metrics/MetricTracker;)V

    .line 468
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->reactionComposer:Lio/intercom/android/sdk/conversation/ReactionInputView;

    invoke-virtual {v6}, Lio/intercom/android/sdk/models/Part;->getReactionReply()Lio/intercom/android/sdk/models/ReactionReply;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-virtual {v1, v2, v3, v0, v4}, Lio/intercom/android/sdk/conversation/ReactionInputView;->setUpReactions(Lio/intercom/android/sdk/models/ReactionReply;ZLio/intercom/android/sdk/conversation/ReactionListener;Lio/intercom/com/bumptech/glide/i;)V

    .line 469
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->hideComposer()V

    goto :goto_0
.end method

.method private static createGalleryImageForUri(Landroid/net/Uri;)Lcom/intercom/input/gallery/c;
    .locals 6

    .prologue
    .line 253
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 254
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 255
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 256
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 257
    new-instance v2, Lcom/intercom/input/gallery/c$a;

    invoke-direct {v2}, Lcom/intercom/input/gallery/c$a;-><init>()V

    .line 258
    invoke-virtual {v2, v0}, Lcom/intercom/input/gallery/c$a;->c(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    .line 259
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/c$a;->a(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    .line 260
    invoke-virtual {v2, v0}, Lcom/intercom/input/gallery/c$a;->d(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 261
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-virtual {v2, v0}, Lcom/intercom/input/gallery/c$a;->c(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v0

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 262
    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/c$a;->b(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v0

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 263
    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/c$a;->a(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v0

    iget-object v1, v1, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    .line 264
    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/c$a;->b(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Lcom/intercom/input/gallery/c$a;->a()Lcom/intercom/input/gallery/c;

    move-result-object v0

    .line 257
    return-object v0
.end method

.method private createNativeContentPresenter(Landroid/view/View;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter;
    .locals 20

    .prologue
    .line 423
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v3

    .line 424
    sget v1, Lio/intercom/android/sdk/R$id;->intercom_link:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lio/intercom/android/sdk/views/IntercomLinkView;

    .line 425
    sget v1, Lio/intercom/android/sdk/R$id;->pill:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 426
    sget v1, Lio/intercom/android/sdk/R$id;->conversation_list:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Landroid/support/v7/widget/RecyclerView;

    .line 427
    new-instance v15, Lio/intercom/android/sdk/blocks/Blocks;

    invoke-virtual/range {p0 .. p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getBlocksTwig()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v2

    invoke-direct {v15, v1, v2}, Lio/intercom/android/sdk/blocks/Blocks;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/twig/Twig;)V

    .line 428
    invoke-virtual {v3}, Lio/intercom/android/sdk/Injector;->getApi()Lio/intercom/android/sdk/api/Api;

    move-result-object v4

    .line 429
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 430
    new-instance v9, Lio/intercom/android/sdk/utilities/ContextLocaliser;

    move-object/from16 v0, p0

    iget-object v1, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-direct {v9, v1}, Lio/intercom/android/sdk/utilities/ContextLocaliser;-><init>(Lio/intercom/android/sdk/Provider;)V

    .line 431
    invoke-virtual/range {p0 .. p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 433
    invoke-virtual {v3}, Lio/intercom/android/sdk/Injector;->getTimeProvider()Lio/intercom/android/sdk/commons/utilities/TimeProvider;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    move-object/from16 v3, p0

    .line 431
    invoke-static/range {v1 .. v10}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->create(Landroid/app/Activity;Ljava/util/List;Lio/intercom/android/sdk/conversation/ConversationPartAdapter$Listener;Lio/intercom/android/sdk/api/Api;Ljava/lang/String;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/commons/utilities/TimeProvider;Lio/intercom/android/sdk/utilities/ContextLocaliser;Lio/intercom/com/bumptech/glide/i;)Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    move-result-object v7

    .line 434
    move-object/from16 v0, p0

    iget-object v9, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    move-object/from16 v0, p0

    iget-object v12, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    move-object/from16 v18, v0

    move-object/from16 v5, p0

    move-object/from16 v6, v19

    move-object v8, v11

    move-object v10, v4

    move-object v11, v2

    invoke-static/range {v5 .. v18}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->create(Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Landroid/support/v7/widget/RecyclerView;Lio/intercom/android/sdk/conversation/ConversationPartAdapter;Lio/intercom/android/sdk/views/IntercomLinkView;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/api/Api;Ljava/util/List;Ljava/lang/String;Landroid/widget/TextView;Lio/intercom/android/sdk/conversation/SoundPlayer;Lio/intercom/android/sdk/blocks/Blocks;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;Lio/intercom/com/bumptech/glide/i;)Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    move-result-object v1

    return-object v1
.end method

.method private createWebViewContentPresenter(Landroid/view/View;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter;
    .locals 17

    .prologue
    .line 440
    sget v1, Lio/intercom/android/sdk/R$id;->conversation_web_view_layout:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Landroid/view/ViewGroup;

    .line 441
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 442
    sget v1, Lio/intercom/android/sdk/R$id;->intercom_link:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lio/intercom/android/sdk/views/IntercomLinkView;

    .line 443
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v10

    .line 444
    invoke-virtual {v10}, Lio/intercom/android/sdk/Injector;->getApi()Lio/intercom/android/sdk/api/Api;

    move-result-object v7

    .line 445
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 446
    new-instance v1, Lio/intercom/android/sdk/conversation/ObservableWebView;

    invoke-virtual/range {p0 .. p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-direct {v1, v2}, Lio/intercom/android/sdk/conversation/ObservableWebView;-><init>(Landroid/content/Context;)V

    .line 447
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v6, -0x1

    invoke-direct {v2, v4, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 448
    invoke-virtual {v5, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 449
    invoke-virtual {v10}, Lio/intercom/android/sdk/Injector;->getAppIdentity()Lio/intercom/android/sdk/identity/AppIdentity;

    move-result-object v4

    .line 450
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 451
    sget v6, Lio/intercom/android/sdk/R$layout;->intercom_row_loading:I

    const/4 v9, 0x0

    invoke-virtual {v2, v6, v5, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 452
    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 453
    invoke-virtual {v5, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 454
    move-object/from16 v0, p0

    iget-object v5, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    move-object/from16 v0, p0

    iget-object v9, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    .line 456
    invoke-virtual {v10}, Lio/intercom/android/sdk/Injector;->getGson()Lio/intercom/com/google/gson/e;

    move-result-object v11

    new-instance v12, Lio/intercom/android/sdk/conversation/JavascriptRunner;

    invoke-direct {v12, v1}, Lio/intercom/android/sdk/conversation/JavascriptRunner;-><init>(Landroid/webkit/WebView;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    move-object/from16 v0, p0

    iget-object v14, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->bus:Lio/intercom/com/a/a/b;

    move-object/from16 v0, p0

    iget-object v15, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    move-object/from16 v16, v0

    move-object/from16 v10, p0

    .line 454
    invoke-static/range {v1 .. v16}, Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;->create(Lio/intercom/android/sdk/conversation/ObservableWebView;Landroid/view/View;Lio/intercom/android/sdk/views/IntercomLinkView;Lio/intercom/android/sdk/identity/AppIdentity;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/api/Api;Ljava/util/List;Ljava/lang/String;Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Lio/intercom/com/google/gson/e;Lio/intercom/android/sdk/conversation/JavascriptRunner;Lio/intercom/android/sdk/store/Store;Lio/intercom/com/a/a/b;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;Lio/intercom/android/sdk/profile/ProfilePresenter;)Lio/intercom/android/sdk/conversation/WebViewConversationContentPresenter;

    move-result-object v1

    return-object v1
.end method

.method private displayErrorView()V
    .locals 2

    .prologue
    .line 739
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setVisibility(I)V

    .line 740
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerHolder:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 741
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->showErrorView()V

    .line 742
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Lio/intercom/android/sdk/models/LastParticipatingAdmin;ZZLjava/lang/String;Ljava/util/List;)Lio/intercom/android/sdk/conversation/ConversationFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/intercom/android/sdk/models/LastParticipatingAdmin;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Participant;",
            ">;)",
            "Lio/intercom/android/sdk/conversation/ConversationFragment;"
        }
    .end annotation

    .prologue
    .line 164
    new-instance v0, Lio/intercom/android/sdk/conversation/ConversationFragment;

    invoke-direct {v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;-><init>()V

    .line 165
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 166
    const-string v2, "conversationId"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v2, "initial_message"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v2, "last_participant"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 169
    const-string v2, "group_participants"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 170
    const-string v2, "intercomsdk-isRead"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 171
    const-string v2, "is_two_pane"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 172
    const-class v2, Lio/intercom/android/sdk/models/Part;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 173
    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ConversationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 174
    return-object v0
.end method

.method private toggleProfile()V
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->closeProfile()V

    .line 632
    :goto_0
    return-void

    .line 629
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->profileClicked()V

    .line 630
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->smoothScrollToTop()V

    goto :goto_0
.end method

.method private trackLastPart(Lio/intercom/android/sdk/models/Part;)V
    .locals 5

    .prologue
    .line 723
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->isLinkList()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 724
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/metrics/MetricTracker;->receivedOperatorReply(Ljava/lang/String;)V

    .line 729
    :goto_0
    return-void

    .line 726
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->hasAttachments()Z

    move-result v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->isLinkCard()Z

    move-result v2

    .line 727
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    .line 726
    invoke-virtual {v0, v1, v2, v3, v4}, Lio/intercom/android/sdk/metrics/MetricTracker;->receivedReply(ZZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateLastActiveTime()V
    .locals 3

    .prologue
    .line 510
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getLastParticipatingAdmin()Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 513
    new-instance v0, Lio/intercom/android/sdk/utilities/TimeFormatter;

    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v2

    invoke-virtual {v2}, Lio/intercom/android/sdk/Injector;->getTimeProvider()Lio/intercom/android/sdk/commons/utilities/TimeProvider;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/intercom/android/sdk/utilities/TimeFormatter;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/commons/utilities/TimeProvider;)V

    .line 514
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/utilities/TimeFormatter;->getAdminActiveStatus(Lio/intercom/android/sdk/models/LastParticipatingAdmin;Lio/intercom/android/sdk/Provider;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastActiveTime:Ljava/lang/CharSequence;

    .line 516
    :cond_0
    return-void
.end method

.method private updateProfileToolbar(Lio/intercom/android/sdk/models/TeamPresence;)V
    .locals 5

    .prologue
    .line 498
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    invoke-static {v0}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->isNull(Lio/intercom/android/sdk/models/LastParticipatingAdmin;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    sget-object v1, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->NONE:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    if-ne v0, v1, :cond_1

    .line 500
    :cond_0
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 501
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-virtual {v1, p1, v0, v2}, Lio/intercom/android/sdk/profile/ProfilePresenter;->setTeamPresence(Lio/intercom/android/sdk/models/TeamPresence;ILio/intercom/com/bumptech/glide/i;)V

    .line 507
    :goto_0
    return-void

    .line 503
    :cond_1
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->updateLastActiveTime()V

    .line 504
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->groupParticipants:Ljava/util/List;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastActiveTime:Ljava/lang/CharSequence;

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    .line 505
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 504
    invoke-virtual {v0, v1, v2, v3, v4}, Lio/intercom/android/sdk/profile/ProfilePresenter;->setTeammatePresence(Lio/intercom/android/sdk/models/LastParticipatingAdmin;Ljava/util/List;Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method


# virtual methods
.method public adminIsTyping(Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;)V
    .locals 2
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 773
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {p1}, Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0, p1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->onAdminStartedTyping(Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;)V

    .line 776
    :cond_0
    return-void
.end method

.method public conversationFailure(Lio/intercom/android/sdk/models/events/failure/ConversationFailedEvent;)V
    .locals 1
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 733
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->displayErrorView()V

    .line 736
    :cond_0
    return-void
.end method

.method public conversationSuccess(Lio/intercom/android/sdk/models/events/ConversationEvent;)V
    .locals 8
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 639
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerHolder:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 641
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 642
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ConversationEvent;->getResponse()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v4

    .line 644
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ConversationEvent;->getResponse()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 645
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_3

    .line 646
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ConversationEvent;->getResponse()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    .line 649
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 650
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v1, p1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->onConversationFetched(Lio/intercom/android/sdk/models/events/ConversationEvent;)V

    .line 651
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->displayConversation()V

    .line 654
    :cond_0
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Conversation;->isRead()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    .line 655
    :goto_0
    if-eqz v1, :cond_1

    .line 656
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v5

    invoke-virtual {v5}, Lio/intercom/android/sdk/Injector;->getApi()Lio/intercom/android/sdk/api/Api;

    move-result-object v5

    iget-object v6, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lio/intercom/android/sdk/api/Api;->markConversationAsRead(Ljava/lang/String;)V

    .line 657
    iget-object v5, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    iget-object v6, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-static {v6}, Lio/intercom/android/sdk/actions/Actions;->conversationMarkedAsRead(Ljava/lang/String;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v6

    invoke-virtual {v5, v6}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 658
    iget-object v5, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    iget-object v6, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    iget-object v7, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v7}, Lio/intercom/android/sdk/identity/UserIdentity;->getIntercomId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lio/intercom/android/sdk/nexus/NexusEvent;->getConversationSeenEvent(Ljava/lang/String;Ljava/lang/String;)Lio/intercom/android/sdk/nexus/NexusEvent;

    move-result-object v6

    invoke-virtual {v5, v6}, Lio/intercom/android/sdk/nexus/NexusClient;->fire(Lio/intercom/android/sdk/nexus/NexusEvent;)V

    .line 662
    :cond_1
    if-nez v0, :cond_7

    .line 663
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 665
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->isReply()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    .line 666
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->trackLastPart(Lio/intercom/android/sdk/models/Part;)V

    .line 668
    :cond_2
    iget-boolean v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->canOpenProfile:Z

    if-eqz v0, :cond_3

    .line 669
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 670
    if-eqz v0, :cond_3

    .line 671
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profileExpansionLogic:Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v1, v4}, Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;->shouldExpandProfile(Lio/intercom/android/sdk/models/Conversation;)Z

    move-result v1

    .line 673
    new-instance v4, Lio/intercom/android/sdk/conversation/ConversationFragment$8;

    invoke-direct {v4, p0, v1}, Lio/intercom/android/sdk/conversation/ConversationFragment$8;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;Z)V

    const-wide/16 v6, 0x32

    invoke-virtual {v0, v4, v6, v7}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 688
    if-eqz v1, :cond_6

    .line 689
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->scrollToTop()V

    .line 693
    :goto_1
    iput-boolean v3, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->canOpenProfile:Z

    .line 711
    :cond_3
    :goto_2
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getLastParticipatingAdmin()Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    .line 712
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getGroupConversationParticipants()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->groupParticipants:Ljava/util/List;

    .line 714
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    if-eqz v0, :cond_4

    .line 715
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->updateLastActiveTime()V

    .line 718
    :cond_4
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->TEAM_PRESENCE:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/TeamPresence;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->updateProfileToolbar(Lio/intercom/android/sdk/models/TeamPresence;)V

    .line 719
    iput-boolean v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->hasLoadedConversation:Z

    .line 720
    return-void

    :cond_5
    move v1, v3

    .line 654
    goto/16 :goto_0

    .line 691
    :cond_6
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->scrollToBottom()V

    goto :goto_1

    .line 698
    :cond_7
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v4, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 699
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 700
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->isAdmin()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 701
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/SoundPlayer;->playMessageReceivedSound()V

    .line 706
    :cond_9
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->onNewPartReceived()V

    .line 707
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->trackLastPart(Lio/intercom/android/sdk/models/Part;)V

    goto :goto_2
.end method

.method displayConversation()V
    .locals 2

    .prologue
    .line 474
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->showContentView()V

    .line 477
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->configureInputView(Lio/intercom/android/sdk/models/Conversation;)V

    .line 479
    :cond_0
    return-void
.end method

.method displayLoadingView()V
    .locals 2

    .prologue
    .line 361
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setVisibility(I)V

    .line 363
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->hideComposer()V

    .line 364
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->showLoadingView()V

    .line 366
    :cond_0
    return-void
.end method

.method public getConversation()Lio/intercom/android/sdk/models/Conversation;
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    return-object v0
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    return-object v0
.end method

.method public handleOnBackPressed()V
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->onBackPressed()V

    .line 886
    return-void
.end method

.method hasNotLoadedLastAdminForExistingConversation()Z
    .locals 1

    .prologue
    .line 494
    iget-boolean v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->hasLoadedConversation:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    invoke-static {v0}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->isNull(Lio/intercom/android/sdk/models/LastParticipatingAdmin;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newComment(Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;)V
    .locals 2
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 767
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 768
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0, p1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->onNewCommentEventReceived(Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;)V

    .line 770
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 269
    packed-switch p1, :pswitch_data_0

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 271
    :pswitch_0
    if-eqz p3, :cond_0

    const-string v0, "gallery_image"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    const-string v0, "gallery_image"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/c;

    .line 275
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerListener:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter$Listener;

    invoke-interface {v1, v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter$Listener;->onUploadImageSelected(Lcom/intercom/input/gallery/c;)V

    goto :goto_0

    .line 269
    :pswitch_data_0
    .packed-switch 0x19
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 182
    :try_start_0
    move-object v0, p1

    check-cast v0, Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;

    move-object v1, v0

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->listener:Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    return-void

    .line 183
    :catch_0
    move-exception v1

    .line 184
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement ConversationFragment.Listener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCloseClicked()V
    .locals 1

    .prologue
    .line 861
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->listener:Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;->onToolbarCloseClicked()V

    .line 862
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 536
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 537
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->currentOrientation:I

    if-eq v0, v1, :cond_0

    .line 538
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->orientationChanged:Z

    .line 539
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->updateMaxLines()V

    .line 541
    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->currentOrientation:I

    .line 542
    return-void
.end method

.method public onConversationCreated(Lio/intercom/android/sdk/models/Conversation;Z)V
    .locals 7

    .prologue
    .line 604
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    .line 605
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    .line 607
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/profile/ProfilePresenter;->setConversationId(Ljava/lang/String;)V

    .line 608
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->setConversationId(Ljava/lang/String;)V

    .line 609
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_reply_to_conversation:I

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->setHint(I)V

    .line 611
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v2}, Lio/intercom/android/sdk/identity/UserIdentity;->getIntercomId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lio/intercom/android/sdk/nexus/NexusEvent;->getNewCommentEvent(Ljava/lang/String;Ljava/lang/String;)Lio/intercom/android/sdk/nexus/NexusEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/nexus/NexusClient;->fire(Lio/intercom/android/sdk/nexus/NexusEvent;)V

    .line 613
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lio/intercom/android/sdk/models/Part;

    .line 614
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Part;->hasAttachments()Z

    move-result v1

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Part;->isGifOnlyPart()Z

    move-result v3

    .line 615
    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v6, Lio/intercom/android/sdk/store/Selectors;->TEAM_PRESENCE:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v2, v6}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/intercom/android/sdk/models/TeamPresence;

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/TeamPresence;->getOfficeHours()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    move v2, p2

    .line 614
    invoke-virtual/range {v0 .. v6}, Lio/intercom/android/sdk/metrics/MetricTracker;->sentInNewConversation(ZZZLjava/lang/String;Ljava/lang/String;Z)V

    .line 616
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/metrics/MetricTracker;->startConversation(Ljava/lang/String;)V

    .line 618
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->hasLoadedConversation:Z

    .line 619
    return-void
.end method

.method public onConversationUpdated(Lio/intercom/android/sdk/models/Conversation;)V
    .locals 1

    .prologue
    .line 622
    new-instance v0, Lio/intercom/android/sdk/models/events/ConversationEvent;

    invoke-direct {v0, p1}, Lio/intercom/android/sdk/models/events/ConversationEvent;-><init>(Lio/intercom/android/sdk/models/Conversation;)V

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationSuccess(Lio/intercom/android/sdk/models/events/ConversationEvent;)V

    .line 623
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 189
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 190
    invoke-static {p0}, Lio/intercom/com/bumptech/glide/c;->a(Landroid/support/v4/app/Fragment;)Lio/intercom/com/bumptech/glide/i;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 191
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v1

    .line 193
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getBus()Lio/intercom/com/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->bus:Lio/intercom/com/a/a/b;

    .line 194
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getNexusClient()Lio/intercom/android/sdk/nexus/NexusClient;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    .line 195
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getAppConfigProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 196
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getUserIdentity()Lio/intercom/android/sdk/identity/UserIdentity;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 197
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getStore()Lio/intercom/android/sdk/store/Store;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    .line 199
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    invoke-virtual {v0}, Lio/intercom/android/sdk/store/Store;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/State;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/State;->lastScreenshot()Landroid/net/Uri;

    move-result-object v0

    .line 200
    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    if-eq v0, v2, :cond_0

    .line 201
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {}, Lio/intercom/android/sdk/actions/Actions;->screenshotLightboxOpened()Lio/intercom/android/sdk/actions/Action;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 202
    invoke-static {v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->createGalleryImageForUri(Landroid/net/Uri;)Lcom/intercom/input/gallery/c;

    move-result-object v0

    .line 203
    const-class v2, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;

    .line 204
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->a(Landroid/content/Context;Lcom/intercom/input/gallery/c;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 205
    const/16 v2, 0x19

    invoke-virtual {p0, v0, v2}, Lio/intercom/android/sdk/conversation/ConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 208
    :cond_0
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getMetricTracker()Lio/intercom/android/sdk/metrics/MetricTracker;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    .line 209
    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getOpsMetricTracker()Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    .line 211
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "start"

    const-string v2, "time-to-process-action-load-conversation-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 214
    if-eqz v1, :cond_4

    .line 215
    const-class v0, Lio/intercom/android/sdk/models/Part;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 216
    const-string v0, "conversationId"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    .line 217
    const-string v0, "initial_message"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->initialMessage:Ljava/lang/String;

    .line 219
    const-string v0, "last_participant"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    .line 221
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    if-nez v0, :cond_1

    .line 222
    sget-object v0, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->NULL:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    .line 225
    :cond_1
    const-string v0, "group_participants"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->groupParticipants:Ljava/util/List;

    .line 227
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->groupParticipants:Ljava/util/List;

    if-nez v0, :cond_2

    .line 228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->groupParticipants:Ljava/util/List;

    .line 231
    :cond_2
    const-string v0, "intercomsdk-isRead"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 232
    if-nez v0, :cond_3

    .line 233
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-static {v2}, Lio/intercom/android/sdk/actions/Actions;->conversationMarkedAsRead(Ljava/lang/String;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 236
    :cond_3
    const-string v0, "is_two_pane"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->isTwoPane:Z

    .line 238
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 239
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {}, Lio/intercom/android/sdk/actions/Actions;->composerOpened()Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 245
    :cond_4
    :goto_0
    new-instance v0, Lio/intercom/android/sdk/models/Conversation;

    invoke-direct {v0}, Lio/intercom/android/sdk/models/Conversation;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    .line 247
    new-instance v0, Lio/intercom/android/sdk/conversation/SoundPlayer;

    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-direct {v0, v1, v2}, Lio/intercom/android/sdk/conversation/SoundPlayer;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/Provider;)V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    .line 249
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->currentOrientation:I

    .line 250
    return-void

    .line 241
    :cond_5
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-static {v1}, Lio/intercom/android/sdk/actions/Actions;->conversationOpened(Ljava/lang/String;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 285
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 286
    sget v0, Lio/intercom/android/sdk/R$layout;->intercom_fragment_conversation:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    .line 296
    :cond_0
    :goto_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    sget v2, Lio/intercom/android/sdk/R$id;->composer_holder:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerHolder:Landroid/widget/FrameLayout;

    .line 298
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    sget v2, Lio/intercom/android/sdk/R$id;->conversation_coordinator_layout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CoordinatorLayout;

    .line 299
    new-instance v2, Lio/intercom/android/sdk/profile/ProfilePresenter;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v5, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-direct {v2, v0, v3, v4, v5}, Lio/intercom/android/sdk/profile/ProfilePresenter;-><init>(Landroid/support/design/widget/CoordinatorLayout;Lio/intercom/android/sdk/metrics/MetricTracker;Lio/intercom/android/sdk/Provider;Lio/intercom/com/bumptech/glide/i;)V

    iput-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    .line 300
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/profile/ProfilePresenter;->setConversationId(Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    sget v2, Lio/intercom/android/sdk/R$id;->error_layout_conversation:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/views/IntercomErrorView;

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    .line 302
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonTextColor(I)V

    .line 303
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    new-instance v2, Lio/intercom/android/sdk/conversation/ConversationFragment$1;

    invoke-direct {v2, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$1;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    new-instance v2, Lio/intercom/android/sdk/conversation/ConversationFragment$2;

    invoke-direct {v2, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$2;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/profile/ProfilePresenter;->addListener(Landroid/support/design/widget/AppBarLayout$b;)V

    .line 316
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    sget v2, Lio/intercom/android/sdk/R$id;->wallpaper:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 317
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-static {v2, v3, v4}, Lio/intercom/android/sdk/imageloader/WallpaperLoader;->create(Landroid/content/Context;Lio/intercom/android/sdk/Provider;Lio/intercom/com/bumptech/glide/i;)Lio/intercom/android/sdk/imageloader/WallpaperLoader;

    move-result-object v2

    iput-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->wallpaperLoader:Lio/intercom/android/sdk/imageloader/WallpaperLoader;

    .line 318
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->wallpaperLoader:Lio/intercom/android/sdk/imageloader/WallpaperLoader;

    new-instance v3, Lio/intercom/android/sdk/conversation/ConversationFragment$3;

    invoke-direct {v3, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$3;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V

    invoke-virtual {v2, v0, v3}, Lio/intercom/android/sdk/imageloader/WallpaperLoader;->loadWallpaperInto(Landroid/widget/ImageView;Lio/intercom/android/sdk/imageloader/WallpaperLoader$Listener;)V

    .line 324
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    sget v2, Lio/intercom/android/sdk/R$id;->reaction_input_view:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/conversation/ReactionInputView;

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->reactionComposer:Lio/intercom/android/sdk/conversation/ReactionInputView;

    .line 327
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    sget v2, Lio/intercom/android/sdk/R$id;->intercom_toolbar:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/views/IntercomToolbar;

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    .line 328
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v0, p0}, Lio/intercom/android/sdk/views/IntercomToolbar;->setListener(Lio/intercom/android/sdk/views/IntercomToolbar$Listener;)V

    .line 329
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    iget-boolean v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->isTwoPane:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/views/IntercomToolbar;->setInboxButtonVisibility(I)V

    .line 330
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->listener:Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;->setStatusBarColor()V

    .line 333
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->globalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 334
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    return-object v0

    .line 290
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 291
    if-eqz v0, :cond_0

    .line 292
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 329
    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {}, Lio/intercom/android/sdk/actions/Actions;->conversationClosed()Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 581
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 582
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 567
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->globalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v0, v1}, Lio/intercom/android/sdk/utilities/ViewUtils;->removeGlobalLayoutListener(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 568
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->cleanup()V

    .line 569
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->cleanup()V

    .line 572
    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->wallpaperLoader:Lio/intercom/android/sdk/imageloader/WallpaperLoader;

    invoke-virtual {v0}, Lio/intercom/android/sdk/imageloader/WallpaperLoader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 576
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 577
    return-void

    .line 573
    :catch_0
    move-exception v0

    .line 574
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->twig:Lio/intercom/android/sdk/twig/Twig;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t close LongTermImageLoader: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lio/intercom/android/sdk/twig/Twig;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onInboxClicked()V
    .locals 3

    .prologue
    .line 869
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->cleanup()V

    .line 870
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->listener:Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationFragment$Listener;->onBackToInboxClicked()V

    .line 871
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomToolbar;->setInboxButtonVisibility(I)V

    .line 872
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v0}, Lio/intercom/android/sdk/views/IntercomToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_conversations_with_app:I

    invoke-static {v0, v1}, Lio/intercom/android/sdk/utilities/Phrase;->from(Landroid/content/Context;I)Lio/intercom/android/sdk/utilities/Phrase;

    move-result-object v1

    const-string v2, "name"

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 873
    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/utilities/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lio/intercom/android/sdk/utilities/Phrase;

    move-result-object v0

    .line 874
    invoke-virtual {v0}, Lio/intercom/android/sdk/utilities/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 875
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/views/IntercomToolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 876
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomToolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 877
    return-void
.end method

.method public onPartClicked(Lio/intercom/android/sdk/models/Part;)V
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0, p1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->onPartClicked(Lio/intercom/android/sdk/models/Part;)V

    .line 846
    return-void
.end method

.method public onPostCardClicked()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 849
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v1

    .line 850
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v0

    const-string v2, "post"

    if-ne v0, v2, :cond_0

    .line 851
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->onPostCardClicked(Lio/intercom/android/sdk/models/Part;)V

    .line 853
    :cond_0
    return-void
.end method

.method public onPostCardClicked(Lio/intercom/android/sdk/models/Part;)V
    .locals 4

    .prologue
    .line 856
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, ""

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lio/intercom/android/sdk/activities/IntercomPostActivity;->buildPostIntent(Landroid/content/Context;Lio/intercom/android/sdk/models/Part;Ljava/lang/String;Lio/intercom/android/sdk/models/LastParticipatingAdmin;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->startActivity(Landroid/content/Intent;)V

    .line 857
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 482
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 483
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->networkConnectivityMonitor:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;

    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;->startListening(Landroid/content/Context;)V

    .line 484
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->networkConnectivityMonitor:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->connectivityEventListener:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor$ConnectivityEventListener;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;->setListener(Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor$ConnectivityEventListener;)V

    .line 486
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->UNREAD_COUNT:Lio/intercom/android/sdk/store/Store$Selector;

    sget-object v2, Lio/intercom/android/sdk/store/Selectors;->TEAM_PRESENCE:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1, v2, p0}, Lio/intercom/android/sdk/store/Store;->subscribeToChanges(Lio/intercom/android/sdk/store/Store$Selector;Lio/intercom/android/sdk/store/Store$Selector;Lio/intercom/android/sdk/store/Store$Subscriber2;)Lio/intercom/android/sdk/store/Store$Subscription;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->subscription:Lio/intercom/android/sdk/store/Store$Subscription;

    .line 487
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->bus:Lio/intercom/com/a/a/b;

    invoke-virtual {v0, p0}, Lio/intercom/com/a/a/b;->register(Ljava/lang/Object;)V

    .line 488
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->bus:Lio/intercom/com/a/a/b;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-virtual {v0, v1}, Lio/intercom/com/a/a/b;->register(Ljava/lang/Object;)V

    .line 490
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->fetchConversation(Ljava/lang/String;)V

    .line 491
    return-void
.end method

.method public onStateChange(Ljava/lang/Integer;Lio/intercom/android/sdk/models/TeamPresence;)V
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/views/IntercomToolbar;->setUnreadCount(Ljava/lang/Integer;)V

    .line 547
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->hasNotLoadedLastAdminForExistingConversation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->setUnknownPresence()V

    .line 553
    :goto_0
    return-void

    .line 551
    :cond_0
    invoke-direct {p0, p2}, Lio/intercom/android/sdk/conversation/ConversationFragment;->updateProfileToolbar(Lio/intercom/android/sdk/models/TeamPresence;)V

    goto :goto_0
.end method

.method public bridge synthetic onStateChange(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 97
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Lio/intercom/android/sdk/models/TeamPresence;

    invoke-virtual {p0, p1, p2}, Lio/intercom/android/sdk/conversation/ConversationFragment;->onStateChange(Ljava/lang/Integer;Lio/intercom/android/sdk/models/TeamPresence;)V

    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->subscription:Lio/intercom/android/sdk/store/Store$Subscription;

    invoke-static {v0}, Lio/intercom/android/sdk/utilities/StoreUtils;->safeUnsubscribe(Lio/intercom/android/sdk/store/Store$Subscription;)V

    .line 557
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->bus:Lio/intercom/com/a/a/b;

    invoke-virtual {v0, p0}, Lio/intercom/com/a/a/b;->unregister(Ljava/lang/Object;)V

    .line 558
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->bus:Lio/intercom/com/a/a/b;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-virtual {v0, v1}, Lio/intercom/com/a/a/b;->unregister(Ljava/lang/Object;)V

    .line 559
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->networkConnectivityMonitor:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;->setListener(Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor$ConnectivityEventListener;)V

    .line 560
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->networkConnectivityMonitor:Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;

    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/utilities/connectivity/NetworkConnectivityMonitor;->stopListening(Landroid/content/Context;)V

    .line 561
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    invoke-virtual {v0}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->clear()V

    .line 562
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->onStop()V

    .line 563
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 564
    return-void
.end method

.method public onToolbarClicked()V
    .locals 0

    .prologue
    .line 865
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->toggleProfile()V

    .line 866
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 369
    invoke-super/range {p0 .. p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 371
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    const-string v1, "conversation-web-view"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/identity/AppConfig;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373
    :try_start_0
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/ConversationFragment;->createWebViewContentPresenter(Landroid/view/View;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 381
    :goto_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->rootView:Landroid/view/View;

    sget v1, Lio/intercom/android/sdk/R$id;->composer_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 382
    new-instance v0, Lio/intercom/android/sdk/conversation/ConversationFragment$5;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$5;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 390
    new-instance v0, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerListener:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter$Listener;

    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getChildFragmentManager()Landroid/support/v4/app/n;

    move-result-object v3

    .line 391
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    iget-object v5, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    iget-object v6, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    iget-object v7, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v8, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    iget-object v9, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    iget-object v10, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->initialMessage:Ljava/lang/String;

    iget-object v11, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    .line 392
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v12

    iget-object v13, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v13}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v13}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v13

    invoke-direct/range {v0 .. v13}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;-><init>(Lio/intercom/android/sdk/conversation/composer/ComposerPresenter$Listener;Landroid/widget/FrameLayout;Landroid/support/v4/app/n;Landroid/view/LayoutInflater;Lio/intercom/android/sdk/nexus/NexusClient;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/metrics/MetricTracker;Ljava/lang/String;Ljava/lang/String;Lio/intercom/android/sdk/store/Store;Landroid/content/Context;I)V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    .line 394
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->setup()V

    .line 395
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->requestFocus()V

    .line 397
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->configureInputView(Lio/intercom/android/sdk/models/Conversation;)V

    .line 399
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 400
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerHolder:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 401
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->displayConversation()V

    .line 407
    :goto_1
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->canOpenProfile:Z

    .line 409
    new-instance v0, Lio/intercom/android/sdk/conversation/ConversationFragment$6;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$6;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V

    const-wide/16 v2, 0x32

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 420
    :cond_0
    return-void

    .line 374
    :catch_0
    move-exception v0

    .line 375
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/ConversationFragment;->createNativeContentPresenter(Landroid/view/View;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    goto/16 :goto_0

    .line 378
    :cond_1
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/ConversationFragment;->createNativeContentPresenter(Landroid/view/View;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    goto/16 :goto_0

    .line 403
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->setConversationId(Ljava/lang/String;)V

    .line 404
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->displayLoadingView()V

    goto :goto_1
.end method

.method public replySuccess(Lio/intercom/android/sdk/models/events/ReplyEvent;)V
    .locals 12
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 745
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ReplyEvent;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 746
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->nexusClient:Lio/intercom/android/sdk/nexus/NexusClient;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v2}, Lio/intercom/android/sdk/identity/UserIdentity;->getIntercomId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lio/intercom/android/sdk/nexus/NexusEvent;->getNewCommentEvent(Ljava/lang/String;Ljava/lang/String;)Lio/intercom/android/sdk/nexus/NexusEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/nexus/NexusClient;->fire(Lio/intercom/android/sdk/nexus/NexusEvent;)V

    .line 748
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ReplyEvent;->getResponse()Lio/intercom/android/sdk/models/Part;

    move-result-object v4

    .line 750
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Conversation;->getParticipant(Ljava/lang/String;)Lio/intercom/android/sdk/models/Participant;

    move-result-object v0

    .line 751
    sget-object v1, Lio/intercom/android/sdk/models/Participant;->NULL:Lio/intercom/android/sdk/models/Participant;

    if-ne v0, v1, :cond_0

    .line 752
    new-instance v0, Lio/intercom/android/sdk/models/Participant$Builder;

    invoke-direct {v0}, Lio/intercom/android/sdk/models/Participant$Builder;-><init>()V

    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Participant$Builder;->withId(Ljava/lang/String;)Lio/intercom/android/sdk/models/Participant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Participant$Builder;->build()Lio/intercom/android/sdk/models/Participant;

    move-result-object v0

    .line 753
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Conversation;->getParticipants()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->getParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 755
    :cond_0
    invoke-virtual {v4, v0}, Lio/intercom/android/sdk/models/Part;->setParticipant(Lio/intercom/android/sdk/models/Participant;)V

    .line 757
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0, p1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->onReplyDelivered(Lio/intercom/android/sdk/models/events/ReplyEvent;)V

    .line 759
    new-instance v8, Lio/intercom/android/sdk/utilities/TimeFormatter;

    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getTimeProvider()Lio/intercom/android/sdk/commons/utilities/TimeProvider;

    move-result-object v1

    invoke-direct {v8, v0, v1}, Lio/intercom/android/sdk/utilities/TimeFormatter;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/commons/utilities/TimeProvider;)V

    .line 760
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->hasAttachments()Z

    move-result v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ReplyEvent;->isAnnotated()Z

    move-result v2

    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->isGifOnlyPart()Z

    move-result v3

    .line 761
    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversationId:Ljava/lang/String;

    iget-object v6, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v7, Lio/intercom/android/sdk/store/Selectors;->TEAM_PRESENCE:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v6, v7}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/intercom/android/sdk/models/TeamPresence;

    invoke-virtual {v6}, Lio/intercom/android/sdk/models/TeamPresence;->getOfficeHours()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    iget-object v7, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    .line 762
    invoke-virtual {v7}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->isActive()Z

    move-result v7

    iget-object v9, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->lastParticipant:Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    invoke-virtual {v9}, Lio/intercom/android/sdk/models/LastParticipatingAdmin;->getLastActiveAt()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lio/intercom/android/sdk/utilities/TimeFormatter;->getLastActiveMinutes(J)Ljava/lang/String;

    move-result-object v8

    .line 760
    invoke-virtual/range {v0 .. v8}, Lio/intercom/android/sdk/metrics/MetricTracker;->sentInConversation(ZZZLjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 764
    :cond_1
    return-void
.end method

.method public sdkWindowFinishedAnimating()V
    .locals 2

    .prologue
    .line 589
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profileExpansionLogic:Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/ProfileExpansionLogic;->shouldExpandProfile(Lio/intercom/android/sdk/models/Conversation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->profilePresenter:Lio/intercom/android/sdk/profile/ProfilePresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/profile/ProfilePresenter;->profileAutoOpened()V

    .line 591
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->smoothScrollToTop()V

    .line 593
    :cond_0
    return-void
.end method

.method public shouldHandleOnBackPressed()Z
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
