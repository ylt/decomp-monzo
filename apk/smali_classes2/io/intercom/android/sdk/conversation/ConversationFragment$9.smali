.class Lio/intercom/android/sdk/conversation/ConversationFragment$9;
.super Ljava/lang/Object;
.source "ConversationFragment.java"

# interfaces
.implements Lio/intercom/android/sdk/conversation/composer/ComposerPresenter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/conversation/ConversationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final blockFactory:Lio/intercom/android/sdk/blocks/BlockFactory;

.field final synthetic this$0:Lio/intercom/android/sdk/conversation/ConversationFragment;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/ConversationFragment;)V
    .locals 2

    .prologue
    .line 778
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->this$0:Lio/intercom/android/sdk/conversation/ConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 779
    new-instance v0, Lio/intercom/android/sdk/blocks/BlockFactory;

    new-instance v1, Lio/intercom/android/sdk/blocks/logic/TextSplittingStrategy;

    invoke-direct {v1}, Lio/intercom/android/sdk/blocks/logic/TextSplittingStrategy;-><init>()V

    invoke-direct {v0, v1}, Lio/intercom/android/sdk/blocks/BlockFactory;-><init>(Lio/intercom/android/sdk/blocks/logic/TextSplittingStrategy;)V

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->blockFactory:Lio/intercom/android/sdk/blocks/BlockFactory;

    return-void
.end method

.method private createBlocks(Lcom/intercom/input/gallery/c;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intercom/input/gallery/c;",
            ")",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 799
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 800
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "image"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 801
    new-instance v1, Lio/intercom/android/sdk/blocks/models/Block$Builder;

    invoke-direct {v1}, Lio/intercom/android/sdk/blocks/models/Block$Builder;-><init>()V

    .line 802
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withUrl(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    sget-object v2, Lio/intercom/android/sdk/blocks/BlockType;->LOCALIMAGE:Lio/intercom/android/sdk/blocks/BlockType;

    .line 803
    invoke-virtual {v2}, Lio/intercom/android/sdk/blocks/BlockType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 804
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withWidth(I)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 805
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withHeight(I)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 801
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 818
    :goto_0
    return-object v0

    .line 808
    :cond_0
    new-instance v1, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    invoke-direct {v1}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;-><init>()V

    .line 809
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->withName(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v1

    .line 810
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->withUrl(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v1

    .line 811
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->withContentType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v1

    .line 812
    invoke-virtual {v1}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->build()Lio/intercom/android/sdk/blocks/models/BlockAttachment;

    move-result-object v1

    .line 814
    new-instance v2, Lio/intercom/android/sdk/blocks/models/Block$Builder;

    invoke-direct {v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;-><init>()V

    .line 815
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withAttachments(Ljava/util/List;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    sget-object v2, Lio/intercom/android/sdk/blocks/BlockType;->LOCAL_ATTACHMENT:Lio/intercom/android/sdk/blocks/BlockType;

    .line 816
    invoke-virtual {v2}, Lio/intercom/android/sdk/blocks/BlockType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 814
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private showUploadError()V
    .locals 3

    .prologue
    .line 822
    new-instance v0, Landroid/support/v7/app/d$a;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->this$0:Lio/intercom/android/sdk/conversation/ConversationFragment;

    invoke-virtual {v1}, Lio/intercom/android/sdk/conversation/ConversationFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/d$a;-><init>(Landroid/content/Context;)V

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_failed_to_send:I

    .line 823
    invoke-virtual {v0, v1}, Landroid/support/v7/app/d$a;->a(I)Landroid/support/v7/app/d$a;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_file_too_big:I

    .line 824
    invoke-virtual {v0, v1}, Landroid/support/v7/app/d$a;->b(I)Landroid/support/v7/app/d$a;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lio/intercom/android/sdk/conversation/ConversationFragment$9$1;

    invoke-direct {v2, p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$9$1;-><init>(Lio/intercom/android/sdk/conversation/ConversationFragment$9;)V

    .line 825
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/d$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 829
    invoke-virtual {v0}, Landroid/support/v7/app/d$a;->c()Landroid/support/v7/app/d;

    .line 830
    return-void
.end method


# virtual methods
.method public onRemoteImageSelected(Lcom/intercom/input/gallery/c;)V
    .locals 3

    .prologue
    .line 833
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->this$0:Lio/intercom/android/sdk/conversation/ConversationFragment;

    iget-object v0, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    new-instance v1, Lio/intercom/android/sdk/blocks/models/Block$Builder;

    invoke-direct {v1}, Lio/intercom/android/sdk/blocks/models/Block$Builder;-><init>()V

    const-string v2, "image"

    .line 834
    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 835
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withUrl(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 836
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withAttribution(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 837
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withHeight(I)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 838
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withWidth(I)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 833
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->sendPart(Ljava/util/List;)V

    .line 840
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->this$0:Lio/intercom/android/sdk/conversation/ConversationFragment;

    iget-object v0, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->composerPresenter:Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/composer/ComposerPresenter;->returnToDefaultInput()V

    .line 841
    return-void
.end method

.method public onSendButtonPressed(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 782
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 783
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 784
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->this$0:Lio/intercom/android/sdk/conversation/ConversationFragment;

    invoke-static {v1}, Lio/intercom/android/sdk/conversation/ConversationFragment;->access$000(Lio/intercom/android/sdk/conversation/ConversationFragment;)Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    move-result-object v1

    const-string v2, "start"

    const-string v3, "time-to-process-action-send-part-ms"

    invoke-virtual {v1, v2, v3}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->this$0:Lio/intercom/android/sdk/conversation/ConversationFragment;

    iget-object v1, v1, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->blockFactory:Lio/intercom/android/sdk/blocks/BlockFactory;

    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/blocks/BlockFactory;->getBlocksForText(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->sendPart(Ljava/util/List;)V

    .line 787
    :cond_0
    return-void
.end method

.method public onUploadImageSelected(Lcom/intercom/input/gallery/c;)V
    .locals 2

    .prologue
    .line 790
    invoke-virtual {p1}, Lcom/intercom/input/gallery/c;->h()I

    move-result v0

    const/high16 v1, 0x2800000

    if-le v0, v1, :cond_0

    .line 791
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->showUploadError()V

    .line 796
    :goto_0
    return-void

    .line 795
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->this$0:Lio/intercom/android/sdk/conversation/ConversationFragment;

    iget-object v0, v0, Lio/intercom/android/sdk/conversation/ConversationFragment;->contentPresenter:Lio/intercom/android/sdk/conversation/ConversationContentPresenter;

    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/ConversationFragment$9;->createBlocks(Lcom/intercom/input/gallery/c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter;->uploadImage(Ljava/util/List;Lcom/intercom/input/gallery/c;)V

    goto :goto_0
.end method
