.class public Lio/intercom/android/sdk/conversation/ObservableWebView;
.super Landroid/webkit/WebView;
.source "ObservableWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;
    }
.end annotation


# instance fields
.field private onScrollChangeListener:Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .locals 6

    .prologue
    .line 18
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->onScrollChanged(IIII)V

    .line 19
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ObservableWebView;->onScrollChangeListener:Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/ObservableWebView;->onScrollChangeListener:Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;->onScrollChange(Landroid/webkit/WebView;IIII)V

    .line 22
    :cond_0
    return-void
.end method

.method public setOnScrollChangeListener(Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/ObservableWebView;->onScrollChangeListener:Lio/intercom/android/sdk/conversation/ObservableWebView$OnScrollChangeListener;

    .line 26
    return-void
.end method
