.class interface abstract Lio/intercom/android/sdk/conversation/ConversationContentPresenter;
.super Ljava/lang/Object;
.source "ConversationContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;
    }
.end annotation


# virtual methods
.method public abstract cleanup()V
.end method

.method public abstract fetchConversation(Ljava/lang/String;)V
.end method

.method public abstract isAtBottom()Z
.end method

.method public abstract onAdminStartedTyping(Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;)V
.end method

.method public abstract onConversationFetched(Lio/intercom/android/sdk/models/events/ConversationEvent;)V
.end method

.method public abstract onGlobalLayout()V
.end method

.method public abstract onNewCommentEventReceived(Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;)V
.end method

.method public abstract onNewConversation(Lio/intercom/android/sdk/models/events/NewConversationEvent;)V
.end method

.method public abstract onNewPartReceived()V
.end method

.method public abstract onPartClicked(Lio/intercom/android/sdk/models/Part;)V
.end method

.method public abstract onProfileScrolled()V
.end method

.method public abstract onReplyDelivered(Lio/intercom/android/sdk/models/events/ReplyEvent;)V
.end method

.method public abstract scrollToBottom()V
.end method

.method public abstract scrollToTop()V
.end method

.method public abstract sendPart(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setup()V
.end method

.method public abstract showContentView()V
.end method

.method public abstract showErrorView()V
.end method

.method public abstract showLoadingView()V
.end method

.method public abstract smoothScrollToTop()V
.end method

.method public abstract uploadImage(Ljava/util/List;Lcom/intercom/input/gallery/c;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;",
            "Lcom/intercom/input/gallery/c;",
            ")V"
        }
    .end annotation
.end method
