.class public Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;
.super Lcom/intercom/input/gallery/i;
.source "LocalGalleryLightBoxFragment.java"


# static fields
.field private static final ANNOTATION_COLORS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const v2, -0x29c6e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, -0x3907c7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, -0xbb2a03

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;->ANNOTATION_COLORS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/intercom/input/gallery/i;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/List;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;->ANNOTATION_COLORS:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected getInjector(Lcom/intercom/input/gallery/i;)Lcom/intercom/input/gallery/i$a;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment$1;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment$1;-><init>(Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;)V

    return-object v0
.end method
