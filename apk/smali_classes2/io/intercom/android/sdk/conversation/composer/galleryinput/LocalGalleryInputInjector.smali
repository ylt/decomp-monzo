.class Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryInputInjector;
.super Ljava/lang/Object;
.source "LocalGalleryInputInjector.java"

# interfaces
.implements Lcom/intercom/input/gallery/g$a;


# instance fields
.field private final requestManager:Lio/intercom/com/bumptech/glide/i;


# direct methods
.method public constructor <init>(Lio/intercom/com/bumptech/glide/i;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryInputInjector;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 29
    return-void
.end method


# virtual methods
.method public getDataSource(Lcom/intercom/input/gallery/g;)Lcom/intercom/input/gallery/e;
    .locals 1

    .prologue
    .line 52
    invoke-static {p1}, Lcom/intercom/input/gallery/m;->a(Lcom/intercom/input/gallery/g;)Lcom/intercom/input/gallery/e;

    move-result-object v0

    return-object v0
.end method

.method public getEmptyViewSubtitle(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget v0, Lio/intercom/android/sdk/R$string;->intercom_no_photos_on_device:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEmptyViewTitle(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget v0, Lio/intercom/android/sdk/R$string;->intercom_no_photos:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getErrorViewSubtitle(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public getErrorViewTitle(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpanderButton(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 32
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$layout;->intercom_expander_button:I

    const/4 v2, 0x0

    .line 33
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 34
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/Injector;->getAppConfigProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v1

    invoke-interface {v1}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v1}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setColorFilter(I)V

    .line 35
    return-object v0
.end method

.method public getImageLoader(Lcom/intercom/input/gallery/g;)Lcom/intercom/composer/e;
    .locals 3

    .prologue
    .line 56
    new-instance v0, Lio/intercom/android/sdk/conversation/composer/galleryinput/DownscaleOnlyCenterCrop;

    invoke-virtual {p1}, Lcom/intercom/input/gallery/g;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/android/sdk/conversation/composer/galleryinput/DownscaleOnlyCenterCrop;-><init>(Landroid/content/Context;)V

    .line 57
    sget-object v1, Lio/intercom/com/bumptech/glide/load/engine/h;->b:Lio/intercom/com/bumptech/glide/load/engine/h;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryInputInjector;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-static {v1, v0, v2}, Lio/intercom/android/sdk/conversation/composer/galleryinput/GalleryImageLoader;->create(Lio/intercom/com/bumptech/glide/load/engine/h;Lio/intercom/com/bumptech/glide/load/resource/bitmap/f;Lio/intercom/com/bumptech/glide/i;)Lio/intercom/android/sdk/conversation/composer/galleryinput/GalleryImageLoader;

    move-result-object v0

    return-object v0
.end method

.method public getLightBoxFragmentClass(Lcom/intercom/input/gallery/g;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intercom/input/gallery/g;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/intercom/input/gallery/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    const-class v0, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;

    return-object v0
.end method

.method public getSearchView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public getThemeColor(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/Injector;->getAppConfigProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v0

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    return v0
.end method

.method public getToolbar(Landroid/view/ViewGroup;)Landroid/support/v7/widget/Toolbar;
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$layout;->intercom_gallery_input_toolbar:I

    const/4 v2, 0x0

    .line 44
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 43
    return-object v0
.end method
