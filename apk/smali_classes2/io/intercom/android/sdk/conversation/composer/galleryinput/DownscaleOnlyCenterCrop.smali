.class Lio/intercom/android/sdk/conversation/composer/galleryinput/DownscaleOnlyCenterCrop;
.super Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;
.source "DownscaleOnlyCenterCrop.java"


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;-><init>(Landroid/content/Context;)V

    .line 13
    return-void
.end method


# virtual methods
.method protected transform(Lio/intercom/com/bumptech/glide/load/engine/a/e;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-gt v0, p4, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-le v0, p3, :cond_1

    .line 17
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lio/intercom/com/bumptech/glide/load/resource/bitmap/h;->transform(Lio/intercom/com/bumptech/glide/load/engine/a/e;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 19
    :cond_1
    return-object p2
.end method
