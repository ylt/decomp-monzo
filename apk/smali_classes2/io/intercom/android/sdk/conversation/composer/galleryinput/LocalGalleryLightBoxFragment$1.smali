.class Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment$1;
.super Ljava/lang/Object;
.source "LocalGalleryLightBoxFragment.java"

# interfaces
.implements Lcom/intercom/input/gallery/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;->getInjector(Lcom/intercom/input/gallery/i;)Lcom/intercom/input/gallery/i$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment$1;->this$0:Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private rootInjector()Lio/intercom/android/sdk/Injector;
    .locals 1

    .prologue
    .line 31
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAnnotationColors(Lcom/intercom/input/gallery/i;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intercom/input/gallery/i;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {}, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment;->access$000()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getImageLoader(Lcom/intercom/input/gallery/i;)Lcom/intercom/composer/e;
    .locals 3

    .prologue
    .line 19
    sget-object v0, Lio/intercom/com/bumptech/glide/load/engine/h;->b:Lio/intercom/com/bumptech/glide/load/engine/h;

    const/4 v1, 0x0

    invoke-static {p1}, Lio/intercom/com/bumptech/glide/c;->a(Landroid/support/v4/app/Fragment;)Lio/intercom/com/bumptech/glide/i;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/intercom/android/sdk/conversation/composer/galleryinput/GalleryImageLoader;->create(Lio/intercom/com/bumptech/glide/load/engine/h;Lio/intercom/com/bumptech/glide/load/resource/bitmap/f;Lio/intercom/com/bumptech/glide/i;)Lio/intercom/android/sdk/conversation/composer/galleryinput/GalleryImageLoader;

    move-result-object v0

    return-object v0
.end method

.method public isAnnotationEnabled(Lcom/intercom/input/gallery/i;)Z
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/composer/galleryinput/LocalGalleryLightBoxFragment$1;->rootInjector()Lio/intercom/android/sdk/Injector;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/Injector;->getAppConfigProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v0

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    const-string v1, "image-annotation"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/identity/AppConfig;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
