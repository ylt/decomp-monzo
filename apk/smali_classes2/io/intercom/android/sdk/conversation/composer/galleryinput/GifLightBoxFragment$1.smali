.class Lio/intercom/android/sdk/conversation/composer/galleryinput/GifLightBoxFragment$1;
.super Ljava/lang/Object;
.source "GifLightBoxFragment.java"

# interfaces
.implements Lcom/intercom/input/gallery/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/composer/galleryinput/GifLightBoxFragment;->getInjector(Lcom/intercom/input/gallery/i;)Lcom/intercom/input/gallery/i$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/composer/galleryinput/GifLightBoxFragment;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/composer/galleryinput/GifLightBoxFragment;)V
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/composer/galleryinput/GifLightBoxFragment$1;->this$0:Lio/intercom/android/sdk/conversation/composer/galleryinput/GifLightBoxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnnotationColors(Lcom/intercom/input/gallery/i;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intercom/input/gallery/i;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getImageLoader(Lcom/intercom/input/gallery/i;)Lcom/intercom/composer/e;
    .locals 3

    .prologue
    .line 16
    sget-object v0, Lio/intercom/com/bumptech/glide/load/engine/h;->c:Lio/intercom/com/bumptech/glide/load/engine/h;

    const/4 v1, 0x0

    invoke-static {p1}, Lio/intercom/com/bumptech/glide/c;->a(Landroid/support/v4/app/Fragment;)Lio/intercom/com/bumptech/glide/i;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/intercom/android/sdk/conversation/composer/galleryinput/GalleryImageLoader;->create(Lio/intercom/com/bumptech/glide/load/engine/h;Lio/intercom/com/bumptech/glide/load/resource/bitmap/f;Lio/intercom/com/bumptech/glide/i;)Lio/intercom/android/sdk/conversation/composer/galleryinput/GalleryImageLoader;

    move-result-object v0

    return-object v0
.end method

.method public isAnnotationEnabled(Lcom/intercom/input/gallery/i;)Z
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    return v0
.end method
