.class Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;
.super Ljava/lang/Object;
.source "WebViewHostWrapper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->handleAction(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

.field final synthetic val$conversation:Lio/intercom/android/sdk/models/Conversation;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;Lio/intercom/android/sdk/models/Conversation;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    iput-object p2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->val$conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$000(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    move-result-object v0

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$100(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/store/Store;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->val$conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-static {v1}, Lio/intercom/android/sdk/actions/Actions;->fetchConversationSuccess(Lio/intercom/android/sdk/models/Conversation;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 61
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$000(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    move-result-object v0

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->val$conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 62
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$100(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/store/Store;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->val$conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-static {v1}, Lio/intercom/android/sdk/actions/Actions;->newConversationSuccess(Lio/intercom/android/sdk/models/Conversation;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$000(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->val$conversation:Lio/intercom/android/sdk/models/Conversation;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;->onConversationCreated(Lio/intercom/android/sdk/models/Conversation;Z)V

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$000(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;->val$conversation:Lio/intercom/android/sdk/models/Conversation;

    invoke-interface {v0, v1}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;->onConversationUpdated(Lio/intercom/android/sdk/models/Conversation;)V

    goto :goto_0
.end method
