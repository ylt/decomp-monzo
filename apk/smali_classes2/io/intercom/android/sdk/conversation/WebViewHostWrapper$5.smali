.class Lio/intercom/android/sdk/conversation/WebViewHostWrapper$5;
.super Ljava/lang/Object;
.source "WebViewHostWrapper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->handleAction(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

.field final synthetic val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$5;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    iput-object p2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$5;->val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$5;->val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->getValue()Ljava/util/Map;

    move-result-object v0

    const-string v1, "eventType"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$5;->val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    invoke-virtual {v1}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->getValue()Ljava/util/Map;

    move-result-object v1

    const-string v2, "event"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 112
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$5;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    invoke-static {v2}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->access$300(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    return-void
.end method
