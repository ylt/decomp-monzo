.class Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;
.super Ljava/lang/Object;
.source "WebViewHostWrapper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->handleAction(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

.field final synthetic val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;Lio/intercom/android/sdk/conversation/WebViewConversationAction;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;->this$0:Lio/intercom/android/sdk/conversation/WebViewHostWrapper;

    iput-object p2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;->val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    iput-object p3, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 77
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;->val$action:Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->getValue()Ljava/util/Map;

    move-result-object v0

    const-string v1, "src"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 78
    if-eqz v0, :cond_0

    .line 79
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;->val$context:Landroid/content/Context;

    invoke-static {v2, v0, v3, v3, v3}, Lio/intercom/android/sdk/lightbox/LightBoxActivity;->imageIntent(Landroid/content/Context;Ljava/lang/String;ZII)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 82
    :cond_0
    return-void
.end method
