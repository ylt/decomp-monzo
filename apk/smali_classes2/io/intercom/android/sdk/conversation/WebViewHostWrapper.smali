.class public Lio/intercom/android/sdk/conversation/WebViewHostWrapper;
.super Ljava/lang/Object;
.source "WebViewHostWrapper.java"


# static fields
.field private static final ARTICLE_CARD_CLICKED:Ljava/lang/String; = "ARTICLE_CARD_CLICKED"

.field private static final CONVERSATION_CHANGED:Ljava/lang/String; = "CONVERSATION_CHANGED"

.field private static final IMAGE_CLICKED:Ljava/lang/String; = "IMAGE_CLICKED"

.field private static final OPS_METRIC_EVENT:Ljava/lang/String; = "OPS_METRIC_EVENT"

.field private static final POST_CARD_CLICKED:Ljava/lang/String; = "POST_CARD_CLICKED"


# instance fields
.field private final gson:Lio/intercom/com/google/gson/e;

.field private final host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

.field private final opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

.field private final parts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;"
        }
    .end annotation
.end field

.field private final store:Lio/intercom/android/sdk/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;"
        }
    .end annotation
.end field

.field private final webView:Landroid/webkit/WebView;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Landroid/webkit/WebView;Lio/intercom/com/google/gson/e;Lio/intercom/android/sdk/store/Store;Ljava/util/List;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;",
            "Landroid/webkit/WebView;",
            "Lio/intercom/com/google/gson/e;",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;",
            "Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    .line 38
    iput-object p2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->webView:Landroid/webkit/WebView;

    .line 39
    iput-object p3, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->gson:Lio/intercom/com/google/gson/e;

    .line 40
    iput-object p4, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->store:Lio/intercom/android/sdk/store/Store;

    .line 41
    iput-object p5, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->parts:Ljava/util/List;

    .line 42
    iput-object p6, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    return-object v0
.end method

.method static synthetic access$100(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/store/Store;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->store:Lio/intercom/android/sdk/store/Store;

    return-object v0
.end method

.method static synthetic access$200(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$300(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    return-object v0
.end method


# virtual methods
.method public handleAction(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->gson:Lio/intercom/com/google/gson/e;

    const-class v1, Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    invoke-virtual {v0, p1, v1}, Lio/intercom/com/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/conversation/WebViewConversationAction;

    .line 49
    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->getType()Ljava/lang/String;

    move-result-object v2

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 122
    :goto_1
    return-void

    .line 49
    :sswitch_0
    const-string v3, "CONVERSATION_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "IMAGE_CLICKED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "ARTICLE_CARD_CLICKED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "POST_CARD_CLICKED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "OPS_METRIC_EVENT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    .line 52
    :pswitch_0
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->gson:Lio/intercom/com/google/gson/e;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/WebViewConversationAction;->getValue()Ljava/util/Map;

    move-result-object v0

    const-string v2, "conversation"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/intercom/com/google/gson/e;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->gson:Lio/intercom/com/google/gson/e;

    const-class v2, Lio/intercom/android/sdk/models/Conversation$Builder;

    invoke-virtual {v1, v0, v2}, Lio/intercom/com/google/gson/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Conversation$Builder;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation$Builder;->build()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->parts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 55
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->parts:Ljava/util/List;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 56
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->webView:Landroid/webkit/WebView;

    new-instance v2, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;

    invoke-direct {v2, p0, v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$1;-><init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;Lio/intercom/android/sdk/models/Conversation;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 74
    :pswitch_1
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 75
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->webView:Landroid/webkit/WebView;

    new-instance v3, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;

    invoke-direct {v3, p0, v0, v1}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$2;-><init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;Lio/intercom/android/sdk/conversation/WebViewConversationAction;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 87
    :pswitch_2
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->webView:Landroid/webkit/WebView;

    new-instance v2, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$3;

    invoke-direct {v2, p0, v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$3;-><init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 100
    :pswitch_3
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->webView:Landroid/webkit/WebView;

    new-instance v1, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$4;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$4;-><init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 108
    :pswitch_4
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/WebViewHostWrapper;->webView:Landroid/webkit/WebView;

    new-instance v2, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$5;

    invoke-direct {v2, p0, v0}, Lio/intercom/android/sdk/conversation/WebViewHostWrapper$5;-><init>(Lio/intercom/android/sdk/conversation/WebViewHostWrapper;Lio/intercom/android/sdk/conversation/WebViewConversationAction;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 49
    :sswitch_data_0
    .sparse-switch
        -0x5c43c848 -> :sswitch_4
        -0x57663d5f -> :sswitch_2
        -0x3cacf6fd -> :sswitch_1
        -0x1e5bd928 -> :sswitch_0
        -0xf74fce9 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
