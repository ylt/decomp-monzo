.class Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$1;
.super Landroid/support/v7/widget/RecyclerView$m;
.source "NativeConversationContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->setup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$m;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 2

    .prologue
    .line 184
    if-nez p2, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isAtBottom()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->access$000(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    :cond_0
    return-void
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-static {v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->access$100(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$1;->this$0:Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;->updateIntercomLink(Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;)V

    .line 192
    return-void
.end method
