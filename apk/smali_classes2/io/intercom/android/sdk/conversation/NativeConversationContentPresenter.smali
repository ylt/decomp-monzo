.class Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;
.super Ljava/lang/Object;
.source "NativeConversationContentPresenter.java"

# interfaces
.implements Lio/intercom/android/sdk/conversation/ConversationContentPresenter;
.implements Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;


# static fields
.field private static final IMAGE_MIME_TYPE:Ljava/lang/String; = "image"

.field private static final PART_DISPLAY_DELIVERED_TIMEOUT:J

.field static final TIMESTAMP_UPDATE_PERIOD:J


# instance fields
.field private final adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

.field private final api:Lio/intercom/android/sdk/api/Api;

.field private final blocks:Lio/intercom/android/sdk/blocks/Blocks;

.field private final blocksAdminViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

.field private final blocksAnnouncementViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

.field private final blocksConversationRatingViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

.field private final blocksLinkListHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

.field private final blocksLinkViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

.field private final blocksPreviewViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

.field private final blocksUserViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

.field private conversationId:Ljava/lang/String;

.field private final conversationList:Landroid/support/v7/widget/RecyclerView;

.field private final host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

.field private final intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

.field private final isTypingViews:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/intercom/android/sdk/views/AdminIsTypingView;",
            ">;"
        }
    .end annotation
.end field

.field private final layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

.field private final loadingPart:Lio/intercom/android/sdk/models/Part;

.field private final opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

.field private final parts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;"
        }
    .end annotation
.end field

.field private final pill:Landroid/widget/TextView;

.field private replyDeliveredUpdater:Ljava/lang/Runnable;

.field final sendingParts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;"
        }
    .end annotation
.end field

.field private final soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

.field private final timestampAdder:Lio/intercom/android/sdk/blocks/function/TimestampAdder;

.field private final timestampUpdater:Ljava/lang/Runnable;

.field private final uploadingImageCache:Lio/intercom/android/sdk/blocks/UploadingImageCache;

.field private final userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 77
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->PART_DISPLAY_DELIVERED_TIMEOUT:J

    .line 78
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->TIMESTAMP_UPDATE_PERIOD:J

    return-void
.end method

.method constructor <init>(Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Landroid/support/v7/widget/RecyclerView;Lio/intercom/android/sdk/conversation/ConversationPartAdapter;Landroid/support/v7/widget/LinearLayoutManager;Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;Landroid/widget/TextView;Ljava/util/List;Lio/intercom/android/sdk/conversation/SoundPlayer;Lio/intercom/android/sdk/blocks/Blocks;Lio/intercom/android/sdk/blocks/ViewHolderGenerator;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/api/Api;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;Lio/intercom/android/sdk/blocks/UploadingImageCache;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;",
            "Landroid/support/v7/widget/RecyclerView;",
            "Lio/intercom/android/sdk/conversation/ConversationPartAdapter;",
            "Landroid/support/v7/widget/LinearLayoutManager;",
            "Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;",
            "Landroid/widget/TextView;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;",
            "Lio/intercom/android/sdk/conversation/SoundPlayer;",
            "Lio/intercom/android/sdk/blocks/Blocks;",
            "Lio/intercom/android/sdk/blocks/ViewHolderGenerator;",
            "Lio/intercom/android/sdk/identity/UserIdentity;",
            "Lio/intercom/android/sdk/api/Api;",
            "Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;",
            "Lio/intercom/android/sdk/blocks/UploadingImageCache;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v1, Lio/intercom/android/sdk/models/Part$Builder;

    invoke-direct {v1}, Lio/intercom/android/sdk/models/Part$Builder;-><init>()V

    const-string v2, "loading_layout_style"

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/models/Part$Builder;->withStyle(Ljava/lang/String;)Lio/intercom/android/sdk/models/Part$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Part$Builder;->build()Lio/intercom/android/sdk/models/Part;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->loadingPart:Lio/intercom/android/sdk/models/Part;

    .line 81
    invoke-static {}, Lio/intercom/android/sdk/blocks/function/TimestampAdder;->create()Lio/intercom/android/sdk/blocks/function/TimestampAdder;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->timestampAdder:Lio/intercom/android/sdk/blocks/function/TimestampAdder;

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->sendingParts:Ljava/util/List;

    .line 83
    new-instance v1, Landroid/support/v4/g/a;

    invoke-direct {v1}, Landroid/support/v4/g/a;-><init>()V

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isTypingViews:Ljava/util/Map;

    .line 714
    new-instance v1, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$10;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$10;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->timestampUpdater:Ljava/lang/Runnable;

    .line 150
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    .line 151
    iput-object p2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    .line 152
    iput-object p3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    .line 153
    iput-object p4, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    .line 154
    iput-object p5, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    .line 155
    iput-object p6, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->pill:Landroid/widget/TextView;

    .line 156
    iput-object p7, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    .line 157
    iput-object p8, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    .line 158
    iput-object p9, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocks:Lio/intercom/android/sdk/blocks/Blocks;

    .line 159
    iput-object p11, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 160
    iput-object p12, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->api:Lio/intercom/android/sdk/api/Api;

    .line 161
    move-object/from16 v0, p13

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    .line 162
    move-object/from16 v0, p14

    iput-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->uploadingImageCache:Lio/intercom/android/sdk/blocks/UploadingImageCache;

    .line 164
    invoke-virtual {p10}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->getUserHolder()Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksUserViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 165
    invoke-virtual {p10}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->getContainerCardHolder()Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksAnnouncementViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 166
    invoke-virtual {p10}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->getAdminHolder()Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksAdminViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 167
    invoke-virtual {p10}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->getLinkHolder()Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksLinkViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 168
    invoke-virtual {p10}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->getConversationRatingHolder()Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksConversationRatingViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 169
    invoke-virtual {p10}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->getPreviewHolder()Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksPreviewViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 170
    invoke-virtual {p10}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;->getLinkListHolder()Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksLinkListHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    .line 171
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->pill:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->smoothScrollToBottom()V

    return-void
.end method

.method static synthetic access$300(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Lio/intercom/android/sdk/api/Api;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->api:Lio/intercom/android/sdk/api/Api;

    return-object v0
.end method

.method static synthetic access$400(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Lio/intercom/android/sdk/conversation/ConversationPartAdapter;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    return-object v0
.end method

.method static synthetic access$600(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method private addSendingPart(Ljava/util/List;)Lio/intercom/android/sdk/models/Part;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;)",
            "Lio/intercom/android/sdk/models/Part;"
        }
    .end annotation

    .prologue
    .line 524
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->createSendingPart(Ljava/util/List;)Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    .line 525
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->sendingParts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 526
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->displaySendingPart(Lio/intercom/android/sdk/models/Part;)V

    .line 527
    return-object v0
.end method

.method private addViewForPart(Lio/intercom/android/sdk/models/Part;)V
    .locals 6

    .prologue
    .line 341
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getBlocks()Ljava/util/List;

    move-result-object v1

    .line 344
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getAttachments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 345
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 346
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getAttachments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Attachments;

    .line 347
    new-instance v4, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    invoke-direct {v4}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;-><init>()V

    .line 348
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Attachments;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->withName(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v4

    .line 349
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Attachments;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->withUrl(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v4

    .line 350
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Attachments;->getContentType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->withContentType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v0

    .line 351
    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->build()Lio/intercom/android/sdk/blocks/models/BlockAttachment;

    move-result-object v0

    .line 347
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 353
    :cond_0
    new-instance v0, Lio/intercom/android/sdk/blocks/models/Block$Builder;

    invoke-direct {v0}, Lio/intercom/android/sdk/blocks/models/Block$Builder;-><init>()V

    sget-object v3, Lio/intercom/android/sdk/blocks/BlockType;->ATTACHMENTLIST:Lio/intercom/android/sdk/blocks/BlockType;

    .line 354
    invoke-virtual {v3}, Lio/intercom/android/sdk/blocks/BlockType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v0

    .line 355
    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withAttachments(Ljava/util/List;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v0

    .line 356
    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->build()Lio/intercom/android/sdk/blocks/models/Block;

    move-result-object v0

    .line 353
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    :cond_1
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getParticipant()Lio/intercom/android/sdk/models/Participant;

    move-result-object v0

    .line 361
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v2}, Lio/intercom/android/sdk/identity/UserIdentity;->getIntercomId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/models/Participant;->isUserWithId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocks:Lio/intercom/android/sdk/blocks/Blocks;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksUserViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    invoke-virtual {v2, v1, v3}, Lio/intercom/android/sdk/blocks/Blocks;->createBlocks(Ljava/util/List;Lio/intercom/android/sdk/blocks/BlocksViewHolder;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->setViewForPart(Lio/intercom/android/sdk/models/Part;Landroid/view/ViewGroup;)V

    .line 376
    :goto_1
    return-void

    .line 363
    :cond_2
    const-string v0, "post"

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "note"

    .line 364
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365
    :cond_3
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocks:Lio/intercom/android/sdk/blocks/Blocks;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksAnnouncementViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    invoke-virtual {v2, v1, v3}, Lio/intercom/android/sdk/blocks/Blocks;->createBlocks(Ljava/util/List;Lio/intercom/android/sdk/blocks/BlocksViewHolder;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->setViewForPart(Lio/intercom/android/sdk/models/Part;Landroid/view/ViewGroup;)V

    goto :goto_1

    .line 366
    :cond_4
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->isLinkCard()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 367
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocks:Lio/intercom/android/sdk/blocks/Blocks;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksLinkViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    invoke-virtual {v2, v1, v3}, Lio/intercom/android/sdk/blocks/Blocks;->createBlocks(Ljava/util/List;Lio/intercom/android/sdk/blocks/BlocksViewHolder;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->setViewForPart(Lio/intercom/android/sdk/models/Part;Landroid/view/ViewGroup;)V

    goto :goto_1

    .line 368
    :cond_5
    sget-object v0, Lio/intercom/android/sdk/blocks/BlockType;->CONVERSATIONRATING:Lio/intercom/android/sdk/blocks/BlockType;

    invoke-virtual {p1, v0}, Lio/intercom/android/sdk/models/Part;->isSingleBlockPartOfType(Lio/intercom/android/sdk/blocks/BlockType;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 369
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocks:Lio/intercom/android/sdk/blocks/Blocks;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksConversationRatingViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    invoke-virtual {v2, v1, v3}, Lio/intercom/android/sdk/blocks/Blocks;->createBlocks(Ljava/util/List;Lio/intercom/android/sdk/blocks/BlocksViewHolder;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->setViewForPart(Lio/intercom/android/sdk/models/Part;Landroid/view/ViewGroup;)V

    goto :goto_1

    .line 370
    :cond_6
    sget-object v0, Lio/intercom/android/sdk/blocks/BlockType;->LINKLIST:Lio/intercom/android/sdk/blocks/BlockType;

    invoke-virtual {p1, v0}, Lio/intercom/android/sdk/models/Part;->isSingleBlockPartOfType(Lio/intercom/android/sdk/blocks/BlockType;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 371
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocks:Lio/intercom/android/sdk/blocks/Blocks;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksLinkListHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    invoke-virtual {v2, v1, v3}, Lio/intercom/android/sdk/blocks/Blocks;->createBlocks(Ljava/util/List;Lio/intercom/android/sdk/blocks/BlocksViewHolder;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->setViewForPart(Lio/intercom/android/sdk/models/Part;Landroid/view/ViewGroup;)V

    goto :goto_1

    .line 374
    :cond_7
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocks:Lio/intercom/android/sdk/blocks/Blocks;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksAdminViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    invoke-virtual {v2, v1, v3}, Lio/intercom/android/sdk/blocks/Blocks;->createBlocks(Ljava/util/List;Lio/intercom/android/sdk/blocks/BlocksViewHolder;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->setViewForPart(Lio/intercom/android/sdk/models/Part;Landroid/view/ViewGroup;)V

    goto :goto_1
.end method

.method static create(Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Landroid/support/v7/widget/RecyclerView;Lio/intercom/android/sdk/conversation/ConversationPartAdapter;Lio/intercom/android/sdk/views/IntercomLinkView;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/api/Api;Ljava/util/List;Ljava/lang/String;Landroid/widget/TextView;Lio/intercom/android/sdk/conversation/SoundPlayer;Lio/intercom/android/sdk/blocks/Blocks;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;Lio/intercom/com/bumptech/glide/i;)Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;",
            "Landroid/support/v7/widget/RecyclerView;",
            "Lio/intercom/android/sdk/conversation/ConversationPartAdapter;",
            "Lio/intercom/android/sdk/views/IntercomLinkView;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Lio/intercom/android/sdk/api/Api;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Part;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/widget/TextView;",
            "Lio/intercom/android/sdk/conversation/SoundPlayer;",
            "Lio/intercom/android/sdk/blocks/Blocks;",
            "Lio/intercom/android/sdk/identity/UserIdentity;",
            "Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;",
            "Lio/intercom/com/bumptech/glide/i;",
            ")",
            "Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;"
        }
    .end annotation

    .prologue
    .line 124
    new-instance v10, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v10, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 125
    new-instance v1, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;-><init>(Lio/intercom/android/sdk/views/IntercomLinkView;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/api/Api;Ljava/util/List;Ljava/lang/String;)V

    .line 127
    new-instance v3, Lio/intercom/android/sdk/blocks/UploadingImageCache;

    invoke-direct {v3}, Lio/intercom/android/sdk/blocks/UploadingImageCache;-><init>()V

    .line 129
    new-instance v2, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;

    new-instance v7, Lio/intercom/android/sdk/blocks/LightboxOpeningImageClickListener;

    move-object/from16 v0, p5

    invoke-direct {v7, v0}, Lio/intercom/android/sdk/blocks/LightboxOpeningImageClickListener;-><init>(Lio/intercom/android/sdk/api/Api;)V

    new-instance v8, Lio/intercom/android/sdk/blocks/LinkOpeningButtonClickListener;

    move-object/from16 v0, p5

    invoke-direct {v8, v0}, Lio/intercom/android/sdk/blocks/LinkOpeningButtonClickListener;-><init>(Lio/intercom/android/sdk/api/Api;)V

    move-object/from16 v4, p5

    move-object/from16 v5, p4

    move-object/from16 v6, p7

    move-object/from16 v9, p13

    invoke-direct/range {v2 .. v9}, Lio/intercom/android/sdk/blocks/ViewHolderGenerator;-><init>(Lio/intercom/android/sdk/blocks/UploadingImageCache;Lio/intercom/android/sdk/api/Api;Lio/intercom/android/sdk/Provider;Ljava/lang/String;Lio/intercom/android/sdk/blocks/ImageClickListener;Lio/intercom/android/sdk/blocks/ButtonClickListener;Lio/intercom/com/bumptech/glide/i;)V

    .line 132
    new-instance v4, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object v8, v10

    move-object v9, v1

    move-object/from16 v10, p8

    move-object/from16 v11, p6

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object v14, v2

    move-object/from16 v15, p11

    move-object/from16 v16, p5

    move-object/from16 v17, p12

    move-object/from16 v18, v3

    invoke-direct/range {v4 .. v18}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;-><init>(Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;Landroid/support/v7/widget/RecyclerView;Lio/intercom/android/sdk/conversation/ConversationPartAdapter;Landroid/support/v7/widget/LinearLayoutManager;Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;Landroid/widget/TextView;Ljava/util/List;Lio/intercom/android/sdk/conversation/SoundPlayer;Lio/intercom/android/sdk/blocks/Blocks;Lio/intercom/android/sdk/blocks/ViewHolderGenerator;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/android/sdk/api/Api;Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;Lio/intercom/android/sdk/blocks/UploadingImageCache;)V

    return-object v4
.end method

.method private createBlocksForUpload(Lio/intercom/android/sdk/models/events/UploadEvent;Lio/intercom/android/sdk/blocks/models/Block;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/models/events/UploadEvent;",
            "Lio/intercom/android/sdk/blocks/models/Block;",
            ")",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->getUpload()Lio/intercom/android/sdk/models/Upload;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Upload;->getContentType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    invoke-virtual {p2}, Lio/intercom/android/sdk/blocks/models/Block;->toBuilder()Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v0

    sget-object v1, Lio/intercom/android/sdk/blocks/BlockType;->IMAGE:Lio/intercom/android/sdk/blocks/BlockType;

    .line 583
    invoke-virtual {v1}, Lio/intercom/android/sdk/blocks/BlockType;->getSerializedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v0

    .line 584
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->getUpload()Lio/intercom/android/sdk/models/Upload;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Upload;->getPublicUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withUrl(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v0

    .line 582
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 597
    :goto_0
    return-object v0

    .line 586
    :cond_0
    invoke-virtual {p2}, Lio/intercom/android/sdk/blocks/models/Block;->getAttachments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/blocks/models/BlockAttachment;

    .line 588
    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/BlockAttachment;->toBuilder()Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v0

    .line 589
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->getSize()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->withSize(J)Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v0

    .line 590
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->getUpload()Lio/intercom/android/sdk/models/Upload;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Upload;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->withId(I)Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;

    move-result-object v0

    .line 591
    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/BlockAttachment$Builder;->build()Lio/intercom/android/sdk/blocks/models/BlockAttachment;

    move-result-object v0

    .line 593
    invoke-virtual {p2}, Lio/intercom/android/sdk/blocks/models/Block;->toBuilder()Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v1

    .line 594
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withAttachments(Ljava/util/List;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v0

    sget-object v1, Lio/intercom/android/sdk/blocks/BlockType;->ATTACHMENTLIST:Lio/intercom/android/sdk/blocks/BlockType;

    .line 595
    invoke-virtual {v1}, Lio/intercom/android/sdk/blocks/BlockType;->getSerializedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->withType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v0

    .line 597
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private createSendingPart(Ljava/util/List;)Lio/intercom/android/sdk/models/Part;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;)",
            "Lio/intercom/android/sdk/models/Part;"
        }
    .end annotation

    .prologue
    .line 531
    new-instance v0, Lio/intercom/android/sdk/models/Part$Builder;

    invoke-direct {v0}, Lio/intercom/android/sdk/models/Part$Builder;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v2, Lio/intercom/android/sdk/commons/utilities/TimeProvider;->SYSTEM:Lio/intercom/android/sdk/commons/utilities/TimeProvider;

    .line 532
    invoke-interface {v2}, Lio/intercom/android/sdk/commons/utilities/TimeProvider;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/intercom/android/sdk/models/Part$Builder;->withCreatedAt(J)Lio/intercom/android/sdk/models/Part$Builder;

    move-result-object v0

    const-string v1, "chat"

    .line 533
    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Part$Builder;->withStyle(Ljava/lang/String;)Lio/intercom/android/sdk/models/Part$Builder;

    move-result-object v0

    .line 534
    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/models/Part$Builder;->withBlocks(Ljava/util/List;)Lio/intercom/android/sdk/models/Part$Builder;

    move-result-object v0

    .line 535
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part$Builder;->build()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    .line 536
    sget-object v1, Lio/intercom/android/sdk/models/Part$MessageState;->SENDING:Lio/intercom/android/sdk/models/Part$MessageState;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Part;->setMessageState(Lio/intercom/android/sdk/models/Part$MessageState;)V

    .line 537
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->getUserParticipant()Lio/intercom/android/sdk/models/Participant;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Part;->setParticipant(Lio/intercom/android/sdk/models/Participant;)V

    .line 538
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Part;->setEntranceAnimation(Z)V

    .line 539
    return-object v0
.end method

.method private displaySendingPart(Lio/intercom/android/sdk/models/Part;)V
    .locals 4

    .prologue
    .line 379
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/SoundPlayer;->playReplySentSound()V

    .line 380
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocks:Lio/intercom/android/sdk/blocks/Blocks;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getBlocks()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->blocksPreviewViewHolder:Lio/intercom/android/sdk/blocks/BlocksViewHolder;

    invoke-virtual {v1, v2, v3}, Lio/intercom/android/sdk/blocks/Blocks;->createBlocks(Ljava/util/List;Lio/intercom/android/sdk/blocks/BlocksViewHolder;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->setViewForPart(Lio/intercom/android/sdk/models/Part;Landroid/view/ViewGroup;)V

    .line 382
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 383
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->smoothScrollToBottom()V

    .line 384
    return-void
.end method

.method private hideLoadingIndicator()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->loadingPart:Lio/intercom/android/sdk/models/Part;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 288
    if-ltz v0, :cond_0

    .line 289
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 290
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyItemRemoved(I)V

    .line 292
    :cond_0
    return-void
.end method

.method private markAsFailed(ILjava/lang/String;Z)V
    .locals 3

    .prologue
    .line 313
    invoke-direct {p0, p1, p2}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->positionOfPart(ILjava/lang/String;)I

    move-result v0

    .line 314
    if-ltz v0, :cond_1

    .line 315
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 316
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->getViewForPart(Lio/intercom/android/sdk/models/Part;)Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 317
    instance-of v2, v1, Lio/intercom/android/sdk/conversation/UploadProgressListener;

    if-eqz v2, :cond_0

    .line 318
    check-cast v1, Lio/intercom/android/sdk/conversation/UploadProgressListener;

    .line 319
    invoke-interface {v1}, Lio/intercom/android/sdk/conversation/UploadProgressListener;->uploadStopped()V

    .line 321
    :cond_0
    if-eqz p3, :cond_2

    sget-object v1, Lio/intercom/android/sdk/models/Part$MessageState;->UPLOAD_FAILED:Lio/intercom/android/sdk/models/Part$MessageState;

    :goto_0
    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Part;->setMessageState(Lio/intercom/android/sdk/models/Part$MessageState;)V

    .line 322
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 324
    :cond_1
    return-void

    .line 321
    :cond_2
    sget-object v1, Lio/intercom/android/sdk/models/Part$MessageState;->FAILED:Lio/intercom/android/sdk/models/Part$MessageState;

    goto :goto_0
.end method

.method private positionOfPart(ILjava/lang/String;)I
    .locals 1

    .prologue
    .line 295
    if-ltz p1, :cond_2

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 296
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 297
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    :cond_0
    :goto_0
    return p1

    .line 302
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    :goto_1
    if-ltz p1, :cond_2

    .line 303
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    add-int/lit8 p1, p1, -0x1

    goto :goto_1

    .line 309
    :cond_2
    const/4 p1, -0x1

    goto :goto_0
.end method

.method private retryFailedUpload(Lio/intercom/android/sdk/models/Part;)V
    .locals 6

    .prologue
    .line 470
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->positionOfPart(ILjava/lang/String;)I

    move-result v2

    .line 472
    if-ltz v2, :cond_0

    .line 473
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->getViewForPart(Lio/intercom/android/sdk/models/Part;)Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/conversation/UploadProgressListener;

    .line 474
    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/UploadProgressListener;->uploadStarted()V

    move-object v5, v0

    .line 485
    :goto_0
    sget-object v0, Lio/intercom/android/sdk/models/Part$MessageState;->SENDING:Lio/intercom/android/sdk/models/Part$MessageState;

    invoke-virtual {p1, v0}, Lio/intercom/android/sdk/models/Part;->setMessageState(Lio/intercom/android/sdk/models/Part$MessageState;)V

    .line 486
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 487
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 488
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 490
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->api:Lio/intercom/android/sdk/api/Api;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getUpload()Lcom/intercom/input/gallery/c;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->uploadingImageCache:Lio/intercom/android/sdk/blocks/UploadingImageCache;

    invoke-virtual/range {v0 .. v5}, Lio/intercom/android/sdk/api/Api;->uploadFile(Lcom/intercom/input/gallery/c;ILjava/lang/String;Lio/intercom/android/sdk/blocks/UploadingImageCache;Lio/intercom/android/sdk/conversation/UploadProgressListener;)V

    .line 491
    return-void

    .line 476
    :cond_0
    new-instance v5, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$5;

    invoke-direct {v5, p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$5;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V

    goto :goto_0
.end method

.method private showLoadingIndicator()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 282
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->loadingPart:Lio/intercom/android/sdk/models/Part;

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 283
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyItemInserted(I)V

    .line 284
    return-void
.end method

.method private showRetryDialog(Lio/intercom/android/sdk/models/Part;)V
    .locals 3

    .prologue
    .line 494
    new-instance v0, Landroid/support/v7/app/d$a;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/d$a;-><init>(Landroid/content/Context;)V

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_inbox_error_state_title:I

    .line 495
    invoke-virtual {v0, v1}, Landroid/support/v7/app/d$a;->a(I)Landroid/support/v7/app/d$a;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_failed_delivery:I

    .line 496
    invoke-virtual {v0, v1}, Landroid/support/v7/app/d$a;->b(I)Landroid/support/v7/app/d$a;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_retry:I

    new-instance v2, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$6;

    invoke-direct {v2, p0, p1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$6;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;Lio/intercom/android/sdk/models/Part;)V

    .line 497
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/d$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 501
    invoke-virtual {v0}, Landroid/support/v7/app/d$a;->c()Landroid/support/v7/app/d;

    .line 502
    return-void
.end method

.method private smoothScrollToBottom()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->c(I)V

    .line 211
    return-void
.end method

.method private updateSendPartOpsMetric()V
    .locals 3

    .prologue
    .line 653
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "finish"

    const-string v2, "time-to-complete-request-send-part-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "start"

    const-string v2, "time-to-render-result-send-part-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$8;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$8;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 662
    return-void
.end method


# virtual methods
.method public addBottomPadding(I)V
    .locals 5

    .prologue
    .line 222
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    .line 223
    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    .line 224
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    .line 225
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    .line 226
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v4, p1

    .line 222
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 227
    return-void
.end method

.method public cleanup()V
    .locals 2

    .prologue
    .line 722
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->replyDeliveredUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 723
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->timestampUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 724
    return-void
.end method

.method public fetchConversation(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 447
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "start"

    const-string v2, "time-to-complete-request-load-conversation-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    iput-object p1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationId:Ljava/lang/String;

    .line 451
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$4;

    invoke-direct {v1, p0, p1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$4;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 458
    :cond_0
    return-void
.end method

.method getUserParticipant()Lio/intercom/android/sdk/models/Participant;
    .locals 3

    .prologue
    .line 543
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->getIntercomId()Ljava/lang/String;

    move-result-object v1

    .line 544
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;->getConversation()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v0

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Conversation;->getParticipant(Ljava/lang/String;)Lio/intercom/android/sdk/models/Participant;

    move-result-object v0

    .line 545
    sget-object v2, Lio/intercom/android/sdk/models/Participant;->NULL:Lio/intercom/android/sdk/models/Participant;

    if-ne v0, v2, :cond_0

    .line 546
    new-instance v0, Lio/intercom/android/sdk/models/Participant$Builder;

    invoke-direct {v0}, Lio/intercom/android/sdk/models/Participant$Builder;-><init>()V

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Participant$Builder;->withId(Ljava/lang/String;)Lio/intercom/android/sdk/models/Participant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Participant$Builder;->build()Lio/intercom/android/sdk/models/Participant;

    move-result-object v0

    .line 548
    :cond_0
    return-object v0
.end method

.method public isAtBottom()Z
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->q()I

    move-result v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutManager;->z()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newConversationFailure(Lio/intercom/android/sdk/models/events/failure/NewConversationFailedEvent;)V
    .locals 3
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 327
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/failure/NewConversationFailedEvent;->getPosition()I

    move-result v0

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/failure/NewConversationFailedEvent;->getPartId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->markAsFailed(ILjava/lang/String;Z)V

    .line 328
    return-void
.end method

.method public newConversationSuccess(Lio/intercom/android/sdk/models/events/NewConversationEvent;)V
    .locals 3
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 729
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/NewConversationEvent;->getPartId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 731
    invoke-virtual {p0, p1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->onNewConversation(Lio/intercom/android/sdk/models/events/NewConversationEvent;)V

    .line 732
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/NewConversationEvent;->getConversation()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/NewConversationEvent;->isAnnotatedImage()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;->onConversationCreated(Lio/intercom/android/sdk/models/Conversation;Z)V

    .line 734
    :cond_0
    return-void
.end method

.method public declared-synchronized onAdminStartedTyping(Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;)V
    .locals 7

    .prologue
    .line 387
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;->getAdminId()Ljava/lang/String;

    move-result-object v1

    .line 389
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isTypingViews:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isTypingViews:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/views/AdminIsTypingView;

    invoke-virtual {v0}, Lio/intercom/android/sdk/views/AdminIsTypingView;->renewTypingAnimation()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 394
    :cond_1
    :try_start_1
    new-instance v0, Lio/intercom/android/sdk/models/Part$Builder;

    invoke-direct {v0}, Lio/intercom/android/sdk/models/Part$Builder;-><init>()V

    const/4 v2, 0x1

    .line 395
    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/models/Part$Builder;->withParticipantIsAdmin(Z)Lio/intercom/android/sdk/models/Part$Builder;

    move-result-object v0

    const-string v2, "admin_is_typing_style"

    .line 396
    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/models/Part$Builder;->withStyle(Ljava/lang/String;)Lio/intercom/android/sdk/models/Part$Builder;

    move-result-object v0

    .line 397
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part$Builder;->build()Lio/intercom/android/sdk/models/Part;

    move-result-object v2

    .line 399
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->host:Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;

    invoke-interface {v0}, Lio/intercom/android/sdk/conversation/ConversationContentPresenter$Host;->getConversation()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v0

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Conversation;->getParticipant(Ljava/lang/String;)Lio/intercom/android/sdk/models/Participant;

    move-result-object v0

    .line 400
    sget-object v3, Lio/intercom/android/sdk/models/Participant;->NULL:Lio/intercom/android/sdk/models/Participant;

    if-ne v0, v3, :cond_2

    .line 401
    invoke-virtual {p1}, Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;->getAdminName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "admin"

    const-string v4, ""

    .line 402
    invoke-virtual {p1}, Lio/intercom/android/sdk/conversation/events/AdminIsTypingEvent;->getAdminAvatarUrl()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-static {v5, v6}, Lio/intercom/android/sdk/models/Avatar;->create(Ljava/lang/String;Ljava/lang/String;)Lio/intercom/android/sdk/models/Avatar;

    move-result-object v5

    .line 401
    invoke-static {v1, v0, v3, v4, v5}, Lio/intercom/android/sdk/models/Participant;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lio/intercom/android/sdk/models/Avatar;)Lio/intercom/android/sdk/models/Participant;

    move-result-object v0

    .line 405
    :cond_2
    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/models/Part;->setParticipant(Lio/intercom/android/sdk/models/Participant;)V

    .line 406
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/models/Part;->setEntranceAnimation(Z)V

    .line 408
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isAtBottom()Z

    move-result v3

    .line 410
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 413
    sget v0, Lio/intercom/android/sdk/R$layout;->intercom_blocks_admin_layout:I

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 414
    new-instance v5, Lio/intercom/android/sdk/views/AdminIsTypingView;

    invoke-direct {v5, v4}, Lio/intercom/android/sdk/views/AdminIsTypingView;-><init>(Landroid/content/Context;)V

    .line 415
    new-instance v4, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$3;

    invoke-direct {v4, p0, v1, v2}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$3;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;Ljava/lang/String;Lio/intercom/android/sdk/models/Part;)V

    invoke-virtual {v5, v4}, Lio/intercom/android/sdk/views/AdminIsTypingView;->setListener(Lio/intercom/android/sdk/views/AdminIsTypingView$Listener;)V

    .line 420
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 422
    iget-object v4, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isTypingViews:Ljava/util/Map;

    invoke-interface {v4, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->setViewForPart(Lio/intercom/android/sdk/models/Part;Landroid/view/ViewGroup;)V

    .line 425
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 427
    if-eqz v3, :cond_0

    .line 428
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->smoothScrollToBottom()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method onAdminStoppedTyping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isTypingViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0, p2}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->positionOfPart(ILjava/lang/String;)I

    move-result v0

    .line 439
    if-ltz v0, :cond_0

    .line 440
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 441
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 443
    :cond_0
    return-void
.end method

.method public onConversationFetched(Lio/intercom/android/sdk/models/events/ConversationEvent;)V
    .locals 4

    .prologue
    .line 665
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "finish"

    const-string v2, "time-to-complete-request-load-conversation-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "start"

    const-string v2, "time-to-render-result-load-conversation-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ConversationEvent;->getResponse()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v0

    .line 672
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 674
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 675
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->getViewForPart(Lio/intercom/android/sdk/models/Part;)Landroid/view/ViewGroup;

    move-result-object v2

    if-nez v2, :cond_0

    .line 676
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->addViewForPart(Lio/intercom/android/sdk/models/Part;)V

    .line 678
    :cond_0
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 681
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->sendingParts:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 682
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->timestampAdder:Lio/intercom/android/sdk/blocks/function/TimestampAdder;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/blocks/function/TimestampAdder;->addDayDividers(Ljava/util/List;)V

    .line 683
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 684
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->timestampUpdater:Ljava/lang/Runnable;

    sget-wide v2, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->TIMESTAMP_UPDATE_PERIOD:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 686
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$9;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$9;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 693
    return-void
.end method

.method public onGlobalLayout()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    invoke-virtual {v0, p0}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;->updateIntercomLink(Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;)V

    .line 244
    return-void
.end method

.method public onNewCommentEventReceived(Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;)V
    .locals 0

    .prologue
    .line 434
    return-void
.end method

.method public onNewConversation(Lio/intercom/android/sdk/models/events/NewConversationEvent;)V
    .locals 3

    .prologue
    .line 610
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->updateSendPartOpsMetric()V

    .line 612
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/NewConversationEvent;->getConversation()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v1

    .line 613
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/NewConversationEvent;->getConversation()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getParts()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 614
    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationId:Ljava/lang/String;

    .line 615
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 616
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->sendingParts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 618
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->addViewForPart(Lio/intercom/android/sdk/models/Part;)V

    .line 619
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 620
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 621
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/SoundPlayer;->playReplyDeliveredSound()V

    .line 623
    return-void
.end method

.method public onNewPartReceived()V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isTypingViews:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/views/AdminIsTypingView;

    .line 252
    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {v0}, Lio/intercom/android/sdk/views/AdminIsTypingView;->cancelTypingAnimation()V

    goto :goto_0

    .line 256
    :cond_1
    invoke-virtual {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->isAtBottom()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 257
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->smoothScrollToBottom()V

    .line 261
    :cond_2
    :goto_1
    return-void

    .line 258
    :cond_3
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->isAdmin()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 259
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->pill:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onPartClicked(Lio/intercom/android/sdk/models/Part;)V
    .locals 2

    .prologue
    .line 461
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getMessageState()Lio/intercom/android/sdk/models/Part$MessageState;

    move-result-object v0

    sget-object v1, Lio/intercom/android/sdk/models/Part$MessageState;->FAILED:Lio/intercom/android/sdk/models/Part$MessageState;

    if-ne v0, v1, :cond_1

    .line 462
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->showRetryDialog(Lio/intercom/android/sdk/models/Part;)V

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getMessageState()Lio/intercom/android/sdk/models/Part$MessageState;

    move-result-object v0

    sget-object v1, Lio/intercom/android/sdk/models/Part$MessageState;->UPLOAD_FAILED:Lio/intercom/android/sdk/models/Part$MessageState;

    if-ne v0, v1, :cond_0

    .line 464
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->retryFailedUpload(Lio/intercom/android/sdk/models/Part;)V

    goto :goto_0
.end method

.method public onProfileScrolled()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 236
    if-lez v0, :cond_0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    .line 237
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 239
    :goto_0
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;->onProfileScrolled(Landroid/view/View;)V

    .line 240
    return-void

    .line 237
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReplyDelivered(Lio/intercom/android/sdk/models/events/ReplyEvent;)V
    .locals 4

    .prologue
    .line 626
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->updateSendPartOpsMetric()V

    .line 628
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ReplyEvent;->getResponse()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    .line 630
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ReplyEvent;->getPosition()I

    move-result v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/ReplyEvent;->getPartId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->positionOfPart(ILjava/lang/String;)I

    move-result v1

    .line 631
    if-ltz v1, :cond_0

    .line 632
    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->sendingParts:Ljava/util/List;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 635
    :cond_0
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->addViewForPart(Lio/intercom/android/sdk/models/Part;)V

    .line 636
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Part;->setDisplayDelivered(Z)V

    .line 637
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 638
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 640
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    invoke-virtual {v1}, Lio/intercom/android/sdk/conversation/SoundPlayer;->playReplyDeliveredSound()V

    .line 641
    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->replyDeliveredUpdater:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 642
    new-instance v1, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$7;

    invoke-direct {v1, p0, v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$7;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;Lio/intercom/android/sdk/models/Part;)V

    iput-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->replyDeliveredUpdater:Ljava/lang/Runnable;

    .line 648
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->replyDeliveredUpdater:Ljava/lang/Runnable;

    sget-wide v2, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->PART_DISPLAY_DELIVERED_TIMEOUT:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 650
    return-void
.end method

.method public onUserContentSeenByAdmin(Lio/intercom/android/sdk/models/events/realtime/UserContentSeenByAdminEvent;)V
    .locals 4
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 697
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/realtime/UserContentSeenByAdminEvent;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 699
    const/4 v1, 0x0

    .line 700
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_1

    .line 701
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Part;

    .line 702
    if-nez v1, :cond_0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->isAdmin()Z

    move-result v3

    if-nez v3, :cond_0

    .line 703
    const-string v1, "seen"

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/models/Part;->setSeenByAdmin(Ljava/lang/String;)V

    .line 704
    const/4 v0, 0x1

    .line 700
    :goto_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 706
    :cond_0
    const-string v3, "hide"

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/models/Part;->setSeenByAdmin(Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    .line 709
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 711
    :cond_2
    return-void
.end method

.method public replyFailure(Lio/intercom/android/sdk/models/events/failure/ReplyFailedEvent;)V
    .locals 3
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 331
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/failure/ReplyFailedEvent;->getPosition()I

    move-result v0

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/failure/ReplyFailedEvent;->getPartId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/failure/ReplyFailedEvent;->isUpload()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->markAsFailed(ILjava/lang/String;Z)V

    .line 332
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/SoundPlayer;->playReplyFailedSound()V

    .line 333
    return-void
.end method

.method retryTapped(Lio/intercom/android/sdk/models/Part;)V
    .locals 5

    .prologue
    .line 505
    sget-object v0, Lio/intercom/android/sdk/models/Part$MessageState;->SENDING:Lio/intercom/android/sdk/models/Part$MessageState;

    invoke-virtual {p1, v0}, Lio/intercom/android/sdk/models/Part;->setMessageState(Lio/intercom/android/sdk/models/Part$MessageState;)V

    .line 507
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 508
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 509
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->notifyDataSetChanged()V

    .line 511
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Part;->getBlocks()Ljava/util/List;

    move-result-object v2

    .line 514
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 515
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 516
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 517
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/blocks/models/Block;

    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/Block;->toBuilder()Lio/intercom/android/sdk/blocks/models/Block$Builder;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 516
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 520
    :cond_0
    invoke-virtual {p0, v4}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->sendPart(Ljava/util/List;)V

    .line 521
    return-void
.end method

.method public scrollToBottom()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 219
    return-void
.end method

.method public scrollToTop()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 215
    return-void
.end method

.method public sendPart(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 552
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "finish"

    const-string v2, "time-to-process-action-send-part-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "start"

    const-string v2, "time-to-complete-request-send-part-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->addSendingPart(Ljava/util/List;)Lio/intercom/android/sdk/models/Part;

    move-result-object v2

    .line 555
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->api:Lio/intercom/android/sdk/api/Api;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2, v5}, Lio/intercom/android/sdk/api/Api;->startNewConversation(Ljava/util/List;ILjava/lang/String;Z)V

    .line 560
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->api:Lio/intercom/android/sdk/api/Api;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationId:Ljava/lang/String;

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lio/intercom/android/sdk/api/Api;->replyToConversation(Ljava/lang/String;Ljava/util/List;ILjava/lang/String;ZZ)V

    goto :goto_0
.end method

.method public setup()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 174
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->opsMetricTracker:Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;

    const-string v1, "finish"

    const-string v2, "time-to-process-action-load-conversation-ms"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/metrics/ops/OpsMetricTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 176
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 177
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lio/intercom/android/sdk/views/decoration/ConversationItemDecoration;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lio/intercom/android/sdk/views/decoration/ConversationItemDecoration;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 178
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/RecyclerView$e;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bi;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/bi;->a(Z)V

    .line 179
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 180
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$1;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$1;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$m;)V

    .line 195
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->intercomLinkPresenter:Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;

    invoke-virtual {v0, p0}, Lio/intercom/android/sdk/conversation/IntercomLinkPresenter;->setup(Lio/intercom/android/sdk/conversation/IntercomLinkPresenter$IntercomLinkHost;)V

    .line 197
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->pill:Landroid/widget/TextView;

    new-instance v1, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$2;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter$2;-><init>(Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    return-void
.end method

.method public showContentView()V
    .locals 2

    .prologue
    .line 276
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->hideLoadingIndicator()V

    .line 277
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->pill:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 279
    return-void
.end method

.method public showErrorView()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 264
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->hideLoadingIndicator()V

    .line 265
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->pill:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 267
    return-void
.end method

.method public showLoadingView()V
    .locals 2

    .prologue
    .line 270
    invoke-direct {p0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->showLoadingIndicator()V

    .line 271
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->pill:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 273
    return-void
.end method

.method public smoothScrollToTop()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationList:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->c(I)V

    .line 207
    return-void
.end method

.method public uploadFailure(Lio/intercom/android/sdk/models/events/failure/UploadFailedEvent;)V
    .locals 3
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 336
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/failure/UploadFailedEvent;->getPosition()I

    move-result v0

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/failure/UploadFailedEvent;->getPartId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->markAsFailed(ILjava/lang/String;Z)V

    .line 337
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->soundPlayer:Lio/intercom/android/sdk/conversation/SoundPlayer;

    invoke-virtual {v0}, Lio/intercom/android/sdk/conversation/SoundPlayer;->playReplyFailedSound()V

    .line 338
    return-void
.end method

.method public uploadImage(Ljava/util/List;Lcom/intercom/input/gallery/c;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;",
            "Lcom/intercom/input/gallery/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 603
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->addSendingPart(Ljava/util/List;)Lio/intercom/android/sdk/models/Part;

    move-result-object v1

    .line 604
    invoke-virtual {v1, p2}, Lio/intercom/android/sdk/models/Part;->setUpload(Lcom/intercom/input/gallery/c;)V

    .line 605
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->api:Lio/intercom/android/sdk/api/Api;

    iget-object v2, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->uploadingImageCache:Lio/intercom/android/sdk/blocks/UploadingImageCache;

    iget-object v5, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->adapter:Lio/intercom/android/sdk/conversation/ConversationPartAdapter;

    .line 606
    invoke-virtual {v5, v1}, Lio/intercom/android/sdk/conversation/ConversationPartAdapter;->getViewForPart(Lio/intercom/android/sdk/models/Part;)Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lio/intercom/android/sdk/conversation/UploadProgressListener;

    move-object v1, p2

    .line 605
    invoke-virtual/range {v0 .. v5}, Lio/intercom/android/sdk/api/Api;->uploadFile(Lcom/intercom/input/gallery/c;ILjava/lang/String;Lio/intercom/android/sdk/blocks/UploadingImageCache;Lio/intercom/android/sdk/conversation/UploadProgressListener;)V

    .line 607
    return-void
.end method

.method public uploadSuccess(Lio/intercom/android/sdk/models/events/UploadEvent;)V
    .locals 7
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 563
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->getTempPartPosition()I

    move-result v0

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->getTempPartId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->positionOfPart(ILjava/lang/String;)I

    move-result v3

    .line 564
    if-gez v3, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lio/intercom/android/sdk/models/Part;

    .line 568
    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->getBlocks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 571
    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->getBlocks()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/blocks/models/Block;

    invoke-direct {p0, p1, v0}, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->createBlocksForUpload(Lio/intercom/android/sdk/models/events/UploadEvent;Lio/intercom/android/sdk/blocks/models/Block;)Ljava/util/List;

    move-result-object v2

    .line 573
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 574
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->api:Lio/intercom/android/sdk/api/Api;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->parts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->getTempPartId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->isAnnotatedImage()Z

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Lio/intercom/android/sdk/api/Api;->startNewConversation(Ljava/util/List;ILjava/lang/String;Z)V

    goto :goto_0

    .line 576
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->api:Lio/intercom/android/sdk/api/Api;

    iget-object v1, p0, Lio/intercom/android/sdk/conversation/NativeConversationContentPresenter;->conversationId:Ljava/lang/String;

    invoke-virtual {v4}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/events/UploadEvent;->isAnnotatedImage()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lio/intercom/android/sdk/api/Api;->replyToConversation(Ljava/lang/String;Ljava/util/List;ILjava/lang/String;ZZ)V

    goto :goto_0
.end method
