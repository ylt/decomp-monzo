.class public Lio/intercom/android/sdk/UserAttributes;
.super Ljava/lang/Object;
.source "UserAttributes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/UserAttributes$Builder;
    }
.end annotation


# static fields
.field private static final COMPANIES:Ljava/lang/String; = "companies"

.field private static final CUSTOM_ATTRIBUTES:Ljava/lang/String; = "custom_attributes"

.field private static final EMAIL:Ljava/lang/String; = "email"

.field private static final LANGUAGE_OVERRIDE:Ljava/lang/String; = "language_override"

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final PHONE:Ljava/lang/String; = "phone"

.field private static final SIGNED_UP_AT:Ljava/lang/String; = "signed_up_at"

.field private static final TWIG:Lio/intercom/android/sdk/twig/Twig;

.field private static final UNSUBSCRIBED_FROM_EMAILS:Ljava/lang/String; = "unsubscribed_from_emails"

.field private static final USER_ID:Ljava/lang/String; = "user_id"


# instance fields
.field private final attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final companies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final customAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    sput-object v0, Lio/intercom/android/sdk/UserAttributes;->TWIG:Lio/intercom/android/sdk/twig/Twig;

    return-void
.end method

.method constructor <init>(Lio/intercom/android/sdk/UserAttributes$Builder;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iget-object v0, p1, Lio/intercom/android/sdk/UserAttributes$Builder;->attributes:Ljava/util/Map;

    iput-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->attributes:Ljava/util/Map;

    .line 63
    iget-object v0, p1, Lio/intercom/android/sdk/UserAttributes$Builder;->customAttributes:Ljava/util/Map;

    iput-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->customAttributes:Ljava/util/Map;

    .line 64
    iget-object v0, p1, Lio/intercom/android/sdk/UserAttributes$Builder;->companies:Ljava/util/List;

    iput-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->companies:Ljava/util/List;

    .line 65
    return-void
.end method

.method static synthetic access$000()Lio/intercom/android/sdk/twig/Twig;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lio/intercom/android/sdk/UserAttributes;->TWIG:Lio/intercom/android/sdk/twig/Twig;

    return-object v0
.end method


# virtual methods
.method public isEmpty()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->attributes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->customAttributes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->companies:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->customAttributes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->attributes:Ljava/util/Map;

    const-string v1, "custom_attributes"

    iget-object v2, p0, Lio/intercom/android/sdk/UserAttributes;->customAttributes:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->companies:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    iget-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->attributes:Ljava/util/Map;

    const-string v1, "companies"

    iget-object v2, p0, Lio/intercom/android/sdk/UserAttributes;->companies:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/UserAttributes;->attributes:Ljava/util/Map;

    return-object v0
.end method
