.class Lio/intercom/android/sdk/blocks/NetworkImage;
.super Lio/intercom/android/sdk/blocks/Image;
.source "NetworkImage.java"

# interfaces
.implements Lio/intercom/android/sdk/blocks/blockInterfaces/ImageBlock;


# static fields
.field private static final PROGRESSBAR_DIAMETER_DP:I = 0x28


# instance fields
.field private final appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final listener:Lio/intercom/android/sdk/blocks/ImageClickListener;

.field private final requestManager:Lio/intercom/com/bumptech/glide/i;

.field private final twig:Lio/intercom/android/sdk/twig/Twig;

.field private final uploadingImageCache:Lio/intercom/android/sdk/blocks/UploadingImageCache;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/blocks/StyleType;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/blocks/UploadingImageCache;Lio/intercom/android/sdk/blocks/ImageClickListener;Lio/intercom/com/bumptech/glide/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/blocks/StyleType;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Lio/intercom/android/sdk/blocks/UploadingImageCache;",
            "Lio/intercom/android/sdk/blocks/ImageClickListener;",
            "Lio/intercom/com/bumptech/glide/i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/blocks/Image;-><init>(Lio/intercom/android/sdk/blocks/StyleType;)V

    .line 59
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 64
    iput-object p2, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 65
    iput-object p3, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->uploadingImageCache:Lio/intercom/android/sdk/blocks/UploadingImageCache;

    .line 66
    iput-object p4, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->listener:Lio/intercom/android/sdk/blocks/ImageClickListener;

    .line 67
    iput-object p5, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/blocks/NetworkImage;)Lio/intercom/android/sdk/twig/Twig;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->twig:Lio/intercom/android/sdk/twig/Twig;

    return-object v0
.end method

.method static synthetic access$100(Lio/intercom/android/sdk/blocks/NetworkImage;)Lio/intercom/android/sdk/blocks/ImageClickListener;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->listener:Lio/intercom/android/sdk/blocks/ImageClickListener;

    return-object v0
.end method

.method private loadImageFromUrl(Ljava/lang/String;Ljava/lang/String;IILandroid/content/Context;IILio/intercom/android/sdk/views/ResizableImageView;Landroid/widget/ProgressBar;)V
    .locals 13

    .prologue
    .line 113
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    move-object/from16 v0, p9

    move-object/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lio/intercom/android/sdk/blocks/NetworkImage;->hideLoadingState(Landroid/widget/ProgressBar;Landroid/widget/ImageView;)V

    .line 115
    sget v3, Lio/intercom/android/sdk/R$drawable;->intercom_error:I

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/views/ResizableImageView;->setImageResource(I)V

    .line 170
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v3, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-virtual {v3, p1}, Lio/intercom/com/bumptech/glide/i;->a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v4

    .line 119
    move/from16 v0, p6

    move/from16 v1, p7

    move-object/from16 v2, p8

    invoke-virtual {p0, v0, v1, v2, v4}, Lio/intercom/android/sdk/blocks/NetworkImage;->setImageViewBounds(IILio/intercom/android/sdk/views/ResizableImageView;Lio/intercom/com/bumptech/glide/h;)V

    .line 121
    iget-object v3, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->uploadingImageCache:Lio/intercom/android/sdk/blocks/UploadingImageCache;

    invoke-virtual {v3, p1}, Lio/intercom/android/sdk/blocks/UploadingImageCache;->getLocalImagePathForRemoteUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 122
    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 123
    new-instance v3, Lio/intercom/com/bumptech/glide/f/f;

    invoke-direct {v3}, Lio/intercom/com/bumptech/glide/f/f;-><init>()V

    new-instance v7, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;

    sget v8, Lio/intercom/android/sdk/R$dimen;->intercom_image_rounded_corners:I

    .line 125
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-direct {v7, v8}, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;-><init>(I)V

    .line 124
    invoke-virtual {v3, v7}, Lio/intercom/com/bumptech/glide/f/f;->a(Lio/intercom/com/bumptech/glide/load/l;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v3

    sget v7, Lio/intercom/android/sdk/R$drawable;->intercom_error:I

    .line 126
    invoke-virtual {v3, v7}, Lio/intercom/com/bumptech/glide/f/f;->b(I)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v3

    .line 127
    invoke-static {p1}, Lio/intercom/android/sdk/utilities/ImageUtils;->getDiskCacheStrategy(Ljava/lang/String;)Lio/intercom/com/bumptech/glide/load/engine/h;

    move-result-object v7

    invoke-virtual {v3, v7}, Lio/intercom/com/bumptech/glide/f/f;->b(Lio/intercom/com/bumptech/glide/load/engine/h;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v3

    .line 128
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 129
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 130
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    .line 131
    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {p0, v0, v1, v8}, Lio/intercom/android/sdk/blocks/NetworkImage;->getSampleSize(IILandroid/util/DisplayMetrics;)I

    move-result v8

    iput v8, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 132
    invoke-static {v5, v7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 133
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v7, v6, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 134
    invoke-static {}, Lio/intercom/android/sdk/utilities/ColorUtils;->newGreyscaleFilter()Landroid/graphics/ColorFilter;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 135
    invoke-virtual {v3, v7}, Lio/intercom/com/bumptech/glide/f/f;->a(Landroid/graphics/drawable/Drawable;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v3

    invoke-virtual {v3}, Lio/intercom/com/bumptech/glide/f/f;->h()Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v3

    .line 137
    :cond_1
    invoke-static/range {p3 .. p4}, Lio/intercom/com/bumptech/glide/h/i;->a(II)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 138
    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v3, v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->a(II)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v3

    .line 142
    :cond_2
    invoke-virtual {v4, v3}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v3

    .line 143
    invoke-static {}, Lio/intercom/com/bumptech/glide/load/resource/b/b;->c()Lio/intercom/com/bumptech/glide/load/resource/b/b;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/j;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v12

    new-instance v3, Lio/intercom/android/sdk/blocks/NetworkImage$1;

    move-object v4, p0

    move-object/from16 v5, p9

    move-object/from16 v6, p8

    move-object/from16 v7, p5

    move-object v8, p1

    move-object v9, p2

    move/from16 v10, p3

    move/from16 v11, p4

    invoke-direct/range {v3 .. v11}, Lio/intercom/android/sdk/blocks/NetworkImage$1;-><init>(Lio/intercom/android/sdk/blocks/NetworkImage;Landroid/widget/ProgressBar;Lio/intercom/android/sdk/views/ResizableImageView;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)V

    .line 144
    invoke-virtual {v12, v3}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/e;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v3

    .line 169
    move-object/from16 v0, p8

    invoke-virtual {v3, v0}, Lio/intercom/com/bumptech/glide/h;->a(Landroid/widget/ImageView;)Lio/intercom/com/bumptech/glide/f/a/h;

    goto/16 :goto_0
.end method


# virtual methods
.method public addImage(Ljava/lang/String;Ljava/lang/String;IILio/intercom/android/sdk/blocks/BlockAlignment;ZZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    .line 74
    invoke-virtual/range {p8 .. p8}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 75
    int-to-float v1, p3

    invoke-static {v1, v6}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v7

    .line 76
    move/from16 v0, p4

    int-to-float v1, v0

    invoke-static {v1, v6}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v8

    .line 78
    new-instance v11, Landroid/widget/FrameLayout;

    invoke-direct {v11, v6}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 79
    const/4 v1, -0x2

    const/4 v2, -0x2

    invoke-static {v11, v1, v2}, Lio/intercom/android/sdk/utilities/BlockUtils;->createLayoutParams(Landroid/view/View;II)V

    .line 80
    invoke-static {v11}, Lio/intercom/android/sdk/utilities/BlockUtils;->setDefaultMarginBottom(Landroid/view/View;)V

    .line 82
    new-instance v9, Lio/intercom/android/sdk/views/ResizableImageView;

    invoke-direct {v9, v6}, Lio/intercom/android/sdk/views/ResizableImageView;-><init>(Landroid/content/Context;)V

    .line 83
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 84
    const-string v1, "lightbox_image"

    invoke-virtual {v9, v1}, Lio/intercom/android/sdk/views/ResizableImageView;->setTransitionName(Ljava/lang/String;)V

    .line 86
    :cond_0
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v1}, Lio/intercom/android/sdk/views/ResizableImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    const/4 v1, 0x1

    invoke-virtual {v9, v1}, Lio/intercom/android/sdk/views/ResizableImageView;->setAdjustViewBounds(Z)V

    .line 88
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v9, v1}, Lio/intercom/android/sdk/views/ResizableImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 89
    invoke-virtual {p0, v9}, Lio/intercom/android/sdk/blocks/NetworkImage;->setBackground(Landroid/widget/ImageView;)V

    .line 91
    iget-object v1, p0, Lio/intercom/android/sdk/blocks/NetworkImage;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v1}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v1}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v1

    .line 93
    new-instance v10, Landroid/widget/ProgressBar;

    invoke-direct {v10, v6}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 94
    const/high16 v2, 0x42200000    # 40.0f

    invoke-static {v2, v6}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v2

    .line 95
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x11

    invoke-direct {v3, v2, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v10, v3}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 96
    sget v2, Lio/intercom/android/sdk/R$drawable;->intercom_progress_wheel:I

    invoke-static {v6, v2}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 97
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 98
    invoke-virtual {v10, v2}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 101
    invoke-virtual {v11, v9}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 102
    invoke-virtual {v11, v10}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move/from16 v5, p4

    .line 104
    invoke-direct/range {v1 .. v10}, Lio/intercom/android/sdk/blocks/NetworkImage;->loadImageFromUrl(Ljava/lang/String;Ljava/lang/String;IILandroid/content/Context;IILio/intercom/android/sdk/views/ResizableImageView;Landroid/widget/ProgressBar;)V

    .line 106
    invoke-virtual/range {p5 .. p5}, Lio/intercom/android/sdk/blocks/BlockAlignment;->getGravity()I

    move-result v1

    move/from16 v0, p7

    invoke-static {v11, v1, v0}, Lio/intercom/android/sdk/utilities/BlockUtils;->setLayoutMarginsAndGravity(Landroid/view/View;IZ)V

    .line 107
    return-object v11
.end method

.method getSampleSize(IILandroid/util/DisplayMetrics;)I
    .locals 6

    .prologue
    .line 173
    const/4 v0, 0x1

    .line 175
    iget v1, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    if-gt p1, v1, :cond_0

    iget v1, p3, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le p2, v1, :cond_1

    .line 176
    :cond_0
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v2, v2

    .line 177
    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v2, v4

    .line 176
    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    .line 177
    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    .line 176
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 180
    :cond_1
    return v0
.end method

.method hideLoadingState(Landroid/widget/ProgressBar;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 184
    if-eqz p1, :cond_0

    .line 185
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 186
    const v0, 0x106000d

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 188
    :cond_0
    return-void
.end method
