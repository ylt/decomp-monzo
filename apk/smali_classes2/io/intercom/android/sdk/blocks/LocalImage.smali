.class Lio/intercom/android/sdk/blocks/LocalImage;
.super Lio/intercom/android/sdk/blocks/Image;
.source "LocalImage.java"

# interfaces
.implements Lio/intercom/android/sdk/blocks/blockInterfaces/LocalImageBlock;


# instance fields
.field private final requestManager:Lio/intercom/com/bumptech/glide/i;

.field private final twig:Lio/intercom/android/sdk/twig/Twig;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/blocks/StyleType;Lio/intercom/com/bumptech/glide/i;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/blocks/Image;-><init>(Lio/intercom/android/sdk/blocks/StyleType;)V

    .line 41
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/blocks/LocalImage;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 46
    iput-object p2, p0, Lio/intercom/android/sdk/blocks/LocalImage;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/blocks/LocalImage;)Lio/intercom/android/sdk/twig/Twig;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lio/intercom/android/sdk/blocks/LocalImage;->twig:Lio/intercom/android/sdk/twig/Twig;

    return-object v0
.end method


# virtual methods
.method public addImage(Ljava/lang/String;IILio/intercom/android/sdk/blocks/BlockAlignment;ZZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v5, -0x2

    .line 52
    invoke-virtual {p7}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 53
    int-to-float v1, p2

    invoke-static {v1, v0}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v1

    .line 54
    int-to-float v2, p3

    invoke-static {v2, v0}, Lio/intercom/android/sdk/commons/utilities/ScreenUtils;->dpToPx(FLandroid/content/Context;)I

    move-result v2

    .line 56
    new-instance v3, Lio/intercom/android/sdk/views/ProgressFrameLayout;

    invoke-direct {v3, v0}, Lio/intercom/android/sdk/views/ProgressFrameLayout;-><init>(Landroid/content/Context;)V

    .line 57
    invoke-static {v3, v5, v5}, Lio/intercom/android/sdk/utilities/BlockUtils;->createLayoutParams(Landroid/view/View;II)V

    .line 58
    invoke-static {v3}, Lio/intercom/android/sdk/utilities/BlockUtils;->setDefaultMarginBottom(Landroid/view/View;)V

    .line 60
    new-instance v4, Lio/intercom/android/sdk/views/ResizableImageView;

    invoke-direct {v4, v0}, Lio/intercom/android/sdk/views/ResizableImageView;-><init>(Landroid/content/Context;)V

    .line 61
    invoke-static {v4, v5, v5}, Lio/intercom/android/sdk/utilities/BlockUtils;->createLayoutParams(Landroid/view/View;II)V

    .line 62
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lio/intercom/android/sdk/views/ResizableImageView;->setAdjustViewBounds(Z)V

    .line 63
    sget-object v5, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Lio/intercom/android/sdk/views/ResizableImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 65
    invoke-virtual {v3, v4}, Lio/intercom/android/sdk/views/ProgressFrameLayout;->addView(Landroid/view/View;)V

    .line 67
    iget-object v5, p0, Lio/intercom/android/sdk/blocks/LocalImage;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-virtual {v5, p1}, Lio/intercom/com/bumptech/glide/i;->a(Ljava/lang/Object;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v5

    .line 69
    invoke-virtual {p0, v1, v2, v4, v5}, Lio/intercom/android/sdk/blocks/LocalImage;->setImageViewBounds(IILio/intercom/android/sdk/views/ResizableImageView;Lio/intercom/com/bumptech/glide/h;)V

    .line 71
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lio/intercom/android/sdk/views/ProgressFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 72
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 73
    instance-of v2, v1, Lio/intercom/android/sdk/views/UploadProgressBar;

    if-eqz v2, :cond_0

    .line 74
    sget v2, Lio/intercom/android/sdk/R$dimen;->intercom_local_image_upload_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 75
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v7, 0x11

    invoke-direct {v6, v2, v2, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 77
    invoke-virtual {v3}, Lio/intercom/android/sdk/views/ProgressFrameLayout;->uploadStarted()V

    .line 80
    :cond_0
    invoke-virtual {p0, v4}, Lio/intercom/android/sdk/blocks/LocalImage;->setBackground(Landroid/widget/ImageView;)V

    .line 82
    new-instance v1, Landroid/graphics/ColorMatrix;

    invoke-direct {v1}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 83
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 84
    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v2, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v4, v2}, Lio/intercom/android/sdk/views/ResizableImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 86
    new-instance v1, Lio/intercom/com/bumptech/glide/f/f;

    invoke-direct {v1}, Lio/intercom/com/bumptech/glide/f/f;-><init>()V

    new-instance v2, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;

    sget v6, Lio/intercom/android/sdk/R$dimen;->intercom_image_rounded_corners:I

    .line 89
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {v2, v0}, Lio/intercom/android/sdk/transforms/RoundedCornersTransform;-><init>(I)V

    .line 88
    invoke-virtual {v1, v2}, Lio/intercom/com/bumptech/glide/f/f;->a(Lio/intercom/com/bumptech/glide/load/l;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    .line 90
    invoke-static {p1}, Lio/intercom/android/sdk/utilities/ImageUtils;->getDiskCacheStrategy(Ljava/lang/String;)Lio/intercom/com/bumptech/glide/load/engine/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/f/f;->b(Lio/intercom/com/bumptech/glide/load/engine/h;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v0

    .line 87
    invoke-virtual {v5, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    .line 91
    invoke-static {}, Lio/intercom/com/bumptech/glide/load/resource/b/b;->c()Lio/intercom/com/bumptech/glide/load/resource/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/j;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    new-instance v1, Lio/intercom/android/sdk/blocks/LocalImage$1;

    invoke-direct {v1, p0, v4}, Lio/intercom/android/sdk/blocks/LocalImage$1;-><init>(Lio/intercom/android/sdk/blocks/LocalImage;Lio/intercom/android/sdk/views/ResizableImageView;)V

    .line 92
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/e;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    .line 106
    invoke-virtual {v0, v4}, Lio/intercom/com/bumptech/glide/h;->a(Landroid/widget/ImageView;)Lio/intercom/com/bumptech/glide/f/a/h;

    .line 108
    invoke-virtual {p4}, Lio/intercom/android/sdk/blocks/BlockAlignment;->getGravity()I

    move-result v0

    invoke-static {v3, v0, p6}, Lio/intercom/android/sdk/utilities/BlockUtils;->setLayoutMarginsAndGravity(Landroid/view/View;IZ)V

    .line 109
    return-object v3
.end method
