.class public final Lio/intercom/android/sdk/blocks/models/Block$Builder;
.super Ljava/lang/Object;
.source "Block.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/blocks/models/Block;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field align:Ljava/lang/String;

.field articleId:Ljava/lang/String;

.field attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/BlockAttachment;",
            ">;"
        }
    .end annotation
.end field

.field attribution:Ljava/lang/String;

.field author:Lio/intercom/android/sdk/blocks/models/Author;

.field channels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Channel;",
            ">;"
        }
    .end annotation
.end field

.field data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field description:Ljava/lang/String;

.field embedUrl:Ljava/lang/String;

.field footerLink:Lio/intercom/android/sdk/blocks/models/Block$Builder;

.field height:Ljava/lang/Integer;

.field id:Ljava/lang/String;

.field image:Lio/intercom/android/sdk/blocks/models/Image;

.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field language:Ljava/lang/String;

.field linkType:Ljava/lang/String;

.field linkUrl:Ljava/lang/String;

.field links:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;",
            ">;"
        }
    .end annotation
.end field

.field options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;",
            ">;"
        }
    .end annotation
.end field

.field previewUrl:Ljava/lang/String;

.field provider:Ljava/lang/String;

.field ratingIndex:Ljava/lang/Integer;

.field rating_index:Ljava/lang/Integer;

.field remark:Ljava/lang/String;

.field siteName:Ljava/lang/String;

.field text:Ljava/lang/String;

.field title:Ljava/lang/String;

.field trackingUrl:Ljava/lang/String;

.field type:Ljava/lang/String;

.field url:Ljava/lang/String;

.field username:Ljava/lang/String;

.field width:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lio/intercom/android/sdk/blocks/models/Block;
    .locals 2

    .prologue
    .line 280
    new-instance v0, Lio/intercom/android/sdk/blocks/models/Block;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/intercom/android/sdk/blocks/models/Block;-><init>(Lio/intercom/android/sdk/blocks/models/Block$Builder;Lio/intercom/android/sdk/blocks/models/Block$1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 284
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 339
    :cond_0
    :goto_0
    return v1

    .line 285
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 287
    check-cast p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;

    .line 289
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->type:Ljava/lang/String;

    if-eqz v2, :cond_22

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->type:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 290
    :cond_2
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->text:Ljava/lang/String;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->text:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->text:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    :cond_3
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->title:Ljava/lang/String;

    if-eqz v2, :cond_24

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->title:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 292
    :cond_4
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->description:Ljava/lang/String;

    if-eqz v2, :cond_25

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->description:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    :cond_5
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkType:Ljava/lang/String;

    if-eqz v2, :cond_26

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkType:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 296
    :cond_6
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->siteName:Ljava/lang/String;

    if-eqz v2, :cond_27

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->siteName:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->siteName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 298
    :cond_7
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->articleId:Ljava/lang/String;

    if-eqz v2, :cond_28

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->articleId:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->articleId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 300
    :cond_8
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->author:Lio/intercom/android/sdk/blocks/models/Author;

    if-eqz v2, :cond_29

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->author:Lio/intercom/android/sdk/blocks/models/Author;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->author:Lio/intercom/android/sdk/blocks/models/Author;

    invoke-virtual {v2, v3}, Lio/intercom/android/sdk/blocks/models/Author;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 302
    :cond_9
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->image:Lio/intercom/android/sdk/blocks/models/Image;

    if-eqz v2, :cond_2a

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->image:Lio/intercom/android/sdk/blocks/models/Image;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->image:Lio/intercom/android/sdk/blocks/models/Image;

    invoke-virtual {v2, v3}, Lio/intercom/android/sdk/blocks/models/Image;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 303
    :cond_a
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->data:Ljava/util/Map;

    if-eqz v2, :cond_2b

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->data:Ljava/util/Map;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->data:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 304
    :cond_b
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->language:Ljava/lang/String;

    if-eqz v2, :cond_2c

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->language:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->language:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 306
    :cond_c
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->url:Ljava/lang/String;

    if-eqz v2, :cond_2d

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->url:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    :cond_d
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkUrl:Ljava/lang/String;

    if-eqz v2, :cond_2e

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkUrl:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 309
    :cond_e
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->embedUrl:Ljava/lang/String;

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->embedUrl:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->embedUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 311
    :cond_f
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->trackingUrl:Ljava/lang/String;

    if-eqz v2, :cond_30

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->trackingUrl:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->trackingUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 313
    :cond_10
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->username:Ljava/lang/String;

    if-eqz v2, :cond_31

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->username:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->username:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 315
    :cond_11
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->provider:Ljava/lang/String;

    if-eqz v2, :cond_32

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->provider:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->provider:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 317
    :cond_12
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->id:Ljava/lang/String;

    if-eqz v2, :cond_33

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->id:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 318
    :cond_13
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->align:Ljava/lang/String;

    if-eqz v2, :cond_34

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->align:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->align:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 319
    :cond_14
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->width:Ljava/lang/Integer;

    if-eqz v2, :cond_35

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->width:Ljava/lang/Integer;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->width:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 320
    :cond_15
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->height:Ljava/lang/Integer;

    if-eqz v2, :cond_36

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->height:Ljava/lang/Integer;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->height:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    :cond_16
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->previewUrl:Ljava/lang/String;

    if-eqz v2, :cond_37

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->previewUrl:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->previewUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    :cond_17
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attribution:Ljava/lang/String;

    if-eqz v2, :cond_38

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attribution:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attribution:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 326
    :cond_18
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attachments:Ljava/util/List;

    if-eqz v2, :cond_39

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attachments:Ljava/util/List;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attachments:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 328
    :cond_19
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->items:Ljava/util/List;

    if-eqz v2, :cond_3a

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->items:Ljava/util/List;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->items:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 329
    :cond_1a
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->channels:Ljava/util/List;

    if-eqz v2, :cond_3b

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->channels:Ljava/util/List;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->channels:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 331
    :cond_1b
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->rating_index:Ljava/lang/Integer;

    if-eqz v2, :cond_3c

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->rating_index:Ljava/lang/Integer;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->rating_index:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 333
    :cond_1c
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->ratingIndex:Ljava/lang/Integer;

    if-eqz v2, :cond_3d

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->ratingIndex:Ljava/lang/Integer;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->ratingIndex:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 335
    :cond_1d
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->remark:Ljava/lang/String;

    if-eqz v2, :cond_3e

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->remark:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->remark:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 337
    :cond_1e
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->options:Ljava/util/List;

    if-eqz v2, :cond_3f

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->options:Ljava/util/List;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->options:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 338
    :cond_1f
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->links:Ljava/util/List;

    if-eqz v2, :cond_40

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->links:Ljava/util/List;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->links:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 339
    :cond_20
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->footerLink:Lio/intercom/android/sdk/blocks/models/Block$Builder;

    if-eqz v2, :cond_41

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->footerLink:Lio/intercom/android/sdk/blocks/models/Block$Builder;

    iget-object v1, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->footerLink:Lio/intercom/android/sdk/blocks/models/Block$Builder;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_21
    :goto_1
    move v1, v0

    goto/16 :goto_0

    .line 289
    :cond_22
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->type:Ljava/lang/String;

    if-eqz v2, :cond_2

    goto/16 :goto_0

    .line 290
    :cond_23
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->text:Ljava/lang/String;

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 291
    :cond_24
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->title:Ljava/lang/String;

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 292
    :cond_25
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->description:Ljava/lang/String;

    if-eqz v2, :cond_5

    goto/16 :goto_0

    .line 294
    :cond_26
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkType:Ljava/lang/String;

    if-eqz v2, :cond_6

    goto/16 :goto_0

    .line 296
    :cond_27
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->siteName:Ljava/lang/String;

    if-eqz v2, :cond_7

    goto/16 :goto_0

    .line 298
    :cond_28
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->articleId:Ljava/lang/String;

    if-eqz v2, :cond_8

    goto/16 :goto_0

    .line 300
    :cond_29
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->author:Lio/intercom/android/sdk/blocks/models/Author;

    if-eqz v2, :cond_9

    goto/16 :goto_0

    .line 302
    :cond_2a
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->image:Lio/intercom/android/sdk/blocks/models/Image;

    if-eqz v2, :cond_a

    goto/16 :goto_0

    .line 303
    :cond_2b
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->data:Ljava/util/Map;

    if-eqz v2, :cond_b

    goto/16 :goto_0

    .line 304
    :cond_2c
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->language:Ljava/lang/String;

    if-eqz v2, :cond_c

    goto/16 :goto_0

    .line 306
    :cond_2d
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->url:Ljava/lang/String;

    if-eqz v2, :cond_d

    goto/16 :goto_0

    .line 307
    :cond_2e
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkUrl:Ljava/lang/String;

    if-eqz v2, :cond_e

    goto/16 :goto_0

    .line 309
    :cond_2f
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->embedUrl:Ljava/lang/String;

    if-eqz v2, :cond_f

    goto/16 :goto_0

    .line 311
    :cond_30
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->trackingUrl:Ljava/lang/String;

    if-eqz v2, :cond_10

    goto/16 :goto_0

    .line 313
    :cond_31
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->username:Ljava/lang/String;

    if-eqz v2, :cond_11

    goto/16 :goto_0

    .line 315
    :cond_32
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->provider:Ljava/lang/String;

    if-eqz v2, :cond_12

    goto/16 :goto_0

    .line 317
    :cond_33
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->id:Ljava/lang/String;

    if-eqz v2, :cond_13

    goto/16 :goto_0

    .line 318
    :cond_34
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->align:Ljava/lang/String;

    if-eqz v2, :cond_14

    goto/16 :goto_0

    .line 319
    :cond_35
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->width:Ljava/lang/Integer;

    if-eqz v2, :cond_15

    goto/16 :goto_0

    .line 320
    :cond_36
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->height:Ljava/lang/Integer;

    if-eqz v2, :cond_16

    goto/16 :goto_0

    .line 322
    :cond_37
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->previewUrl:Ljava/lang/String;

    if-eqz v2, :cond_17

    goto/16 :goto_0

    .line 324
    :cond_38
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attribution:Ljava/lang/String;

    if-eqz v2, :cond_18

    goto/16 :goto_0

    .line 326
    :cond_39
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attachments:Ljava/util/List;

    if-eqz v2, :cond_19

    goto/16 :goto_0

    .line 328
    :cond_3a
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->items:Ljava/util/List;

    if-eqz v2, :cond_1a

    goto/16 :goto_0

    .line 329
    :cond_3b
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->channels:Ljava/util/List;

    if-eqz v2, :cond_1b

    goto/16 :goto_0

    .line 331
    :cond_3c
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->rating_index:Ljava/lang/Integer;

    if-eqz v2, :cond_1c

    goto/16 :goto_0

    .line 333
    :cond_3d
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->ratingIndex:Ljava/lang/Integer;

    if-eqz v2, :cond_1d

    goto/16 :goto_0

    .line 335
    :cond_3e
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->remark:Ljava/lang/String;

    if-eqz v2, :cond_1e

    goto/16 :goto_0

    .line 337
    :cond_3f
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->options:Ljava/util/List;

    if-eqz v2, :cond_1f

    goto/16 :goto_0

    .line 338
    :cond_40
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->links:Ljava/util/List;

    if-eqz v2, :cond_20

    goto/16 :goto_0

    .line 339
    :cond_41
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/Block$Builder;->footerLink:Lio/intercom/android/sdk/blocks/models/Block$Builder;

    if-eqz v2, :cond_21

    move v0, v1

    goto/16 :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 343
    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->type:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->type:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 344
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->text:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 345
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->title:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->title:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 346
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->description:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->description:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 347
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkType:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 348
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->siteName:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->siteName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 349
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->articleId:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->articleId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    .line 350
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->author:Lio/intercom/android/sdk/blocks/models/Author;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->author:Lio/intercom/android/sdk/blocks/models/Author;

    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/Author;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    .line 351
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->image:Lio/intercom/android/sdk/blocks/models/Image;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->image:Lio/intercom/android/sdk/blocks/models/Image;

    invoke-virtual {v0}, Lio/intercom/android/sdk/blocks/models/Image;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    .line 352
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->data:Ljava/util/Map;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->data:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    .line 353
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->language:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->language:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    .line 354
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->url:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->url:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v0, v2

    .line 355
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkUrl:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v2

    .line 356
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->embedUrl:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->embedUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v0, v2

    .line 357
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->trackingUrl:Ljava/lang/String;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->trackingUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_e
    add-int/2addr v0, v2

    .line 358
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->username:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->username:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_f
    add-int/2addr v0, v2

    .line 359
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->provider:Ljava/lang/String;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->provider:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_10
    add-int/2addr v0, v2

    .line 360
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->id:Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_11
    add-int/2addr v0, v2

    .line 361
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->align:Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->align:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v0, v2

    .line 362
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    :goto_13
    add-int/2addr v0, v2

    .line 363
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->height:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    :goto_14
    add-int/2addr v0, v2

    .line 364
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->previewUrl:Ljava/lang/String;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->previewUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_15
    add-int/2addr v0, v2

    .line 365
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attribution:Ljava/lang/String;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attribution:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_16
    add-int/2addr v0, v2

    .line 366
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attachments:Ljava/util/List;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_17
    add-int/2addr v0, v2

    .line 367
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->items:Ljava/util/List;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_18
    add-int/2addr v0, v2

    .line 368
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->channels:Ljava/util/List;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->channels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_19
    add-int/2addr v0, v2

    .line 369
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->rating_index:Ljava/lang/Integer;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->rating_index:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    :goto_1a
    add-int/2addr v0, v2

    .line 370
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->ratingIndex:Ljava/lang/Integer;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->ratingIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    :goto_1b
    add-int/2addr v0, v2

    .line 371
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->remark:Ljava/lang/String;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->remark:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1c
    add-int/2addr v0, v2

    .line 372
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->options:Ljava/util/List;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_1d
    add-int/2addr v0, v2

    .line 373
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->links:Ljava/util/List;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->links:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_1e
    add-int/2addr v0, v2

    .line 374
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->footerLink:Lio/intercom/android/sdk/blocks/models/Block$Builder;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->footerLink:Lio/intercom/android/sdk/blocks/models/Block$Builder;

    invoke-virtual {v1}, Lio/intercom/android/sdk/blocks/models/Block$Builder;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 375
    return v0

    :cond_1
    move v0, v1

    .line 343
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 344
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 345
    goto/16 :goto_2

    :cond_4
    move v0, v1

    .line 346
    goto/16 :goto_3

    :cond_5
    move v0, v1

    .line 347
    goto/16 :goto_4

    :cond_6
    move v0, v1

    .line 348
    goto/16 :goto_5

    :cond_7
    move v0, v1

    .line 349
    goto/16 :goto_6

    :cond_8
    move v0, v1

    .line 350
    goto/16 :goto_7

    :cond_9
    move v0, v1

    .line 351
    goto/16 :goto_8

    :cond_a
    move v0, v1

    .line 352
    goto/16 :goto_9

    :cond_b
    move v0, v1

    .line 353
    goto/16 :goto_a

    :cond_c
    move v0, v1

    .line 354
    goto/16 :goto_b

    :cond_d
    move v0, v1

    .line 355
    goto/16 :goto_c

    :cond_e
    move v0, v1

    .line 356
    goto/16 :goto_d

    :cond_f
    move v0, v1

    .line 357
    goto/16 :goto_e

    :cond_10
    move v0, v1

    .line 358
    goto/16 :goto_f

    :cond_11
    move v0, v1

    .line 359
    goto/16 :goto_10

    :cond_12
    move v0, v1

    .line 360
    goto/16 :goto_11

    :cond_13
    move v0, v1

    .line 361
    goto/16 :goto_12

    :cond_14
    move v0, v1

    .line 362
    goto/16 :goto_13

    :cond_15
    move v0, v1

    .line 363
    goto/16 :goto_14

    :cond_16
    move v0, v1

    .line 364
    goto/16 :goto_15

    :cond_17
    move v0, v1

    .line 365
    goto/16 :goto_16

    :cond_18
    move v0, v1

    .line 366
    goto/16 :goto_17

    :cond_19
    move v0, v1

    .line 367
    goto/16 :goto_18

    :cond_1a
    move v0, v1

    .line 368
    goto/16 :goto_19

    :cond_1b
    move v0, v1

    .line 369
    goto/16 :goto_1a

    :cond_1c
    move v0, v1

    .line 370
    goto/16 :goto_1b

    :cond_1d
    move v0, v1

    .line 371
    goto :goto_1c

    :cond_1e
    move v0, v1

    .line 372
    goto :goto_1d

    :cond_1f
    move v0, v1

    .line 373
    goto :goto_1e
.end method

.method public withAlign(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->align:Ljava/lang/String;

    .line 225
    return-object p0
.end method

.method public withArticleId(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->articleId:Ljava/lang/String;

    .line 195
    return-object p0
.end method

.method public withAttachments(Ljava/util/List;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/BlockAttachment;",
            ">;)",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;"
        }
    .end annotation

    .prologue
    .line 255
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attachments:Ljava/util/List;

    .line 256
    return-object p0
.end method

.method public withAttribution(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->attribution:Ljava/lang/String;

    .line 250
    return-object p0
.end method

.method public withAuthor(Lio/intercom/android/sdk/blocks/models/Author;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->author:Lio/intercom/android/sdk/blocks/models/Author;

    .line 200
    return-object p0
.end method

.method public withChannels(Ljava/util/List;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Channel;",
            ">;)",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;"
        }
    .end annotation

    .prologue
    .line 260
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->channels:Ljava/util/List;

    .line 261
    return-object p0
.end method

.method public withData(Ljava/util/Map;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;"
        }
    .end annotation

    .prologue
    .line 209
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->data:Ljava/util/Map;

    .line 210
    return-object p0
.end method

.method public withDescription(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->description:Ljava/lang/String;

    .line 180
    return-object p0
.end method

.method public withHeight(I)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 1

    .prologue
    .line 229
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->height:Ljava/lang/Integer;

    .line 230
    return-object p0
.end method

.method public withImage(Lio/intercom/android/sdk/blocks/models/Image;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->image:Lio/intercom/android/sdk/blocks/models/Image;

    .line 205
    return-object p0
.end method

.method public withItems(Ljava/util/List;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;"
        }
    .end annotation

    .prologue
    .line 239
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->items:Ljava/util/List;

    .line 240
    return-object p0
.end method

.method public withLinkType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->linkType:Ljava/lang/String;

    .line 185
    return-object p0
.end method

.method public withOptions(Ljava/util/List;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;",
            ">;)",
            "Lio/intercom/android/sdk/blocks/models/Block$Builder;"
        }
    .end annotation

    .prologue
    .line 275
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->options:Ljava/util/List;

    .line 276
    return-object p0
.end method

.method public withPreviewUrl(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->previewUrl:Ljava/lang/String;

    .line 245
    return-object p0
.end method

.method public withRatingIndex(Ljava/lang/Integer;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->ratingIndex:Ljava/lang/Integer;

    .line 266
    return-object p0
.end method

.method public withRemark(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->remark:Ljava/lang/String;

    .line 271
    return-object p0
.end method

.method public withSiteName(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->siteName:Ljava/lang/String;

    .line 190
    return-object p0
.end method

.method public withText(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->text:Ljava/lang/String;

    .line 170
    return-object p0
.end method

.method public withTitle(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->title:Ljava/lang/String;

    .line 175
    return-object p0
.end method

.method public withType(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->type:Ljava/lang/String;

    .line 215
    return-object p0
.end method

.method public withUrl(Ljava/lang/String;)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->url:Ljava/lang/String;

    .line 220
    return-object p0
.end method

.method public withWidth(I)Lio/intercom/android/sdk/blocks/models/Block$Builder;
    .locals 1

    .prologue
    .line 234
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/blocks/models/Block$Builder;->width:Ljava/lang/Integer;

    .line 235
    return-object p0
.end method
