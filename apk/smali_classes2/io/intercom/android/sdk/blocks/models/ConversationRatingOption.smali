.class public Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;
.super Ljava/lang/Object;
.source "ConversationRatingOption.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final emoji:Ljava/lang/String;

.field private final index:I

.field private final unicode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$1;

    invoke-direct {v0}, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$1;-><init>()V

    sput-object v0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->index:I

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    .line 84
    return-void
.end method

.method private constructor <init>(Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iget-object v0, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;->index:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->index:I

    .line 13
    iget-object v0, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;->emoji:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    iput-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    .line 14
    iget-object v0, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;->unicode:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    iput-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    .line 15
    return-void

    .line 12
    :cond_0
    iget-object v0, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;->index:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 13
    :cond_1
    iget-object v0, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;->emoji:Ljava/lang/String;

    goto :goto_1

    .line 14
    :cond_2
    iget-object v0, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;->unicode:Ljava/lang/String;

    goto :goto_2
.end method

.method synthetic constructor <init>(Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$1;)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;-><init>(Lio/intercom/android/sdk/blocks/models/ConversationRatingOption$Builder;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 59
    check-cast p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;

    .line 61
    iget v2, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->index:I

    iget v3, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->index:I

    if-ne v2, v3, :cond_0

    .line 62
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    iget-object v3, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    :cond_2
    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    iget-object v1, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_3
    :goto_1
    move v1, v0

    goto :goto_0

    .line 62
    :cond_4
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    if-eqz v2, :cond_2

    goto :goto_0

    .line 63
    :cond_5
    iget-object v2, p1, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_1
.end method

.method public getEmoji()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->index:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getUnicode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 69
    iget v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->index:I

    .line 70
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 71
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 72
    return v0

    :cond_1
    move v0, v1

    .line 70
    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->index:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->emoji:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lio/intercom/android/sdk/blocks/models/ConversationRatingOption;->unicode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    return-void
.end method
