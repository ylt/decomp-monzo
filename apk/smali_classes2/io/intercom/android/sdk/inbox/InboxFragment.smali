.class public Lio/intercom/android/sdk/inbox/InboxFragment;
.super Landroid/support/v4/app/Fragment;
.source "InboxFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lio/intercom/android/sdk/inbox/ConversationClickListener;
.implements Lio/intercom/android/sdk/store/Store$Subscriber;
.implements Lio/intercom/android/sdk/views/EndlessScrollListener;
.implements Lio/intercom/android/sdk/views/IntercomToolbar$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/inbox/InboxFragment$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/view/View$OnClickListener;",
        "Lio/intercom/android/sdk/inbox/ConversationClickListener;",
        "Lio/intercom/android/sdk/store/Store$Subscriber",
        "<",
        "Lio/intercom/android/sdk/state/InboxState;",
        ">;",
        "Lio/intercom/android/sdk/views/EndlessScrollListener;",
        "Lio/intercom/android/sdk/views/IntercomToolbar$Listener;"
    }
.end annotation


# static fields
.field private static final ARG_IS_TWO_PANE:Ljava/lang/String; = "is_two_pane"

.field private static final FADE_DURATION_MS:I = 0x96


# instance fields
.field private adapter:Lio/intercom/android/sdk/inbox/InboxAdapter;

.field private appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private composerButton:Landroid/support/design/widget/FloatingActionButton;

.field private endlessRecyclerScrollListener:Lio/intercom/android/sdk/views/EndlessRecyclerScrollListener;

.field private inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

.field inboxView:Landroid/support/v7/widget/RecyclerView;

.field private intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

.field private isTwoPane:Z

.field private layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

.field listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

.field private progressBar:Landroid/widget/ProgressBar;

.field private requestManager:Lio/intercom/com/bumptech/glide/i;

.field private rootView:Landroid/view/View;

.field private store:Lio/intercom/android/sdk/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;"
        }
    .end annotation
.end field

.field private subscription:Lio/intercom/android/sdk/store/Store$Subscription;

.field private final twig:Lio/intercom/android/sdk/twig/Twig;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 70
    sget-object v0, Lio/intercom/android/sdk/inbox/InboxFragment$Listener;->EMPTY:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->isTwoPane:Z

    .line 79
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->twig:Lio/intercom/android/sdk/twig/Twig;

    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/inbox/InboxFragment;)Lio/intercom/android/sdk/store/Store;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->store:Lio/intercom/android/sdk/store/Store;

    return-object v0
.end method

.method private displayEmptyView()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 347
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_no_conversations:I

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setTitle(I)V

    .line 348
    invoke-virtual {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_empty_conversations:I

    invoke-static {v0, v1}, Lio/intercom/android/sdk/utilities/Phrase;->from(Landroid/content/Context;I)Lio/intercom/android/sdk/utilities/Phrase;

    move-result-object v1

    const-string v2, "name"

    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 349
    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/utilities/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lio/intercom/android/sdk/utilities/Phrase;

    move-result-object v0

    .line 350
    invoke-virtual {v0}, Lio/intercom/android/sdk/utilities/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 351
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/views/IntercomErrorView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonVisibility(I)V

    .line 353
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setVisibility(I)V

    .line 354
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->showComposerButtonIfEnabled()V

    .line 355
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 357
    return-void
.end method

.method private displayErrorView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 364
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_inbox_error_state_title:I

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setTitle(I)V

    .line 365
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_failed_to_load_conversation:I

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setSubtitle(I)V

    .line 366
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_retry:I

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonText(I)V

    .line 368
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonVisibility(I)V

    .line 369
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/views/IntercomErrorView;->setVisibility(I)V

    .line 370
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 371
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 372
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->composerButton:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 373
    return-void
.end method

.method private displayInbox()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 340
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 341
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->showComposerButtonIfEnabled()V

    .line 342
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/views/IntercomErrorView;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 344
    return-void
.end method

.method private displayLoadingView()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 333
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->composerButton:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 337
    return-void
.end method

.method private fadeOutInbox(Landroid/animation/Animator$AnimatorListener;)V
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    const/16 v1, 0x96

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomToolbar;->fadeOutTitle(I)V

    .line 245
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 246
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    .line 247
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 248
    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 249
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 250
    return-void
.end method

.method private isInboundMessageEnabled()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->isInboundMessages()Z

    move-result v0

    return v0
.end method

.method public static newInstance(Z)Lio/intercom/android/sdk/inbox/InboxFragment;
    .locals 3

    .prologue
    .line 84
    new-instance v0, Lio/intercom/android/sdk/inbox/InboxFragment;

    invoke-direct {v0}, Lio/intercom/android/sdk/inbox/InboxFragment;-><init>()V

    .line 85
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 86
    const-string v2, "is_two_pane"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 87
    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/inbox/InboxFragment;->setArguments(Landroid/os/Bundle;)V

    .line 88
    return-object v0
.end method

.method private setColorScheme()V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    .line 208
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 209
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->composerButton:Landroid/support/design/widget/FloatingActionButton;

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 211
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    invoke-interface {v1}, Lio/intercom/android/sdk/inbox/InboxFragment$Listener;->setStatusBarColor()V

    .line 212
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/views/IntercomToolbar;->setBackgroundColor(I)V

    .line 213
    return-void
.end method

.method private setToolbarTitle()V
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getName()Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    sget v1, Lio/intercom/android/sdk/R$string;->intercom_conversations:I

    invoke-virtual {p0, v1}, Lio/intercom/android/sdk/inbox/InboxFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomToolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 204
    :goto_0
    return-void

    .line 199
    :cond_0
    invoke-virtual {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lio/intercom/android/sdk/R$string;->intercom_conversations_with_app:I

    invoke-static {v1, v2}, Lio/intercom/android/sdk/utilities/Phrase;->from(Landroid/content/Context;I)Lio/intercom/android/sdk/utilities/Phrase;

    move-result-object v1

    const-string v2, "name"

    .line 200
    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/utilities/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lio/intercom/android/sdk/utilities/Phrase;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lio/intercom/android/sdk/utilities/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/views/IntercomToolbar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showComposerButtonIfEnabled()V
    .locals 2

    .prologue
    .line 360
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->composerButton:Landroid/support/design/widget/FloatingActionButton;

    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->isInboundMessageEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 361
    return-void

    .line 360
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 96
    :try_start_0
    move-object v0, p1

    check-cast v0, Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    move-object v1, v0

    iput-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    return-void

    .line 97
    :catch_0
    move-exception v1

    .line 98
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement InboxFragment.Listener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 230
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lio/intercom/android/sdk/R$id;->compose_action_button:I

    if-ne v0, v1, :cond_0

    .line 231
    iget-boolean v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->isTwoPane:Z

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    invoke-interface {v0}, Lio/intercom/android/sdk/inbox/InboxFragment$Listener;->onComposerSelected()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    new-instance v0, Lio/intercom/android/sdk/inbox/InboxFragment$3;

    invoke-direct {v0, p0}, Lio/intercom/android/sdk/inbox/InboxFragment$3;-><init>(Lio/intercom/android/sdk/inbox/InboxFragment;)V

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/inbox/InboxFragment;->fadeOutInbox(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public onCloseClicked()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    invoke-interface {v0}, Lio/intercom/android/sdk/inbox/InboxFragment$Listener;->onToolbarCloseClicked()V

    .line 281
    return-void
.end method

.method public onConversationClicked(I)V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->store:Lio/intercom/android/sdk/store/Store;

    invoke-virtual {v0}, Lio/intercom/android/sdk/store/Store;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/State;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/State;->inboxState()Lio/intercom/android/sdk/state/InboxState;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/InboxState;->conversations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Conversation;

    .line 254
    iget-boolean v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->isTwoPane:Z

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    invoke-interface {v1, v0}, Lio/intercom/android/sdk/inbox/InboxFragment$Listener;->onConversationSelected(Lio/intercom/android/sdk/models/Conversation;)V

    .line 263
    :goto_0
    return-void

    .line 257
    :cond_0
    new-instance v1, Lio/intercom/android/sdk/inbox/InboxFragment$4;

    invoke-direct {v1, p0, v0}, Lio/intercom/android/sdk/inbox/InboxFragment$4;-><init>(Lio/intercom/android/sdk/inbox/InboxFragment;Lio/intercom/android/sdk/models/Conversation;)V

    invoke-direct {p0, v1}, Lio/intercom/android/sdk/inbox/InboxFragment;->fadeOutInbox(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 104
    invoke-static {}, Lio/intercom/android/sdk/Injector;->get()Lio/intercom/android/sdk/Injector;

    move-result-object v2

    .line 106
    invoke-static {p0}, Lio/intercom/com/bumptech/glide/c;->a(Landroid/support/v4/app/Fragment;)Lio/intercom/com/bumptech/glide/i;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 108
    invoke-virtual {v2}, Lio/intercom/android/sdk/Injector;->getStore()Lio/intercom/android/sdk/store/Store;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->store:Lio/intercom/android/sdk/store/Store;

    .line 109
    invoke-virtual {v2}, Lio/intercom/android/sdk/Injector;->getAppConfigProvider()Lio/intercom/android/sdk/Provider;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 111
    invoke-virtual {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v8

    .line 112
    new-instance v4, Lio/intercom/android/sdk/utilities/TimeFormatter;

    invoke-virtual {v2}, Lio/intercom/android/sdk/Injector;->getTimeProvider()Lio/intercom/android/sdk/commons/utilities/TimeProvider;

    move-result-object v0

    invoke-direct {v4, v8, v0}, Lio/intercom/android/sdk/utilities/TimeFormatter;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/commons/utilities/TimeProvider;)V

    .line 113
    new-instance v0, Lio/intercom/android/sdk/inbox/InboxAdapter;

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v3, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->store:Lio/intercom/android/sdk/store/Store;

    iget-object v5, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 114
    invoke-virtual {v2}, Lio/intercom/android/sdk/Injector;->getUserIdentity()Lio/intercom/android/sdk/identity/UserIdentity;

    move-result-object v6

    iget-object v7, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->requestManager:Lio/intercom/com/bumptech/glide/i;

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lio/intercom/android/sdk/inbox/InboxAdapter;-><init>(Landroid/view/LayoutInflater;Lio/intercom/android/sdk/inbox/ConversationClickListener;Lio/intercom/android/sdk/store/Store;Lio/intercom/android/sdk/utilities/TimeFormatter;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/com/bumptech/glide/i;)V

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->adapter:Lio/intercom/android/sdk/inbox/InboxAdapter;

    .line 115
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, v8}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    .line 116
    new-instance v0, Lio/intercom/android/sdk/views/EndlessRecyclerScrollListener;

    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, v1, p0}, Lio/intercom/android/sdk/views/EndlessRecyclerScrollListener;-><init>(Landroid/support/v7/widget/LinearLayoutManager;Lio/intercom/android/sdk/views/EndlessScrollListener;)V

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->endlessRecyclerScrollListener:Lio/intercom/android/sdk/views/EndlessRecyclerScrollListener;

    .line 118
    invoke-virtual {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_0

    .line 120
    const-string v1, "is_two_pane"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->isTwoPane:Z

    .line 122
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 125
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v1, "inbox frag"

    const-string v2, "creating view for fragment"

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    if-nez v0, :cond_2

    .line 129
    sget v0, Lio/intercom/android/sdk/R$layout;->intercom_fragment_inbox:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    .line 130
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    sget v1, Lio/intercom/android/sdk/R$id;->progress_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 132
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    sget v1, Lio/intercom/android/sdk/R$id;->inbox_recycler_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    .line 133
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 134
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->endlessRecyclerScrollListener:Lio/intercom/android/sdk/views/EndlessRecyclerScrollListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$m;)V

    .line 135
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->adapter:Lio/intercom/android/sdk/inbox/InboxAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 137
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    sget v1, Lio/intercom/android/sdk/R$id;->compose_action_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/FloatingActionButton;

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->composerButton:Landroid/support/design/widget/FloatingActionButton;

    .line 138
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->composerButton:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0, p0}, Landroid/support/design/widget/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    sget v1, Lio/intercom/android/sdk/R$id;->error_layout_inbox:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/views/IntercomErrorView;

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    .line 141
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonTextColor(I)V

    .line 142
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxErrorView:Lio/intercom/android/sdk/views/IntercomErrorView;

    new-instance v1, Lio/intercom/android/sdk/inbox/InboxFragment$1;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/inbox/InboxFragment$1;-><init>(Lio/intercom/android/sdk/inbox/InboxFragment;)V

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/IntercomErrorView;->setActionButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    sget v1, Lio/intercom/android/sdk/R$id;->intercom_toolbar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/views/IntercomToolbar;

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    .line 149
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v0, p0}, Lio/intercom/android/sdk/views/IntercomToolbar;->setListener(Lio/intercom/android/sdk/views/IntercomToolbar$Listener;)V

    .line 150
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/views/IntercomToolbar;->setSubtitleVisibility(I)V

    .line 151
    iget-boolean v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->isTwoPane:Z

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->intercomToolbar:Lio/intercom/android/sdk/views/IntercomToolbar;

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/views/IntercomToolbar;->setCloseButtonVisibility(I)V

    .line 155
    :cond_0
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lio/intercom/android/sdk/inbox/InboxFragment$2;

    invoke-direct {v1, p0}, Lio/intercom/android/sdk/inbox/InboxFragment$2;-><init>(Lio/intercom/android/sdk/inbox/InboxFragment;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 169
    :cond_1
    :goto_0
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    return-object v0

    .line 163
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 164
    if-eqz v0, :cond_1

    .line 165
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->rootView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 222
    sget-object v0, Lio/intercom/android/sdk/inbox/InboxFragment$Listener;->EMPTY:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    .line 223
    return-void
.end method

.method public onInboxClicked()V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method public onLoadMore()V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->store:Lio/intercom/android/sdk/store/Store;

    invoke-virtual {v0}, Lio/intercom/android/sdk/store/Store;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/State;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/State;->inboxState()Lio/intercom/android/sdk/state/InboxState;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Lio/intercom/android/sdk/state/InboxState;->conversations()Ljava/util/List;

    move-result-object v1

    .line 268
    invoke-virtual {v0}, Lio/intercom/android/sdk/state/InboxState;->status()Lio/intercom/android/sdk/state/InboxState$Status;

    move-result-object v0

    sget-object v2, Lio/intercom/android/sdk/state/InboxState$Status;->LOADING:Lio/intercom/android/sdk/state/InboxState$Status;

    if-eq v0, v2, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getCreatedAt()J

    move-result-wide v0

    .line 272
    iget-object v2, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {v0, v1}, Lio/intercom/android/sdk/actions/Actions;->fetchInboxBeforeDateRequest(J)Lio/intercom/android/sdk/actions/Action;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {}, Lio/intercom/android/sdk/actions/Actions;->inboxOpened()Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 185
    iget-boolean v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->isTwoPane:Z

    if-nez v0, :cond_0

    .line 186
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAlpha(F)V

    .line 188
    :cond_0
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->setToolbarTitle()V

    .line 189
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->setColorScheme()V

    .line 191
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 192
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 178
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->INBOX:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1, p0}, Lio/intercom/android/sdk/store/Store;->subscribeToChanges(Lio/intercom/android/sdk/store/Store$Selector;Lio/intercom/android/sdk/store/Store$Subscriber;)Lio/intercom/android/sdk/store/Store$Subscription;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->subscription:Lio/intercom/android/sdk/store/Store$Subscription;

    .line 179
    return-void
.end method

.method public onStateChange(Lio/intercom/android/sdk/state/InboxState;)V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->adapter:Lio/intercom/android/sdk/inbox/InboxAdapter;

    invoke-virtual {v0, p1}, Lio/intercom/android/sdk/inbox/InboxAdapter;->setInboxState(Lio/intercom/android/sdk/state/InboxState;)V

    .line 296
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->adapter:Lio/intercom/android/sdk/inbox/InboxAdapter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/inbox/InboxAdapter;->notifyDataSetChanged()V

    .line 297
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->endlessRecyclerScrollListener:Lio/intercom/android/sdk/views/EndlessRecyclerScrollListener;

    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->hasMorePages()Z

    move-result v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/views/EndlessRecyclerScrollListener;->setMorePagesAvailable(Z)V

    .line 299
    invoke-virtual {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    sget-object v0, Lio/intercom/android/sdk/inbox/InboxFragment$5;->$SwitchMap$io$intercom$android$sdk$state$InboxState$Status:[I

    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->status()Lio/intercom/android/sdk/state/InboxState$Status;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/android/sdk/state/InboxState$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 327
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->displayErrorView()V

    goto :goto_0

    .line 305
    :pswitch_0
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->displayLoadingView()V

    goto :goto_0

    .line 308
    :pswitch_1
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->conversations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->displayLoadingView()V

    goto :goto_0

    .line 311
    :cond_2
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->displayInbox()V

    goto :goto_0

    .line 315
    :pswitch_2
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->o()I

    move-result v0

    if-nez v0, :cond_3

    .line 316
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->e(I)V

    .line 318
    :cond_3
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->conversations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 319
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->displayEmptyView()V

    .line 323
    :goto_1
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->listener:Lio/intercom/android/sdk/inbox/InboxFragment$Listener;

    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->conversations()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->status()Lio/intercom/android/sdk/state/InboxState$Status;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lio/intercom/android/sdk/inbox/InboxFragment$Listener;->onConversationsLoaded(Ljava/util/List;Lio/intercom/android/sdk/state/InboxState$Status;)V

    goto :goto_0

    .line 321
    :cond_4
    invoke-direct {p0}, Lio/intercom/android/sdk/inbox/InboxFragment;->displayInbox()V

    goto :goto_1

    .line 303
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onStateChange(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 52
    check-cast p1, Lio/intercom/android/sdk/state/InboxState;

    invoke-virtual {p0, p1}, Lio/intercom/android/sdk/inbox/InboxFragment;->onStateChange(Lio/intercom/android/sdk/state/InboxState;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->subscription:Lio/intercom/android/sdk/store/Store$Subscription;

    invoke-static {v0}, Lio/intercom/android/sdk/utilities/StoreUtils;->safeUnsubscribe(Lio/intercom/android/sdk/store/Store$Subscription;)V

    .line 217
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 218
    return-void
.end method

.method public onToolbarClicked()V
    .locals 0

    .prologue
    .line 285
    return-void
.end method

.method public setOverScrollColour()V
    .locals 2

    .prologue
    .line 276
    iget-object v1, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->inboxView:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lio/intercom/android/sdk/inbox/InboxFragment;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    invoke-static {v1, v0}, Lio/intercom/android/sdk/utilities/ViewUtils;->setOverScrollColour(Landroid/support/v7/widget/RecyclerView;I)V

    .line 277
    return-void
.end method
