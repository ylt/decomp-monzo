.class public Lio/intercom/android/sdk/twig/Twig;
.super Ljava/lang/Object;
.source "Twig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/twig/Twig$LogLevel;
    }
.end annotation


# static fields
.field public static final DISABLED:I = 0x8

.field private static final MAX_LOG_LENGTH:I = 0xfa0


# instance fields
.field private final internalLoggingEnabled:Z

.field private logLevel:I

.field private final tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p1, p0, Lio/intercom/android/sdk/twig/Twig;->logLevel:I

    .line 31
    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, Lio/intercom/android/sdk/twig/Twig;->tag:Ljava/lang/String;

    .line 32
    iput-boolean p3, p0, Lio/intercom/android/sdk/twig/Twig;->internalLoggingEnabled:Z

    .line 33
    return-void

    .line 31
    :cond_0
    const-string p2, "Twig"

    goto :goto_0
.end method

.method private getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 228
    new-instance v0, Ljava/io/StringWriter;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/io/StringWriter;-><init>(I)V

    .line 229
    new-instance v1, Ljava/io/PrintWriter;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V

    .line 230
    invoke-virtual {p1, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 231
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    .line 232
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private log(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 199
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xfa0

    if-ge v0, v1, :cond_1

    .line 200
    invoke-direct {p0, p1, p2, p3}, Lio/intercom/android/sdk/twig/Twig;->printLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_0
    return-void

    .line 205
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    if-ge v2, v1, :cond_0

    .line 206
    const/16 v0, 0xa

    invoke-virtual {p3, v0, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 207
    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 209
    :goto_1
    add-int/lit16 v3, v2, 0xfa0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 210
    invoke-virtual {p3, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 211
    invoke-direct {p0, p1, p2, v2}, Lio/intercom/android/sdk/twig/Twig;->printLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 213
    if-lt v3, v0, :cond_3

    .line 205
    add-int/lit8 v2, v3, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 207
    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private varargs prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 167
    iget v0, p0, Lio/intercom/android/sdk/twig/Twig;->logLevel:I

    if-ge p1, v0, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    invoke-virtual {p0}, Lio/intercom/android/sdk/twig/Twig;->getTag()Ljava/lang/String;

    move-result-object v1

    .line 173
    if-eqz p3, :cond_5

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    .line 174
    const/4 p3, 0x0

    move-object v0, p3

    .line 176
    :goto_1
    if-nez v0, :cond_3

    .line 177
    if-eqz p2, :cond_0

    .line 180
    invoke-direct {p0, p2}, Lio/intercom/android/sdk/twig/Twig;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 190
    :cond_2
    :goto_2
    invoke-direct {p0, p1, v1, v0}, Lio/intercom/android/sdk/twig/Twig;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :cond_3
    array-length v2, p4

    if-lez v2, :cond_4

    .line 183
    invoke-static {v0, p4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 185
    :cond_4
    if-eqz p2, :cond_2

    .line 186
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2}, Lio/intercom/android/sdk/twig/Twig;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v0, p3

    goto :goto_1
.end method

.method private printLog(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    .line 219
    invoke-static {p2, p3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :goto_0
    return-void

    .line 221
    :cond_0
    invoke-static {p1, p2, p3}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public d(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, p1, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method public varargs d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1, p2, p3}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method public varargs e(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 97
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    return-void
.end method

.method public e(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x6

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, p1, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    return-void
.end method

.method public varargs e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x6

    invoke-direct {p0, v0, p1, p2, p3}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    return-void
.end method

.method getLogLevel()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lio/intercom/android/sdk/twig/Twig;->logLevel:I

    return v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lio/intercom/android/sdk/twig/Twig;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public varargs i(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 67
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public i(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 77
    const/4 v0, 0x4

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, p1, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    return-void
.end method

.method public varargs i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1, p2, p3}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method public internal(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lio/intercom/android/sdk/twig/Twig;->tag:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public internal(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 134
    iget-boolean v0, p0, Lio/intercom/android/sdk/twig/Twig;->internalLoggingEnabled:Z

    if-eqz v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INTERNAL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_0
    return-void
.end method

.method varargs log(ILjava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    return-void
.end method

.method public setLogLevel(I)V
    .locals 0

    .prologue
    .line 150
    iput p1, p0, Lio/intercom/android/sdk/twig/Twig;->logLevel:I

    .line 151
    return-void
.end method

.method public varargs v(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 37
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method public v(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, p1, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    return-void
.end method

.method public varargs v(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2, p3}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method public varargs w(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 82
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method public w(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, p1, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    return-void
.end method

.method public varargs w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x5

    invoke-direct {p0, v0, p1, p2, p3}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    return-void
.end method

.method public varargs wtf(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 112
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    return-void
.end method

.method public wtf(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x7

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, p1, v1, v2}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    return-void
.end method

.method public varargs wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x7

    invoke-direct {p0, v0, p1, p2, p3}, Lio/intercom/android/sdk/twig/Twig;->prepareLog(ILjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    return-void
.end method
