.class public Lio/intercom/android/sdk/overlay/OverlayManager;
.super Ljava/lang/Object;
.source "OverlayManager.java"

# interfaces
.implements Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;
.implements Lio/intercom/android/sdk/store/Store$Subscriber2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;",
        "Lio/intercom/android/sdk/store/Store$Subscriber2",
        "<",
        "Ljava/lang/Integer;",
        "Lio/intercom/android/sdk/state/OverlayState;",
        ">;"
    }
.end annotation


# instance fields
.field private final appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final application:Landroid/app/Application;

.field private final handler:Landroid/os/Handler;

.field final inAppNotificationManager:Lio/intercom/android/sdk/overlay/InAppNotificationManager;

.field final launcherPresenter:Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;

.field private final metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

.field private final requestManager:Lio/intercom/com/bumptech/glide/i;

.field private final store:Lio/intercom/android/sdk/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;"
        }
    .end annotation
.end field

.field private final twig:Lio/intercom/android/sdk/twig/Twig;

.field private final userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lio/intercom/com/a/a/b;Lio/intercom/android/sdk/store/Store;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/metrics/MetricTracker;Lio/intercom/android/sdk/identity/UserIdentity;Lio/intercom/com/bumptech/glide/i;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lio/intercom/com/a/a/b;",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Lio/intercom/android/sdk/metrics/MetricTracker;",
            "Lio/intercom/android/sdk/identity/UserIdentity;",
            "Lio/intercom/com/bumptech/glide/i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 70
    iput-object p1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->application:Landroid/app/Application;

    .line 71
    iput-object p4, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 72
    iput-object p3, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->store:Lio/intercom/android/sdk/store/Store;

    .line 73
    iput-object p5, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    .line 74
    iput-object p6, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 75
    iput-object p7, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 76
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 78
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->handler:Landroid/os/Handler;

    .line 79
    new-instance v0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;

    iget-object v2, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->handler:Landroid/os/Handler;

    new-instance v6, Lio/intercom/android/sdk/utilities/ContextLocaliser;

    invoke-direct {v6, p4}, Lio/intercom/android/sdk/utilities/ContextLocaliser;-><init>(Lio/intercom/android/sdk/Provider;)V

    move-object v3, p0

    move-object v4, p5

    move-object v5, p4

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;-><init>(Landroid/view/LayoutInflater;Landroid/os/Handler;Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;Lio/intercom/android/sdk/metrics/MetricTracker;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/utilities/ContextLocaliser;Lio/intercom/com/bumptech/glide/i;)V

    iput-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->inAppNotificationManager:Lio/intercom/android/sdk/overlay/InAppNotificationManager;

    .line 81
    new-instance v0, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour;

    sget-object v2, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;->DEFAULT:Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;

    invoke-direct {v0, p4, p3, v2, p5}, Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour;-><init>(Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/store/Store;Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour$LauncherType;Lio/intercom/android/sdk/metrics/MetricTracker;)V

    .line 83
    new-instance v2, Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;

    invoke-direct {v2, v1, v0, p5}, Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;-><init>(Landroid/view/LayoutInflater;Lio/intercom/android/sdk/overlay/LauncherOpenBehaviour;Lio/intercom/android/sdk/metrics/MetricTracker;)V

    iput-object v2, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->launcherPresenter:Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;

    .line 85
    invoke-virtual {p2, p0}, Lio/intercom/com/a/a/b;->register(Ljava/lang/Object;)V

    .line 87
    sget-object v0, Lio/intercom/android/sdk/store/Selectors;->UNREAD_COUNT:Lio/intercom/android/sdk/store/Store$Selector;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->OVERLAY:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {p3, v0, v1, p0}, Lio/intercom/android/sdk/store/Store;->subscribeToChanges(Lio/intercom/android/sdk/store/Store$Selector;Lio/intercom/android/sdk/store/Store$Selector;Lio/intercom/android/sdk/store/Store$Subscriber2;)Lio/intercom/android/sdk/store/Store$Subscription;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/overlay/OverlayManager;)Lio/intercom/android/sdk/Provider;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    return-object v0
.end method

.method static synthetic access$100(Lio/intercom/android/sdk/overlay/OverlayManager;)Lio/intercom/android/sdk/identity/UserIdentity;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    return-object v0
.end method

.method private getRootView(Landroid/app/Activity;)Landroid/view/ViewGroup;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 271
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_overlay_root:I

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 272
    if-nez v0, :cond_0

    .line 273
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 274
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 275
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 276
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFitsSystemWindows(Z)V

    .line 277
    sget v1, Lio/intercom/android/sdk/R$id;->intercom_overlay_root:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    .line 278
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x50

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 281
    :cond_0
    return-object v0
.end method

.method private openMessenger(Lio/intercom/android/sdk/models/Conversation;)V
    .locals 4

    .prologue
    .line 310
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->application:Landroid/app/Application;

    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->application:Landroid/app/Application;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    .line 311
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getLastParticipatingAdmin()Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    move-result-object v3

    .line 310
    invoke-static {v1, v2, v3}, Lio/intercom/android/sdk/activities/IntercomMessengerActivity;->openConversation(Landroid/content/Context;Ljava/lang/String;Lio/intercom/android/sdk/models/LastParticipatingAdmin;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Application;->startActivity(Landroid/content/Intent;)V

    .line 312
    return-void
.end method

.method private openNote(Lio/intercom/android/sdk/models/Conversation;Z)V
    .locals 5

    .prologue
    .line 327
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    .line 328
    if-eqz p2, :cond_0

    .line 329
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/intercom/android/sdk/metrics/MetricTracker;->viewedInAppFromSnippet(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :goto_0
    :try_start_0
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->application:Landroid/app/Application;

    iget-object v2, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->application:Landroid/app/Application;

    .line 335
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getLastParticipatingAdmin()Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    move-result-object v4

    .line 334
    invoke-static {v2, v0, v3, v4}, Lio/intercom/android/sdk/activities/IntercomNoteActivity;->buildNoteIntent(Landroid/content/Context;Lio/intercom/android/sdk/models/Part;Ljava/lang/String;Lio/intercom/android/sdk/models/LastParticipatingAdmin;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Application;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_1
    return-void

    .line 331
    :cond_0
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/intercom/android/sdk/metrics/MetricTracker;->viewedInAppFromFull(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 336
    :catch_0
    move-exception v0

    .line 337
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->twig:Lio/intercom/android/sdk/twig/Twig;

    const-string v2, "Overlay"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error loading the note "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private openPost(Lio/intercom/android/sdk/models/Conversation;Z)V
    .locals 6

    .prologue
    .line 315
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    .line 316
    if-eqz p2, :cond_0

    .line 317
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/metrics/MetricTracker;->viewedInAppFromSnippet(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :goto_0
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->application:Landroid/app/Application;

    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->application:Landroid/app/Application;

    .line 323
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v2

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getLastParticipatingAdmin()Lio/intercom/android/sdk/models/LastParticipatingAdmin;

    move-result-object v4

    const/4 v5, 0x1

    .line 322
    invoke-static {v1, v2, v3, v4, v5}, Lio/intercom/android/sdk/activities/IntercomPostActivity;->buildPostIntent(Landroid/content/Context;Lio/intercom/android/sdk/models/Part;Ljava/lang/String;Lio/intercom/android/sdk/models/LastParticipatingAdmin;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Application;->startActivity(Landroid/content/Intent;)V

    .line 324
    return-void

    .line 319
    :cond_0
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/metrics/MetricTracker;->viewedInAppFromFull(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelAnimations()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 182
    return-void
.end method

.method public configUpdated(Lio/intercom/android/sdk/models/events/ConfigUpdateEvent;)V
    .locals 4
    .annotation runtime Lio/intercom/com/a/a/h;
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->OVERLAY:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/OverlayState;

    .line 163
    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->resumedHostActivity()Landroid/app/Activity;

    move-result-object v1

    .line 164
    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->conversations()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->notificationVisibility()Lio/intercom/android/sdk/Intercom$Visibility;

    move-result-object v3

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->launcherVisibility()Lio/intercom/android/sdk/Intercom$Visibility;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lio/intercom/android/sdk/overlay/OverlayManager;->shouldDisplayLauncher(Ljava/util/List;Lio/intercom/android/sdk/Intercom$Visibility;Lio/intercom/android/sdk/Intercom$Visibility;Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/overlay/OverlayManager;->getRootView(Landroid/app/Activity;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 167
    new-instance v1, Lio/intercom/android/sdk/overlay/OverlayManager$2;

    invoke-direct {v1, p0, v0}, Lio/intercom/android/sdk/overlay/OverlayManager$2;-><init>(Lio/intercom/android/sdk/overlay/OverlayManager;Landroid/view/ViewGroup;)V

    invoke-static {v0, v1}, Lio/intercom/android/sdk/utilities/ViewUtils;->waitForViewAttachment(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 176
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->launcherPresenter:Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;

    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->getBaseColor()I

    move-result v0

    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;->setLauncherBackgroundColor(I)V

    goto :goto_0
.end method

.method displayNotifications(Ljava/util/List;Landroid/app/Activity;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Landroid/app/Activity;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 215
    invoke-direct {p0, p2}, Lio/intercom/android/sdk/overlay/OverlayManager;->getRootView(Landroid/app/Activity;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 216
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Conversation;

    .line 217
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v2

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v2

    .line 218
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v3

    invoke-virtual {v3}, Lio/intercom/android/sdk/models/Part;->getDeliveryOption()Lio/intercom/android/sdk/models/Part$DeliveryOption;

    move-result-object v3

    .line 223
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-gt v4, v6, :cond_0

    iget-object v4, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->inAppNotificationManager:Lio/intercom/android/sdk/overlay/InAppNotificationManager;

    .line 224
    invoke-virtual {v4}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->isDisplaying()Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lio/intercom/android/sdk/models/Part$DeliveryOption;->SUMMARY:Lio/intercom/android/sdk/models/Part$DeliveryOption;

    if-eq v3, v4, :cond_0

    sget-object v4, Lio/intercom/android/sdk/models/Part$DeliveryOption;->FULL:Lio/intercom/android/sdk/models/Part$DeliveryOption;

    if-ne v3, v4, :cond_4

    const-string v4, "chat"

    .line 226
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 228
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->launcherPresenter:Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;->isDisplaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->launcherPresenter:Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;

    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;->getAndUnsetLauncher()Lio/intercom/android/sdk/overlay/DefaultLauncher;

    move-result-object v0

    .line 230
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v6, :cond_2

    .line 231
    new-instance v2, Lio/intercom/android/sdk/overlay/OverlayManager$5;

    invoke-direct {v2, p0, v0, v1, p1}, Lio/intercom/android/sdk/overlay/OverlayManager$5;-><init>(Lio/intercom/android/sdk/overlay/OverlayManager;Lio/intercom/android/sdk/overlay/DefaultLauncher;Landroid/view/ViewGroup;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/overlay/DefaultLauncher;->pulseForTransformation(Landroid/animation/Animator$AnimatorListener;)V

    .line 259
    :cond_1
    :goto_0
    return-void

    .line 242
    :cond_2
    new-instance v2, Lio/intercom/android/sdk/overlay/OverlayManager$6;

    invoke-direct {v2, p0, v0, v1, p1}, Lio/intercom/android/sdk/overlay/OverlayManager$6;-><init>(Lio/intercom/android/sdk/overlay/OverlayManager;Lio/intercom/android/sdk/overlay/DefaultLauncher;Landroid/view/ViewGroup;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/overlay/DefaultLauncher;->fadeOffScreen(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 250
    :cond_3
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->inAppNotificationManager:Lio/intercom/android/sdk/overlay/InAppNotificationManager;

    invoke-virtual {v0, v1, p1}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->displayNotifications(Landroid/view/ViewGroup;Ljava/util/List;)V

    goto :goto_0

    .line 252
    :cond_4
    sget-object v1, Lio/intercom/android/sdk/models/Part$DeliveryOption;->FULL:Lio/intercom/android/sdk/models/Part$DeliveryOption;

    if-ne v3, v1, :cond_1

    .line 253
    const-string v1, "post"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 254
    invoke-direct {p0, v0, v5}, Lio/intercom/android/sdk/overlay/OverlayManager;->openPost(Lio/intercom/android/sdk/models/Conversation;Z)V

    goto :goto_0

    .line 255
    :cond_5
    const-string v1, "note"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 256
    invoke-direct {p0, v0, v5}, Lio/intercom/android/sdk/overlay/OverlayManager;->openNote(Lio/intercom/android/sdk/models/Conversation;Z)V

    goto :goto_0
.end method

.method public getRootView()Landroid/view/ViewGroup;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->OVERLAY:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/OverlayState;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->resumedHostActivity()Landroid/app/Activity;

    move-result-object v0

    .line 263
    if-nez v0, :cond_0

    .line 264
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 267
    :cond_0
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/overlay/OverlayManager;->getRootView(Landroid/app/Activity;)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public markAsDismissed(Lio/intercom/android/sdk/models/Conversation;)V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {p1}, Lio/intercom/android/sdk/actions/Actions;->conversationMarkedAsDismissed(Lio/intercom/android/sdk/models/Conversation;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;

    .line 287
    return-void
.end method

.method public onStateChange(Ljava/lang/Integer;Lio/intercom/android/sdk/state/OverlayState;)V
    .locals 5

    .prologue
    .line 91
    invoke-virtual {p2}, Lio/intercom/android/sdk/state/OverlayState;->notificationVisibility()Lio/intercom/android/sdk/Intercom$Visibility;

    move-result-object v0

    .line 92
    invoke-virtual {p2}, Lio/intercom/android/sdk/state/OverlayState;->resumedHostActivity()Landroid/app/Activity;

    move-result-object v1

    .line 93
    invoke-virtual {p2}, Lio/intercom/android/sdk/state/OverlayState;->conversations()Ljava/util/List;

    move-result-object v2

    .line 95
    iget-object v3, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->launcherPresenter:Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;

    invoke-virtual {p2}, Lio/intercom/android/sdk/state/OverlayState;->bottomPadding()I

    move-result v4

    invoke-virtual {v3, v4}, Lio/intercom/android/sdk/overlay/DefaultLauncherPresenter;->setBottomPadding(I)V

    .line 96
    iget-object v3, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->inAppNotificationManager:Lio/intercom/android/sdk/overlay/InAppNotificationManager;

    invoke-virtual {p2}, Lio/intercom/android/sdk/state/OverlayState;->bottomPadding()I

    move-result v4

    invoke-virtual {v3, v4}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->setBottomPadding(I)V

    .line 97
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_1

    .line 98
    invoke-virtual {v1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    invoke-virtual {p0, v2, v0, v1}, Lio/intercom/android/sdk/overlay/OverlayManager;->shouldDisplayNotifications(Ljava/util/List;Lio/intercom/android/sdk/Intercom$Visibility;Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 103
    iget-object v3, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 104
    invoke-virtual {v3}, Lio/intercom/android/sdk/identity/UserIdentity;->getFingerprint()Ljava/lang/String;

    move-result-object v3

    .line 103
    invoke-virtual {p0, v2, v0, v1, v3}, Lio/intercom/android/sdk/overlay/OverlayManager;->preloadAvatarThenDisplayNotifications(Ljava/util/List;Lio/intercom/android/sdk/Intercom$Visibility;Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {p2}, Lio/intercom/android/sdk/state/OverlayState;->launcherVisibility()Lio/intercom/android/sdk/Intercom$Visibility;

    move-result-object v3

    invoke-virtual {p0, v2, v0, v3, v1}, Lio/intercom/android/sdk/overlay/OverlayManager;->shouldDisplayLauncher(Ljava/util/List;Lio/intercom/android/sdk/Intercom$Visibility;Lio/intercom/android/sdk/Intercom$Visibility;Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/overlay/OverlayManager;->getRootView(Landroid/app/Activity;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 107
    new-instance v1, Lio/intercom/android/sdk/overlay/OverlayManager$1;

    invoke-direct {v1, p0, v0, p1}, Lio/intercom/android/sdk/overlay/OverlayManager$1;-><init>(Lio/intercom/android/sdk/overlay/OverlayManager;Landroid/view/ViewGroup;Ljava/lang/Integer;)V

    invoke-static {v0, v1}, Lio/intercom/android/sdk/utilities/ViewUtils;->waitForViewAttachment(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 116
    :cond_3
    invoke-virtual {p0, v1}, Lio/intercom/android/sdk/overlay/OverlayManager;->removeOverlaysIfPresent(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public bridge synthetic onStateChange(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 53
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Lio/intercom/android/sdk/state/OverlayState;

    invoke-virtual {p0, p1, p2}, Lio/intercom/android/sdk/overlay/OverlayManager;->onStateChange(Ljava/lang/Integer;Lio/intercom/android/sdk/state/OverlayState;)V

    return-void
.end method

.method public openNotification(Lio/intercom/android/sdk/models/Conversation;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 290
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getLastAdminPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v1

    .line 293
    const-string v2, "post"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    invoke-direct {p0, p1, v3}, Lio/intercom/android/sdk/overlay/OverlayManager;->openPost(Lio/intercom/android/sdk/models/Conversation;Z)V

    .line 307
    :goto_0
    return-void

    .line 295
    :cond_0
    const-string v2, "note"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 296
    invoke-direct {p0, p1, v3}, Lio/intercom/android/sdk/overlay/OverlayManager;->openNote(Lio/intercom/android/sdk/models/Conversation;Z)V

    goto :goto_0

    .line 298
    :cond_1
    invoke-direct {p0, p1}, Lio/intercom/android/sdk/overlay/OverlayManager;->openMessenger(Lio/intercom/android/sdk/models/Conversation;)V

    .line 300
    const-string v2, "chat"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lio/intercom/android/sdk/models/Part$DeliveryOption;->FULL:Lio/intercom/android/sdk/models/Part$DeliveryOption;

    .line 301
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getDeliveryOption()Lio/intercom/android/sdk/models/Part$DeliveryOption;

    move-result-object v2

    if-ne v1, v2, :cond_2

    .line 302
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/metrics/MetricTracker;->openedConversationFromFull(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :cond_2
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/metrics/MetricTracker;->openedConversationFromSnippet(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method preloadAvatarThenDisplayNotifications(Ljava/util/List;Lio/intercom/android/sdk/Intercom$Visibility;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Lio/intercom/android/sdk/Intercom$Visibility;",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 203
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getLastAdmin()Lio/intercom/android/sdk/models/Participant;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Participant;->getAvatar()Lio/intercom/android/sdk/models/Avatar;

    move-result-object v6

    new-instance v0, Lio/intercom/android/sdk/overlay/OverlayManager$4;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lio/intercom/android/sdk/overlay/OverlayManager$4;-><init>(Lio/intercom/android/sdk/overlay/OverlayManager;Ljava/lang/String;Ljava/util/List;Lio/intercom/android/sdk/Intercom$Visibility;Landroid/app/Activity;)V

    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->requestManager:Lio/intercom/com/bumptech/glide/i;

    invoke-static {v6, v0, v1}, Lio/intercom/android/sdk/utilities/AvatarUtils;->preloadAvatar(Lio/intercom/android/sdk/models/Avatar;Ljava/lang/Runnable;Lio/intercom/com/bumptech/glide/i;)V

    .line 212
    return-void
.end method

.method public refreshStateBecauseUserIdentityIsNotInStoreYet()V
    .locals 3

    .prologue
    .line 367
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->UNREAD_COUNT:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v2, Lio/intercom/android/sdk/store/Selectors;->OVERLAY:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/intercom/android/sdk/state/OverlayState;

    invoke-virtual {p0, v0, v1}, Lio/intercom/android/sdk/overlay/OverlayManager;->onStateChange(Ljava/lang/Integer;Lio/intercom/android/sdk/state/OverlayState;)V

    .line 368
    return-void
.end method

.method public removeOverlaysIfPresent(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 185
    sget v0, Lio/intercom/android/sdk/R$id;->intercom_overlay_root:I

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 186
    new-instance v1, Lio/intercom/android/sdk/overlay/OverlayManager$3;

    invoke-direct {v1, p0, v0}, Lio/intercom/android/sdk/overlay/OverlayManager$3;-><init>(Lio/intercom/android/sdk/overlay/OverlayManager;Landroid/view/View;)V

    invoke-virtual {p1, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 197
    return-void
.end method

.method public sendUnreadConversationsReceivedMetric(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/intercom/android/sdk/models/Conversation;

    .line 136
    invoke-virtual {v3}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v5

    .line 138
    sget-object v0, Lio/intercom/android/sdk/models/Part$DeliveryOption;->SUMMARY:Lio/intercom/android/sdk/models/Part$DeliveryOption;

    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->getDeliveryOption()Lio/intercom/android/sdk/models/Part$DeliveryOption;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 139
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->isInitialMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->hasAttachments()Z

    move-result v1

    .line 141
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->isLinkCard()Z

    move-result v2

    .line 142
    invoke-virtual {v3}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v3

    .line 143
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v4

    .line 144
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v5

    .line 140
    invoke-virtual/range {v0 .. v5}, Lio/intercom/android/sdk/metrics/MetricTracker;->receivedMessageFromSnippetWhenClosed(ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 146
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->hasAttachments()Z

    move-result v1

    .line 147
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->isLinkCard()Z

    move-result v2

    .line 148
    invoke-virtual {v3}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v3

    .line 149
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v4

    .line 146
    invoke-virtual {v0, v1, v2, v3, v4}, Lio/intercom/android/sdk/metrics/MetricTracker;->receivedReplyFromSnippetWhenClosed(ZZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->hasAttachments()Z

    move-result v1

    .line 153
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->isLinkCard()Z

    move-result v2

    .line 154
    invoke-virtual {v3}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v3

    .line 155
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v4

    .line 156
    invoke-virtual {v5}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v5

    .line 152
    invoke-virtual/range {v0 .. v5}, Lio/intercom/android/sdk/metrics/MetricTracker;->receivedMessageFromFullWhenClosed(ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :cond_2
    return-void
.end method

.method shouldDisplayLauncher(Ljava/util/List;Lio/intercom/android/sdk/Intercom$Visibility;Lio/intercom/android/sdk/Intercom$Visibility;Landroid/app/Activity;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Lio/intercom/android/sdk/Intercom$Visibility;",
            "Lio/intercom/android/sdk/Intercom$Visibility;",
            "Landroid/app/Activity;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 351
    sget-object v0, Lio/intercom/android/sdk/Intercom$Visibility;->VISIBLE:Lio/intercom/android/sdk/Intercom$Visibility;

    if-ne p2, v0, :cond_1

    .line 352
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Conversation;

    .line 353
    sget-object v3, Lio/intercom/android/sdk/models/Part$DeliveryOption;->BADGE:Lio/intercom/android/sdk/models/Part$DeliveryOption;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getDeliveryOption()Lio/intercom/android/sdk/models/Part$DeliveryOption;

    move-result-object v0

    if-eq v3, v0, :cond_0

    .line 359
    :goto_0
    return v1

    :cond_1
    sget-object v0, Lio/intercom/android/sdk/Intercom$Visibility;->VISIBLE:Lio/intercom/android/sdk/Intercom$Visibility;

    if-ne p3, v0, :cond_2

    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 360
    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->isSoftReset()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 361
    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->identityExists()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 362
    invoke-interface {v0}, Lio/intercom/android/sdk/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/identity/AppConfig;

    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/AppConfig;->isReceivedFromServer()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p4, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 359
    goto :goto_0

    :cond_2
    move v0, v1

    .line 362
    goto :goto_1
.end method

.method shouldDisplayNotifications(Ljava/util/List;Lio/intercom/android/sdk/Intercom$Visibility;Landroid/app/Activity;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Lio/intercom/android/sdk/Intercom$Visibility;",
            "Landroid/app/Activity;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 343
    sget-object v0, Lio/intercom/android/sdk/Intercom$Visibility;->VISIBLE:Lio/intercom/android/sdk/Intercom$Visibility;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->userIdentity:Lio/intercom/android/sdk/identity/UserIdentity;

    .line 344
    invoke-virtual {v0}, Lio/intercom/android/sdk/identity/UserIdentity;->isSoftReset()Z

    move-result v0

    if-nez v0, :cond_0

    .line 345
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    .line 343
    :goto_0
    return v0

    .line 345
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public softReset()V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lio/intercom/android/sdk/overlay/OverlayManager;->cancelAnimations()V

    .line 124
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager;->store:Lio/intercom/android/sdk/store/Store;

    sget-object v1, Lio/intercom/android/sdk/store/Selectors;->OVERLAY:Lio/intercom/android/sdk/store/Store$Selector;

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/store/Store;->select(Lio/intercom/android/sdk/store/Store$Selector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/state/OverlayState;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/OverlayState;->resumedHostActivity()Landroid/app/Activity;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0, v0}, Lio/intercom/android/sdk/overlay/OverlayManager;->removeOverlaysIfPresent(Landroid/app/Activity;)V

    .line 128
    :cond_0
    return-void
.end method
