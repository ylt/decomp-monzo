.class public interface abstract Lio/intercom/android/sdk/overlay/ScreenshotEventListener;
.super Ljava/lang/Object;
.source "ScreenshotEventListener.java"


# virtual methods
.method public abstract onScreenshotDeleted(Landroid/net/Uri;)V
.end method

.method public abstract onScreenshotFolderDeleted()V
.end method

.method public abstract onScreenshotTaken(Landroid/net/Uri;)V
.end method
