.class Lio/intercom/android/sdk/overlay/InAppNotificationManager;
.super Ljava/lang/Object;
.source "InAppNotificationManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lio/intercom/android/sdk/overlay/InAppNotification$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;
    }
.end annotation


# static fields
.field private static final MAX_DISPLAYED_NOTIFICATIONS:I = 0x3

.field private static final PILL_START_OFFSET_DP:I = 0x42


# instance fields
.field private final appConfigProvider:Lio/intercom/android/sdk/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;"
        }
    .end annotation
.end field

.field private bottomPadding:I

.field private final contextLocaliser:Lio/intercom/android/sdk/utilities/ContextLocaliser;

.field final handler:Landroid/os/Handler;

.field private hasAnimated:Z

.field private final inflater:Landroid/view/LayoutInflater;

.field private final lastDisplayedConversations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;"
        }
    .end annotation
.end field

.field private final listener:Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;

.field private final metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

.field private final notifications:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/intercom/android/sdk/overlay/InAppNotification;",
            ">;"
        }
    .end annotation
.end field

.field private pillPosition:F

.field private final requestManager:Lio/intercom/com/bumptech/glide/i;

.field private final twig:Lio/intercom/android/sdk/twig/Twig;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/os/Handler;Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;Lio/intercom/android/sdk/metrics/MetricTracker;Lio/intercom/android/sdk/Provider;Lio/intercom/android/sdk/utilities/ContextLocaliser;Lio/intercom/com/bumptech/glide/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Landroid/os/Handler;",
            "Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;",
            "Lio/intercom/android/sdk/metrics/MetricTracker;",
            "Lio/intercom/android/sdk/Provider",
            "<",
            "Lio/intercom/android/sdk/identity/AppConfig;",
            ">;",
            "Lio/intercom/android/sdk/utilities/ContextLocaliser;",
            "Lio/intercom/com/bumptech/glide/i;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->lastDisplayedConversations:Ljava/util/List;

    .line 56
    iput-boolean v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->hasAnimated:Z

    .line 60
    iput v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->bottomPadding:I

    .line 67
    iput-object p1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->inflater:Landroid/view/LayoutInflater;

    .line 68
    iput-object p3, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->listener:Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;

    .line 69
    iput-object p2, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->handler:Landroid/os/Handler;

    .line 70
    iput-object p4, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    .line 71
    iput-object p5, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    .line 72
    iput-object p6, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->contextLocaliser:Lio/intercom/android/sdk/utilities/ContextLocaliser;

    .line 73
    iput-object p7, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->requestManager:Lio/intercom/com/bumptech/glide/i;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lio/intercom/android/sdk/overlay/InAppNotificationManager;)F
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->pillPosition:F

    return v0
.end method

.method private addNewNotifications(Ljava/util/List;Landroid/view/ViewGroup;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 166
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    move v3, v9

    :goto_0
    if-ge v3, v10, :cond_5

    .line 167
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/intercom/android/sdk/models/Conversation;

    .line 169
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->contextLocaliser:Lio/intercom/android/sdk/utilities/ContextLocaliser;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/utilities/ContextLocaliser;->createLocalisedContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 170
    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getLastPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    .line 171
    const/4 v4, 0x2

    if-ge v10, v4, :cond_0

    const-string v4, "chat"

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 172
    :cond_0
    new-instance v0, Lio/intercom/android/sdk/overlay/StackableSnippet;

    iget-object v4, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->handler:Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    iget-object v7, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v8, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->requestManager:Lio/intercom/com/bumptech/glide/i;

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lio/intercom/android/sdk/overlay/StackableSnippet;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/models/Conversation;ILandroid/os/Handler;ILio/intercom/android/sdk/overlay/InAppNotification$Listener;Lio/intercom/android/sdk/Provider;Lio/intercom/com/bumptech/glide/i;)V

    .line 182
    :goto_1
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    const/4 v1, 0x3

    if-ge v3, v1, :cond_1

    .line 184
    iget-object v2, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->inflater:Landroid/view/LayoutInflater;

    iget-boolean v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->hasAnimated:Z

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    iget v4, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->bottomPadding:I

    invoke-virtual {v0, p2, v2, v1, v4}, Lio/intercom/android/sdk/overlay/InAppNotification;->display(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;ZI)V

    .line 166
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 174
    :cond_2
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getDeliveryOption()Lio/intercom/android/sdk/models/Part$DeliveryOption;

    move-result-object v0

    sget-object v4, Lio/intercom/android/sdk/models/Part$DeliveryOption;->SUMMARY:Lio/intercom/android/sdk/models/Part$DeliveryOption;

    if-ne v0, v4, :cond_3

    .line 175
    new-instance v0, Lio/intercom/android/sdk/overlay/ChatSnippet;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget-object v6, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v7, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->requestManager:Lio/intercom/com/bumptech/glide/i;

    move-object v5, p0

    invoke-direct/range {v0 .. v7}, Lio/intercom/android/sdk/overlay/ChatSnippet;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/models/Conversation;IILio/intercom/android/sdk/overlay/InAppNotification$Listener;Lio/intercom/android/sdk/Provider;Lio/intercom/com/bumptech/glide/i;)V

    goto :goto_1

    .line 178
    :cond_3
    new-instance v0, Lio/intercom/android/sdk/overlay/ChatFull;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget-object v6, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    iget-object v7, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v8, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->requestManager:Lio/intercom/com/bumptech/glide/i;

    move-object v5, p0

    invoke-direct/range {v0 .. v8}, Lio/intercom/android/sdk/overlay/ChatFull;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/models/Conversation;IILio/intercom/android/sdk/overlay/InAppNotification$Listener;Lio/intercom/android/sdk/metrics/MetricTracker;Lio/intercom/android/sdk/Provider;Lio/intercom/com/bumptech/glide/i;)V

    goto :goto_1

    :cond_4
    move v1, v9

    .line 184
    goto :goto_2

    .line 187
    :cond_5
    return-void
.end method

.method private animatePill(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 303
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->handler:Landroid/os/Handler;

    new-instance v1, Lio/intercom/android/sdk/overlay/InAppNotificationManager$4;

    invoke-direct {v1, p0, p1}, Lio/intercom/android/sdk/overlay/InAppNotificationManager$4;-><init>(Lio/intercom/android/sdk/overlay/InAppNotificationManager;Landroid/view/View;)V

    const/high16 v2, 0x43fa0000    # 500.0f

    .line 314
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lio/intercom/android/sdk/utilities/SystemSettings;->getTransitionScale(Landroid/content/Context;)F

    move-result v3

    mul-float/2addr v2, v3

    float-to-long v2, v2

    .line 303
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 315
    return-void
.end method

.method private displayPill(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    const v3, 0x3ecccccd    # 0.4f

    .line 278
    sget v0, Lio/intercom/android/sdk/R$id;->notification_pill:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 279
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    .line 280
    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    if-nez v0, :cond_2

    .line 285
    sget v0, Lio/intercom/android/sdk/R$layout;->intercom_notification_pill:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 286
    sget v0, Lio/intercom/android/sdk/R$id;->notification_pill:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 287
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 288
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setScaleX(F)V

    .line 289
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setScaleY(F)V

    .line 290
    iget v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->pillPosition:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setY(F)V

    .line 291
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->animatePill(Landroid/view/View;)V

    .line 295
    :cond_2
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->contextLocaliser:Lio/intercom/android/sdk/utilities/ContextLocaliser;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/intercom/android/sdk/utilities/ContextLocaliser;->createLocalisedContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 296
    sget v2, Lio/intercom/android/sdk/R$string;->intercom_plus_x_more:I

    invoke-static {v1, v2}, Lio/intercom/android/sdk/utilities/Phrase;->from(Landroid/content/Context;I)Lio/intercom/android/sdk/utilities/Phrase;

    move-result-object v1

    const-string v2, "n"

    iget-object v3, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    .line 297
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    invoke-virtual {v1, v2, v3}, Lio/intercom/android/sdk/utilities/Phrase;->put(Ljava/lang/String;I)Lio/intercom/android/sdk/utilities/Phrase;

    move-result-object v1

    .line 298
    invoke-virtual {v1, v0}, Lio/intercom/android/sdk/utilities/Phrase;->into(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method private isReply(Lio/intercom/android/sdk/models/Conversation;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 326
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->lastDisplayedConversations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->lastDisplayedConversations:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/models/Conversation;

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 326
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 327
    goto :goto_0
.end method

.method private updateNotifications(Ljava/util/List;Ljava/util/Map;Landroid/view/ViewGroup;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/intercom/android/sdk/overlay/InAppNotification;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 192
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/intercom/android/sdk/models/Conversation;

    .line 194
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 195
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/InAppNotification;

    move-object v6, v0

    .line 196
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/InAppNotification;

    .line 198
    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/InAppNotification;->getPosition()I

    move-result v3

    invoke-virtual {v6}, Lio/intercom/android/sdk/overlay/InAppNotification;->getPosition()I

    move-result v4

    if-ge v3, v4, :cond_5

    :goto_1
    move-object v6, v0

    .line 201
    goto :goto_0

    .line 203
    :cond_0
    invoke-direct {p0, v2}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->isReply(Lio/intercom/android/sdk/models/Conversation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    new-instance v0, Lio/intercom/android/sdk/overlay/InAppNotificationManager$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lio/intercom/android/sdk/overlay/InAppNotificationManager$2;-><init>(Lio/intercom/android/sdk/overlay/InAppNotificationManager;Ljava/util/List;Ljava/util/Map;Landroid/view/ViewGroup;)V

    invoke-virtual {v6, v2, v0}, Lio/intercom/android/sdk/overlay/InAppNotification;->update(Lio/intercom/android/sdk/models/Conversation;Ljava/lang/Runnable;)V

    .line 234
    :goto_2
    return-void

    .line 210
    :cond_1
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    instance-of v0, v6, Lio/intercom/android/sdk/overlay/ChatSnippet;

    if-eqz v0, :cond_2

    .line 212
    new-instance v0, Lio/intercom/android/sdk/overlay/InAppNotificationManager$3;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lio/intercom/android/sdk/overlay/InAppNotificationManager$3;-><init>(Lio/intercom/android/sdk/overlay/InAppNotificationManager;Lio/intercom/android/sdk/models/Conversation;Landroid/view/ViewGroup;Ljava/util/Map;Ljava/util/List;)V

    invoke-virtual {v6, p3, v0}, Lio/intercom/android/sdk/overlay/InAppNotification;->moveBackward(Landroid/view/ViewGroup;Landroid/animation/AnimatorListenerAdapter;)V

    goto :goto_2

    .line 223
    :cond_2
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/InAppNotification;

    .line 224
    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/InAppNotification;->getPosition()I

    move-result v3

    const/4 v4, 0x3

    if-ge v3, v4, :cond_3

    .line 225
    invoke-virtual {v0, p3}, Lio/intercom/android/sdk/overlay/InAppNotification;->moveBackward(Landroid/view/ViewGroup;)V

    goto :goto_3

    .line 227
    :cond_3
    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/InAppNotification;->getPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Lio/intercom/android/sdk/overlay/InAppNotification;->setPosition(I)V

    goto :goto_3

    .line 230
    :cond_4
    invoke-virtual {p0, v2, p3, p2}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->addNewView(Lio/intercom/android/sdk/models/Conversation;Landroid/view/ViewGroup;Ljava/util/Map;)V

    .line 231
    invoke-virtual {p0, p1, p2, p3}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->syncUpdates(Ljava/util/List;Ljava/util/Map;Landroid/view/ViewGroup;)V

    goto :goto_2

    :cond_5
    move-object v0, v6

    goto :goto_1
.end method

.method private updatePillPosition(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 318
    const-string v0, "y"

    const/4 v1, 0x2

    new-array v1, v1, [F

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    aput v2, v1, v3

    iget v2, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->pillPosition:F

    aput v2, v1, v4

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 320
    new-array v1, v4, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v1, v3

    invoke-static {p1, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    .line 321
    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 323
    return-void
.end method


# virtual methods
.method addNewView(Lio/intercom/android/sdk/models/Conversation;Landroid/view/ViewGroup;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/models/Conversation;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/intercom/android/sdk/overlay/InAppNotification;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->contextLocaliser:Lio/intercom/android/sdk/utilities/ContextLocaliser;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/utilities/ContextLocaliser;->createLocalisedContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 238
    new-instance v0, Lio/intercom/android/sdk/overlay/StackableSnippet;

    const/4 v3, 0x0

    iget-object v4, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->handler:Landroid/os/Handler;

    .line 239
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    iget-object v7, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v8, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->requestManager:Lio/intercom/com/bumptech/glide/i;

    move-object v2, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lio/intercom/android/sdk/overlay/StackableSnippet;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/models/Conversation;ILandroid/os/Handler;ILio/intercom/android/sdk/overlay/InAppNotification$Listener;Lio/intercom/android/sdk/Provider;Lio/intercom/com/bumptech/glide/i;)V

    .line 240
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->inflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x1

    iget v3, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->bottomPadding:I

    invoke-virtual {v0, p2, v1, v2, v3}, Lio/intercom/android/sdk/overlay/InAppNotification;->display(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;ZI)V

    .line 242
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/intercom/android/sdk/overlay/InAppNotification;

    invoke-virtual {v1}, Lio/intercom/android/sdk/overlay/InAppNotification;->removeView()V

    .line 246
    :cond_0
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    return-void
.end method

.method public displayNotifications(Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lio/intercom/android/sdk/overlay/InAppNotificationManager$1;

    invoke-direct {v0, p0, p2, p1}, Lio/intercom/android/sdk/overlay/InAppNotificationManager$1;-><init>(Lio/intercom/android/sdk/overlay/InAppNotificationManager;Ljava/util/List;Landroid/view/ViewGroup;)V

    invoke-static {p1, v0}, Lio/intercom/android/sdk/utilities/ViewUtils;->waitForViewAttachment(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 82
    return-void
.end method

.method displayNotificationsAfterAttach(Ljava/util/List;Landroid/view/ViewGroup;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 85
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->lastDisplayedConversations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 86
    :goto_0
    new-instance v2, Ljava/util/HashMap;

    iget-object v3, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 87
    if-eqz v0, :cond_2

    .line 88
    invoke-direct {p0, p1, v2, p2}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->updateNotifications(Ljava/util/List;Ljava/util/Map;Landroid/view/ViewGroup;)V

    .line 93
    :cond_0
    :goto_1
    iput-boolean v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->hasAnimated:Z

    .line 95
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 96
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    sget v2, Lio/intercom/android/sdk/R$dimen;->intercom_overlay_pill_bottom_margin:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    sget v2, Lio/intercom/android/sdk/R$dimen;->intercom_bottom_padding:I

    .line 97
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, v1, v0

    iget v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->bottomPadding:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->pillPosition:F

    .line 99
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->inflater:Landroid/view/LayoutInflater;

    invoke-direct {p0, v0, p2}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->displayPill(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 101
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->lastDisplayedConversations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 102
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->lastDisplayedConversations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 103
    return-void

    .line 85
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :cond_2
    invoke-virtual {p0}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->isDisplaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    invoke-direct {p0, p1, p2}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->addNewNotifications(Ljava/util/List;Landroid/view/ViewGroup;)V

    goto :goto_1
.end method

.method public isDisplaying()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lio/intercom/android/sdk/activities/IntercomMessengerActivity;->openInbox(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 163
    return-void
.end method

.method public onInAppNotificationDismiss(Lio/intercom/android/sdk/overlay/InAppNotification;)V
    .locals 5

    .prologue
    .line 126
    const/4 v0, 0x0

    .line 128
    :try_start_0
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->listener:Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;

    invoke-interface {v1}, Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;->getRootView()Landroid/view/ViewGroup;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 133
    :goto_0
    invoke-virtual {p1}, Lio/intercom/android/sdk/overlay/InAppNotification;->getConversation()Lio/intercom/android/sdk/models/Conversation;

    move-result-object v2

    .line 134
    if-eqz v1, :cond_1

    .line 135
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->listener:Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;

    invoke-interface {v0, v2}, Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;->markAsDismissed(Lio/intercom/android/sdk/models/Conversation;)V

    .line 136
    invoke-virtual {p1}, Lio/intercom/android/sdk/overlay/InAppNotification;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 137
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->lastDisplayedConversations:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/InAppNotification;

    .line 141
    iget-object v4, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->inflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v1, v4}, Lio/intercom/android/sdk/overlay/InAppNotification;->moveForward(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    goto :goto_1

    .line 129
    :catch_0
    move-exception v1

    .line 130
    iget-object v2, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->twig:Lio/intercom/android/sdk/twig/Twig;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get root view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 143
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->inflater:Landroid/view/LayoutInflater;

    invoke-direct {p0, v0, v1}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->displayPill(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 145
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->hasAnimated:Z

    .line 150
    :cond_1
    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getLastAdminPart()Lio/intercom/android/sdk/models/Part;

    move-result-object v0

    .line 151
    const-string v1, "chat"

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getMessageStyle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lio/intercom/android/sdk/models/Part$DeliveryOption;->FULL:Lio/intercom/android/sdk/models/Part$DeliveryOption;

    .line 152
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getDeliveryOption()Lio/intercom/android/sdk/models/Part$DeliveryOption;

    move-result-object v3

    if-ne v1, v3, :cond_2

    .line 153
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/metrics/MetricTracker;->closedInAppFromFull(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :goto_2
    return-void

    .line 154
    :cond_2
    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->isInitialMessage()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 155
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/metrics/MetricTracker;->dismissInAppMessageSnippet(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 157
    :cond_3
    iget-object v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->metricTracker:Lio/intercom/android/sdk/metrics/MetricTracker;

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lio/intercom/android/sdk/models/Part;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/intercom/android/sdk/metrics/MetricTracker;->dismissInAppCommentSnippet(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onInAppNotificationTap(Lio/intercom/android/sdk/models/Conversation;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->listener:Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;

    invoke-interface {v0, p1}, Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;->openNotification(Lio/intercom/android/sdk/models/Conversation;)V

    .line 123
    return-void
.end method

.method public reset(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/InAppNotification;

    .line 111
    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/InAppNotification;->removeView()V

    goto :goto_0

    .line 114
    :cond_0
    sget v0, Lio/intercom/android/sdk/R$id;->notification_pill:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 118
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 119
    return-void
.end method

.method public setBottomPadding(I)V
    .locals 5

    .prologue
    .line 331
    iput p1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->bottomPadding:I

    .line 333
    invoke-virtual {p0}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->isDisplaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/InAppNotification;

    .line 338
    iget-object v2, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->inflater:Landroid/view/LayoutInflater;

    invoke-virtual {v2}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->bottomPadding:I

    invoke-virtual {v0, v2, v3}, Lio/intercom/android/sdk/overlay/InAppNotification;->updateBottomPadding(Landroid/content/Context;I)V

    goto :goto_1

    .line 340
    :cond_2
    const/4 v1, 0x0

    .line 342
    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->listener:Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;

    invoke-interface {v0}, Lio/intercom/android/sdk/overlay/InAppNotificationManager$Listener;->getRootView()Landroid/view/ViewGroup;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 347
    :goto_2
    if-eqz v0, :cond_0

    .line 348
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 349
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    sget v3, Lio/intercom/android/sdk/R$dimen;->intercom_overlay_pill_bottom_margin:I

    .line 350
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    sget v3, Lio/intercom/android/sdk/R$dimen;->intercom_bottom_padding:I

    .line 351
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v1, v2, v1

    sub-int/2addr v1, p1

    int-to-float v1, v1

    iput v1, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->pillPosition:F

    .line 353
    sget v1, Lio/intercom/android/sdk/R$id;->notification_pill:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 354
    if-eqz v0, :cond_0

    .line 355
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->updatePillPosition(Landroid/view/View;)V

    goto :goto_0

    .line 343
    :catch_0
    move-exception v0

    .line 344
    iget-object v2, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->twig:Lio/intercom/android/sdk/twig/Twig;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get root view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/twig/Twig;->internal(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_2
.end method

.method syncUpdates(Ljava/util/List;Ljava/util/Map;Landroid/view/ViewGroup;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/intercom/android/sdk/overlay/InAppNotification;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 252
    move v3, v10

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 253
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/intercom/android/sdk/models/Conversation;

    .line 254
    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/InAppNotification;

    .line 255
    instance-of v1, v0, Lio/intercom/android/sdk/overlay/ChatSnippet;

    if-eqz v1, :cond_0

    if-gtz v3, :cond_1

    :cond_0
    if-nez v0, :cond_6

    .line 256
    :cond_1
    if-eqz v0, :cond_2

    .line 257
    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/InAppNotification;->removeView()V

    .line 259
    :cond_2
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->contextLocaliser:Lio/intercom/android/sdk/utilities/ContextLocaliser;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/android/sdk/utilities/ContextLocaliser;->createLocalisedContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 260
    new-instance v0, Lio/intercom/android/sdk/overlay/StackableSnippet;

    iget-object v4, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->handler:Landroid/os/Handler;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    iget-object v7, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->appConfigProvider:Lio/intercom/android/sdk/Provider;

    iget-object v8, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->requestManager:Lio/intercom/com/bumptech/glide/i;

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lio/intercom/android/sdk/overlay/StackableSnippet;-><init>(Landroid/content/Context;Lio/intercom/android/sdk/models/Conversation;ILandroid/os/Handler;ILio/intercom/android/sdk/overlay/InAppNotification$Listener;Lio/intercom/android/sdk/Provider;Lio/intercom/com/bumptech/glide/i;)V

    move-object v4, v0

    .line 264
    :goto_1
    const/4 v0, 0x3

    if-ge v3, v0, :cond_3

    .line 265
    iget-object v7, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->inflater:Landroid/view/LayoutInflater;

    iget v9, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->bottomPadding:I

    move-object v5, p3

    move-object v6, v2

    move v8, v10

    invoke-virtual/range {v4 .. v9}, Lio/intercom/android/sdk/overlay/InAppNotification;->display(Landroid/view/ViewGroup;Lio/intercom/android/sdk/models/Conversation;Landroid/view/LayoutInflater;ZI)V

    .line 269
    :goto_2
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->notifications:Ljava/util/Map;

    invoke-virtual {v2}, Lio/intercom/android/sdk/models/Conversation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 267
    :cond_3
    invoke-virtual {v4}, Lio/intercom/android/sdk/overlay/InAppNotification;->removeView()V

    goto :goto_2

    .line 272
    :cond_4
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/intercom/android/sdk/overlay/InAppNotification;

    .line 273
    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/InAppNotification;->removeView()V

    goto :goto_3

    .line 275
    :cond_5
    return-void

    :cond_6
    move-object v4, v0

    goto :goto_1
.end method
