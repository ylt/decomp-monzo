.class Lio/intercom/android/sdk/overlay/OverlayManager$6;
.super Landroid/animation/AnimatorListenerAdapter;
.source "OverlayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/intercom/android/sdk/overlay/OverlayManager;->displayNotifications(Ljava/util/List;Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lio/intercom/android/sdk/overlay/OverlayManager;

.field final synthetic val$conversations:Ljava/util/List;

.field final synthetic val$defaultLauncher:Lio/intercom/android/sdk/overlay/DefaultLauncher;

.field final synthetic val$rootView:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/overlay/OverlayManager;Lio/intercom/android/sdk/overlay/DefaultLauncher;Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lio/intercom/android/sdk/overlay/OverlayManager$6;->this$0:Lio/intercom/android/sdk/overlay/OverlayManager;

    iput-object p2, p0, Lio/intercom/android/sdk/overlay/OverlayManager$6;->val$defaultLauncher:Lio/intercom/android/sdk/overlay/DefaultLauncher;

    iput-object p3, p0, Lio/intercom/android/sdk/overlay/OverlayManager$6;->val$rootView:Landroid/view/ViewGroup;

    iput-object p4, p0, Lio/intercom/android/sdk/overlay/OverlayManager$6;->val$conversations:Ljava/util/List;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 244
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager$6;->val$defaultLauncher:Lio/intercom/android/sdk/overlay/DefaultLauncher;

    invoke-virtual {v0}, Lio/intercom/android/sdk/overlay/DefaultLauncher;->removeView()V

    .line 245
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/OverlayManager$6;->this$0:Lio/intercom/android/sdk/overlay/OverlayManager;

    iget-object v0, v0, Lio/intercom/android/sdk/overlay/OverlayManager;->inAppNotificationManager:Lio/intercom/android/sdk/overlay/InAppNotificationManager;

    iget-object v1, p0, Lio/intercom/android/sdk/overlay/OverlayManager$6;->val$rootView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lio/intercom/android/sdk/overlay/OverlayManager$6;->val$conversations:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lio/intercom/android/sdk/overlay/InAppNotificationManager;->displayNotifications(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 246
    return-void
.end method
