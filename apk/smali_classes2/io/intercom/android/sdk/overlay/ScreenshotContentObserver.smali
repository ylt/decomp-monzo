.class public Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;
.super Landroid/database/ContentObserver;
.source "ScreenshotContentObserver.java"


# static fields
.field private static final DEFAULT_DETECT_WINDOW_SECONDS:J = 0x5L

.field private static final EXTERNAL_CONTENT_URI_STRING:Ljava/lang/String;

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final ROOT_FOLDER_URI_STRING:Ljava/lang/String; = "content://media/external"

.field private static final SORT_ORDER:Ljava/lang/String; = "date_added DESC"


# instance fields
.field private final contentResolver:Landroid/content/ContentResolver;

.field private lastScreenshotTimestamp:J

.field private final store:Lio/intercom/android/sdk/store/Store;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;"
        }
    .end annotation
.end field

.field private final twig:Lio/intercom/android/sdk/twig/Twig;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 21
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->EXTERNAL_CONTENT_URI_STRING:Ljava/lang/String;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "date_added"

    aput-object v2, v0, v1

    sput-object v0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lio/intercom/android/sdk/store/Store;Landroid/content/ContentResolver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/intercom/android/sdk/store/Store",
            "<",
            "Lio/intercom/android/sdk/state/State;",
            ">;",
            "Landroid/content/ContentResolver;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 33
    invoke-static {}, Lio/intercom/android/sdk/logger/LumberMill;->getLogger()Lio/intercom/android/sdk/twig/Twig;

    move-result-object v0

    iput-object v0, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->twig:Lio/intercom/android/sdk/twig/Twig;

    .line 39
    iput-object p1, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->store:Lio/intercom/android/sdk/store/Store;

    .line 40
    iput-object p2, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->contentResolver:Landroid/content/ContentResolver;

    .line 41
    return-void
.end method

.method private getLatestScreenshot(Landroid/database/Cursor;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 76
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    const-string v1, "date_added"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 79
    invoke-direct {p0, v0}, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->isInScreenshotFolder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v2, v3}, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->isRecentEnough(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    iput-wide v2, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->lastScreenshotTimestamp:J

    .line 81
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 84
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    goto :goto_0
.end method

.method private isInScreenshotFolder(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 88
    const-string v0, "Screenshot"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private isRecentEnough(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 92
    sget-object v1, Lio/intercom/android/sdk/commons/utilities/TimeProvider;->SYSTEM:Lio/intercom/android/sdk/commons/utilities/TimeProvider;

    invoke-interface {v1}, Lio/intercom/android/sdk/commons/utilities/TimeProvider;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 93
    iget-wide v4, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->lastScreenshotTimestamp:J

    cmp-long v1, p1, v4

    if-gez v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v0

    :cond_1
    sub-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x5

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private queryContentForUri(Landroid/net/Uri;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 57
    .line 59
    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->contentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "date_added DESC"

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 60
    :try_start_1
    invoke-direct {p0, v1}, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->getLatestScreenshot(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v0

    .line 61
    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    if-ne v0, v2, :cond_1

    .line 62
    iget-object v0, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {}, Lio/intercom/android/sdk/actions/Actions;->screenshotDeleted()Lio/intercom/android/sdk/actions/Action;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 69
    :goto_0
    if-eqz v1, :cond_0

    .line 70
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 73
    :cond_0
    :goto_1
    return-void

    .line 64
    :cond_1
    :try_start_2
    iget-object v2, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->store:Lio/intercom/android/sdk/store/Store;

    invoke-static {v0}, Lio/intercom/android/sdk/actions/Actions;->screenshotTaken(Landroid/net/Uri;)Lio/intercom/android/sdk/actions/Action;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/intercom/android/sdk/store/Store;->dispatch(Lio/intercom/android/sdk/actions/Action;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    :goto_2
    :try_start_3
    iget-object v2, p0, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->twig:Lio/intercom/android/sdk/twig/Twig;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t query for screenshots: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Lio/intercom/android/sdk/twig/Twig;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 69
    if-eqz v1, :cond_0

    .line 70
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 69
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_2

    .line 70
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 69
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 66
    :catch_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    const-string v1, "content://media/external"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 48
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->queryContentForUri(Landroid/net/Uri;)V

    .line 53
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 54
    return-void

    .line 49
    :cond_1
    sget-object v1, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->EXTERNAL_CONTENT_URI_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-direct {p0, p2}, Lio/intercom/android/sdk/overlay/ScreenshotContentObserver;->queryContentForUri(Landroid/net/Uri;)V

    goto :goto_0
.end method
