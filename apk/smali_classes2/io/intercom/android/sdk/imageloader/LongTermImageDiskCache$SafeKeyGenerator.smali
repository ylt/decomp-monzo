.class Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache$SafeKeyGenerator;
.super Ljava/lang/Object;
.source "LongTermImageDiskCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SafeKeyGenerator"
.end annotation


# instance fields
.field private final loadIdToSafeHash:Lio/intercom/com/bumptech/glide/h/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/intercom/com/bumptech/glide/h/e",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    new-instance v0, Lio/intercom/com/bumptech/glide/h/e;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Lio/intercom/com/bumptech/glide/h/e;-><init>(I)V

    iput-object v0, p0, Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache$SafeKeyGenerator;->loadIdToSafeHash:Lio/intercom/com/bumptech/glide/h/e;

    .line 175
    return-void
.end method


# virtual methods
.method public getSafeKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 179
    iget-object v1, p0, Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache$SafeKeyGenerator;->loadIdToSafeHash:Lio/intercom/com/bumptech/glide/h/e;

    monitor-enter v1

    .line 180
    :try_start_0
    iget-object v0, p0, Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache$SafeKeyGenerator;->loadIdToSafeHash:Lio/intercom/com/bumptech/glide/h/e;

    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/h/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 181
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    if-nez v0, :cond_0

    .line 184
    :try_start_1
    const-string v1, "SHA-256"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 185
    const-string v2, "UTF-8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 187
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-static {v1}, Lio/intercom/com/bumptech/glide/h/i;->a([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 193
    :goto_0
    iget-object v1, p0, Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache$SafeKeyGenerator;->loadIdToSafeHash:Lio/intercom/com/bumptech/glide/h/e;

    monitor-enter v1

    .line 194
    :try_start_2
    iget-object v2, p0, Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache$SafeKeyGenerator;->loadIdToSafeHash:Lio/intercom/com/bumptech/glide/h/e;

    invoke-virtual {v2, p1, v0}, Lio/intercom/com/bumptech/glide/h/e;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 197
    :cond_0
    return-object v0

    .line 181
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 188
    :catch_0
    move-exception v1

    .line 189
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 190
    :catch_1
    move-exception v1

    .line 191
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 195
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method
