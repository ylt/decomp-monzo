.class Lio/intercom/android/sdk/imageloader/LongTermImageLoader$Fetcher;
.super Ljava/lang/Object;
.source "LongTermImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/android/sdk/imageloader/LongTermImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Fetcher"
.end annotation


# instance fields
.field final diskCache:Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lio/intercom/android/sdk/imageloader/LongTermImageLoader$Fetcher;->diskCache:Lio/intercom/android/sdk/imageloader/LongTermImageDiskCache;

    .line 79
    return-void
.end method


# virtual methods
.method fetchImageFromWeb(Ljava/lang/String;Lio/intercom/android/sdk/imageloader/LongTermImageLoader$OnImageReadyListener;Lio/intercom/com/bumptech/glide/i;)V
    .locals 7

    .prologue
    const/high16 v2, -0x80000000

    .line 102
    .line 103
    invoke-virtual {p3}, Lio/intercom/com/bumptech/glide/i;->c()Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    .line 104
    invoke-virtual {v0, p1}, Lio/intercom/com/bumptech/glide/h;->a(Ljava/lang/String;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    new-instance v1, Lio/intercom/com/bumptech/glide/f/f;

    invoke-direct {v1}, Lio/intercom/com/bumptech/glide/f/f;-><init>()V

    sget-object v3, Lio/intercom/com/bumptech/glide/load/engine/h;->b:Lio/intercom/com/bumptech/glide/load/engine/h;

    .line 106
    invoke-virtual {v1, v3}, Lio/intercom/com/bumptech/glide/f/f;->b(Lio/intercom/com/bumptech/glide/load/engine/h;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v6

    new-instance v0, Lio/intercom/android/sdk/imageloader/LongTermImageLoader$Fetcher$2;

    move-object v1, p0

    move v3, v2

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lio/intercom/android/sdk/imageloader/LongTermImageLoader$Fetcher$2;-><init>(Lio/intercom/android/sdk/imageloader/LongTermImageLoader$Fetcher;IILio/intercom/android/sdk/imageloader/LongTermImageLoader$OnImageReadyListener;Ljava/lang/String;)V

    .line 107
    invoke-virtual {v6, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;)Lio/intercom/com/bumptech/glide/f/a/h;

    .line 115
    return-void
.end method

.method loadImageFromFile(Ljava/lang/String;Ljava/io/File;Lio/intercom/android/sdk/imageloader/LongTermImageLoader$OnImageReadyListener;Lio/intercom/com/bumptech/glide/i;)V
    .locals 8

    .prologue
    const/high16 v2, -0x80000000

    .line 83
    .line 84
    invoke-virtual {p4}, Lio/intercom/com/bumptech/glide/i;->c()Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    .line 85
    invoke-virtual {v0, p2}, Lio/intercom/com/bumptech/glide/h;->a(Ljava/io/File;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v0

    new-instance v1, Lio/intercom/com/bumptech/glide/f/f;

    invoke-direct {v1}, Lio/intercom/com/bumptech/glide/f/f;-><init>()V

    sget-object v3, Lio/intercom/com/bumptech/glide/load/engine/h;->b:Lio/intercom/com/bumptech/glide/load/engine/h;

    .line 87
    invoke-virtual {v1, v3}, Lio/intercom/com/bumptech/glide/f/f;->b(Lio/intercom/com/bumptech/glide/load/engine/h;)Lio/intercom/com/bumptech/glide/f/f;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/f;)Lio/intercom/com/bumptech/glide/h;

    move-result-object v7

    new-instance v0, Lio/intercom/android/sdk/imageloader/LongTermImageLoader$Fetcher$1;

    move-object v1, p0

    move v3, v2

    move-object v4, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lio/intercom/android/sdk/imageloader/LongTermImageLoader$Fetcher$1;-><init>(Lio/intercom/android/sdk/imageloader/LongTermImageLoader$Fetcher;IILjava/lang/String;Lio/intercom/android/sdk/imageloader/LongTermImageLoader$OnImageReadyListener;Lio/intercom/com/bumptech/glide/i;)V

    .line 88
    invoke-virtual {v7, v0}, Lio/intercom/com/bumptech/glide/h;->a(Lio/intercom/com/bumptech/glide/f/a/h;)Lio/intercom/com/bumptech/glide/f/a/h;

    .line 98
    return-void
.end method
