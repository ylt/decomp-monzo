.class final Lio/intercom/android/sdk/state/AutoValue_State;
.super Lio/intercom/android/sdk/state/State;
.source "AutoValue_State.java"


# instance fields
.field private final activeConversationState:Lio/intercom/android/sdk/state/ActiveConversationState;

.field private final hasConversations:Z

.field private final hostAppState:Lio/intercom/android/sdk/state/HostAppState;

.field private final inboxState:Lio/intercom/android/sdk/state/InboxState;

.field private final lastScreenshot:Landroid/net/Uri;

.field private final overlayState:Lio/intercom/android/sdk/state/OverlayState;

.field private final teamPresence:Lio/intercom/android/sdk/models/TeamPresence;

.field private final uiState:Lio/intercom/android/sdk/state/UiState;

.field private final unreadConversationIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ZLio/intercom/android/sdk/state/UiState;Lio/intercom/android/sdk/models/TeamPresence;Ljava/util/Set;Lio/intercom/android/sdk/state/InboxState;Lio/intercom/android/sdk/state/HostAppState;Lio/intercom/android/sdk/state/OverlayState;Landroid/net/Uri;Lio/intercom/android/sdk/state/ActiveConversationState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lio/intercom/android/sdk/state/UiState;",
            "Lio/intercom/android/sdk/models/TeamPresence;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lio/intercom/android/sdk/state/InboxState;",
            "Lio/intercom/android/sdk/state/HostAppState;",
            "Lio/intercom/android/sdk/state/OverlayState;",
            "Landroid/net/Uri;",
            "Lio/intercom/android/sdk/state/ActiveConversationState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lio/intercom/android/sdk/state/State;-><init>()V

    .line 30
    iput-boolean p1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hasConversations:Z

    .line 31
    if-nez p2, :cond_0

    .line 32
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null uiState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    iput-object p2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->uiState:Lio/intercom/android/sdk/state/UiState;

    .line 35
    if-nez p3, :cond_1

    .line 36
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null teamPresence"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_1
    iput-object p3, p0, Lio/intercom/android/sdk/state/AutoValue_State;->teamPresence:Lio/intercom/android/sdk/models/TeamPresence;

    .line 39
    if-nez p4, :cond_2

    .line 40
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null unreadConversationIds"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_2
    iput-object p4, p0, Lio/intercom/android/sdk/state/AutoValue_State;->unreadConversationIds:Ljava/util/Set;

    .line 43
    if-nez p5, :cond_3

    .line 44
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null inboxState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_3
    iput-object p5, p0, Lio/intercom/android/sdk/state/AutoValue_State;->inboxState:Lio/intercom/android/sdk/state/InboxState;

    .line 47
    if-nez p6, :cond_4

    .line 48
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null hostAppState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_4
    iput-object p6, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hostAppState:Lio/intercom/android/sdk/state/HostAppState;

    .line 51
    if-nez p7, :cond_5

    .line 52
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null overlayState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_5
    iput-object p7, p0, Lio/intercom/android/sdk/state/AutoValue_State;->overlayState:Lio/intercom/android/sdk/state/OverlayState;

    .line 55
    if-nez p8, :cond_6

    .line 56
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null lastScreenshot"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_6
    iput-object p8, p0, Lio/intercom/android/sdk/state/AutoValue_State;->lastScreenshot:Landroid/net/Uri;

    .line 59
    if-nez p9, :cond_7

    .line 60
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null activeConversationState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_7
    iput-object p9, p0, Lio/intercom/android/sdk/state/AutoValue_State;->activeConversationState:Lio/intercom/android/sdk/state/ActiveConversationState;

    .line 63
    return-void
.end method


# virtual methods
.method public activeConversationState()Lio/intercom/android/sdk/state/ActiveConversationState;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->activeConversationState:Lio/intercom/android/sdk/state/ActiveConversationState;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    if-ne p1, p0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return v0

    .line 130
    :cond_1
    instance-of v2, p1, Lio/intercom/android/sdk/state/State;

    if-eqz v2, :cond_3

    .line 131
    check-cast p1, Lio/intercom/android/sdk/state/State;

    .line 132
    iget-boolean v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hasConversations:Z

    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->hasConversations()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->uiState:Lio/intercom/android/sdk/state/UiState;

    .line 133
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->uiState()Lio/intercom/android/sdk/state/UiState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->teamPresence:Lio/intercom/android/sdk/models/TeamPresence;

    .line 134
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->teamPresence()Lio/intercom/android/sdk/models/TeamPresence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->unreadConversationIds:Ljava/util/Set;

    .line 135
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->unreadConversationIds()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->inboxState:Lio/intercom/android/sdk/state/InboxState;

    .line 136
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->inboxState()Lio/intercom/android/sdk/state/InboxState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hostAppState:Lio/intercom/android/sdk/state/HostAppState;

    .line 137
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->hostAppState()Lio/intercom/android/sdk/state/HostAppState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->overlayState:Lio/intercom/android/sdk/state/OverlayState;

    .line 138
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->overlayState()Lio/intercom/android/sdk/state/OverlayState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->lastScreenshot:Landroid/net/Uri;

    .line 139
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->lastScreenshot()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_State;->activeConversationState:Lio/intercom/android/sdk/state/ActiveConversationState;

    .line 140
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/State;->activeConversationState()Lio/intercom/android/sdk/state/ActiveConversationState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 142
    goto :goto_0
.end method

.method public hasConversations()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hasConversations:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 147
    .line 149
    iget-boolean v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hasConversations:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    xor-int/2addr v0, v2

    .line 150
    mul-int/2addr v0, v2

    .line 151
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->uiState:Lio/intercom/android/sdk/state/UiState;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 152
    mul-int/2addr v0, v2

    .line 153
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->teamPresence:Lio/intercom/android/sdk/models/TeamPresence;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 154
    mul-int/2addr v0, v2

    .line 155
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->unreadConversationIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 156
    mul-int/2addr v0, v2

    .line 157
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->inboxState:Lio/intercom/android/sdk/state/InboxState;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 158
    mul-int/2addr v0, v2

    .line 159
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hostAppState:Lio/intercom/android/sdk/state/HostAppState;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 160
    mul-int/2addr v0, v2

    .line 161
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->overlayState:Lio/intercom/android/sdk/state/OverlayState;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 162
    mul-int/2addr v0, v2

    .line 163
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->lastScreenshot:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 164
    mul-int/2addr v0, v2

    .line 165
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->activeConversationState:Lio/intercom/android/sdk/state/ActiveConversationState;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 166
    return v0

    .line 149
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public hostAppState()Lio/intercom/android/sdk/state/HostAppState;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hostAppState:Lio/intercom/android/sdk/state/HostAppState;

    return-object v0
.end method

.method public inboxState()Lio/intercom/android/sdk/state/InboxState;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->inboxState:Lio/intercom/android/sdk/state/InboxState;

    return-object v0
.end method

.method public lastScreenshot()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->lastScreenshot:Landroid/net/Uri;

    return-object v0
.end method

.method public overlayState()Lio/intercom/android/sdk/state/OverlayState;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->overlayState:Lio/intercom/android/sdk/state/OverlayState;

    return-object v0
.end method

.method public teamPresence()Lio/intercom/android/sdk/models/TeamPresence;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->teamPresence:Lio/intercom/android/sdk/models/TeamPresence;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State{hasConversations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hasConversations:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uiState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->uiState:Lio/intercom/android/sdk/state/UiState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", teamPresence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->teamPresence:Lio/intercom/android/sdk/models/TeamPresence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", unreadConversationIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->unreadConversationIds:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inboxState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->inboxState:Lio/intercom/android/sdk/state/InboxState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hostAppState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->hostAppState:Lio/intercom/android/sdk/state/HostAppState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", overlayState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->overlayState:Lio/intercom/android/sdk/state/OverlayState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastScreenshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->lastScreenshot:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", activeConversationState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_State;->activeConversationState:Lio/intercom/android/sdk/state/ActiveConversationState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public uiState()Lio/intercom/android/sdk/state/UiState;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->uiState:Lio/intercom/android/sdk/state/UiState;

    return-object v0
.end method

.method public unreadConversationIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_State;->unreadConversationIds:Ljava/util/Set;

    return-object v0
.end method
