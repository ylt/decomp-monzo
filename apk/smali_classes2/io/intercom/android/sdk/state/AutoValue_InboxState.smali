.class final Lio/intercom/android/sdk/state/AutoValue_InboxState;
.super Lio/intercom/android/sdk/state/InboxState;
.source "AutoValue_InboxState.java"


# instance fields
.field private final conversations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;"
        }
    .end annotation
.end field

.field private final hasMorePages:Z

.field private final status:Lio/intercom/android/sdk/state/InboxState$Status;


# direct methods
.method constructor <init>(Ljava/util/List;Lio/intercom/android/sdk/state/InboxState$Status;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;",
            "Lio/intercom/android/sdk/state/InboxState$Status;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Lio/intercom/android/sdk/state/InboxState;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null conversations"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->conversations:Ljava/util/List;

    .line 21
    if-nez p2, :cond_1

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null status"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->status:Lio/intercom/android/sdk/state/InboxState$Status;

    .line 25
    iput-boolean p3, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->hasMorePages:Z

    .line 26
    return-void
.end method


# virtual methods
.method public conversations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/models/Conversation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->conversations:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    instance-of v2, p1, Lio/intercom/android/sdk/state/InboxState;

    if-eqz v2, :cond_3

    .line 58
    check-cast p1, Lio/intercom/android/sdk/state/InboxState;

    .line 59
    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->conversations:Ljava/util/List;

    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->conversations()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->status:Lio/intercom/android/sdk/state/InboxState$Status;

    .line 60
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->status()Lio/intercom/android/sdk/state/InboxState$Status;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/intercom/android/sdk/state/InboxState$Status;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->hasMorePages:Z

    .line 61
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/InboxState;->hasMorePages()Z

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 63
    goto :goto_0
.end method

.method public hasMorePages()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->hasMorePages:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 68
    .line 70
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->conversations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 71
    mul-int/2addr v0, v2

    .line 72
    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->status:Lio/intercom/android/sdk/state/InboxState$Status;

    invoke-virtual {v1}, Lio/intercom/android/sdk/state/InboxState$Status;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 73
    mul-int v1, v0, v2

    .line 74
    iget-boolean v0, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->hasMorePages:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 75
    return v0

    .line 74
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public status()Lio/intercom/android/sdk/state/InboxState$Status;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->status:Lio/intercom/android/sdk/state/InboxState$Status;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InboxState{conversations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->conversations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->status:Lio/intercom/android/sdk/state/InboxState$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hasMorePages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lio/intercom/android/sdk/state/AutoValue_InboxState;->hasMorePages:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
