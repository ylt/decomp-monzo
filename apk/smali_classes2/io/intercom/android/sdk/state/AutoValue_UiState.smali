.class final Lio/intercom/android/sdk/state/AutoValue_UiState;
.super Lio/intercom/android/sdk/state/UiState;
.source "AutoValue_UiState.java"


# instance fields
.field private final conversationId:Ljava/lang/String;

.field private final screen:Lio/intercom/android/sdk/state/UiState$Screen;


# direct methods
.method constructor <init>(Lio/intercom/android/sdk/state/UiState$Screen;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Lio/intercom/android/sdk/state/UiState;-><init>()V

    .line 14
    if-nez p1, :cond_0

    .line 15
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null screen"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->screen:Lio/intercom/android/sdk/state/UiState$Screen;

    .line 18
    iput-object p2, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->conversationId:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public conversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->conversationId:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 45
    :cond_1
    instance-of v2, p1, Lio/intercom/android/sdk/state/UiState;

    if-eqz v2, :cond_4

    .line 46
    check-cast p1, Lio/intercom/android/sdk/state/UiState;

    .line 47
    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->screen:Lio/intercom/android/sdk/state/UiState$Screen;

    invoke-virtual {p1}, Lio/intercom/android/sdk/state/UiState;->screen()Lio/intercom/android/sdk/state/UiState$Screen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/intercom/android/sdk/state/UiState$Screen;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->conversationId:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 48
    invoke-virtual {p1}, Lio/intercom/android/sdk/state/UiState;->conversationId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->conversationId:Ljava/lang/String;

    invoke-virtual {p1}, Lio/intercom/android/sdk/state/UiState;->conversationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    const v1, 0xf4243

    .line 55
    .line 57
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->screen:Lio/intercom/android/sdk/state/UiState$Screen;

    invoke-virtual {v0}, Lio/intercom/android/sdk/state/UiState$Screen;->hashCode()I

    move-result v0

    xor-int/2addr v0, v1

    .line 58
    mul-int/2addr v1, v0

    .line 59
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->conversationId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 60
    return v0

    .line 59
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->conversationId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public screen()Lio/intercom/android/sdk/state/UiState$Screen;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->screen:Lio/intercom/android/sdk/state/UiState$Screen;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UiState{screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->screen:Lio/intercom/android/sdk/state/UiState$Screen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", conversationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/state/AutoValue_UiState;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
