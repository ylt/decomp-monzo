.class final Lio/intercom/android/sdk/models/AutoValue_Link;
.super Lio/intercom/android/sdk/models/Link;
.source "AutoValue_Link.java"


# instance fields
.field private final blocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block;",
            ">;"
        }
    .end annotation
.end field

.field private final card:Lio/intercom/android/sdk/models/Card;

.field private final createdAt:J

.field private final id:Ljava/lang/String;

.field private final reactionReply:Lio/intercom/android/sdk/models/ReactionReply;

.field private final updatedAt:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/intercom/android/sdk/models/Card;Ljava/util/List;Lio/intercom/android/sdk/models/ReactionReply;JJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/intercom/android/sdk/models/Card;",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block;",
            ">;",
            "Lio/intercom/android/sdk/models/ReactionReply;",
            "JJ)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Lio/intercom/android/sdk/models/Link;-><init>()V

    .line 23
    if-nez p1, :cond_0

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null id"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iput-object p1, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->id:Ljava/lang/String;

    .line 27
    if-nez p2, :cond_1

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null card"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_1
    iput-object p2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->card:Lio/intercom/android/sdk/models/Card;

    .line 31
    if-nez p3, :cond_2

    .line 32
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null blocks"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->blocks:Ljava/util/List;

    .line 35
    if-nez p4, :cond_3

    .line 36
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null reactionReply"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_3
    iput-object p4, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->reactionReply:Lio/intercom/android/sdk/models/ReactionReply;

    .line 39
    iput-wide p5, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->createdAt:J

    .line 40
    iput-wide p7, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->updatedAt:J

    .line 41
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    if-ne p1, p0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    instance-of v2, p1, Lio/intercom/android/sdk/models/Link;

    if-eqz v2, :cond_3

    .line 91
    check-cast p1, Lio/intercom/android/sdk/models/Link;

    .line 92
    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->id:Ljava/lang/String;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->card:Lio/intercom/android/sdk/models/Card;

    .line 93
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getCard()Lio/intercom/android/sdk/models/Card;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->blocks:Ljava/util/List;

    .line 94
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getBlocks()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->reactionReply:Lio/intercom/android/sdk/models/ReactionReply;

    .line 95
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getReactionReply()Lio/intercom/android/sdk/models/ReactionReply;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/intercom/android/sdk/models/ReactionReply;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->createdAt:J

    .line 96
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getCreatedAt()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->updatedAt:J

    .line 97
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Link;->getUpdatedAt()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 99
    goto :goto_0
.end method

.method public getBlocks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lio/intercom/android/sdk/blocks/models/Block;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->blocks:Ljava/util/List;

    return-object v0
.end method

.method public getCard()Lio/intercom/android/sdk/models/Card;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->card:Lio/intercom/android/sdk/models/Card;

    return-object v0
.end method

.method public getCreatedAt()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->createdAt:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getReactionReply()Lio/intercom/android/sdk/models/ReactionReply;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->reactionReply:Lio/intercom/android/sdk/models/ReactionReply;

    return-object v0
.end method

.method public getUpdatedAt()J
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->updatedAt:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/16 v7, 0x20

    const v6, 0xf4243

    .line 104
    .line 106
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v6

    .line 107
    mul-int/2addr v0, v6

    .line 108
    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->card:Lio/intercom/android/sdk/models/Card;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v6

    .line 110
    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->blocks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 111
    mul-int/2addr v0, v6

    .line 112
    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->reactionReply:Lio/intercom/android/sdk/models/ReactionReply;

    invoke-virtual {v1}, Lio/intercom/android/sdk/models/ReactionReply;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 113
    mul-int/2addr v0, v6

    .line 114
    int-to-long v0, v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->createdAt:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->createdAt:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 115
    mul-int/2addr v0, v6

    .line 116
    int-to-long v0, v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->updatedAt:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->updatedAt:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 117
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Link{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->card:Lio/intercom/android/sdk/models/Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", blocks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->blocks:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reactionReply="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->reactionReply:Lio/intercom/android/sdk/models/ReactionReply;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->createdAt:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", updatedAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Link;->updatedAt:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
