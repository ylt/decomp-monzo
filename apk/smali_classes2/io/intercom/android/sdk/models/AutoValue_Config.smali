.class final Lio/intercom/android/sdk/models/AutoValue_Config;
.super Lio/intercom/android/sdk/models/Config;
.source "AutoValue_Config.java"


# instance fields
.field private final audioEnabled:Z

.field private final backgroundRequestsEnabled:Z

.field private final baseColor:Ljava/lang/String;

.field private final batchUserUpdatePeriod:J

.field private final features:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final firstRequest:Z

.field private final inboundMessages:Z

.field private final locale:Ljava/lang/String;

.field private final messengerBackground:Ljava/lang/String;

.field private final metricsEnabled:Z

.field private final name:Ljava/lang/String;

.field private final newSessionThreshold:J

.field private final pingDelayMs:J

.field private final rateLimitCount:I

.field private final rateLimitPeriod:J

.field private final realTimeConfig:Lio/intercom/android/sdk/nexus/NexusConfig;

.field private final showPoweredBy:Z

.field private final softResetTimeout:J

.field private final userUpdateCacheMaxAge:J

.field private final welcomeMessage:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZJIJJJJJLio/intercom/android/sdk/nexus/NexusConfig;Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZZZZJIJJJJJ",
            "Lio/intercom/android/sdk/nexus/NexusConfig;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Lio/intercom/android/sdk/models/Config;-><init>()V

    .line 52
    if-nez p1, :cond_0

    .line 53
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null name"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 55
    :cond_0
    iput-object p1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->name:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->baseColor:Ljava/lang/String;

    .line 57
    if-nez p3, :cond_1

    .line 58
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null welcomeMessage"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 60
    :cond_1
    iput-object p3, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->welcomeMessage:Ljava/lang/String;

    .line 61
    if-nez p4, :cond_2

    .line 62
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null messengerBackground"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 64
    :cond_2
    iput-object p4, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->messengerBackground:Ljava/lang/String;

    .line 65
    if-nez p5, :cond_3

    .line 66
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null locale"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 68
    :cond_3
    iput-object p5, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->locale:Ljava/lang/String;

    .line 69
    iput-boolean p6, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->firstRequest:Z

    .line 70
    iput-boolean p7, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->inboundMessages:Z

    .line 71
    iput-boolean p8, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->showPoweredBy:Z

    .line 72
    iput-boolean p9, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->metricsEnabled:Z

    .line 73
    iput-boolean p10, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->backgroundRequestsEnabled:Z

    .line 74
    move/from16 v0, p11

    iput-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->audioEnabled:Z

    .line 75
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitPeriod:J

    .line 76
    move/from16 v0, p14

    iput v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitCount:I

    .line 77
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->batchUserUpdatePeriod:J

    .line 78
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->userUpdateCacheMaxAge:J

    .line 79
    move-wide/from16 v0, p19

    iput-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->softResetTimeout:J

    .line 80
    move-wide/from16 v0, p21

    iput-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->newSessionThreshold:J

    .line 81
    move-wide/from16 v0, p23

    iput-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->pingDelayMs:J

    .line 82
    if-nez p25, :cond_4

    .line 83
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null realTimeConfig"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 85
    :cond_4
    move-object/from16 v0, p25

    iput-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->realTimeConfig:Lio/intercom/android/sdk/nexus/NexusConfig;

    .line 86
    if-nez p26, :cond_5

    .line 87
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null features"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 89
    :cond_5
    move-object/from16 v0, p26

    iput-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->features:Ljava/util/Set;

    .line 90
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 221
    if-ne p1, p0, :cond_1

    .line 247
    :cond_0
    :goto_0
    return v0

    .line 224
    :cond_1
    instance-of v2, p1, Lio/intercom/android/sdk/models/Config;

    if-eqz v2, :cond_4

    .line 225
    check-cast p1, Lio/intercom/android/sdk/models/Config;

    .line 226
    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->baseColor:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 227
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getBaseColor()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->welcomeMessage:Ljava/lang/String;

    .line 228
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getWelcomeMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->messengerBackground:Ljava/lang/String;

    .line 229
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getMessengerBackground()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->locale:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->firstRequest:Z

    .line 231
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->isFirstRequest()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->inboundMessages:Z

    .line 232
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->isInboundMessages()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->showPoweredBy:Z

    .line 233
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->isShowPoweredBy()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->metricsEnabled:Z

    .line 234
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->isMetricsEnabled()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->backgroundRequestsEnabled:Z

    .line 235
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->isBackgroundRequestsEnabled()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->audioEnabled:Z

    .line 236
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->isAudioEnabled()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitPeriod:J

    .line 237
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getRateLimitPeriod()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitCount:I

    .line 238
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getRateLimitCount()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->batchUserUpdatePeriod:J

    .line 239
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getBatchUserUpdatePeriod()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->userUpdateCacheMaxAge:J

    .line 240
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getUserUpdateCacheMaxAge()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->softResetTimeout:J

    .line 241
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getSoftResetTimeout()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->newSessionThreshold:J

    .line 242
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getNewSessionThreshold()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->pingDelayMs:J

    .line 243
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getPingDelayMs()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->realTimeConfig:Lio/intercom/android/sdk/nexus/NexusConfig;

    .line 244
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getRealTimeConfig()Lio/intercom/android/sdk/nexus/NexusConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/intercom/android/sdk/nexus/NexusConfig;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->features:Ljava/util/Set;

    .line 245
    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getFeatures()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto/16 :goto_0

    .line 227
    :cond_3
    iget-object v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->baseColor:Ljava/lang/String;

    invoke-virtual {p1}, Lio/intercom/android/sdk/models/Config;->getBaseColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1

    :cond_4
    move v0, v1

    .line 247
    goto/16 :goto_0
.end method

.method public getBaseColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->baseColor:Ljava/lang/String;

    return-object v0
.end method

.method public getBatchUserUpdatePeriod()J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->batchUserUpdatePeriod:J

    return-wide v0
.end method

.method public getFeatures()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->features:Ljava/util/Set;

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->locale:Ljava/lang/String;

    return-object v0
.end method

.method public getMessengerBackground()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->messengerBackground:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNewSessionThreshold()J
    .locals 2

    .prologue
    .line 175
    iget-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->newSessionThreshold:J

    return-wide v0
.end method

.method public getPingDelayMs()J
    .locals 2

    .prologue
    .line 180
    iget-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->pingDelayMs:J

    return-wide v0
.end method

.method public getRateLimitCount()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitCount:I

    return v0
.end method

.method public getRateLimitPeriod()J
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitPeriod:J

    return-wide v0
.end method

.method public getRealTimeConfig()Lio/intercom/android/sdk/nexus/NexusConfig;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->realTimeConfig:Lio/intercom/android/sdk/nexus/NexusConfig;

    return-object v0
.end method

.method public getSoftResetTimeout()J
    .locals 2

    .prologue
    .line 170
    iget-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->softResetTimeout:J

    return-wide v0
.end method

.method public getUserUpdateCacheMaxAge()J
    .locals 2

    .prologue
    .line 165
    iget-wide v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->userUpdateCacheMaxAge:J

    return-wide v0
.end method

.method public getWelcomeMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->welcomeMessage:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    const/16 v7, 0x20

    const v6, 0xf4243

    .line 252
    .line 254
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v6

    .line 255
    mul-int v3, v0, v6

    .line 256
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->baseColor:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v3

    .line 257
    mul-int/2addr v0, v6

    .line 258
    iget-object v3, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->welcomeMessage:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    xor-int/2addr v0, v3

    .line 259
    mul-int/2addr v0, v6

    .line 260
    iget-object v3, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->messengerBackground:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    xor-int/2addr v0, v3

    .line 261
    mul-int/2addr v0, v6

    .line 262
    iget-object v3, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->locale:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    xor-int/2addr v0, v3

    .line 263
    mul-int v3, v0, v6

    .line 264
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->firstRequest:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v3

    .line 265
    mul-int v3, v0, v6

    .line 266
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->inboundMessages:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v3

    .line 267
    mul-int v3, v0, v6

    .line 268
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->showPoweredBy:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v3

    .line 269
    mul-int v3, v0, v6

    .line 270
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->metricsEnabled:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v3

    .line 271
    mul-int v3, v0, v6

    .line 272
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->backgroundRequestsEnabled:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    xor-int/2addr v0, v3

    .line 273
    mul-int/2addr v0, v6

    .line 274
    iget-boolean v3, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->audioEnabled:Z

    if-eqz v3, :cond_6

    :goto_6
    xor-int/2addr v0, v1

    .line 275
    mul-int/2addr v0, v6

    .line 276
    int-to-long v0, v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitPeriod:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitPeriod:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 277
    mul-int/2addr v0, v6

    .line 278
    iget v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitCount:I

    xor-int/2addr v0, v1

    .line 279
    mul-int/2addr v0, v6

    .line 280
    int-to-long v0, v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->batchUserUpdatePeriod:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->batchUserUpdatePeriod:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 281
    mul-int/2addr v0, v6

    .line 282
    int-to-long v0, v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->userUpdateCacheMaxAge:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->userUpdateCacheMaxAge:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 283
    mul-int/2addr v0, v6

    .line 284
    int-to-long v0, v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->softResetTimeout:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->softResetTimeout:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 285
    mul-int/2addr v0, v6

    .line 286
    int-to-long v0, v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->newSessionThreshold:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->newSessionThreshold:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 287
    mul-int/2addr v0, v6

    .line 288
    int-to-long v0, v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->pingDelayMs:J

    ushr-long/2addr v2, v7

    iget-wide v4, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->pingDelayMs:J

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 289
    mul-int/2addr v0, v6

    .line 290
    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->realTimeConfig:Lio/intercom/android/sdk/nexus/NexusConfig;

    invoke-virtual {v1}, Lio/intercom/android/sdk/nexus/NexusConfig;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 291
    mul-int/2addr v0, v6

    .line 292
    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->features:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 293
    return v0

    .line 256
    :cond_0
    iget-object v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->baseColor:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 264
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 266
    goto :goto_2

    :cond_3
    move v0, v2

    .line 268
    goto :goto_3

    :cond_4
    move v0, v2

    .line 270
    goto :goto_4

    :cond_5
    move v0, v2

    .line 272
    goto :goto_5

    :cond_6
    move v1, v2

    .line 274
    goto :goto_6
.end method

.method public isAudioEnabled()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->audioEnabled:Z

    return v0
.end method

.method public isBackgroundRequestsEnabled()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->backgroundRequestsEnabled:Z

    return v0
.end method

.method public isFirstRequest()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->firstRequest:Z

    return v0
.end method

.method public isInboundMessages()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->inboundMessages:Z

    return v0
.end method

.method public isMetricsEnabled()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->metricsEnabled:Z

    return v0
.end method

.method public isShowPoweredBy()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->showPoweredBy:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Config{name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", baseColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->baseColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", welcomeMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->welcomeMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", messengerBackground="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->messengerBackground:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->locale:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", firstRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->firstRequest:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inboundMessages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->inboundMessages:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", showPoweredBy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->showPoweredBy:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", metricsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->metricsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", backgroundRequestsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->backgroundRequestsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", audioEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->audioEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rateLimitPeriod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitPeriod:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rateLimitCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->rateLimitCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", batchUserUpdatePeriod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->batchUserUpdatePeriod:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userUpdateCacheMaxAge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->userUpdateCacheMaxAge:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", softResetTimeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->softResetTimeout:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newSessionThreshold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->newSessionThreshold:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pingDelayMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->pingDelayMs:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", realTimeConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->realTimeConfig:Lio/intercom/android/sdk/nexus/NexusConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", features="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/intercom/android/sdk/models/AutoValue_Config;->features:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
