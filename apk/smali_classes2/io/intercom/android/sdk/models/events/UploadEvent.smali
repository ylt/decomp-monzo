.class public Lio/intercom/android/sdk/models/events/UploadEvent;
.super Ljava/lang/Object;
.source "UploadEvent.java"


# instance fields
.field private final isAnnotated:Z

.field private final size:J

.field private final tempPartId:Ljava/lang/String;

.field private final tempPartPosition:I

.field private final upload:Lio/intercom/android/sdk/models/Upload;


# direct methods
.method public constructor <init>(Lio/intercom/android/sdk/models/Upload;JILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->upload:Lio/intercom/android/sdk/models/Upload;

    .line 14
    iput-wide p2, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->size:J

    .line 15
    iput p4, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->tempPartPosition:I

    .line 16
    iput-object p5, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->tempPartId:Ljava/lang/String;

    .line 17
    iput-boolean p6, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->isAnnotated:Z

    .line 18
    return-void
.end method


# virtual methods
.method public getSize()J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->size:J

    return-wide v0
.end method

.method public getTempPartId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->tempPartId:Ljava/lang/String;

    return-object v0
.end method

.method public getTempPartPosition()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->tempPartPosition:I

    return v0
.end method

.method public getUpload()Lio/intercom/android/sdk/models/Upload;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->upload:Lio/intercom/android/sdk/models/Upload;

    return-object v0
.end method

.method public isAnnotatedImage()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/events/UploadEvent;->isAnnotated:Z

    return v0
.end method
