.class public Lio/intercom/android/sdk/models/events/NewConversationEvent;
.super Ljava/lang/Object;
.source "NewConversationEvent.java"


# instance fields
.field private final conversation:Lio/intercom/android/sdk/models/Conversation;

.field private final isAnnotatedImage:Z

.field private final partId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lio/intercom/android/sdk/models/Conversation;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lio/intercom/android/sdk/models/events/NewConversationEvent;->conversation:Lio/intercom/android/sdk/models/Conversation;

    .line 12
    iput-object p2, p0, Lio/intercom/android/sdk/models/events/NewConversationEvent;->partId:Ljava/lang/String;

    .line 13
    iput-boolean p3, p0, Lio/intercom/android/sdk/models/events/NewConversationEvent;->isAnnotatedImage:Z

    .line 14
    return-void
.end method


# virtual methods
.method public getConversation()Lio/intercom/android/sdk/models/Conversation;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lio/intercom/android/sdk/models/events/NewConversationEvent;->conversation:Lio/intercom/android/sdk/models/Conversation;

    return-object v0
.end method

.method public getPartId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lio/intercom/android/sdk/models/events/NewConversationEvent;->partId:Ljava/lang/String;

    return-object v0
.end method

.method public isAnnotatedImage()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/events/NewConversationEvent;->isAnnotatedImage:Z

    return v0
.end method
