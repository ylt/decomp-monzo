.class public Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;
.super Ljava/lang/Object;
.source "NewCommentEvent.java"


# instance fields
.field private final conversationId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;->conversationId:Ljava/lang/String;

    .line 9
    return-void
.end method


# virtual methods
.method public getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lio/intercom/android/sdk/models/events/realtime/NewCommentEvent;->conversationId:Ljava/lang/String;

    return-object v0
.end method
