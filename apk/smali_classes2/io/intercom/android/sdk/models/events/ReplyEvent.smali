.class public Lio/intercom/android/sdk/models/events/ReplyEvent;
.super Ljava/lang/Object;
.source "ReplyEvent.java"


# instance fields
.field private final conversationId:Ljava/lang/String;

.field private final isAnnotated:Z

.field private final partId:Ljava/lang/String;

.field private final position:I

.field private final response:Lio/intercom/android/sdk/models/Part;


# direct methods
.method public constructor <init>(Lio/intercom/android/sdk/models/Part;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->response:Lio/intercom/android/sdk/models/Part;

    .line 14
    iput-object p3, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->partId:Ljava/lang/String;

    .line 15
    iput p2, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->position:I

    .line 16
    iput-object p4, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->conversationId:Ljava/lang/String;

    .line 17
    iput-boolean p5, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->isAnnotated:Z

    .line 18
    return-void
.end method


# virtual methods
.method public getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->conversationId:Ljava/lang/String;

    return-object v0
.end method

.method public getPartId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->partId:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->position:I

    return v0
.end method

.method public getResponse()Lio/intercom/android/sdk/models/Part;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->response:Lio/intercom/android/sdk/models/Part;

    return-object v0
.end method

.method public isAnnotated()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lio/intercom/android/sdk/models/events/ReplyEvent;->isAnnotated:Z

    return v0
.end method
