.class final Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;
.super Ljava/lang/Object;
.source "WebSocketWriter.java"

# interfaces
.implements Lio/intercom/a/r;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/okhttp3/internal/ws/WebSocketWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "FrameSink"
.end annotation


# instance fields
.field closed:Z

.field contentLength:J

.field formatOpcode:I

.field isFirstFrame:Z

.field final synthetic this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;


# direct methods
.method constructor <init>(Lio/intercom/okhttp3/internal/ws/WebSocketWriter;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 243
    iget-boolean v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->closed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :cond_0
    iget-object v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget v1, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->formatOpcode:I

    iget-object v2, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget-object v2, v2, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->buffer:Lio/intercom/a/c;

    invoke-virtual {v2}, Lio/intercom/a/c;->a()J

    move-result-wide v2

    iget-boolean v4, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->isFirstFrame:Z

    invoke-virtual/range {v0 .. v5}, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->writeMessageFrame(IJZZ)V

    .line 246
    iput-boolean v5, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->closed:Z

    .line 247
    iget-object v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->activeWriter:Z

    .line 248
    return-void
.end method

.method public flush()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 231
    iget-boolean v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->closed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_0
    iget-object v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget v1, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->formatOpcode:I

    iget-object v2, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget-object v2, v2, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->buffer:Lio/intercom/a/c;

    invoke-virtual {v2}, Lio/intercom/a/c;->a()J

    move-result-wide v2

    iget-boolean v4, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->isFirstFrame:Z

    invoke-virtual/range {v0 .. v5}, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->writeMessageFrame(IJZZ)V

    .line 234
    iput-boolean v5, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->isFirstFrame:Z

    .line 235
    return-void
.end method

.method public timeout()Lio/intercom/a/t;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget-object v0, v0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->sink:Lio/intercom/a/d;

    invoke-interface {v0}, Lio/intercom/a/d;->timeout()Lio/intercom/a/t;

    move-result-object v0

    return-object v0
.end method

.method public write(Lio/intercom/a/c;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 214
    iget-boolean v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->closed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    iget-object v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget-object v0, v0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->buffer:Lio/intercom/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lio/intercom/a/c;->write(Lio/intercom/a/c;J)V

    .line 219
    iget-boolean v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->isFirstFrame:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->contentLength:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget-object v0, v0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->buffer:Lio/intercom/a/c;

    .line 221
    invoke-virtual {v0}, Lio/intercom/a/c;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->contentLength:J

    const-wide/16 v6, 0x2000

    sub-long/2addr v2, v6

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 223
    :goto_0
    iget-object v1, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget-object v1, v1, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->buffer:Lio/intercom/a/c;

    invoke-virtual {v1}, Lio/intercom/a/c;->h()J

    move-result-wide v2

    .line 224
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    if-nez v0, :cond_1

    .line 225
    iget-object v0, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->this$0:Lio/intercom/okhttp3/internal/ws/WebSocketWriter;

    iget v1, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->formatOpcode:I

    iget-boolean v4, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->isFirstFrame:Z

    invoke-virtual/range {v0 .. v5}, Lio/intercom/okhttp3/internal/ws/WebSocketWriter;->writeMessageFrame(IJZZ)V

    .line 226
    iput-boolean v5, p0, Lio/intercom/okhttp3/internal/ws/WebSocketWriter$FrameSink;->isFirstFrame:Z

    .line 228
    :cond_1
    return-void

    :cond_2
    move v0, v5

    .line 221
    goto :goto_0
.end method
