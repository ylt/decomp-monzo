.class public final Lio/intercom/okhttp3/internal/http1/Http1Codec;
.super Ljava/lang/Object;
.source "Http1Codec.java"

# interfaces
.implements Lio/intercom/okhttp3/internal/http/HttpCodec;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/okhttp3/internal/http1/Http1Codec$UnknownLengthSource;,
        Lio/intercom/okhttp3/internal/http1/Http1Codec$ChunkedSource;,
        Lio/intercom/okhttp3/internal/http1/Http1Codec$FixedLengthSource;,
        Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;,
        Lio/intercom/okhttp3/internal/http1/Http1Codec$ChunkedSink;,
        Lio/intercom/okhttp3/internal/http1/Http1Codec$FixedLengthSink;
    }
.end annotation


# static fields
.field private static final STATE_CLOSED:I = 0x6

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_OPEN_REQUEST_BODY:I = 0x1

.field private static final STATE_OPEN_RESPONSE_BODY:I = 0x4

.field private static final STATE_READING_RESPONSE_BODY:I = 0x5

.field private static final STATE_READ_RESPONSE_HEADERS:I = 0x3

.field private static final STATE_WRITING_REQUEST_BODY:I = 0x2


# instance fields
.field final client:Lio/intercom/okhttp3/OkHttpClient;

.field final sink:Lio/intercom/a/d;

.field final source:Lio/intercom/a/e;

.field state:I

.field final streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;


# direct methods
.method public constructor <init>(Lio/intercom/okhttp3/OkHttpClient;Lio/intercom/okhttp3/internal/connection/StreamAllocation;Lio/intercom/a/e;Lio/intercom/a/d;)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    .line 89
    iput-object p1, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->client:Lio/intercom/okhttp3/OkHttpClient;

    .line 90
    iput-object p2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    .line 91
    iput-object p3, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->source:Lio/intercom/a/e;

    .line 92
    iput-object p4, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->sink:Lio/intercom/a/d;

    .line 93
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    invoke-virtual {v0}, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->connection()Lio/intercom/okhttp3/internal/connection/RealConnection;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lio/intercom/okhttp3/internal/connection/RealConnection;->cancel()V

    .line 113
    :cond_0
    return-void
.end method

.method public createRequestBody(Lio/intercom/okhttp3/Request;J)Lio/intercom/a/r;
    .locals 2

    .prologue
    .line 96
    const-string v0, "chunked"

    const-string v1, "Transfer-Encoding"

    invoke-virtual {p1, v1}, Lio/intercom/okhttp3/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->newChunkedSink()Lio/intercom/a/r;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 101
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    .line 103
    invoke-virtual {p0, p2, p3}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->newFixedLengthSink(J)Lio/intercom/a/r;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method detachTimeout(Lio/intercom/a/i;)V
    .locals 2

    .prologue
    .line 257
    invoke-virtual {p1}, Lio/intercom/a/i;->a()Lio/intercom/a/t;

    move-result-object v0

    .line 258
    sget-object v1, Lio/intercom/a/t;->NONE:Lio/intercom/a/t;

    invoke-virtual {p1, v1}, Lio/intercom/a/i;->a(Lio/intercom/a/t;)Lio/intercom/a/i;

    .line 259
    invoke-virtual {v0}, Lio/intercom/a/t;->clearDeadline()Lio/intercom/a/t;

    .line 260
    invoke-virtual {v0}, Lio/intercom/a/t;->clearTimeout()Lio/intercom/a/t;

    .line 261
    return-void
.end method

.method public finishRequest()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->sink:Lio/intercom/a/d;

    invoke-interface {v0}, Lio/intercom/a/d;->flush()V

    .line 165
    return-void
.end method

.method public flushRequest()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->sink:Lio/intercom/a/d;

    invoke-interface {v0}, Lio/intercom/a/d;->flush()V

    .line 161
    return-void
.end method

.method public isClosed()Z
    .locals 2

    .prologue
    .line 156
    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newChunkedSink()Lio/intercom/a/r;
    .locals 3

    .prologue
    .line 220
    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    .line 222
    new-instance v0, Lio/intercom/okhttp3/internal/http1/Http1Codec$ChunkedSink;

    invoke-direct {v0, p0}, Lio/intercom/okhttp3/internal/http1/Http1Codec$ChunkedSink;-><init>(Lio/intercom/okhttp3/internal/http1/Http1Codec;)V

    return-object v0
.end method

.method public newChunkedSource(Lio/intercom/okhttp3/HttpUrl;)Lio/intercom/a/s;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    .line 240
    new-instance v0, Lio/intercom/okhttp3/internal/http1/Http1Codec$ChunkedSource;

    invoke-direct {v0, p0, p1}, Lio/intercom/okhttp3/internal/http1/Http1Codec$ChunkedSource;-><init>(Lio/intercom/okhttp3/internal/http1/Http1Codec;Lio/intercom/okhttp3/HttpUrl;)V

    return-object v0
.end method

.method public newFixedLengthSink(J)Lio/intercom/a/r;
    .locals 3

    .prologue
    .line 226
    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    .line 228
    new-instance v0, Lio/intercom/okhttp3/internal/http1/Http1Codec$FixedLengthSink;

    invoke-direct {v0, p0, p1, p2}, Lio/intercom/okhttp3/internal/http1/Http1Codec$FixedLengthSink;-><init>(Lio/intercom/okhttp3/internal/http1/Http1Codec;J)V

    return-object v0
.end method

.method public newFixedLengthSource(J)Lio/intercom/a/s;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    .line 234
    new-instance v0, Lio/intercom/okhttp3/internal/http1/Http1Codec$FixedLengthSource;

    invoke-direct {v0, p0, p1, p2}, Lio/intercom/okhttp3/internal/http1/Http1Codec$FixedLengthSource;-><init>(Lio/intercom/okhttp3/internal/http1/Http1Codec;J)V

    return-object v0
.end method

.method public newUnknownLengthSource()Lio/intercom/a/s;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 244
    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :cond_0
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "streamAllocation == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_1
    const/4 v0, 0x5

    iput v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    .line 247
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    invoke-virtual {v0}, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->noNewStreams()V

    .line 248
    new-instance v0, Lio/intercom/okhttp3/internal/http1/Http1Codec$UnknownLengthSource;

    invoke-direct {v0, p0}, Lio/intercom/okhttp3/internal/http1/Http1Codec$UnknownLengthSource;-><init>(Lio/intercom/okhttp3/internal/http1/Http1Codec;)V

    return-object v0
.end method

.method public openResponseBody(Lio/intercom/okhttp3/Response;)Lio/intercom/okhttp3/ResponseBody;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    .line 132
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    iget-object v0, v0, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->eventListener:Lio/intercom/okhttp3/EventListener;

    iget-object v1, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    iget-object v1, v1, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->call:Lio/intercom/okhttp3/Call;

    invoke-virtual {v0, v1}, Lio/intercom/okhttp3/EventListener;->responseBodyStart(Lio/intercom/okhttp3/Call;)V

    .line 133
    const-string v0, "Content-Type"

    invoke-virtual {p1, v0}, Lio/intercom/okhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-static {p1}, Lio/intercom/okhttp3/internal/http/HttpHeaders;->hasBody(Lio/intercom/okhttp3/Response;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p0, v6, v7}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->newFixedLengthSource(J)Lio/intercom/a/s;

    move-result-object v2

    .line 137
    new-instance v0, Lio/intercom/okhttp3/internal/http/RealResponseBody;

    invoke-static {v2}, Lio/intercom/a/l;->a(Lio/intercom/a/s;)Lio/intercom/a/e;

    move-result-object v2

    invoke-direct {v0, v1, v6, v7, v2}, Lio/intercom/okhttp3/internal/http/RealResponseBody;-><init>(Ljava/lang/String;JLio/intercom/a/e;)V

    .line 151
    :goto_0
    return-object v0

    .line 140
    :cond_0
    const-string v0, "chunked"

    const-string v2, "Transfer-Encoding"

    invoke-virtual {p1, v2}, Lio/intercom/okhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    invoke-virtual {p1}, Lio/intercom/okhttp3/Response;->request()Lio/intercom/okhttp3/Request;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/okhttp3/Request;->url()Lio/intercom/okhttp3/HttpUrl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->newChunkedSource(Lio/intercom/okhttp3/HttpUrl;)Lio/intercom/a/s;

    move-result-object v2

    .line 142
    new-instance v0, Lio/intercom/okhttp3/internal/http/RealResponseBody;

    invoke-static {v2}, Lio/intercom/a/l;->a(Lio/intercom/a/s;)Lio/intercom/a/e;

    move-result-object v2

    invoke-direct {v0, v1, v4, v5, v2}, Lio/intercom/okhttp3/internal/http/RealResponseBody;-><init>(Ljava/lang/String;JLio/intercom/a/e;)V

    goto :goto_0

    .line 145
    :cond_1
    invoke-static {p1}, Lio/intercom/okhttp3/internal/http/HttpHeaders;->contentLength(Lio/intercom/okhttp3/Response;)J

    move-result-wide v2

    .line 146
    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 147
    invoke-virtual {p0, v2, v3}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->newFixedLengthSource(J)Lio/intercom/a/s;

    move-result-object v4

    .line 148
    new-instance v0, Lio/intercom/okhttp3/internal/http/RealResponseBody;

    invoke-static {v4}, Lio/intercom/a/l;->a(Lio/intercom/a/s;)Lio/intercom/a/e;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lio/intercom/okhttp3/internal/http/RealResponseBody;-><init>(Ljava/lang/String;JLio/intercom/a/e;)V

    goto :goto_0

    .line 151
    :cond_2
    new-instance v0, Lio/intercom/okhttp3/internal/http/RealResponseBody;

    invoke-virtual {p0}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->newUnknownLengthSource()Lio/intercom/a/s;

    move-result-object v2

    invoke-static {v2}, Lio/intercom/a/l;->a(Lio/intercom/a/s;)Lio/intercom/a/e;

    move-result-object v2

    invoke-direct {v0, v1, v4, v5, v2}, Lio/intercom/okhttp3/internal/http/RealResponseBody;-><init>(Ljava/lang/String;JLio/intercom/a/e;)V

    goto :goto_0
.end method

.method public readHeaders()Lio/intercom/okhttp3/Headers;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    new-instance v0, Lio/intercom/okhttp3/Headers$Builder;

    invoke-direct {v0}, Lio/intercom/okhttp3/Headers$Builder;-><init>()V

    .line 213
    :goto_0
    iget-object v1, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->source:Lio/intercom/a/e;

    invoke-interface {v1}, Lio/intercom/a/e;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    sget-object v2, Lio/intercom/okhttp3/internal/Internal;->instance:Lio/intercom/okhttp3/internal/Internal;

    invoke-virtual {v2, v0, v1}, Lio/intercom/okhttp3/internal/Internal;->addLenient(Lio/intercom/okhttp3/Headers$Builder;Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_0
    invoke-virtual {v0}, Lio/intercom/okhttp3/Headers$Builder;->build()Lio/intercom/okhttp3/Headers;

    move-result-object v0

    return-object v0
.end method

.method public readResponseHeaders(Z)Lio/intercom/okhttp3/Response$Builder;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    :try_start_0
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->source:Lio/intercom/a/e;

    invoke-interface {v0}, Lio/intercom/a/e;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/intercom/okhttp3/internal/http/StatusLine;->parse(Ljava/lang/String;)Lio/intercom/okhttp3/internal/http/StatusLine;

    move-result-object v1

    .line 189
    new-instance v0, Lio/intercom/okhttp3/Response$Builder;

    invoke-direct {v0}, Lio/intercom/okhttp3/Response$Builder;-><init>()V

    iget-object v2, v1, Lio/intercom/okhttp3/internal/http/StatusLine;->protocol:Lio/intercom/okhttp3/Protocol;

    .line 190
    invoke-virtual {v0, v2}, Lio/intercom/okhttp3/Response$Builder;->protocol(Lio/intercom/okhttp3/Protocol;)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    iget v2, v1, Lio/intercom/okhttp3/internal/http/StatusLine;->code:I

    .line 191
    invoke-virtual {v0, v2}, Lio/intercom/okhttp3/Response$Builder;->code(I)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    iget-object v2, v1, Lio/intercom/okhttp3/internal/http/StatusLine;->message:Ljava/lang/String;

    .line 192
    invoke-virtual {v0, v2}, Lio/intercom/okhttp3/Response$Builder;->message(Ljava/lang/String;)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 193
    invoke-virtual {p0}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->readHeaders()Lio/intercom/okhttp3/Headers;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/intercom/okhttp3/Response$Builder;->headers(Lio/intercom/okhttp3/Headers;)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 195
    if-eqz p1, :cond_1

    iget v1, v1, Lio/intercom/okhttp3/internal/http/StatusLine;->code:I

    const/16 v2, 0x64

    if-ne v1, v2, :cond_1

    .line 196
    const/4 v0, 0x0

    .line 200
    :goto_0
    return-object v0

    .line 199
    :cond_1
    const/4 v1, 0x4

    iput v1, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 203
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unexpected end of stream on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 205
    throw v1
.end method

.method public writeRequest(Lio/intercom/okhttp3/Headers;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    iget v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->sink:Lio/intercom/a/d;

    invoke-interface {v0, p2}, Lio/intercom/a/d;->b(Ljava/lang/String;)Lio/intercom/a/d;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lio/intercom/a/d;->b(Ljava/lang/String;)Lio/intercom/a/d;

    .line 171
    const/4 v0, 0x0

    invoke-virtual {p1}, Lio/intercom/okhttp3/Headers;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 172
    iget-object v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->sink:Lio/intercom/a/d;

    invoke-virtual {p1, v0}, Lio/intercom/okhttp3/Headers;->name(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/intercom/a/d;->b(Ljava/lang/String;)Lio/intercom/a/d;

    move-result-object v2

    const-string v3, ": "

    .line 173
    invoke-interface {v2, v3}, Lio/intercom/a/d;->b(Ljava/lang/String;)Lio/intercom/a/d;

    move-result-object v2

    .line 174
    invoke-virtual {p1, v0}, Lio/intercom/okhttp3/Headers;->value(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/intercom/a/d;->b(Ljava/lang/String;)Lio/intercom/a/d;

    move-result-object v2

    const-string v3, "\r\n"

    .line 175
    invoke-interface {v2, v3}, Lio/intercom/a/d;->b(Ljava/lang/String;)Lio/intercom/a/d;

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_1
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->sink:Lio/intercom/a/d;

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lio/intercom/a/d;->b(Ljava/lang/String;)Lio/intercom/a/d;

    .line 178
    const/4 v0, 0x1

    iput v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    .line 179
    return-void
.end method

.method public writeRequestHeaders(Lio/intercom/okhttp3/Request;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    .line 127
    invoke-virtual {v0}, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->connection()Lio/intercom/okhttp3/internal/connection/RealConnection;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/okhttp3/internal/connection/RealConnection;->route()Lio/intercom/okhttp3/Route;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/okhttp3/Route;->proxy()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    .line 126
    invoke-static {p1, v0}, Lio/intercom/okhttp3/internal/http/RequestLine;->get(Lio/intercom/okhttp3/Request;Ljava/net/Proxy$Type;)Ljava/lang/String;

    move-result-object v0

    .line 128
    invoke-virtual {p1}, Lio/intercom/okhttp3/Request;->headers()Lio/intercom/okhttp3/Headers;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->writeRequest(Lio/intercom/okhttp3/Headers;Ljava/lang/String;)V

    .line 129
    return-void
.end method
