.class abstract Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;
.super Ljava/lang/Object;
.source "Http1Codec.java"

# interfaces
.implements Lio/intercom/a/s;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/intercom/okhttp3/internal/http1/Http1Codec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "AbstractSource"
.end annotation


# instance fields
.field protected bytesRead:J

.field protected closed:Z

.field final synthetic this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

.field protected final timeout:Lio/intercom/a/i;


# direct methods
.method private constructor <init>(Lio/intercom/okhttp3/internal/http1/Http1Codec;)V
    .locals 2

    .prologue
    .line 341
    iput-object p1, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342
    new-instance v0, Lio/intercom/a/i;

    iget-object v1, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget-object v1, v1, Lio/intercom/okhttp3/internal/http1/Http1Codec;->source:Lio/intercom/a/e;

    invoke-interface {v1}, Lio/intercom/a/e;->timeout()Lio/intercom/a/t;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/a/i;-><init>(Lio/intercom/a/t;)V

    iput-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->timeout:Lio/intercom/a/i;

    .line 344
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->bytesRead:J

    return-void
.end method

.method synthetic constructor <init>(Lio/intercom/okhttp3/internal/http1/Http1Codec;Lio/intercom/okhttp3/internal/http1/Http1Codec$1;)V
    .locals 0

    .prologue
    .line 341
    invoke-direct {p0, p1}, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;-><init>(Lio/intercom/okhttp3/internal/http1/Http1Codec;)V

    return-void
.end method


# virtual methods
.method protected final endOfInput(ZLjava/io/IOException;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x6

    .line 368
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget v0, v0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    if-ne v0, v2, :cond_1

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget v0, v0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget v2, v2, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 371
    :cond_2
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget-object v1, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->timeout:Lio/intercom/a/i;

    invoke-virtual {v0, v1}, Lio/intercom/okhttp3/internal/http1/Http1Codec;->detachTimeout(Lio/intercom/a/i;)V

    .line 373
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iput v2, v0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->state:I

    .line 374
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget-object v0, v0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget-object v1, v0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->streamAllocation:Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    if-nez p1, :cond_3

    const/4 v2, 0x1

    :goto_1
    iget-object v3, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget-wide v4, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->bytesRead:J

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->streamFinished(ZLio/intercom/okhttp3/internal/http/HttpCodec;JLjava/io/IOException;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public read(Lio/intercom/a/c;J)J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 352
    :try_start_0
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->this$0:Lio/intercom/okhttp3/internal/http1/Http1Codec;

    iget-object v0, v0, Lio/intercom/okhttp3/internal/http1/Http1Codec;->source:Lio/intercom/a/e;

    invoke-interface {v0, p1, p2, p3}, Lio/intercom/a/e;->read(Lio/intercom/a/c;J)J

    move-result-wide v0

    .line 353
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 354
    iget-wide v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->bytesRead:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->bytesRead:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    :cond_0
    return-wide v0

    .line 357
    :catch_0
    move-exception v0

    .line 358
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->endOfInput(ZLjava/io/IOException;)V

    .line 359
    throw v0
.end method

.method public timeout()Lio/intercom/a/t;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lio/intercom/okhttp3/internal/http1/Http1Codec$AbstractSource;->timeout:Lio/intercom/a/i;

    return-object v0
.end method
