.class public final Lio/intercom/okhttp3/internal/http/CallServerInterceptor;
.super Ljava/lang/Object;
.source "CallServerInterceptor.java"

# interfaces
.implements Lio/intercom/okhttp3/Interceptor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/okhttp3/internal/http/CallServerInterceptor$CountingSink;
    }
.end annotation


# instance fields
.field private final forWebSocket:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean p1, p0, Lio/intercom/okhttp3/internal/http/CallServerInterceptor;->forWebSocket:Z

    .line 38
    return-void
.end method


# virtual methods
.method public intercept(Lio/intercom/okhttp3/Interceptor$Chain;)Lio/intercom/okhttp3/Response;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    check-cast p1, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;

    .line 42
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->httpStream()Lio/intercom/okhttp3/internal/http/HttpCodec;

    move-result-object v2

    .line 43
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->streamAllocation()Lio/intercom/okhttp3/internal/connection/StreamAllocation;

    move-result-object v3

    .line 44
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->connection()Lio/intercom/okhttp3/Connection;

    move-result-object v0

    check-cast v0, Lio/intercom/okhttp3/internal/connection/RealConnection;

    .line 45
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->request()Lio/intercom/okhttp3/Request;

    move-result-object v4

    .line 47
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 49
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->eventListener()Lio/intercom/okhttp3/EventListener;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->call()Lio/intercom/okhttp3/Call;

    move-result-object v5

    invoke-virtual {v1, v5}, Lio/intercom/okhttp3/EventListener;->requestHeadersStart(Lio/intercom/okhttp3/Call;)V

    .line 50
    invoke-interface {v2, v4}, Lio/intercom/okhttp3/internal/http/HttpCodec;->writeRequestHeaders(Lio/intercom/okhttp3/Request;)V

    .line 51
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->eventListener()Lio/intercom/okhttp3/EventListener;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->call()Lio/intercom/okhttp3/Call;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, Lio/intercom/okhttp3/EventListener;->requestHeadersEnd(Lio/intercom/okhttp3/Call;Lio/intercom/okhttp3/Request;)V

    .line 53
    const/4 v1, 0x0

    .line 54
    invoke-virtual {v4}, Lio/intercom/okhttp3/Request;->method()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lio/intercom/okhttp3/internal/http/HttpMethod;->permitsRequestBody(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {v4}, Lio/intercom/okhttp3/Request;->body()Lio/intercom/okhttp3/RequestBody;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 58
    const-string v5, "100-continue"

    const-string v8, "Expect"

    invoke-virtual {v4, v8}, Lio/intercom/okhttp3/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 59
    invoke-interface {v2}, Lio/intercom/okhttp3/internal/http/HttpCodec;->flushRequest()V

    .line 60
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->eventListener()Lio/intercom/okhttp3/EventListener;

    move-result-object v1

    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->call()Lio/intercom/okhttp3/Call;

    move-result-object v5

    invoke-virtual {v1, v5}, Lio/intercom/okhttp3/EventListener;->responseHeadersStart(Lio/intercom/okhttp3/Call;)V

    .line 61
    const/4 v1, 0x1

    invoke-interface {v2, v1}, Lio/intercom/okhttp3/internal/http/HttpCodec;->readResponseHeaders(Z)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v1

    .line 64
    :cond_0
    if-nez v1, :cond_5

    .line 66
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->eventListener()Lio/intercom/okhttp3/EventListener;

    move-result-object v0

    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->call()Lio/intercom/okhttp3/Call;

    move-result-object v5

    invoke-virtual {v0, v5}, Lio/intercom/okhttp3/EventListener;->requestBodyStart(Lio/intercom/okhttp3/Call;)V

    .line 67
    invoke-virtual {v4}, Lio/intercom/okhttp3/Request;->body()Lio/intercom/okhttp3/RequestBody;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/okhttp3/RequestBody;->contentLength()J

    move-result-wide v8

    .line 68
    new-instance v0, Lio/intercom/okhttp3/internal/http/CallServerInterceptor$CountingSink;

    .line 69
    invoke-interface {v2, v4, v8, v9}, Lio/intercom/okhttp3/internal/http/HttpCodec;->createRequestBody(Lio/intercom/okhttp3/Request;J)Lio/intercom/a/r;

    move-result-object v5

    invoke-direct {v0, v5}, Lio/intercom/okhttp3/internal/http/CallServerInterceptor$CountingSink;-><init>(Lio/intercom/a/r;)V

    .line 70
    invoke-static {v0}, Lio/intercom/a/l;->a(Lio/intercom/a/r;)Lio/intercom/a/d;

    move-result-object v5

    .line 72
    invoke-virtual {v4}, Lio/intercom/okhttp3/Request;->body()Lio/intercom/okhttp3/RequestBody;

    move-result-object v8

    invoke-virtual {v8, v5}, Lio/intercom/okhttp3/RequestBody;->writeTo(Lio/intercom/a/d;)V

    .line 73
    invoke-interface {v5}, Lio/intercom/a/d;->close()V

    .line 74
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->eventListener()Lio/intercom/okhttp3/EventListener;

    move-result-object v5

    .line 75
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->call()Lio/intercom/okhttp3/Call;

    move-result-object v8

    iget-wide v10, v0, Lio/intercom/okhttp3/internal/http/CallServerInterceptor$CountingSink;->successfulCount:J

    invoke-virtual {v5, v8, v10, v11}, Lio/intercom/okhttp3/EventListener;->requestBodyEnd(Lio/intercom/okhttp3/Call;J)V

    move-object v0, v1

    .line 84
    :goto_0
    invoke-interface {v2}, Lio/intercom/okhttp3/internal/http/HttpCodec;->finishRequest()V

    .line 86
    if-nez v0, :cond_1

    .line 87
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->eventListener()Lio/intercom/okhttp3/EventListener;

    move-result-object v0

    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->call()Lio/intercom/okhttp3/Call;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/okhttp3/EventListener;->responseHeadersStart(Lio/intercom/okhttp3/Call;)V

    .line 88
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Lio/intercom/okhttp3/internal/http/HttpCodec;->readResponseHeaders(Z)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 92
    :cond_1
    invoke-virtual {v0, v4}, Lio/intercom/okhttp3/Response$Builder;->request(Lio/intercom/okhttp3/Request;)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {v3}, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->connection()Lio/intercom/okhttp3/internal/connection/RealConnection;

    move-result-object v1

    invoke-virtual {v1}, Lio/intercom/okhttp3/internal/connection/RealConnection;->handshake()Lio/intercom/okhttp3/Handshake;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/intercom/okhttp3/Response$Builder;->handshake(Lio/intercom/okhttp3/Handshake;)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {v0, v6, v7}, Lio/intercom/okhttp3/Response$Builder;->sentRequestAtMillis(J)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lio/intercom/okhttp3/Response$Builder;->receivedResponseAtMillis(J)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lio/intercom/okhttp3/Response$Builder;->build()Lio/intercom/okhttp3/Response;

    move-result-object v0

    .line 98
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->eventListener()Lio/intercom/okhttp3/EventListener;

    move-result-object v1

    .line 99
    invoke-virtual {p1}, Lio/intercom/okhttp3/internal/http/RealInterceptorChain;->call()Lio/intercom/okhttp3/Call;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Lio/intercom/okhttp3/EventListener;->responseHeadersEnd(Lio/intercom/okhttp3/Call;Lio/intercom/okhttp3/Response;)V

    .line 101
    invoke-virtual {v0}, Lio/intercom/okhttp3/Response;->code()I

    move-result v1

    .line 102
    iget-boolean v4, p0, Lio/intercom/okhttp3/internal/http/CallServerInterceptor;->forWebSocket:Z

    if-eqz v4, :cond_7

    const/16 v4, 0x65

    if-ne v1, v4, :cond_7

    .line 104
    invoke-virtual {v0}, Lio/intercom/okhttp3/Response;->newBuilder()Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    sget-object v2, Lio/intercom/okhttp3/internal/Util;->EMPTY_RESPONSE:Lio/intercom/okhttp3/ResponseBody;

    .line 105
    invoke-virtual {v0, v2}, Lio/intercom/okhttp3/Response$Builder;->body(Lio/intercom/okhttp3/ResponseBody;)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lio/intercom/okhttp3/Response$Builder;->build()Lio/intercom/okhttp3/Response;

    move-result-object v0

    .line 113
    :goto_1
    const-string v2, "close"

    invoke-virtual {v0}, Lio/intercom/okhttp3/Response;->request()Lio/intercom/okhttp3/Request;

    move-result-object v4

    const-string v5, "Connection"

    invoke-virtual {v4, v5}, Lio/intercom/okhttp3/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "close"

    const-string v4, "Connection"

    .line 114
    invoke-virtual {v0, v4}, Lio/intercom/okhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 115
    :cond_2
    invoke-virtual {v3}, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->noNewStreams()V

    .line 118
    :cond_3
    const/16 v2, 0xcc

    if-eq v1, v2, :cond_4

    const/16 v2, 0xcd

    if-ne v1, v2, :cond_8

    :cond_4
    invoke-virtual {v0}, Lio/intercom/okhttp3/Response;->body()Lio/intercom/okhttp3/ResponseBody;

    move-result-object v2

    invoke-virtual {v2}, Lio/intercom/okhttp3/ResponseBody;->contentLength()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_8

    .line 119
    new-instance v2, Ljava/net/ProtocolException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HTTP "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " had non-zero Content-Length: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 120
    invoke-virtual {v0}, Lio/intercom/okhttp3/Response;->body()Lio/intercom/okhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lio/intercom/okhttp3/ResponseBody;->contentLength()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 76
    :cond_5
    invoke-virtual {v0}, Lio/intercom/okhttp3/internal/connection/RealConnection;->isMultiplexed()Z

    move-result v0

    if-nez v0, :cond_6

    .line 80
    invoke-virtual {v3}, Lio/intercom/okhttp3/internal/connection/StreamAllocation;->noNewStreams()V

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    .line 108
    :cond_7
    invoke-virtual {v0}, Lio/intercom/okhttp3/Response;->newBuilder()Lio/intercom/okhttp3/Response$Builder;

    move-result-object v4

    .line 109
    invoke-interface {v2, v0}, Lio/intercom/okhttp3/internal/http/HttpCodec;->openResponseBody(Lio/intercom/okhttp3/Response;)Lio/intercom/okhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v4, v0}, Lio/intercom/okhttp3/Response$Builder;->body(Lio/intercom/okhttp3/ResponseBody;)Lio/intercom/okhttp3/Response$Builder;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lio/intercom/okhttp3/Response$Builder;->build()Lio/intercom/okhttp3/Response;

    move-result-object v0

    goto :goto_1

    .line 123
    :cond_8
    return-object v0

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method
