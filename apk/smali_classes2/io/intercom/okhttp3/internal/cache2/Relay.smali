.class final Lio/intercom/okhttp3/internal/cache2/Relay;
.super Ljava/lang/Object;
.source "Relay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/intercom/okhttp3/internal/cache2/Relay$RelaySource;
    }
.end annotation


# static fields
.field private static final FILE_HEADER_SIZE:J = 0x20L

.field static final PREFIX_CLEAN:Lio/intercom/a/f;

.field static final PREFIX_DIRTY:Lio/intercom/a/f;

.field private static final SOURCE_FILE:I = 0x2

.field private static final SOURCE_UPSTREAM:I = 0x1


# instance fields
.field final buffer:Lio/intercom/a/c;

.field final bufferMaxSize:J

.field complete:Z

.field file:Ljava/io/RandomAccessFile;

.field private final metadata:Lio/intercom/a/f;

.field sourceCount:I

.field upstream:Lio/intercom/a/s;

.field final upstreamBuffer:Lio/intercom/a/c;

.field upstreamPos:J

.field upstreamReader:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "OkHttp cache v1\n"

    invoke-static {v0}, Lio/intercom/a/f;->a(Ljava/lang/String;)Lio/intercom/a/f;

    move-result-object v0

    sput-object v0, Lio/intercom/okhttp3/internal/cache2/Relay;->PREFIX_CLEAN:Lio/intercom/a/f;

    .line 46
    const-string v0, "OkHttp DIRTY :(\n"

    invoke-static {v0}, Lio/intercom/a/f;->a(Ljava/lang/String;)Lio/intercom/a/f;

    move-result-object v0

    sput-object v0, Lio/intercom/okhttp3/internal/cache2/Relay;->PREFIX_DIRTY:Lio/intercom/a/f;

    return-void
.end method

.method private constructor <init>(Ljava/io/RandomAccessFile;Lio/intercom/a/s;JLio/intercom/a/f;J)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Lio/intercom/a/c;

    invoke-direct {v0}, Lio/intercom/a/c;-><init>()V

    iput-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->upstreamBuffer:Lio/intercom/a/c;

    .line 94
    new-instance v0, Lio/intercom/a/c;

    invoke-direct {v0}, Lio/intercom/a/c;-><init>()V

    iput-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->buffer:Lio/intercom/a/c;

    .line 108
    iput-object p1, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->file:Ljava/io/RandomAccessFile;

    .line 109
    iput-object p2, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->upstream:Lio/intercom/a/s;

    .line 110
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->complete:Z

    .line 111
    iput-wide p3, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->upstreamPos:J

    .line 112
    iput-object p5, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->metadata:Lio/intercom/a/f;

    .line 113
    iput-wide p6, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->bufferMaxSize:J

    .line 114
    return-void

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static edit(Ljava/io/File;Lio/intercom/a/s;Lio/intercom/a/f;J)Lio/intercom/okhttp3/internal/cache2/Relay;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v3, 0x0

    const-wide/16 v8, -0x1

    .line 126
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v1, p0, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 127
    new-instance v0, Lio/intercom/okhttp3/internal/cache2/Relay;

    move-object v2, p1

    move-object v5, p2

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Lio/intercom/okhttp3/internal/cache2/Relay;-><init>(Ljava/io/RandomAccessFile;Lio/intercom/a/s;JLio/intercom/a/f;J)V

    .line 130
    invoke-virtual {v1, v3, v4}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 131
    sget-object v1, Lio/intercom/okhttp3/internal/cache2/Relay;->PREFIX_DIRTY:Lio/intercom/a/f;

    move-wide v2, v8

    move-wide v4, v8

    invoke-direct/range {v0 .. v5}, Lio/intercom/okhttp3/internal/cache2/Relay;->writeHeader(Lio/intercom/a/f;JJ)V

    .line 133
    return-object v0
.end method

.method public static read(Ljava/io/File;)Lio/intercom/okhttp3/internal/cache2/Relay;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x20

    const-wide/16 v1, 0x0

    .line 144
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v10, p0, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 145
    new-instance v0, Lio/intercom/okhttp3/internal/cache2/FileOperator;

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    invoke-direct {v0, v3}, Lio/intercom/okhttp3/internal/cache2/FileOperator;-><init>(Ljava/nio/channels/FileChannel;)V

    .line 148
    new-instance v3, Lio/intercom/a/c;

    invoke-direct {v3}, Lio/intercom/a/c;-><init>()V

    .line 149
    invoke-virtual/range {v0 .. v5}, Lio/intercom/okhttp3/internal/cache2/FileOperator;->read(JLio/intercom/a/c;J)V

    .line 150
    sget-object v6, Lio/intercom/okhttp3/internal/cache2/Relay;->PREFIX_CLEAN:Lio/intercom/a/f;

    invoke-virtual {v6}, Lio/intercom/a/f;->h()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v3, v6, v7}, Lio/intercom/a/c;->d(J)Lio/intercom/a/f;

    move-result-object v6

    .line 151
    sget-object v7, Lio/intercom/okhttp3/internal/cache2/Relay;->PREFIX_CLEAN:Lio/intercom/a/f;

    invoke-virtual {v6, v7}, Lio/intercom/a/f;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "unreadable cache file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    invoke-virtual {v3}, Lio/intercom/a/c;->l()J

    move-result-wide v12

    .line 153
    invoke-virtual {v3}, Lio/intercom/a/c;->l()J

    move-result-wide v8

    .line 156
    new-instance v7, Lio/intercom/a/c;

    invoke-direct {v7}, Lio/intercom/a/c;-><init>()V

    .line 157
    add-long v5, v4, v12

    move-object v4, v0

    invoke-virtual/range {v4 .. v9}, Lio/intercom/okhttp3/internal/cache2/FileOperator;->read(JLio/intercom/a/c;J)V

    .line 158
    invoke-virtual {v7}, Lio/intercom/a/c;->q()Lio/intercom/a/f;

    move-result-object v9

    .line 161
    new-instance v4, Lio/intercom/okhttp3/internal/cache2/Relay;

    const/4 v6, 0x0

    move-object v5, v10

    move-wide v7, v12

    move-wide v10, v1

    invoke-direct/range {v4 .. v11}, Lio/intercom/okhttp3/internal/cache2/Relay;-><init>(Ljava/io/RandomAccessFile;Lio/intercom/a/s;JLio/intercom/a/f;J)V

    return-object v4
.end method

.method private writeHeader(Lio/intercom/a/f;JJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x20

    .line 166
    new-instance v3, Lio/intercom/a/c;

    invoke-direct {v3}, Lio/intercom/a/c;-><init>()V

    .line 167
    invoke-virtual {v3, p1}, Lio/intercom/a/c;->a(Lio/intercom/a/f;)Lio/intercom/a/c;

    .line 168
    invoke-virtual {v3, p2, p3}, Lio/intercom/a/c;->j(J)Lio/intercom/a/c;

    .line 169
    invoke-virtual {v3, p4, p5}, Lio/intercom/a/c;->j(J)Lio/intercom/a/c;

    .line 170
    invoke-virtual {v3}, Lio/intercom/a/c;->a()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 172
    :cond_0
    new-instance v0, Lio/intercom/okhttp3/internal/cache2/FileOperator;

    iget-object v1, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/okhttp3/internal/cache2/FileOperator;-><init>(Ljava/nio/channels/FileChannel;)V

    .line 173
    const-wide/16 v1, 0x0

    invoke-virtual/range {v0 .. v5}, Lio/intercom/okhttp3/internal/cache2/FileOperator;->write(JLio/intercom/a/c;J)V

    .line 174
    return-void
.end method

.method private writeMetadata(J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    new-instance v3, Lio/intercom/a/c;

    invoke-direct {v3}, Lio/intercom/a/c;-><init>()V

    .line 178
    iget-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->metadata:Lio/intercom/a/f;

    invoke-virtual {v3, v0}, Lio/intercom/a/c;->a(Lio/intercom/a/f;)Lio/intercom/a/c;

    .line 180
    new-instance v0, Lio/intercom/okhttp3/internal/cache2/FileOperator;

    iget-object v1, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/intercom/okhttp3/internal/cache2/FileOperator;-><init>(Ljava/nio/channels/FileChannel;)V

    .line 181
    const-wide/16 v4, 0x20

    add-long v1, v4, p1

    iget-object v4, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->metadata:Lio/intercom/a/f;

    invoke-virtual {v4}, Lio/intercom/a/f;->h()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Lio/intercom/okhttp3/internal/cache2/FileOperator;->write(JLio/intercom/a/c;J)V

    .line 182
    return-void
.end method


# virtual methods
.method commit(J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 186
    invoke-direct {p0, p1, p2}, Lio/intercom/okhttp3/internal/cache2/Relay;->writeMetadata(J)V

    .line 187
    iget-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/nio/channels/FileChannel;->force(Z)V

    .line 190
    sget-object v1, Lio/intercom/okhttp3/internal/cache2/Relay;->PREFIX_CLEAN:Lio/intercom/a/f;

    iget-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->metadata:Lio/intercom/a/f;

    invoke-virtual {v0}, Lio/intercom/a/f;->h()I

    move-result v0

    int-to-long v4, v0

    move-object v0, p0

    move-wide v2, p1

    invoke-direct/range {v0 .. v5}, Lio/intercom/okhttp3/internal/cache2/Relay;->writeHeader(Lio/intercom/a/f;JJ)V

    .line 191
    iget-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/nio/channels/FileChannel;->force(Z)V

    .line 194
    monitor-enter p0

    .line 195
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->complete:Z

    .line 196
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    iget-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->upstream:Lio/intercom/a/s;

    invoke-static {v0}, Lio/intercom/okhttp3/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->upstream:Lio/intercom/a/s;

    .line 200
    return-void

    .line 196
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method isClosed()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->file:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public metadata()Lio/intercom/a/f;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->metadata:Lio/intercom/a/f;

    return-object v0
.end method

.method public newSource()Lio/intercom/a/s;
    .locals 1

    .prologue
    .line 216
    monitor-enter p0

    .line 217
    :try_start_0
    iget-object v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->file:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    .line 221
    :goto_0
    return-object v0

    .line 218
    :cond_0
    iget v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->sourceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/intercom/okhttp3/internal/cache2/Relay;->sourceCount:I

    .line 219
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    new-instance v0, Lio/intercom/okhttp3/internal/cache2/Relay$RelaySource;

    invoke-direct {v0, p0}, Lio/intercom/okhttp3/internal/cache2/Relay$RelaySource;-><init>(Lio/intercom/okhttp3/internal/cache2/Relay;)V

    goto :goto_0

    .line 219
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
