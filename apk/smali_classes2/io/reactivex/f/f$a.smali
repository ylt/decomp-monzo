.class final enum Lio/reactivex/f/f$a;
.super Ljava/lang/Enum;
.source "TestObserver.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/f/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/reactivex/f/f$a;",
        ">;",
        "Lio/reactivex/t",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/reactivex/f/f$a;

.field private static final synthetic b:[Lio/reactivex/f/f$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 355
    new-instance v0, Lio/reactivex/f/f$a;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lio/reactivex/f/f$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/reactivex/f/f$a;->a:Lio/reactivex/f/f$a;

    .line 354
    const/4 v0, 0x1

    new-array v0, v0, [Lio/reactivex/f/f$a;

    sget-object v1, Lio/reactivex/f/f$a;->a:Lio/reactivex/f/f$a;

    aput-object v1, v0, v2

    sput-object v0, Lio/reactivex/f/f$a;->b:[Lio/reactivex/f/f$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 354
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/reactivex/f/f$a;
    .locals 1

    .prologue
    .line 354
    const-class v0, Lio/reactivex/f/f$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/reactivex/f/f$a;

    return-object v0
.end method

.method public static values()[Lio/reactivex/f/f$a;
    .locals 1

    .prologue
    .line 354
    sget-object v0, Lio/reactivex/f/f$a;->b:[Lio/reactivex/f/f$a;

    invoke-virtual {v0}, [Lio/reactivex/f/f$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/reactivex/f/f$a;

    return-object v0
.end method


# virtual methods
.method public onComplete()V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 367
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 363
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 359
    return-void
.end method
