.class public final Lio/reactivex/g/a;
.super Ljava/lang/Object;
.source "RxJavaPlugins.java"


# static fields
.field static volatile a:Lio/reactivex/c/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/g",
            "<-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field static volatile b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Ljava/lang/Runnable;",
            "+",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field static volatile c:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;+",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field static volatile d:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;+",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field static volatile e:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;+",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field static volatile f:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;+",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field static volatile g:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/u;",
            "+",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field static volatile h:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/u;",
            "+",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field static volatile i:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/f;",
            "+",
            "Lio/reactivex/f;",
            ">;"
        }
    .end annotation
.end field

.field static volatile j:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/n;",
            "+",
            "Lio/reactivex/n;",
            ">;"
        }
    .end annotation
.end field

.field static volatile k:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/e/a;",
            "+",
            "Lio/reactivex/e/a;",
            ">;"
        }
    .end annotation
.end field

.field static volatile l:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/h;",
            "+",
            "Lio/reactivex/h;",
            ">;"
        }
    .end annotation
.end field

.field static volatile m:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/v;",
            "+",
            "Lio/reactivex/v;",
            ">;"
        }
    .end annotation
.end field

.field static volatile n:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/b;",
            "+",
            "Lio/reactivex/b;",
            ">;"
        }
    .end annotation
.end field

.field static volatile o:Lio/reactivex/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/c",
            "<-",
            "Lio/reactivex/f;",
            "-",
            "Lorg/a/b;",
            "+",
            "Lorg/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static volatile p:Lio/reactivex/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/c",
            "<-",
            "Lio/reactivex/h;",
            "-",
            "Lio/reactivex/j;",
            "+",
            "Lio/reactivex/j;",
            ">;"
        }
    .end annotation
.end field

.field static volatile q:Lio/reactivex/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/c",
            "<-",
            "Lio/reactivex/n;",
            "-",
            "Lio/reactivex/t;",
            "+",
            "Lio/reactivex/t;",
            ">;"
        }
    .end annotation
.end field

.field static volatile r:Lio/reactivex/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/c",
            "<-",
            "Lio/reactivex/v;",
            "-",
            "Lio/reactivex/x;",
            "+",
            "Lio/reactivex/x;",
            ">;"
        }
    .end annotation
.end field

.field static volatile s:Lio/reactivex/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/c",
            "<-",
            "Lio/reactivex/b;",
            "-",
            "Lio/reactivex/c;",
            "+",
            "Lio/reactivex/c;",
            ">;"
        }
    .end annotation
.end field

.field static volatile t:Lio/reactivex/c/e;

.field static volatile u:Z


# direct methods
.method public static a(Lio/reactivex/b;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 1078
    sget-object v0, Lio/reactivex/g/a;->n:Lio/reactivex/c/h;

    .line 1079
    if-eqz v0, :cond_0

    .line 1080
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b;

    .line 1082
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lio/reactivex/b;Lio/reactivex/c;)Lio/reactivex/c;
    .locals 1

    .prologue
    .line 951
    sget-object v0, Lio/reactivex/g/a;->s:Lio/reactivex/c/c;

    .line 952
    if-eqz v0, :cond_0

    .line 953
    invoke-static {v0, p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/c/c;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/c;

    .line 955
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public static a(Lio/reactivex/e/a;)Lio/reactivex/e/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/e/a",
            "<TT;>;)",
            "Lio/reactivex/e/a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1048
    sget-object v0, Lio/reactivex/g/a;->k:Lio/reactivex/c/h;

    .line 1049
    if-eqz v0, :cond_0

    .line 1050
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/e/a;

    .line 1052
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lio/reactivex/f;)Lio/reactivex/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/f",
            "<TT;>;)",
            "Lio/reactivex/f",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1000
    sget-object v0, Lio/reactivex/g/a;->i:Lio/reactivex/c/h;

    .line 1001
    if-eqz v0, :cond_0

    .line 1002
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/f;

    .line 1004
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lio/reactivex/h;)Lio/reactivex/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/h",
            "<TT;>;)",
            "Lio/reactivex/h",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 984
    sget-object v0, Lio/reactivex/g/a;->l:Lio/reactivex/c/h;

    .line 985
    if-eqz v0, :cond_0

    .line 986
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/h;

    .line 988
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lio/reactivex/h;Lio/reactivex/j;)Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/h",
            "<TT;>;",
            "Lio/reactivex/j",
            "<-TT;>;)",
            "Lio/reactivex/j",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 968
    sget-object v0, Lio/reactivex/g/a;->p:Lio/reactivex/c/c;

    .line 969
    if-eqz v0, :cond_0

    .line 970
    invoke-static {v0, p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/c/c;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/j;

    .line 972
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public static a(Lio/reactivex/n;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;)",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1032
    sget-object v0, Lio/reactivex/g/a;->j:Lio/reactivex/c/h;

    .line 1033
    if-eqz v0, :cond_0

    .line 1034
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/n;

    .line 1036
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lio/reactivex/n;Lio/reactivex/t;)Lio/reactivex/t;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;",
            "Lio/reactivex/t",
            "<-TT;>;)",
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 919
    sget-object v0, Lio/reactivex/g/a;->q:Lio/reactivex/c/c;

    .line 920
    if-eqz v0, :cond_0

    .line 921
    invoke-static {v0, p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/c/c;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/t;

    .line 923
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method static a(Lio/reactivex/c/h;Ljava/util/concurrent/Callable;)Lio/reactivex/u;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h",
            "<-",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;+",
            "Lio/reactivex/u;",
            ">;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;)",
            "Lio/reactivex/u;"
        }
    .end annotation

    .prologue
    .line 1303
    invoke-static {p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "Scheduler Callable result can\'t be null"

    invoke-static {v0, v1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/u;

    return-object v0
.end method

.method public static a(Lio/reactivex/u;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 331
    sget-object v0, Lio/reactivex/g/a;->g:Lio/reactivex/c/h;

    .line 332
    if-nez v0, :cond_0

    .line 335
    :goto_0
    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/u;

    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lio/reactivex/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;)",
            "Lio/reactivex/u;"
        }
    .end annotation

    .prologue
    .line 268
    const-string v0, "Scheduler Callable can\'t be null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lio/reactivex/g/a;->c:Lio/reactivex/c/h;

    .line 270
    if-nez v0, :cond_0

    .line 271
    invoke-static {p0}, Lio/reactivex/g/a;->e(Ljava/util/concurrent/Callable;)Lio/reactivex/u;

    move-result-object v0

    .line 273
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/util/concurrent/Callable;)Lio/reactivex/u;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lio/reactivex/v;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/v",
            "<TT;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1064
    sget-object v0, Lio/reactivex/g/a;->m:Lio/reactivex/c/h;

    .line 1065
    if-eqz v0, :cond_0

    .line 1066
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/v;

    .line 1068
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lio/reactivex/v;Lio/reactivex/x;)Lio/reactivex/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/v",
            "<TT;>;",
            "Lio/reactivex/x",
            "<-TT;>;)",
            "Lio/reactivex/x",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 936
    sget-object v0, Lio/reactivex/g/a;->r:Lio/reactivex/c/c;

    .line 937
    if-eqz v0, :cond_0

    .line 938
    invoke-static {v0, p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/c/c;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/x;

    .line 940
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method static a(Lio/reactivex/c/c;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/c",
            "<TT;TU;TR;>;TT;TU;)TR;"
        }
    .end annotation

    .prologue
    .line 1271
    :try_start_0
    invoke-interface {p0, p1, p2}, Lio/reactivex/c/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1272
    :catch_0
    move-exception v0

    .line 1273
    invoke-static {v0}, Lio/reactivex/d/j/j;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method static a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<TT;TR;>;TT;)TR;"
        }
    .end annotation

    .prologue
    .line 1251
    :try_start_0
    invoke-interface {p0, p1}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1252
    :catch_0
    move-exception v0

    .line 1253
    invoke-static {v0}, Lio/reactivex/d/j/j;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static a(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 449
    sget-object v0, Lio/reactivex/g/a;->b:Lio/reactivex/c/h;

    .line 450
    if-nez v0, :cond_0

    .line 453
    :goto_0
    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Lio/reactivex/f;Lorg/a/b;)Lorg/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/f",
            "<TT;>;",
            "Lorg/a/b",
            "<-TT;>;)",
            "Lorg/a/b",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 902
    sget-object v0, Lio/reactivex/g/a;->o:Lio/reactivex/c/c;

    .line 903
    if-eqz v0, :cond_0

    .line 904
    invoke-static {v0, p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/c/c;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/a/b;

    .line 906
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 343
    sget-object v1, Lio/reactivex/g/a;->a:Lio/reactivex/c/g;

    .line 345
    if-nez p0, :cond_1

    .line 346
    new-instance p0, Ljava/lang/NullPointerException;

    const-string v0, "onError called with null. Null values are generally not allowed in 2.x operators and sources."

    invoke-direct {p0, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 353
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    .line 355
    :try_start_0
    invoke-interface {v1, p0}, Lio/reactivex/c/g;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :goto_1
    return-void

    .line 348
    :cond_1
    invoke-static {p0}, Lio/reactivex/g/a;->b(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 349
    new-instance v0, Lio/reactivex/exceptions/UndeliverableException;

    invoke-direct {v0, p0}, Lio/reactivex/exceptions/UndeliverableException;-><init>(Ljava/lang/Throwable;)V

    move-object p0, v0

    goto :goto_0

    .line 357
    :catch_0
    move-exception v0

    .line 359
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 360
    invoke-static {v0}, Lio/reactivex/g/a;->c(Ljava/lang/Throwable;)V

    .line 364
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 365
    invoke-static {p0}, Lio/reactivex/g/a;->c(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 167
    sget-boolean v0, Lio/reactivex/g/a;->u:Z

    return v0
.end method

.method public static b(Lio/reactivex/u;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 421
    sget-object v0, Lio/reactivex/g/a;->h:Lio/reactivex/c/h;

    .line 422
    if-nez v0, :cond_0

    .line 425
    :goto_0
    return-object p0

    :cond_0
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/u;

    move-object p0, v0

    goto :goto_0
.end method

.method public static b(Ljava/util/concurrent/Callable;)Lio/reactivex/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;)",
            "Lio/reactivex/u;"
        }
    .end annotation

    .prologue
    .line 284
    const-string v0, "Scheduler Callable can\'t be null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lio/reactivex/g/a;->e:Lio/reactivex/c/h;

    .line 286
    if-nez v0, :cond_0

    .line 287
    invoke-static {p0}, Lio/reactivex/g/a;->e(Ljava/util/concurrent/Callable;)Lio/reactivex/u;

    move-result-object v0

    .line 289
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/util/concurrent/Callable;)Lio/reactivex/u;

    move-result-object v0

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 1143
    sget-object v0, Lio/reactivex/g/a;->t:Lio/reactivex/c/e;

    .line 1144
    if-eqz v0, :cond_0

    .line 1146
    :try_start_0
    invoke-interface {v0}, Lio/reactivex/c/e;->a()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1151
    :goto_0
    return v0

    .line 1147
    :catch_0
    move-exception v0

    .line 1148
    invoke-static {v0}, Lio/reactivex/d/j/j;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 1151
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Ljava/lang/Throwable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 378
    instance-of v1, p0, Lio/reactivex/exceptions/OnErrorNotImplementedException;

    if-eqz v1, :cond_1

    .line 405
    :cond_0
    :goto_0
    return v0

    .line 383
    :cond_1
    instance-of v1, p0, Lio/reactivex/exceptions/MissingBackpressureException;

    if-nez v1, :cond_0

    .line 388
    instance-of v1, p0, Ljava/lang/IllegalStateException;

    if-nez v1, :cond_0

    .line 393
    instance-of v1, p0, Ljava/lang/NullPointerException;

    if-nez v1, :cond_0

    .line 397
    instance-of v1, p0, Ljava/lang/IllegalArgumentException;

    if-nez v1, :cond_0

    .line 401
    instance-of v1, p0, Lio/reactivex/exceptions/CompositeException;

    if-nez v1, :cond_0

    .line 405
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/util/concurrent/Callable;)Lio/reactivex/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;)",
            "Lio/reactivex/u;"
        }
    .end annotation

    .prologue
    .line 300
    const-string v0, "Scheduler Callable can\'t be null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lio/reactivex/g/a;->f:Lio/reactivex/c/h;

    .line 302
    if-nez v0, :cond_0

    .line 303
    invoke-static {p0}, Lio/reactivex/g/a;->e(Ljava/util/concurrent/Callable;)Lio/reactivex/u;

    move-result-object v0

    .line 305
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/util/concurrent/Callable;)Lio/reactivex/u;

    move-result-object v0

    goto :goto_0
.end method

.method static c(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 409
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 410
    invoke-virtual {v0}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    .line 411
    invoke-interface {v1, v0, p0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 412
    return-void
.end method

.method public static d(Ljava/util/concurrent/Callable;)Lio/reactivex/u;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;)",
            "Lio/reactivex/u;"
        }
    .end annotation

    .prologue
    .line 316
    const-string v0, "Scheduler Callable can\'t be null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 317
    sget-object v0, Lio/reactivex/g/a;->d:Lio/reactivex/c/h;

    .line 318
    if-nez v0, :cond_0

    .line 319
    invoke-static {p0}, Lio/reactivex/g/a;->e(Ljava/util/concurrent/Callable;)Lio/reactivex/u;

    move-result-object v0

    .line 321
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p0}, Lio/reactivex/g/a;->a(Lio/reactivex/c/h;Ljava/util/concurrent/Callable;)Lio/reactivex/u;

    move-result-object v0

    goto :goto_0
.end method

.method static e(Ljava/util/concurrent/Callable;)Lio/reactivex/u;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/u;",
            ">;)",
            "Lio/reactivex/u;"
        }
    .end annotation

    .prologue
    .line 1287
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "Scheduler Callable result can\'t be null"

    invoke-static {v0, v1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/u;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1288
    :catch_0
    move-exception v0

    .line 1289
    invoke-static {v0}, Lio/reactivex/d/j/j;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
