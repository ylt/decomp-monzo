.class public final Lio/reactivex/i/d;
.super Lio/reactivex/i/c;
.source "UnicastSubject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/i/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/i/c",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/f/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/f/c",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lio/reactivex/t",
            "<-TT;>;>;"
        }
    .end annotation
.end field

.field final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field final d:Z

.field volatile e:Z

.field volatile f:Z

.field g:Ljava/lang/Throwable;

.field final h:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final i:Lio/reactivex/d/d/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/d/b",
            "<TT;>;"
        }
    .end annotation
.end field

.field j:Z


# direct methods
.method constructor <init>(ILjava/lang/Runnable;Z)V
    .locals 2

    .prologue
    .line 190
    invoke-direct {p0}, Lio/reactivex/i/c;-><init>()V

    .line 191
    new-instance v0, Lio/reactivex/d/f/c;

    const-string v1, "capacityHint"

    invoke-static {p1, v1}, Lio/reactivex/d/b/b;->a(ILjava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Lio/reactivex/d/f/c;-><init>(I)V

    iput-object v0, p0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    .line 192
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v1, "onTerminate"

    invoke-static {p2, v1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lio/reactivex/i/d;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 193
    iput-boolean p3, p0, Lio/reactivex/i/d;->d:Z

    .line 194
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 195
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lio/reactivex/i/d;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 196
    new-instance v0, Lio/reactivex/i/d$a;

    invoke-direct {v0, p0}, Lio/reactivex/i/d$a;-><init>(Lio/reactivex/i/d;)V

    iput-object v0, p0, Lio/reactivex/i/d;->i:Lio/reactivex/d/d/b;

    .line 197
    return-void
.end method

.method constructor <init>(IZ)V
    .locals 2

    .prologue
    .line 161
    invoke-direct {p0}, Lio/reactivex/i/c;-><init>()V

    .line 162
    new-instance v0, Lio/reactivex/d/f/c;

    const-string v1, "capacityHint"

    invoke-static {p1, v1}, Lio/reactivex/d/b/b;->a(ILjava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Lio/reactivex/d/f/c;-><init>(I)V

    iput-object v0, p0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    .line 163
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lio/reactivex/i/d;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 164
    iput-boolean p2, p0, Lio/reactivex/i/d;->d:Z

    .line 165
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 166
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lio/reactivex/i/d;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 167
    new-instance v0, Lio/reactivex/i/d$a;

    invoke-direct {v0, p0}, Lio/reactivex/i/d$a;-><init>(Lio/reactivex/i/d;)V

    iput-object v0, p0, Lio/reactivex/i/d;->i:Lio/reactivex/d/d/b;

    .line 168
    return-void
.end method

.method public static a()Lio/reactivex/i/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/i/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lio/reactivex/i/d;

    invoke-static {}, Lio/reactivex/i/d;->bufferSize()I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lio/reactivex/i/d;-><init>(IZ)V

    return-object v0
.end method

.method public static a(I)Lio/reactivex/i/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lio/reactivex/i/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lio/reactivex/i/d;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lio/reactivex/i/d;-><init>(IZ)V

    return-object v0
.end method

.method public static a(ILjava/lang/Runnable;)Lio/reactivex/i/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Runnable;",
            ")",
            "Lio/reactivex/i/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lio/reactivex/i/d;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lio/reactivex/i/d;-><init>(ILjava/lang/Runnable;Z)V

    return-object v0
.end method


# virtual methods
.method a(Lio/reactivex/t;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 271
    .line 272
    iget-object v6, p0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    .line 273
    iget-boolean v0, p0, Lio/reactivex/i/d;->d:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    move v3, v1

    move v4, v1

    .line 278
    :cond_0
    :goto_1
    iget-boolean v5, p0, Lio/reactivex/i/d;->e:Z

    if-eqz v5, :cond_3

    .line 279
    iget-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 280
    invoke-interface {v6}, Lio/reactivex/d/c/i;->c()V

    .line 315
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 273
    goto :goto_0

    .line 284
    :cond_3
    iget-boolean v7, p0, Lio/reactivex/i/d;->f:Z

    .line 285
    iget-object v5, p0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    invoke-virtual {v5}, Lio/reactivex/d/f/c;->n_()Ljava/lang/Object;

    move-result-object v8

    .line 286
    if-nez v8, :cond_5

    move v5, v1

    .line 288
    :goto_3
    if-eqz v7, :cond_6

    .line 289
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 290
    invoke-virtual {p0, v6, p1}, Lio/reactivex/i/d;->a(Lio/reactivex/d/c/i;Lio/reactivex/t;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v2

    .line 297
    :cond_4
    if-eqz v5, :cond_6

    .line 298
    invoke-virtual {p0, p1}, Lio/reactivex/i/d;->c(Lio/reactivex/t;)V

    goto :goto_2

    :cond_5
    move v5, v2

    .line 286
    goto :goto_3

    .line 303
    :cond_6
    if-eqz v5, :cond_7

    .line 310
    iget-object v5, p0, Lio/reactivex/i/d;->i:Lio/reactivex/d/d/b;

    neg-int v4, v4

    invoke-virtual {v5, v4}, Lio/reactivex/d/d/b;->addAndGet(I)I

    move-result v4

    .line 311
    if-nez v4, :cond_0

    goto :goto_2

    .line 307
    :cond_7
    invoke-interface {p1, v8}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method a(Lio/reactivex/d/c/i;Lio/reactivex/t;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/c/i",
            "<TT;>;",
            "Lio/reactivex/t",
            "<-TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lio/reactivex/i/d;->g:Ljava/lang/Throwable;

    .line 364
    if-eqz v0, :cond_0

    .line 365
    iget-object v1, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 366
    invoke-interface {p1}, Lio/reactivex/d/c/i;->c()V

    .line 367
    invoke-interface {p2, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 368
    const/4 v0, 0x1

    .line 370
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Lio/reactivex/t;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 318
    .line 320
    iget-object v2, p0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    .line 321
    iget-boolean v0, p0, Lio/reactivex/i/d;->d:Z

    if-nez v0, :cond_2

    move v0, v1

    .line 325
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lio/reactivex/i/d;->e:Z

    if-eqz v3, :cond_3

    .line 326
    iget-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 327
    invoke-virtual {v2}, Lio/reactivex/d/f/c;->c()V

    .line 350
    :cond_1
    :goto_1
    return-void

    .line 321
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 330
    :cond_3
    iget-boolean v3, p0, Lio/reactivex/i/d;->f:Z

    .line 332
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 333
    invoke-virtual {p0, v2, p1}, Lio/reactivex/i/d;->a(Lio/reactivex/d/c/i;Lio/reactivex/t;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 338
    :cond_4
    invoke-interface {p1, v5}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 340
    if-eqz v3, :cond_5

    .line 341
    invoke-virtual {p0, p1}, Lio/reactivex/i/d;->c(Lio/reactivex/t;)V

    goto :goto_1

    .line 345
    :cond_5
    iget-object v3, p0, Lio/reactivex/i/d;->i:Lio/reactivex/d/d/b;

    neg-int v1, v1

    invoke-virtual {v3, v1}, Lio/reactivex/d/d/b;->addAndGet(I)I

    move-result v1

    .line 346
    if-nez v1, :cond_0

    goto :goto_1
.end method

.method c()V
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lio/reactivex/i/d;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 216
    if-eqz v0, :cond_0

    iget-object v1, p0, Lio/reactivex/i/d;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 219
    :cond_0
    return-void
.end method

.method c(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 353
    iget-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 354
    iget-object v0, p0, Lio/reactivex/i/d;->g:Ljava/lang/Throwable;

    .line 355
    if-eqz v0, :cond_0

    .line 356
    invoke-interface {p1, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 360
    :goto_0
    return-void

    .line 358
    :cond_0
    invoke-interface {p1}, Lio/reactivex/t;->onComplete()V

    goto :goto_0
.end method

.method d()V
    .locals 3

    .prologue
    .line 375
    iget-object v0, p0, Lio/reactivex/i/d;->i:Lio/reactivex/d/d/b;

    invoke-virtual {v0}, Lio/reactivex/d/d/b;->getAndIncrement()I

    move-result v0

    if-eqz v0, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    iget-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/t;

    .line 380
    const/4 v1, 0x1

    move v2, v1

    move-object v1, v0

    move v0, v2

    .line 384
    :goto_1
    if-eqz v1, :cond_3

    .line 385
    iget-boolean v0, p0, Lio/reactivex/i/d;->j:Z

    if-eqz v0, :cond_2

    .line 386
    invoke-virtual {p0, v1}, Lio/reactivex/i/d;->b(Lio/reactivex/t;)V

    goto :goto_0

    .line 388
    :cond_2
    invoke-virtual {p0, v1}, Lio/reactivex/i/d;->a(Lio/reactivex/t;)V

    goto :goto_0

    .line 393
    :cond_3
    iget-object v1, p0, Lio/reactivex/i/d;->i:Lio/reactivex/d/d/b;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Lio/reactivex/d/d/b;->addAndGet(I)I

    move-result v1

    .line 394
    if-eqz v1, :cond_0

    .line 398
    iget-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/t;

    move v2, v1

    move-object v1, v0

    move v0, v2

    goto :goto_1
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lio/reactivex/i/d;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio/reactivex/i/d;->e:Z

    if-eqz v0, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/i/d;->f:Z

    .line 265
    invoke-virtual {p0}, Lio/reactivex/i/d;->c()V

    .line 267
    invoke-virtual {p0}, Lio/reactivex/i/d;->d()V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lio/reactivex/i/d;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio/reactivex/i/d;->e:Z

    if-eqz v0, :cond_1

    .line 244
    :cond_0
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 256
    :goto_0
    return-void

    .line 247
    :cond_1
    if-nez p1, :cond_2

    .line 248
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "onError called with null. Null values are generally not allowed in 2.x operators and sources."

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 250
    :cond_2
    iput-object p1, p0, Lio/reactivex/i/d;->g:Ljava/lang/Throwable;

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/i/d;->f:Z

    .line 253
    invoke-virtual {p0}, Lio/reactivex/i/d;->c()V

    .line 255
    invoke-virtual {p0}, Lio/reactivex/i/d;->d()V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 230
    iget-boolean v0, p0, Lio/reactivex/i/d;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio/reactivex/i/d;->e:Z

    if-eqz v0, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    if-nez p1, :cond_2

    .line 234
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "onNext called with null. Null values are generally not allowed in 2.x operators and sources."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lio/reactivex/i/d;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 237
    :cond_2
    iget-object v0, p0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    invoke-virtual {v0, p1}, Lio/reactivex/d/f/c;->a(Ljava/lang/Object;)Z

    .line 238
    invoke-virtual {p0}, Lio/reactivex/i/d;->d()V

    goto :goto_0
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lio/reactivex/i/d;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio/reactivex/i/d;->e:Z

    if-eqz v0, :cond_1

    .line 224
    :cond_0
    invoke-interface {p1}, Lio/reactivex/b/b;->dispose()V

    .line 226
    :cond_1
    return-void
.end method

.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lio/reactivex/i/d;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lio/reactivex/i/d;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lio/reactivex/i/d;->i:Lio/reactivex/d/d/b;

    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 203
    iget-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 204
    iget-boolean v0, p0, Lio/reactivex/i/d;->e:Z

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 212
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/i/d;->d()V

    goto :goto_0

    .line 210
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Only a single observer allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p1}, Lio/reactivex/d/a/e;->a(Ljava/lang/Throwable;Lio/reactivex/t;)V

    goto :goto_0
.end method
