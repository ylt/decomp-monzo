.class final Lio/reactivex/i/d$a;
.super Lio/reactivex/d/d/b;
.source "UnicastSubject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/i/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/d/d/b",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/i/d;


# direct methods
.method constructor <init>(Lio/reactivex/i/d;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    invoke-direct {p0}, Lio/reactivex/d/d/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    .line 432
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lio/reactivex/i/d;->j:Z

    .line 434
    const/4 v0, 0x2

    .line 436
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-object v0, v0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    invoke-virtual {v0}, Lio/reactivex/d/f/c;->b()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-object v0, v0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    invoke-virtual {v0}, Lio/reactivex/d/f/c;->c()V

    .line 453
    return-void
.end method

.method public dispose()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 457
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-boolean v0, v0, Lio/reactivex/i/d;->e:Z

    if-nez v0, :cond_0

    .line 458
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lio/reactivex/i/d;->e:Z

    .line 460
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    invoke-virtual {v0}, Lio/reactivex/i/d;->c()V

    .line 462
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-object v0, v0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 463
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-object v0, v0, Lio/reactivex/i/d;->i:Lio/reactivex/d/d/b;

    invoke-virtual {v0}, Lio/reactivex/d/d/b;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 464
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-object v0, v0, Lio/reactivex/i/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 465
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-object v0, v0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    invoke-virtual {v0}, Lio/reactivex/d/f/c;->c()V

    .line 468
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-boolean v0, v0, Lio/reactivex/i/d;->e:Z

    return v0
.end method

.method public n_()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 442
    iget-object v0, p0, Lio/reactivex/i/d$a;->a:Lio/reactivex/i/d;

    iget-object v0, v0, Lio/reactivex/i/d;->a:Lio/reactivex/d/f/c;

    invoke-virtual {v0}, Lio/reactivex/d/f/c;->n_()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
