.class public abstract Lio/reactivex/v;
.super Ljava/lang/Object;
.source "Single.java"

# interfaces
.implements Lio/reactivex/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/z",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs a(Lio/reactivex/c/h;[Lio/reactivex/z;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-[",
            "Ljava/lang/Object;",
            "+TR;>;[",
            "Lio/reactivex/z",
            "<+TT;>;)",
            "Lio/reactivex/v",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1486
    const-string v0, "zipper is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1487
    const-string v0, "sources is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1488
    array-length v0, p1

    if-nez v0, :cond_0

    .line 1489
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;

    move-result-object v0

    .line 1491
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/reactivex/d/e/e/w;

    invoke-direct {v0, p1, p0}, Lio/reactivex/d/e/e/w;-><init>([Lio/reactivex/z;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lio/reactivex/f;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/f",
            "<TT;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 3163
    new-instance v0, Lio/reactivex/d/e/b/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/reactivex/d/e/b/j;-><init>(Lio/reactivex/f;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/reactivex/y;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/y",
            "<TT;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 361
    const-string v0, "source is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 362
    new-instance v0, Lio/reactivex/d/e/e/a;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/a;-><init>(Lio/reactivex/y;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/z",
            "<+TT1;>;",
            "Lio/reactivex/z",
            "<+TT2;>;",
            "Lio/reactivex/c/c",
            "<-TT1;-TT2;+TR;>;)",
            "Lio/reactivex/v",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1079
    const-string v0, "source1 is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1080
    const-string v0, "source2 is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1081
    invoke-static {p2}, Lio/reactivex/d/b/a;->a(Lio/reactivex/c/c;)Lio/reactivex/c/h;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Lio/reactivex/z;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;[Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/i;)Lio/reactivex/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/z",
            "<+TT1;>;",
            "Lio/reactivex/z",
            "<+TT2;>;",
            "Lio/reactivex/z",
            "<+TT3;>;",
            "Lio/reactivex/c/i",
            "<-TT1;-TT2;-TT3;+TR;>;)",
            "Lio/reactivex/v",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1118
    const-string v0, "source1 is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1119
    const-string v0, "source2 is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1120
    const-string v0, "source3 is null"

    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1121
    invoke-static {p3}, Lio/reactivex/d/b/a;->a(Lio/reactivex/c/i;)Lio/reactivex/c/h;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Lio/reactivex/z;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;[Lio/reactivex/z;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;Lio/reactivex/c/h;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+",
            "Lio/reactivex/z",
            "<+TT;>;>;",
            "Lio/reactivex/c/h",
            "<-[",
            "Ljava/lang/Object;",
            "+TR;>;)",
            "Lio/reactivex/v",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1044
    const-string v0, "zipper is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1045
    const-string v0, "sources is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1046
    new-instance v0, Lio/reactivex/d/e/e/x;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/x;-><init>(Ljava/lang/Iterable;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 645
    const-string v0, "value is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 646
    new-instance v0, Lio/reactivex/d/e/e/n;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/n;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Throwable;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 423
    const-string v0, "error is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 424
    invoke-static {p0}, Lio/reactivex/d/b/a;->a(Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+",
            "Lio/reactivex/z",
            "<+TT;>;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 380
    const-string v0, "singleSupplier is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 381
    new-instance v0, Lio/reactivex/d/e/e/b;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/b;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/concurrent/Callable;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 398
    const-string v0, "errorSupplier is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 399
    new-instance v0, Lio/reactivex/d/e/e/j;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/j;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+TT;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 447
    const-string v0, "callable is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 448
    new-instance v0, Lio/reactivex/d/e/e/m;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/m;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public static o_()Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 846
    sget-object v0, Lio/reactivex/d/e/e/p;->a:Lio/reactivex/v;

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g",
            "<-TT;>;",
            "Lio/reactivex/c/g",
            "<-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/b/b;"
        }
    .end annotation

    .prologue
    .line 2682
    const-string v0, "onSuccess is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2683
    const-string v0, "onError is null"

    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2685
    new-instance v0, Lio/reactivex/d/d/j;

    invoke-direct {v0, p1, p2}, Lio/reactivex/d/d/j;-><init>(Lio/reactivex/c/g;Lio/reactivex/c/g;)V

    .line 2686
    invoke-virtual {p0, v0}, Lio/reactivex/v;->a(Lio/reactivex/x;)V

    .line 2687
    return-object v0
.end method

.method public final a(Lio/reactivex/c/q;)Lio/reactivex/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/q",
            "<-TT;>;)",
            "Lio/reactivex/h",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1965
    const-string v0, "predicate is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1966
    new-instance v0, Lio/reactivex/d/e/c/f;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/c/f;-><init>(Lio/reactivex/z;Lio/reactivex/c/q;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/h;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2530
    invoke-virtual {p0}, Lio/reactivex/v;->d()Lio/reactivex/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lio/reactivex/f;->a(J)Lio/reactivex/f;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Lio/reactivex/f;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/c/a;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/a;",
            ")",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1850
    const-string v0, "onFinally is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1851
    new-instance v0, Lio/reactivex/d/e/e/e;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/e;-><init>(Lio/reactivex/z;Lio/reactivex/c/a;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/c/b;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/b",
            "<-TT;-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1904
    const-string v0, "onEvent is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1905
    new-instance v0, Lio/reactivex/d/e/e/g;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/g;-><init>(Lio/reactivex/z;Lio/reactivex/c/b;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/c/d;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/d",
            "<-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2548
    invoke-virtual {p0}, Lio/reactivex/v;->d()Lio/reactivex/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/f;->a(Lio/reactivex/c/d;)Lio/reactivex/f;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Lio/reactivex/f;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/c/g;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g",
            "<-TT;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1801
    const-string v0, "doAfterSuccess is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1802
    new-instance v0, Lio/reactivex/d/e/e/d;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/d;-><init>(Lio/reactivex/z;Lio/reactivex/c/g;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/c/h;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/z",
            "<+TR;>;>;)",
            "Lio/reactivex/v",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1988
    const-string v0, "mapper is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1989
    new-instance v0, Lio/reactivex/d/e/e/k;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/k;-><init>(Lio/reactivex/z;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/u;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u;",
            ")",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2295
    const-string v0, "scheduler is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2296
    new-instance v0, Lio/reactivex/d/e/e/q;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/q;-><init>(Lio/reactivex/z;Lio/reactivex/u;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/v;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/v",
            "<+TT;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2379
    const-string v0, "resumeSingleInCaseOfError is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2380
    invoke-static {p1}, Lio/reactivex/d/b/a;->b(Ljava/lang/Object;)Lio/reactivex/c/h;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/reactivex/v;->f(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/z",
            "<TU;>;",
            "Lio/reactivex/c/c",
            "<-TT;-TU;+TR;>;)",
            "Lio/reactivex/v",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 3114
    invoke-static {p0, p1, p2}, Lio/reactivex/v;->a(Lio/reactivex/z;Lio/reactivex/z;Lio/reactivex/c/c;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TU;>;)",
            "Lio/reactivex/v",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 1587
    const-string v0, "clazz is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1588
    invoke-static {p1}, Lio/reactivex/d/b/a;->a(Ljava/lang/Class;)Lio/reactivex/c/h;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lio/reactivex/x;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2693
    const-string v0, "subscriber is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2695
    invoke-static {p0, p1}, Lio/reactivex/g/a;->a(Lio/reactivex/v;Lio/reactivex/x;)Lio/reactivex/x;

    move-result-object v0

    .line 2697
    const-string v1, "subscriber returned by the RxJavaPlugins hook is null"

    invoke-static {v0, v1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2700
    :try_start_0
    invoke-virtual {p0, v0}, Lio/reactivex/v;->b(Lio/reactivex/x;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 2709
    return-void

    .line 2701
    :catch_0
    move-exception v0

    .line 2702
    throw v0

    .line 2703
    :catch_1
    move-exception v0

    .line 2704
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 2705
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "subscribeActual failed"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 2706
    invoke-virtual {v1, v0}, Ljava/lang/NullPointerException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 2707
    throw v1
.end method

.method public final b(Lio/reactivex/c/h;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;)",
            "Lio/reactivex/n",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 2113
    invoke-virtual {p0}, Lio/reactivex/v;->f()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/n;->flatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lio/reactivex/c/g;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g",
            "<-",
            "Lio/reactivex/b/b;",
            ">;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1868
    const-string v0, "onSubscribe is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1869
    new-instance v0, Lio/reactivex/d/e/e/h;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/h;-><init>(Lio/reactivex/z;Lio/reactivex/c/g;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lio/reactivex/u;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/u;",
            ")",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2767
    const-string v0, "scheduler is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2768
    new-instance v0, Lio/reactivex/d/e/e/t;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/t;-><init>(Lio/reactivex/z;Lio/reactivex/u;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2345
    const-string v0, "value is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2346
    new-instance v0, Lio/reactivex/d/e/e/r;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lio/reactivex/d/e/e/r;-><init>(Lio/reactivex/z;Lio/reactivex/c/h;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 2152
    new-instance v0, Lio/reactivex/d/d/g;

    invoke-direct {v0}, Lio/reactivex/d/d/g;-><init>()V

    .line 2153
    invoke-virtual {p0, v0}, Lio/reactivex/v;->a(Lio/reactivex/x;)V

    .line 2154
    invoke-virtual {v0}, Lio/reactivex/d/d/g;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(Lio/reactivex/x;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-TT;>;)V"
        }
    .end annotation
.end method

.method public final c()Lio/reactivex/b;
    .locals 1

    .prologue
    .line 2982
    new-instance v0, Lio/reactivex/d/e/a/g;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/a/g;-><init>(Lio/reactivex/z;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lio/reactivex/c/h;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/d;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .prologue
    .line 2136
    const-string v0, "mapper is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2137
    new-instance v0, Lio/reactivex/d/e/e/l;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/l;-><init>(Lio/reactivex/z;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/b;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lio/reactivex/c/g;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g",
            "<-TT;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1886
    const-string v0, "onSuccess is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1887
    new-instance v0, Lio/reactivex/d/e/e/i;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/i;-><init>(Lio/reactivex/z;Lio/reactivex/c/g;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lio/reactivex/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/f",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 3003
    instance-of v0, p0, Lio/reactivex/d/c/a;

    if-eqz v0, :cond_0

    .line 3004
    check-cast p0, Lio/reactivex/d/c/a;

    invoke-interface {p0}, Lio/reactivex/d/c/a;->a()Lio/reactivex/f;

    move-result-object v0

    .line 3006
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/reactivex/d/e/e/u;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/u;-><init>(Lio/reactivex/z;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/f;)Lio/reactivex/f;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(Lio/reactivex/c/g;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g",
            "<-",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1922
    const-string v0, "onError is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1923
    new-instance v0, Lio/reactivex/d/e/e/f;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/f;-><init>(Lio/reactivex/z;Lio/reactivex/c/g;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lio/reactivex/c/h;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-TT;+TR;>;)",
            "Lio/reactivex/v",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 2206
    const-string v0, "mapper is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2207
    new-instance v0, Lio/reactivex/d/e/e/o;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/o;-><init>(Lio/reactivex/z;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lio/reactivex/c/g;)Lio/reactivex/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g",
            "<-TT;>;)",
            "Lio/reactivex/b/b;"
        }
    .end annotation

    .prologue
    .line 2657
    sget-object v0, Lio/reactivex/d/b/a;->f:Lio/reactivex/c/g;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lio/reactivex/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/h",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 3042
    instance-of v0, p0, Lio/reactivex/d/c/b;

    if-eqz v0, :cond_0

    .line 3043
    check-cast p0, Lio/reactivex/d/c/b;

    invoke-interface {p0}, Lio/reactivex/d/c/b;->p_()Lio/reactivex/h;

    move-result-object v0

    .line 3045
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/reactivex/d/e/c/i;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/c/i;-><init>(Lio/reactivex/z;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/h;)Lio/reactivex/h;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(Lio/reactivex/c/h;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h",
            "<",
            "Ljava/lang/Throwable;",
            "+TT;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2328
    const-string v0, "resumeFunction is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2329
    new-instance v0, Lio/reactivex/d/e/e/r;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lio/reactivex/d/e/e/r;-><init>(Lio/reactivex/z;Lio/reactivex/c/h;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 3062
    instance-of v0, p0, Lio/reactivex/d/c/c;

    if-eqz v0, :cond_0

    .line 3063
    check-cast p0, Lio/reactivex/d/c/c;

    invoke-interface {p0}, Lio/reactivex/d/c/c;->q_()Lio/reactivex/n;

    move-result-object v0

    .line 3065
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/reactivex/d/e/e/v;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/e/v;-><init>(Lio/reactivex/z;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    goto :goto_0
.end method

.method public final f(Lio/reactivex/c/h;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h",
            "<-",
            "Ljava/lang/Throwable;",
            "+",
            "Lio/reactivex/z",
            "<+TT;>;>;)",
            "Lio/reactivex/v",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2415
    const-string v0, "resumeFunctionInCaseOfError is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2416
    new-instance v0, Lio/reactivex/d/e/e/s;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/e/s;-><init>(Lio/reactivex/z;Lio/reactivex/c/h;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
