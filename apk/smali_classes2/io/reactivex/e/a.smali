.class public abstract Lio/reactivex/e/a;
.super Lio/reactivex/n;
.source "ConnectableObservable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/n",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lio/reactivex/d/e/d/ch;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/ch;-><init>(Lio/reactivex/e/a;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 103
    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lio/reactivex/e/a;->a(ILio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(ILio/reactivex/c/g;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lio/reactivex/c/g",
            "<-",
            "Lio/reactivex/b/b;",
            ">;)",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 122
    if-gtz p1, :cond_0

    .line 123
    invoke-virtual {p0, p2}, Lio/reactivex/e/a;->a(Lio/reactivex/c/g;)V

    .line 124
    invoke-static {p0}, Lio/reactivex/g/a;->a(Lio/reactivex/e/a;)Lio/reactivex/e/a;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lio/reactivex/d/e/d/k;

    invoke-direct {v0, p0, p1, p2}, Lio/reactivex/d/e/d/k;-><init>(Lio/reactivex/e/a;ILio/reactivex/c/g;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract a(Lio/reactivex/c/g;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/g",
            "<-",
            "Lio/reactivex/b/b;",
            ">;)V"
        }
    .end annotation
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lio/reactivex/e/a;->a(I)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
