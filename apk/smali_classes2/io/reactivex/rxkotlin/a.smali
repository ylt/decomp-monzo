.class public final Lio/reactivex/rxkotlin/a;
.super Ljava/lang/Object;
.source "disposable.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0015\u0010\u0004\u001a\u00020\u0005*\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0001H\u0086\u0002\u00a8\u0006\u0007"
    }
    d2 = {
        "addTo",
        "Lio/reactivex/disposables/Disposable;",
        "compositeDisposable",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "plusAssign",
        "",
        "disposable",
        "rxkotlin_main"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method public static final a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disposable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-virtual {p0, p1}, Lio/reactivex/b/a;->a(Lio/reactivex/b/b;)Z

    .line 11
    return-void
.end method
