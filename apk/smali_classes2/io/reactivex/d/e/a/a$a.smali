.class final Lio/reactivex/d/e/a/a$a;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "CompletableConcatArray.java"

# interfaces
.implements Lio/reactivex/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field final a:Lio/reactivex/c;

.field final b:[Lio/reactivex/d;

.field c:I

.field final d:Lio/reactivex/d/a/k;


# direct methods
.method constructor <init>(Lio/reactivex/c;[Lio/reactivex/d;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 48
    iput-object p1, p0, Lio/reactivex/d/e/a/a$a;->a:Lio/reactivex/c;

    .line 49
    iput-object p2, p0, Lio/reactivex/d/e/a/a$a;->b:[Lio/reactivex/d;

    .line 50
    new-instance v0, Lio/reactivex/d/a/k;

    invoke-direct {v0}, Lio/reactivex/d/a/k;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/a/a$a;->d:Lio/reactivex/d/a/k;

    .line 51
    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lio/reactivex/d/e/a/a$a;->d:Lio/reactivex/d/a/k;

    invoke-virtual {v0}, Lio/reactivex/d/a/k;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-virtual {p0}, Lio/reactivex/d/e/a/a$a;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lio/reactivex/d/e/a/a$a;->b:[Lio/reactivex/d;

    .line 79
    :cond_2
    iget-object v1, p0, Lio/reactivex/d/e/a/a$a;->d:Lio/reactivex/d/a/k;

    invoke-virtual {v1}, Lio/reactivex/d/a/k;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    iget v1, p0, Lio/reactivex/d/e/a/a$a;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lio/reactivex/d/e/a/a$a;->c:I

    .line 84
    array-length v2, v0

    if-ne v1, v2, :cond_3

    .line 85
    iget-object v0, p0, Lio/reactivex/d/e/a/a$a;->a:Lio/reactivex/c;

    invoke-interface {v0}, Lio/reactivex/c;->onComplete()V

    goto :goto_0

    .line 89
    :cond_3
    aget-object v1, v0, v1

    invoke-interface {v1, p0}, Lio/reactivex/d;->a(Lio/reactivex/c;)V

    .line 90
    invoke-virtual {p0}, Lio/reactivex/d/e/a/a$a;->decrementAndGet()I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public onComplete()V
    .locals 0

    .prologue
    .line 65
    invoke-virtual {p0}, Lio/reactivex/d/e/a/a$a;->a()V

    .line 66
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lio/reactivex/d/e/a/a$a;->a:Lio/reactivex/c;

    invoke-interface {v0, p1}, Lio/reactivex/c;->onError(Ljava/lang/Throwable;)V

    .line 61
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lio/reactivex/d/e/a/a$a;->d:Lio/reactivex/d/a/k;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/k;->a(Lio/reactivex/b/b;)Z

    .line 56
    return-void
.end method
