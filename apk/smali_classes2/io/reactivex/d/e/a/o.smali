.class public final Lio/reactivex/d/e/a/o;
.super Lio/reactivex/b;
.source "CompletableSubscribeOn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/a/o$a;
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d;

.field final b:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Lio/reactivex/d;Lio/reactivex/u;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lio/reactivex/b;-><init>()V

    .line 28
    iput-object p1, p0, Lio/reactivex/d/e/a/o;->a:Lio/reactivex/d;

    .line 29
    iput-object p2, p0, Lio/reactivex/d/e/a/o;->b:Lio/reactivex/u;

    .line 30
    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/c;)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lio/reactivex/d/e/a/o$a;

    iget-object v1, p0, Lio/reactivex/d/e/a/o;->a:Lio/reactivex/d;

    invoke-direct {v0, p1, v1}, Lio/reactivex/d/e/a/o$a;-><init>(Lio/reactivex/c;Lio/reactivex/d;)V

    .line 36
    invoke-interface {p1, v0}, Lio/reactivex/c;->onSubscribe(Lio/reactivex/b/b;)V

    .line 38
    iget-object v1, p0, Lio/reactivex/d/e/a/o;->b:Lio/reactivex/u;

    invoke-virtual {v1, v0}, Lio/reactivex/u;->a(Ljava/lang/Runnable;)Lio/reactivex/b/b;

    move-result-object v1

    .line 40
    iget-object v0, v0, Lio/reactivex/d/e/a/o$a;->b:Lio/reactivex/d/a/k;

    invoke-virtual {v0, v1}, Lio/reactivex/d/a/k;->b(Lio/reactivex/b/b;)Z

    .line 42
    return-void
.end method
