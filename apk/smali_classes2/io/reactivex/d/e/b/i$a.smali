.class final Lio/reactivex/d/e/b/i$a;
.super Lio/reactivex/d/i/b;
.source "FlowableSingle.java"

# interfaces
.implements Lio/reactivex/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/b/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/i/b",
        "<TT;>;",
        "Lio/reactivex/g",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field b:Lorg/a/c;

.field c:Z


# direct methods
.method constructor <init>(Lorg/a/b;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/b",
            "<-TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lio/reactivex/d/i/b;-><init>(Lorg/a/b;)V

    .line 49
    iput-object p2, p0, Lio/reactivex/d/e/b/i$a;->a:Ljava/lang/Object;

    .line 50
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lio/reactivex/d/i/b;->a()V

    .line 106
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->b:Lorg/a/c;

    invoke-interface {v0}, Lorg/a/c;->a()V

    .line 107
    return-void
.end method

.method public a(Lorg/a/c;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->b:Lorg/a/c;

    invoke-static {v0, p1}, Lio/reactivex/d/i/d;->a(Lorg/a/c;Lorg/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iput-object p1, p0, Lio/reactivex/d/e/b/i$a;->b:Lorg/a/c;

    .line 56
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->d:Lorg/a/b;

    invoke-interface {v0, p0}, Lorg/a/b;->a(Lorg/a/c;)V

    .line 57
    const-wide v0, 0x7fffffffffffffffL

    invoke-interface {p1, v0, v1}, Lorg/a/c;->a(J)V

    .line 59
    :cond_0
    return-void
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 87
    iget-boolean v0, p0, Lio/reactivex/d/e/b/i$a;->c:Z

    if-eqz v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/b/i$a;->c:Z

    .line 91
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->e:Ljava/lang/Object;

    .line 92
    const/4 v1, 0x0

    iput-object v1, p0, Lio/reactivex/d/e/b/i$a;->e:Ljava/lang/Object;

    .line 93
    if-nez v0, :cond_1

    .line 94
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->a:Ljava/lang/Object;

    .line 96
    :cond_1
    if-nez v0, :cond_2

    .line 97
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->d:Lorg/a/b;

    invoke-interface {v0}, Lorg/a/b;->onComplete()V

    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p0, v0}, Lio/reactivex/d/e/b/i$a;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lio/reactivex/d/e/b/i$a;->c:Z

    if-eqz v0, :cond_0

    .line 78
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/b/i$a;->c:Z

    .line 82
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->d:Lorg/a/b;

    invoke-interface {v0, p1}, Lorg/a/b;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 63
    iget-boolean v0, p0, Lio/reactivex/d/e/b/i$a;->c:Z

    if-eqz v0, :cond_0

    .line 73
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->e:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/b/i$a;->c:Z

    .line 68
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->b:Lorg/a/c;

    invoke-interface {v0}, Lorg/a/c;->a()V

    .line 69
    iget-object v0, p0, Lio/reactivex/d/e/b/i$a;->d:Lorg/a/b;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Sequence contains more than one element!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lorg/a/b;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 72
    :cond_1
    iput-object p1, p0, Lio/reactivex/d/e/b/i$a;->e:Ljava/lang/Object;

    goto :goto_0
.end method
