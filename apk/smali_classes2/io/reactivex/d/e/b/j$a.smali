.class final Lio/reactivex/d/e/b/j$a;
.super Ljava/lang/Object;
.source "FlowableSingleSingle.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/b/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/g",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/x",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field c:Lorg/a/c;

.field d:Z

.field e:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/x;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lio/reactivex/d/e/b/j$a;->a:Lio/reactivex/x;

    .line 62
    iput-object p2, p0, Lio/reactivex/d/e/b/j$a;->b:Ljava/lang/Object;

    .line 63
    return-void
.end method


# virtual methods
.method public a(Lorg/a/c;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    invoke-static {v0, p1}, Lio/reactivex/d/i/d;->a(Lorg/a/c;Lorg/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iput-object p1, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    .line 69
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->a:Lio/reactivex/x;

    invoke-interface {v0, p0}, Lio/reactivex/x;->onSubscribe(Lio/reactivex/b/b;)V

    .line 70
    const-wide v0, 0x7fffffffffffffffL

    invoke-interface {p1, v0, v1}, Lorg/a/c;->a(J)V

    .line 72
    :cond_0
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    invoke-interface {v0}, Lorg/a/c;->a()V

    .line 123
    sget-object v0, Lio/reactivex/d/i/d;->a:Lio/reactivex/d/i/d;

    iput-object v0, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    .line 124
    return-void
.end method

.method public isDisposed()Z
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    sget-object v1, Lio/reactivex/d/i/d;->a:Lio/reactivex/d/i/d;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 102
    iget-boolean v0, p0, Lio/reactivex/d/e/b/j$a;->d:Z

    if-eqz v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/b/j$a;->d:Z

    .line 106
    sget-object v0, Lio/reactivex/d/i/d;->a:Lio/reactivex/d/i/d;

    iput-object v0, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    .line 107
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->e:Ljava/lang/Object;

    .line 108
    const/4 v1, 0x0

    iput-object v1, p0, Lio/reactivex/d/e/b/j$a;->e:Ljava/lang/Object;

    .line 109
    if-nez v0, :cond_1

    .line 110
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->b:Ljava/lang/Object;

    .line 113
    :cond_1
    if-eqz v0, :cond_2

    .line 114
    iget-object v1, p0, Lio/reactivex/d/e/b/j$a;->a:Lio/reactivex/x;

    invoke-interface {v1, v0}, Lio/reactivex/x;->a_(Ljava/lang/Object;)V

    goto :goto_0

    .line 116
    :cond_2
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->a:Lio/reactivex/x;

    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    invoke-interface {v0, v1}, Lio/reactivex/x;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lio/reactivex/d/e/b/j$a;->d:Z

    if-eqz v0, :cond_0

    .line 92
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 98
    :goto_0
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/b/j$a;->d:Z

    .line 96
    sget-object v0, Lio/reactivex/d/i/d;->a:Lio/reactivex/d/i/d;

    iput-object v0, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    .line 97
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->a:Lio/reactivex/x;

    invoke-interface {v0, p1}, Lio/reactivex/x;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 76
    iget-boolean v0, p0, Lio/reactivex/d/e/b/j$a;->d:Z

    if-eqz v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->e:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/b/j$a;->d:Z

    .line 81
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    invoke-interface {v0}, Lorg/a/c;->a()V

    .line 82
    sget-object v0, Lio/reactivex/d/i/d;->a:Lio/reactivex/d/i/d;

    iput-object v0, p0, Lio/reactivex/d/e/b/j$a;->c:Lorg/a/c;

    .line 83
    iget-object v0, p0, Lio/reactivex/d/e/b/j$a;->a:Lio/reactivex/x;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Sequence contains more than one element!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lio/reactivex/x;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 86
    :cond_1
    iput-object p1, p0, Lio/reactivex/d/e/b/j$a;->e:Ljava/lang/Object;

    goto :goto_0
.end method
