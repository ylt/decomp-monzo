.class public final Lio/reactivex/d/e/b/g;
.super Lio/reactivex/d/e/b/a;
.source "FlowableRetryBiPredicate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/b/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/b/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final c:Lio/reactivex/c/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/d",
            "<-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/f;Lio/reactivex/c/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/f",
            "<TT;>;",
            "Lio/reactivex/c/d",
            "<-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lio/reactivex/d/e/b/a;-><init>(Lio/reactivex/f;)V

    .line 31
    iput-object p2, p0, Lio/reactivex/d/e/b/g;->c:Lio/reactivex/c/d;

    .line 32
    return-void
.end method


# virtual methods
.method public b(Lorg/a/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/b",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lio/reactivex/d/i/c;

    invoke-direct {v0}, Lio/reactivex/d/i/c;-><init>()V

    .line 37
    invoke-interface {p1, v0}, Lorg/a/b;->a(Lorg/a/c;)V

    .line 39
    new-instance v1, Lio/reactivex/d/e/b/g$a;

    iget-object v2, p0, Lio/reactivex/d/e/b/g;->c:Lio/reactivex/c/d;

    iget-object v3, p0, Lio/reactivex/d/e/b/g;->b:Lio/reactivex/f;

    invoke-direct {v1, p1, v2, v0, v3}, Lio/reactivex/d/e/b/g$a;-><init>(Lorg/a/b;Lio/reactivex/c/d;Lio/reactivex/d/i/c;Lorg/a/a;)V

    .line 40
    invoke-virtual {v1}, Lio/reactivex/d/e/b/g$a;->a()V

    .line 41
    return-void
.end method
