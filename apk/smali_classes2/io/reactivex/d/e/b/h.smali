.class public final Lio/reactivex/d/e/b/h;
.super Lio/reactivex/d/e/b/a;
.source "FlowableRetryPredicate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/b/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/b/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final c:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field final d:J


# direct methods
.method public constructor <init>(Lio/reactivex/f;JLio/reactivex/c/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/f",
            "<TT;>;J",
            "Lio/reactivex/c/q",
            "<-",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lio/reactivex/d/e/b/a;-><init>(Lio/reactivex/f;)V

    .line 32
    iput-object p4, p0, Lio/reactivex/d/e/b/h;->c:Lio/reactivex/c/q;

    .line 33
    iput-wide p2, p0, Lio/reactivex/d/e/b/h;->d:J

    .line 34
    return-void
.end method


# virtual methods
.method public b(Lorg/a/b;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/b",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v5, Lio/reactivex/d/i/c;

    invoke-direct {v5}, Lio/reactivex/d/i/c;-><init>()V

    .line 39
    invoke-interface {p1, v5}, Lorg/a/b;->a(Lorg/a/c;)V

    .line 41
    new-instance v0, Lio/reactivex/d/e/b/h$a;

    iget-wide v2, p0, Lio/reactivex/d/e/b/h;->d:J

    iget-object v4, p0, Lio/reactivex/d/e/b/h;->c:Lio/reactivex/c/q;

    iget-object v6, p0, Lio/reactivex/d/e/b/h;->b:Lio/reactivex/f;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/d/e/b/h$a;-><init>(Lorg/a/b;JLio/reactivex/c/q;Lio/reactivex/d/i/c;Lorg/a/a;)V

    .line 42
    invoke-virtual {v0}, Lio/reactivex/d/e/b/h$a;->a()V

    .line 43
    return-void
.end method
