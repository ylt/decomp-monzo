.class public final Lio/reactivex/d/e/b/c;
.super Lio/reactivex/d/e/b/a;
.source "FlowableOnBackpressureBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/b/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/b/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final c:I

.field final d:Z

.field final e:Z

.field final f:Lio/reactivex/c/a;


# direct methods
.method public constructor <init>(Lio/reactivex/f;IZZLio/reactivex/c/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/f",
            "<TT;>;IZZ",
            "Lio/reactivex/c/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lio/reactivex/d/e/b/a;-><init>(Lio/reactivex/f;)V

    .line 38
    iput p2, p0, Lio/reactivex/d/e/b/c;->c:I

    .line 39
    iput-boolean p3, p0, Lio/reactivex/d/e/b/c;->d:Z

    .line 40
    iput-boolean p4, p0, Lio/reactivex/d/e/b/c;->e:Z

    .line 41
    iput-object p5, p0, Lio/reactivex/d/e/b/c;->f:Lio/reactivex/c/a;

    .line 42
    return-void
.end method


# virtual methods
.method protected b(Lorg/a/b;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/a/b",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v6, p0, Lio/reactivex/d/e/b/c;->b:Lio/reactivex/f;

    new-instance v0, Lio/reactivex/d/e/b/c$a;

    iget v2, p0, Lio/reactivex/d/e/b/c;->c:I

    iget-boolean v3, p0, Lio/reactivex/d/e/b/c;->d:Z

    iget-boolean v4, p0, Lio/reactivex/d/e/b/c;->e:Z

    iget-object v5, p0, Lio/reactivex/d/e/b/c;->f:Lio/reactivex/c/a;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/b/c$a;-><init>(Lorg/a/b;IZZLio/reactivex/c/a;)V

    invoke-virtual {v6, v0}, Lio/reactivex/f;->a(Lio/reactivex/g;)V

    .line 47
    return-void
.end method
