.class final Lio/reactivex/d/e/d/bn$a;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableIntervalRange.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/bn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final b:J

.field c:J


# direct methods
.method constructor <init>(Lio/reactivex/t;JJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Long;",
            ">;JJ)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 72
    iput-object p1, p0, Lio/reactivex/d/e/d/bn$a;->a:Lio/reactivex/t;

    .line 73
    iput-wide p2, p0, Lio/reactivex/d/e/d/bn$a;->c:J

    .line 74
    iput-wide p4, p0, Lio/reactivex/d/e/d/bn$a;->b:J

    .line 75
    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 105
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 106
    return-void
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 79
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 80
    return-void
.end method

.method public isDisposed()Z
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lio/reactivex/d/e/d/bn$a;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 89
    invoke-virtual {p0}, Lio/reactivex/d/e/d/bn$a;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    iget-wide v0, p0, Lio/reactivex/d/e/d/bn$a;->c:J

    .line 91
    iget-object v2, p0, Lio/reactivex/d/e/d/bn$a;->a:Lio/reactivex/t;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 93
    iget-wide v2, p0, Lio/reactivex/d/e/d/bn$a;->b:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 94
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 95
    iget-object v0, p0, Lio/reactivex/d/e/d/bn$a;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lio/reactivex/d/e/d/bn$a;->c:J

    goto :goto_0
.end method
