.class public final Lio/reactivex/d/e/d/bk;
.super Lio/reactivex/b;
.source "ObservableIgnoreElementsCompletable.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/bk$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/b;",
        "Lio/reactivex/d/c/c",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Lio/reactivex/b;-><init>()V

    .line 26
    iput-object p1, p0, Lio/reactivex/d/e/d/bk;->a:Lio/reactivex/r;

    .line 27
    return-void
.end method


# virtual methods
.method public b(Lio/reactivex/c;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lio/reactivex/d/e/d/bk;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/bk$a;

    invoke-direct {v1, p1}, Lio/reactivex/d/e/d/bk$a;-><init>(Lio/reactivex/c;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 32
    return-void
.end method

.method public q_()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lio/reactivex/d/e/d/bj;

    iget-object v1, p0, Lio/reactivex/d/e/d/bk;->a:Lio/reactivex/r;

    invoke-direct {v0, v1}, Lio/reactivex/d/e/d/bj;-><init>(Lio/reactivex/r;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
