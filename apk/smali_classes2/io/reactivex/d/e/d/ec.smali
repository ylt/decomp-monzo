.class public final Lio/reactivex/d/e/d/ec;
.super Lio/reactivex/d/e/d/a;
.source "ObservableWithLatestFrom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/ec$a;,
        Lio/reactivex/d/e/d/ec$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TR;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/c",
            "<-TT;-TU;+TR;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TU;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/c;Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/c",
            "<-TT;-TU;+TR;>;",
            "Lio/reactivex/r",
            "<+TU;>;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 32
    iput-object p2, p0, Lio/reactivex/d/e/d/ec;->b:Lio/reactivex/c/c;

    .line 33
    iput-object p3, p0, Lio/reactivex/d/e/d/ec;->c:Lio/reactivex/r;

    .line 34
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TR;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lio/reactivex/f/e;

    invoke-direct {v0, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    .line 39
    new-instance v1, Lio/reactivex/d/e/d/ec$b;

    iget-object v2, p0, Lio/reactivex/d/e/d/ec;->b:Lio/reactivex/c/c;

    invoke-direct {v1, v0, v2}, Lio/reactivex/d/e/d/ec$b;-><init>(Lio/reactivex/t;Lio/reactivex/c/c;)V

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/f/e;->onSubscribe(Lio/reactivex/b/b;)V

    .line 43
    iget-object v0, p0, Lio/reactivex/d/e/d/ec;->c:Lio/reactivex/r;

    new-instance v2, Lio/reactivex/d/e/d/ec$a;

    invoke-direct {v2, p0, v1}, Lio/reactivex/d/e/d/ec$a;-><init>(Lio/reactivex/d/e/d/ec;Lio/reactivex/d/e/d/ec$b;)V

    invoke-interface {v0, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 45
    iget-object v0, p0, Lio/reactivex/d/e/d/ec;->a:Lio/reactivex/r;

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 46
    return-void
.end method
