.class final Lio/reactivex/d/e/d/v$b;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableConcatMap.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/v$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TU;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/d/a/k;

.field final c:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TU;>;>;"
        }
    .end annotation
.end field

.field final d:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<TU;>;"
        }
    .end annotation
.end field

.field final e:I

.field f:Lio/reactivex/d/c/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/c/i",
            "<TT;>;"
        }
    .end annotation
.end field

.field g:Lio/reactivex/b/b;

.field volatile h:Z

.field volatile i:Z

.field volatile j:Z

.field k:I


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/c/h;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TU;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TU;>;>;I)V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 81
    iput-object p1, p0, Lio/reactivex/d/e/d/v$b;->a:Lio/reactivex/t;

    .line 82
    iput-object p2, p0, Lio/reactivex/d/e/d/v$b;->c:Lio/reactivex/c/h;

    .line 83
    iput p3, p0, Lio/reactivex/d/e/d/v$b;->e:I

    .line 84
    new-instance v0, Lio/reactivex/d/e/d/v$b$a;

    invoke-direct {v0, p1, p0}, Lio/reactivex/d/e/d/v$b$a;-><init>(Lio/reactivex/t;Lio/reactivex/d/e/d/v$b;)V

    iput-object v0, p0, Lio/reactivex/d/e/d/v$b;->d:Lio/reactivex/t;

    .line 85
    new-instance v0, Lio/reactivex/d/a/k;

    invoke-direct {v0}, Lio/reactivex/d/a/k;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/v$b;->b:Lio/reactivex/d/a/k;

    .line 86
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->h:Z

    .line 153
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->b()V

    .line 154
    return-void
.end method

.method a(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->b:Lio/reactivex/d/a/k;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/k;->a(Lio/reactivex/b/b;)Z

    .line 174
    return-void
.end method

.method b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 177
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->getAndIncrement()I

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->i:Z

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    invoke-interface {v0}, Lio/reactivex/d/c/i;->c()V

    goto :goto_0

    .line 186
    :cond_1
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->h:Z

    if-nez v0, :cond_4

    .line 188
    iget-boolean v2, p0, Lio/reactivex/d/e/d/v$b;->j:Z

    .line 193
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    invoke-interface {v0}, Lio/reactivex/d/c/i;->n_()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 202
    if-nez v3, :cond_2

    move v0, v1

    .line 204
    :goto_1
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 205
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$b;->i:Z

    .line 206
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    .line 195
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 196
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->dispose()V

    .line 197
    iget-object v1, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    invoke-interface {v1}, Lio/reactivex/d/c/i;->c()V

    .line 198
    iget-object v1, p0, Lio/reactivex/d/e/d/v$b;->a:Lio/reactivex/t;

    invoke-interface {v1, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 202
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 210
    :cond_3
    if-nez v0, :cond_4

    .line 214
    :try_start_1
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->c:Lio/reactivex/c/h;

    invoke-interface {v0, v3}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "The mapper returned a null ObservableSource"

    invoke-static {v0, v2}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 223
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$b;->h:Z

    .line 224
    iget-object v2, p0, Lio/reactivex/d/e/d/v$b;->d:Lio/reactivex/t;

    invoke-interface {v0, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 228
    :cond_4
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 215
    :catch_1
    move-exception v0

    .line 216
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 217
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->dispose()V

    .line 218
    iget-object v1, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    invoke-interface {v1}, Lio/reactivex/d/c/i;->c()V

    .line 219
    iget-object v1, p0, Lio/reactivex/d/e/d/v$b;->a:Lio/reactivex/t;

    invoke-interface {v1, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->i:Z

    .line 164
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->b:Lio/reactivex/d/a/k;

    invoke-virtual {v0}, Lio/reactivex/d/a/k;->dispose()V

    .line 165
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->g:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 167
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 168
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    invoke-interface {v0}, Lio/reactivex/d/c/i;->c()V

    .line 170
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->i:Z

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->j:Z

    if-eqz v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->j:Z

    .line 148
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->b()V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->j:Z

    if-eqz v0, :cond_0

    .line 135
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 141
    :goto_0
    return-void

    .line 138
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->j:Z

    .line 139
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->dispose()V

    .line 140
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 124
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$b;->j:Z

    if-eqz v0, :cond_0

    .line 131
    :goto_0
    return-void

    .line 127
    :cond_0
    iget v0, p0, Lio/reactivex/d/e/d/v$b;->k:I

    if-nez v0, :cond_1

    .line 128
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    invoke-interface {v0, p1}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    .line 130
    :cond_1
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->b()V

    goto :goto_0
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 89
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->g:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iput-object p1, p0, Lio/reactivex/d/e/d/v$b;->g:Lio/reactivex/b/b;

    .line 91
    instance-of v0, p1, Lio/reactivex/d/c/d;

    if-eqz v0, :cond_2

    .line 93
    check-cast p1, Lio/reactivex/d/c/d;

    .line 95
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Lio/reactivex/d/c/d;->a(I)I

    move-result v0

    .line 96
    if-ne v0, v1, :cond_1

    .line 97
    iput v0, p0, Lio/reactivex/d/e/d/v$b;->k:I

    .line 98
    iput-object p1, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    .line 99
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$b;->j:Z

    .line 101
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 103
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$b;->b()V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 108
    iput v0, p0, Lio/reactivex/d/e/d/v$b;->k:I

    .line 109
    iput-object p1, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    .line 111
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    goto :goto_0

    .line 117
    :cond_2
    new-instance v0, Lio/reactivex/d/f/c;

    iget v1, p0, Lio/reactivex/d/e/d/v$b;->e:I

    invoke-direct {v0, v1}, Lio/reactivex/d/f/c;-><init>(I)V

    iput-object v0, p0, Lio/reactivex/d/e/d/v$b;->f:Lio/reactivex/d/c/i;

    .line 119
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    goto :goto_0
.end method
