.class final Lio/reactivex/d/e/d/v$a$a;
.super Ljava/lang/Object;
.source "ObservableConcatMap.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/v$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/t",
        "<TR;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TR;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/d/e/d/v$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/d/v$a",
            "<*TR;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/d/e/d/v$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TR;>;",
            "Lio/reactivex/d/e/d/v$a",
            "<*TR;>;)V"
        }
    .end annotation

    .prologue
    .line 488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 489
    iput-object p1, p0, Lio/reactivex/d/e/d/v$a$a;->a:Lio/reactivex/t;

    .line 490
    iput-object p2, p0, Lio/reactivex/d/e/d/v$a$a;->b:Lio/reactivex/d/e/d/v$a;

    .line 491
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a$a;->b:Lio/reactivex/d/e/d/v$a;

    .line 520
    const/4 v1, 0x0

    iput-boolean v1, v0, Lio/reactivex/d/e/d/v$a;->j:Z

    .line 521
    invoke-virtual {v0}, Lio/reactivex/d/e/d/v$a;->a()V

    .line 522
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 505
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a$a;->b:Lio/reactivex/d/e/d/v$a;

    .line 506
    iget-object v1, v0, Lio/reactivex/d/e/d/v$a;->d:Lio/reactivex/d/j/c;

    invoke-virtual {v1, p1}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 507
    iget-boolean v1, v0, Lio/reactivex/d/e/d/v$a;->g:Z

    if-nez v1, :cond_0

    .line 508
    iget-object v1, v0, Lio/reactivex/d/e/d/v$a;->i:Lio/reactivex/b/b;

    invoke-interface {v1}, Lio/reactivex/b/b;->dispose()V

    .line 510
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, Lio/reactivex/d/e/d/v$a;->j:Z

    .line 511
    invoke-virtual {v0}, Lio/reactivex/d/e/d/v$a;->a()V

    .line 515
    :goto_0
    return-void

    .line 513
    :cond_1
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 500
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 501
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a$a;->b:Lio/reactivex/d/e/d/v$a;

    iget-object v0, v0, Lio/reactivex/d/e/d/v$a;->f:Lio/reactivex/d/a/k;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/k;->b(Lio/reactivex/b/b;)Z

    .line 496
    return-void
.end method
