.class final Lio/reactivex/d/e/d/bl$l;
.super Ljava/lang/Object;
.source "ObservableInternalHelper.java"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/bl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "l"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Lio/reactivex/n",
        "<TT;>;",
        "Lio/reactivex/r",
        "<TR;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;+",
            "Lio/reactivex/r",
            "<TR;>;>;"
        }
    .end annotation
.end field

.field private final b:Lio/reactivex/u;


# direct methods
.method constructor <init>(Lio/reactivex/c/h;Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;+",
            "Lio/reactivex/r",
            "<TR;>;>;",
            "Lio/reactivex/u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    iput-object p1, p0, Lio/reactivex/d/e/d/bl$l;->a:Lio/reactivex/c/h;

    .line 402
    iput-object p2, p0, Lio/reactivex/d/e/d/bl$l;->b:Lio/reactivex/u;

    .line 403
    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/n;)Lio/reactivex/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/n",
            "<TT;>;)",
            "Lio/reactivex/r",
            "<TR;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 407
    iget-object v0, p0, Lio/reactivex/d/e/d/bl$l;->a:Lio/reactivex/c/h;

    invoke-interface {v0, p1}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "The selector returned a null ObservableSource"

    invoke-static {v0, v1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    .line 408
    invoke-static {v0}, Lio/reactivex/n;->wrap(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lio/reactivex/d/e/d/bl$l;->b:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 396
    check-cast p1, Lio/reactivex/n;

    invoke-virtual {p0, p1}, Lio/reactivex/d/e/d/bl$l;->a(Lio/reactivex/n;)Lio/reactivex/r;

    move-result-object v0

    return-object v0
.end method
