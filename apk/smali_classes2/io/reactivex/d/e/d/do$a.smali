.class final Lio/reactivex/d/e/d/do$a;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableThrottleFirstTimed.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/do;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/u$c;

.field e:Lio/reactivex/b/b;

.field volatile f:Z

.field g:Z


# direct methods
.method constructor <init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u$c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 63
    iput-object p1, p0, Lio/reactivex/d/e/d/do$a;->a:Lio/reactivex/t;

    .line 64
    iput-wide p2, p0, Lio/reactivex/d/e/d/do$a;->b:J

    .line 65
    iput-object p4, p0, Lio/reactivex/d/e/d/do$a;->c:Ljava/util/concurrent/TimeUnit;

    .line 66
    iput-object p5, p0, Lio/reactivex/d/e/d/do$a;->d:Lio/reactivex/u$c;

    .line 67
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->e:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 122
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->d:Lio/reactivex/u$c;

    invoke-virtual {v0}, Lio/reactivex/u$c;->dispose()V

    .line 123
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->d:Lio/reactivex/u$c;

    invoke-virtual {v0}, Lio/reactivex/u$c;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lio/reactivex/d/e/d/do$a;->g:Z

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/do$a;->g:Z

    .line 114
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 115
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->d:Lio/reactivex/u$c;

    invoke-virtual {v0}, Lio/reactivex/u$c;->dispose()V

    .line 117
    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lio/reactivex/d/e/d/do$a;->g:Z

    if-eqz v0, :cond_0

    .line 102
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 108
    :goto_0
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/do$a;->g:Z

    .line 105
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 106
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->d:Lio/reactivex/u$c;

    invoke-virtual {v0}, Lio/reactivex/u$c;->dispose()V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 79
    iget-boolean v0, p0, Lio/reactivex/d/e/d/do$a;->f:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lio/reactivex/d/e/d/do$a;->g:Z

    if-nez v0, :cond_1

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/do$a;->f:Z

    .line 82
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 84
    invoke-virtual {p0}, Lio/reactivex/d/e/d/do$a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    .line 85
    if-eqz v0, :cond_0

    .line 86
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 88
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->d:Lio/reactivex/u$c;

    iget-wide v2, p0, Lio/reactivex/d/e/d/do$a;->b:J

    iget-object v1, p0, Lio/reactivex/d/e/d/do$a;->c:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, v2, v3, v1}, Lio/reactivex/u$c;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/b;

    move-result-object v0

    invoke-static {p0, v0}, Lio/reactivex/d/a/d;->c(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 92
    :cond_1
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->e:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iput-object p1, p0, Lio/reactivex/d/e/d/do$a;->e:Lio/reactivex/b/b;

    .line 73
    iget-object v0, p0, Lio/reactivex/d/e/d/do$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 75
    :cond_0
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/reactivex/d/e/d/do$a;->f:Z

    .line 97
    return-void
.end method
