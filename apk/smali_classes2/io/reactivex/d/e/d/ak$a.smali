.class final Lio/reactivex/d/e/d/ak$a;
.super Lio/reactivex/d/d/b;
.source "ObservableDoFinally.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/ak;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/d/b",
        "<TT;>;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/a;

.field c:Lio/reactivex/b/b;

.field d:Lio/reactivex/d/c/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/c/d",
            "<TT;>;"
        }
    .end annotation
.end field

.field e:Z


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/c/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;",
            "Lio/reactivex/c/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Lio/reactivex/d/d/b;-><init>()V

    .line 63
    iput-object p1, p0, Lio/reactivex/d/e/d/ak$a;->a:Lio/reactivex/t;

    .line 64
    iput-object p2, p0, Lio/reactivex/d/e/d/ak$a;->b:Lio/reactivex/c/a;

    .line 65
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 110
    iget-object v2, p0, Lio/reactivex/d/e/d/ak$a;->d:Lio/reactivex/d/c/d;

    .line 111
    if-eqz v2, :cond_1

    and-int/lit8 v3, p1, 0x4

    if-nez v3, :cond_1

    .line 112
    invoke-interface {v2, p1}, Lio/reactivex/d/c/d;->a(I)I

    move-result v2

    .line 113
    if-eqz v2, :cond_0

    .line 114
    if-ne v2, v0, :cond_2

    :goto_0
    iput-boolean v0, p0, Lio/reactivex/d/e/d/ak$a;->e:Z

    :cond_0
    move v1, v2

    .line 118
    :cond_1
    return v1

    :cond_2
    move v0, v1

    .line 114
    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->d:Lio/reactivex/d/c/d;

    invoke-interface {v0}, Lio/reactivex/d/c/d;->b()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->d:Lio/reactivex/d/c/d;

    invoke-interface {v0}, Lio/reactivex/d/c/d;->c()V

    .line 124
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    .line 142
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/d/ak$a;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->b:Lio/reactivex/c/a;

    invoke-interface {v0}, Lio/reactivex/c/a;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 145
    :catch_0
    move-exception v0

    .line 146
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 147
    invoke-static {v0}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 100
    invoke-virtual {p0}, Lio/reactivex/d/e/d/ak$a;->d()V

    .line 101
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public n_()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->d:Lio/reactivex/d/c/d;

    invoke-interface {v0}, Lio/reactivex/d/c/d;->n_()Ljava/lang/Object;

    move-result-object v0

    .line 135
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lio/reactivex/d/e/d/ak$a;->e:Z

    if-eqz v1, :cond_0

    .line 136
    invoke-virtual {p0}, Lio/reactivex/d/e/d/ak$a;->d()V

    .line 138
    :cond_0
    return-object v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 94
    invoke-virtual {p0}, Lio/reactivex/d/e/d/ak$a;->d()V

    .line 95
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 88
    invoke-virtual {p0}, Lio/reactivex/d/e/d/ak$a;->d()V

    .line 89
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->c:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    iput-object p1, p0, Lio/reactivex/d/e/d/ak$a;->c:Lio/reactivex/b/b;

    .line 72
    instance-of v0, p1, Lio/reactivex/d/c/d;

    if-eqz v0, :cond_0

    .line 73
    check-cast p1, Lio/reactivex/d/c/d;

    iput-object p1, p0, Lio/reactivex/d/e/d/ak$a;->d:Lio/reactivex/d/c/d;

    .line 76
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/ak$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 78
    :cond_1
    return-void
.end method
