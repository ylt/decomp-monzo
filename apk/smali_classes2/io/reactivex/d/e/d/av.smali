.class public final Lio/reactivex/d/e/d/av;
.super Lio/reactivex/b;
.source "ObservableFlatMapCompletableCompletable.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/av$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/b;",
        "Lio/reactivex/d/c/c",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/d;",
            ">;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/h;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/d;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lio/reactivex/b;-><init>()V

    .line 42
    iput-object p1, p0, Lio/reactivex/d/e/d/av;->a:Lio/reactivex/r;

    .line 43
    iput-object p2, p0, Lio/reactivex/d/e/d/av;->b:Lio/reactivex/c/h;

    .line 44
    iput-boolean p3, p0, Lio/reactivex/d/e/d/av;->c:Z

    .line 45
    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/c;)V
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lio/reactivex/d/e/d/av;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/av$a;

    iget-object v2, p0, Lio/reactivex/d/e/d/av;->b:Lio/reactivex/c/h;

    iget-boolean v3, p0, Lio/reactivex/d/e/d/av;->c:Z

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/d/av$a;-><init>(Lio/reactivex/c;Lio/reactivex/c/h;Z)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 50
    return-void
.end method

.method public q_()Lio/reactivex/n;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lio/reactivex/d/e/d/au;

    iget-object v1, p0, Lio/reactivex/d/e/d/av;->a:Lio/reactivex/r;

    iget-object v2, p0, Lio/reactivex/d/e/d/av;->b:Lio/reactivex/c/h;

    iget-boolean v3, p0, Lio/reactivex/d/e/d/av;->c:Z

    invoke-direct {v0, v1, v2, v3}, Lio/reactivex/d/e/d/au;-><init>(Lio/reactivex/r;Lio/reactivex/c/h;Z)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
