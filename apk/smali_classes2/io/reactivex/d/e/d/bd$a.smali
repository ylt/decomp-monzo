.class final Lio/reactivex/d/e/d/bd$a;
.super Ljava/lang/Object;
.source "ObservableFromPublisher.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/bd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/g",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field b:Lorg/a/c;


# direct methods
.method constructor <init>(Lio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lio/reactivex/d/e/d/bd$a;->a:Lio/reactivex/t;

    .line 42
    return-void
.end method


# virtual methods
.method public a(Lorg/a/c;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lio/reactivex/d/e/d/bd$a;->b:Lorg/a/c;

    invoke-static {v0, p1}, Lio/reactivex/d/i/d;->a(Lorg/a/c;Lorg/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iput-object p1, p0, Lio/reactivex/d/e/d/bd$a;->b:Lorg/a/c;

    .line 63
    iget-object v0, p0, Lio/reactivex/d/e/d/bd$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 64
    const-wide v0, 0x7fffffffffffffffL

    invoke-interface {p1, v0, v1}, Lorg/a/c;->a(J)V

    .line 66
    :cond_0
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lio/reactivex/d/e/d/bd$a;->b:Lorg/a/c;

    invoke-interface {v0}, Lorg/a/c;->a()V

    .line 71
    sget-object v0, Lio/reactivex/d/i/d;->a:Lio/reactivex/d/i/d;

    iput-object v0, p0, Lio/reactivex/d/e/d/bd$a;->b:Lorg/a/c;

    .line 72
    return-void
.end method

.method public isDisposed()Z
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lio/reactivex/d/e/d/bd$a;->b:Lorg/a/c;

    sget-object v1, Lio/reactivex/d/i/d;->a:Lio/reactivex/d/i/d;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lio/reactivex/d/e/d/bd$a;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 47
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lio/reactivex/d/e/d/bd$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 52
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lio/reactivex/d/e/d/bd$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 57
    return-void
.end method
