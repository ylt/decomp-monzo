.class final Lio/reactivex/d/e/d/df$a;
.super Ljava/lang/Object;
.source "ObservableSwitchIfEmpty.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/df;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/d/a/k;

.field d:Z


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lio/reactivex/d/e/d/df$a;->a:Lio/reactivex/t;

    .line 43
    iput-object p2, p0, Lio/reactivex/d/e/d/df$a;->b:Lio/reactivex/r;

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/df$a;->d:Z

    .line 45
    new-instance v0, Lio/reactivex/d/a/k;

    invoke-direct {v0}, Lio/reactivex/d/a/k;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/df$a;->c:Lio/reactivex/d/a/k;

    .line 46
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lio/reactivex/d/e/d/df$a;->d:Z

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/reactivex/d/e/d/df$a;->d:Z

    .line 70
    iget-object v0, p0, Lio/reactivex/d/e/d/df$a;->b:Lio/reactivex/r;

    invoke-interface {v0, p0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/df$a;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lio/reactivex/d/e/d/df$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 64
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 55
    iget-boolean v0, p0, Lio/reactivex/d/e/d/df$a;->d:Z

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/reactivex/d/e/d/df$a;->d:Z

    .line 58
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/df$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 59
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lio/reactivex/d/e/d/df$a;->c:Lio/reactivex/d/a/k;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/k;->a(Lio/reactivex/b/b;)Z

    .line 51
    return-void
.end method
