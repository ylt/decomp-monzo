.class public final Lio/reactivex/d/e/d/bx;
.super Lio/reactivex/d/e/d/a;
.source "ObservableObserveOn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/bx$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/u;

.field final c:Z

.field final d:I


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/u;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/u;",
            "ZI)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 33
    iput-object p2, p0, Lio/reactivex/d/e/d/bx;->b:Lio/reactivex/u;

    .line 34
    iput-boolean p3, p0, Lio/reactivex/d/e/d/bx;->c:Z

    .line 35
    iput p4, p0, Lio/reactivex/d/e/d/bx;->d:I

    .line 36
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lio/reactivex/d/e/d/bx;->b:Lio/reactivex/u;

    instance-of v0, v0, Lio/reactivex/d/g/n;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lio/reactivex/d/e/d/bx;->a:Lio/reactivex/r;

    invoke-interface {v0, p1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 47
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/bx;->b:Lio/reactivex/u;

    invoke-virtual {v0}, Lio/reactivex/u;->a()Lio/reactivex/u$c;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lio/reactivex/d/e/d/bx;->a:Lio/reactivex/r;

    new-instance v2, Lio/reactivex/d/e/d/bx$a;

    iget-boolean v3, p0, Lio/reactivex/d/e/d/bx;->c:Z

    iget v4, p0, Lio/reactivex/d/e/d/bx;->d:I

    invoke-direct {v2, p1, v0, v3, v4}, Lio/reactivex/d/e/d/bx$a;-><init>(Lio/reactivex/t;Lio/reactivex/u$c;ZI)V

    invoke-interface {v1, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method
