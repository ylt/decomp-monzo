.class final Lio/reactivex/d/e/d/ae$a;
.super Ljava/lang/Object;
.source "ObservableDelaySubscriptionOther.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/ae$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/t",
        "<TU;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/a/k;

.field final b:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field c:Z

.field final synthetic d:Lio/reactivex/d/e/d/ae;


# direct methods
.method constructor <init>(Lio/reactivex/d/e/d/ae;Lio/reactivex/d/a/k;Lio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/a/k;",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    iput-object p1, p0, Lio/reactivex/d/e/d/ae$a;->d:Lio/reactivex/d/e/d/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p2, p0, Lio/reactivex/d/e/d/ae$a;->a:Lio/reactivex/d/a/k;

    .line 53
    iput-object p3, p0, Lio/reactivex/d/e/d/ae$a;->b:Lio/reactivex/t;

    .line 54
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 2

    .prologue
    .line 78
    iget-boolean v0, p0, Lio/reactivex/d/e/d/ae$a;->c:Z

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/ae$a;->c:Z

    .line 83
    iget-object v0, p0, Lio/reactivex/d/e/d/ae$a;->d:Lio/reactivex/d/e/d/ae;

    iget-object v0, v0, Lio/reactivex/d/e/d/ae;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/ae$a$a;

    invoke-direct {v1, p0}, Lio/reactivex/d/e/d/ae$a$a;-><init>(Lio/reactivex/d/e/d/ae$a;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lio/reactivex/d/e/d/ae$a;->c:Z

    if-eqz v0, :cond_0

    .line 69
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/ae$a;->c:Z

    .line 73
    iget-object v0, p0, Lio/reactivex/d/e/d/ae$a;->b:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TU;)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0}, Lio/reactivex/d/e/d/ae$a;->onComplete()V

    .line 64
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lio/reactivex/d/e/d/ae$a;->a:Lio/reactivex/d/a/k;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/k;->a(Lio/reactivex/b/b;)Z

    .line 59
    return-void
.end method
