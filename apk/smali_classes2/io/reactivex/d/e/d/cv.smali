.class public final Lio/reactivex/d/e/d/cv;
.super Lio/reactivex/v;
.source "ObservableSequenceEqualSingle.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/cv$b;,
        Lio/reactivex/d/e/d/cv$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/v",
        "<",
        "Ljava/lang/Boolean;",
        ">;",
        "Lio/reactivex/d/c/c",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/c/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/d",
            "<-TT;-TT;>;"
        }
    .end annotation
.end field

.field final d:I


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/d;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<+TT;>;",
            "Lio/reactivex/r",
            "<+TT;>;",
            "Lio/reactivex/c/d",
            "<-TT;-TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Lio/reactivex/v;-><init>()V

    .line 35
    iput-object p1, p0, Lio/reactivex/d/e/d/cv;->a:Lio/reactivex/r;

    .line 36
    iput-object p2, p0, Lio/reactivex/d/e/d/cv;->b:Lio/reactivex/r;

    .line 37
    iput-object p3, p0, Lio/reactivex/d/e/d/cv;->c:Lio/reactivex/c/d;

    .line 38
    iput p4, p0, Lio/reactivex/d/e/d/cv;->d:I

    .line 39
    return-void
.end method


# virtual methods
.method public b(Lio/reactivex/x;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Lio/reactivex/d/e/d/cv$a;

    iget v2, p0, Lio/reactivex/d/e/d/cv;->d:I

    iget-object v3, p0, Lio/reactivex/d/e/d/cv;->a:Lio/reactivex/r;

    iget-object v4, p0, Lio/reactivex/d/e/d/cv;->b:Lio/reactivex/r;

    iget-object v5, p0, Lio/reactivex/d/e/d/cv;->c:Lio/reactivex/c/d;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/cv$a;-><init>(Lio/reactivex/x;ILio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/d;)V

    .line 44
    invoke-interface {p1, v0}, Lio/reactivex/x;->onSubscribe(Lio/reactivex/b/b;)V

    .line 45
    invoke-virtual {v0}, Lio/reactivex/d/e/d/cv$a;->a()V

    .line 46
    return-void
.end method

.method public q_()Lio/reactivex/n;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lio/reactivex/d/e/d/cu;

    iget-object v1, p0, Lio/reactivex/d/e/d/cv;->a:Lio/reactivex/r;

    iget-object v2, p0, Lio/reactivex/d/e/d/cv;->b:Lio/reactivex/r;

    iget-object v3, p0, Lio/reactivex/d/e/d/cv;->c:Lio/reactivex/c/d;

    iget v4, p0, Lio/reactivex/d/e/d/cv;->d:I

    invoke-direct {v0, v1, v2, v3, v4}, Lio/reactivex/d/e/d/cu;-><init>(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/d;I)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
