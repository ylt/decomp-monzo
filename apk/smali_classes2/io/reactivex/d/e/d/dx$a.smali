.class final Lio/reactivex/d/e/d/dx$a;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableWindow.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/dx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:I

.field d:J

.field e:Lio/reactivex/b/b;

.field f:Lio/reactivex/i/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/i/d",
            "<TT;>;"
        }
    .end annotation
.end field

.field volatile g:Z


# direct methods
.method constructor <init>(Lio/reactivex/t;JI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;>;JI)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 63
    iput-object p1, p0, Lio/reactivex/d/e/d/dx$a;->a:Lio/reactivex/t;

    .line 64
    iput-wide p2, p0, Lio/reactivex/d/e/d/dx$a;->b:J

    .line 65
    iput p4, p0, Lio/reactivex/d/e/d/dx$a;->c:I

    .line 66
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/dx$a;->g:Z

    .line 122
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dx$a;->g:Z

    return v0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->f:Lio/reactivex/i/d;

    .line 112
    if-eqz v0, :cond_0

    .line 113
    const/4 v1, 0x0

    iput-object v1, p0, Lio/reactivex/d/e/d/dx$a;->f:Lio/reactivex/i/d;

    .line 114
    invoke-virtual {v0}, Lio/reactivex/i/d;->onComplete()V

    .line 116
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 117
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->f:Lio/reactivex/i/d;

    .line 102
    if-eqz v0, :cond_0

    .line 103
    const/4 v1, 0x0

    iput-object v1, p0, Lio/reactivex/d/e/d/dx$a;->f:Lio/reactivex/i/d;

    .line 104
    invoke-virtual {v0, p1}, Lio/reactivex/i/d;->onError(Ljava/lang/Throwable;)V

    .line 106
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 107
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->f:Lio/reactivex/i/d;

    .line 80
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lio/reactivex/d/e/d/dx$a;->g:Z

    if-nez v1, :cond_0

    .line 81
    iget v0, p0, Lio/reactivex/d/e/d/dx$a;->c:I

    invoke-static {v0, p0}, Lio/reactivex/i/d;->a(ILjava/lang/Runnable;)Lio/reactivex/i/d;

    move-result-object v0

    .line 82
    iput-object v0, p0, Lio/reactivex/d/e/d/dx$a;->f:Lio/reactivex/i/d;

    .line 83
    iget-object v1, p0, Lio/reactivex/d/e/d/dx$a;->a:Lio/reactivex/t;

    invoke-interface {v1, v0}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 86
    :cond_0
    if-eqz v0, :cond_1

    .line 87
    invoke-virtual {v0, p1}, Lio/reactivex/i/d;->onNext(Ljava/lang/Object;)V

    .line 88
    iget-wide v2, p0, Lio/reactivex/d/e/d/dx$a;->d:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lio/reactivex/d/e/d/dx$a;->d:J

    iget-wide v4, p0, Lio/reactivex/d/e/d/dx$a;->b:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 89
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lio/reactivex/d/e/d/dx$a;->d:J

    .line 90
    const/4 v1, 0x0

    iput-object v1, p0, Lio/reactivex/d/e/d/dx$a;->f:Lio/reactivex/i/d;

    .line 91
    invoke-virtual {v0}, Lio/reactivex/i/d;->onComplete()V

    .line 92
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dx$a;->g:Z

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->e:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 97
    :cond_1
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->e:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iput-object p1, p0, Lio/reactivex/d/e/d/dx$a;->e:Lio/reactivex/b/b;

    .line 73
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 75
    :cond_0
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dx$a;->g:Z

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$a;->e:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 134
    :cond_0
    return-void
.end method
