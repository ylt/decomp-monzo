.class final Lio/reactivex/d/e/d/dk$a;
.super Ljava/util/concurrent/atomic/AtomicBoolean;
.source "ObservableTakeLastTimed.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/dk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:J

.field final d:Ljava/util/concurrent/TimeUnit;

.field final e:Lio/reactivex/u;

.field final f:Lio/reactivex/d/f/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/f/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final g:Z

.field h:Lio/reactivex/b/b;

.field volatile i:Z

.field j:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lio/reactivex/t;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/u;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;JJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 67
    iput-object p1, p0, Lio/reactivex/d/e/d/dk$a;->a:Lio/reactivex/t;

    .line 68
    iput-wide p2, p0, Lio/reactivex/d/e/d/dk$a;->b:J

    .line 69
    iput-wide p4, p0, Lio/reactivex/d/e/d/dk$a;->c:J

    .line 70
    iput-object p6, p0, Lio/reactivex/d/e/d/dk$a;->d:Ljava/util/concurrent/TimeUnit;

    .line 71
    iput-object p7, p0, Lio/reactivex/d/e/d/dk$a;->e:Lio/reactivex/u;

    .line 72
    new-instance v0, Lio/reactivex/d/f/c;

    invoke-direct {v0, p8}, Lio/reactivex/d/f/c;-><init>(I)V

    iput-object v0, p0, Lio/reactivex/d/e/d/dk$a;->f:Lio/reactivex/d/f/c;

    .line 73
    iput-boolean p9, p0, Lio/reactivex/d/e/d/dk$a;->g:Z

    .line 74
    return-void
.end method


# virtual methods
.method a()V
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 135
    invoke-virtual {p0, v3, v2}, Lio/reactivex/d/e/d/dk$a;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v4, p0, Lio/reactivex/d/e/d/dk$a;->a:Lio/reactivex/t;

    .line 140
    iget-object v5, p0, Lio/reactivex/d/e/d/dk$a;->f:Lio/reactivex/d/f/c;

    .line 141
    iget-boolean v6, p0, Lio/reactivex/d/e/d/dk$a;->g:Z

    .line 144
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dk$a;->i:Z

    if-eqz v0, :cond_2

    .line 145
    invoke-virtual {v5}, Lio/reactivex/d/f/c;->c()V

    goto :goto_0

    .line 149
    :cond_2
    if-nez v6, :cond_3

    .line 150
    iget-object v0, p0, Lio/reactivex/d/e/d/dk$a;->j:Ljava/lang/Throwable;

    .line 151
    if-eqz v0, :cond_3

    .line 152
    invoke-virtual {v5}, Lio/reactivex/d/f/c;->c()V

    .line 153
    invoke-interface {v4, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 158
    :cond_3
    invoke-virtual {v5}, Lio/reactivex/d/f/c;->n_()Ljava/lang/Object;

    move-result-object v0

    .line 159
    if-nez v0, :cond_4

    move v1, v2

    .line 161
    :goto_2
    if-eqz v1, :cond_6

    .line 162
    iget-object v0, p0, Lio/reactivex/d/e/d/dk$a;->j:Ljava/lang/Throwable;

    .line 163
    if-eqz v0, :cond_5

    .line 164
    invoke-interface {v4, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_4
    move v1, v3

    .line 159
    goto :goto_2

    .line 166
    :cond_5
    invoke-interface {v4}, Lio/reactivex/t;->onComplete()V

    goto :goto_0

    .line 172
    :cond_6
    invoke-virtual {v5}, Lio/reactivex/d/f/c;->n_()Ljava/lang/Object;

    move-result-object v1

    .line 174
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v0, p0, Lio/reactivex/d/e/d/dk$a;->e:Lio/reactivex/u;

    iget-object v7, p0, Lio/reactivex/d/e/d/dk$a;->d:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v7}, Lio/reactivex/u;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    iget-wide v12, p0, Lio/reactivex/d/e/d/dk$a;->c:J

    sub-long/2addr v10, v12

    cmp-long v0, v8, v10

    if-ltz v0, :cond_1

    .line 178
    invoke-interface {v4, v1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public dispose()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 119
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dk$a;->i:Z

    if-nez v0, :cond_0

    .line 120
    iput-boolean v1, p0, Lio/reactivex/d/e/d/dk$a;->i:Z

    .line 121
    iget-object v0, p0, Lio/reactivex/d/e/d/dk$a;->h:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 123
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/d/dk$a;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lio/reactivex/d/e/d/dk$a;->f:Lio/reactivex/d/f/c;

    invoke-virtual {v0}, Lio/reactivex/d/f/c;->c()V

    .line 127
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dk$a;->i:Z

    return v0
.end method

.method public onComplete()V
    .locals 0

    .prologue
    .line 114
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dk$a;->a()V

    .line 115
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lio/reactivex/d/e/d/dk$a;->j:Ljava/lang/Throwable;

    .line 109
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dk$a;->a()V

    .line 110
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v2, p0, Lio/reactivex/d/e/d/dk$a;->f:Lio/reactivex/d/f/c;

    .line 88
    iget-object v0, p0, Lio/reactivex/d/e/d/dk$a;->e:Lio/reactivex/u;

    iget-object v1, p0, Lio/reactivex/d/e/d/dk$a;->d:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lio/reactivex/u;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    .line 89
    iget-wide v6, p0, Lio/reactivex/d/e/d/dk$a;->c:J

    .line 90
    iget-wide v8, p0, Lio/reactivex/d/e/d/dk$a;->b:J

    .line 91
    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, v8, v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 93
    :goto_0
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0, p1}, Lio/reactivex/d/f/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 95
    :goto_1
    invoke-virtual {v2}, Lio/reactivex/d/f/c;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 96
    invoke-virtual {v2}, Lio/reactivex/d/f/c;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 97
    sub-long v12, v4, v6

    cmp-long v0, v10, v12

    if-lez v0, :cond_0

    if-nez v1, :cond_2

    invoke-virtual {v2}, Lio/reactivex/d/f/c;->e()I

    move-result v0

    shr-int/lit8 v0, v0, 0x1

    int-to-long v10, v0

    cmp-long v0, v10, v8

    if-lez v0, :cond_2

    .line 98
    :cond_0
    invoke-virtual {v2}, Lio/reactivex/d/f/c;->n_()Ljava/lang/Object;

    .line 99
    invoke-virtual {v2}, Lio/reactivex/d/f/c;->n_()Ljava/lang/Object;

    goto :goto_1

    .line 91
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 104
    :cond_2
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lio/reactivex/d/e/d/dk$a;->h:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iput-object p1, p0, Lio/reactivex/d/e/d/dk$a;->h:Lio/reactivex/b/b;

    .line 80
    iget-object v0, p0, Lio/reactivex/d/e/d/dk$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 82
    :cond_0
    return-void
.end method
