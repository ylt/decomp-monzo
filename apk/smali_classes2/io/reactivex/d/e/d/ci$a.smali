.class final Lio/reactivex/d/e/d/ci$a;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableRepeat.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/ci;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/d/a/k;

.field final c:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field d:J


# direct methods
.method constructor <init>(Lio/reactivex/t;JLio/reactivex/d/a/k;Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;J",
            "Lio/reactivex/d/a/k;",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 47
    iput-object p1, p0, Lio/reactivex/d/e/d/ci$a;->a:Lio/reactivex/t;

    .line 48
    iput-object p4, p0, Lio/reactivex/d/e/d/ci$a;->b:Lio/reactivex/d/a/k;

    .line 49
    iput-object p5, p0, Lio/reactivex/d/e/d/ci$a;->c:Lio/reactivex/r;

    .line 50
    iput-wide p2, p0, Lio/reactivex/d/e/d/ci$a;->d:J

    .line 51
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lio/reactivex/d/e/d/ci$a;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_1

    .line 85
    const/4 v0, 0x1

    .line 87
    :cond_0
    iget-object v1, p0, Lio/reactivex/d/e/d/ci$a;->b:Lio/reactivex/d/a/k;

    invoke-virtual {v1}, Lio/reactivex/d/a/k;->isDisposed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 98
    :cond_1
    :goto_0
    return-void

    .line 90
    :cond_2
    iget-object v1, p0, Lio/reactivex/d/e/d/ci$a;->c:Lio/reactivex/r;

    invoke-interface {v1, p0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 92
    neg-int v0, v0

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/d/ci$a;->addAndGet(I)I

    move-result v0

    .line 93
    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public onComplete()V
    .locals 4

    .prologue
    .line 69
    iget-wide v0, p0, Lio/reactivex/d/e/d/ci$a;->d:J

    .line 70
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 71
    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lio/reactivex/d/e/d/ci$a;->d:J

    .line 73
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 74
    invoke-virtual {p0}, Lio/reactivex/d/e/d/ci$a;->a()V

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p0, Lio/reactivex/d/e/d/ci$a;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lio/reactivex/d/e/d/ci$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 65
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lio/reactivex/d/e/d/ci$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 61
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lio/reactivex/d/e/d/ci$a;->b:Lio/reactivex/d/a/k;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/k;->b(Lio/reactivex/b/b;)Z

    .line 56
    return-void
.end method
