.class final Lio/reactivex/d/e/d/cj$a;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableRepeatUntil.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/cj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/d/a/k;

.field final c:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final d:Lio/reactivex/c/e;


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/c/e;Lio/reactivex/d/a/k;Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;",
            "Lio/reactivex/c/e;",
            "Lio/reactivex/d/a/k;",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 49
    iput-object p1, p0, Lio/reactivex/d/e/d/cj$a;->a:Lio/reactivex/t;

    .line 50
    iput-object p3, p0, Lio/reactivex/d/e/d/cj$a;->b:Lio/reactivex/d/a/k;

    .line 51
    iput-object p4, p0, Lio/reactivex/d/e/d/cj$a;->c:Lio/reactivex/r;

    .line 52
    iput-object p2, p0, Lio/reactivex/d/e/d/cj$a;->d:Lio/reactivex/c/e;

    .line 53
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lio/reactivex/d/e/d/cj$a;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_1

    .line 91
    const/4 v0, 0x1

    .line 93
    :cond_0
    iget-object v1, p0, Lio/reactivex/d/e/d/cj$a;->c:Lio/reactivex/r;

    invoke-interface {v1, p0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 95
    neg-int v0, v0

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/d/cj$a;->addAndGet(I)I

    move-result v0

    .line 96
    if-nez v0, :cond_0

    .line 101
    :cond_1
    return-void
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 73
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/d/cj$a;->d:Lio/reactivex/c/e;

    invoke-interface {v0}, Lio/reactivex/c/e;->a()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 79
    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lio/reactivex/d/e/d/cj$a;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 84
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 76
    iget-object v1, p0, Lio/reactivex/d/e/d/cj$a;->a:Lio/reactivex/t;

    invoke-interface {v1, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/d/cj$a;->a()V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lio/reactivex/d/e/d/cj$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 67
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lio/reactivex/d/e/d/cj$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lio/reactivex/d/e/d/cj$a;->b:Lio/reactivex/d/a/k;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/k;->b(Lio/reactivex/b/b;)Z

    .line 58
    return-void
.end method
