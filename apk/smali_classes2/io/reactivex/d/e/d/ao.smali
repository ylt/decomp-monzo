.class public final Lio/reactivex/d/e/d/ao;
.super Lio/reactivex/h;
.source "ObservableElementAtMaybe.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/ao$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/h",
        "<TT;>;",
        "Lio/reactivex/d/c/c",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:J


# direct methods
.method public constructor <init>(Lio/reactivex/r;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;J)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Lio/reactivex/h;-><init>()V

    .line 26
    iput-object p1, p0, Lio/reactivex/d/e/d/ao;->a:Lio/reactivex/r;

    .line 27
    iput-wide p2, p0, Lio/reactivex/d/e/d/ao;->b:J

    .line 28
    return-void
.end method


# virtual methods
.method public b(Lio/reactivex/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lio/reactivex/d/e/d/ao;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/ao$a;

    iget-wide v2, p0, Lio/reactivex/d/e/d/ao;->b:J

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/d/ao$a;-><init>(Lio/reactivex/j;J)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 32
    return-void
.end method

.method public q_()Lio/reactivex/n;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lio/reactivex/d/e/d/an;

    iget-object v1, p0, Lio/reactivex/d/e/d/ao;->a:Lio/reactivex/r;

    iget-wide v2, p0, Lio/reactivex/d/e/d/ao;->b:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/an;-><init>(Lio/reactivex/r;JLjava/lang/Object;Z)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
