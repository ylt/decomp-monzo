.class final Lio/reactivex/d/e/d/dr$b;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableTimeoutTimed.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/dr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dr$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/u$c;

.field e:Lio/reactivex/b/b;

.field volatile f:J

.field volatile g:Z


# direct methods
.method constructor <init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u$c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 200
    iput-object p1, p0, Lio/reactivex/d/e/d/dr$b;->a:Lio/reactivex/t;

    .line 201
    iput-wide p2, p0, Lio/reactivex/d/e/d/dr$b;->b:J

    .line 202
    iput-object p4, p0, Lio/reactivex/d/e/d/dr$b;->c:Ljava/util/concurrent/TimeUnit;

    .line 203
    iput-object p5, p0, Lio/reactivex/d/e/d/dr$b;->d:Lio/reactivex/u$c;

    .line 204
    return-void
.end method


# virtual methods
.method a(J)V
    .locals 5

    .prologue
    .line 230
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dr$b;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    .line 231
    if-eqz v0, :cond_0

    .line 232
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 235
    :cond_0
    sget-object v1, Lio/reactivex/d/e/d/dr;->f:Lio/reactivex/b/b;

    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/d/dr$b;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lio/reactivex/d/e/d/dr$b;->d:Lio/reactivex/u$c;

    new-instance v1, Lio/reactivex/d/e/d/dr$b$a;

    invoke-direct {v1, p0, p1, p2}, Lio/reactivex/d/e/d/dr$b$a;-><init>(Lio/reactivex/d/e/d/dr$b;J)V

    iget-wide v2, p0, Lio/reactivex/d/e/d/dr$b;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/dr$b;->c:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lio/reactivex/u$c;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/b;

    move-result-object v0

    .line 238
    invoke-static {p0, v0}, Lio/reactivex/d/a/d;->c(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 240
    :cond_1
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lio/reactivex/d/e/d/dr$b;->e:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 268
    iget-object v0, p0, Lio/reactivex/d/e/d/dr$b;->d:Lio/reactivex/u$c;

    invoke-virtual {v0}, Lio/reactivex/u$c;->dispose()V

    .line 269
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lio/reactivex/d/e/d/dr$b;->d:Lio/reactivex/u$c;

    invoke-virtual {v0}, Lio/reactivex/u$c;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dr$b;->g:Z

    if-eqz v0, :cond_0

    .line 263
    :goto_0
    return-void

    .line 259
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/dr$b;->g:Z

    .line 261
    iget-object v0, p0, Lio/reactivex/d/e/d/dr$b;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 262
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dr$b;->dispose()V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 244
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dr$b;->g:Z

    if-eqz v0, :cond_0

    .line 245
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 252
    :goto_0
    return-void

    .line 248
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/dr$b;->g:Z

    .line 250
    iget-object v0, p0, Lio/reactivex/d/e/d/dr$b;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 251
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dr$b;->dispose()V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 218
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dr$b;->g:Z

    if-eqz v0, :cond_0

    .line 227
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-wide v0, p0, Lio/reactivex/d/e/d/dr$b;->f:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 222
    iput-wide v0, p0, Lio/reactivex/d/e/d/dr$b;->f:J

    .line 224
    iget-object v2, p0, Lio/reactivex/d/e/d/dr$b;->a:Lio/reactivex/t;

    invoke-interface {v2, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 226
    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/d/dr$b;->a(J)V

    goto :goto_0
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lio/reactivex/d/e/d/dr$b;->e:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iput-object p1, p0, Lio/reactivex/d/e/d/dr$b;->e:Lio/reactivex/b/b;

    .line 210
    iget-object v0, p0, Lio/reactivex/d/e/d/dr$b;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 211
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lio/reactivex/d/e/d/dr$b;->a(J)V

    .line 214
    :cond_0
    return-void
.end method
