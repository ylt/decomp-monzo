.class public final Lio/reactivex/d/e/d/ad;
.super Lio/reactivex/d/e/d/a;
.source "ObservableDelay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/ad$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/u;

.field final e:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 32
    iput-wide p2, p0, Lio/reactivex/d/e/d/ad;->b:J

    .line 33
    iput-object p4, p0, Lio/reactivex/d/e/d/ad;->c:Ljava/util/concurrent/TimeUnit;

    .line 34
    iput-object p5, p0, Lio/reactivex/d/e/d/ad;->d:Lio/reactivex/u;

    .line 35
    iput-boolean p6, p0, Lio/reactivex/d/e/d/ad;->e:Z

    .line 36
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    iget-boolean v0, p0, Lio/reactivex/d/e/d/ad;->e:Z

    if-eqz v0, :cond_0

    move-object v1, p1

    .line 48
    :goto_0
    iget-object v0, p0, Lio/reactivex/d/e/d/ad;->d:Lio/reactivex/u;

    invoke-virtual {v0}, Lio/reactivex/u;->a()Lio/reactivex/u$c;

    move-result-object v5

    .line 50
    iget-object v7, p0, Lio/reactivex/d/e/d/ad;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/ad$a;

    iget-wide v2, p0, Lio/reactivex/d/e/d/ad;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/ad;->c:Ljava/util/concurrent/TimeUnit;

    iget-boolean v6, p0, Lio/reactivex/d/e/d/ad;->e:Z

    invoke-direct/range {v0 .. v6}, Lio/reactivex/d/e/d/ad$a;-><init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u$c;Z)V

    invoke-interface {v7, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 51
    return-void

    .line 45
    :cond_0
    new-instance v1, Lio/reactivex/f/e;

    invoke-direct {v1, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    goto :goto_0
.end method
