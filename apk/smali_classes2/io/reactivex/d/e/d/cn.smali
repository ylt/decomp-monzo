.class public final Lio/reactivex/d/e/d/cn;
.super Lio/reactivex/d/e/d/a;
.source "ObservableRetryPredicate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/cn$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field final c:J


# direct methods
.method public constructor <init>(Lio/reactivex/n;JLio/reactivex/c/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/n",
            "<TT;>;J",
            "Lio/reactivex/c/q",
            "<-",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 31
    iput-object p4, p0, Lio/reactivex/d/e/d/cn;->b:Lio/reactivex/c/q;

    .line 32
    iput-wide p2, p0, Lio/reactivex/d/e/d/cn;->c:J

    .line 33
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v5, Lio/reactivex/d/a/k;

    invoke-direct {v5}, Lio/reactivex/d/a/k;-><init>()V

    .line 38
    invoke-interface {p1, v5}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 40
    new-instance v0, Lio/reactivex/d/e/d/cn$a;

    iget-wide v2, p0, Lio/reactivex/d/e/d/cn;->c:J

    iget-object v4, p0, Lio/reactivex/d/e/d/cn;->b:Lio/reactivex/c/q;

    iget-object v6, p0, Lio/reactivex/d/e/d/cn;->a:Lio/reactivex/r;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/d/e/d/cn$a;-><init>(Lio/reactivex/t;JLio/reactivex/c/q;Lio/reactivex/d/a/k;Lio/reactivex/r;)V

    .line 41
    invoke-virtual {v0}, Lio/reactivex/d/e/d/cn$a;->a()V

    .line 42
    return-void
.end method
