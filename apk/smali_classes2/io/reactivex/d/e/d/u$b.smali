.class final Lio/reactivex/d/e/d/u$b;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableCombineLatest.java"

# interfaces
.implements Lio/reactivex/b/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/b/b;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TR;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-[",
            "Ljava/lang/Object;",
            "+TR;>;"
        }
    .end annotation
.end field

.field final c:[Lio/reactivex/d/e/d/u$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lio/reactivex/d/e/d/u$a",
            "<TT;TR;>;"
        }
    .end annotation
.end field

.field final d:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field final e:Lio/reactivex/d/f/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/f/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final f:Z

.field volatile g:Z

.field volatile h:Z

.field final i:Lio/reactivex/d/j/c;

.field j:I

.field k:I


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/c/h;IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TR;>;",
            "Lio/reactivex/c/h",
            "<-[",
            "Ljava/lang/Object;",
            "+TR;>;IIZ)V"
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 90
    new-instance v0, Lio/reactivex/d/j/c;

    invoke-direct {v0}, Lio/reactivex/d/j/c;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/u$b;->i:Lio/reactivex/d/j/c;

    .line 99
    iput-object p1, p0, Lio/reactivex/d/e/d/u$b;->a:Lio/reactivex/t;

    .line 100
    iput-object p2, p0, Lio/reactivex/d/e/d/u$b;->b:Lio/reactivex/c/h;

    .line 101
    iput-boolean p5, p0, Lio/reactivex/d/e/d/u$b;->f:Z

    .line 102
    new-array v0, p3, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lio/reactivex/d/e/d/u$b;->d:[Ljava/lang/Object;

    .line 103
    new-array v0, p3, [Lio/reactivex/d/e/d/u$a;

    iput-object v0, p0, Lio/reactivex/d/e/d/u$b;->c:[Lio/reactivex/d/e/d/u$a;

    .line 104
    new-instance v0, Lio/reactivex/d/f/c;

    invoke-direct {v0, p4}, Lio/reactivex/d/f/c;-><init>(I)V

    iput-object v0, p0, Lio/reactivex/d/e/d/u$b;->e:Lio/reactivex/d/f/c;

    .line 105
    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    .line 145
    iget-object v1, p0, Lio/reactivex/d/e/d/u$b;->c:[Lio/reactivex/d/e/d/u$a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 146
    invoke-virtual {v3}, Lio/reactivex/d/e/d/u$a;->a()V

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    return-void
.end method

.method a(Lio/reactivex/d/f/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/f/c",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/d/u$b;->b(Lio/reactivex/d/f/c;)V

    .line 141
    invoke-virtual {p0}, Lio/reactivex/d/e/d/u$b;->a()V

    .line 142
    return-void
.end method

.method a(Ljava/lang/Object;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 158
    iget-object v2, p0, Lio/reactivex/d/e/d/u$b;->c:[Lio/reactivex/d/e/d/u$a;

    aget-object v5, v2, p2

    .line 165
    monitor-enter p0

    .line 166
    :try_start_0
    iget-boolean v2, p0, Lio/reactivex/d/e/d/u$b;->g:Z

    if-eqz v2, :cond_1

    .line 167
    monitor-exit p0

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v2, p0, Lio/reactivex/d/e/d/u$b;->d:[Ljava/lang/Object;

    array-length v6, v2

    .line 170
    iget-object v2, p0, Lio/reactivex/d/e/d/u$b;->d:[Ljava/lang/Object;

    aget-object v7, v2, p2

    .line 171
    iget v2, p0, Lio/reactivex/d/e/d/u$b;->j:I

    .line 172
    if-nez v7, :cond_2

    .line 173
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lio/reactivex/d/e/d/u$b;->j:I

    :cond_2
    move v4, v2

    .line 175
    iget v2, p0, Lio/reactivex/d/e/d/u$b;->k:I

    .line 176
    if-nez p1, :cond_7

    .line 177
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lio/reactivex/d/e/d/u$b;->k:I

    move v3, v2

    .line 181
    :goto_1
    if-ne v4, v6, :cond_8

    move v2, v1

    .line 183
    :goto_2
    if-eq v3, v6, :cond_3

    if-nez p1, :cond_4

    if-nez v7, :cond_4

    :cond_3
    move v0, v1

    .line 185
    :cond_4
    if-nez v0, :cond_a

    .line 186
    if-eqz p1, :cond_9

    if-eqz v2, :cond_9

    .line 187
    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->e:Lio/reactivex/d/f/c;

    iget-object v1, p0, Lio/reactivex/d/e/d/u$b;->d:[Ljava/lang/Object;

    invoke-virtual {v1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lio/reactivex/d/f/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 195
    :cond_5
    :goto_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    if-nez v2, :cond_6

    if-nez p1, :cond_0

    .line 199
    :cond_6
    invoke-virtual {p0}, Lio/reactivex/d/e/d/u$b;->b()V

    goto :goto_0

    .line 179
    :cond_7
    :try_start_1
    iget-object v3, p0, Lio/reactivex/d/e/d/u$b;->d:[Ljava/lang/Object;

    aput-object p1, v3, p2

    move v3, v2

    goto :goto_1

    :cond_8
    move v2, v0

    .line 181
    goto :goto_2

    .line 189
    :cond_9
    if-nez p1, :cond_5

    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->i:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/u$b;->h:Z

    goto :goto_3

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 193
    :cond_a
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lio/reactivex/d/e/d/u$b;->h:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->i:Lio/reactivex/d/j/c;

    invoke-virtual {v0, p1}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 295
    :cond_0
    return-void
.end method

.method public a([Lio/reactivex/r;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 108
    iget-object v2, p0, Lio/reactivex/d/e/d/u$b;->c:[Lio/reactivex/d/e/d/u$a;

    .line 109
    array-length v3, v2

    move v1, v0

    .line 110
    :goto_0
    if-ge v1, v3, :cond_0

    .line 111
    new-instance v4, Lio/reactivex/d/e/d/u$a;

    invoke-direct {v4, p0, v1}, Lio/reactivex/d/e/d/u$a;-><init>(Lio/reactivex/d/e/d/u$b;I)V

    aput-object v4, v2, v1

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 113
    :cond_0
    invoke-virtual {p0, v0}, Lio/reactivex/d/e/d/u$b;->lazySet(I)V

    .line 114
    iget-object v1, p0, Lio/reactivex/d/e/d/u$b;->a:Lio/reactivex/t;

    invoke-interface {v1, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 115
    :goto_1
    if-ge v0, v3, :cond_1

    .line 116
    iget-boolean v1, p0, Lio/reactivex/d/e/d/u$b;->h:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lio/reactivex/d/e/d/u$b;->g:Z

    if-eqz v1, :cond_2

    .line 121
    :cond_1
    return-void

    .line 119
    :cond_2
    aget-object v1, p1, v0

    aget-object v4, v2, v0

    invoke-interface {v1, v4}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method a(ZZLio/reactivex/t;Lio/reactivex/d/f/c;Z)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lio/reactivex/t",
            "<*>;",
            "Lio/reactivex/d/f/c",
            "<*>;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 258
    iget-boolean v0, p0, Lio/reactivex/d/e/d/u$b;->g:Z

    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p0, p4}, Lio/reactivex/d/e/d/u$b;->a(Lio/reactivex/d/f/c;)V

    move v0, v1

    .line 288
    :goto_0
    return v0

    .line 262
    :cond_0
    if-eqz p1, :cond_4

    .line 263
    if-eqz p5, :cond_2

    .line 264
    if-eqz p2, :cond_4

    .line 265
    invoke-virtual {p0, p4}, Lio/reactivex/d/e/d/u$b;->a(Lio/reactivex/d/f/c;)V

    .line 266
    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->i:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_1

    .line 268
    invoke-interface {p3, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    :goto_1
    move v0, v1

    .line 272
    goto :goto_0

    .line 270
    :cond_1
    invoke-interface {p3}, Lio/reactivex/t;->onComplete()V

    goto :goto_1

    .line 275
    :cond_2
    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->i:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 276
    if-eqz v0, :cond_3

    .line 277
    invoke-virtual {p0, p4}, Lio/reactivex/d/e/d/u$b;->a(Lio/reactivex/d/f/c;)V

    .line 278
    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->i:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {p3, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    move v0, v1

    .line 279
    goto :goto_0

    .line 281
    :cond_3
    if-eqz p2, :cond_4

    .line 282
    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->e:Lio/reactivex/d/f/c;

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/d/u$b;->b(Lio/reactivex/d/f/c;)V

    .line 283
    invoke-interface {p3}, Lio/reactivex/t;->onComplete()V

    move v0, v1

    .line 284
    goto :goto_0

    .line 288
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 202
    invoke-virtual {p0}, Lio/reactivex/d/e/d/u$b;->getAndIncrement()I

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v4, p0, Lio/reactivex/d/e/d/u$b;->e:Lio/reactivex/d/f/c;

    .line 207
    iget-object v3, p0, Lio/reactivex/d/e/d/u$b;->a:Lio/reactivex/t;

    .line 208
    iget-boolean v5, p0, Lio/reactivex/d/e/d/u$b;->f:Z

    move v6, v7

    .line 213
    :goto_1
    iget-boolean v1, p0, Lio/reactivex/d/e/d/u$b;->h:Z

    invoke-virtual {v4}, Lio/reactivex/d/f/c;->b()Z

    move-result v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lio/reactivex/d/e/d/u$b;->a(ZZLio/reactivex/t;Lio/reactivex/d/f/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    :goto_2
    iget-boolean v1, p0, Lio/reactivex/d/e/d/u$b;->h:Z

    .line 221
    invoke-virtual {v4}, Lio/reactivex/d/f/c;->n_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/u$a;

    .line 222
    if-nez v0, :cond_2

    move v2, v7

    :goto_3
    move-object v0, p0

    .line 224
    invoke-virtual/range {v0 .. v5}, Lio/reactivex/d/e/d/u$b;->a(ZZLio/reactivex/t;Lio/reactivex/d/f/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    if-eqz v2, :cond_3

    .line 249
    neg-int v0, v6

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/d/u$b;->addAndGet(I)I

    move-result v0

    .line 250
    if-eqz v0, :cond_0

    move v6, v0

    goto :goto_1

    .line 222
    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    .line 233
    :cond_3
    invoke-virtual {v4}, Lio/reactivex/d/f/c;->n_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 237
    :try_start_0
    iget-object v1, p0, Lio/reactivex/d/e/d/u$b;->b:Lio/reactivex/c/h;

    invoke-interface {v1, v0}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "The combiner returned a null"

    invoke-static {v0, v1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 246
    invoke-interface {v3, v0}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    goto :goto_2

    .line 238
    :catch_0
    move-exception v0

    .line 239
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 240
    iput-boolean v7, p0, Lio/reactivex/d/e/d/u$b;->g:Z

    .line 241
    invoke-virtual {p0, v4}, Lio/reactivex/d/e/d/u$b;->a(Lio/reactivex/d/f/c;)V

    .line 242
    invoke-interface {v3, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method b(Lio/reactivex/d/f/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/f/c",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 151
    monitor-enter p0

    .line 152
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->d:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 153
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    invoke-virtual {p1}, Lio/reactivex/d/f/c;->c()V

    .line 155
    return-void

    .line 153
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lio/reactivex/d/e/d/u$b;->g:Z

    if-nez v0, :cond_0

    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/u$b;->g:Z

    .line 127
    invoke-virtual {p0}, Lio/reactivex/d/e/d/u$b;->a()V

    .line 128
    invoke-virtual {p0}, Lio/reactivex/d/e/d/u$b;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lio/reactivex/d/e/d/u$b;->e:Lio/reactivex/d/f/c;

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/d/u$b;->b(Lio/reactivex/d/f/c;)V

    .line 132
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lio/reactivex/d/e/d/u$b;->g:Z

    return v0
.end method
