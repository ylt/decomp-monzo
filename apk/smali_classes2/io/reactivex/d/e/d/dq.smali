.class public final Lio/reactivex/d/e/d/dq;
.super Lio/reactivex/d/e/d/a;
.source "ObservableTimeout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dq$d;,
        Lio/reactivex/d/e/d/dq$b;,
        Lio/reactivex/d/e/d/dq$a;,
        Lio/reactivex/d/e/d/dq$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TU;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field final d:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/h;Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/r",
            "<TU;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<TV;>;>;",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 40
    iput-object p2, p0, Lio/reactivex/d/e/d/dq;->b:Lio/reactivex/r;

    .line 41
    iput-object p3, p0, Lio/reactivex/d/e/d/dq;->c:Lio/reactivex/c/h;

    .line 42
    iput-object p4, p0, Lio/reactivex/d/e/d/dq;->d:Lio/reactivex/r;

    .line 43
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lio/reactivex/d/e/d/dq;->d:Lio/reactivex/r;

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lio/reactivex/d/e/d/dq;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/dq$c;

    new-instance v2, Lio/reactivex/f/e;

    invoke-direct {v2, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    iget-object v3, p0, Lio/reactivex/d/e/d/dq;->b:Lio/reactivex/r;

    iget-object v4, p0, Lio/reactivex/d/e/d/dq;->c:Lio/reactivex/c/h;

    invoke-direct {v1, v2, v3, v4}, Lio/reactivex/d/e/d/dq$c;-><init>(Lio/reactivex/t;Lio/reactivex/r;Lio/reactivex/c/h;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 55
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/dq;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/dq$d;

    iget-object v2, p0, Lio/reactivex/d/e/d/dq;->b:Lio/reactivex/r;

    iget-object v3, p0, Lio/reactivex/d/e/d/dq;->c:Lio/reactivex/c/h;

    iget-object v4, p0, Lio/reactivex/d/e/d/dq;->d:Lio/reactivex/r;

    invoke-direct {v1, p1, v2, v3, v4}, Lio/reactivex/d/e/d/dq$d;-><init>(Lio/reactivex/t;Lio/reactivex/r;Lio/reactivex/c/h;Lio/reactivex/r;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method
