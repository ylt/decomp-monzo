.class final Lio/reactivex/d/e/d/v$b$a;
.super Ljava/lang/Object;
.source "ObservableConcatMap.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/v$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<U:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/t",
        "<TU;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TU;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/d/e/d/v$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/d/v$b",
            "<**>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/d/e/d/v$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TU;>;",
            "Lio/reactivex/d/e/d/v$b",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    iput-object p1, p0, Lio/reactivex/d/e/d/v$b$a;->a:Lio/reactivex/t;

    .line 240
    iput-object p2, p0, Lio/reactivex/d/e/d/v$b$a;->b:Lio/reactivex/d/e/d/v$b;

    .line 241
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b$a;->b:Lio/reactivex/d/e/d/v$b;

    invoke-virtual {v0}, Lio/reactivex/d/e/d/v$b;->a()V

    .line 260
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b$a;->b:Lio/reactivex/d/e/d/v$b;

    invoke-virtual {v0}, Lio/reactivex/d/e/d/v$b;->dispose()V

    .line 255
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 256
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TU;)V"
        }
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 251
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lio/reactivex/d/e/d/v$b$a;->b:Lio/reactivex/d/e/d/v$b;

    invoke-virtual {v0, p1}, Lio/reactivex/d/e/d/v$b;->a(Lio/reactivex/b/b;)V

    .line 246
    return-void
.end method
