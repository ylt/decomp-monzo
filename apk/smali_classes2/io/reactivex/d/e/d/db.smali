.class public final Lio/reactivex/d/e/d/db;
.super Lio/reactivex/d/e/d/a;
.source "ObservableSkipLastTimed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/db$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/u;

.field final e:I

.field final f:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 34
    iput-wide p2, p0, Lio/reactivex/d/e/d/db;->b:J

    .line 35
    iput-object p4, p0, Lio/reactivex/d/e/d/db;->c:Ljava/util/concurrent/TimeUnit;

    .line 36
    iput-object p5, p0, Lio/reactivex/d/e/d/db;->d:Lio/reactivex/u;

    .line 37
    iput p6, p0, Lio/reactivex/d/e/d/db;->e:I

    .line 38
    iput-boolean p7, p0, Lio/reactivex/d/e/d/db;->f:Z

    .line 39
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v8, p0, Lio/reactivex/d/e/d/db;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/db$a;

    iget-wide v2, p0, Lio/reactivex/d/e/d/db;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/db;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lio/reactivex/d/e/d/db;->d:Lio/reactivex/u;

    iget v6, p0, Lio/reactivex/d/e/d/db;->e:I

    iget-boolean v7, p0, Lio/reactivex/d/e/d/db;->f:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lio/reactivex/d/e/d/db$a;-><init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;IZ)V

    invoke-interface {v8, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 44
    return-void
.end method
