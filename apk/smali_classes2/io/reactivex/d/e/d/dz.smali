.class public final Lio/reactivex/d/e/d/dz;
.super Lio/reactivex/d/e/d/a;
.source "ObservableWindowBoundarySelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dz$a;,
        Lio/reactivex/d/e/d/dz$b;,
        Lio/reactivex/d/e/d/dz$d;,
        Lio/reactivex/d/e/d/dz$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;",
        "Lio/reactivex/n",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TB;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TB;+",
            "Lio/reactivex/r",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field final d:I


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/h;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/r",
            "<TB;>;",
            "Lio/reactivex/c/h",
            "<-TB;+",
            "Lio/reactivex/r",
            "<TV;>;>;I)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 44
    iput-object p2, p0, Lio/reactivex/d/e/d/dz;->b:Lio/reactivex/r;

    .line 45
    iput-object p3, p0, Lio/reactivex/d/e/d/dz;->c:Lio/reactivex/c/h;

    .line 46
    iput p4, p0, Lio/reactivex/d/e/d/dz;->d:I

    .line 47
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lio/reactivex/d/e/d/dz;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/dz$c;

    new-instance v2, Lio/reactivex/f/e;

    invoke-direct {v2, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    iget-object v3, p0, Lio/reactivex/d/e/d/dz;->b:Lio/reactivex/r;

    iget-object v4, p0, Lio/reactivex/d/e/d/dz;->c:Lio/reactivex/c/h;

    iget v5, p0, Lio/reactivex/d/e/d/dz;->d:I

    invoke-direct {v1, v2, v3, v4, v5}, Lio/reactivex/d/e/d/dz$c;-><init>(Lio/reactivex/t;Lio/reactivex/r;Lio/reactivex/c/h;I)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 54
    return-void
.end method
