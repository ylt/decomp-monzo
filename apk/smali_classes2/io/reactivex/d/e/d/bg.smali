.class public final Lio/reactivex/d/e/d/bg;
.super Lio/reactivex/d/e/d/a;
.source "ObservableGroupBy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/bg$c;,
        Lio/reactivex/d/e/d/bg$b;,
        Lio/reactivex/d/e/d/bg$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;",
        "Lio/reactivex/e/b",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+TK;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+TV;>;"
        }
    .end annotation
.end field

.field final d:I

.field final e:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/h;Lio/reactivex/c/h;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/h",
            "<-TT;+TK;>;",
            "Lio/reactivex/c/h",
            "<-TT;+TV;>;IZ)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 40
    iput-object p2, p0, Lio/reactivex/d/e/d/bg;->b:Lio/reactivex/c/h;

    .line 41
    iput-object p3, p0, Lio/reactivex/d/e/d/bg;->c:Lio/reactivex/c/h;

    .line 42
    iput p4, p0, Lio/reactivex/d/e/d/bg;->d:I

    .line 43
    iput-boolean p5, p0, Lio/reactivex/d/e/d/bg;->e:Z

    .line 44
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Lio/reactivex/e/b",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v6, p0, Lio/reactivex/d/e/d/bg;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/bg$a;

    iget-object v2, p0, Lio/reactivex/d/e/d/bg;->b:Lio/reactivex/c/h;

    iget-object v3, p0, Lio/reactivex/d/e/d/bg;->c:Lio/reactivex/c/h;

    iget v4, p0, Lio/reactivex/d/e/d/bg;->d:I

    iget-boolean v5, p0, Lio/reactivex/d/e/d/bg;->e:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/bg$a;-><init>(Lio/reactivex/t;Lio/reactivex/c/h;Lio/reactivex/c/h;IZ)V

    invoke-interface {v6, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 49
    return-void
.end method
