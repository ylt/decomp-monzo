.class public final Lio/reactivex/d/e/d/dx;
.super Lio/reactivex/d/e/d/a;
.source "ObservableWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dx$b;,
        Lio/reactivex/d/e/d/dx$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;",
        "Lio/reactivex/n",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:J

.field final d:I


# direct methods
.method public constructor <init>(Lio/reactivex/r;JJI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;JJI)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 31
    iput-wide p2, p0, Lio/reactivex/d/e/d/dx;->b:J

    .line 32
    iput-wide p4, p0, Lio/reactivex/d/e/d/dx;->c:J

    .line 33
    iput p6, p0, Lio/reactivex/d/e/d/dx;->d:I

    .line 34
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    iget-wide v0, p0, Lio/reactivex/d/e/d/dx;->b:J

    iget-wide v2, p0, Lio/reactivex/d/e/d/dx;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 39
    iget-object v0, p0, Lio/reactivex/d/e/d/dx;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/dx$a;

    iget-wide v2, p0, Lio/reactivex/d/e/d/dx;->b:J

    iget v4, p0, Lio/reactivex/d/e/d/dx;->d:I

    invoke-direct {v1, p1, v2, v3, v4}, Lio/reactivex/d/e/d/dx$a;-><init>(Lio/reactivex/t;JI)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v7, p0, Lio/reactivex/d/e/d/dx;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/dx$b;

    iget-wide v2, p0, Lio/reactivex/d/e/d/dx;->b:J

    iget-wide v4, p0, Lio/reactivex/d/e/d/dx;->c:J

    iget v6, p0, Lio/reactivex/d/e/d/dx;->d:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/d/e/d/dx$b;-><init>(Lio/reactivex/t;JJI)V

    invoke-interface {v7, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method
