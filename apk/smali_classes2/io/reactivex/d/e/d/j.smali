.class public final Lio/reactivex/d/e/d/j;
.super Lio/reactivex/v;
.source "ObservableAnySingle.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/v",
        "<",
        "Ljava/lang/Boolean;",
        ">;",
        "Lio/reactivex/d/c/c",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/q",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Lio/reactivex/v;-><init>()V

    .line 29
    iput-object p1, p0, Lio/reactivex/d/e/d/j;->a:Lio/reactivex/r;

    .line 30
    iput-object p2, p0, Lio/reactivex/d/e/d/j;->b:Lio/reactivex/c/q;

    .line 31
    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/x;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lio/reactivex/d/e/d/j;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/j$a;

    iget-object v2, p0, Lio/reactivex/d/e/d/j;->b:Lio/reactivex/c/q;

    invoke-direct {v1, p1, v2}, Lio/reactivex/d/e/d/j$a;-><init>(Lio/reactivex/x;Lio/reactivex/c/q;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 36
    return-void
.end method

.method public q_()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lio/reactivex/d/e/d/i;

    iget-object v1, p0, Lio/reactivex/d/e/d/j;->a:Lio/reactivex/r;

    iget-object v2, p0, Lio/reactivex/d/e/d/j;->b:Lio/reactivex/c/q;

    invoke-direct {v0, v1, v2}, Lio/reactivex/d/e/d/i;-><init>(Lio/reactivex/r;Lio/reactivex/c/q;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
