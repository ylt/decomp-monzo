.class public final Lio/reactivex/d/e/d/q;
.super Lio/reactivex/d/e/d/a;
.source "ObservableBufferTimed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/q$a;,
        Lio/reactivex/d/e/d/q$c;,
        Lio/reactivex/d/e/d/q$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U::",
        "Ljava/util/Collection",
        "<-TT;>;>",
        "Lio/reactivex/d/e/d/a",
        "<TT;TU;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:J

.field final d:Ljava/util/concurrent/TimeUnit;

.field final e:Lio/reactivex/u;

.field final f:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TU;>;"
        }
    .end annotation
.end field

.field final g:I

.field final h:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/u;Ljava/util/concurrent/Callable;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;JJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            "Ljava/util/concurrent/Callable",
            "<TU;>;IZ)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 46
    iput-wide p2, p0, Lio/reactivex/d/e/d/q;->b:J

    .line 47
    iput-wide p4, p0, Lio/reactivex/d/e/d/q;->c:J

    .line 48
    iput-object p6, p0, Lio/reactivex/d/e/d/q;->d:Ljava/util/concurrent/TimeUnit;

    .line 49
    iput-object p7, p0, Lio/reactivex/d/e/d/q;->e:Lio/reactivex/u;

    .line 50
    iput-object p8, p0, Lio/reactivex/d/e/d/q;->f:Ljava/util/concurrent/Callable;

    .line 51
    iput p9, p0, Lio/reactivex/d/e/d/q;->g:I

    .line 52
    iput-boolean p10, p0, Lio/reactivex/d/e/d/q;->h:Z

    .line 53
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TU;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    iget-wide v0, p0, Lio/reactivex/d/e/d/q;->b:J

    iget-wide v2, p0, Lio/reactivex/d/e/d/q;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Lio/reactivex/d/e/d/q;->g:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 58
    iget-object v0, p0, Lio/reactivex/d/e/d/q;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/q$b;

    new-instance v2, Lio/reactivex/f/e;

    invoke-direct {v2, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    iget-object v3, p0, Lio/reactivex/d/e/d/q;->f:Ljava/util/concurrent/Callable;

    iget-wide v4, p0, Lio/reactivex/d/e/d/q;->b:J

    iget-object v6, p0, Lio/reactivex/d/e/d/q;->d:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p0, Lio/reactivex/d/e/d/q;->e:Lio/reactivex/u;

    invoke-direct/range {v1 .. v7}, Lio/reactivex/d/e/d/q$b;-><init>(Lio/reactivex/t;Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 79
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/q;->e:Lio/reactivex/u;

    invoke-virtual {v0}, Lio/reactivex/u;->a()Lio/reactivex/u$c;

    move-result-object v9

    .line 65
    iget-wide v0, p0, Lio/reactivex/d/e/d/q;->b:J

    iget-wide v2, p0, Lio/reactivex/d/e/d/q;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 66
    iget-object v0, p0, Lio/reactivex/d/e/d/q;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/q$a;

    new-instance v2, Lio/reactivex/f/e;

    invoke-direct {v2, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    iget-object v3, p0, Lio/reactivex/d/e/d/q;->f:Ljava/util/concurrent/Callable;

    iget-wide v4, p0, Lio/reactivex/d/e/d/q;->b:J

    iget-object v6, p0, Lio/reactivex/d/e/d/q;->d:Ljava/util/concurrent/TimeUnit;

    iget v7, p0, Lio/reactivex/d/e/d/q;->g:I

    iget-boolean v8, p0, Lio/reactivex/d/e/d/q;->h:Z

    invoke-direct/range {v1 .. v9}, Lio/reactivex/d/e/d/q$a;-><init>(Lio/reactivex/t;Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;IZLio/reactivex/u$c;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0

    .line 75
    :cond_1
    iget-object v0, p0, Lio/reactivex/d/e/d/q;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/q$c;

    new-instance v2, Lio/reactivex/f/e;

    invoke-direct {v2, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    iget-object v3, p0, Lio/reactivex/d/e/d/q;->f:Ljava/util/concurrent/Callable;

    iget-wide v4, p0, Lio/reactivex/d/e/d/q;->b:J

    iget-wide v6, p0, Lio/reactivex/d/e/d/q;->c:J

    iget-object v8, p0, Lio/reactivex/d/e/d/q;->d:Ljava/util/concurrent/TimeUnit;

    invoke-direct/range {v1 .. v9}, Lio/reactivex/d/e/d/q$c;-><init>(Lio/reactivex/t;Ljava/util/concurrent/Callable;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/u$c;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method
