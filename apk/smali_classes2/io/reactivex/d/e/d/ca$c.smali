.class final Lio/reactivex/d/e/d/ca$c;
.super Ljava/lang/Object;
.source "ObservablePublish.java"

# interfaces
.implements Lio/reactivex/r;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/ca;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/r",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lio/reactivex/d/e/d/ca$b",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lio/reactivex/d/e/d/ca$b",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    iput-object p1, p0, Lio/reactivex/d/e/d/ca$c;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 324
    return-void
.end method


# virtual methods
.method public subscribe(Lio/reactivex/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 329
    new-instance v2, Lio/reactivex/d/e/d/ca$a;

    invoke-direct {v2, p1}, Lio/reactivex/d/e/d/ca$a;-><init>(Lio/reactivex/t;)V

    .line 330
    invoke-interface {p1, v2}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 335
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/ca$c;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/ca$b;

    .line 337
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lio/reactivex/d/e/d/ca$b;->isDisposed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 339
    :cond_1
    new-instance v1, Lio/reactivex/d/e/d/ca$b;

    iget-object v3, p0, Lio/reactivex/d/e/d/ca$c;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1, v3}, Lio/reactivex/d/e/d/ca$b;-><init>(Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 341
    iget-object v3, p0, Lio/reactivex/d/e/d/ca$c;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 354
    :cond_2
    invoke-virtual {v0, v2}, Lio/reactivex/d/e/d/ca$b;->a(Lio/reactivex/d/e/d/ca$a;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    invoke-virtual {v2, v0}, Lio/reactivex/d/e/d/ca$a;->a(Lio/reactivex/d/e/d/ca$b;)V

    .line 383
    return-void
.end method
