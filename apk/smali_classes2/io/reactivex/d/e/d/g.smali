.class public final Lio/reactivex/d/e/d/g;
.super Lio/reactivex/v;
.source "ObservableAllSingle.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/v",
        "<",
        "Ljava/lang/Boolean;",
        ">;",
        "Lio/reactivex/d/c/c",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/q",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Lio/reactivex/v;-><init>()V

    .line 28
    iput-object p1, p0, Lio/reactivex/d/e/d/g;->a:Lio/reactivex/r;

    .line 29
    iput-object p2, p0, Lio/reactivex/d/e/d/g;->b:Lio/reactivex/c/q;

    .line 30
    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/x;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lio/reactivex/d/e/d/g;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/g$a;

    iget-object v2, p0, Lio/reactivex/d/e/d/g;->b:Lio/reactivex/c/q;

    invoke-direct {v1, p1, v2}, Lio/reactivex/d/e/d/g$a;-><init>(Lio/reactivex/x;Lio/reactivex/c/q;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 35
    return-void
.end method

.method public q_()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lio/reactivex/d/e/d/f;

    iget-object v1, p0, Lio/reactivex/d/e/d/g;->a:Lio/reactivex/r;

    iget-object v2, p0, Lio/reactivex/d/e/d/g;->b:Lio/reactivex/c/q;

    invoke-direct {v0, v1, v2}, Lio/reactivex/d/e/d/f;-><init>(Lio/reactivex/r;Lio/reactivex/c/q;)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
