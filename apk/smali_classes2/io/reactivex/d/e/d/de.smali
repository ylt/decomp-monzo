.class public final Lio/reactivex/d/e/d/de;
.super Lio/reactivex/d/e/d/a;
.source "ObservableSubscribeOn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/de$b;,
        Lio/reactivex/d/e/d/de$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 27
    iput-object p2, p0, Lio/reactivex/d/e/d/de;->b:Lio/reactivex/u;

    .line 28
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Lio/reactivex/d/e/d/de$a;

    invoke-direct {v0, p1}, Lio/reactivex/d/e/d/de$a;-><init>(Lio/reactivex/t;)V

    .line 34
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 36
    iget-object v1, p0, Lio/reactivex/d/e/d/de;->b:Lio/reactivex/u;

    new-instance v2, Lio/reactivex/d/e/d/de$b;

    invoke-direct {v2, p0, v0}, Lio/reactivex/d/e/d/de$b;-><init>(Lio/reactivex/d/e/d/de;Lio/reactivex/d/e/d/de$a;)V

    invoke-virtual {v1, v2}, Lio/reactivex/u;->a(Ljava/lang/Runnable;)Lio/reactivex/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/d/e/d/de$a;->a(Lio/reactivex/b/b;)V

    .line 37
    return-void
.end method
