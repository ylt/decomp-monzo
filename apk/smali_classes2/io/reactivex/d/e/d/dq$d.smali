.class final Lio/reactivex/d/e/d/dq$d;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableTimeout.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/d/e/d/dq$a;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/dq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/d/e/d/dq$a;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TU;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field final d:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final e:Lio/reactivex/d/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/a/j",
            "<TT;>;"
        }
    .end annotation
.end field

.field f:Lio/reactivex/b/b;

.field g:Z

.field volatile h:J


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/r;Lio/reactivex/c/h;Lio/reactivex/r;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;",
            "Lio/reactivex/r",
            "<TU;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<TV;>;>;",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 236
    iput-object p1, p0, Lio/reactivex/d/e/d/dq$d;->a:Lio/reactivex/t;

    .line 237
    iput-object p2, p0, Lio/reactivex/d/e/d/dq$d;->b:Lio/reactivex/r;

    .line 238
    iput-object p3, p0, Lio/reactivex/d/e/d/dq$d;->c:Lio/reactivex/c/h;

    .line 239
    iput-object p4, p0, Lio/reactivex/d/e/d/dq$d;->d:Lio/reactivex/r;

    .line 240
    new-instance v0, Lio/reactivex/d/a/j;

    const/16 v1, 0x8

    invoke-direct {v0, p1, p0, v1}, Lio/reactivex/d/a/j;-><init>(Lio/reactivex/t;Lio/reactivex/b/b;I)V

    iput-object v0, p0, Lio/reactivex/d/e/d/dq$d;->e:Lio/reactivex/d/a/j;

    .line 241
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 3

    .prologue
    .line 336
    iget-wide v0, p0, Lio/reactivex/d/e/d/dq$d;->h:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dq$d;->dispose()V

    .line 338
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->d:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/d/o;

    iget-object v2, p0, Lio/reactivex/d/e/d/dq$d;->e:Lio/reactivex/d/a/j;

    invoke-direct {v1, v2}, Lio/reactivex/d/d/o;-><init>(Lio/reactivex/d/a/j;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 340
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->f:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 345
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 346
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 324
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->f:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 327
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->f:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 314
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dq$d;->g:Z

    if-eqz v0, :cond_0

    .line 320
    :goto_0
    return-void

    .line 317
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/dq$d;->g:Z

    .line 318
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dq$d;->dispose()V

    .line 319
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->e:Lio/reactivex/d/a/j;

    iget-object v1, p0, Lio/reactivex/d/e/d/dq$d;->f:Lio/reactivex/b/b;

    invoke-virtual {v0, v1}, Lio/reactivex/d/a/j;->b(Lio/reactivex/b/b;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 303
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dq$d;->g:Z

    if-eqz v0, :cond_0

    .line 304
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 310
    :goto_0
    return-void

    .line 307
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/dq$d;->g:Z

    .line 308
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dq$d;->dispose()V

    .line 309
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->e:Lio/reactivex/d/a/j;

    iget-object v1, p0, Lio/reactivex/d/e/d/dq$d;->f:Lio/reactivex/b/b;

    invoke-virtual {v0, p1, v1}, Lio/reactivex/d/a/j;->a(Ljava/lang/Throwable;Lio/reactivex/b/b;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 269
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dq$d;->g:Z

    if-eqz v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    iget-wide v0, p0, Lio/reactivex/d/e/d/dq$d;->h:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    .line 273
    iput-wide v2, p0, Lio/reactivex/d/e/d/dq$d;->h:J

    .line 275
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->e:Lio/reactivex/d/a/j;

    iget-object v1, p0, Lio/reactivex/d/e/d/dq$d;->f:Lio/reactivex/b/b;

    invoke-virtual {v0, p1, v1}, Lio/reactivex/d/a/j;->a(Ljava/lang/Object;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lio/reactivex/d/e/d/dq$d;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    .line 280
    if-eqz v0, :cond_2

    .line 281
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 287
    :cond_2
    :try_start_0
    iget-object v1, p0, Lio/reactivex/d/e/d/dq$d;->c:Lio/reactivex/c/h;

    invoke-interface {v1, p1}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v4, "The ObservableSource returned is null"

    invoke-static {v1, v4}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/r;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    new-instance v4, Lio/reactivex/d/e/d/dq$b;

    invoke-direct {v4, p0, v2, v3}, Lio/reactivex/d/e/d/dq$b;-><init>(Lio/reactivex/d/e/d/dq$a;J)V

    .line 296
    invoke-virtual {p0, v0, v4}, Lio/reactivex/d/e/d/dq$d;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    invoke-interface {v1, v4}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0

    .line 288
    :catch_0
    move-exception v0

    .line 289
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 290
    iget-object v1, p0, Lio/reactivex/d/e/d/dq$d;->a:Lio/reactivex/t;

    invoke-interface {v1, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 6

    .prologue
    .line 245
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->f:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iput-object p1, p0, Lio/reactivex/d/e/d/dq$d;->f:Lio/reactivex/b/b;

    .line 248
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->e:Lio/reactivex/d/a/j;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/j;->a(Lio/reactivex/b/b;)Z

    .line 250
    iget-object v0, p0, Lio/reactivex/d/e/d/dq$d;->a:Lio/reactivex/t;

    .line 252
    iget-object v1, p0, Lio/reactivex/d/e/d/dq$d;->b:Lio/reactivex/r;

    .line 254
    if-eqz v1, :cond_1

    .line 255
    new-instance v2, Lio/reactivex/d/e/d/dq$b;

    const-wide/16 v4, 0x0

    invoke-direct {v2, p0, v4, v5}, Lio/reactivex/d/e/d/dq$b;-><init>(Lio/reactivex/d/e/d/dq$a;J)V

    .line 257
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v2}, Lio/reactivex/d/e/d/dq$d;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 258
    iget-object v3, p0, Lio/reactivex/d/e/d/dq$d;->e:Lio/reactivex/d/a/j;

    invoke-interface {v0, v3}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 259
    invoke-interface {v1, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iget-object v1, p0, Lio/reactivex/d/e/d/dq$d;->e:Lio/reactivex/d/a/j;

    invoke-interface {v0, v1}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    goto :goto_0
.end method
