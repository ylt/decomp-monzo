.class final Lio/reactivex/d/e/d/ch$b;
.super Ljava/lang/Object;
.source "ObservableRefCount.java"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/ch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lio/reactivex/b/b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/d/e/d/ch;

.field private final b:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lio/reactivex/d/e/d/ch;Lio/reactivex/t;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 183
    iput-object p1, p0, Lio/reactivex/d/e/d/ch$b;->a:Lio/reactivex/d/e/d/ch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p2, p0, Lio/reactivex/d/e/d/ch$b;->b:Lio/reactivex/t;

    .line 185
    iput-object p3, p0, Lio/reactivex/d/e/d/ch$b;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 186
    return-void
.end method


# virtual methods
.method public a(Lio/reactivex/b/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 191
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/d/ch$b;->a:Lio/reactivex/d/e/d/ch;

    iget-object v0, v0, Lio/reactivex/d/e/d/ch;->c:Lio/reactivex/b/a;

    invoke-virtual {v0, p1}, Lio/reactivex/b/a;->a(Lio/reactivex/b/b;)Z

    .line 193
    iget-object v0, p0, Lio/reactivex/d/e/d/ch$b;->a:Lio/reactivex/d/e/d/ch;

    iget-object v1, p0, Lio/reactivex/d/e/d/ch$b;->b:Lio/reactivex/t;

    iget-object v2, p0, Lio/reactivex/d/e/d/ch$b;->a:Lio/reactivex/d/e/d/ch;

    iget-object v2, v2, Lio/reactivex/d/e/d/ch;->c:Lio/reactivex/b/a;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/d/e/d/ch;->a(Lio/reactivex/t;Lio/reactivex/b/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    iget-object v0, p0, Lio/reactivex/d/e/d/ch$b;->a:Lio/reactivex/d/e/d/ch;

    iget-object v0, v0, Lio/reactivex/d/e/d/ch;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 197
    iget-object v0, p0, Lio/reactivex/d/e/d/ch$b;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 199
    return-void

    .line 196
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lio/reactivex/d/e/d/ch$b;->a:Lio/reactivex/d/e/d/ch;

    iget-object v1, v1, Lio/reactivex/d/e/d/ch;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 197
    iget-object v1, p0, Lio/reactivex/d/e/d/ch$b;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 179
    check-cast p1, Lio/reactivex/b/b;

    invoke-virtual {p0, p1}, Lio/reactivex/d/e/d/ch$b;->a(Lio/reactivex/b/b;)V

    return-void
.end method
