.class final Lio/reactivex/d/e/d/bh$c;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableGroupJoin.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/bh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/e/d/bh$b;

.field final b:Z

.field final c:I


# direct methods
.method constructor <init>(Lio/reactivex/d/e/d/bh$b;ZI)V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 443
    iput-object p1, p0, Lio/reactivex/d/e/d/bh$c;->a:Lio/reactivex/d/e/d/bh$b;

    .line 444
    iput-boolean p2, p0, Lio/reactivex/d/e/d/bh$c;->b:Z

    .line 445
    iput p3, p0, Lio/reactivex/d/e/d/bh$c;->c:I

    .line 446
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .prologue
    .line 450
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 451
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 455
    invoke-virtual {p0}, Lio/reactivex/d/e/d/bh$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;)Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Lio/reactivex/d/e/d/bh$c;->a:Lio/reactivex/d/e/d/bh$b;

    iget-boolean v1, p0, Lio/reactivex/d/e/d/bh$c;->b:Z

    invoke-interface {v0, v1, p0}, Lio/reactivex/d/e/d/bh$b;->a(ZLio/reactivex/d/e/d/bh$c;)V

    .line 478
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lio/reactivex/d/e/d/bh$c;->a:Lio/reactivex/d/e/d/bh$b;

    invoke-interface {v0, p1}, Lio/reactivex/d/e/d/bh$b;->b(Ljava/lang/Throwable;)V

    .line 473
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 465
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lio/reactivex/d/e/d/bh$c;->a:Lio/reactivex/d/e/d/bh$b;

    iget-boolean v1, p0, Lio/reactivex/d/e/d/bh$c;->b:Z

    invoke-interface {v0, v1, p0}, Lio/reactivex/d/e/d/bh$b;->a(ZLio/reactivex/d/e/d/bh$c;)V

    .line 468
    :cond_0
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 460
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 461
    return-void
.end method
