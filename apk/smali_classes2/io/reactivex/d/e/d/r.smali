.class public final Lio/reactivex/d/e/d/r;
.super Lio/reactivex/d/e/d/a;
.source "ObservableCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/r$b;,
        Lio/reactivex/d/e/d/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/d/e/d/r$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/d/r$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field final c:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method private constructor <init>(Lio/reactivex/n;Lio/reactivex/d/e/d/r$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/n",
            "<TT;>;",
            "Lio/reactivex/d/e/d/r$a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 68
    iput-object p2, p0, Lio/reactivex/d/e/d/r;->b:Lio/reactivex/d/e/d/r$a;

    .line 69
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/r;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 70
    return-void
.end method

.method public static a(Lio/reactivex/n;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;)",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 44
    const/16 v0, 0x10

    invoke-static {p0, v0}, Lio/reactivex/d/e/d/r;->a(Lio/reactivex/n;I)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/reactivex/n;I)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;I)",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 55
    const-string v0, "capacityHint"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(ILjava/lang/String;)I

    .line 56
    new-instance v0, Lio/reactivex/d/e/d/r$a;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/d/r$a;-><init>(Lio/reactivex/n;I)V

    .line 57
    new-instance v1, Lio/reactivex/d/e/d/r;

    invoke-direct {v1, p0, v0}, Lio/reactivex/d/e/d/r;-><init>(Lio/reactivex/n;Lio/reactivex/d/e/d/r$a;)V

    invoke-static {v1}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lio/reactivex/d/e/d/r$b;

    iget-object v1, p0, Lio/reactivex/d/e/d/r;->b:Lio/reactivex/d/e/d/r$a;

    invoke-direct {v0, p1, v1}, Lio/reactivex/d/e/d/r$b;-><init>(Lio/reactivex/t;Lio/reactivex/d/e/d/r$a;)V

    .line 76
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 78
    iget-object v1, p0, Lio/reactivex/d/e/d/r;->b:Lio/reactivex/d/e/d/r$a;

    invoke-virtual {v1, v0}, Lio/reactivex/d/e/d/r$a;->a(Lio/reactivex/d/e/d/r$b;)Z

    .line 81
    iget-object v1, p0, Lio/reactivex/d/e/d/r;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lio/reactivex/d/e/d/r;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lio/reactivex/d/e/d/r;->b:Lio/reactivex/d/e/d/r$a;

    invoke-virtual {v1}, Lio/reactivex/d/e/d/r$a;->a()V

    .line 85
    :cond_0
    invoke-virtual {v0}, Lio/reactivex/d/e/d/r$b;->a()V

    .line 86
    return-void
.end method
