.class public final Lio/reactivex/d/e/d/dk;
.super Lio/reactivex/d/e/d/a;
.source "ObservableTakeLastTimed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dk$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:J

.field final d:Ljava/util/concurrent/TimeUnit;

.field final e:Lio/reactivex/u;

.field final f:I

.field final g:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/u;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;JJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 35
    iput-wide p2, p0, Lio/reactivex/d/e/d/dk;->b:J

    .line 36
    iput-wide p4, p0, Lio/reactivex/d/e/d/dk;->c:J

    .line 37
    iput-object p6, p0, Lio/reactivex/d/e/d/dk;->d:Ljava/util/concurrent/TimeUnit;

    .line 38
    iput-object p7, p0, Lio/reactivex/d/e/d/dk;->e:Lio/reactivex/u;

    .line 39
    iput p8, p0, Lio/reactivex/d/e/d/dk;->f:I

    .line 40
    iput-boolean p9, p0, Lio/reactivex/d/e/d/dk;->g:Z

    .line 41
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v10, p0, Lio/reactivex/d/e/d/dk;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/dk$a;

    iget-wide v2, p0, Lio/reactivex/d/e/d/dk;->b:J

    iget-wide v4, p0, Lio/reactivex/d/e/d/dk;->c:J

    iget-object v6, p0, Lio/reactivex/d/e/d/dk;->d:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p0, Lio/reactivex/d/e/d/dk;->e:Lio/reactivex/u;

    iget v8, p0, Lio/reactivex/d/e/d/dk;->f:I

    iget-boolean v9, p0, Lio/reactivex/d/e/d/dk;->g:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, Lio/reactivex/d/e/d/dk$a;-><init>(Lio/reactivex/t;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/u;IZ)V

    invoke-interface {v10, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 46
    return-void
.end method
