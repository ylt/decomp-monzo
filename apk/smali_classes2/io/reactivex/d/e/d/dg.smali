.class public final Lio/reactivex/d/e/d/dg;
.super Lio/reactivex/d/e/d/a;
.source "ObservableSwitchMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dg$a;,
        Lio/reactivex/d/e/d/dg$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TR;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;"
        }
    .end annotation
.end field

.field final c:I

.field final d:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/h;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;IZ)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 38
    iput-object p2, p0, Lio/reactivex/d/e/d/dg;->b:Lio/reactivex/c/h;

    .line 39
    iput p3, p0, Lio/reactivex/d/e/d/dg;->c:I

    .line 40
    iput-boolean p4, p0, Lio/reactivex/d/e/d/dg;->d:Z

    .line 41
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TR;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lio/reactivex/d/e/d/dg;->a:Lio/reactivex/r;

    iget-object v1, p0, Lio/reactivex/d/e/d/dg;->b:Lio/reactivex/c/h;

    invoke-static {v0, p1, v1}, Lio/reactivex/d/e/d/cr;->a(Lio/reactivex/r;Lio/reactivex/t;Lio/reactivex/c/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/dg;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/dg$b;

    iget-object v2, p0, Lio/reactivex/d/e/d/dg;->b:Lio/reactivex/c/h;

    iget v3, p0, Lio/reactivex/d/e/d/dg;->c:I

    iget-boolean v4, p0, Lio/reactivex/d/e/d/dg;->d:Z

    invoke-direct {v1, p1, v2, v3, v4}, Lio/reactivex/d/e/d/dg$b;-><init>(Lio/reactivex/t;Lio/reactivex/c/h;IZ)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method
