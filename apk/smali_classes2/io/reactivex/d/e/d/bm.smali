.class public final Lio/reactivex/d/e/d/bm;
.super Lio/reactivex/n;
.source "ObservableInterval.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/bm$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/n",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/u;

.field final b:J

.field final c:J

.field final d:Ljava/util/concurrent/TimeUnit;


# direct methods
.method public constructor <init>(JJLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 32
    iput-wide p1, p0, Lio/reactivex/d/e/d/bm;->b:J

    .line 33
    iput-wide p3, p0, Lio/reactivex/d/e/d/bm;->c:J

    .line 34
    iput-object p5, p0, Lio/reactivex/d/e/d/bm;->d:Ljava/util/concurrent/TimeUnit;

    .line 35
    iput-object p6, p0, Lio/reactivex/d/e/d/bm;->a:Lio/reactivex/u;

    .line 36
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v1, Lio/reactivex/d/e/d/bm$a;

    invoke-direct {v1, p1}, Lio/reactivex/d/e/d/bm$a;-><init>(Lio/reactivex/t;)V

    .line 41
    invoke-interface {p1, v1}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 43
    iget-object v0, p0, Lio/reactivex/d/e/d/bm;->a:Lio/reactivex/u;

    .line 45
    instance-of v2, v0, Lio/reactivex/d/g/n;

    if-eqz v2, :cond_0

    .line 46
    invoke-virtual {v0}, Lio/reactivex/u;->a()Lio/reactivex/u$c;

    move-result-object v0

    .line 47
    invoke-virtual {v1, v0}, Lio/reactivex/d/e/d/bm$a;->a(Lio/reactivex/b/b;)V

    .line 48
    iget-wide v2, p0, Lio/reactivex/d/e/d/bm;->b:J

    iget-wide v4, p0, Lio/reactivex/d/e/d/bm;->c:J

    iget-object v6, p0, Lio/reactivex/d/e/d/bm;->d:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Lio/reactivex/u$c;->a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/b;

    .line 53
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-wide v2, p0, Lio/reactivex/d/e/d/bm;->b:J

    iget-wide v4, p0, Lio/reactivex/d/e/d/bm;->c:J

    iget-object v6, p0, Lio/reactivex/d/e/d/bm;->d:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Lio/reactivex/u;->a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/b;

    move-result-object v0

    .line 51
    invoke-virtual {v1, v0}, Lio/reactivex/d/e/d/bm$a;->a(Lio/reactivex/b/b;)V

    goto :goto_0
.end method
