.class public final Lio/reactivex/d/e/d/ck;
.super Lio/reactivex/d/e/d/a;
.source "ObservableRepeatWhen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/ck$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;+",
            "Lio/reactivex/r",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;+",
            "Lio/reactivex/r",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 38
    iput-object p2, p0, Lio/reactivex/d/e/d/ck;->b:Lio/reactivex/c/h;

    .line 39
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {}, Lio/reactivex/i/a;->a()Lio/reactivex/i/a;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/i/a;->b()Lio/reactivex/i/c;

    move-result-object v1

    .line 48
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/d/ck;->b:Lio/reactivex/c/h;

    invoke-interface {v0, v1}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "The handler returned a null ObservableSource"

    invoke-static {v0, v2}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    new-instance v2, Lio/reactivex/d/e/d/ck$a;

    iget-object v3, p0, Lio/reactivex/d/e/d/ck;->a:Lio/reactivex/r;

    invoke-direct {v2, p1, v1, v3}, Lio/reactivex/d/e/d/ck$a;-><init>(Lio/reactivex/t;Lio/reactivex/i/c;Lio/reactivex/r;)V

    .line 56
    invoke-interface {p1, v2}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 58
    iget-object v1, v2, Lio/reactivex/d/e/d/ck$a;->e:Lio/reactivex/d/e/d/ck$a$a;

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 60
    invoke-virtual {v2}, Lio/reactivex/d/e/d/ck$a;->c()V

    .line 61
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 51
    invoke-static {v0, p1}, Lio/reactivex/d/a/e;->a(Ljava/lang/Throwable;Lio/reactivex/t;)V

    goto :goto_0
.end method
