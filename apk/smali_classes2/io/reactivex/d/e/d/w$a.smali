.class final Lio/reactivex/d/e/d/w$a;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableConcatMapEager.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/d/d/r;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/d/d/r",
        "<TR;>;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TR;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;"
        }
    .end annotation
.end field

.field final c:I

.field final d:I

.field final e:Lio/reactivex/d/j/i;

.field final f:Lio/reactivex/d/j/c;

.field final g:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lio/reactivex/d/d/q",
            "<TR;>;>;"
        }
    .end annotation
.end field

.field h:Lio/reactivex/d/c/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/c/i",
            "<TT;>;"
        }
    .end annotation
.end field

.field i:Lio/reactivex/b/b;

.field volatile j:Z

.field k:I

.field volatile l:Z

.field m:Lio/reactivex/d/d/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/d/q",
            "<TR;>;"
        }
    .end annotation
.end field

.field n:I


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/c/h;IILio/reactivex/d/j/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TR;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;II",
            "Lio/reactivex/d/j/i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 93
    iput-object p1, p0, Lio/reactivex/d/e/d/w$a;->a:Lio/reactivex/t;

    .line 94
    iput-object p2, p0, Lio/reactivex/d/e/d/w$a;->b:Lio/reactivex/c/h;

    .line 95
    iput p3, p0, Lio/reactivex/d/e/d/w$a;->c:I

    .line 96
    iput p4, p0, Lio/reactivex/d/e/d/w$a;->d:I

    .line 97
    iput-object p5, p0, Lio/reactivex/d/e/d/w$a;->e:Lio/reactivex/d/j/i;

    .line 98
    new-instance v0, Lio/reactivex/d/j/c;

    invoke-direct {v0}, Lio/reactivex/d/j/c;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    .line 99
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/w$a;->g:Ljava/util/ArrayDeque;

    .line 100
    return-void
.end method


# virtual methods
.method public a()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 222
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->getAndIncrement()I

    move-result v0

    if-eqz v0, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    iget-object v5, p0, Lio/reactivex/d/e/d/w$a;->h:Lio/reactivex/d/c/i;

    .line 229
    iget-object v6, p0, Lio/reactivex/d/e/d/w$a;->g:Ljava/util/ArrayDeque;

    .line 230
    iget-object v7, p0, Lio/reactivex/d/e/d/w$a;->a:Lio/reactivex/t;

    .line 231
    iget-object v8, p0, Lio/reactivex/d/e/d/w$a;->e:Lio/reactivex/d/j/i;

    move v1, v2

    .line 236
    :goto_1
    iget v0, p0, Lio/reactivex/d/e/d/w$a;->n:I

    move v3, v0

    .line 238
    :goto_2
    iget v0, p0, Lio/reactivex/d/e/d/w$a;->c:I

    if-eq v3, v0, :cond_4

    .line 239
    iget-boolean v0, p0, Lio/reactivex/d/e/d/w$a;->l:Z

    if-eqz v0, :cond_2

    .line 240
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 241
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    goto :goto_0

    .line 245
    :cond_2
    sget-object v0, Lio/reactivex/d/j/i;->a:Lio/reactivex/d/j/i;

    if-ne v8, v0, :cond_3

    .line 246
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 247
    if-eqz v0, :cond_3

    .line 248
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 249
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    .line 251
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v7, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 260
    :cond_3
    :try_start_0
    invoke-interface {v5}, Lio/reactivex/d/c/i;->n_()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 262
    if-nez v0, :cond_5

    .line 286
    :cond_4
    iput v3, p0, Lio/reactivex/d/e/d/w$a;->n:I

    .line 288
    iget-boolean v0, p0, Lio/reactivex/d/e/d/w$a;->l:Z

    if-eqz v0, :cond_6

    .line 289
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 290
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    goto :goto_0

    .line 266
    :cond_5
    :try_start_1
    iget-object v9, p0, Lio/reactivex/d/e/d/w$a;->b:Lio/reactivex/c/h;

    invoke-interface {v9, v0}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v9, "The mapper returned a null ObservableSource"

    invoke-static {v0, v9}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 277
    new-instance v9, Lio/reactivex/d/d/q;

    iget v10, p0, Lio/reactivex/d/e/d/w$a;->d:I

    invoke-direct {v9, p0, v10}, Lio/reactivex/d/d/q;-><init>(Lio/reactivex/d/d/r;I)V

    .line 279
    invoke-virtual {v6, v9}, Ljava/util/ArrayDeque;->offer(Ljava/lang/Object;)Z

    .line 281
    invoke-interface {v0, v9}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 283
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 284
    goto :goto_2

    .line 267
    :catch_0
    move-exception v0

    .line 268
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 269
    iget-object v1, p0, Lio/reactivex/d/e/d/w$a;->i:Lio/reactivex/b/b;

    invoke-interface {v1}, Lio/reactivex/b/b;->dispose()V

    .line 270
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 271
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    .line 272
    iget-object v1, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v1, v0}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    .line 273
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v7, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 294
    :cond_6
    sget-object v0, Lio/reactivex/d/j/i;->a:Lio/reactivex/d/j/i;

    if-ne v8, v0, :cond_7

    .line 295
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 296
    if-eqz v0, :cond_7

    .line 297
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 298
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    .line 300
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v7, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 305
    :cond_7
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->m:Lio/reactivex/d/d/q;

    .line 307
    if-nez v0, :cond_c

    .line 308
    sget-object v0, Lio/reactivex/d/j/i;->b:Lio/reactivex/d/j/i;

    if-ne v8, v0, :cond_8

    .line 309
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 310
    if-eqz v0, :cond_8

    .line 311
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 312
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    .line 314
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v7, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 318
    :cond_8
    iget-boolean v9, p0, Lio/reactivex/d/e/d/w$a;->j:Z

    .line 320
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/d/q;

    .line 322
    if-nez v0, :cond_9

    move v3, v2

    .line 324
    :goto_3
    if-eqz v9, :cond_b

    if-eqz v3, :cond_b

    .line 325
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 326
    if-eqz v0, :cond_a

    .line 327
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 328
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    .line 330
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v7, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_9
    move v3, v4

    .line 322
    goto :goto_3

    .line 332
    :cond_a
    invoke-interface {v7}, Lio/reactivex/t;->onComplete()V

    goto/16 :goto_0

    .line 337
    :cond_b
    if-nez v3, :cond_c

    .line 338
    iput-object v0, p0, Lio/reactivex/d/e/d/w$a;->m:Lio/reactivex/d/d/q;

    :cond_c
    move-object v3, v0

    .line 343
    if-eqz v3, :cond_11

    .line 344
    invoke-virtual {v3}, Lio/reactivex/d/d/q;->c()Lio/reactivex/d/c/i;

    move-result-object v9

    .line 347
    :goto_4
    iget-boolean v0, p0, Lio/reactivex/d/e/d/w$a;->l:Z

    if-eqz v0, :cond_d

    .line 348
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 349
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    goto/16 :goto_0

    .line 353
    :cond_d
    invoke-virtual {v3}, Lio/reactivex/d/d/q;->a()Z

    move-result v10

    .line 355
    sget-object v0, Lio/reactivex/d/j/i;->a:Lio/reactivex/d/j/i;

    if-ne v8, v0, :cond_e

    .line 356
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 357
    if-eqz v0, :cond_e

    .line 358
    invoke-interface {v5}, Lio/reactivex/d/c/i;->c()V

    .line 359
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    .line 361
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v7, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 369
    :cond_e
    :try_start_2
    invoke-interface {v9}, Lio/reactivex/d/c/i;->n_()Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v11

    .line 379
    if-nez v11, :cond_f

    move v0, v2

    .line 381
    :goto_5
    if-eqz v10, :cond_10

    if-eqz v0, :cond_10

    .line 382
    iput-object v12, p0, Lio/reactivex/d/e/d/w$a;->m:Lio/reactivex/d/d/q;

    .line 383
    iget v0, p0, Lio/reactivex/d/e/d/w$a;->n:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lio/reactivex/d/e/d/w$a;->n:I

    goto/16 :goto_1

    .line 370
    :catch_1
    move-exception v0

    .line 371
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 372
    iget-object v3, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v3, v0}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    .line 374
    iput-object v12, p0, Lio/reactivex/d/e/d/w$a;->m:Lio/reactivex/d/d/q;

    .line 375
    iget v0, p0, Lio/reactivex/d/e/d/w$a;->n:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lio/reactivex/d/e/d/w$a;->n:I

    goto/16 :goto_1

    :cond_f
    move v0, v4

    .line 379
    goto :goto_5

    .line 387
    :cond_10
    if-eqz v0, :cond_12

    .line 395
    :cond_11
    neg-int v0, v1

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/d/w$a;->addAndGet(I)I

    move-result v0

    .line 396
    if-eqz v0, :cond_0

    move v1, v0

    .line 399
    goto/16 :goto_1

    .line 391
    :cond_12
    invoke-interface {v7, v11}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    goto :goto_4
.end method

.method public a(Lio/reactivex/d/d/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/d/q",
            "<TR;>;)V"
        }
    .end annotation

    .prologue
    .line 216
    invoke-virtual {p1}, Lio/reactivex/d/d/q;->b()V

    .line 217
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->a()V

    .line 218
    return-void
.end method

.method public a(Lio/reactivex/d/d/q;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/d/q",
            "<TR;>;TR;)V"
        }
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p1}, Lio/reactivex/d/d/q;->c()Lio/reactivex/d/c/i;

    move-result-object v0

    invoke-interface {v0, p2}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    .line 198
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->a()V

    .line 199
    return-void
.end method

.method public a(Lio/reactivex/d/d/q;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/d/q",
            "<TR;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0, p2}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->e:Lio/reactivex/d/j/i;

    sget-object v1, Lio/reactivex/d/j/i;->a:Lio/reactivex/d/j/i;

    if-ne v0, v1, :cond_0

    .line 205
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->i:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 207
    :cond_0
    invoke-virtual {p1}, Lio/reactivex/d/d/q;->b()V

    .line 208
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->a()V

    .line 212
    :goto_0
    return-void

    .line 210
    :cond_1
    invoke-static {p2}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->m:Lio/reactivex/d/d/q;

    .line 179
    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {v0}, Lio/reactivex/d/d/q;->dispose()V

    .line 185
    :cond_0
    :goto_0
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->g:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/d/q;

    .line 187
    if-nez v0, :cond_1

    .line 188
    return-void

    .line 191
    :cond_1
    invoke-virtual {v0}, Lio/reactivex/d/d/q;->dispose()V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/w$a;->l:Z

    .line 165
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->h:Lio/reactivex/d/c/i;

    invoke-interface {v0}, Lio/reactivex/d/c/i;->c()V

    .line 167
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->b()V

    .line 169
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lio/reactivex/d/e/d/w$a;->l:Z

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/w$a;->j:Z

    .line 159
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->a()V

    .line 160
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->f:Lio/reactivex/d/j/c;

    invoke-virtual {v0, p1}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/w$a;->j:Z

    .line 150
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->a()V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 140
    iget v0, p0, Lio/reactivex/d/e/d/w$a;->k:I

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->h:Lio/reactivex/d/c/i;

    invoke-interface {v0, p1}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    .line 143
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->a()V

    .line 144
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 105
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->i:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iput-object p1, p0, Lio/reactivex/d/e/d/w$a;->i:Lio/reactivex/b/b;

    .line 108
    instance-of v0, p1, Lio/reactivex/d/c/d;

    if-eqz v0, :cond_2

    .line 109
    check-cast p1, Lio/reactivex/d/c/d;

    .line 111
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Lio/reactivex/d/c/d;->a(I)I

    move-result v0

    .line 112
    if-ne v0, v1, :cond_1

    .line 113
    iput v0, p0, Lio/reactivex/d/e/d/w$a;->k:I

    .line 114
    iput-object p1, p0, Lio/reactivex/d/e/d/w$a;->h:Lio/reactivex/d/c/i;

    .line 115
    iput-boolean v1, p0, Lio/reactivex/d/e/d/w$a;->j:Z

    .line 117
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 119
    invoke-virtual {p0}, Lio/reactivex/d/e/d/w$a;->a()V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 123
    iput v0, p0, Lio/reactivex/d/e/d/w$a;->k:I

    .line 124
    iput-object p1, p0, Lio/reactivex/d/e/d/w$a;->h:Lio/reactivex/d/c/i;

    .line 126
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    goto :goto_0

    .line 132
    :cond_2
    iget v0, p0, Lio/reactivex/d/e/d/w$a;->d:I

    invoke-static {v0}, Lio/reactivex/d/j/r;->a(I)Lio/reactivex/d/c/i;

    move-result-object v0

    iput-object v0, p0, Lio/reactivex/d/e/d/w$a;->h:Lio/reactivex/d/c/i;

    .line 134
    iget-object v0, p0, Lio/reactivex/d/e/d/w$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    goto :goto_0
.end method
