.class public final Lio/reactivex/d/e/d/ds;
.super Lio/reactivex/n;
.source "ObservableTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/ds$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/n",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/u;

.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 28
    iput-wide p1, p0, Lio/reactivex/d/e/d/ds;->b:J

    .line 29
    iput-object p3, p0, Lio/reactivex/d/e/d/ds;->c:Ljava/util/concurrent/TimeUnit;

    .line 30
    iput-object p4, p0, Lio/reactivex/d/e/d/ds;->a:Lio/reactivex/u;

    .line 31
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lio/reactivex/d/e/d/ds$a;

    invoke-direct {v0, p1}, Lio/reactivex/d/e/d/ds$a;-><init>(Lio/reactivex/t;)V

    .line 36
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 38
    iget-object v1, p0, Lio/reactivex/d/e/d/ds;->a:Lio/reactivex/u;

    iget-wide v2, p0, Lio/reactivex/d/e/d/ds;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/ds;->c:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v0, v2, v3, v4}, Lio/reactivex/u;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex/b/b;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/d/e/d/ds$a;->a(Lio/reactivex/b/b;)V

    .line 41
    return-void
.end method
