.class final Lio/reactivex/d/e/d/bh$d;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableGroupJoin.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/bh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/e/d/bh$b;

.field final b:Z


# direct methods
.method constructor <init>(Lio/reactivex/d/e/d/bh$b;Z)V
    .locals 0

    .prologue
    .line 392
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 393
    iput-object p1, p0, Lio/reactivex/d/e/d/bh$d;->a:Lio/reactivex/d/e/d/bh$b;

    .line 394
    iput-boolean p2, p0, Lio/reactivex/d/e/d/bh$d;->b:Z

    .line 395
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .prologue
    .line 399
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 400
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0}, Lio/reactivex/d/e/d/bh$d;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;)Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lio/reactivex/d/e/d/bh$d;->a:Lio/reactivex/d/e/d/bh$b;

    invoke-interface {v0, p0}, Lio/reactivex/d/e/d/bh$b;->a(Lio/reactivex/d/e/d/bh$d;)V

    .line 425
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lio/reactivex/d/e/d/bh$d;->a:Lio/reactivex/d/e/d/bh$b;

    invoke-interface {v0, p1}, Lio/reactivex/d/e/d/bh$b;->a(Ljava/lang/Throwable;)V

    .line 420
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Lio/reactivex/d/e/d/bh$d;->a:Lio/reactivex/d/e/d/bh$b;

    iget-boolean v1, p0, Lio/reactivex/d/e/d/bh$d;->b:Z

    invoke-interface {v0, v1, p1}, Lio/reactivex/d/e/d/bh$b;->a(ZLjava/lang/Object;)V

    .line 415
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 409
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 410
    return-void
.end method
