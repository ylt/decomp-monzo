.class final Lio/reactivex/d/e/d/cl$m;
.super Lio/reactivex/d/e/d/cl$a;
.source "ObservableReplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/cl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "m"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/cl$a",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final c:Lio/reactivex/u;

.field final d:J

.field final e:Ljava/util/concurrent/TimeUnit;

.field final f:I


# direct methods
.method constructor <init>(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V
    .locals 0

    .prologue
    .line 804
    invoke-direct {p0}, Lio/reactivex/d/e/d/cl$a;-><init>()V

    .line 805
    iput-object p5, p0, Lio/reactivex/d/e/d/cl$m;->c:Lio/reactivex/u;

    .line 806
    iput p1, p0, Lio/reactivex/d/e/d/cl$m;->f:I

    .line 807
    iput-wide p2, p0, Lio/reactivex/d/e/d/cl$m;->d:J

    .line 808
    iput-object p4, p0, Lio/reactivex/d/e/d/cl$m;->e:Ljava/util/concurrent/TimeUnit;

    .line 809
    return-void
.end method


# virtual methods
.method b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 813
    new-instance v0, Lio/reactivex/h/b;

    iget-object v1, p0, Lio/reactivex/d/e/d/cl$m;->c:Lio/reactivex/u;

    iget-object v2, p0, Lio/reactivex/d/e/d/cl$m;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2}, Lio/reactivex/u;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    iget-object v1, p0, Lio/reactivex/d/e/d/cl$m;->e:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, p1, v2, v3, v1}, Lio/reactivex/h/b;-><init>(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 818
    check-cast p1, Lio/reactivex/h/b;

    invoke-virtual {p1}, Lio/reactivex/h/b;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method c()V
    .locals 9

    .prologue
    .line 823
    iget-object v0, p0, Lio/reactivex/d/e/d/cl$m;->c:Lio/reactivex/u;

    iget-object v1, p0, Lio/reactivex/d/e/d/cl$m;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lio/reactivex/u;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iget-wide v2, p0, Lio/reactivex/d/e/d/cl$m;->d:J

    sub-long v4, v0, v2

    .line 825
    invoke-virtual {p0}, Lio/reactivex/d/e/d/cl$m;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/cl$f;

    .line 826
    invoke-virtual {v0}, Lio/reactivex/d/e/d/cl$f;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/d/e/d/cl$f;

    .line 828
    const/4 v2, 0x0

    move-object v3, v0

    move-object v8, v1

    move v1, v2

    move-object v2, v8

    .line 830
    :goto_0
    if-eqz v2, :cond_1

    .line 831
    iget v0, p0, Lio/reactivex/d/e/d/cl$m;->b:I

    iget v6, p0, Lio/reactivex/d/e/d/cl$m;->f:I

    if-le v0, v6, :cond_0

    .line 832
    add-int/lit8 v1, v1, 0x1

    .line 833
    iget v0, p0, Lio/reactivex/d/e/d/cl$m;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lio/reactivex/d/e/d/cl$m;->b:I

    .line 835
    invoke-virtual {v2}, Lio/reactivex/d/e/d/cl$f;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/cl$f;

    move-object v3, v2

    move-object v2, v0

    goto :goto_0

    .line 837
    :cond_0
    iget-object v0, v2, Lio/reactivex/d/e/d/cl$f;->a:Ljava/lang/Object;

    check-cast v0, Lio/reactivex/h/b;

    .line 838
    invoke-virtual {v0}, Lio/reactivex/h/b;->b()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-gtz v0, :cond_1

    .line 839
    add-int/lit8 v1, v1, 0x1

    .line 840
    iget v0, p0, Lio/reactivex/d/e/d/cl$m;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lio/reactivex/d/e/d/cl$m;->b:I

    .line 842
    invoke-virtual {v2}, Lio/reactivex/d/e/d/cl$f;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/cl$f;

    move-object v3, v2

    move-object v2, v0

    .line 846
    goto :goto_0

    .line 851
    :cond_1
    if-eqz v1, :cond_2

    .line 852
    invoke-virtual {p0, v3}, Lio/reactivex/d/e/d/cl$m;->b(Lio/reactivex/d/e/d/cl$f;)V

    .line 854
    :cond_2
    return-void
.end method

.method d()V
    .locals 9

    .prologue
    .line 857
    iget-object v0, p0, Lio/reactivex/d/e/d/cl$m;->c:Lio/reactivex/u;

    iget-object v1, p0, Lio/reactivex/d/e/d/cl$m;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lio/reactivex/u;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iget-wide v2, p0, Lio/reactivex/d/e/d/cl$m;->d:J

    sub-long v4, v0, v2

    .line 859
    invoke-virtual {p0}, Lio/reactivex/d/e/d/cl$m;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/cl$f;

    .line 860
    invoke-virtual {v0}, Lio/reactivex/d/e/d/cl$f;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/d/e/d/cl$f;

    .line 862
    const/4 v2, 0x0

    move-object v3, v0

    move-object v8, v1

    move v1, v2

    move-object v2, v8

    .line 864
    :goto_0
    if-eqz v2, :cond_0

    iget v0, p0, Lio/reactivex/d/e/d/cl$m;->b:I

    const/4 v6, 0x1

    if-le v0, v6, :cond_0

    .line 865
    iget-object v0, v2, Lio/reactivex/d/e/d/cl$f;->a:Ljava/lang/Object;

    check-cast v0, Lio/reactivex/h/b;

    .line 866
    invoke-virtual {v0}, Lio/reactivex/h/b;->b()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-gtz v0, :cond_0

    .line 867
    add-int/lit8 v1, v1, 0x1

    .line 868
    iget v0, p0, Lio/reactivex/d/e/d/cl$m;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lio/reactivex/d/e/d/cl$m;->b:I

    .line 870
    invoke-virtual {v2}, Lio/reactivex/d/e/d/cl$f;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/cl$f;

    move-object v3, v2

    move-object v2, v0

    .line 874
    goto :goto_0

    .line 878
    :cond_0
    if-eqz v1, :cond_1

    .line 879
    invoke-virtual {p0, v3}, Lio/reactivex/d/e/d/cl$m;->b(Lio/reactivex/d/e/d/cl$f;)V

    .line 881
    :cond_1
    return-void
.end method

.method e()Lio/reactivex/d/e/d/cl$f;
    .locals 8

    .prologue
    .line 885
    iget-object v0, p0, Lio/reactivex/d/e/d/cl$m;->c:Lio/reactivex/u;

    iget-object v1, p0, Lio/reactivex/d/e/d/cl$m;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lio/reactivex/u;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iget-wide v2, p0, Lio/reactivex/d/e/d/cl$m;->d:J

    sub-long v4, v0, v2

    .line 886
    invoke-virtual {p0}, Lio/reactivex/d/e/d/cl$m;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/cl$f;

    .line 887
    invoke-virtual {v0}, Lio/reactivex/d/e/d/cl$f;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/d/e/d/cl$f;

    move-object v2, v0

    .line 889
    :goto_0
    if-nez v1, :cond_1

    .line 903
    :cond_0
    return-object v2

    .line 892
    :cond_1
    iget-object v0, v1, Lio/reactivex/d/e/d/cl$f;->a:Ljava/lang/Object;

    check-cast v0, Lio/reactivex/h/b;

    .line 893
    invoke-virtual {v0}, Lio/reactivex/h/b;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lio/reactivex/d/j/n;->b(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lio/reactivex/h/b;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lio/reactivex/d/j/n;->c(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 896
    invoke-virtual {v0}, Lio/reactivex/h/b;->b()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-gtz v0, :cond_0

    .line 898
    invoke-virtual {v1}, Lio/reactivex/d/e/d/cl$f;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/e/d/cl$f;

    move-object v2, v1

    move-object v1, v0

    .line 902
    goto :goto_0
.end method
