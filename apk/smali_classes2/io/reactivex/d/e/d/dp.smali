.class public final Lio/reactivex/d/e/d/dp;
.super Lio/reactivex/d/e/d/a;
.source "ObservableTimeInterval.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dp$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;",
        "Lio/reactivex/h/b",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/u;

.field final c:Ljava/util/concurrent/TimeUnit;


# direct methods
.method public constructor <init>(Lio/reactivex/r;Ljava/util/concurrent/TimeUnit;Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 29
    iput-object p3, p0, Lio/reactivex/d/e/d/dp;->b:Lio/reactivex/u;

    .line 30
    iput-object p2, p0, Lio/reactivex/d/e/d/dp;->c:Ljava/util/concurrent/TimeUnit;

    .line 31
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Lio/reactivex/h/b",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lio/reactivex/d/e/d/dp;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/dp$a;

    iget-object v2, p0, Lio/reactivex/d/e/d/dp;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lio/reactivex/d/e/d/dp;->b:Lio/reactivex/u;

    invoke-direct {v1, p1, v2, v3}, Lio/reactivex/d/e/d/dp$a;-><init>(Lio/reactivex/t;Ljava/util/concurrent/TimeUnit;Lio/reactivex/u;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 36
    return-void
.end method
