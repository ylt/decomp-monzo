.class public final Lio/reactivex/d/e/d/bl;
.super Ljava/lang/Object;
.source "ObservableInternalHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/bl$l;,
        Lio/reactivex/d/e/d/bl$o;,
        Lio/reactivex/d/e/d/bl$b;,
        Lio/reactivex/d/e/d/bl$a;,
        Lio/reactivex/d/e/d/bl$k;,
        Lio/reactivex/d/e/d/bl$g;,
        Lio/reactivex/d/e/d/bl$p;,
        Lio/reactivex/d/e/d/bl$c;,
        Lio/reactivex/d/e/d/bl$e;,
        Lio/reactivex/d/e/d/bl$d;,
        Lio/reactivex/d/e/d/bl$h;,
        Lio/reactivex/d/e/d/bl$i;,
        Lio/reactivex/d/e/d/bl$j;,
        Lio/reactivex/d/e/d/bl$f;,
        Lio/reactivex/d/e/d/bl$m;,
        Lio/reactivex/d/e/d/bl$n;
    }
.end annotation


# direct methods
.method public static a(Lio/reactivex/c/b;)Lio/reactivex/c/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/b",
            "<TS;",
            "Lio/reactivex/e",
            "<TT;>;>;)",
            "Lio/reactivex/c/c",
            "<TS;",
            "Lio/reactivex/e",
            "<TT;>;TS;>;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lio/reactivex/d/e/d/bl$m;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$m;-><init>(Lio/reactivex/c/b;)V

    return-object v0
.end method

.method public static a(Lio/reactivex/c/g;)Lio/reactivex/c/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/g",
            "<",
            "Lio/reactivex/e",
            "<TT;>;>;)",
            "Lio/reactivex/c/c",
            "<TS;",
            "Lio/reactivex/e",
            "<TT;>;TS;>;"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lio/reactivex/d/e/d/bl$n;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$n;-><init>(Lio/reactivex/c/g;)V

    return-object v0
.end method

.method public static a(Lio/reactivex/t;)Lio/reactivex/c/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/t",
            "<TT;>;)",
            "Lio/reactivex/c/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 130
    new-instance v0, Lio/reactivex/d/e/d/bl$j;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$j;-><init>(Lio/reactivex/t;)V

    return-object v0
.end method

.method public static a(Lio/reactivex/c/h;)Lio/reactivex/c/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<TU;>;>;)",
            "Lio/reactivex/c/h",
            "<TT;",
            "Lio/reactivex/r",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lio/reactivex/d/e/d/bl$f;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$f;-><init>(Lio/reactivex/c/h;)V

    return-object v0
.end method

.method public static a(Lio/reactivex/c/h;Lio/reactivex/c/c;)Lio/reactivex/c/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TU;>;>;",
            "Lio/reactivex/c/c",
            "<-TT;-TU;+TR;>;)",
            "Lio/reactivex/c/h",
            "<TT;",
            "Lio/reactivex/r",
            "<TR;>;>;"
        }
    .end annotation

    .prologue
    .line 177
    new-instance v0, Lio/reactivex/d/e/d/bl$e;

    invoke-direct {v0, p1, p0}, Lio/reactivex/d/e/d/bl$e;-><init>(Lio/reactivex/c/c;Lio/reactivex/c/h;)V

    return-object v0
.end method

.method public static a(Lio/reactivex/c/h;Lio/reactivex/u;)Lio/reactivex/c/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;+",
            "Lio/reactivex/r",
            "<TR;>;>;",
            "Lio/reactivex/u;",
            ")",
            "Lio/reactivex/c/h",
            "<",
            "Lio/reactivex/n",
            "<TT;>;",
            "Lio/reactivex/r",
            "<TR;>;>;"
        }
    .end annotation

    .prologue
    .line 240
    new-instance v0, Lio/reactivex/d/e/d/bl$l;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/d/bl$l;-><init>(Lio/reactivex/c/h;Lio/reactivex/u;)V

    return-object v0
.end method

.method public static a(Lio/reactivex/n;Lio/reactivex/c/h;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/z",
            "<+TR;>;>;)",
            "Lio/reactivex/n",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 298
    invoke-static {p1}, Lio/reactivex/d/e/d/bl;->d(Lio/reactivex/c/h;)Lio/reactivex/c/h;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;I)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/reactivex/n;)Ljava/util/concurrent/Callable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/e/a",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 224
    new-instance v0, Lio/reactivex/d/e/d/bl$k;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$k;-><init>(Lio/reactivex/n;)V

    return-object v0
.end method

.method public static a(Lio/reactivex/n;I)Ljava/util/concurrent/Callable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;I)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/e/a",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 228
    new-instance v0, Lio/reactivex/d/e/d/bl$a;

    invoke-direct {v0, p0, p1}, Lio/reactivex/d/e/d/bl$a;-><init>(Lio/reactivex/n;I)V

    return-object v0
.end method

.method public static a(Lio/reactivex/n;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)Ljava/util/concurrent/Callable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/e/a",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 232
    new-instance v1, Lio/reactivex/d/e/d/bl$b;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lio/reactivex/d/e/d/bl$b;-><init>(Lio/reactivex/n;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V

    return-object v1
.end method

.method public static a(Lio/reactivex/n;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)Ljava/util/concurrent/Callable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lio/reactivex/e/a",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 236
    new-instance v0, Lio/reactivex/d/e/d/bl$o;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/bl$o;-><init>(Lio/reactivex/n;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V

    return-object v0
.end method

.method public static b(Lio/reactivex/t;)Lio/reactivex/c/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/t",
            "<TT;>;)",
            "Lio/reactivex/c/g",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lio/reactivex/d/e/d/bl$i;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$i;-><init>(Lio/reactivex/t;)V

    return-object v0
.end method

.method public static b(Lio/reactivex/c/h;)Lio/reactivex/c/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Ljava/lang/Iterable",
            "<+TU;>;>;)",
            "Lio/reactivex/c/h",
            "<TT;",
            "Lio/reactivex/r",
            "<TU;>;>;"
        }
    .end annotation

    .prologue
    .line 194
    new-instance v0, Lio/reactivex/d/e/d/bl$c;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$c;-><init>(Lio/reactivex/c/h;)V

    return-object v0
.end method

.method public static b(Lio/reactivex/n;Lio/reactivex/c/h;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/n",
            "<TT;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/z",
            "<+TR;>;>;)",
            "Lio/reactivex/n",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 303
    invoke-static {p1}, Lio/reactivex/d/e/d/bl;->d(Lio/reactivex/c/h;)Lio/reactivex/c/h;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lio/reactivex/n;->switchMapDelayError(Lio/reactivex/c/h;I)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lio/reactivex/t;)Lio/reactivex/c/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/t",
            "<TT;>;)",
            "Lio/reactivex/c/a;"
        }
    .end annotation

    .prologue
    .line 138
    new-instance v0, Lio/reactivex/d/e/d/bl$h;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$h;-><init>(Lio/reactivex/t;)V

    return-object v0
.end method

.method public static c(Lio/reactivex/c/h;)Lio/reactivex/c/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-[",
            "Ljava/lang/Object;",
            "+TR;>;)",
            "Lio/reactivex/c/h",
            "<",
            "Ljava/util/List",
            "<",
            "Lio/reactivex/r",
            "<+TT;>;>;",
            "Lio/reactivex/r",
            "<+TR;>;>;"
        }
    .end annotation

    .prologue
    .line 294
    new-instance v0, Lio/reactivex/d/e/d/bl$p;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$p;-><init>(Lio/reactivex/c/h;)V

    return-object v0
.end method

.method private static d(Lio/reactivex/c/h;)Lio/reactivex/c/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/z",
            "<+TR;>;>;)",
            "Lio/reactivex/c/h",
            "<TT;",
            "Lio/reactivex/n",
            "<TR;>;>;"
        }
    .end annotation

    .prologue
    .line 308
    const-string v0, "mapper is null"

    invoke-static {p0, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 309
    new-instance v0, Lio/reactivex/d/e/d/bl$g;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/bl$g;-><init>(Lio/reactivex/c/h;)V

    return-object v0
.end method
