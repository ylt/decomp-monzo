.class public final Lio/reactivex/d/e/d/dr;
.super Lio/reactivex/d/e/d/a;
.source "ObservableTimeoutTimed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dr$a;,
        Lio/reactivex/d/e/d/dr$b;,
        Lio/reactivex/d/e/d/dr$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# static fields
.field static final f:Lio/reactivex/b/b;


# instance fields
.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/u;

.field final e:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lio/reactivex/d/e/d/dr$a;

    invoke-direct {v0}, Lio/reactivex/d/e/d/dr$a;-><init>()V

    sput-object v0, Lio/reactivex/d/e/d/dr;->f:Lio/reactivex/b/b;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/r;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 39
    iput-wide p2, p0, Lio/reactivex/d/e/d/dr;->b:J

    .line 40
    iput-object p4, p0, Lio/reactivex/d/e/d/dr;->c:Ljava/util/concurrent/TimeUnit;

    .line 41
    iput-object p5, p0, Lio/reactivex/d/e/d/dr;->d:Lio/reactivex/u;

    .line 42
    iput-object p6, p0, Lio/reactivex/d/e/d/dr;->e:Lio/reactivex/r;

    .line 43
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lio/reactivex/d/e/d/dr;->e:Lio/reactivex/r;

    if-nez v0, :cond_0

    .line 48
    iget-object v6, p0, Lio/reactivex/d/e/d/dr;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/dr$b;

    new-instance v1, Lio/reactivex/f/e;

    invoke-direct {v1, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    iget-wide v2, p0, Lio/reactivex/d/e/d/dr;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/dr;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lio/reactivex/d/e/d/dr;->d:Lio/reactivex/u;

    .line 50
    invoke-virtual {v5}, Lio/reactivex/u;->a()Lio/reactivex/u$c;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/dr$b;-><init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u$c;)V

    .line 48
    invoke-interface {v6, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 56
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v7, p0, Lio/reactivex/d/e/d/dr;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/dr$c;

    iget-wide v2, p0, Lio/reactivex/d/e/d/dr;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/dr;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lio/reactivex/d/e/d/dr;->d:Lio/reactivex/u;

    .line 54
    invoke-virtual {v1}, Lio/reactivex/u;->a()Lio/reactivex/u$c;

    move-result-object v5

    iget-object v6, p0, Lio/reactivex/d/e/d/dr;->e:Lio/reactivex/r;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lio/reactivex/d/e/d/dr$c;-><init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u$c;Lio/reactivex/r;)V

    .line 52
    invoke-interface {v7, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method
