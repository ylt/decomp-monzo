.class public final Lio/reactivex/d/e/d/ae;
.super Lio/reactivex/n;
.source "ObservableDelaySubscriptionOther.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/ae$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/n",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TU;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<+TT;>;",
            "Lio/reactivex/r",
            "<TU;>;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 32
    iput-object p1, p0, Lio/reactivex/d/e/d/ae;->a:Lio/reactivex/r;

    .line 33
    iput-object p2, p0, Lio/reactivex/d/e/d/ae;->b:Lio/reactivex/r;

    .line 34
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lio/reactivex/d/a/k;

    invoke-direct {v0}, Lio/reactivex/d/a/k;-><init>()V

    .line 39
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 41
    new-instance v1, Lio/reactivex/d/e/d/ae$a;

    invoke-direct {v1, p0, v0, p1}, Lio/reactivex/d/e/d/ae$a;-><init>(Lio/reactivex/d/e/d/ae;Lio/reactivex/d/a/k;Lio/reactivex/t;)V

    .line 43
    iget-object v0, p0, Lio/reactivex/d/e/d/ae;->b:Lio/reactivex/r;

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 44
    return-void
.end method
