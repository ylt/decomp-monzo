.class final Lio/reactivex/d/e/d/h$b;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableAmb.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/e/d/h$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/d/h$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:I

.field final c:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field d:Z


# direct methods
.method constructor <init>(Lio/reactivex/d/e/d/h$a;ILio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/e/d/h$a",
            "<TT;>;I",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 150
    iput-object p1, p0, Lio/reactivex/d/e/d/h$b;->a:Lio/reactivex/d/e/d/h$a;

    .line 151
    iput p2, p0, Lio/reactivex/d/e/d/h$b;->b:I

    .line 152
    iput-object p3, p0, Lio/reactivex/d/e/d/h$b;->c:Lio/reactivex/t;

    .line 153
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 201
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 202
    return-void
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 190
    iget-boolean v0, p0, Lio/reactivex/d/e/d/h$b;->d:Z

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->c:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->a:Lio/reactivex/d/e/d/h$a;

    iget v1, p0, Lio/reactivex/d/e/d/h$b;->b:I

    invoke-virtual {v0, v1}, Lio/reactivex/d/e/d/h$a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/h$b;->d:Z

    .line 195
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->c:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 176
    iget-boolean v0, p0, Lio/reactivex/d/e/d/h$b;->d:Z

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->c:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 186
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->a:Lio/reactivex/d/e/d/h$a;

    iget v1, p0, Lio/reactivex/d/e/d/h$b;->b:I

    invoke-virtual {v0, v1}, Lio/reactivex/d/e/d/h$a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/h$b;->d:Z

    .line 181
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->c:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 183
    :cond_1
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 162
    iget-boolean v0, p0, Lio/reactivex/d/e/d/h$b;->d:Z

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->c:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 172
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->a:Lio/reactivex/d/e/d/h$a;

    iget v1, p0, Lio/reactivex/d/e/d/h$b;->b:I

    invoke-virtual {v0, v1}, Lio/reactivex/d/e/d/h$a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/h$b;->d:Z

    .line 167
    iget-object v0, p0, Lio/reactivex/d/e/d/h$b;->c:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 169
    :cond_1
    invoke-virtual {p0}, Lio/reactivex/d/e/d/h$b;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    goto :goto_0
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 157
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 158
    return-void
.end method
