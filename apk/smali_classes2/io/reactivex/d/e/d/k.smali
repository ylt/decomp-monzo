.class public final Lio/reactivex/d/e/d/k;
.super Lio/reactivex/n;
.source "ObservableAutoConnect.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/n",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/e/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/e/a",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final b:I

.field final c:Lio/reactivex/c/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/g",
            "<-",
            "Lio/reactivex/b/b;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Lio/reactivex/e/a;ILio/reactivex/c/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/e/a",
            "<+TT;>;I",
            "Lio/reactivex/c/g",
            "<-",
            "Lio/reactivex/b/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 38
    iput-object p1, p0, Lio/reactivex/d/e/d/k;->a:Lio/reactivex/e/a;

    .line 39
    iput p2, p0, Lio/reactivex/d/e/d/k;->b:I

    .line 40
    iput-object p3, p0, Lio/reactivex/d/e/d/k;->c:Lio/reactivex/c/g;

    .line 41
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/k;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 42
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lio/reactivex/d/e/d/k;->a:Lio/reactivex/e/a;

    invoke-virtual {v0, p1}, Lio/reactivex/e/a;->subscribe(Lio/reactivex/t;)V

    .line 47
    iget-object v0, p0, Lio/reactivex/d/e/d/k;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iget v1, p0, Lio/reactivex/d/e/d/k;->b:I

    if-ne v0, v1, :cond_0

    .line 48
    iget-object v0, p0, Lio/reactivex/d/e/d/k;->a:Lio/reactivex/e/a;

    iget-object v1, p0, Lio/reactivex/d/e/d/k;->c:Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/e/a;->a(Lio/reactivex/c/g;)V

    .line 50
    :cond_0
    return-void
.end method
