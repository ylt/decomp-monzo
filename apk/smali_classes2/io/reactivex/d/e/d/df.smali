.class public final Lio/reactivex/d/e/d/df;
.super Lio/reactivex/d/e/d/a;
.source "ObservableSwitchIfEmpty.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/df$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 24
    iput-object p2, p0, Lio/reactivex/d/e/d/df;->b:Lio/reactivex/r;

    .line 25
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lio/reactivex/d/e/d/df$a;

    iget-object v1, p0, Lio/reactivex/d/e/d/df;->b:Lio/reactivex/r;

    invoke-direct {v0, p1, v1}, Lio/reactivex/d/e/d/df$a;-><init>(Lio/reactivex/t;Lio/reactivex/r;)V

    .line 30
    iget-object v1, v0, Lio/reactivex/d/e/d/df$a;->c:Lio/reactivex/d/a/k;

    invoke-interface {p1, v1}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 31
    iget-object v1, p0, Lio/reactivex/d/e/d/df;->a:Lio/reactivex/r;

    invoke-interface {v1, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 32
    return-void
.end method
