.class final Lio/reactivex/d/e/d/aw$a$a;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ObservableFlatMapMaybe.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/aw$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/j",
        "<TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lio/reactivex/d/e/d/aw$a;


# direct methods
.method constructor <init>(Lio/reactivex/d/e/d/aw$a;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lio/reactivex/d/e/d/aw$a$a;->a:Lio/reactivex/d/e/d/aw$a;

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 310
    iget-object v0, p0, Lio/reactivex/d/e/d/aw$a$a;->a:Lio/reactivex/d/e/d/aw$a;

    invoke-virtual {v0, p0, p1}, Lio/reactivex/d/e/d/aw$a;->a(Lio/reactivex/d/e/d/aw$a$a;Ljava/lang/Object;)V

    .line 311
    return-void
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 330
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 331
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lio/reactivex/d/e/d/aw$a$a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;)Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lio/reactivex/d/e/d/aw$a$a;->a:Lio/reactivex/d/e/d/aw$a;

    invoke-virtual {v0, p0}, Lio/reactivex/d/e/d/aw$a;->a(Lio/reactivex/d/e/d/aw$a$a;)V

    .line 321
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lio/reactivex/d/e/d/aw$a$a;->a:Lio/reactivex/d/e/d/aw$a;

    invoke-virtual {v0, p0, p1}, Lio/reactivex/d/e/d/aw$a;->a(Lio/reactivex/d/e/d/aw$a$a;Ljava/lang/Throwable;)V

    .line 316
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 305
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 306
    return-void
.end method
