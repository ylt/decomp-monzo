.class public final Lio/reactivex/d/e/d/cu;
.super Lio/reactivex/n;
.source "ObservableSequenceEqual.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/cu$b;,
        Lio/reactivex/d/e/d/cu$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/n",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/c/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/d",
            "<-TT;-TT;>;"
        }
    .end annotation
.end field

.field final d:I


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/d;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<+TT;>;",
            "Lio/reactivex/r",
            "<+TT;>;",
            "Lio/reactivex/c/d",
            "<-TT;-TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 33
    iput-object p1, p0, Lio/reactivex/d/e/d/cu;->a:Lio/reactivex/r;

    .line 34
    iput-object p2, p0, Lio/reactivex/d/e/d/cu;->b:Lio/reactivex/r;

    .line 35
    iput-object p3, p0, Lio/reactivex/d/e/d/cu;->c:Lio/reactivex/c/d;

    .line 36
    iput p4, p0, Lio/reactivex/d/e/d/cu;->d:I

    .line 37
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lio/reactivex/d/e/d/cu$a;

    iget v2, p0, Lio/reactivex/d/e/d/cu;->d:I

    iget-object v3, p0, Lio/reactivex/d/e/d/cu;->a:Lio/reactivex/r;

    iget-object v4, p0, Lio/reactivex/d/e/d/cu;->b:Lio/reactivex/r;

    iget-object v5, p0, Lio/reactivex/d/e/d/cu;->c:Lio/reactivex/c/d;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/cu$a;-><init>(Lio/reactivex/t;ILio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/d;)V

    .line 42
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 43
    invoke-virtual {v0}, Lio/reactivex/d/e/d/cu$a;->a()V

    .line 44
    return-void
.end method
