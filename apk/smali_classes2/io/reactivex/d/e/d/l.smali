.class public final Lio/reactivex/d/e/d/l;
.super Ljava/lang/Object;
.source "ObservableBlockingSubscribe.java"


# direct methods
.method public static a(Lio/reactivex/r;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/r",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lio/reactivex/d/j/f;

    invoke-direct {v0}, Lio/reactivex/d/j/f;-><init>()V

    .line 78
    new-instance v1, Lio/reactivex/d/d/s;

    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v2

    .line 79
    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v3

    invoke-direct {v1, v2, v0, v0, v3}, Lio/reactivex/d/d/s;-><init>(Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/a;Lio/reactivex/c/g;)V

    .line 81
    invoke-interface {p0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 83
    invoke-static {v0, v1}, Lio/reactivex/d/j/e;->a(Ljava/util/concurrent/CountDownLatch;Lio/reactivex/b/b;)V

    .line 84
    iget-object v0, v0, Lio/reactivex/d/j/f;->a:Ljava/lang/Throwable;

    .line 85
    if-eqz v0, :cond_0

    .line 86
    invoke-static {v0}, Lio/reactivex/d/j/j;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 88
    :cond_0
    return-void
.end method

.method public static a(Lio/reactivex/r;Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/r",
            "<+TT;>;",
            "Lio/reactivex/c/g",
            "<-TT;>;",
            "Lio/reactivex/c/g",
            "<-",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/c/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 100
    const-string v0, "onNext is null"

    invoke-static {p1, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 101
    const-string v0, "onError is null"

    invoke-static {p2, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 102
    const-string v0, "onComplete is null"

    invoke-static {p3, v0}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 103
    new-instance v0, Lio/reactivex/d/d/s;

    invoke-static {}, Lio/reactivex/d/b/a;->b()Lio/reactivex/c/g;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, Lio/reactivex/d/d/s;-><init>(Lio/reactivex/c/g;Lio/reactivex/c/g;Lio/reactivex/c/a;Lio/reactivex/c/g;)V

    invoke-static {p0, v0}, Lio/reactivex/d/e/d/l;->a(Lio/reactivex/r;Lio/reactivex/t;)V

    .line 104
    return-void
.end method

.method public static a(Lio/reactivex/r;Lio/reactivex/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/r",
            "<+TT;>;",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 45
    new-instance v2, Lio/reactivex/d/d/h;

    invoke-direct {v2, v1}, Lio/reactivex/d/d/h;-><init>(Ljava/util/Queue;)V

    .line 46
    invoke-interface {p1, v2}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 48
    invoke-interface {p0, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 50
    :cond_0
    invoke-virtual {v2}, Lio/reactivex/d/d/h;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 69
    :cond_1
    :goto_0
    return-void

    .line 53
    :cond_2
    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    .line 54
    if-nez v0, :cond_3

    .line 56
    :try_start_0
    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 63
    :cond_3
    invoke-virtual {v2}, Lio/reactivex/d/d/h;->isDisposed()Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lio/reactivex/d/d/h;->a:Ljava/lang/Object;

    if-eq p0, v3, :cond_1

    .line 65
    invoke-static {v0, p1}, Lio/reactivex/d/j/n;->b(Ljava/lang/Object;Lio/reactivex/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    invoke-virtual {v2}, Lio/reactivex/d/d/h;->dispose()V

    .line 59
    invoke-interface {p1, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
