.class final Lio/reactivex/d/e/d/co$a;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableRetryWhen.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/co;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/co$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field final c:Lio/reactivex/d/j/c;

.field final d:Lio/reactivex/i/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/i/c",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lio/reactivex/d/e/d/co$a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/d/co$a",
            "<TT;>.a;"
        }
    .end annotation
.end field

.field final f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lio/reactivex/b/b;",
            ">;"
        }
    .end annotation
.end field

.field final g:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TT;>;"
        }
    .end annotation
.end field

.field volatile h:Z


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/i/c;Lio/reactivex/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;",
            "Lio/reactivex/i/c",
            "<",
            "Ljava/lang/Throwable;",
            ">;",
            "Lio/reactivex/r",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 84
    iput-object p1, p0, Lio/reactivex/d/e/d/co$a;->a:Lio/reactivex/t;

    .line 85
    iput-object p2, p0, Lio/reactivex/d/e/d/co$a;->d:Lio/reactivex/i/c;

    .line 86
    iput-object p3, p0, Lio/reactivex/d/e/d/co$a;->g:Lio/reactivex/r;

    .line 87
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/co$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 88
    new-instance v0, Lio/reactivex/d/j/c;

    invoke-direct {v0}, Lio/reactivex/d/j/c;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/co$a;->c:Lio/reactivex/d/j/c;

    .line 89
    new-instance v0, Lio/reactivex/d/e/d/co$a$a;

    invoke-direct {v0, p0}, Lio/reactivex/d/e/d/co$a$a;-><init>(Lio/reactivex/d/e/d/co$a;)V

    iput-object v0, p0, Lio/reactivex/d/e/d/co$a;->e:Lio/reactivex/d/e/d/co$a$a;

    .line 90
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/co$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    .line 91
    return-void
.end method


# virtual methods
.method a()V
    .locals 0

    .prologue
    .line 127
    invoke-virtual {p0}, Lio/reactivex/d/e/d/co$a;->c()V

    .line 128
    return-void
.end method

.method a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 132
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->a:Lio/reactivex/t;

    iget-object v1, p0, Lio/reactivex/d/e/d/co$a;->c:Lio/reactivex/d/j/c;

    invoke-static {v0, p1, p0, v1}, Lio/reactivex/d/j/k;->a(Lio/reactivex/t;Ljava/lang/Throwable;Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/d/j/c;)V

    .line 133
    return-void
.end method

.method b()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 137
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->a:Lio/reactivex/t;

    iget-object v1, p0, Lio/reactivex/d/e/d/co$a;->c:Lio/reactivex/d/j/c;

    invoke-static {v0, p0, v1}, Lio/reactivex/d/j/k;->a(Lio/reactivex/t;Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/d/j/c;)V

    .line 138
    return-void
.end method

.method c()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_1

    .line 144
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/d/co$a;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 154
    :cond_1
    :goto_0
    return-void

    .line 148
    :cond_2
    iget-boolean v0, p0, Lio/reactivex/d/e/d/co$a;->h:Z

    if-nez v0, :cond_3

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/co$a;->h:Z

    .line 150
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->g:Lio/reactivex/r;

    invoke-interface {v0, p0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 152
    :cond_3
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 123
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->e:Lio/reactivex/d/e/d/co$a$a;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 124
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;)Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->e:Lio/reactivex/d/e/d/co$a$a;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 112
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->a:Lio/reactivex/t;

    iget-object v1, p0, Lio/reactivex/d/e/d/co$a;->c:Lio/reactivex/d/j/c;

    invoke-static {v0, p0, v1}, Lio/reactivex/d/j/k;->a(Lio/reactivex/t;Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/d/j/c;)V

    .line 113
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/reactivex/d/e/d/co$a;->h:Z

    .line 106
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->d:Lio/reactivex/i/c;

    invoke-virtual {v0, p1}, Lio/reactivex/i/c;->onNext(Ljava/lang/Object;)V

    .line 107
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->a:Lio/reactivex/t;

    iget-object v1, p0, Lio/reactivex/d/e/d/co$a;->c:Lio/reactivex/d/j/c;

    invoke-static {v0, p1, p0, v1}, Lio/reactivex/d/j/k;->a(Lio/reactivex/t;Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/d/j/c;)V

    .line 101
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lio/reactivex/d/e/d/co$a;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->c(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 96
    return-void
.end method
