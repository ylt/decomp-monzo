.class final Lio/reactivex/d/e/d/ag$a;
.super Ljava/lang/Object;
.source "ObservableDetach.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field b:Lio/reactivex/b/b;


# direct methods
.method constructor <init>(Lio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lio/reactivex/d/e/d/ag$a;->a:Lio/reactivex/t;

    .line 46
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lio/reactivex/d/e/d/ag$a;->b:Lio/reactivex/b/b;

    .line 51
    sget-object v1, Lio/reactivex/d/j/g;->a:Lio/reactivex/d/j/g;

    iput-object v1, p0, Lio/reactivex/d/e/d/ag$a;->b:Lio/reactivex/b/b;

    .line 52
    invoke-static {}, Lio/reactivex/d/j/g;->b()Lio/reactivex/t;

    move-result-object v1

    iput-object v1, p0, Lio/reactivex/d/e/d/ag$a;->a:Lio/reactivex/t;

    .line 53
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 54
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lio/reactivex/d/e/d/ag$a;->b:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lio/reactivex/d/e/d/ag$a;->a:Lio/reactivex/t;

    .line 86
    sget-object v1, Lio/reactivex/d/j/g;->a:Lio/reactivex/d/j/g;

    iput-object v1, p0, Lio/reactivex/d/e/d/ag$a;->b:Lio/reactivex/b/b;

    .line 87
    invoke-static {}, Lio/reactivex/d/j/g;->b()Lio/reactivex/t;

    move-result-object v1

    iput-object v1, p0, Lio/reactivex/d/e/d/ag$a;->a:Lio/reactivex/t;

    .line 88
    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 89
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lio/reactivex/d/e/d/ag$a;->a:Lio/reactivex/t;

    .line 78
    sget-object v1, Lio/reactivex/d/j/g;->a:Lio/reactivex/d/j/g;

    iput-object v1, p0, Lio/reactivex/d/e/d/ag$a;->b:Lio/reactivex/b/b;

    .line 79
    invoke-static {}, Lio/reactivex/d/j/g;->b()Lio/reactivex/t;

    move-result-object v1

    iput-object v1, p0, Lio/reactivex/d/e/d/ag$a;->a:Lio/reactivex/t;

    .line 80
    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 81
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lio/reactivex/d/e/d/ag$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lio/reactivex/d/e/d/ag$a;->b:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iput-object p1, p0, Lio/reactivex/d/e/d/ag$a;->b:Lio/reactivex/b/b;

    .line 66
    iget-object v0, p0, Lio/reactivex/d/e/d/ag$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 68
    :cond_0
    return-void
.end method
