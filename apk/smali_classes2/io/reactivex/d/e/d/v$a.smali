.class final Lio/reactivex/d/e/d/v$a;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "ObservableConcatMap.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/v$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TR;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;"
        }
    .end annotation
.end field

.field final c:I

.field final d:Lio/reactivex/d/j/c;

.field final e:Lio/reactivex/d/e/d/v$a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/d/v$a$a",
            "<TR;>;"
        }
    .end annotation
.end field

.field final f:Lio/reactivex/d/a/k;

.field final g:Z

.field h:Lio/reactivex/d/c/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/c/i",
            "<TT;>;"
        }
    .end annotation
.end field

.field i:Lio/reactivex/b/b;

.field volatile j:Z

.field volatile k:Z

.field volatile l:Z

.field m:I


# direct methods
.method constructor <init>(Lio/reactivex/t;Lio/reactivex/c/h;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TR;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;IZ)V"
        }
    .end annotation

    .prologue
    .line 299
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 300
    iput-object p1, p0, Lio/reactivex/d/e/d/v$a;->a:Lio/reactivex/t;

    .line 301
    iput-object p2, p0, Lio/reactivex/d/e/d/v$a;->b:Lio/reactivex/c/h;

    .line 302
    iput p3, p0, Lio/reactivex/d/e/d/v$a;->c:I

    .line 303
    iput-boolean p4, p0, Lio/reactivex/d/e/d/v$a;->g:Z

    .line 304
    new-instance v0, Lio/reactivex/d/j/c;

    invoke-direct {v0}, Lio/reactivex/d/j/c;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/v$a;->d:Lio/reactivex/d/j/c;

    .line 305
    new-instance v0, Lio/reactivex/d/e/d/v$a$a;

    invoke-direct {v0, p1, p0}, Lio/reactivex/d/e/d/v$a$a;-><init>(Lio/reactivex/t;Lio/reactivex/d/e/d/v$a;)V

    iput-object v0, p0, Lio/reactivex/d/e/d/v$a;->e:Lio/reactivex/d/e/d/v$a$a;

    .line 306
    new-instance v0, Lio/reactivex/d/a/k;

    invoke-direct {v0}, Lio/reactivex/d/a/k;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/v$a;->f:Lio/reactivex/d/a/k;

    .line 307
    return-void
.end method


# virtual methods
.method a()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 383
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$a;->getAndIncrement()I

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    :goto_0
    return-void

    .line 387
    :cond_0
    iget-object v2, p0, Lio/reactivex/d/e/d/v$a;->a:Lio/reactivex/t;

    .line 388
    iget-object v3, p0, Lio/reactivex/d/e/d/v$a;->h:Lio/reactivex/d/c/i;

    .line 389
    iget-object v4, p0, Lio/reactivex/d/e/d/v$a;->d:Lio/reactivex/d/j/c;

    .line 393
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$a;->j:Z

    if-nez v0, :cond_8

    .line 395
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$a;->l:Z

    if-eqz v0, :cond_2

    .line 396
    invoke-interface {v3}, Lio/reactivex/d/c/i;->c()V

    goto :goto_0

    .line 400
    :cond_2
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$a;->g:Z

    if-nez v0, :cond_3

    .line 401
    invoke-virtual {v4}, Lio/reactivex/d/j/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 402
    if-eqz v0, :cond_3

    .line 403
    invoke-interface {v3}, Lio/reactivex/d/c/i;->c()V

    .line 404
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$a;->l:Z

    .line 405
    invoke-virtual {v4}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v2, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 410
    :cond_3
    iget-boolean v5, p0, Lio/reactivex/d/e/d/v$a;->k:Z

    .line 415
    :try_start_0
    invoke-interface {v3}, Lio/reactivex/d/c/i;->n_()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 425
    if-nez v6, :cond_4

    move v0, v1

    .line 427
    :goto_2
    if-eqz v5, :cond_6

    if-eqz v0, :cond_6

    .line 428
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$a;->l:Z

    .line 429
    invoke-virtual {v4}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    .line 430
    if-eqz v0, :cond_5

    .line 431
    invoke-interface {v2, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 416
    :catch_0
    move-exception v0

    .line 417
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 418
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$a;->l:Z

    .line 419
    iget-object v1, p0, Lio/reactivex/d/e/d/v$a;->i:Lio/reactivex/b/b;

    invoke-interface {v1}, Lio/reactivex/b/b;->dispose()V

    .line 420
    invoke-virtual {v4, v0}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    .line 421
    invoke-virtual {v4}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v2, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 425
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 433
    :cond_5
    invoke-interface {v2}, Lio/reactivex/t;->onComplete()V

    goto :goto_0

    .line 438
    :cond_6
    if-nez v0, :cond_8

    .line 443
    :try_start_1
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->b:Lio/reactivex/c/h;

    invoke-interface {v0, v6}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v5, "The mapper returned a null ObservableSource"

    invoke-static {v0, v5}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 454
    instance-of v5, v0, Ljava/util/concurrent/Callable;

    if-eqz v5, :cond_7

    .line 458
    :try_start_2
    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 465
    if-eqz v0, :cond_1

    iget-boolean v5, p0, Lio/reactivex/d/e/d/v$a;->l:Z

    if-nez v5, :cond_1

    .line 466
    invoke-interface {v2, v0}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    goto :goto_1

    .line 444
    :catch_1
    move-exception v0

    .line 445
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 446
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$a;->l:Z

    .line 447
    iget-object v1, p0, Lio/reactivex/d/e/d/v$a;->i:Lio/reactivex/b/b;

    invoke-interface {v1}, Lio/reactivex/b/b;->dispose()V

    .line 448
    invoke-interface {v3}, Lio/reactivex/d/c/i;->c()V

    .line 449
    invoke-virtual {v4, v0}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    .line 450
    invoke-virtual {v4}, Lio/reactivex/d/j/c;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v2, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 459
    :catch_2
    move-exception v0

    .line 460
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 461
    invoke-virtual {v4, v0}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    goto/16 :goto_1

    .line 470
    :cond_7
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$a;->j:Z

    .line 471
    iget-object v5, p0, Lio/reactivex/d/e/d/v$a;->e:Lio/reactivex/d/e/d/v$a$a;

    invoke-interface {v0, v5}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 476
    :cond_8
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$a;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/v$a;->l:Z

    .line 377
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->i:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 378
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->f:Lio/reactivex/d/a/k;

    invoke-virtual {v0}, Lio/reactivex/d/a/k;->dispose()V

    .line 379
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 371
    iget-boolean v0, p0, Lio/reactivex/d/e/d/v$a;->l:Z

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/v$a;->k:Z

    .line 366
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$a;->a()V

    .line 367
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->d:Lio/reactivex/d/j/c;

    invoke-virtual {v0, p1}, Lio/reactivex/d/j/c;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/v$a;->k:Z

    .line 357
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$a;->a()V

    .line 361
    :goto_0
    return-void

    .line 359
    :cond_0
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 347
    iget v0, p0, Lio/reactivex/d/e/d/v$a;->m:I

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->h:Lio/reactivex/d/c/i;

    invoke-interface {v0, p1}, Lio/reactivex/d/c/i;->a(Ljava/lang/Object;)Z

    .line 350
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$a;->a()V

    .line 351
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 311
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->i:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iput-object p1, p0, Lio/reactivex/d/e/d/v$a;->i:Lio/reactivex/b/b;

    .line 314
    instance-of v0, p1, Lio/reactivex/d/c/d;

    if-eqz v0, :cond_2

    .line 316
    check-cast p1, Lio/reactivex/d/c/d;

    .line 318
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Lio/reactivex/d/c/d;->a(I)I

    move-result v0

    .line 319
    if-ne v0, v1, :cond_1

    .line 320
    iput v0, p0, Lio/reactivex/d/e/d/v$a;->m:I

    .line 321
    iput-object p1, p0, Lio/reactivex/d/e/d/v$a;->h:Lio/reactivex/d/c/i;

    .line 322
    iput-boolean v1, p0, Lio/reactivex/d/e/d/v$a;->k:Z

    .line 324
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 326
    invoke-virtual {p0}, Lio/reactivex/d/e/d/v$a;->a()V

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 330
    iput v0, p0, Lio/reactivex/d/e/d/v$a;->m:I

    .line 331
    iput-object p1, p0, Lio/reactivex/d/e/d/v$a;->h:Lio/reactivex/d/c/i;

    .line 333
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    goto :goto_0

    .line 339
    :cond_2
    new-instance v0, Lio/reactivex/d/f/c;

    iget v1, p0, Lio/reactivex/d/e/d/v$a;->c:I

    invoke-direct {v0, v1}, Lio/reactivex/d/f/c;-><init>(I)V

    iput-object v0, p0, Lio/reactivex/d/e/d/v$a;->h:Lio/reactivex/d/c/i;

    .line 341
    iget-object v0, p0, Lio/reactivex/d/e/d/v$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    goto :goto_0
.end method
