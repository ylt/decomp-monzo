.class public final Lio/reactivex/d/e/d/w;
.super Lio/reactivex/d/e/d/a;
.source "ObservableConcatMapEager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/w$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TR;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;"
        }
    .end annotation
.end field

.field final c:Lio/reactivex/d/j/i;

.field final d:I

.field final e:I


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/h;Lio/reactivex/d/j/i;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TR;>;>;",
            "Lio/reactivex/d/j/i;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 45
    iput-object p2, p0, Lio/reactivex/d/e/d/w;->b:Lio/reactivex/c/h;

    .line 46
    iput-object p3, p0, Lio/reactivex/d/e/d/w;->c:Lio/reactivex/d/j/i;

    .line 47
    iput p4, p0, Lio/reactivex/d/e/d/w;->d:I

    .line 48
    iput p5, p0, Lio/reactivex/d/e/d/w;->e:I

    .line 49
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TR;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v6, p0, Lio/reactivex/d/e/d/w;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/w$a;

    iget-object v2, p0, Lio/reactivex/d/e/d/w;->b:Lio/reactivex/c/h;

    iget v3, p0, Lio/reactivex/d/e/d/w;->d:I

    iget v4, p0, Lio/reactivex/d/e/d/w;->e:I

    iget-object v5, p0, Lio/reactivex/d/e/d/w;->c:Lio/reactivex/d/j/i;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/w$a;-><init>(Lio/reactivex/t;Lio/reactivex/c/h;IILio/reactivex/d/j/i;)V

    invoke-interface {v6, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 54
    return-void
.end method
