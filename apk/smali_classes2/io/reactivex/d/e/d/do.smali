.class public final Lio/reactivex/d/e/d/do;
.super Lio/reactivex/d/e/d/a;
.source "ObservableThrottleFirstTimed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/do$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Lio/reactivex/r;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 34
    iput-wide p2, p0, Lio/reactivex/d/e/d/do;->b:J

    .line 35
    iput-object p4, p0, Lio/reactivex/d/e/d/do;->c:Ljava/util/concurrent/TimeUnit;

    .line 36
    iput-object p5, p0, Lio/reactivex/d/e/d/do;->d:Lio/reactivex/u;

    .line 37
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v6, p0, Lio/reactivex/d/e/d/do;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/do$a;

    new-instance v1, Lio/reactivex/f/e;

    invoke-direct {v1, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    iget-wide v2, p0, Lio/reactivex/d/e/d/do;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/do;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lio/reactivex/d/e/d/do;->d:Lio/reactivex/u;

    .line 43
    invoke-virtual {v5}, Lio/reactivex/u;->a()Lio/reactivex/u$c;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/do$a;-><init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u$c;)V

    .line 41
    invoke-interface {v6, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 44
    return-void
.end method
