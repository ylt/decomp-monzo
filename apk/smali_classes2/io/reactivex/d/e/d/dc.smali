.class public final Lio/reactivex/d/e/d/dc;
.super Lio/reactivex/d/e/d/a;
.source "ObservableSkipUntil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/dc$a;,
        Lio/reactivex/d/e/d/dc$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TU;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/r",
            "<TU;>;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 25
    iput-object p2, p0, Lio/reactivex/d/e/d/dc;->b:Lio/reactivex/r;

    .line 26
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lio/reactivex/f/e;

    invoke-direct {v0, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    .line 33
    new-instance v1, Lio/reactivex/d/a/a;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lio/reactivex/d/a/a;-><init>(I)V

    .line 35
    invoke-virtual {v0, v1}, Lio/reactivex/f/e;->onSubscribe(Lio/reactivex/b/b;)V

    .line 37
    new-instance v2, Lio/reactivex/d/e/d/dc$b;

    invoke-direct {v2, v0, v1}, Lio/reactivex/d/e/d/dc$b;-><init>(Lio/reactivex/t;Lio/reactivex/d/a/a;)V

    .line 39
    iget-object v3, p0, Lio/reactivex/d/e/d/dc;->b:Lio/reactivex/r;

    new-instance v4, Lio/reactivex/d/e/d/dc$a;

    invoke-direct {v4, p0, v1, v2, v0}, Lio/reactivex/d/e/d/dc$a;-><init>(Lio/reactivex/d/e/d/dc;Lio/reactivex/d/a/a;Lio/reactivex/d/e/d/dc$b;Lio/reactivex/f/e;)V

    invoke-interface {v3, v4}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 41
    iget-object v0, p0, Lio/reactivex/d/e/d/dc;->a:Lio/reactivex/r;

    invoke-interface {v0, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 42
    return-void
.end method
