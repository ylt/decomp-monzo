.class public final Lio/reactivex/d/e/d/ap;
.super Lio/reactivex/v;
.source "ObservableElementAtSingle.java"

# interfaces
.implements Lio/reactivex/d/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/ap$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/v",
        "<TT;>;",
        "Lio/reactivex/d/c/c",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/r;JLjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;JTT;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lio/reactivex/v;-><init>()V

    .line 30
    iput-object p1, p0, Lio/reactivex/d/e/d/ap;->a:Lio/reactivex/r;

    .line 31
    iput-wide p2, p0, Lio/reactivex/d/e/d/ap;->b:J

    .line 32
    iput-object p4, p0, Lio/reactivex/d/e/d/ap;->c:Ljava/lang/Object;

    .line 33
    return-void
.end method


# virtual methods
.method public b(Lio/reactivex/x;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lio/reactivex/d/e/d/ap;->a:Lio/reactivex/r;

    new-instance v1, Lio/reactivex/d/e/d/ap$a;

    iget-wide v2, p0, Lio/reactivex/d/e/d/ap;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/ap;->c:Ljava/lang/Object;

    invoke-direct {v1, p1, v2, v3, v4}, Lio/reactivex/d/e/d/ap$a;-><init>(Lio/reactivex/x;JLjava/lang/Object;)V

    invoke-interface {v0, v1}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 38
    return-void
.end method

.method public q_()Lio/reactivex/n;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lio/reactivex/d/e/d/an;

    iget-object v1, p0, Lio/reactivex/d/e/d/ap;->a:Lio/reactivex/r;

    iget-wide v2, p0, Lio/reactivex/d/e/d/ap;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/ap;->c:Ljava/lang/Object;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/an;-><init>(Lio/reactivex/r;JLjava/lang/Object;Z)V

    invoke-static {v0}, Lio/reactivex/g/a;->a(Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
