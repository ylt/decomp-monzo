.class public final Lio/reactivex/d/e/d/cp;
.super Lio/reactivex/d/e/d/a;
.source "ObservableSampleTimed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/cp$a;,
        Lio/reactivex/d/e/d/cp$b;,
        Lio/reactivex/d/e/d/cp$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lio/reactivex/u;

.field final e:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/u;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 33
    iput-wide p2, p0, Lio/reactivex/d/e/d/cp;->b:J

    .line 34
    iput-object p4, p0, Lio/reactivex/d/e/d/cp;->c:Ljava/util/concurrent/TimeUnit;

    .line 35
    iput-object p5, p0, Lio/reactivex/d/e/d/cp;->d:Lio/reactivex/u;

    .line 36
    iput-boolean p6, p0, Lio/reactivex/d/e/d/cp;->e:Z

    .line 37
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v1, Lio/reactivex/f/e;

    invoke-direct {v1, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    .line 43
    iget-boolean v0, p0, Lio/reactivex/d/e/d/cp;->e:Z

    if-eqz v0, :cond_0

    .line 44
    iget-object v6, p0, Lio/reactivex/d/e/d/cp;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/cp$a;

    iget-wide v2, p0, Lio/reactivex/d/e/d/cp;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/cp;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lio/reactivex/d/e/d/cp;->d:Lio/reactivex/u;

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/cp$a;-><init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V

    invoke-interface {v6, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    iget-object v6, p0, Lio/reactivex/d/e/d/cp;->a:Lio/reactivex/r;

    new-instance v0, Lio/reactivex/d/e/d/cp$b;

    iget-wide v2, p0, Lio/reactivex/d/e/d/cp;->b:J

    iget-object v4, p0, Lio/reactivex/d/e/d/cp;->c:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lio/reactivex/d/e/d/cp;->d:Lio/reactivex/u;

    invoke-direct/range {v0 .. v5}, Lio/reactivex/d/e/d/cp$b;-><init>(Lio/reactivex/t;JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)V

    invoke-interface {v6, v0}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method
