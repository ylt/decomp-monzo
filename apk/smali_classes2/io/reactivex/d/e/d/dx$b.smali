.class final Lio/reactivex/d/e/d/dx$b;
.super Ljava/util/concurrent/atomic/AtomicBoolean;
.source "ObservableWindow.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/d/dx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field final b:J

.field final c:J

.field final d:I

.field final e:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lio/reactivex/i/d",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field f:J

.field volatile g:Z

.field h:J

.field i:Lio/reactivex/b/b;

.field final j:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method constructor <init>(Lio/reactivex/t;JJI)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Lio/reactivex/n",
            "<TT;>;>;JJI)V"
        }
    .end annotation

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 156
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/dx$b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 159
    iput-object p1, p0, Lio/reactivex/d/e/d/dx$b;->a:Lio/reactivex/t;

    .line 160
    iput-wide p2, p0, Lio/reactivex/d/e/d/dx$b;->b:J

    .line 161
    iput-wide p4, p0, Lio/reactivex/d/e/d/dx$b;->c:J

    .line 162
    iput p6, p0, Lio/reactivex/d/e/d/dx$b;->d:I

    .line 163
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/d/dx$b;->e:Ljava/util/ArrayDeque;

    .line 164
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/e/d/dx$b;->g:Z

    .line 231
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dx$b;->g:Z

    return v0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 221
    iget-object v1, p0, Lio/reactivex/d/e/d/dx$b;->e:Ljava/util/ArrayDeque;

    .line 222
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/i/d;

    invoke-virtual {v0}, Lio/reactivex/i/d;->onComplete()V

    goto :goto_0

    .line 225
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$b;->a:Lio/reactivex/t;

    invoke-interface {v0}, Lio/reactivex/t;->onComplete()V

    .line 226
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 212
    iget-object v1, p0, Lio/reactivex/d/e/d/dx$b;->e:Ljava/util/ArrayDeque;

    .line 213
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/i/d;

    invoke-virtual {v0, p1}, Lio/reactivex/i/d;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 216
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$b;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 217
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    .line 177
    iget-object v1, p0, Lio/reactivex/d/e/d/dx$b;->e:Ljava/util/ArrayDeque;

    .line 179
    iget-wide v2, p0, Lio/reactivex/d/e/d/dx$b;->f:J

    .line 181
    iget-wide v4, p0, Lio/reactivex/d/e/d/dx$b;->c:J

    .line 183
    rem-long v6, v2, v4

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio/reactivex/d/e/d/dx$b;->g:Z

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 185
    iget v0, p0, Lio/reactivex/d/e/d/dx$b;->d:I

    invoke-static {v0, p0}, Lio/reactivex/i/d;->a(ILjava/lang/Runnable;)Lio/reactivex/i/d;

    move-result-object v0

    .line 186
    invoke-virtual {v1, v0}, Ljava/util/ArrayDeque;->offer(Ljava/lang/Object;)Z

    .line 187
    iget-object v6, p0, Lio/reactivex/d/e/d/dx$b;->a:Lio/reactivex/t;

    invoke-interface {v6, v0}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 190
    :cond_0
    iget-wide v6, p0, Lio/reactivex/d/e/d/dx$b;->h:J

    add-long/2addr v6, v10

    .line 192
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/i/d;

    .line 193
    invoke-virtual {v0, p1}, Lio/reactivex/i/d;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 196
    :cond_1
    iget-wide v8, p0, Lio/reactivex/d/e/d/dx$b;->b:J

    cmp-long v0, v6, v8

    if-ltz v0, :cond_3

    .line 197
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/i/d;

    invoke-virtual {v0}, Lio/reactivex/i/d;->onComplete()V

    .line 198
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lio/reactivex/d/e/d/dx$b;->g:Z

    if-eqz v0, :cond_2

    .line 199
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$b;->i:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 208
    :goto_1
    return-void

    .line 202
    :cond_2
    sub-long v0, v6, v4

    iput-wide v0, p0, Lio/reactivex/d/e/d/dx$b;->h:J

    .line 207
    :goto_2
    add-long v0, v2, v10

    iput-wide v0, p0, Lio/reactivex/d/e/d/dx$b;->f:J

    goto :goto_1

    .line 204
    :cond_3
    iput-wide v6, p0, Lio/reactivex/d/e/d/dx$b;->h:J

    goto :goto_2
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$b;->i:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iput-object p1, p0, Lio/reactivex/d/e/d/dx$b;->i:Lio/reactivex/b/b;

    .line 171
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$b;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 173
    :cond_0
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 241
    iget-boolean v0, p0, Lio/reactivex/d/e/d/dx$b;->g:Z

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lio/reactivex/d/e/d/dx$b;->i:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 245
    :cond_0
    return-void
.end method
