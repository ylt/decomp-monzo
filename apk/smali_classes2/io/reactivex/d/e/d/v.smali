.class public final Lio/reactivex/d/e/d/v;
.super Lio/reactivex/d/e/d/a;
.source "ObservableConcatMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/v$a;,
        Lio/reactivex/d/e/d/v$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TU;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TU;>;>;"
        }
    .end annotation
.end field

.field final c:I

.field final d:Lio/reactivex/d/j/i;


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/c/h;ILio/reactivex/d/j/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/c/h",
            "<-TT;+",
            "Lio/reactivex/r",
            "<+TU;>;>;I",
            "Lio/reactivex/d/j/i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 39
    iput-object p2, p0, Lio/reactivex/d/e/d/v;->b:Lio/reactivex/c/h;

    .line 40
    iput-object p4, p0, Lio/reactivex/d/e/d/v;->d:Lio/reactivex/d/j/i;

    .line 41
    const/16 v0, 0x8

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lio/reactivex/d/e/d/v;->c:I

    .line 42
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TU;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lio/reactivex/d/e/d/v;->a:Lio/reactivex/r;

    iget-object v1, p0, Lio/reactivex/d/e/d/v;->b:Lio/reactivex/c/h;

    invoke-static {v0, p1, v1}, Lio/reactivex/d/e/d/cr;->a(Lio/reactivex/r;Lio/reactivex/t;Lio/reactivex/c/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/d/v;->d:Lio/reactivex/d/j/i;

    sget-object v1, Lio/reactivex/d/j/i;->a:Lio/reactivex/d/j/i;

    if-ne v0, v1, :cond_1

    .line 51
    new-instance v0, Lio/reactivex/f/e;

    invoke-direct {v0, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    .line 52
    iget-object v1, p0, Lio/reactivex/d/e/d/v;->a:Lio/reactivex/r;

    new-instance v2, Lio/reactivex/d/e/d/v$b;

    iget-object v3, p0, Lio/reactivex/d/e/d/v;->b:Lio/reactivex/c/h;

    iget v4, p0, Lio/reactivex/d/e/d/v;->c:I

    invoke-direct {v2, v0, v3, v4}, Lio/reactivex/d/e/d/v$b;-><init>(Lio/reactivex/t;Lio/reactivex/c/h;I)V

    invoke-interface {v1, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0

    .line 54
    :cond_1
    iget-object v1, p0, Lio/reactivex/d/e/d/v;->a:Lio/reactivex/r;

    new-instance v2, Lio/reactivex/d/e/d/v$a;

    iget-object v3, p0, Lio/reactivex/d/e/d/v;->b:Lio/reactivex/c/h;

    iget v4, p0, Lio/reactivex/d/e/d/v;->c:I

    iget-object v0, p0, Lio/reactivex/d/e/d/v;->d:Lio/reactivex/d/j/i;

    sget-object v5, Lio/reactivex/d/j/i;->c:Lio/reactivex/d/j/i;

    if-ne v0, v5, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v2, p1, v3, v4, v0}, Lio/reactivex/d/e/d/v$a;-><init>(Lio/reactivex/t;Lio/reactivex/c/h;IZ)V

    invoke-interface {v1, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
