.class public final Lio/reactivex/d/e/d/cq;
.super Lio/reactivex/d/e/d/a;
.source "ObservableSampleWithObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/d/cq$a;,
        Lio/reactivex/d/e/d/cq$b;,
        Lio/reactivex/d/e/d/cq$d;,
        Lio/reactivex/d/e/d/cq$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/d/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/r",
            "<*>;"
        }
    .end annotation
.end field

.field final c:Z


# direct methods
.method public constructor <init>(Lio/reactivex/r;Lio/reactivex/r;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/r",
            "<TT;>;",
            "Lio/reactivex/r",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lio/reactivex/d/e/d/a;-><init>(Lio/reactivex/r;)V

    .line 31
    iput-object p2, p0, Lio/reactivex/d/e/d/cq;->b:Lio/reactivex/r;

    .line 32
    iput-boolean p3, p0, Lio/reactivex/d/e/d/cq;->c:Z

    .line 33
    return-void
.end method


# virtual methods
.method public subscribeActual(Lio/reactivex/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lio/reactivex/f/e;

    invoke-direct {v0, p1}, Lio/reactivex/f/e;-><init>(Lio/reactivex/t;)V

    .line 38
    iget-boolean v1, p0, Lio/reactivex/d/e/d/cq;->c:Z

    if-eqz v1, :cond_0

    .line 39
    iget-object v1, p0, Lio/reactivex/d/e/d/cq;->a:Lio/reactivex/r;

    new-instance v2, Lio/reactivex/d/e/d/cq$a;

    iget-object v3, p0, Lio/reactivex/d/e/d/cq;->b:Lio/reactivex/r;

    invoke-direct {v2, v0, v3}, Lio/reactivex/d/e/d/cq$a;-><init>(Lio/reactivex/t;Lio/reactivex/r;)V

    invoke-interface {v1, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v1, p0, Lio/reactivex/d/e/d/cq;->a:Lio/reactivex/r;

    new-instance v2, Lio/reactivex/d/e/d/cq$b;

    iget-object v3, p0, Lio/reactivex/d/e/d/cq;->b:Lio/reactivex/r;

    invoke-direct {v2, v0, v3}, Lio/reactivex/d/e/d/cq$b;-><init>(Lio/reactivex/t;Lio/reactivex/r;)V

    invoke-interface {v1, v2}, Lio/reactivex/r;->subscribe(Lio/reactivex/t;)V

    goto :goto_0
.end method
