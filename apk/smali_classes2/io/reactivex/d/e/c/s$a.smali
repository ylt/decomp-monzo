.class final Lio/reactivex/d/e/c/s$a;
.super Ljava/lang/Object;
.source "MaybePeek.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/c/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/j",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/j",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/d/e/c/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/e/c/s",
            "<TT;>;"
        }
    .end annotation
.end field

.field c:Lio/reactivex/b/b;


# direct methods
.method constructor <init>(Lio/reactivex/j;Lio/reactivex/d/e/c/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j",
            "<-TT;>;",
            "Lio/reactivex/d/e/c/s",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lio/reactivex/d/e/c/s$a;->a:Lio/reactivex/j;

    .line 68
    iput-object p2, p0, Lio/reactivex/d/e/c/s$a;->b:Lio/reactivex/d/e/c/s;

    .line 69
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 174
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->b:Lio/reactivex/d/e/c/s;

    iget-object v0, v0, Lio/reactivex/d/e/c/s;->f:Lio/reactivex/c/a;

    invoke-interface {v0}, Lio/reactivex/c/a;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 177
    invoke-static {v0}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 139
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->b:Lio/reactivex/d/e/c/s;

    iget-object v0, v0, Lio/reactivex/d/e/c/s;->d:Lio/reactivex/c/g;

    invoke-interface {v0, p1}, Lio/reactivex/c/g;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    .line 147
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p1}, Lio/reactivex/j;->onError(Ljava/lang/Throwable;)V

    .line 149
    invoke-virtual {p0}, Lio/reactivex/d/e/c/s$a;->a()V

    .line 150
    return-void

    .line 140
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 141
    invoke-static {v1}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 142
    new-instance v0, Lio/reactivex/exceptions/CompositeException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Throwable;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Lio/reactivex/exceptions/CompositeException;-><init>([Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public a_(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-ne v0, v1, :cond_0

    .line 125
    :goto_0
    return-void

    .line 114
    :cond_0
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->b:Lio/reactivex/d/e/c/s;

    iget-object v0, v0, Lio/reactivex/d/e/c/s;->c:Lio/reactivex/c/g;

    invoke-interface {v0, p1}, Lio/reactivex/c/g;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    .line 122
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p1}, Lio/reactivex/j;->a_(Ljava/lang/Object;)V

    .line 124
    invoke-virtual {p0}, Lio/reactivex/d/e/c/s$a;->a()V

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 117
    invoke-virtual {p0, v0}, Lio/reactivex/d/e/c/s$a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 74
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->b:Lio/reactivex/d/e/c/s;

    iget-object v0, v0, Lio/reactivex/d/e/c/s;->g:Lio/reactivex/c/a;

    invoke-interface {v0}, Lio/reactivex/c/a;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 81
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    .line 82
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 77
    invoke-static {v0}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-ne v0, v1, :cond_0

    .line 170
    :goto_0
    return-void

    .line 159
    :cond_0
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->b:Lio/reactivex/d/e/c/s;

    iget-object v0, v0, Lio/reactivex/d/e/c/s;->e:Lio/reactivex/c/a;

    invoke-interface {v0}, Lio/reactivex/c/a;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    .line 167
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->a:Lio/reactivex/j;

    invoke-interface {v0}, Lio/reactivex/j;->onComplete()V

    .line 169
    invoke-virtual {p0}, Lio/reactivex/d/e/c/s$a;->a()V

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 162
    invoke-virtual {p0, v0}, Lio/reactivex/d/e/c/s$a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-ne v0, v1, :cond_0

    .line 130
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 135
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/c/s$a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->b:Lio/reactivex/d/e/c/s;

    iget-object v0, v0, Lio/reactivex/d/e/c/s;->b:Lio/reactivex/c/g;

    invoke-interface {v0, p1}, Lio/reactivex/c/g;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    iput-object p1, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    .line 104
    iget-object v0, p0, Lio/reactivex/d/e/c/s$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p0}, Lio/reactivex/j;->onSubscribe(Lio/reactivex/b/b;)V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 96
    invoke-interface {p1}, Lio/reactivex/b/b;->dispose()V

    .line 97
    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v1, p0, Lio/reactivex/d/e/c/s$a;->c:Lio/reactivex/b/b;

    .line 98
    iget-object v1, p0, Lio/reactivex/d/e/c/s$a;->a:Lio/reactivex/j;

    invoke-static {v0, v1}, Lio/reactivex/d/a/e;->a(Ljava/lang/Throwable;Lio/reactivex/j;)V

    goto :goto_0
.end method
