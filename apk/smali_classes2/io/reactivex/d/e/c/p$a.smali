.class final Lio/reactivex/d/e/c/p$a;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "MaybeObserveOn.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/j;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/c/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/j",
        "<TT;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/j",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/u;

.field c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field d:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lio/reactivex/j;Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j",
            "<-TT;>;",
            "Lio/reactivex/u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 56
    iput-object p1, p0, Lio/reactivex/d/e/c/p$a;->a:Lio/reactivex/j;

    .line 57
    iput-object p2, p0, Lio/reactivex/d/e/c/p$a;->b:Lio/reactivex/u;

    .line 58
    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 79
    iput-object p1, p0, Lio/reactivex/d/e/c/p$a;->c:Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lio/reactivex/d/e/c/p$a;->b:Lio/reactivex/u;

    invoke-virtual {v0, p0}, Lio/reactivex/u;->a(Ljava/lang/Runnable;)Lio/reactivex/b/b;

    move-result-object v0

    invoke-static {p0, v0}, Lio/reactivex/d/a/d;->c(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 81
    return-void
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 62
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 63
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lio/reactivex/d/e/c/p$a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;)Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lio/reactivex/d/e/c/p$a;->b:Lio/reactivex/u;

    invoke-virtual {v0, p0}, Lio/reactivex/u;->a(Ljava/lang/Runnable;)Lio/reactivex/b/b;

    move-result-object v0

    invoke-static {p0, v0}, Lio/reactivex/d/a/d;->c(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 92
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 85
    iput-object p1, p0, Lio/reactivex/d/e/c/p$a;->d:Ljava/lang/Throwable;

    .line 86
    iget-object v0, p0, Lio/reactivex/d/e/c/p$a;->b:Lio/reactivex/u;

    invoke-virtual {v0, p0}, Lio/reactivex/u;->a(Ljava/lang/Runnable;)Lio/reactivex/b/b;

    move-result-object v0

    invoke-static {p0, v0}, Lio/reactivex/d/a/d;->c(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 87
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 72
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lio/reactivex/d/e/c/p$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p0}, Lio/reactivex/j;->onSubscribe(Lio/reactivex/b/b;)V

    .line 75
    :cond_0
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lio/reactivex/d/e/c/p$a;->d:Ljava/lang/Throwable;

    .line 97
    if-eqz v0, :cond_0

    .line 98
    iput-object v1, p0, Lio/reactivex/d/e/c/p$a;->d:Ljava/lang/Throwable;

    .line 99
    iget-object v1, p0, Lio/reactivex/d/e/c/p$a;->a:Lio/reactivex/j;

    invoke-interface {v1, v0}, Lio/reactivex/j;->onError(Ljava/lang/Throwable;)V

    .line 109
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/c/p$a;->c:Ljava/lang/Object;

    .line 102
    if-eqz v0, :cond_1

    .line 103
    iput-object v1, p0, Lio/reactivex/d/e/c/p$a;->c:Ljava/lang/Object;

    .line 104
    iget-object v1, p0, Lio/reactivex/d/e/c/p$a;->a:Lio/reactivex/j;

    invoke-interface {v1, v0}, Lio/reactivex/j;->a_(Ljava/lang/Object;)V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lio/reactivex/d/e/c/p$a;->a:Lio/reactivex/j;

    invoke-interface {v0}, Lio/reactivex/j;->onComplete()V

    goto :goto_0
.end method
