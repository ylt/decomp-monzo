.class public final Lio/reactivex/d/e/c/t;
.super Lio/reactivex/d/e/c/a;
.source "MaybeSubscribeOn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/c/t$a;,
        Lio/reactivex/d/e/c/t$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/e/c/a",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field final b:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Lio/reactivex/l;Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/l",
            "<TT;>;",
            "Lio/reactivex/u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lio/reactivex/d/e/c/a;-><init>(Lio/reactivex/l;)V

    .line 32
    iput-object p2, p0, Lio/reactivex/d/e/c/t;->b:Lio/reactivex/u;

    .line 33
    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/j;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lio/reactivex/d/e/c/t$a;

    invoke-direct {v0, p1}, Lio/reactivex/d/e/c/t$a;-><init>(Lio/reactivex/j;)V

    .line 38
    invoke-interface {p1, v0}, Lio/reactivex/j;->onSubscribe(Lio/reactivex/b/b;)V

    .line 40
    iget-object v1, v0, Lio/reactivex/d/e/c/t$a;->a:Lio/reactivex/d/a/k;

    iget-object v2, p0, Lio/reactivex/d/e/c/t;->b:Lio/reactivex/u;

    new-instance v3, Lio/reactivex/d/e/c/t$b;

    iget-object v4, p0, Lio/reactivex/d/e/c/t;->a:Lio/reactivex/l;

    invoke-direct {v3, v0, v4}, Lio/reactivex/d/e/c/t$b;-><init>(Lio/reactivex/j;Lio/reactivex/l;)V

    invoke-virtual {v2, v3}, Lio/reactivex/u;->a(Ljava/lang/Runnable;)Lio/reactivex/b/b;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/d/a/k;->b(Lio/reactivex/b/b;)Z

    .line 41
    return-void
.end method
