.class final Lio/reactivex/d/e/c/t$a;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "MaybeSubscribeOn.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/c/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/j",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/a/k;

.field final b:Lio/reactivex/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/j",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 69
    iput-object p1, p0, Lio/reactivex/d/e/c/t$a;->b:Lio/reactivex/j;

    .line 70
    new-instance v0, Lio/reactivex/d/a/k;

    invoke-direct {v0}, Lio/reactivex/d/a/k;-><init>()V

    iput-object v0, p0, Lio/reactivex/d/e/c/t$a;->a:Lio/reactivex/d/a/k;

    .line 71
    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lio/reactivex/d/e/c/t$a;->b:Lio/reactivex/j;

    invoke-interface {v0, p1}, Lio/reactivex/j;->a_(Ljava/lang/Object;)V

    .line 92
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 75
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 76
    iget-object v0, p0, Lio/reactivex/d/e/c/t$a;->a:Lio/reactivex/d/a/k;

    invoke-virtual {v0}, Lio/reactivex/d/a/k;->dispose()V

    .line 77
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lio/reactivex/d/e/c/t$a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;)Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lio/reactivex/d/e/c/t$a;->b:Lio/reactivex/j;

    invoke-interface {v0}, Lio/reactivex/j;->onComplete()V

    .line 102
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lio/reactivex/d/e/c/t$a;->b:Lio/reactivex/j;

    invoke-interface {v0, p1}, Lio/reactivex/j;->onError(Ljava/lang/Throwable;)V

    .line 97
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 86
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 87
    return-void
.end method
