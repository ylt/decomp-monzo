.class final Lio/reactivex/d/e/c/c$a;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "MaybeCreate.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/i;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/j",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 59
    iput-object p1, p0, Lio/reactivex/d/e/c/c$a;->a:Lio/reactivex/j;

    .line 60
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lio/reactivex/d/e/c/c$a;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-eq v0, v1, :cond_0

    .line 116
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/c/c$a;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    .line 117
    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-eq v0, v1, :cond_0

    .line 119
    :try_start_0
    iget-object v1, p0, Lio/reactivex/d/e/c/c$a;->a:Lio/reactivex/j;

    invoke-interface {v1}, Lio/reactivex/j;->onComplete()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    if-eqz v0, :cond_0

    .line 122
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 127
    :cond_0
    return-void

    .line 121
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 122
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    :cond_1
    throw v1
.end method

.method public a(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 131
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    .line 132
    return-void
.end method

.method public a(Lio/reactivex/c/f;)V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lio/reactivex/d/a/b;

    invoke-direct {v0, p1}, Lio/reactivex/d/a/b;-><init>(Lio/reactivex/c/f;)V

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/c/c$a;->a(Lio/reactivex/b/b;)V

    .line 137
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p0}, Lio/reactivex/d/e/c/c$a;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-eq v0, v1, :cond_0

    .line 68
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/c/c$a;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    .line 69
    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-eq v0, v1, :cond_0

    .line 71
    if-nez p1, :cond_1

    .line 72
    :try_start_0
    iget-object v1, p0, Lio/reactivex/d/e/c/c$a;->a:Lio/reactivex/j;

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "onSuccess called with null. Null values are generally not allowed in 2.x operators and sources."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lio/reactivex/j;->onError(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :goto_0
    if-eqz v0, :cond_0

    .line 78
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 83
    :cond_0
    return-void

    .line 74
    :cond_1
    :try_start_1
    iget-object v1, p0, Lio/reactivex/d/e/c/c$a;->a:Lio/reactivex/j;

    invoke-interface {v1, p1}, Lio/reactivex/j;->a_(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 77
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    .line 78
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    :cond_2
    throw v1
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/c/c$a;->b(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 90
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/Throwable;)Z
    .locals 2

    .prologue
    .line 94
    if-nez p1, :cond_0

    .line 95
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "onError called with null. Null values are generally not allowed in 2.x operators and sources."

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 97
    :cond_0
    invoke-virtual {p0}, Lio/reactivex/d/e/c/c$a;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-eq v0, v1, :cond_3

    .line 98
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/c/c$a;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    .line 99
    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    if-eq v0, v1, :cond_3

    .line 101
    :try_start_0
    iget-object v1, p0, Lio/reactivex/d/e/c/c$a;->a:Lio/reactivex/j;

    invoke-interface {v1, p1}, Lio/reactivex/j;->onError(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    if-eqz v0, :cond_1

    .line 104
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 107
    :cond_1
    const/4 v0, 0x1

    .line 110
    :goto_0
    return v0

    .line 103
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    .line 104
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    :cond_2
    throw v1

    .line 110
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 141
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 142
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lio/reactivex/d/e/c/c$a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;)Z

    move-result v0

    return v0
.end method
