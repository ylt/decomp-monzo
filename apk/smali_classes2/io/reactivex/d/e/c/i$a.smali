.class final Lio/reactivex/d/e/c/i$a;
.super Ljava/lang/Object;
.source "MaybeFromSingle.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/x;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/c/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/x",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/j",
            "<-TT;>;"
        }
    .end annotation
.end field

.field b:Lio/reactivex/b/b;


# direct methods
.method constructor <init>(Lio/reactivex/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lio/reactivex/d/e/c/i$a;->a:Lio/reactivex/j;

    .line 51
    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/i$a;->b:Lio/reactivex/b/b;

    .line 76
    iget-object v0, p0, Lio/reactivex/d/e/c/i$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p1}, Lio/reactivex/j;->a_(Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lio/reactivex/d/e/c/i$a;->b:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 56
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/i$a;->b:Lio/reactivex/b/b;

    .line 57
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lio/reactivex/d/e/c/i$a;->b:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/i$a;->b:Lio/reactivex/b/b;

    .line 82
    iget-object v0, p0, Lio/reactivex/d/e/c/i$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p1}, Lio/reactivex/j;->onError(Ljava/lang/Throwable;)V

    .line 83
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lio/reactivex/d/e/c/i$a;->b:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iput-object p1, p0, Lio/reactivex/d/e/c/i$a;->b:Lio/reactivex/b/b;

    .line 69
    iget-object v0, p0, Lio/reactivex/d/e/c/i$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p0}, Lio/reactivex/j;->onSubscribe(Lio/reactivex/b/b;)V

    .line 71
    :cond_0
    return-void
.end method
