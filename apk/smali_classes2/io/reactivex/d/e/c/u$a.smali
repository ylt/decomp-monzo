.class final Lio/reactivex/d/e/c/u$a;
.super Lio/reactivex/d/d/k;
.source "MaybeToObservable.java"

# interfaces
.implements Lio/reactivex/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/c/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/d/d/k",
        "<TT;>;",
        "Lio/reactivex/j",
        "<TT;>;"
    }
.end annotation


# instance fields
.field c:Lio/reactivex/b/b;


# direct methods
.method constructor <init>(Lio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lio/reactivex/d/d/k;-><init>(Lio/reactivex/t;)V

    .line 55
    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/c/u$a;->b(Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lio/reactivex/d/d/k;->dispose()V

    .line 84
    iget-object v0, p0, Lio/reactivex/d/e/c/u$a;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 85
    return-void
.end method

.method public onComplete()V
    .locals 0

    .prologue
    .line 78
    invoke-virtual {p0}, Lio/reactivex/d/e/c/u$a;->d()V

    .line 79
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lio/reactivex/d/e/c/u$a;->a(Ljava/lang/Throwable;)V

    .line 74
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lio/reactivex/d/e/c/u$a;->c:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iput-object p1, p0, Lio/reactivex/d/e/c/u$a;->c:Lio/reactivex/b/b;

    .line 62
    iget-object v0, p0, Lio/reactivex/d/e/c/u$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 64
    :cond_0
    return-void
.end method
