.class final Lio/reactivex/d/e/c/f$a;
.super Ljava/lang/Object;
.source "MaybeFilterSingle.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/x;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/c/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/x",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/j",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-TT;>;"
        }
    .end annotation
.end field

.field c:Lio/reactivex/b/b;


# direct methods
.method constructor <init>(Lio/reactivex/j;Lio/reactivex/c/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/j",
            "<-TT;>;",
            "Lio/reactivex/c/q",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lio/reactivex/d/e/c/f$a;->a:Lio/reactivex/j;

    .line 53
    iput-object p2, p0, Lio/reactivex/d/e/c/f$a;->b:Lio/reactivex/c/q;

    .line 54
    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 82
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/c/f$a;->b:Lio/reactivex/c/q;

    invoke-interface {v0, p1}, Lio/reactivex/c/q;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 89
    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lio/reactivex/d/e/c/f$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p1}, Lio/reactivex/j;->a_(Ljava/lang/Object;)V

    .line 94
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    .line 84
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 85
    iget-object v1, p0, Lio/reactivex/d/e/c/f$a;->a:Lio/reactivex/j;

    invoke-interface {v1, v0}, Lio/reactivex/j;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 92
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/e/c/f$a;->a:Lio/reactivex/j;

    invoke-interface {v0}, Lio/reactivex/j;->onComplete()V

    goto :goto_0
.end method

.method public dispose()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lio/reactivex/d/e/c/f$a;->c:Lio/reactivex/b/b;

    .line 59
    sget-object v1, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v1, p0, Lio/reactivex/d/e/c/f$a;->c:Lio/reactivex/b/b;

    .line 60
    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 61
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lio/reactivex/d/e/c/f$a;->c:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lio/reactivex/d/e/c/f$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p1}, Lio/reactivex/j;->onError(Ljava/lang/Throwable;)V

    .line 99
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lio/reactivex/d/e/c/f$a;->c:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iput-object p1, p0, Lio/reactivex/d/e/c/f$a;->c:Lio/reactivex/b/b;

    .line 73
    iget-object v0, p0, Lio/reactivex/d/e/c/f$a;->a:Lio/reactivex/j;

    invoke-interface {v0, p0}, Lio/reactivex/j;->onSubscribe(Lio/reactivex/b/b;)V

    .line 75
    :cond_0
    return-void
.end method
