.class final Lio/reactivex/d/e/c/m$a;
.super Ljava/lang/Object;
.source "MaybeIsEmptySingle.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/c/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/j",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/x",
            "<-",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field b:Lio/reactivex/b/b;


# direct methods
.method constructor <init>(Lio/reactivex/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lio/reactivex/d/e/c/m$a;->a:Lio/reactivex/x;

    .line 61
    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 85
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/m$a;->b:Lio/reactivex/b/b;

    .line 86
    iget-object v0, p0, Lio/reactivex/d/e/c/m$a;->a:Lio/reactivex/x;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/x;->a_(Ljava/lang/Object;)V

    .line 87
    return-void
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lio/reactivex/d/e/c/m$a;->b:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->dispose()V

    .line 66
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/m$a;->b:Lio/reactivex/b/b;

    .line 67
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lio/reactivex/d/e/c/m$a;->b:Lio/reactivex/b/b;

    invoke-interface {v0}, Lio/reactivex/b/b;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/m$a;->b:Lio/reactivex/b/b;

    .line 98
    iget-object v0, p0, Lio/reactivex/d/e/c/m$a;->a:Lio/reactivex/x;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/x;->a_(Ljava/lang/Object;)V

    .line 99
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lio/reactivex/d/a/d;->a:Lio/reactivex/d/a/d;

    iput-object v0, p0, Lio/reactivex/d/e/c/m$a;->b:Lio/reactivex/b/b;

    .line 92
    iget-object v0, p0, Lio/reactivex/d/e/c/m$a;->a:Lio/reactivex/x;

    invoke-interface {v0, p1}, Lio/reactivex/x;->onError(Ljava/lang/Throwable;)V

    .line 93
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lio/reactivex/d/e/c/m$a;->b:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iput-object p1, p0, Lio/reactivex/d/e/c/m$a;->b:Lio/reactivex/b/b;

    .line 79
    iget-object v0, p0, Lio/reactivex/d/e/c/m$a;->a:Lio/reactivex/x;

    invoke-interface {v0, p0}, Lio/reactivex/x;->onSubscribe(Lio/reactivex/b/b;)V

    .line 81
    :cond_0
    return-void
.end method
