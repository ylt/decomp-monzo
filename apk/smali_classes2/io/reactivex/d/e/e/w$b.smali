.class final Lio/reactivex/d/e/e/w$b;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "SingleZipArray.java"

# interfaces
.implements Lio/reactivex/b/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex/d/e/e/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex/b/b;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/x",
            "<-TR;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/c/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/h",
            "<-[",
            "Ljava/lang/Object;",
            "+TR;>;"
        }
    .end annotation
.end field

.field final c:[Lio/reactivex/d/e/e/w$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lio/reactivex/d/e/e/w$c",
            "<TT;>;"
        }
    .end annotation
.end field

.field final d:[Ljava/lang/Object;


# direct methods
.method constructor <init>(Lio/reactivex/x;ILio/reactivex/c/h;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-TR;>;I",
            "Lio/reactivex/c/h",
            "<-[",
            "Ljava/lang/Object;",
            "+TR;>;)V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 84
    iput-object p1, p0, Lio/reactivex/d/e/e/w$b;->a:Lio/reactivex/x;

    .line 85
    iput-object p3, p0, Lio/reactivex/d/e/e/w$b;->b:Lio/reactivex/c/h;

    .line 86
    new-array v1, p2, [Lio/reactivex/d/e/e/w$c;

    .line 87
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 88
    new-instance v2, Lio/reactivex/d/e/e/w$c;

    invoke-direct {v2, p0, v0}, Lio/reactivex/d/e/e/w$c;-><init>(Lio/reactivex/d/e/e/w$b;I)V

    aput-object v2, v1, v0

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_0
    iput-object v1, p0, Lio/reactivex/d/e/e/w$b;->c:[Lio/reactivex/d/e/e/w$c;

    .line 91
    new-array v0, p2, [Ljava/lang/Object;

    iput-object v0, p0, Lio/reactivex/d/e/e/w$b;->d:[Ljava/lang/Object;

    .line 92
    return-void
.end method


# virtual methods
.method a(I)V
    .locals 4

    .prologue
    .line 126
    iget-object v1, p0, Lio/reactivex/d/e/e/w$b;->c:[Lio/reactivex/d/e/e/w$c;

    .line 127
    array-length v2, v1

    .line 128
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 129
    aget-object v3, v1, v0

    invoke-virtual {v3}, Lio/reactivex/d/e/e/w$c;->a()V

    .line 128
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    :cond_0
    add-int/lit8 v0, p1, 0x1

    :goto_1
    if-ge v0, v2, :cond_1

    .line 132
    aget-object v3, v1, v0

    invoke-virtual {v3}, Lio/reactivex/d/e/e/w$c;->a()V

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 134
    :cond_1
    return-void
.end method

.method a(Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lio/reactivex/d/e/e/w$b;->d:[Ljava/lang/Object;

    aput-object p1, v0, p2

    .line 110
    invoke-virtual {p0}, Lio/reactivex/d/e/e/w$b;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 114
    :try_start_0
    iget-object v0, p0, Lio/reactivex/d/e/e/w$b;->b:Lio/reactivex/c/h;

    iget-object v1, p0, Lio/reactivex/d/e/e/w$b;->d:[Ljava/lang/Object;

    invoke-interface {v0, v1}, Lio/reactivex/c/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "The zipper returned a null value"

    invoke-static {v0, v1}, Lio/reactivex/d/b/b;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 121
    iget-object v1, p0, Lio/reactivex/d/e/e/w$b;->a:Lio/reactivex/x;

    invoke-interface {v1, v0}, Lio/reactivex/x;->a_(Ljava/lang/Object;)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 116
    invoke-static {v0}, Lio/reactivex/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 117
    iget-object v1, p0, Lio/reactivex/d/e/e/w$b;->a:Lio/reactivex/x;

    invoke-interface {v1, v0}, Lio/reactivex/x;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method a(Ljava/lang/Throwable;I)V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/reactivex/d/e/e/w$b;->getAndSet(I)I

    move-result v0

    if-lez v0, :cond_0

    .line 138
    invoke-virtual {p0, p2}, Lio/reactivex/d/e/e/w$b;->a(I)V

    .line 139
    iget-object v0, p0, Lio/reactivex/d/e/e/w$b;->a:Lio/reactivex/x;

    invoke-interface {v0, p1}, Lio/reactivex/x;->onError(Ljava/lang/Throwable;)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-virtual {p0, v0}, Lio/reactivex/d/e/e/w$b;->getAndSet(I)I

    move-result v1

    if-lez v1, :cond_0

    .line 102
    iget-object v1, p0, Lio/reactivex/d/e/e/w$b;->c:[Lio/reactivex/d/e/e/w$c;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 103
    invoke-virtual {v3}, Lio/reactivex/d/e/e/w$c;->a()V

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lio/reactivex/d/e/e/w$b;->get()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
