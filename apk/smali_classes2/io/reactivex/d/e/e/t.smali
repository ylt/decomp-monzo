.class public final Lio/reactivex/d/e/e/t;
.super Lio/reactivex/v;
.source "SingleSubscribeOn.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex/d/e/e/t$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/v",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/z;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/z",
            "<+TT;>;"
        }
    .end annotation
.end field

.field final b:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Lio/reactivex/z;Lio/reactivex/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/z",
            "<+TT;>;",
            "Lio/reactivex/u;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Lio/reactivex/v;-><init>()V

    .line 28
    iput-object p1, p0, Lio/reactivex/d/e/e/t;->a:Lio/reactivex/z;

    .line 29
    iput-object p2, p0, Lio/reactivex/d/e/e/t;->b:Lio/reactivex/u;

    .line 30
    return-void
.end method


# virtual methods
.method protected b(Lio/reactivex/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/x",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lio/reactivex/d/e/e/t$a;

    iget-object v1, p0, Lio/reactivex/d/e/e/t;->a:Lio/reactivex/z;

    invoke-direct {v0, p1, v1}, Lio/reactivex/d/e/e/t$a;-><init>(Lio/reactivex/x;Lio/reactivex/z;)V

    .line 35
    invoke-interface {p1, v0}, Lio/reactivex/x;->onSubscribe(Lio/reactivex/b/b;)V

    .line 37
    iget-object v1, p0, Lio/reactivex/d/e/e/t;->b:Lio/reactivex/u;

    invoke-virtual {v1, v0}, Lio/reactivex/u;->a(Ljava/lang/Runnable;)Lio/reactivex/b/b;

    move-result-object v1

    .line 39
    iget-object v0, v0, Lio/reactivex/d/e/e/t$a;->b:Lio/reactivex/d/a/k;

    invoke-virtual {v0, v1}, Lio/reactivex/d/a/k;->b(Lio/reactivex/b/b;)Z

    .line 41
    return-void
.end method
