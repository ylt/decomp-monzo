.class public final enum Lio/reactivex/d/j/g;
.super Ljava/lang/Enum;
.source "EmptyComponent.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/c;
.implements Lio/reactivex/g;
.implements Lio/reactivex/j;
.implements Lio/reactivex/t;
.implements Lio/reactivex/x;
.implements Lorg/a/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/reactivex/d/j/g;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/c;",
        "Lio/reactivex/g",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lio/reactivex/j",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lio/reactivex/t",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lio/reactivex/x",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lorg/a/c;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/reactivex/d/j/g;

.field private static final synthetic b:[Lio/reactivex/d/j/g;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    new-instance v0, Lio/reactivex/d/j/g;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lio/reactivex/d/j/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/reactivex/d/j/g;->a:Lio/reactivex/d/j/g;

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [Lio/reactivex/d/j/g;

    sget-object v1, Lio/reactivex/d/j/g;->a:Lio/reactivex/d/j/g;

    aput-object v1, v0, v2

    sput-object v0, Lio/reactivex/d/j/g;->b:[Lio/reactivex/d/j/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static b()Lio/reactivex/t;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/t",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 36
    sget-object v0, Lio/reactivex/d/j/g;->a:Lio/reactivex/d/j/g;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lio/reactivex/d/j/g;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lio/reactivex/d/j/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/reactivex/d/j/g;

    return-object v0
.end method

.method public static values()[Lio/reactivex/d/j/g;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lio/reactivex/d/j/g;->b:[Lio/reactivex/d/j/g;

    invoke-virtual {v0}, [Lio/reactivex/d/j/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/reactivex/d/j/g;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public a(Lorg/a/c;)V
    .locals 0

    .prologue
    .line 66
    invoke-interface {p1}, Lorg/a/c;->a()V

    .line 67
    return-void
.end method

.method public a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public onComplete()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 76
    invoke-static {p1}, Lio/reactivex/g/a;->a(Ljava/lang/Throwable;)V

    .line 77
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 0

    .prologue
    .line 61
    invoke-interface {p1}, Lio/reactivex/b/b;->dispose()V

    .line 62
    return-void
.end method
