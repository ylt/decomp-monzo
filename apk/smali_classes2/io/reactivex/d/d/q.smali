.class public final Lio/reactivex/d/d/q;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "InnerQueuedObserver.java"

# interfaces
.implements Lio/reactivex/b/b;
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Lio/reactivex/b/b;",
        ">;",
        "Lio/reactivex/b/b;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/d/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/d/r",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:I

.field c:Lio/reactivex/d/c/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/c/i",
            "<TT;>;"
        }
    .end annotation
.end field

.field volatile d:Z

.field e:I


# direct methods
.method public constructor <init>(Lio/reactivex/d/d/r;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/d/r",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 48
    iput-object p1, p0, Lio/reactivex/d/d/q;->a:Lio/reactivex/d/d/r;

    .line 49
    iput p2, p0, Lio/reactivex/d/d/q;->b:I

    .line 50
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lio/reactivex/d/d/q;->d:Z

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex/d/d/q;->d:Z

    .line 113
    return-void
.end method

.method public c()Lio/reactivex/d/c/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/d/c/i",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lio/reactivex/d/d/q;->c:Lio/reactivex/d/c/i;

    return-object v0
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 99
    invoke-static {p0}, Lio/reactivex/d/a/d;->a(Ljava/util/concurrent/atomic/AtomicReference;)Z

    .line 100
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lio/reactivex/d/d/q;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/b/b;

    invoke-static {v0}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;)Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lio/reactivex/d/d/q;->a:Lio/reactivex/d/d/r;

    invoke-interface {v0, p0}, Lio/reactivex/d/d/r;->a(Lio/reactivex/d/d/q;)V

    .line 95
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lio/reactivex/d/d/q;->a:Lio/reactivex/d/d/r;

    invoke-interface {v0, p0, p1}, Lio/reactivex/d/d/r;->a(Lio/reactivex/d/d/q;Ljava/lang/Throwable;)V

    .line 90
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 80
    iget v0, p0, Lio/reactivex/d/d/q;->e:I

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lio/reactivex/d/d/q;->a:Lio/reactivex/d/d/r;

    invoke-interface {v0, p0, p1}, Lio/reactivex/d/d/r;->a(Lio/reactivex/d/d/q;Ljava/lang/Object;)V

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lio/reactivex/d/d/q;->a:Lio/reactivex/d/d/r;

    invoke-interface {v0}, Lio/reactivex/d/d/r;->a()V

    goto :goto_0
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 54
    invoke-static {p0, p1}, Lio/reactivex/d/a/d;->b(Ljava/util/concurrent/atomic/AtomicReference;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    instance-of v0, p1, Lio/reactivex/d/c/d;

    if-eqz v0, :cond_2

    .line 57
    check-cast p1, Lio/reactivex/d/c/d;

    .line 59
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Lio/reactivex/d/c/d;->a(I)I

    move-result v0

    .line 60
    if-ne v0, v1, :cond_1

    .line 61
    iput v0, p0, Lio/reactivex/d/d/q;->e:I

    .line 62
    iput-object p1, p0, Lio/reactivex/d/d/q;->c:Lio/reactivex/d/c/i;

    .line 63
    iput-boolean v1, p0, Lio/reactivex/d/d/q;->d:Z

    .line 64
    iget-object v0, p0, Lio/reactivex/d/d/q;->a:Lio/reactivex/d/d/r;

    invoke-interface {v0, p0}, Lio/reactivex/d/d/r;->a(Lio/reactivex/d/d/q;)V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 68
    iput v0, p0, Lio/reactivex/d/d/q;->e:I

    .line 69
    iput-object p1, p0, Lio/reactivex/d/d/q;->c:Lio/reactivex/d/c/i;

    goto :goto_0

    .line 74
    :cond_2
    iget v0, p0, Lio/reactivex/d/d/q;->b:I

    neg-int v0, v0

    invoke-static {v0}, Lio/reactivex/d/j/r;->a(I)Lio/reactivex/d/c/i;

    move-result-object v0

    iput-object v0, p0, Lio/reactivex/d/d/q;->c:Lio/reactivex/d/c/i;

    goto :goto_0
.end method
