.class public final Lio/reactivex/d/d/o;
.super Ljava/lang/Object;
.source "FullArbiterObserver.java"

# interfaces
.implements Lio/reactivex/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/t",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/d/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/d/a/j",
            "<TT;>;"
        }
    .end annotation
.end field

.field b:Lio/reactivex/b/b;


# direct methods
.method public constructor <init>(Lio/reactivex/d/a/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/d/a/j",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lio/reactivex/d/d/o;->a:Lio/reactivex/d/a/j;

    .line 32
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lio/reactivex/d/d/o;->a:Lio/reactivex/d/a/j;

    iget-object v1, p0, Lio/reactivex/d/d/o;->b:Lio/reactivex/b/b;

    invoke-virtual {v0, v1}, Lio/reactivex/d/a/j;->b(Lio/reactivex/b/b;)V

    .line 55
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lio/reactivex/d/d/o;->a:Lio/reactivex/d/a/j;

    iget-object v1, p0, Lio/reactivex/d/d/o;->b:Lio/reactivex/b/b;

    invoke-virtual {v0, p1, v1}, Lio/reactivex/d/a/j;->a(Ljava/lang/Throwable;Lio/reactivex/b/b;)V

    .line 50
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lio/reactivex/d/d/o;->a:Lio/reactivex/d/a/j;

    iget-object v1, p0, Lio/reactivex/d/d/o;->b:Lio/reactivex/b/b;

    invoke-virtual {v0, p1, v1}, Lio/reactivex/d/a/j;->a(Ljava/lang/Object;Lio/reactivex/b/b;)Z

    .line 45
    return-void
.end method

.method public onSubscribe(Lio/reactivex/b/b;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lio/reactivex/d/d/o;->b:Lio/reactivex/b/b;

    invoke-static {v0, p1}, Lio/reactivex/d/a/d;->a(Lio/reactivex/b/b;Lio/reactivex/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    iput-object p1, p0, Lio/reactivex/d/d/o;->b:Lio/reactivex/b/b;

    .line 38
    iget-object v0, p0, Lio/reactivex/d/d/o;->a:Lio/reactivex/d/a/j;

    invoke-virtual {v0, p1}, Lio/reactivex/d/a/j;->a(Lio/reactivex/b/b;)Z

    .line 40
    :cond_0
    return-void
.end method
