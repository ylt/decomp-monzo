.class public Lio/realm/m;
.super Lco/uk/getmondo/payments/a/a/a;
.source "DirectDebitRealmProxy.java"

# interfaces
.implements Lio/realm/internal/l;
.implements Lio/realm/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/m$a;
    }
.end annotation


# static fields
.field private static final d:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lio/realm/m$a;

.field private c:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/payments/a/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 94
    invoke-static {}, Lio/realm/m;->r()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/m;->d:Lio/realm/internal/OsObjectSchemaInfo;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 98
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    const-string v1, "accountId"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    const-string v1, "payerSortCode"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    const-string v1, "payerAccountNumber"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    const-string v1, "payerName"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    const-string v1, "serviceUserNumber"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v1, "serviceUserName"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    const-string v1, "reference"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const-string v1, "active"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v1, "_created"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v1, "_lastCollected"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/m;->e:Ljava/util/List;

    .line 110
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Lco/uk/getmondo/payments/a/a/a;-><init>()V

    .line 113
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 114
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;Ljava/util/Map;)J
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/a;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 885
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 886
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 942
    :cond_0
    :goto_0
    return-wide v4

    .line 888
    :cond_1
    const-class v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 889
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 890
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lio/realm/m$a;

    .line 891
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 892
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->e()Ljava/lang/String;

    move-result-object v2

    .line 894
    if-nez v2, :cond_a

    .line 895
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 899
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v6, v4, v10

    if-nez v6, :cond_b

    .line 900
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 904
    :goto_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 905
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->f()Ljava/lang/String;

    move-result-object v6

    .line 906
    if-eqz v6, :cond_2

    .line 907
    iget-wide v2, v8, Lio/realm/m$a;->b:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_2
    move-object v2, p1

    .line 909
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->g()Ljava/lang/String;

    move-result-object v6

    .line 910
    if-eqz v6, :cond_3

    .line 911
    iget-wide v2, v8, Lio/realm/m$a;->c:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_3
    move-object v2, p1

    .line 913
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->h()Ljava/lang/String;

    move-result-object v6

    .line 914
    if-eqz v6, :cond_4

    .line 915
    iget-wide v2, v8, Lio/realm/m$a;->d:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_4
    move-object v2, p1

    .line 917
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->i()Ljava/lang/String;

    move-result-object v6

    .line 918
    if-eqz v6, :cond_5

    .line 919
    iget-wide v2, v8, Lio/realm/m$a;->e:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_5
    move-object v2, p1

    .line 921
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->j()Ljava/lang/String;

    move-result-object v6

    .line 922
    if-eqz v6, :cond_6

    .line 923
    iget-wide v2, v8, Lio/realm/m$a;->f:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_6
    move-object v2, p1

    .line 925
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->t_()Ljava/lang/String;

    move-result-object v6

    .line 926
    if-eqz v6, :cond_7

    .line 927
    iget-wide v2, v8, Lio/realm/m$a;->g:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_7
    move-object v2, p1

    .line 929
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->l()Ljava/lang/String;

    move-result-object v6

    .line 930
    if-eqz v6, :cond_8

    .line 931
    iget-wide v2, v8, Lio/realm/m$a;->h:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 933
    :cond_8
    iget-wide v2, v8, Lio/realm/m$a;->i:J

    move-object v6, p1

    check-cast v6, Lio/realm/n;

    invoke-interface {v6}, Lio/realm/n;->m()Z

    move-result v6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v2, p1

    .line 934
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->n()Ljava/lang/String;

    move-result-object v6

    .line 935
    if-eqz v6, :cond_9

    .line 936
    iget-wide v2, v8, Lio/realm/m$a;->j:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 938
    :cond_9
    check-cast p1, Lio/realm/n;

    invoke-interface {p1}, Lio/realm/n;->o()Ljava/lang/String;

    move-result-object v6

    .line 939
    if-eqz v6, :cond_0

    .line 940
    iget-wide v2, v8, Lio/realm/m$a;->k:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 897
    :cond_a
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 902
    :cond_b
    invoke-static {v2}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto/16 :goto_2
.end method

.method public static a(Lco/uk/getmondo/payments/a/a/a;IILjava/util/Map;)Lco/uk/getmondo/payments/a/a/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/payments/a/a/a;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/payments/a/a/a;"
        }
    .end annotation

    .prologue
    .line 1175
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 1176
    :cond_0
    const/4 v0, 0x0

    .line 1204
    :goto_0
    return-object v0

    .line 1178
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 1180
    if-nez v0, :cond_2

    .line 1181
    new-instance v1, Lco/uk/getmondo/payments/a/a/a;

    invoke-direct {v1}, Lco/uk/getmondo/payments/a/a/a;-><init>()V

    .line 1182
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 1191
    check-cast v0, Lio/realm/n;

    .line 1192
    check-cast p0, Lio/realm/n;

    .line 1193
    invoke-interface {p0}, Lio/realm/n;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->a(Ljava/lang/String;)V

    .line 1194
    invoke-interface {p0}, Lio/realm/n;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->b(Ljava/lang/String;)V

    .line 1195
    invoke-interface {p0}, Lio/realm/n;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->c(Ljava/lang/String;)V

    .line 1196
    invoke-interface {p0}, Lio/realm/n;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->d(Ljava/lang/String;)V

    .line 1197
    invoke-interface {p0}, Lio/realm/n;->i()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->e(Ljava/lang/String;)V

    .line 1198
    invoke-interface {p0}, Lio/realm/n;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->f(Ljava/lang/String;)V

    .line 1199
    invoke-interface {p0}, Lio/realm/n;->t_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->g(Ljava/lang/String;)V

    .line 1200
    invoke-interface {p0}, Lio/realm/n;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->h(Ljava/lang/String;)V

    .line 1201
    invoke-interface {p0}, Lio/realm/n;->m()Z

    move-result v2

    invoke-interface {v0, v2}, Lio/realm/n;->a(Z)V

    .line 1202
    invoke-interface {p0}, Lio/realm/n;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->i(Ljava/lang/String;)V

    .line 1203
    invoke-interface {p0}, Lio/realm/n;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/n;->j(Ljava/lang/String;)V

    move-object v0, v1

    .line 1204
    goto :goto_0

    .line 1185
    :cond_2
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_3

    .line 1186
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    goto :goto_0

    .line 1188
    :cond_3
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/payments/a/a/a;

    .line 1189
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto :goto_1
.end method

.method static a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;Lco/uk/getmondo/payments/a/a/a;Ljava/util/Map;)Lco/uk/getmondo/payments/a/a/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/a;",
            "Lco/uk/getmondo/payments/a/a/a;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/payments/a/a/a;"
        }
    .end annotation

    .prologue
    .line 1208
    move-object v0, p1

    check-cast v0, Lio/realm/n;

    .line 1209
    check-cast p2, Lio/realm/n;

    .line 1210
    invoke-interface {p2}, Lio/realm/n;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->b(Ljava/lang/String;)V

    .line 1211
    invoke-interface {p2}, Lio/realm/n;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->c(Ljava/lang/String;)V

    .line 1212
    invoke-interface {p2}, Lio/realm/n;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->d(Ljava/lang/String;)V

    .line 1213
    invoke-interface {p2}, Lio/realm/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->e(Ljava/lang/String;)V

    .line 1214
    invoke-interface {p2}, Lio/realm/n;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->f(Ljava/lang/String;)V

    .line 1215
    invoke-interface {p2}, Lio/realm/n;->t_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->g(Ljava/lang/String;)V

    .line 1216
    invoke-interface {p2}, Lio/realm/n;->l()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->h(Ljava/lang/String;)V

    .line 1217
    invoke-interface {p2}, Lio/realm/n;->m()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/n;->a(Z)V

    .line 1218
    invoke-interface {p2}, Lio/realm/n;->n()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->i(Ljava/lang/String;)V

    .line 1219
    invoke-interface {p2}, Lio/realm/n;->o()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/n;->j(Ljava/lang/String;)V

    .line 1220
    return-object p1
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/a;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/a;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/payments/a/a/a;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 814
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-wide v2, v2, Lio/realm/g;->c:J

    iget-wide v6, p0, Lio/realm/av;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 815
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 817
    :cond_0
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 854
    :goto_0
    return-object p1

    .line 820
    :cond_1
    sget-object v2, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v2}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/g$b;

    .line 821
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/l;

    .line 822
    if-eqz v3, :cond_2

    .line 823
    check-cast v3, Lco/uk/getmondo/payments/a/a/a;

    move-object p1, v3

    goto :goto_0

    .line 826
    :cond_2
    const/4 v5, 0x0

    .line 828
    if-eqz p2, :cond_6

    .line 829
    const-class v3, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {p0, v3}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 830
    invoke-virtual {v8}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v3, p1

    .line 831
    check-cast v3, Lio/realm/n;

    invoke-interface {v3}, Lio/realm/n;->e()Ljava/lang/String;

    move-result-object v3

    .line 833
    if-nez v3, :cond_3

    .line 834
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v6

    .line 838
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-eqz v3, :cond_4

    .line 840
    :try_start_0
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    iget-object v3, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v3, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 841
    new-instance v4, Lio/realm/m;

    invoke-direct {v4}, Lio/realm/m;-><init>()V

    .line 842
    move-object v0, v4

    check-cast v0, Lio/realm/internal/l;

    move-object v3, v0

    invoke-interface {p3, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 844
    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    move v2, p2

    .line 851
    :goto_2
    if-eqz v2, :cond_5

    .line 852
    invoke-static {p0, v4, p1, p3}, Lio/realm/m;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;Lco/uk/getmondo/payments/a/a/a;Ljava/util/Map;)Lco/uk/getmondo/payments/a/a/a;

    move-result-object p1

    goto :goto_0

    .line 836
    :cond_3
    invoke-virtual {v8, v6, v7, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 844
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    throw v3

    :cond_4
    move v2, v4

    move-object v4, v5

    .line 847
    goto :goto_2

    .line 854
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/m;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/a;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v2, p2

    move-object v4, v5

    goto :goto_2
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/m$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0xb

    .line 461
    const-string v0, "class_DirectDebit"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 462
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'DirectDebit\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 464
    :cond_0
    const-string v0, "class_DirectDebit"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 465
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 466
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 467
    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 468
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 11 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 470
    :cond_1
    if-eqz p1, :cond_3

    .line 471
    const-string v0, "Field count is more than expected - expected 11 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 476
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 477
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 478
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 473
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 11 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 481
    :cond_4
    new-instance v0, Lio/realm/m$a;

    invoke-direct {v0, p0, v2}, Lio/realm/m$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 483
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 484
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 486
    :cond_5
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lio/realm/m$a;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 487
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 491
    :cond_6
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 492
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 494
    :cond_7
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_8

    .line 495
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'id\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 497
    :cond_8
    iget-wide v4, v0, Lio/realm/m$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_9

    .line 498
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@PrimaryKey field \'id\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_9
    const-string v1, "id"

    invoke-virtual {v2, v1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->j(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 501
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 503
    :cond_a
    const-string v1, "accountId"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 504
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'accountId\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 506
    :cond_b
    const-string v1, "accountId"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_c

    .line 507
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'accountId\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 509
    :cond_c
    iget-wide v4, v0, Lio/realm/m$a;->b:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_d

    .line 510
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'accountId\' is required. Either set @Required to field \'accountId\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_d
    const-string v1, "payerSortCode"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 513
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'payerSortCode\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 515
    :cond_e
    const-string v1, "payerSortCode"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_f

    .line 516
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'payerSortCode\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 518
    :cond_f
    iget-wide v4, v0, Lio/realm/m$a;->c:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_10

    .line 519
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'payerSortCode\' is required. Either set @Required to field \'payerSortCode\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 521
    :cond_10
    const-string v1, "payerAccountNumber"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 522
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'payerAccountNumber\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 524
    :cond_11
    const-string v1, "payerAccountNumber"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_12

    .line 525
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'payerAccountNumber\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 527
    :cond_12
    iget-wide v4, v0, Lio/realm/m$a;->d:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_13

    .line 528
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'payerAccountNumber\' is required. Either set @Required to field \'payerAccountNumber\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 530
    :cond_13
    const-string v1, "payerName"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 531
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'payerName\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 533
    :cond_14
    const-string v1, "payerName"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_15

    .line 534
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'payerName\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 536
    :cond_15
    iget-wide v4, v0, Lio/realm/m$a;->e:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_16

    .line 537
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'payerName\' is required. Either set @Required to field \'payerName\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 539
    :cond_16
    const-string v1, "serviceUserNumber"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 540
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'serviceUserNumber\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 542
    :cond_17
    const-string v1, "serviceUserNumber"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_18

    .line 543
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'serviceUserNumber\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 545
    :cond_18
    iget-wide v4, v0, Lio/realm/m$a;->f:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_19

    .line 546
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'serviceUserNumber\' is required. Either set @Required to field \'serviceUserNumber\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 548
    :cond_19
    const-string v1, "serviceUserName"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    .line 549
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'serviceUserName\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 551
    :cond_1a
    const-string v1, "serviceUserName"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_1b

    .line 552
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'serviceUserName\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 554
    :cond_1b
    iget-wide v4, v0, Lio/realm/m$a;->g:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 555
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'serviceUserName\' is required. Either set @Required to field \'serviceUserName\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 557
    :cond_1c
    const-string v1, "reference"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 558
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'reference\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 560
    :cond_1d
    const-string v1, "reference"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_1e

    .line 561
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'reference\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 563
    :cond_1e
    iget-wide v4, v0, Lio/realm/m$a;->h:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 564
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'reference\' is required. Either set @Required to field \'reference\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_1f
    const-string v1, "active"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_20

    .line 567
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'active\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_20
    const-string v1, "active"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_21

    .line 570
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'active\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_21
    iget-wide v4, v0, Lio/realm/m$a;->i:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 573
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'active\' does support null values in the existing Realm file. Use corresponding boxed type for field \'active\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 575
    :cond_22
    const-string v1, "_created"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    .line 576
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'_created\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 578
    :cond_23
    const-string v1, "_created"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_24

    .line 579
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'_created\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 581
    :cond_24
    iget-wide v4, v0, Lio/realm/m$a;->j:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_25

    .line 582
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'_created\' is required. Either set @Required to field \'_created\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 584
    :cond_25
    const-string v1, "_lastCollected"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_26

    .line 585
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'_lastCollected\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 587
    :cond_26
    const-string v1, "_lastCollected"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_27

    .line 588
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'_lastCollected\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 590
    :cond_27
    iget-wide v4, v0, Lio/realm/m$a;->k:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_28

    .line 591
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'_lastCollected\' is required. Either set @Required to field \'_lastCollected\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 594
    :cond_28
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1091
    const-class v2, Lco/uk/getmondo/payments/a/a/a;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v12

    .line 1092
    invoke-virtual {v12}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    .line 1093
    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v4, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lio/realm/m$a;

    .line 1094
    invoke-virtual {v12}, Lio/realm/internal/Table;->d()J

    move-result-wide v14

    .line 1096
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1097
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lco/uk/getmondo/payments/a/a/a;

    .line 1098
    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1101
    instance-of v4, v11, Lio/realm/internal/l;

    if-eqz v4, :cond_1

    move-object v4, v11

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v4, v11

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v11

    .line 1102
    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v4, v11

    .line 1105
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->e()Ljava/lang/String;

    move-result-object v4

    .line 1107
    if-nez v4, :cond_3

    .line 1108
    invoke-static {v2, v3, v14, v15}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v6

    .line 1112
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 1113
    invoke-static {v12, v4}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v6

    .line 1115
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v11, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v11

    .line 1116
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->f()Ljava/lang/String;

    move-result-object v8

    .line 1117
    if-eqz v8, :cond_4

    .line 1118
    iget-wide v4, v10, Lio/realm/m$a;->b:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_2
    move-object v4, v11

    .line 1122
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->g()Ljava/lang/String;

    move-result-object v8

    .line 1123
    if-eqz v8, :cond_5

    .line 1124
    iget-wide v4, v10, Lio/realm/m$a;->c:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_3
    move-object v4, v11

    .line 1128
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->h()Ljava/lang/String;

    move-result-object v8

    .line 1129
    if-eqz v8, :cond_6

    .line 1130
    iget-wide v4, v10, Lio/realm/m$a;->d:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_4
    move-object v4, v11

    .line 1134
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->i()Ljava/lang/String;

    move-result-object v8

    .line 1135
    if-eqz v8, :cond_7

    .line 1136
    iget-wide v4, v10, Lio/realm/m$a;->e:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_5
    move-object v4, v11

    .line 1140
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->j()Ljava/lang/String;

    move-result-object v8

    .line 1141
    if-eqz v8, :cond_8

    .line 1142
    iget-wide v4, v10, Lio/realm/m$a;->f:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_6
    move-object v4, v11

    .line 1146
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->t_()Ljava/lang/String;

    move-result-object v8

    .line 1147
    if-eqz v8, :cond_9

    .line 1148
    iget-wide v4, v10, Lio/realm/m$a;->g:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_7
    move-object v4, v11

    .line 1152
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->l()Ljava/lang/String;

    move-result-object v8

    .line 1153
    if-eqz v8, :cond_a

    .line 1154
    iget-wide v4, v10, Lio/realm/m$a;->h:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1158
    :goto_8
    iget-wide v4, v10, Lio/realm/m$a;->i:J

    move-object v8, v11

    check-cast v8, Lio/realm/n;

    invoke-interface {v8}, Lio/realm/n;->m()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v4, v11

    .line 1159
    check-cast v4, Lio/realm/n;

    invoke-interface {v4}, Lio/realm/n;->n()Ljava/lang/String;

    move-result-object v8

    .line 1160
    if-eqz v8, :cond_b

    .line 1161
    iget-wide v4, v10, Lio/realm/m$a;->j:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1165
    :goto_9
    check-cast v11, Lio/realm/n;

    invoke-interface {v11}, Lio/realm/n;->o()Ljava/lang/String;

    move-result-object v8

    .line 1166
    if-eqz v8, :cond_c

    .line 1167
    iget-wide v4, v10, Lio/realm/m$a;->k:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 1110
    :cond_3
    invoke-static {v2, v3, v14, v15, v4}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v6

    goto/16 :goto_1

    .line 1120
    :cond_4
    iget-wide v4, v10, Lio/realm/m$a;->b:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_2

    .line 1126
    :cond_5
    iget-wide v4, v10, Lio/realm/m$a;->c:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_3

    .line 1132
    :cond_6
    iget-wide v4, v10, Lio/realm/m$a;->d:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_4

    .line 1138
    :cond_7
    iget-wide v4, v10, Lio/realm/m$a;->e:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_5

    .line 1144
    :cond_8
    iget-wide v4, v10, Lio/realm/m$a;->f:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_6

    .line 1150
    :cond_9
    iget-wide v4, v10, Lio/realm/m$a;->g:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_7

    .line 1156
    :cond_a
    iget-wide v4, v10, Lio/realm/m$a;->h:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_8

    .line 1163
    :cond_b
    iget-wide v4, v10, Lio/realm/m$a;->j:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_9

    .line 1169
    :cond_c
    iget-wide v4, v10, Lio/realm/m$a;->k:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_0

    .line 1172
    :cond_d
    return-void
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;Ljava/util/Map;)J
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/a;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1014
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1015
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 1087
    :goto_0
    return-wide v4

    .line 1017
    :cond_0
    const-class v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 1018
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 1019
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lio/realm/m$a;

    .line 1020
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 1021
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->e()Ljava/lang/String;

    move-result-object v2

    .line 1023
    if-nez v2, :cond_2

    .line 1024
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 1028
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v6, v4, v10

    if-nez v6, :cond_1

    .line 1029
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 1031
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 1032
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->f()Ljava/lang/String;

    move-result-object v6

    .line 1033
    if-eqz v6, :cond_3

    .line 1034
    iget-wide v2, v8, Lio/realm/m$a;->b:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_2
    move-object v2, p1

    .line 1038
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->g()Ljava/lang/String;

    move-result-object v6

    .line 1039
    if-eqz v6, :cond_4

    .line 1040
    iget-wide v2, v8, Lio/realm/m$a;->c:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_3
    move-object v2, p1

    .line 1044
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->h()Ljava/lang/String;

    move-result-object v6

    .line 1045
    if-eqz v6, :cond_5

    .line 1046
    iget-wide v2, v8, Lio/realm/m$a;->d:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_4
    move-object v2, p1

    .line 1050
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->i()Ljava/lang/String;

    move-result-object v6

    .line 1051
    if-eqz v6, :cond_6

    .line 1052
    iget-wide v2, v8, Lio/realm/m$a;->e:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_5
    move-object v2, p1

    .line 1056
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->j()Ljava/lang/String;

    move-result-object v6

    .line 1057
    if-eqz v6, :cond_7

    .line 1058
    iget-wide v2, v8, Lio/realm/m$a;->f:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_6
    move-object v2, p1

    .line 1062
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->t_()Ljava/lang/String;

    move-result-object v6

    .line 1063
    if-eqz v6, :cond_8

    .line 1064
    iget-wide v2, v8, Lio/realm/m$a;->g:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_7
    move-object v2, p1

    .line 1068
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->l()Ljava/lang/String;

    move-result-object v6

    .line 1069
    if-eqz v6, :cond_9

    .line 1070
    iget-wide v2, v8, Lio/realm/m$a;->h:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1074
    :goto_8
    iget-wide v2, v8, Lio/realm/m$a;->i:J

    move-object v6, p1

    check-cast v6, Lio/realm/n;

    invoke-interface {v6}, Lio/realm/n;->m()Z

    move-result v6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v2, p1

    .line 1075
    check-cast v2, Lio/realm/n;

    invoke-interface {v2}, Lio/realm/n;->n()Ljava/lang/String;

    move-result-object v6

    .line 1076
    if-eqz v6, :cond_a

    .line 1077
    iget-wide v2, v8, Lio/realm/m$a;->j:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1081
    :goto_9
    check-cast p1, Lio/realm/n;

    invoke-interface {p1}, Lio/realm/n;->o()Ljava/lang/String;

    move-result-object v6

    .line 1082
    if-eqz v6, :cond_b

    .line 1083
    iget-wide v2, v8, Lio/realm/m$a;->k:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 1026
    :cond_2
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 1036
    :cond_3
    iget-wide v2, v8, Lio/realm/m$a;->b:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_2

    .line 1042
    :cond_4
    iget-wide v2, v8, Lio/realm/m$a;->c:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_3

    .line 1048
    :cond_5
    iget-wide v2, v8, Lio/realm/m$a;->d:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_4

    .line 1054
    :cond_6
    iget-wide v2, v8, Lio/realm/m$a;->e:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_5

    .line 1060
    :cond_7
    iget-wide v2, v8, Lio/realm/m$a;->f:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_6

    .line 1066
    :cond_8
    iget-wide v2, v8, Lio/realm/m$a;->g:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_7

    .line 1072
    :cond_9
    iget-wide v2, v8, Lio/realm/m$a;->h:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_8

    .line 1079
    :cond_a
    iget-wide v2, v8, Lio/realm/m$a;->j:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_9

    .line 1085
    :cond_b
    iget-wide v2, v8, Lio/realm/m$a;->k:J

    move v6, v7

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_0
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/a;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/payments/a/a/a;"
        }
    .end annotation

    .prologue
    .line 859
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 860
    if-eqz v0, :cond_0

    .line 861
    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    .line 881
    :goto_0
    return-object v0

    .line 865
    :cond_0
    const-class v1, Lco/uk/getmondo/payments/a/a/a;

    move-object v0, p1

    check-cast v0, Lio/realm/n;

    invoke-interface {v0}, Lio/realm/n;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lio/realm/av;->a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    move-object v1, v0

    .line 866
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    check-cast p1, Lio/realm/n;

    move-object v1, v0

    .line 869
    check-cast v1, Lio/realm/n;

    .line 871
    invoke-interface {p1}, Lio/realm/n;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->b(Ljava/lang/String;)V

    .line 872
    invoke-interface {p1}, Lio/realm/n;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->c(Ljava/lang/String;)V

    .line 873
    invoke-interface {p1}, Lio/realm/n;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->d(Ljava/lang/String;)V

    .line 874
    invoke-interface {p1}, Lio/realm/n;->i()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->e(Ljava/lang/String;)V

    .line 875
    invoke-interface {p1}, Lio/realm/n;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->f(Ljava/lang/String;)V

    .line 876
    invoke-interface {p1}, Lio/realm/n;->t_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->g(Ljava/lang/String;)V

    .line 877
    invoke-interface {p1}, Lio/realm/n;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->h(Ljava/lang/String;)V

    .line 878
    invoke-interface {p1}, Lio/realm/n;->m()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/n;->a(Z)V

    .line 879
    invoke-interface {p1}, Lio/realm/n;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->i(Ljava/lang/String;)V

    .line 880
    invoke-interface {p1}, Lio/realm/n;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/n;->j(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static p()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 457
    sget-object v0, Lio/realm/m;->d:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 598
    const-string v0, "class_DirectDebit"

    return-object v0
.end method

.method private static r()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 441
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "DirectDebit"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 442
    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 443
    const-string v7, "accountId"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 444
    const-string v7, "payerSortCode"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 445
    const-string v7, "payerAccountNumber"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 446
    const-string v7, "payerName"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 447
    const-string v7, "serviceUserNumber"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 448
    const-string v7, "serviceUserName"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 449
    const-string v7, "reference"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 450
    const-string v7, "active"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 451
    const-string v1, "_created"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 452
    const-string v1, "_lastCollected"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 453
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 145
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Z)V
    .locals 8

    .prologue
    .line 367
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 372
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v2, Lio/realm/m$a;->i:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 376
    :cond_1
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 377
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->i:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 157
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 162
    if-nez p1, :cond_1

    .line 163
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->b:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 166
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->b:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 170
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 171
    if-nez p1, :cond_3

    .line 172
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 175
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->b:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 187
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 192
    if-nez p1, :cond_1

    .line 193
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 196
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 200
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 201
    if-nez p1, :cond_3

    .line 202
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 205
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->c:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 217
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 222
    if-nez p1, :cond_1

    .line 223
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->d:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 226
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->d:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 230
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 231
    if-nez p1, :cond_3

    .line 232
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 235
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->d:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 134
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 247
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 248
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 252
    if-nez p1, :cond_1

    .line 253
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->e:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 256
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->e:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 260
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 261
    if-nez p1, :cond_3

    .line 262
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 265
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->e:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 152
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 277
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 278
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 282
    if-nez p1, :cond_1

    .line 283
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->f:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 286
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->f:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 290
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 291
    if-nez p1, :cond_3

    .line 292
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 295
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->f:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 182
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 307
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 308
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 312
    if-nez p1, :cond_1

    .line 313
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->g:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 316
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->g:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 320
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 321
    if-nez p1, :cond_3

    .line 322
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->g:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 325
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->g:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 211
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 212
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 337
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 338
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 356
    :goto_0
    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 342
    if-nez p1, :cond_1

    .line 343
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->h:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 346
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->h:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 350
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 351
    if-nez p1, :cond_3

    .line 352
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 355
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->h:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 4

    .prologue
    .line 241
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 242
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 389
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 390
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 393
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 394
    if-nez p1, :cond_1

    .line 395
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->j:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 398
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->j:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 402
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 403
    if-nez p1, :cond_3

    .line 404
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->j:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 407
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->j:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 4

    .prologue
    .line 271
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 272
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 419
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 420
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    :goto_0
    return-void

    .line 423
    :cond_0
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 424
    if-nez p1, :cond_1

    .line 425
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v0, Lio/realm/m$a;->k:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 428
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v1, v1, Lio/realm/m$a;->k:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 432
    :cond_2
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 433
    if-nez p1, :cond_3

    .line 434
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->k:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 437
    :cond_3
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->k:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 331
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 332
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Z
    .locals 4

    .prologue
    .line 361
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 362
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->i:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 4

    .prologue
    .line 383
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 384
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->j:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 4

    .prologue
    .line 413
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 414
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->k:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1279
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    return-object v0
.end method

.method public t_()Ljava/lang/String;
    .locals 4

    .prologue
    .line 301
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 302
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    iget-wide v2, v1, Lio/realm/m$a;->g:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1226
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1227
    const-string v0, "Invalid object"

    .line 1274
    :goto_0
    return-object v0

    .line 1229
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "DirectDebit = proxy["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1230
    const-string v0, "{id:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1231
    invoke-virtual {p0}, Lio/realm/m;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lio/realm/m;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1232
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1233
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234
    const-string v0, "{accountId:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1235
    invoke-virtual {p0}, Lio/realm/m;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lio/realm/m;->f()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1236
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1237
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1238
    const-string v0, "{payerSortCode:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1239
    invoke-virtual {p0}, Lio/realm/m;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lio/realm/m;->g()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1240
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1241
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1242
    const-string v0, "{payerAccountNumber:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1243
    invoke-virtual {p0}, Lio/realm/m;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lio/realm/m;->h()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1244
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1245
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1246
    const-string v0, "{payerName:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1247
    invoke-virtual {p0}, Lio/realm/m;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lio/realm/m;->i()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1248
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1249
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1250
    const-string v0, "{serviceUserNumber:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1251
    invoke-virtual {p0}, Lio/realm/m;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lio/realm/m;->j()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1252
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1253
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1254
    const-string v0, "{serviceUserName:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1255
    invoke-virtual {p0}, Lio/realm/m;->t_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lio/realm/m;->t_()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1256
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1257
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1258
    const-string v0, "{reference:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1259
    invoke-virtual {p0}, Lio/realm/m;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lio/realm/m;->l()Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1260
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1261
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1262
    const-string v0, "{active:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1263
    invoke-virtual {p0}, Lio/realm/m;->m()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1264
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1265
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1266
    const-string v0, "{_created:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1267
    invoke-virtual {p0}, Lio/realm/m;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lio/realm/m;->n()Ljava/lang/String;

    move-result-object v0

    :goto_9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1268
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1269
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1270
    const-string v0, "{_lastCollected:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1271
    invoke-virtual {p0}, Lio/realm/m;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lio/realm/m;->o()Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1272
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1273
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1274
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1231
    :cond_1
    const-string v0, "null"

    goto/16 :goto_1

    .line 1235
    :cond_2
    const-string v0, "null"

    goto/16 :goto_2

    .line 1239
    :cond_3
    const-string v0, "null"

    goto/16 :goto_3

    .line 1243
    :cond_4
    const-string v0, "null"

    goto/16 :goto_4

    .line 1247
    :cond_5
    const-string v0, "null"

    goto/16 :goto_5

    .line 1251
    :cond_6
    const-string v0, "null"

    goto/16 :goto_6

    .line 1255
    :cond_7
    const-string v0, "null"

    goto/16 :goto_7

    .line 1259
    :cond_8
    const-string v0, "null"

    goto :goto_8

    .line 1267
    :cond_9
    const-string v0, "null"

    goto :goto_9

    .line 1271
    :cond_a
    const-string v0, "null"

    goto :goto_a
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lio/realm/m;->c:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 121
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 122
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/m$a;

    iput-object v1, p0, Lio/realm/m;->b:Lio/realm/m$a;

    .line 123
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/m;->c:Lio/realm/au;

    .line 124
    iget-object v1, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 125
    iget-object v1, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 126
    iget-object v1, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 127
    iget-object v1, p0, Lio/realm/m;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method
