.class public Lio/realm/q;
.super Lco/uk/getmondo/d/l;
.source "FeatureFlagsRealmProxy.java"

# interfaces
.implements Lio/realm/internal/l;
.implements Lio/realm/r;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/q$a;
    }
.end annotation


# static fields
.field private static final c:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lio/realm/q$a;

.field private b:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/d/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    invoke-static {}, Lio/realm/q;->f()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/q;->c:Lio/realm/internal/OsObjectSchemaInfo;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 71
    const-string v1, "currentAccountP2pEnabled"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    const-string v1, "potsEnabled"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/q;->d:Ljava/util/List;

    .line 74
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lco/uk/getmondo/d/l;-><init>()V

    .line 77
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 78
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/l;Ljava/util/Map;)J
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/l;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 295
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 305
    :goto_0
    return-wide v4

    .line 298
    :cond_0
    const-class v0, Lco/uk/getmondo/d/l;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 299
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 300
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/l;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lio/realm/q$a;

    .line 301
    invoke-static {v3}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;)J

    move-result-wide v4

    .line 302
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    iget-wide v2, v8, Lio/realm/q$a;->a:J

    move-object v6, p1

    check-cast v6, Lio/realm/r;

    invoke-interface {v6}, Lio/realm/r;->b()Z

    move-result v6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 304
    iget-wide v2, v8, Lio/realm/q$a;->b:J

    check-cast p1, Lio/realm/r;

    invoke-interface {p1}, Lio/realm/r;->c()Z

    move-result v6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto :goto_0
.end method

.method public static a(Lco/uk/getmondo/d/l;IILjava/util/Map;)Lco/uk/getmondo/d/l;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/l;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/d/l;"
        }
    .end annotation

    .prologue
    .line 365
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 366
    :cond_0
    const/4 v0, 0x0

    .line 385
    :goto_0
    return-object v0

    .line 368
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 370
    if-nez v0, :cond_2

    .line 371
    new-instance v1, Lco/uk/getmondo/d/l;

    invoke-direct {v1}, Lco/uk/getmondo/d/l;-><init>()V

    .line 372
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 381
    check-cast v0, Lio/realm/r;

    .line 382
    check-cast p0, Lio/realm/r;

    .line 383
    invoke-interface {p0}, Lio/realm/r;->b()Z

    move-result v2

    invoke-interface {v0, v2}, Lio/realm/r;->a(Z)V

    .line 384
    invoke-interface {p0}, Lio/realm/r;->c()Z

    move-result v2

    invoke-interface {v0, v2}, Lio/realm/r;->b(Z)V

    move-object v0, v1

    .line 385
    goto :goto_0

    .line 375
    :cond_2
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_3

    .line 376
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/d/l;

    goto :goto_0

    .line 378
    :cond_3
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/d/l;

    .line 379
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto :goto_1
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/l;ZLjava/util/Map;)Lco/uk/getmondo/d/l;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/l;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/l;"
        }
    .end annotation

    .prologue
    .line 261
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-wide v0, v0, Lio/realm/g;->c:J

    iget-wide v2, p0, Lio/realm/av;->c:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 262
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_0
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    :goto_0
    return-object p1

    .line 267
    :cond_1
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 268
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 269
    if-eqz v0, :cond_2

    .line 270
    check-cast v0, Lco/uk/getmondo/d/l;

    move-object p1, v0

    goto :goto_0

    .line 273
    :cond_2
    invoke-static {p0, p1, p2, p3}, Lio/realm/q;->b(Lio/realm/av;Lco/uk/getmondo/d/l;ZLjava/util/Map;)Lco/uk/getmondo/d/l;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/q$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0x2

    .line 150
    const-string v0, "class_FeatureFlags"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'FeatureFlags\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_0
    const-string v0, "class_FeatureFlags"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 154
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 155
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 156
    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 157
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 2 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_1
    if-eqz p1, :cond_3

    .line 160
    const-string v0, "Field count is more than expected - expected 2 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 166
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 167
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 162
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 2 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_4
    new-instance v0, Lio/realm/q$a;

    invoke-direct {v0, p0, v2}, Lio/realm/q$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 172
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 173
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key defined for field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was removed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_5
    const-string v1, "currentAccountP2pEnabled"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 177
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'currentAccountP2pEnabled\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_6
    const-string v1, "currentAccountP2pEnabled"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_7

    .line 180
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'currentAccountP2pEnabled\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_7
    iget-wide v4, v0, Lio/realm/q$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 183
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'currentAccountP2pEnabled\' does support null values in the existing Realm file. Use corresponding boxed type for field \'currentAccountP2pEnabled\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_8
    const-string v1, "potsEnabled"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 186
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'potsEnabled\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_9
    const-string v1, "potsEnabled"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_a

    .line 189
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'potsEnabled\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_a
    iget-wide v4, v0, Lio/realm/q$a;->b:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 192
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'potsEnabled\' does support null values in the existing Realm file. Use corresponding boxed type for field \'potsEnabled\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_b
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 344
    const-class v0, Lco/uk/getmondo/d/l;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v10

    .line 345
    invoke-virtual {v10}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 346
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v3, Lco/uk/getmondo/d/l;

    invoke-virtual {v2, v3}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lio/realm/q$a;

    .line 348
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 349
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lco/uk/getmondo/d/l;

    .line 350
    invoke-interface {p2, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 353
    instance-of v2, v9, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, v9

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, v9

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v2, v9

    .line 354
    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/n;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, v9, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 357
    :cond_1
    invoke-static {v10}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;)J

    move-result-wide v4

    .line 358
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, v9, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    iget-wide v2, v8, Lio/realm/q$a;->a:J

    move-object v6, v9

    check-cast v6, Lio/realm/r;

    invoke-interface {v6}, Lio/realm/r;->b()Z

    move-result v6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 360
    iget-wide v2, v8, Lio/realm/q$a;->b:J

    check-cast v9, Lio/realm/r;

    invoke-interface {v9}, Lio/realm/r;->c()Z

    move-result v6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto :goto_0

    .line 362
    :cond_2
    return-void
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/l;Ljava/util/Map;)J
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/l;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 330
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 340
    :goto_0
    return-wide v4

    .line 333
    :cond_0
    const-class v0, Lco/uk/getmondo/d/l;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 334
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 335
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/l;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lio/realm/q$a;

    .line 336
    invoke-static {v3}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;)J

    move-result-wide v4

    .line 337
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    iget-wide v2, v8, Lio/realm/q$a;->a:J

    move-object v6, p1

    check-cast v6, Lio/realm/r;

    invoke-interface {v6}, Lio/realm/r;->b()Z

    move-result v6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 339
    iget-wide v2, v8, Lio/realm/q$a;->b:J

    check-cast p1, Lio/realm/r;

    invoke-interface {p1}, Lio/realm/r;->c()Z

    move-result v6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto :goto_0
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/l;ZLjava/util/Map;)Lco/uk/getmondo/d/l;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/l;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/l;"
        }
    .end annotation

    .prologue
    .line 277
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 278
    if-eqz v0, :cond_0

    .line 279
    check-cast v0, Lco/uk/getmondo/d/l;

    .line 291
    :goto_0
    return-object v0

    .line 283
    :cond_0
    const-class v0, Lco/uk/getmondo/d/l;

    const/4 v1, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lio/realm/av;->a(Ljava/lang/Class;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/l;

    move-object v1, v0

    .line 284
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    check-cast p1, Lio/realm/r;

    move-object v1, v0

    .line 287
    check-cast v1, Lio/realm/r;

    .line 289
    invoke-interface {p1}, Lio/realm/r;->b()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/r;->a(Z)V

    .line 290
    invoke-interface {p1}, Lio/realm/r;->c()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/r;->b(Z)V

    goto :goto_0
.end method

.method public static d()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lio/realm/q;->c:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    const-string v0, "class_FeatureFlags"

    return-object v0
.end method

.method private static f()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 139
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "FeatureFlags"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 140
    const-string v1, "currentAccountP2pEnabled"

    sget-object v2, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 141
    const-string v1, "potsEnabled"

    sget-object v2, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 142
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 8

    .prologue
    .line 103
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 108
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/q;->a:Lio/realm/q$a;

    iget-wide v2, v2, Lio/realm/q$a;->a:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 113
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/q;->a:Lio/realm/q$a;

    iget-wide v2, v1, Lio/realm/q$a;->a:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 8

    .prologue
    .line 125
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 130
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/q;->a:Lio/realm/q$a;

    iget-wide v2, v2, Lio/realm/q$a;->b:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 134
    :cond_1
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 135
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/q;->a:Lio/realm/q$a;

    iget-wide v2, v1, Lio/realm/q$a;->b:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    .line 97
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 98
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/q;->a:Lio/realm/q$a;

    iget-wide v2, v1, Lio/realm/q$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 4

    .prologue
    .line 119
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 120
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/q;->a:Lio/realm/q$a;

    iget-wide v2, v1, Lio/realm/q$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 408
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 391
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 392
    const-string v0, "Invalid object"

    .line 403
    :goto_0
    return-object v0

    .line 394
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FeatureFlags = proxy["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 395
    const-string v1, "{currentAccountP2pEnabled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    invoke-virtual {p0}, Lio/realm/q;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 397
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    const-string v1, "{potsEnabled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    invoke-virtual {p0}, Lio/realm/q;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 401
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lio/realm/q;->b:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 85
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 86
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/q$a;

    iput-object v1, p0, Lio/realm/q;->a:Lio/realm/q$a;

    .line 87
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/q;->b:Lio/realm/au;

    .line 88
    iget-object v1, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 89
    iget-object v1, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 90
    iget-object v1, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 91
    iget-object v1, p0, Lio/realm/q;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method
