.class final Lio/realm/bm$a;
.super Lio/realm/internal/c;
.source "TransactionRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/bm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:J

.field f:J

.field g:J

.field h:J

.field i:J

.field j:J

.field k:J

.field l:J

.field m:J

.field n:J

.field o:J

.field p:J

.field q:J

.field r:J

.field s:J

.field t:J

.field u:J

.field v:J

.field w:J

.field x:J

.field y:J

.field z:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 65
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 66
    const-string v0, "id"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->a:J

    .line 67
    const-string v0, "amountValue"

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->b:J

    .line 68
    const-string v0, "amountCurrency"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->c:J

    .line 69
    const-string v0, "amountLocalValue"

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->d:J

    .line 70
    const-string v0, "amountLocalCurrency"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->e:J

    .line 71
    const-string v0, "created"

    sget-object v1, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->f:J

    .line 72
    const-string v0, "createdDateFormatted"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->g:J

    .line 73
    const-string v0, "updated"

    sget-object v1, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->h:J

    .line 74
    const-string v0, "merchantDescription"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->i:J

    .line 75
    const-string v0, "description"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->j:J

    .line 76
    const-string v0, "declineReason"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->k:J

    .line 77
    const-string v0, "notes"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->l:J

    .line 78
    const-string v0, "hideAmount"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->m:J

    .line 79
    const-string v0, "settled"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->n:J

    .line 80
    const-string v0, "merchant"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->o:J

    .line 81
    const-string v0, "category"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->p:J

    .line 82
    const-string v0, "fromAtm"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->q:J

    .line 83
    const-string v0, "peerToPeer"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->r:J

    .line 84
    const-string v0, "topUp"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->s:J

    .line 85
    const-string v0, "includeInSpending"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->t:J

    .line 86
    const-string v0, "attachments"

    sget-object v1, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->u:J

    .line 87
    const-string v0, "peer"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->v:J

    .line 88
    const-string v0, "bankDetails"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->w:J

    .line 89
    const-string v0, "fromMonzoMe"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->x:J

    .line 90
    const-string v0, "scheme"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->y:J

    .line 91
    const-string v0, "bacsDirectDebitInstructionId"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/bm$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/bm$a;->z:J

    .line 92
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 96
    invoke-virtual {p0, p1, p0}, Lio/realm/bm$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 97
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lio/realm/bm$a;

    invoke-direct {v0, p0, p1}, Lio/realm/bm$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 106
    check-cast p1, Lio/realm/bm$a;

    .line 107
    check-cast p2, Lio/realm/bm$a;

    .line 108
    iget-wide v0, p1, Lio/realm/bm$a;->a:J

    iput-wide v0, p2, Lio/realm/bm$a;->a:J

    .line 109
    iget-wide v0, p1, Lio/realm/bm$a;->b:J

    iput-wide v0, p2, Lio/realm/bm$a;->b:J

    .line 110
    iget-wide v0, p1, Lio/realm/bm$a;->c:J

    iput-wide v0, p2, Lio/realm/bm$a;->c:J

    .line 111
    iget-wide v0, p1, Lio/realm/bm$a;->d:J

    iput-wide v0, p2, Lio/realm/bm$a;->d:J

    .line 112
    iget-wide v0, p1, Lio/realm/bm$a;->e:J

    iput-wide v0, p2, Lio/realm/bm$a;->e:J

    .line 113
    iget-wide v0, p1, Lio/realm/bm$a;->f:J

    iput-wide v0, p2, Lio/realm/bm$a;->f:J

    .line 114
    iget-wide v0, p1, Lio/realm/bm$a;->g:J

    iput-wide v0, p2, Lio/realm/bm$a;->g:J

    .line 115
    iget-wide v0, p1, Lio/realm/bm$a;->h:J

    iput-wide v0, p2, Lio/realm/bm$a;->h:J

    .line 116
    iget-wide v0, p1, Lio/realm/bm$a;->i:J

    iput-wide v0, p2, Lio/realm/bm$a;->i:J

    .line 117
    iget-wide v0, p1, Lio/realm/bm$a;->j:J

    iput-wide v0, p2, Lio/realm/bm$a;->j:J

    .line 118
    iget-wide v0, p1, Lio/realm/bm$a;->k:J

    iput-wide v0, p2, Lio/realm/bm$a;->k:J

    .line 119
    iget-wide v0, p1, Lio/realm/bm$a;->l:J

    iput-wide v0, p2, Lio/realm/bm$a;->l:J

    .line 120
    iget-wide v0, p1, Lio/realm/bm$a;->m:J

    iput-wide v0, p2, Lio/realm/bm$a;->m:J

    .line 121
    iget-wide v0, p1, Lio/realm/bm$a;->n:J

    iput-wide v0, p2, Lio/realm/bm$a;->n:J

    .line 122
    iget-wide v0, p1, Lio/realm/bm$a;->o:J

    iput-wide v0, p2, Lio/realm/bm$a;->o:J

    .line 123
    iget-wide v0, p1, Lio/realm/bm$a;->p:J

    iput-wide v0, p2, Lio/realm/bm$a;->p:J

    .line 124
    iget-wide v0, p1, Lio/realm/bm$a;->q:J

    iput-wide v0, p2, Lio/realm/bm$a;->q:J

    .line 125
    iget-wide v0, p1, Lio/realm/bm$a;->r:J

    iput-wide v0, p2, Lio/realm/bm$a;->r:J

    .line 126
    iget-wide v0, p1, Lio/realm/bm$a;->s:J

    iput-wide v0, p2, Lio/realm/bm$a;->s:J

    .line 127
    iget-wide v0, p1, Lio/realm/bm$a;->t:J

    iput-wide v0, p2, Lio/realm/bm$a;->t:J

    .line 128
    iget-wide v0, p1, Lio/realm/bm$a;->u:J

    iput-wide v0, p2, Lio/realm/bm$a;->u:J

    .line 129
    iget-wide v0, p1, Lio/realm/bm$a;->v:J

    iput-wide v0, p2, Lio/realm/bm$a;->v:J

    .line 130
    iget-wide v0, p1, Lio/realm/bm$a;->w:J

    iput-wide v0, p2, Lio/realm/bm$a;->w:J

    .line 131
    iget-wide v0, p1, Lio/realm/bm$a;->x:J

    iput-wide v0, p2, Lio/realm/bm$a;->x:J

    .line 132
    iget-wide v0, p1, Lio/realm/bm$a;->y:J

    iput-wide v0, p2, Lio/realm/bm$a;->y:J

    .line 133
    iget-wide v0, p1, Lio/realm/bm$a;->z:J

    iput-wide v0, p2, Lio/realm/bm$a;->z:J

    .line 134
    return-void
.end method
