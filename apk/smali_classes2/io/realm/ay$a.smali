.class public Lio/realm/ay$a;
.super Ljava/lang/Object;
.source "RealmConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/ay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/io/File;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:[B

.field private e:J

.field private f:Lio/realm/ba;

.field private g:Z

.field private h:Lio/realm/internal/SharedRealm$a;

.field private i:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Lio/realm/a/b;

.field private l:Lio/realm/av$a;

.field private m:Z

.field private n:Lio/realm/CompactOnLaunchCallback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 452
    sget-object v0, Lio/realm/g;->a:Landroid/content/Context;

    invoke-direct {p0, v0}, Lio/realm/ay$a;-><init>(Landroid/content/Context;)V

    .line 453
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 437
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/realm/ay$a;->i:Ljava/util/HashSet;

    .line 438
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/realm/ay$a;->j:Ljava/util/HashSet;

    .line 456
    if-nez p1, :cond_0

    .line 457
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Call `Realm.init(Context)` before creating a RealmConfiguration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 459
    :cond_0
    invoke-static {p1}, Lio/realm/internal/k;->a(Landroid/content/Context;)V

    .line 460
    invoke-direct {p0, p1}, Lio/realm/ay$a;->a(Landroid/content/Context;)V

    .line 461
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 465
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lio/realm/ay$a;->a:Ljava/io/File;

    .line 466
    const-string v0, "default.realm"

    iput-object v0, p0, Lio/realm/ay$a;->b:Ljava/lang/String;

    .line 467
    iput-object v2, p0, Lio/realm/ay$a;->d:[B

    .line 468
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lio/realm/ay$a;->e:J

    .line 469
    iput-object v2, p0, Lio/realm/ay$a;->f:Lio/realm/ba;

    .line 470
    iput-boolean v3, p0, Lio/realm/ay$a;->g:Z

    .line 471
    sget-object v0, Lio/realm/internal/SharedRealm$a;->a:Lio/realm/internal/SharedRealm$a;

    iput-object v0, p0, Lio/realm/ay$a;->h:Lio/realm/internal/SharedRealm$a;

    .line 472
    iput-boolean v3, p0, Lio/realm/ay$a;->m:Z

    .line 473
    iput-object v2, p0, Lio/realm/ay$a;->n:Lio/realm/CompactOnLaunchCallback;

    .line 474
    invoke-static {}, Lio/realm/ay;->r()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lio/realm/ay$a;->i:Ljava/util/HashSet;

    invoke-static {}, Lio/realm/ay;->r()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 477
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lio/realm/ay$a;
    .locals 2

    .prologue
    .line 575
    iget-object v0, p0, Lio/realm/ay$a;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/ay$a;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm cannot clear its schema when previously configured to use an asset file by calling assetFile()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 579
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/ay$a;->g:Z

    .line 580
    return-object p0
.end method

.method public b()Lio/realm/ay;
    .locals 18

    .prologue
    .line 753
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lio/realm/ay$a;->m:Z

    if-eqz v2, :cond_3

    .line 754
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/ay$a;->l:Lio/realm/av$a;

    if-eqz v2, :cond_0

    .line 755
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "This Realm is marked as read-only. Read-only Realms cannot use initialData(Realm.Transaction)."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 757
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/ay$a;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 758
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Only Realms provided using \'assetFile(path)\' can be marked read-only. No such Realm was provided."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 760
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lio/realm/ay$a;->g:Z

    if-eqz v2, :cond_2

    .line 761
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "\'deleteRealmIfMigrationNeeded()\' and read-only Realms cannot be combined"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 763
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/ay$a;->n:Lio/realm/CompactOnLaunchCallback;

    if-eqz v2, :cond_3

    .line 764
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "\'compactOnLaunch()\' and read-only Realms cannot be combined"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 768
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lio/realm/ay$a;->k:Lio/realm/a/b;

    if-nez v2, :cond_4

    invoke-static {}, Lio/realm/ay;->p()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 769
    new-instance v2, Lio/realm/a/a;

    invoke-direct {v2}, Lio/realm/a/a;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lio/realm/ay$a;->k:Lio/realm/a/b;

    .line 772
    :cond_4
    new-instance v2, Lio/realm/ay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/ay$a;->a:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/ay$a;->b:Ljava/lang/String;

    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/ay$a;->a:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/realm/ay$a;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 774
    invoke-static {v5}, Lio/realm/ay;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/ay$a;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lio/realm/ay$a;->d:[B

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/ay$a;->e:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lio/realm/ay$a;->f:Lio/realm/ba;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lio/realm/ay$a;->g:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lio/realm/ay$a;->h:Lio/realm/internal/SharedRealm$a;

    move-object/from16 v0, p0

    iget-object v13, v0, Lio/realm/ay$a;->i:Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v14, v0, Lio/realm/ay$a;->j:Ljava/util/HashSet;

    .line 781
    invoke-static {v13, v14}, Lio/realm/ay;->a(Ljava/util/Set;Ljava/util/Set;)Lio/realm/internal/m;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lio/realm/ay$a;->k:Lio/realm/a/b;

    move-object/from16 v0, p0

    iget-object v15, v0, Lio/realm/ay$a;->l:Lio/realm/av$a;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lio/realm/ay$a;->m:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lio/realm/ay$a;->n:Lio/realm/CompactOnLaunchCallback;

    move-object/from16 v17, v0

    invoke-direct/range {v2 .. v17}, Lio/realm/ay;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BJLio/realm/ba;ZLio/realm/internal/SharedRealm$a;Lio/realm/internal/m;Lio/realm/a/b;Lio/realm/av$a;ZLio/realm/CompactOnLaunchCallback;)V

    .line 772
    return-object v2
.end method
