.class final enum Lio/realm/aw$b;
.super Ljava/lang/Enum;
.source "RealmCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/aw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/aw$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/realm/aw$b;

.field public static final enum b:Lio/realm/aw$b;

.field private static final synthetic c:[Lio/realm/aw$b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    new-instance v0, Lio/realm/aw$b;

    const-string v1, "TYPED_REALM"

    invoke-direct {v0, v1, v2}, Lio/realm/aw$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/realm/aw$b;->a:Lio/realm/aw$b;

    .line 77
    new-instance v0, Lio/realm/aw$b;

    const-string v1, "DYNAMIC_REALM"

    invoke-direct {v0, v1, v3}, Lio/realm/aw$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/realm/aw$b;->b:Lio/realm/aw$b;

    .line 75
    const/4 v0, 0x2

    new-array v0, v0, [Lio/realm/aw$b;

    sget-object v1, Lio/realm/aw$b;->a:Lio/realm/aw$b;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/aw$b;->b:Lio/realm/aw$b;

    aput-object v1, v0, v3

    sput-object v0, Lio/realm/aw$b;->c:[Lio/realm/aw$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Ljava/lang/Class;)Lio/realm/aw$b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/g;",
            ">;)",
            "Lio/realm/aw$b;"
        }
    .end annotation

    .prologue
    .line 80
    const-class v0, Lio/realm/av;

    if-ne p0, v0, :cond_0

    .line 81
    sget-object v0, Lio/realm/aw$b;->a:Lio/realm/aw$b;

    .line 83
    :goto_0
    return-object v0

    .line 82
    :cond_0
    const-class v0, Lio/realm/o;

    if-ne p0, v0, :cond_1

    .line 83
    sget-object v0, Lio/realm/aw$b;->b:Lio/realm/aw$b;

    goto :goto_0

    .line 86
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The type of Realm class must be Realm or DynamicRealm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/aw$b;
    .locals 1

    .prologue
    .line 75
    const-class v0, Lio/realm/aw$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/aw$b;

    return-object v0
.end method

.method public static values()[Lio/realm/aw$b;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lio/realm/aw$b;->c:[Lio/realm/aw$b;

    invoke-virtual {v0}, [Lio/realm/aw$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/aw$b;

    return-object v0
.end method
