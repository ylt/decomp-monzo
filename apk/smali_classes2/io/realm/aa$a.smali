.class final Lio/realm/aa$a;
.super Lio/realm/internal/c;
.source "InboundP2PRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/aa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 45
    const-string v0, "id"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aa$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aa$a;->a:J

    .line 46
    const-string v0, "enabled"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aa$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aa$a;->b:J

    .line 47
    const-string v0, "max"

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aa$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aa$a;->c:J

    .line 48
    const-string v0, "min"

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aa$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aa$a;->d:J

    .line 49
    const-string v0, "ineligibilityReason"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aa$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aa$a;->e:J

    .line 50
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 54
    invoke-virtual {p0, p1, p0}, Lio/realm/aa$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 55
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lio/realm/aa$a;

    invoke-direct {v0, p0, p1}, Lio/realm/aa$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 64
    check-cast p1, Lio/realm/aa$a;

    .line 65
    check-cast p2, Lio/realm/aa$a;

    .line 66
    iget-wide v0, p1, Lio/realm/aa$a;->a:J

    iput-wide v0, p2, Lio/realm/aa$a;->a:J

    .line 67
    iget-wide v0, p1, Lio/realm/aa$a;->b:J

    iput-wide v0, p2, Lio/realm/aa$a;->b:J

    .line 68
    iget-wide v0, p1, Lio/realm/aa$a;->c:J

    iput-wide v0, p2, Lio/realm/aa$a;->c:J

    .line 69
    iget-wide v0, p1, Lio/realm/aa$a;->d:J

    iput-wide v0, p2, Lio/realm/aa$a;->d:J

    .line 70
    iget-wide v0, p1, Lio/realm/aa$a;->e:J

    iput-wide v0, p2, Lio/realm/aa$a;->e:J

    .line 71
    return-void
.end method
