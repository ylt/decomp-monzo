.class public Lio/realm/bh;
.super Ljava/lang/Object;
.source "RealmSchema.java"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/realm/internal/Table;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Lio/realm/internal/Table;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Lio/realm/be;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/realm/be;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lio/realm/g;

.field private f:Lio/realm/internal/b;


# direct methods
.method constructor <init>(Lio/realm/g;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/realm/bh;->a:Ljava/util/Map;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/realm/bh;->b:Ljava/util/Map;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/realm/bh;->c:Ljava/util/Map;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/realm/bh;->d:Ljava/util/Map;

    .line 59
    iput-object p1, p0, Lio/realm/bh;->e:Lio/realm/g;

    .line 60
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 337
    invoke-virtual {p0}, Lio/realm/bh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to use column index before set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_0
    return-void
.end method


# virtual methods
.method a(Ljava/lang/Class;)Lio/realm/internal/Table;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)",
            "Lio/realm/internal/Table;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lio/realm/bh;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Table;

    .line 210
    if-eqz v0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-object v0

    .line 212
    :cond_1
    invoke-static {p1}, Lio/realm/internal/Util;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 213
    invoke-virtual {p0, v1, p1}, Lio/realm/bh;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 215
    iget-object v0, p0, Lio/realm/bh;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Table;

    .line 217
    :cond_2
    if-nez v0, :cond_3

    .line 218
    iget-object v0, p0, Lio/realm/bh;->e:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->m()Lio/realm/internal/SharedRealm;

    move-result-object v0

    iget-object v2, p0, Lio/realm/bh;->e:Lio/realm/g;

    invoke-virtual {v2}, Lio/realm/g;->h()Lio/realm/ay;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v2

    invoke-virtual {v2, v1}, Lio/realm/internal/m;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 219
    iget-object v2, p0, Lio/realm/bh;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    :cond_3
    invoke-virtual {p0, v1, p1}, Lio/realm/bh;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    iget-object v1, p0, Lio/realm/bh;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method a(Ljava/lang/String;)Lio/realm/internal/Table;
    .locals 3

    .prologue
    .line 198
    invoke-static {p1}, Lio/realm/internal/Table;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199
    iget-object v0, p0, Lio/realm/bh;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Table;

    .line 200
    if-eqz v0, :cond_0

    .line 205
    :goto_0
    return-object v0

    .line 202
    :cond_0
    iget-object v0, p0, Lio/realm/bh;->e:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->m()Lio/realm/internal/SharedRealm;

    move-result-object v0

    invoke-virtual {v0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 203
    iget-object v2, p0, Lio/realm/bh;->a:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 67
    return-void
.end method

.method final a(JLjava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Lio/realm/internal/c/a",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/lang/String;",
            ">;",
            "Lio/realm/internal/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283
    iget-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    if-eqz v0, :cond_0

    .line 284
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "An instance of ColumnIndices is already set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_0
    new-instance v0, Lio/realm/internal/b;

    invoke-direct {v0, p1, p2, p3}, Lio/realm/internal/b;-><init>(JLjava/util/Map;)V

    iput-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    .line 287
    return-void
.end method

.method final a(Lio/realm/internal/b;)V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    if-eqz v0, :cond_0

    .line 271
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "An instance of ColumnIndices is already set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273
    :cond_0
    new-instance v0, Lio/realm/internal/b;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lio/realm/internal/b;-><init>(Lio/realm/internal/b;Z)V

    iput-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    .line 274
    return-void
.end method

.method final a(Ljava/lang/Class;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 302
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method b(Ljava/lang/Class;)Lio/realm/be;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)",
            "Lio/realm/be;"
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lio/realm/bh;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/be;

    .line 231
    if-eqz v0, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-object v0

    .line 233
    :cond_1
    invoke-static {p1}, Lio/realm/internal/Util;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 234
    invoke-virtual {p0, v1, p1}, Lio/realm/bh;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 236
    iget-object v0, p0, Lio/realm/bh;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/be;

    .line 238
    :cond_2
    if-nez v0, :cond_3

    .line 239
    invoke-virtual {p0, p1}, Lio/realm/bh;->a(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v2

    .line 240
    new-instance v0, Lio/realm/be;

    iget-object v3, p0, Lio/realm/bh;->e:Lio/realm/g;

    invoke-virtual {p0, v1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    invoke-direct {v0, v3, p0, v2, v4}, Lio/realm/be;-><init>(Lio/realm/g;Lio/realm/bh;Lio/realm/internal/Table;Lio/realm/internal/c;)V

    .line 241
    iget-object v2, p0, Lio/realm/bh;->c:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    :cond_3
    invoke-virtual {p0, v1, p1}, Lio/realm/bh;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lio/realm/bh;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected final b(Ljava/lang/String;)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 332
    invoke-direct {p0}, Lio/realm/bh;->f()V

    .line 333
    iget-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    invoke-virtual {v0, p1}, Lio/realm/internal/b;->a(Ljava/lang/String;)Lio/realm/internal/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lio/realm/be;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lio/realm/bh;->e:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->m()Lio/realm/internal/SharedRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->g()J

    move-result-wide v0

    long-to-int v1, v0

    .line 91
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 92
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 93
    iget-object v3, p0, Lio/realm/bh;->e:Lio/realm/g;

    invoke-virtual {v3}, Lio/realm/g;->m()Lio/realm/internal/SharedRealm;

    move-result-object v3

    invoke-virtual {v3, v0}, Lio/realm/internal/SharedRealm;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 94
    invoke-static {v3}, Lio/realm/internal/Table;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 92
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_0
    new-instance v4, Lio/realm/be;

    iget-object v5, p0, Lio/realm/bh;->e:Lio/realm/g;

    iget-object v6, p0, Lio/realm/bh;->e:Lio/realm/g;

    invoke-virtual {v6}, Lio/realm/g;->m()Lio/realm/internal/SharedRealm;

    move-result-object v6

    invoke-virtual {v6, v3}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    invoke-direct {v4, v5, p0, v3}, Lio/realm/be;-><init>(Lio/realm/g;Lio/realm/bh;Lio/realm/internal/Table;)V

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 99
    :cond_1
    return-object v2
.end method

.method b(Lio/realm/internal/b;)V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    invoke-virtual {v0, p1}, Lio/realm/internal/b;->a(Lio/realm/internal/b;)V

    .line 299
    return-void
.end method

.method final c()Lio/realm/internal/b;
    .locals 3

    .prologue
    .line 313
    invoke-direct {p0}, Lio/realm/bh;->f()V

    .line 314
    new-instance v0, Lio/realm/internal/b;

    iget-object v1, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lio/realm/internal/b;-><init>(Lio/realm/internal/b;Z)V

    return-object v0
.end method

.method final c(Ljava/lang/Class;)Lio/realm/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)",
            "Lio/realm/internal/c;"
        }
    .end annotation

    .prologue
    .line 327
    invoke-direct {p0}, Lio/realm/bh;->f()V

    .line 328
    iget-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    invoke-virtual {v0, p1}, Lio/realm/internal/b;->a(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v0

    return-object v0
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()J
    .locals 2

    .prologue
    .line 322
    invoke-direct {p0}, Lio/realm/bh;->f()V

    .line 323
    iget-object v0, p0, Lio/realm/bh;->f:Lio/realm/internal/b;

    invoke-virtual {v0}, Lio/realm/internal/b;->a()J

    move-result-wide v0

    return-wide v0
.end method
