.class public Lio/realm/av;
.super Lio/realm/g;
.source "Realm.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/av$a;
    }
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;

.field private static i:Lio/realm/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lio/realm/av;->h:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lio/realm/aw;)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lio/realm/g;-><init>(Lio/realm/aw;)V

    .line 150
    return-void
.end method

.method static a(Lio/realm/aw;)Lio/realm/av;
    .locals 3

    .prologue
    .line 369
    invoke-virtual {p0}, Lio/realm/aw;->a()Lio/realm/ay;

    move-result-object v0

    .line 371
    :try_start_0
    invoke-static {p0}, Lio/realm/av;->b(Lio/realm/aw;)Lio/realm/av;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 387
    :goto_0
    return-object v0

    .line 373
    :catch_0
    move-exception v1

    .line 374
    invoke-virtual {v0}, Lio/realm/ay;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 375
    invoke-static {v0}, Lio/realm/av;->c(Lio/realm/ay;)Z

    .line 387
    :cond_0
    :goto_1
    invoke-static {p0}, Lio/realm/av;->b(Lio/realm/aw;)Lio/realm/av;

    move-result-object v0

    goto :goto_0

    .line 378
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lio/realm/ay;->e()Lio/realm/ba;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 379
    invoke-static {v0, v1}, Lio/realm/av;->a(Lio/realm/ay;Lio/realm/exceptions/RealmMigrationNeededException;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 381
    :catch_1
    move-exception v0

    .line 383
    new-instance v1, Lio/realm/exceptions/RealmFileException;

    sget-object v2, Lio/realm/exceptions/RealmFileException$Kind;->NOT_FOUND:Lio/realm/exceptions/RealmFileException$Kind;

    invoke-direct {v1, v2, v0}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Lio/realm/bb;ILjava/util/Map;)Lio/realm/bb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;I",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)TE;"
        }
    .end annotation

    .prologue
    .line 1603
    invoke-virtual {p0}, Lio/realm/av;->e()V

    .line 1604
    iget-object v0, p0, Lio/realm/av;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lio/realm/internal/m;->a(Lio/realm/bb;ILjava/util/Map;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method private a(Lio/realm/bb;ZLjava/util/Map;)Lio/realm/bb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 1598
    invoke-virtual {p0}, Lio/realm/av;->e()V

    .line 1599
    iget-object v0, p0, Lio/realm/av;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lio/realm/internal/m;->a(Lio/realm/av;Lio/realm/bb;ZLjava/util/Map;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1620
    if-gez p1, :cond_0

    .line 1621
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maxDepth must be > 0. It was: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1623
    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 195
    const-class v1, Lio/realm/av;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lio/realm/g;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 196
    if-nez p0, :cond_0

    .line 197
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Non-null context required."

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 199
    :cond_0
    :try_start_1
    invoke-static {p0}, Lio/realm/av;->b(Landroid/content/Context;)V

    .line 200
    invoke-static {p0}, Lio/realm/internal/k;->a(Landroid/content/Context;)V

    .line 201
    new-instance v0, Lio/realm/ay$a;

    invoke-direct {v0, p0}, Lio/realm/ay$a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lio/realm/ay$a;->b()Lio/realm/ay;

    move-result-object v0

    invoke-static {v0}, Lio/realm/av;->b(Lio/realm/ay;)V

    .line 202
    invoke-static {}, Lio/realm/internal/h;->a()Lio/realm/internal/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lio/realm/internal/h;->a(Landroid/content/Context;)V

    .line 203
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lio/realm/g;->a:Landroid/content/Context;

    .line 204
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, ".realm.temp"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lio/realm/internal/SharedRealm;->a(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    :cond_1
    monitor-exit v1

    return-void
.end method

.method private static a(Lio/realm/av;)V
    .locals 15

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 436
    .line 442
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lio/realm/av;->a(Z)V

    .line 443
    invoke-virtual {p0}, Lio/realm/av;->h()Lio/realm/ay;

    move-result-object v8

    .line 444
    invoke-virtual {p0}, Lio/realm/av;->i()J

    move-result-wide v6

    .line 445
    const-wide/16 v4, -0x1

    cmp-long v0, v6, v4

    if-nez v0, :cond_1

    move v3, v2

    .line 446
    :goto_0
    invoke-virtual {v8}, Lio/realm/ay;->d()J

    move-result-wide v4

    .line 448
    invoke-virtual {v8}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v9

    .line 449
    invoke-virtual {v9}, Lio/realm/internal/m;->b()Ljava/util/Set;

    move-result-object v0

    .line 451
    invoke-virtual {v8}, Lio/realm/ay;->q()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 453
    invoke-virtual {v8}, Lio/realm/ay;->o()Z

    move-result v10

    if-nez v10, :cond_a

    .line 454
    new-instance v10, Lio/realm/internal/OsSchemaInfo;

    invoke-virtual {v9}, Lio/realm/internal/m;->a()Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v11

    invoke-direct {v10, v11}, Lio/realm/internal/OsSchemaInfo;-><init>(Ljava/util/Collection;)V

    .line 457
    iget-object v11, p0, Lio/realm/av;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v11, v10, v4, v5}, Lio/realm/internal/SharedRealm;->a(Lio/realm/internal/OsSchemaInfo;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    :goto_1
    :try_start_1
    new-instance v10, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v11

    invoke-direct {v10, v11}, Ljava/util/HashMap;-><init>(I)V

    .line 476
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 477
    invoke-virtual {v9, v0}, Lio/realm/internal/m;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lio/realm/internal/Table;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 478
    invoke-static {v0, v12}, Lio/realm/internal/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Lio/realm/internal/c/a;

    move-result-object v12

    .line 480
    iget-object v13, p0, Lio/realm/av;->e:Lio/realm/internal/SharedRealm;

    .line 481
    invoke-virtual {v8}, Lio/realm/ay;->q()Z

    move-result v14

    .line 480
    invoke-virtual {v9, v0, v13, v14}, Lio/realm/internal/m;->a(Ljava/lang/Class;Lio/realm/internal/SharedRealm;Z)Lio/realm/internal/c;

    move-result-object v0

    invoke-interface {v10, v12, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 493
    :catch_0
    move-exception v0

    .line 495
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 497
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_9

    .line 498
    invoke-virtual {p0}, Lio/realm/av;->c()V

    .line 500
    :cond_0
    :goto_4
    throw v0

    :cond_1
    move v3, v1

    .line 445
    goto :goto_0

    .line 462
    :cond_2
    if-eqz v3, :cond_a

    .line 463
    :try_start_3
    invoke-virtual {v8}, Lio/realm/ay;->o()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 464
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot create the Realm schema in a read-only file."

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 468
    :cond_3
    new-instance v10, Lio/realm/internal/OsSchemaInfo;

    invoke-virtual {v9}, Lio/realm/internal/m;->a()Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v11

    invoke-direct {v10, v11}, Lio/realm/internal/OsSchemaInfo;-><init>(Ljava/util/Collection;)V

    .line 469
    iget-object v11, p0, Lio/realm/av;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v11, v10, v4, v5}, Lio/realm/internal/SharedRealm;->a(Lio/realm/internal/OsSchemaInfo;J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 484
    :cond_4
    :try_start_4
    invoke-virtual {p0}, Lio/realm/av;->k()Lio/realm/bh;

    move-result-object v0

    .line 485
    if-eqz v3, :cond_7

    .line 484
    :goto_5
    invoke-virtual {v0, v4, v5, v10}, Lio/realm/bh;->a(JLjava/util/Map;)V

    .line 489
    invoke-virtual {v8}, Lio/realm/ay;->i()Lio/realm/av$a;

    move-result-object v0

    .line 490
    if-eqz v0, :cond_5

    if-eqz v3, :cond_5

    .line 491
    invoke-interface {v0, p0}, Lio/realm/av$a;->a(Lio/realm/av;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 497
    :cond_5
    if-eqz v2, :cond_8

    .line 498
    invoke-virtual {p0}, Lio/realm/av;->c()V

    .line 503
    :cond_6
    :goto_6
    return-void

    :cond_7
    move-wide v4, v6

    .line 485
    goto :goto_5

    .line 499
    :cond_8
    invoke-virtual {p0}, Lio/realm/av;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 500
    invoke-virtual {p0}, Lio/realm/av;->d()V

    goto :goto_6

    .line 499
    :cond_9
    invoke-virtual {p0}, Lio/realm/av;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    invoke-virtual {p0}, Lio/realm/av;->d()V

    goto :goto_4

    .line 497
    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_3

    :cond_a
    move v2, v1

    goto/16 :goto_1
.end method

.method private static a(Lio/realm/ay;Lio/realm/exceptions/RealmMigrationNeededException;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1657
    const/4 v0, 0x0

    new-instance v1, Lio/realm/av$1;

    invoke-direct {v1}, Lio/realm/av$1;-><init>()V

    invoke-static {p0, v0, v1, p1}, Lio/realm/g;->a(Lio/realm/ay;Lio/realm/ba;Lio/realm/g$a;Lio/realm/exceptions/RealmMigrationNeededException;)V

    .line 1662
    return-void
.end method

.method private static b(Lio/realm/aw;)Lio/realm/av;
    .locals 11

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 392
    new-instance v0, Lio/realm/av;

    invoke-direct {v0, p0}, Lio/realm/av;-><init>(Lio/realm/aw;)V

    .line 393
    iget-object v1, v0, Lio/realm/av;->d:Lio/realm/ay;

    .line 395
    invoke-virtual {v0}, Lio/realm/av;->i()J

    move-result-wide v2

    .line 396
    invoke-virtual {v1}, Lio/realm/ay;->d()J

    move-result-wide v4

    .line 398
    invoke-virtual {p0}, Lio/realm/aw;->b()[Lio/realm/internal/b;

    move-result-object v6

    invoke-static {v6, v4, v5}, Lio/realm/aw;->a([Lio/realm/internal/b;J)Lio/realm/internal/b;

    move-result-object v6

    .line 401
    if-eqz v6, :cond_0

    .line 403
    iget-object v1, v0, Lio/realm/av;->f:Lio/realm/bh;

    invoke-virtual {v1, v6}, Lio/realm/bh;->a(Lio/realm/internal/b;)V

    .line 430
    :goto_0
    return-object v0

    .line 405
    :cond_0
    invoke-virtual {v1}, Lio/realm/ay;->q()Z

    move-result v6

    .line 407
    if-nez v6, :cond_2

    const-wide/16 v6, -0x1

    cmp-long v6, v2, v6

    if-eqz v6, :cond_2

    .line 408
    cmp-long v6, v2, v4

    if-gez v6, :cond_1

    .line 409
    invoke-virtual {v0}, Lio/realm/av;->j()V

    .line 410
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    .line 411
    invoke-virtual {v1}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v1

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Realm on disk need to migrate from v%s to v%s"

    new-array v8, v8, [Ljava/lang/Object;

    .line 412
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v8, v9

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v8, v10

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 414
    :cond_1
    cmp-long v1, v4, v2

    if-gez v1, :cond_2

    .line 415
    invoke-virtual {v0}, Lio/realm/av;->j()V

    .line 416
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Realm on disk is newer than the one specified: v%s vs. v%s"

    new-array v7, v8, [Ljava/lang/Object;

    .line 417
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v9

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v10

    invoke-static {v1, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :cond_2
    :try_start_0
    invoke-static {v0}, Lio/realm/av;->a(Lio/realm/av;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 424
    :catch_0
    move-exception v1

    .line 425
    invoke-virtual {v0}, Lio/realm/av;->j()V

    .line 426
    throw v1
.end method

.method private static b(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 221
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_2

    .line 223
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 258
    :cond_0
    return-void

    .line 230
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_6

    .line 240
    :cond_3
    const/4 v0, 0x5

    new-array v1, v0, [J

    fill-array-data v1, :array_0

    .line 241
    const-wide/16 v4, 0xc8

    .line 242
    const-wide/16 v2, 0x0

    .line 243
    const/4 v0, -0x1

    .line 244
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_6

    .line 245
    :cond_5
    add-int/lit8 v0, v0, 0x1

    array-length v6, v1

    add-int/lit8 v6, v6, -0x1

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    aget-wide v6, v1, v6

    .line 246
    invoke-static {v6, v7}, Landroid/os/SystemClock;->sleep(J)V

    .line 247
    add-long/2addr v2, v6

    .line 248
    cmp-long v6, v2, v4

    if-lez v6, :cond_4

    .line 255
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Context.getFilesDir() returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " which is not an existing directory. See https://issuetracker.google.com/issues/36918154"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :catch_0
    move-exception v1

    goto :goto_0

    .line 240
    nop

    :array_0
    .array-data 8
        0x1
        0x2
        0x5
        0xa
        0x10
    .end array-data
.end method

.method public static b(Lio/realm/ay;)V
    .locals 2

    .prologue
    .line 333
    if-nez p0, :cond_0

    .line 334
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non-null RealmConfiguration must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336
    :cond_0
    sget-object v1, Lio/realm/av;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 337
    :try_start_0
    sput-object p0, Lio/realm/av;->i:Lio/realm/ay;

    .line 338
    monitor-exit v1

    .line 339
    return-void

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static c(Lio/realm/ay;)Z
    .locals 1

    .prologue
    .line 1690
    invoke-static {p0}, Lio/realm/g;->a(Lio/realm/ay;)Z

    move-result v0

    return v0
.end method

.method private d(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1614
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->a(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1615
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "A RealmObject with no @PrimaryKey cannot be updated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1617
    :cond_0
    return-void
.end method

.method private f(Lio/realm/bb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 1608
    if-nez p1, :cond_0

    .line 1609
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null objects cannot be copied into Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1611
    :cond_0
    return-void
.end method

.method private g(Lio/realm/bb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 1626
    if-nez p1, :cond_0

    .line 1627
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null objects cannot be copied from Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1629
    :cond_0
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1630
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only valid managed objects can be copied from Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1632
    :cond_2
    instance-of v0, p1, Lio/realm/p;

    if-eqz v0, :cond_3

    .line 1633
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DynamicRealmObject cannot be copied from Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1635
    :cond_3
    return-void
.end method

.method public static n()Lio/realm/av;
    .locals 2

    .prologue
    .line 273
    invoke-static {}, Lio/realm/av;->o()Lio/realm/ay;

    move-result-object v0

    .line 274
    if-nez v0, :cond_1

    .line 275
    sget-object v0, Lio/realm/g;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 276
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Call `Realm.init(Context)` before calling this method."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Set default configuration by using `Realm.setDefaultConfiguration(RealmConfiguration)`."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_1
    const-class v1, Lio/realm/av;

    invoke-static {v0, v1}, Lio/realm/aw;->a(Lio/realm/ay;Ljava/lang/Class;)Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    return-object v0
.end method

.method public static o()Lio/realm/ay;
    .locals 2

    .prologue
    .line 347
    sget-object v1, Lio/realm/av;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 348
    :try_start_0
    sget-object v0, Lio/realm/av;->i:Lio/realm/ay;

    monitor-exit v1

    return-object v0

    .line 349
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static p()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1773
    const-string v1, "io.realm.DefaultRealmModule"

    .line 1777
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1778
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    .line 1779
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 1780
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 1782
    :goto_0
    return-object v0

    .line 1781
    :catch_0
    move-exception v0

    .line 1782
    const/4 v0, 0x0

    goto :goto_0

    .line 1783
    :catch_1
    move-exception v0

    .line 1784
    new-instance v2, Lio/realm/exceptions/RealmException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not create an instance of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 1785
    :catch_2
    move-exception v0

    .line 1786
    new-instance v2, Lio/realm/exceptions/RealmException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not create an instance of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 1787
    :catch_3
    move-exception v0

    .line 1788
    new-instance v2, Lio/realm/exceptions/RealmException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not create an instance of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method


# virtual methods
.method public a(Lio/realm/bb;)Lio/realm/bb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 1010
    invoke-direct {p0, p1}, Lio/realm/av;->f(Lio/realm/bb;)V

    .line 1011
    const/4 v0, 0x0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lio/realm/av;->a(Lio/realm/bb;ZLjava/util/Map;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public a(Lio/realm/bb;I)Lio/realm/bb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;I)TE;"
        }
    .end annotation

    .prologue
    .line 1328
    invoke-direct {p0, p2}, Lio/realm/av;->a(I)V

    .line 1329
    invoke-direct {p0, p1}, Lio/realm/av;->g(Lio/realm/bb;)V

    .line 1330
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lio/realm/av;->a(Lio/realm/bb;ILjava/util/Map;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/Object;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 988
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->a(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v1

    .line 990
    iget-object v0, p0, Lio/realm/av;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v0

    .line 991
    invoke-static {v1, p2}, Lio/realm/internal/OsObject;->a(Lio/realm/internal/Table;Ljava/lang/Object;)Lio/realm/internal/UncheckedRow;

    move-result-object v3

    iget-object v1, p0, Lio/realm/av;->f:Lio/realm/bh;

    .line 992
    invoke-virtual {v1, p1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v1, p1

    move-object v2, p0

    move v5, p3

    move-object v6, p4

    .line 990
    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/m;->a(Ljava/lang/Class;Ljava/lang/Object;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Class;ZLjava/util/List;)Lio/realm/bb;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 937
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->a(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v1

    .line 939
    invoke-virtual {v1}, Lio/realm/internal/Table;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 940
    new-instance v0, Lio/realm/exceptions/RealmException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "\'%s\' has a primary key, use \'createObject(Class<E>, Object)\' instead."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 941
    invoke-virtual {v1}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 940
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 943
    :cond_0
    iget-object v0, p0, Lio/realm/av;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v0

    .line 944
    invoke-static {v1}, Lio/realm/internal/OsObject;->a(Lio/realm/internal/Table;)Lio/realm/internal/UncheckedRow;

    move-result-object v3

    iget-object v1, p0, Lio/realm/av;->f:Lio/realm/bh;

    .line 945
    invoke-virtual {v1, p1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v1, p1

    move-object v2, p0

    move v5, p2

    move-object v6, p3

    .line 943
    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/m;->a(Ljava/lang/Class;Ljava/lang/Object;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;)Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1341
    invoke-virtual {p0}, Lio/realm/av;->e()V

    .line 1342
    invoke-static {p0, p1}, Lio/realm/bf;->a(Lio/realm/av;Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method a([Lio/realm/internal/b;)Lio/realm/internal/b;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1727
    iget-object v0, p0, Lio/realm/av;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->e()J

    move-result-wide v2

    .line 1728
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    invoke-virtual {v0}, Lio/realm/bh;->e()J

    move-result-wide v4

    .line 1729
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 1761
    :goto_0
    return-object v1

    .line 1734
    :cond_0
    invoke-static {p1, v2, v3}, Lio/realm/aw;->a([Lio/realm/internal/b;J)Lio/realm/internal/b;

    move-result-object v0

    .line 1736
    if-nez v0, :cond_2

    .line 1737
    invoke-virtual {p0}, Lio/realm/av;->h()Lio/realm/ay;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v1

    .line 1740
    invoke-virtual {v1}, Lio/realm/internal/m;->b()Ljava/util/Set;

    move-result-object v0

    .line 1742
    new-instance v4, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 1748
    :try_start_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 1749
    iget-object v6, p0, Lio/realm/av;->e:Lio/realm/internal/SharedRealm;

    const/4 v7, 0x1

    invoke-virtual {v1, v0, v6, v7}, Lio/realm/internal/m;->a(Ljava/lang/Class;Lio/realm/internal/SharedRealm;Z)Lio/realm/internal/c;

    move-result-object v6

    .line 1750
    invoke-virtual {v1, v0}, Lio/realm/internal/m;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lio/realm/internal/Table;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1751
    invoke-static {v0, v7}, Lio/realm/internal/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Lio/realm/internal/c/a;

    move-result-object v0

    .line 1752
    invoke-interface {v4, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1754
    :catch_0
    move-exception v0

    .line 1755
    throw v0

    .line 1758
    :cond_1
    new-instance v0, Lio/realm/internal/b;

    invoke-direct {v0, v2, v3, v4}, Lio/realm/internal/b;-><init>(JLjava/util/Map;)V

    move-object v1, v0

    .line 1760
    :cond_2
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    invoke-virtual {v2, v0}, Lio/realm/bh;->b(Lio/realm/internal/b;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Iterable",
            "<TE;>;)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1245
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, v0}, Lio/realm/av;->a(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Iterable",
            "<TE;>;I)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1270
    invoke-direct {p0, p2}, Lio/realm/av;->a(I)V

    .line 1271
    if-nez p1, :cond_0

    .line 1272
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1282
    :goto_0
    return-object v0

    .line 1275
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1276
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1277
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    .line 1278
    invoke-direct {p0, v0}, Lio/realm/av;->g(Lio/realm/bb;)V

    .line 1279
    invoke-direct {p0, v0, p2, v2}, Lio/realm/av;->a(Lio/realm/bb;ILjava/util/Map;)Lio/realm/bb;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 1282
    goto :goto_0
.end method

.method public a(Lio/realm/av$a;)V
    .locals 3

    .prologue
    .line 1397
    if-nez p1, :cond_0

    .line 1398
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transaction should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1401
    :cond_0
    invoke-virtual {p0}, Lio/realm/av;->b()V

    .line 1403
    :try_start_0
    invoke-interface {p1, p0}, Lio/realm/av$a;->a(Lio/realm/av;)V

    .line 1404
    invoke-virtual {p0}, Lio/realm/av;->c()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1413
    return-void

    .line 1405
    :catch_0
    move-exception v0

    .line 1406
    invoke-virtual {p0}, Lio/realm/av;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1407
    invoke-virtual {p0}, Lio/realm/av;->d()V

    .line 1411
    :goto_0
    throw v0

    .line 1409
    :cond_1
    const-string v1, "Could not cancel transaction, not currently in a transaction."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lio/realm/log/RealmLog;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lio/realm/bb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1156
    invoke-virtual {p0}, Lio/realm/av;->f()V

    .line 1157
    if-nez p1, :cond_0

    .line 1158
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null objects cannot be inserted into Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1160
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1164
    :goto_0
    return-void

    .line 1163
    :cond_1
    iget-object v0, p0, Lio/realm/av;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/m;->a(Lio/realm/av;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public bridge synthetic a()Z
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->a()Z

    move-result v0

    return v0
.end method

.method public b(Lio/realm/bb;)Lio/realm/bb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 1029
    invoke-direct {p0, p1}, Lio/realm/av;->f(Lio/realm/bb;)V

    .line 1030
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/av;->d(Ljava/lang/Class;)V

    .line 1031
    const/4 v0, 0x1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lio/realm/av;->a(Lio/realm/bb;ZLjava/util/Map;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b()V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->b()V

    return-void
.end method

.method public b(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1591
    invoke-virtual {p0}, Lio/realm/av;->e()V

    .line 1592
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->a(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->b()V

    .line 1593
    return-void
.end method

.method c(Ljava/lang/Class;)Lio/realm/internal/Table;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)",
            "Lio/realm/internal/Table;"
        }
    .end annotation

    .prologue
    .line 1715
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->a(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic c()V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->c()V

    return-void
.end method

.method public c(Lio/realm/bb;)V
    .locals 2

    .prologue
    .line 1121
    invoke-virtual {p0}, Lio/realm/av;->f()V

    .line 1122
    if-nez p1, :cond_0

    .line 1123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null object cannot be inserted into Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1125
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1126
    iget-object v1, p0, Lio/realm/av;->d:Lio/realm/ay;

    invoke-virtual {v1}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v1

    invoke-virtual {v1, p0, p1, v0}, Lio/realm/internal/m;->a(Lio/realm/av;Lio/realm/bb;Ljava/util/Map;)V

    .line 1127
    return-void
.end method

.method public bridge synthetic close()V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->close()V

    return-void
.end method

.method public bridge synthetic d()V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->d()V

    return-void
.end method

.method public d(Lio/realm/bb;)V
    .locals 2

    .prologue
    .line 1191
    invoke-virtual {p0}, Lio/realm/av;->f()V

    .line 1192
    if-nez p1, :cond_0

    .line 1193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null object cannot be inserted into Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1195
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1196
    iget-object v1, p0, Lio/realm/av;->d:Lio/realm/ay;

    invoke-virtual {v1}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v1

    invoke-virtual {v1, p0, p1, v0}, Lio/realm/internal/m;->b(Lio/realm/av;Lio/realm/bb;Ljava/util/Map;)V

    .line 1197
    return-void
.end method

.method public e(Lio/realm/bb;)Lio/realm/bb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 1303
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, v0}, Lio/realm/av;->a(Lio/realm/bb;I)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic h()Lio/realm/ay;
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->h()Lio/realm/ay;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic i()J
    .locals 2

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->i()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic k()Lio/realm/bh;
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->k()Lio/realm/bh;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic l()V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0}, Lio/realm/g;->l()V

    return-void
.end method
