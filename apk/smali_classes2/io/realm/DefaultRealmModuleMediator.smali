.class Lio/realm/DefaultRealmModuleMediator;
.super Lio/realm/internal/m;
.source "DefaultRealmModuleMediator.java"


# annotations
.annotation runtime Lio/realm/annotations/RealmModule;
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 30
    const-class v1, Lco/uk/getmondo/d/p;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 31
    const-class v1, Lco/uk/getmondo/d/ag;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 32
    const-class v1, Lco/uk/getmondo/d/l;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 33
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 34
    const-class v1, Lco/uk/getmondo/d/t;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 35
    const-class v1, Lco/uk/getmondo/d/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 36
    const-class v1, Lco/uk/getmondo/d/n;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    const-class v1, Lco/uk/getmondo/d/u;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 38
    const-class v1, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    const-class v1, Lco/uk/getmondo/d/m;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    const-class v1, Lco/uk/getmondo/d/am;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    const-class v1, Lco/uk/getmondo/d/b;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 42
    const-class v1, Lco/uk/getmondo/d/r;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    const-class v1, Lco/uk/getmondo/d/aj;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    const-class v1, Lco/uk/getmondo/payments/a/a/a;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    const-class v1, Lco/uk/getmondo/d/f;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    const-class v1, Lco/uk/getmondo/d/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    const-class v1, Lco/uk/getmondo/d/w;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 48
    const-class v1, Lco/uk/getmondo/d/o;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    const-class v1, Lco/uk/getmondo/d/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    const-class v1, Lco/uk/getmondo/payments/a/a/b;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    const-class v1, Lco/uk/getmondo/d/v;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lio/realm/DefaultRealmModuleMediator;->a:Ljava/util/Set;

    .line 53
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lio/realm/internal/m;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lio/realm/av;Lio/realm/bb;ZLjava/util/Map;)Lio/realm/bb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Lio/realm/av;",
            "TE;Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 390
    instance-of v0, p2, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 392
    :goto_0
    const-class v1, Lco/uk/getmondo/d/p;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 393
    check-cast p2, Lco/uk/getmondo/d/p;

    invoke-static {p1, p2, p3, p4}, Lio/realm/aa;->a(Lio/realm/av;Lco/uk/getmondo/d/p;ZLjava/util/Map;)Lco/uk/getmondo/d/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    .line 456
    :goto_1
    return-object v0

    .line 390
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 395
    :cond_1
    const-class v1, Lco/uk/getmondo/d/ag;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 396
    check-cast p2, Lco/uk/getmondo/d/ag;

    invoke-static {p1, p2, p3, p4}, Lio/realm/bj;->a(Lio/realm/av;Lco/uk/getmondo/d/ag;ZLjava/util/Map;)Lco/uk/getmondo/d/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_1

    .line 398
    :cond_2
    const-class v1, Lco/uk/getmondo/d/l;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 399
    check-cast p2, Lco/uk/getmondo/d/l;

    invoke-static {p1, p2, p3, p4}, Lio/realm/q;->a(Lio/realm/av;Lco/uk/getmondo/d/l;ZLjava/util/Map;)Lco/uk/getmondo/d/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_1

    .line 401
    :cond_3
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 402
    check-cast p2, Lco/uk/getmondo/payments/a/a/e;

    invoke-static {p1, p2, p3, p4}, Lio/realm/aq;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_1

    .line 404
    :cond_4
    const-class v1, Lco/uk/getmondo/d/t;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 405
    check-cast p2, Lco/uk/getmondo/d/t;

    invoke-static {p1, p2, p3, p4}, Lio/realm/ae;->a(Lio/realm/av;Lco/uk/getmondo/d/t;ZLjava/util/Map;)Lco/uk/getmondo/d/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_1

    .line 407
    :cond_5
    const-class v1, Lco/uk/getmondo/d/aa;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 408
    check-cast p2, Lco/uk/getmondo/d/aa;

    invoke-static {p1, p2, p3, p4}, Lio/realm/as;->a(Lio/realm/av;Lco/uk/getmondo/d/aa;ZLjava/util/Map;)Lco/uk/getmondo/d/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_1

    .line 410
    :cond_6
    const-class v1, Lco/uk/getmondo/d/n;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 411
    check-cast p2, Lco/uk/getmondo/d/n;

    invoke-static {p1, p2, p3, p4}, Lio/realm/u;->a(Lio/realm/av;Lco/uk/getmondo/d/n;ZLjava/util/Map;)Lco/uk/getmondo/d/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 413
    :cond_7
    const-class v1, Lco/uk/getmondo/d/u;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 414
    check-cast p2, Lco/uk/getmondo/d/u;

    invoke-static {p1, p2, p3, p4}, Lio/realm/ag;->a(Lio/realm/av;Lco/uk/getmondo/d/u;ZLjava/util/Map;)Lco/uk/getmondo/d/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 416
    :cond_8
    const-class v1, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 417
    check-cast p2, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-static {p1, p2, p3, p4}, Lio/realm/e;->a(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;ZLjava/util/Map;)Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 419
    :cond_9
    const-class v1, Lco/uk/getmondo/d/m;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 420
    check-cast p2, Lco/uk/getmondo/d/m;

    invoke-static {p1, p2, p3, p4}, Lio/realm/s;->a(Lio/realm/av;Lco/uk/getmondo/d/m;ZLjava/util/Map;)Lco/uk/getmondo/d/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 422
    :cond_a
    const-class v1, Lco/uk/getmondo/d/am;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 423
    check-cast p2, Lco/uk/getmondo/d/am;

    invoke-static {p1, p2, p3, p4}, Lio/realm/bo;->a(Lio/realm/av;Lco/uk/getmondo/d/am;ZLjava/util/Map;)Lco/uk/getmondo/d/am;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 425
    :cond_b
    const-class v1, Lco/uk/getmondo/d/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 426
    check-cast p2, Lco/uk/getmondo/d/b;

    invoke-static {p1, p2, p3, p4}, Lio/realm/a;->a(Lio/realm/av;Lco/uk/getmondo/d/b;ZLjava/util/Map;)Lco/uk/getmondo/d/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 428
    :cond_c
    const-class v1, Lco/uk/getmondo/d/r;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 429
    check-cast p2, Lco/uk/getmondo/d/r;

    invoke-static {p1, p2, p3, p4}, Lio/realm/ac;->a(Lio/realm/av;Lco/uk/getmondo/d/r;ZLjava/util/Map;)Lco/uk/getmondo/d/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 431
    :cond_d
    const-class v1, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 432
    check-cast p2, Lco/uk/getmondo/d/aj;

    invoke-static {p1, p2, p3, p4}, Lio/realm/bm;->a(Lio/realm/av;Lco/uk/getmondo/d/aj;ZLjava/util/Map;)Lco/uk/getmondo/d/aj;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 434
    :cond_e
    const-class v1, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 435
    check-cast p2, Lco/uk/getmondo/payments/a/a/a;

    invoke-static {p1, p2, p3, p4}, Lio/realm/m;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 437
    :cond_f
    const-class v1, Lco/uk/getmondo/d/f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 438
    check-cast p2, Lco/uk/getmondo/d/f;

    invoke-static {p1, p2, p3, p4}, Lio/realm/h;->a(Lio/realm/av;Lco/uk/getmondo/d/f;ZLjava/util/Map;)Lco/uk/getmondo/d/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 440
    :cond_10
    const-class v1, Lco/uk/getmondo/d/d;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 441
    check-cast p2, Lco/uk/getmondo/d/d;

    invoke-static {p1, p2, p3, p4}, Lio/realm/c;->a(Lio/realm/av;Lco/uk/getmondo/d/d;ZLjava/util/Map;)Lco/uk/getmondo/d/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 443
    :cond_11
    const-class v1, Lco/uk/getmondo/d/w;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 444
    check-cast p2, Lco/uk/getmondo/d/w;

    invoke-static {p1, p2, p3, p4}, Lio/realm/ao;->a(Lio/realm/av;Lco/uk/getmondo/d/w;ZLjava/util/Map;)Lco/uk/getmondo/d/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 446
    :cond_12
    const-class v1, Lco/uk/getmondo/d/o;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 447
    check-cast p2, Lco/uk/getmondo/d/o;

    invoke-static {p1, p2, p3, p4}, Lio/realm/y;->a(Lio/realm/av;Lco/uk/getmondo/d/o;ZLjava/util/Map;)Lco/uk/getmondo/d/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 449
    :cond_13
    const-class v1, Lco/uk/getmondo/d/g;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 450
    check-cast p2, Lco/uk/getmondo/d/g;

    invoke-static {p1, p2, p3, p4}, Lio/realm/j;->a(Lio/realm/av;Lco/uk/getmondo/d/g;ZLjava/util/Map;)Lco/uk/getmondo/d/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 452
    :cond_14
    const-class v1, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 453
    check-cast p2, Lco/uk/getmondo/payments/a/a/b;

    invoke-static {p1, p2, p3, p4}, Lio/realm/w;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 455
    :cond_15
    const-class v1, Lco/uk/getmondo/d/v;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 456
    check-cast p2, Lco/uk/getmondo/d/v;

    invoke-static {p1, p2, p3, p4}, Lio/realm/ai;->a(Lio/realm/av;Lco/uk/getmondo/d/v;ZLjava/util/Map;)Lco/uk/getmondo/d/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_1

    .line 458
    :cond_16
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public a(Lio/realm/bb;ILjava/util/Map;)Lio/realm/bb;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;I",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)TE;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 945
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 947
    const-class v1, Lco/uk/getmondo/d/p;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 948
    check-cast p1, Lco/uk/getmondo/d/p;

    invoke-static {p1, v2, p2, p3}, Lio/realm/aa;->a(Lco/uk/getmondo/d/p;IILjava/util/Map;)Lco/uk/getmondo/d/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    .line 1011
    :goto_0
    return-object v0

    .line 950
    :cond_0
    const-class v1, Lco/uk/getmondo/d/ag;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 951
    check-cast p1, Lco/uk/getmondo/d/ag;

    invoke-static {p1, v2, p2, p3}, Lio/realm/bj;->a(Lco/uk/getmondo/d/ag;IILjava/util/Map;)Lco/uk/getmondo/d/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0

    .line 953
    :cond_1
    const-class v1, Lco/uk/getmondo/d/l;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 954
    check-cast p1, Lco/uk/getmondo/d/l;

    invoke-static {p1, v2, p2, p3}, Lio/realm/q;->a(Lco/uk/getmondo/d/l;IILjava/util/Map;)Lco/uk/getmondo/d/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0

    .line 956
    :cond_2
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 957
    check-cast p1, Lco/uk/getmondo/payments/a/a/e;

    invoke-static {p1, v2, p2, p3}, Lio/realm/aq;->a(Lco/uk/getmondo/payments/a/a/e;IILjava/util/Map;)Lco/uk/getmondo/payments/a/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0

    .line 959
    :cond_3
    const-class v1, Lco/uk/getmondo/d/t;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 960
    check-cast p1, Lco/uk/getmondo/d/t;

    invoke-static {p1, v2, p2, p3}, Lio/realm/ae;->a(Lco/uk/getmondo/d/t;IILjava/util/Map;)Lco/uk/getmondo/d/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0

    .line 962
    :cond_4
    const-class v1, Lco/uk/getmondo/d/aa;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 963
    check-cast p1, Lco/uk/getmondo/d/aa;

    invoke-static {p1, v2, p2, p3}, Lio/realm/as;->a(Lco/uk/getmondo/d/aa;IILjava/util/Map;)Lco/uk/getmondo/d/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0

    .line 965
    :cond_5
    const-class v1, Lco/uk/getmondo/d/n;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 966
    check-cast p1, Lco/uk/getmondo/d/n;

    invoke-static {p1, v2, p2, p3}, Lio/realm/u;->a(Lco/uk/getmondo/d/n;IILjava/util/Map;)Lco/uk/getmondo/d/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0

    .line 968
    :cond_6
    const-class v1, Lco/uk/getmondo/d/u;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 969
    check-cast p1, Lco/uk/getmondo/d/u;

    invoke-static {p1, v2, p2, p3}, Lio/realm/ag;->a(Lco/uk/getmondo/d/u;IILjava/util/Map;)Lco/uk/getmondo/d/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 971
    :cond_7
    const-class v1, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 972
    check-cast p1, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-static {p1, v2, p2, p3}, Lio/realm/e;->a(Lco/uk/getmondo/payments/send/data/a/a;IILjava/util/Map;)Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 974
    :cond_8
    const-class v1, Lco/uk/getmondo/d/m;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 975
    check-cast p1, Lco/uk/getmondo/d/m;

    invoke-static {p1, v2, p2, p3}, Lio/realm/s;->a(Lco/uk/getmondo/d/m;IILjava/util/Map;)Lco/uk/getmondo/d/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 977
    :cond_9
    const-class v1, Lco/uk/getmondo/d/am;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 978
    check-cast p1, Lco/uk/getmondo/d/am;

    invoke-static {p1, v2, p2, p3}, Lio/realm/bo;->a(Lco/uk/getmondo/d/am;IILjava/util/Map;)Lco/uk/getmondo/d/am;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 980
    :cond_a
    const-class v1, Lco/uk/getmondo/d/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 981
    check-cast p1, Lco/uk/getmondo/d/b;

    invoke-static {p1, v2, p2, p3}, Lio/realm/a;->a(Lco/uk/getmondo/d/b;IILjava/util/Map;)Lco/uk/getmondo/d/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 983
    :cond_b
    const-class v1, Lco/uk/getmondo/d/r;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 984
    check-cast p1, Lco/uk/getmondo/d/r;

    invoke-static {p1, v2, p2, p3}, Lio/realm/ac;->a(Lco/uk/getmondo/d/r;IILjava/util/Map;)Lco/uk/getmondo/d/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 986
    :cond_c
    const-class v1, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 987
    check-cast p1, Lco/uk/getmondo/d/aj;

    invoke-static {p1, v2, p2, p3}, Lio/realm/bm;->a(Lco/uk/getmondo/d/aj;IILjava/util/Map;)Lco/uk/getmondo/d/aj;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 989
    :cond_d
    const-class v1, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 990
    check-cast p1, Lco/uk/getmondo/payments/a/a/a;

    invoke-static {p1, v2, p2, p3}, Lio/realm/m;->a(Lco/uk/getmondo/payments/a/a/a;IILjava/util/Map;)Lco/uk/getmondo/payments/a/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 992
    :cond_e
    const-class v1, Lco/uk/getmondo/d/f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 993
    check-cast p1, Lco/uk/getmondo/d/f;

    invoke-static {p1, v2, p2, p3}, Lio/realm/h;->a(Lco/uk/getmondo/d/f;IILjava/util/Map;)Lco/uk/getmondo/d/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 995
    :cond_f
    const-class v1, Lco/uk/getmondo/d/d;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 996
    check-cast p1, Lco/uk/getmondo/d/d;

    invoke-static {p1, v2, p2, p3}, Lio/realm/c;->a(Lco/uk/getmondo/d/d;IILjava/util/Map;)Lco/uk/getmondo/d/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 998
    :cond_10
    const-class v1, Lco/uk/getmondo/d/w;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 999
    check-cast p1, Lco/uk/getmondo/d/w;

    invoke-static {p1, v2, p2, p3}, Lio/realm/ao;->a(Lco/uk/getmondo/d/w;IILjava/util/Map;)Lco/uk/getmondo/d/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 1001
    :cond_11
    const-class v1, Lco/uk/getmondo/d/o;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1002
    check-cast p1, Lco/uk/getmondo/d/o;

    invoke-static {p1, v2, p2, p3}, Lio/realm/y;->a(Lco/uk/getmondo/d/o;IILjava/util/Map;)Lco/uk/getmondo/d/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 1004
    :cond_12
    const-class v1, Lco/uk/getmondo/d/g;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1005
    check-cast p1, Lco/uk/getmondo/d/g;

    invoke-static {p1, v2, p2, p3}, Lio/realm/j;->a(Lco/uk/getmondo/d/g;IILjava/util/Map;)Lco/uk/getmondo/d/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 1007
    :cond_13
    const-class v1, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 1008
    check-cast p1, Lco/uk/getmondo/payments/a/a/b;

    invoke-static {p1, v2, p2, p3}, Lio/realm/w;->a(Lco/uk/getmondo/payments/a/a/b;IILjava/util/Map;)Lco/uk/getmondo/payments/a/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 1010
    :cond_14
    const-class v1, Lco/uk/getmondo/d/v;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 1011
    check-cast p1, Lco/uk/getmondo/d/v;

    invoke-static {p1, v2, p2, p3}, Lio/realm/ai;->a(Lco/uk/getmondo/d/v;IILjava/util/Map;)Lco/uk/getmondo/d/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto/16 :goto_0

    .line 1013
    :cond_15
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)Lio/realm/bb;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/Object;",
            "Lio/realm/internal/n;",
            "Lio/realm/internal/c;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 304
    sget-object v1, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v1}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/g$b;

    .line 306
    :try_start_0
    move-object v0, p2

    check-cast v0, Lio/realm/g;

    move-object v2, v0

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v1 .. v6}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 307
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->b(Ljava/lang/Class;)V

    .line 309
    const-class v2, Lco/uk/getmondo/d/p;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 310
    new-instance v2, Lio/realm/aa;

    invoke-direct {v2}, Lio/realm/aa;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    .line 373
    :goto_0
    return-object v2

    .line 312
    :cond_0
    :try_start_1
    const-class v2, Lco/uk/getmondo/d/ag;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 313
    new-instance v2, Lio/realm/bj;

    invoke-direct {v2}, Lio/realm/bj;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto :goto_0

    .line 315
    :cond_1
    :try_start_2
    const-class v2, Lco/uk/getmondo/d/l;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 316
    new-instance v2, Lio/realm/q;

    invoke-direct {v2}, Lio/realm/q;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto :goto_0

    .line 318
    :cond_2
    :try_start_3
    const-class v2, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 319
    new-instance v2, Lio/realm/aq;

    invoke-direct {v2}, Lio/realm/aq;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto :goto_0

    .line 321
    :cond_3
    :try_start_4
    const-class v2, Lco/uk/getmondo/d/t;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 322
    new-instance v2, Lio/realm/ae;

    invoke-direct {v2}, Lio/realm/ae;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto :goto_0

    .line 324
    :cond_4
    :try_start_5
    const-class v2, Lco/uk/getmondo/d/aa;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 325
    new-instance v2, Lio/realm/as;

    invoke-direct {v2}, Lio/realm/as;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto :goto_0

    .line 327
    :cond_5
    :try_start_6
    const-class v2, Lco/uk/getmondo/d/n;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 328
    new-instance v2, Lio/realm/u;

    invoke-direct {v2}, Lio/realm/u;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 330
    :cond_6
    :try_start_7
    const-class v2, Lco/uk/getmondo/d/u;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 331
    new-instance v2, Lio/realm/ag;

    invoke-direct {v2}, Lio/realm/ag;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 333
    :cond_7
    :try_start_8
    const-class v2, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 334
    new-instance v2, Lio/realm/e;

    invoke-direct {v2}, Lio/realm/e;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 336
    :cond_8
    :try_start_9
    const-class v2, Lco/uk/getmondo/d/m;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 337
    new-instance v2, Lio/realm/s;

    invoke-direct {v2}, Lio/realm/s;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 339
    :cond_9
    :try_start_a
    const-class v2, Lco/uk/getmondo/d/am;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 340
    new-instance v2, Lio/realm/bo;

    invoke-direct {v2}, Lio/realm/bo;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 342
    :cond_a
    :try_start_b
    const-class v2, Lco/uk/getmondo/d/b;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 343
    new-instance v2, Lio/realm/a;

    invoke-direct {v2}, Lio/realm/a;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 345
    :cond_b
    :try_start_c
    const-class v2, Lco/uk/getmondo/d/r;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 346
    new-instance v2, Lio/realm/ac;

    invoke-direct {v2}, Lio/realm/ac;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 348
    :cond_c
    :try_start_d
    const-class v2, Lco/uk/getmondo/d/aj;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 349
    new-instance v2, Lio/realm/bm;

    invoke-direct {v2}, Lio/realm/bm;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 351
    :cond_d
    :try_start_e
    const-class v2, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 352
    new-instance v2, Lio/realm/m;

    invoke-direct {v2}, Lio/realm/m;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 354
    :cond_e
    :try_start_f
    const-class v2, Lco/uk/getmondo/d/f;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 355
    new-instance v2, Lio/realm/h;

    invoke-direct {v2}, Lio/realm/h;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 357
    :cond_f
    :try_start_10
    const-class v2, Lco/uk/getmondo/d/d;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 358
    new-instance v2, Lio/realm/c;

    invoke-direct {v2}, Lio/realm/c;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 360
    :cond_10
    :try_start_11
    const-class v2, Lco/uk/getmondo/d/w;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 361
    new-instance v2, Lio/realm/ao;

    invoke-direct {v2}, Lio/realm/ao;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 363
    :cond_11
    :try_start_12
    const-class v2, Lco/uk/getmondo/d/o;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 364
    new-instance v2, Lio/realm/y;

    invoke-direct {v2}, Lio/realm/y;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 366
    :cond_12
    :try_start_13
    const-class v2, Lco/uk/getmondo/d/g;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 367
    new-instance v2, Lio/realm/j;

    invoke-direct {v2}, Lio/realm/j;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 369
    :cond_13
    :try_start_14
    const-class v2, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 370
    new-instance v2, Lio/realm/w;

    invoke-direct {v2}, Lio/realm/w;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 372
    :cond_14
    :try_start_15
    const-class v2, Lco/uk/getmondo/d/v;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 373
    new-instance v2, Lio/realm/ai;

    invoke-direct {v2}, Lio/realm/ai;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/bb;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 377
    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    goto/16 :goto_0

    .line 375
    :cond_15
    :try_start_16
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v2

    throw v2
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 377
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lio/realm/g$b;->f()V

    throw v2
.end method

.method public a(Ljava/lang/Class;Lio/realm/internal/SharedRealm;Z)Lio/realm/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Lio/realm/internal/SharedRealm;",
            "Z)",
            "Lio/realm/internal/c;"
        }
    .end annotation

    .prologue
    .line 85
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->b(Ljava/lang/Class;)V

    .line 87
    const-class v0, Lco/uk/getmondo/d/p;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-static {p2, p3}, Lio/realm/aa;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/aa$a;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    .line 90
    :cond_0
    const-class v0, Lco/uk/getmondo/d/ag;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    invoke-static {p2, p3}, Lio/realm/bj;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/bj$a;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_1
    const-class v0, Lco/uk/getmondo/d/l;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    invoke-static {p2, p3}, Lio/realm/q;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/q$a;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_2
    const-class v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97
    invoke-static {p2, p3}, Lio/realm/aq;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/aq$a;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_3
    const-class v0, Lco/uk/getmondo/d/t;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 100
    invoke-static {p2, p3}, Lio/realm/ae;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/ae$a;

    move-result-object v0

    goto :goto_0

    .line 102
    :cond_4
    const-class v0, Lco/uk/getmondo/d/aa;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 103
    invoke-static {p2, p3}, Lio/realm/as;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/as$a;

    move-result-object v0

    goto :goto_0

    .line 105
    :cond_5
    const-class v0, Lco/uk/getmondo/d/n;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 106
    invoke-static {p2, p3}, Lio/realm/u;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/u$a;

    move-result-object v0

    goto :goto_0

    .line 108
    :cond_6
    const-class v0, Lco/uk/getmondo/d/u;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 109
    invoke-static {p2, p3}, Lio/realm/ag;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/ag$a;

    move-result-object v0

    goto :goto_0

    .line 111
    :cond_7
    const-class v0, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 112
    invoke-static {p2, p3}, Lio/realm/e;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/e$a;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_8
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 115
    invoke-static {p2, p3}, Lio/realm/s;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/s$a;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_9
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 118
    invoke-static {p2, p3}, Lio/realm/bo;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/bo$a;

    move-result-object v0

    goto/16 :goto_0

    .line 120
    :cond_a
    const-class v0, Lco/uk/getmondo/d/b;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 121
    invoke-static {p2, p3}, Lio/realm/a;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/a$a;

    move-result-object v0

    goto/16 :goto_0

    .line 123
    :cond_b
    const-class v0, Lco/uk/getmondo/d/r;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 124
    invoke-static {p2, p3}, Lio/realm/ac;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/ac$a;

    move-result-object v0

    goto/16 :goto_0

    .line 126
    :cond_c
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 127
    invoke-static {p2, p3}, Lio/realm/bm;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/bm$a;

    move-result-object v0

    goto/16 :goto_0

    .line 129
    :cond_d
    const-class v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 130
    invoke-static {p2, p3}, Lio/realm/m;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/m$a;

    move-result-object v0

    goto/16 :goto_0

    .line 132
    :cond_e
    const-class v0, Lco/uk/getmondo/d/f;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 133
    invoke-static {p2, p3}, Lio/realm/h;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/h$a;

    move-result-object v0

    goto/16 :goto_0

    .line 135
    :cond_f
    const-class v0, Lco/uk/getmondo/d/d;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 136
    invoke-static {p2, p3}, Lio/realm/c;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/c$a;

    move-result-object v0

    goto/16 :goto_0

    .line 138
    :cond_10
    const-class v0, Lco/uk/getmondo/d/w;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 139
    invoke-static {p2, p3}, Lio/realm/ao;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/ao$a;

    move-result-object v0

    goto/16 :goto_0

    .line 141
    :cond_11
    const-class v0, Lco/uk/getmondo/d/o;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 142
    invoke-static {p2, p3}, Lio/realm/y;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/y$a;

    move-result-object v0

    goto/16 :goto_0

    .line 144
    :cond_12
    const-class v0, Lco/uk/getmondo/d/g;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 145
    invoke-static {p2, p3}, Lio/realm/j;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/j$a;

    move-result-object v0

    goto/16 :goto_0

    .line 147
    :cond_13
    const-class v0, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 148
    invoke-static {p2, p3}, Lio/realm/w;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/w$a;

    move-result-object v0

    goto/16 :goto_0

    .line 150
    :cond_14
    const-class v0, Lco/uk/getmondo/d/v;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 151
    invoke-static {p2, p3}, Lio/realm/ai;->a(Lio/realm/internal/SharedRealm;Z)Lio/realm/ai$a;

    move-result-object v0

    goto/16 :goto_0

    .line 153
    :cond_15
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 231
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->b(Ljava/lang/Class;)V

    .line 233
    const-class v0, Lco/uk/getmondo/d/p;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-static {}, Lio/realm/aa;->i()Ljava/lang/String;

    move-result-object v0

    .line 297
    :goto_0
    return-object v0

    .line 236
    :cond_0
    const-class v0, Lco/uk/getmondo/d/ag;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    invoke-static {}, Lio/realm/bj;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 239
    :cond_1
    const-class v0, Lco/uk/getmondo/d/l;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    invoke-static {}, Lio/realm/q;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 242
    :cond_2
    const-class v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 243
    invoke-static {}, Lio/realm/aq;->q()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 245
    :cond_3
    const-class v0, Lco/uk/getmondo/d/t;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    invoke-static {}, Lio/realm/ae;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 248
    :cond_4
    const-class v0, Lco/uk/getmondo/d/aa;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 249
    invoke-static {}, Lio/realm/as;->r()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 251
    :cond_5
    const-class v0, Lco/uk/getmondo/d/n;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 252
    invoke-static {}, Lio/realm/u;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 254
    :cond_6
    const-class v0, Lco/uk/getmondo/d/u;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 255
    invoke-static {}, Lio/realm/ag;->y()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 257
    :cond_7
    const-class v0, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 258
    invoke-static {}, Lio/realm/e;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 260
    :cond_8
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 261
    invoke-static {}, Lio/realm/s;->y()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 263
    :cond_9
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 264
    invoke-static {}, Lio/realm/bo;->l()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 266
    :cond_a
    const-class v0, Lco/uk/getmondo/d/b;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 267
    invoke-static {}, Lio/realm/a;->m()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 269
    :cond_b
    const-class v0, Lco/uk/getmondo/d/r;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 270
    invoke-static {}, Lio/realm/ac;->g()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 272
    :cond_c
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 273
    invoke-static {}, Lio/realm/bm;->ai()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 275
    :cond_d
    const-class v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 276
    invoke-static {}, Lio/realm/m;->q()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 278
    :cond_e
    const-class v0, Lco/uk/getmondo/d/f;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 279
    invoke-static {}, Lio/realm/h;->i()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 281
    :cond_f
    const-class v0, Lco/uk/getmondo/d/d;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 282
    invoke-static {}, Lio/realm/c;->h()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 284
    :cond_10
    const-class v0, Lco/uk/getmondo/d/w;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 285
    invoke-static {}, Lio/realm/ao;->u()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 287
    :cond_11
    const-class v0, Lco/uk/getmondo/d/o;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 288
    invoke-static {}, Lio/realm/y;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 290
    :cond_12
    const-class v0, Lco/uk/getmondo/d/g;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 291
    invoke-static {}, Lio/realm/j;->q()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 293
    :cond_13
    const-class v0, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 294
    invoke-static {}, Lio/realm/w;->l()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 296
    :cond_14
    const-class v0, Lco/uk/getmondo/d/v;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 297
    invoke-static {}, Lio/realm/ai;->h()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 299
    :cond_15
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Lio/realm/internal/OsObjectSchemaInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 58
    const-class v1, Lco/uk/getmondo/d/p;

    invoke-static {}, Lio/realm/aa;->h()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-class v1, Lco/uk/getmondo/d/ag;

    invoke-static {}, Lio/realm/bj;->c()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-class v1, Lco/uk/getmondo/d/l;

    invoke-static {}, Lio/realm/q;->d()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-static {}, Lio/realm/aq;->p()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const-class v1, Lco/uk/getmondo/d/t;

    invoke-static {}, Lio/realm/ae;->d()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-class v1, Lco/uk/getmondo/d/aa;

    invoke-static {}, Lio/realm/as;->q()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const-class v1, Lco/uk/getmondo/d/n;

    invoke-static {}, Lio/realm/u;->c()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-class v1, Lco/uk/getmondo/d/u;

    invoke-static {}, Lio/realm/ag;->x()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-class v1, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-static {}, Lio/realm/e;->h()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const-class v1, Lco/uk/getmondo/d/m;

    invoke-static {}, Lio/realm/s;->x()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const-class v1, Lco/uk/getmondo/d/am;

    invoke-static {}, Lio/realm/bo;->j()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const-class v1, Lco/uk/getmondo/d/b;

    invoke-static {}, Lio/realm/a;->l()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const-class v1, Lco/uk/getmondo/d/r;

    invoke-static {}, Lio/realm/ac;->f()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const-class v1, Lco/uk/getmondo/d/aj;

    invoke-static {}, Lio/realm/bm;->ah()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-class v1, Lco/uk/getmondo/payments/a/a/a;

    invoke-static {}, Lio/realm/m;->p()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const-class v1, Lco/uk/getmondo/d/f;

    invoke-static {}, Lio/realm/h;->h()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    const-class v1, Lco/uk/getmondo/d/d;

    invoke-static {}, Lio/realm/c;->g()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-class v1, Lco/uk/getmondo/d/w;

    invoke-static {}, Lio/realm/ao;->t()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    const-class v1, Lco/uk/getmondo/d/o;

    invoke-static {}, Lio/realm/y;->c()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const-class v1, Lco/uk/getmondo/d/g;

    invoke-static {}, Lio/realm/j;->p()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    const-class v1, Lco/uk/getmondo/payments/a/a/b;

    invoke-static {}, Lio/realm/w;->j()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    const-class v1, Lco/uk/getmondo/d/v;

    invoke-static {}, Lio/realm/ai;->g()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    return-object v0
.end method

.method public a(Lio/realm/av;Lio/realm/bb;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lio/realm/bb;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    instance-of v0, p2, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 467
    :goto_0
    const-class v1, Lco/uk/getmondo/d/p;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 468
    check-cast p2, Lco/uk/getmondo/d/p;

    invoke-static {p1, p2, p3}, Lio/realm/aa;->a(Lio/realm/av;Lco/uk/getmondo/d/p;Ljava/util/Map;)J

    .line 514
    :goto_1
    return-void

    .line 465
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 469
    :cond_1
    const-class v1, Lco/uk/getmondo/d/ag;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 470
    check-cast p2, Lco/uk/getmondo/d/ag;

    invoke-static {p1, p2, p3}, Lio/realm/bj;->a(Lio/realm/av;Lco/uk/getmondo/d/ag;Ljava/util/Map;)J

    goto :goto_1

    .line 471
    :cond_2
    const-class v1, Lco/uk/getmondo/d/l;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 472
    check-cast p2, Lco/uk/getmondo/d/l;

    invoke-static {p1, p2, p3}, Lio/realm/q;->a(Lio/realm/av;Lco/uk/getmondo/d/l;Ljava/util/Map;)J

    goto :goto_1

    .line 473
    :cond_3
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 474
    check-cast p2, Lco/uk/getmondo/payments/a/a/e;

    invoke-static {p1, p2, p3}, Lio/realm/aq;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;Ljava/util/Map;)J

    goto :goto_1

    .line 475
    :cond_4
    const-class v1, Lco/uk/getmondo/d/t;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 476
    check-cast p2, Lco/uk/getmondo/d/t;

    invoke-static {p1, p2, p3}, Lio/realm/ae;->a(Lio/realm/av;Lco/uk/getmondo/d/t;Ljava/util/Map;)J

    goto :goto_1

    .line 477
    :cond_5
    const-class v1, Lco/uk/getmondo/d/aa;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 478
    check-cast p2, Lco/uk/getmondo/d/aa;

    invoke-static {p1, p2, p3}, Lio/realm/as;->a(Lio/realm/av;Lco/uk/getmondo/d/aa;Ljava/util/Map;)J

    goto :goto_1

    .line 479
    :cond_6
    const-class v1, Lco/uk/getmondo/d/n;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 480
    check-cast p2, Lco/uk/getmondo/d/n;

    invoke-static {p1, p2, p3}, Lio/realm/u;->a(Lio/realm/av;Lco/uk/getmondo/d/n;Ljava/util/Map;)J

    goto :goto_1

    .line 481
    :cond_7
    const-class v1, Lco/uk/getmondo/d/u;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 482
    check-cast p2, Lco/uk/getmondo/d/u;

    invoke-static {p1, p2, p3}, Lio/realm/ag;->a(Lio/realm/av;Lco/uk/getmondo/d/u;Ljava/util/Map;)J

    goto :goto_1

    .line 483
    :cond_8
    const-class v1, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 484
    check-cast p2, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-static {p1, p2, p3}, Lio/realm/e;->a(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;Ljava/util/Map;)J

    goto :goto_1

    .line 485
    :cond_9
    const-class v1, Lco/uk/getmondo/d/m;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 486
    check-cast p2, Lco/uk/getmondo/d/m;

    invoke-static {p1, p2, p3}, Lio/realm/s;->a(Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 487
    :cond_a
    const-class v1, Lco/uk/getmondo/d/am;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 488
    check-cast p2, Lco/uk/getmondo/d/am;

    invoke-static {p1, p2, p3}, Lio/realm/bo;->a(Lio/realm/av;Lco/uk/getmondo/d/am;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 489
    :cond_b
    const-class v1, Lco/uk/getmondo/d/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 490
    check-cast p2, Lco/uk/getmondo/d/b;

    invoke-static {p1, p2, p3}, Lio/realm/a;->a(Lio/realm/av;Lco/uk/getmondo/d/b;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 491
    :cond_c
    const-class v1, Lco/uk/getmondo/d/r;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 492
    check-cast p2, Lco/uk/getmondo/d/r;

    invoke-static {p1, p2, p3}, Lio/realm/ac;->a(Lio/realm/av;Lco/uk/getmondo/d/r;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 493
    :cond_d
    const-class v1, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 494
    check-cast p2, Lco/uk/getmondo/d/aj;

    invoke-static {p1, p2, p3}, Lio/realm/bm;->a(Lio/realm/av;Lco/uk/getmondo/d/aj;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 495
    :cond_e
    const-class v1, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 496
    check-cast p2, Lco/uk/getmondo/payments/a/a/a;

    invoke-static {p1, p2, p3}, Lio/realm/m;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 497
    :cond_f
    const-class v1, Lco/uk/getmondo/d/f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 498
    check-cast p2, Lco/uk/getmondo/d/f;

    invoke-static {p1, p2, p3}, Lio/realm/h;->a(Lio/realm/av;Lco/uk/getmondo/d/f;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 499
    :cond_10
    const-class v1, Lco/uk/getmondo/d/d;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 500
    check-cast p2, Lco/uk/getmondo/d/d;

    invoke-static {p1, p2, p3}, Lio/realm/c;->a(Lio/realm/av;Lco/uk/getmondo/d/d;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 501
    :cond_11
    const-class v1, Lco/uk/getmondo/d/w;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 502
    check-cast p2, Lco/uk/getmondo/d/w;

    invoke-static {p1, p2, p3}, Lio/realm/ao;->a(Lio/realm/av;Lco/uk/getmondo/d/w;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 503
    :cond_12
    const-class v1, Lco/uk/getmondo/d/o;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 504
    check-cast p2, Lco/uk/getmondo/d/o;

    invoke-static {p1, p2, p3}, Lio/realm/y;->a(Lio/realm/av;Lco/uk/getmondo/d/o;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 505
    :cond_13
    const-class v1, Lco/uk/getmondo/d/g;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 506
    check-cast p2, Lco/uk/getmondo/d/g;

    invoke-static {p1, p2, p3}, Lio/realm/j;->a(Lio/realm/av;Lco/uk/getmondo/d/g;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 507
    :cond_14
    const-class v1, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 508
    check-cast p2, Lco/uk/getmondo/payments/a/a/b;

    invoke-static {p1, p2, p3}, Lio/realm/w;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 509
    :cond_15
    const-class v1, Lco/uk/getmondo/d/v;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 510
    check-cast p2, Lco/uk/getmondo/d/v;

    invoke-static {p1, p2, p3}, Lio/realm/ai;->a(Lio/realm/av;Lco/uk/getmondo/d/v;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 512
    :cond_16
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public a(Lio/realm/av;Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Collection",
            "<+",
            "Lio/realm/bb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 684
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 686
    new-instance v3, Ljava/util/HashMap;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 687
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 689
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    .line 692
    instance-of v1, v0, Lio/realm/internal/l;

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    .line 694
    :goto_0
    const-class v4, Lco/uk/getmondo/d/p;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 695
    check-cast v0, Lco/uk/getmondo/d/p;

    invoke-static {p1, v0, v3}, Lio/realm/aa;->b(Lio/realm/av;Lco/uk/getmondo/d/p;Ljava/util/Map;)J

    .line 741
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742
    const-class v0, Lco/uk/getmondo/d/p;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 743
    invoke-static {p1, v2, v3}, Lio/realm/aa;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    .line 791
    :cond_0
    :goto_2
    return-void

    .line 692
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_0

    .line 696
    :cond_2
    const-class v4, Lco/uk/getmondo/d/ag;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 697
    check-cast v0, Lco/uk/getmondo/d/ag;

    invoke-static {p1, v0, v3}, Lio/realm/bj;->b(Lio/realm/av;Lco/uk/getmondo/d/ag;Ljava/util/Map;)J

    goto :goto_1

    .line 698
    :cond_3
    const-class v4, Lco/uk/getmondo/d/l;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 699
    check-cast v0, Lco/uk/getmondo/d/l;

    invoke-static {p1, v0, v3}, Lio/realm/q;->b(Lio/realm/av;Lco/uk/getmondo/d/l;Ljava/util/Map;)J

    goto :goto_1

    .line 700
    :cond_4
    const-class v4, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 701
    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-static {p1, v0, v3}, Lio/realm/aq;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;Ljava/util/Map;)J

    goto :goto_1

    .line 702
    :cond_5
    const-class v4, Lco/uk/getmondo/d/t;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 703
    check-cast v0, Lco/uk/getmondo/d/t;

    invoke-static {p1, v0, v3}, Lio/realm/ae;->b(Lio/realm/av;Lco/uk/getmondo/d/t;Ljava/util/Map;)J

    goto :goto_1

    .line 704
    :cond_6
    const-class v4, Lco/uk/getmondo/d/aa;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 705
    check-cast v0, Lco/uk/getmondo/d/aa;

    invoke-static {p1, v0, v3}, Lio/realm/as;->b(Lio/realm/av;Lco/uk/getmondo/d/aa;Ljava/util/Map;)J

    goto :goto_1

    .line 706
    :cond_7
    const-class v4, Lco/uk/getmondo/d/n;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 707
    check-cast v0, Lco/uk/getmondo/d/n;

    invoke-static {p1, v0, v3}, Lio/realm/u;->b(Lio/realm/av;Lco/uk/getmondo/d/n;Ljava/util/Map;)J

    goto :goto_1

    .line 708
    :cond_8
    const-class v4, Lco/uk/getmondo/d/u;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 709
    check-cast v0, Lco/uk/getmondo/d/u;

    invoke-static {p1, v0, v3}, Lio/realm/ag;->b(Lio/realm/av;Lco/uk/getmondo/d/u;Ljava/util/Map;)J

    goto :goto_1

    .line 710
    :cond_9
    const-class v4, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 711
    check-cast v0, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-static {p1, v0, v3}, Lio/realm/e;->b(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 712
    :cond_a
    const-class v4, Lco/uk/getmondo/d/m;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 713
    check-cast v0, Lco/uk/getmondo/d/m;

    invoke-static {p1, v0, v3}, Lio/realm/s;->b(Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 714
    :cond_b
    const-class v4, Lco/uk/getmondo/d/am;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 715
    check-cast v0, Lco/uk/getmondo/d/am;

    invoke-static {p1, v0, v3}, Lio/realm/bo;->b(Lio/realm/av;Lco/uk/getmondo/d/am;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 716
    :cond_c
    const-class v4, Lco/uk/getmondo/d/b;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 717
    check-cast v0, Lco/uk/getmondo/d/b;

    invoke-static {p1, v0, v3}, Lio/realm/a;->b(Lio/realm/av;Lco/uk/getmondo/d/b;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 718
    :cond_d
    const-class v4, Lco/uk/getmondo/d/r;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 719
    check-cast v0, Lco/uk/getmondo/d/r;

    invoke-static {p1, v0, v3}, Lio/realm/ac;->b(Lio/realm/av;Lco/uk/getmondo/d/r;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 720
    :cond_e
    const-class v4, Lco/uk/getmondo/d/aj;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 721
    check-cast v0, Lco/uk/getmondo/d/aj;

    invoke-static {p1, v0, v3}, Lio/realm/bm;->b(Lio/realm/av;Lco/uk/getmondo/d/aj;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 722
    :cond_f
    const-class v4, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 723
    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-static {p1, v0, v3}, Lio/realm/m;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 724
    :cond_10
    const-class v4, Lco/uk/getmondo/d/f;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 725
    check-cast v0, Lco/uk/getmondo/d/f;

    invoke-static {p1, v0, v3}, Lio/realm/h;->b(Lio/realm/av;Lco/uk/getmondo/d/f;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 726
    :cond_11
    const-class v4, Lco/uk/getmondo/d/d;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 727
    check-cast v0, Lco/uk/getmondo/d/d;

    invoke-static {p1, v0, v3}, Lio/realm/c;->b(Lio/realm/av;Lco/uk/getmondo/d/d;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 728
    :cond_12
    const-class v4, Lco/uk/getmondo/d/w;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 729
    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-static {p1, v0, v3}, Lio/realm/ao;->b(Lio/realm/av;Lco/uk/getmondo/d/w;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 730
    :cond_13
    const-class v4, Lco/uk/getmondo/d/o;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 731
    check-cast v0, Lco/uk/getmondo/d/o;

    invoke-static {p1, v0, v3}, Lio/realm/y;->b(Lio/realm/av;Lco/uk/getmondo/d/o;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 732
    :cond_14
    const-class v4, Lco/uk/getmondo/d/g;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 733
    check-cast v0, Lco/uk/getmondo/d/g;

    invoke-static {p1, v0, v3}, Lio/realm/j;->b(Lio/realm/av;Lco/uk/getmondo/d/g;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 734
    :cond_15
    const-class v4, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 735
    check-cast v0, Lco/uk/getmondo/payments/a/a/b;

    invoke-static {p1, v0, v3}, Lio/realm/w;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 736
    :cond_16
    const-class v4, Lco/uk/getmondo/d/v;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 737
    check-cast v0, Lco/uk/getmondo/d/v;

    invoke-static {p1, v0, v3}, Lio/realm/ai;->b(Lio/realm/av;Lco/uk/getmondo/d/v;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 739
    :cond_17
    invoke-static {v1}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0

    .line 744
    :cond_18
    const-class v0, Lco/uk/getmondo/d/ag;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 745
    invoke-static {p1, v2, v3}, Lio/realm/bj;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 746
    :cond_19
    const-class v0, Lco/uk/getmondo/d/l;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 747
    invoke-static {p1, v2, v3}, Lio/realm/q;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 748
    :cond_1a
    const-class v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 749
    invoke-static {p1, v2, v3}, Lio/realm/aq;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 750
    :cond_1b
    const-class v0, Lco/uk/getmondo/d/t;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 751
    invoke-static {p1, v2, v3}, Lio/realm/ae;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 752
    :cond_1c
    const-class v0, Lco/uk/getmondo/d/aa;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 753
    invoke-static {p1, v2, v3}, Lio/realm/as;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 754
    :cond_1d
    const-class v0, Lco/uk/getmondo/d/n;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 755
    invoke-static {p1, v2, v3}, Lio/realm/u;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 756
    :cond_1e
    const-class v0, Lco/uk/getmondo/d/u;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 757
    invoke-static {p1, v2, v3}, Lio/realm/ag;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 758
    :cond_1f
    const-class v0, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 759
    invoke-static {p1, v2, v3}, Lio/realm/e;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 760
    :cond_20
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 761
    invoke-static {p1, v2, v3}, Lio/realm/s;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 762
    :cond_21
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 763
    invoke-static {p1, v2, v3}, Lio/realm/bo;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 764
    :cond_22
    const-class v0, Lco/uk/getmondo/d/b;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 765
    invoke-static {p1, v2, v3}, Lio/realm/a;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 766
    :cond_23
    const-class v0, Lco/uk/getmondo/d/r;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 767
    invoke-static {p1, v2, v3}, Lio/realm/ac;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 768
    :cond_24
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 769
    invoke-static {p1, v2, v3}, Lio/realm/bm;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 770
    :cond_25
    const-class v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 771
    invoke-static {p1, v2, v3}, Lio/realm/m;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 772
    :cond_26
    const-class v0, Lco/uk/getmondo/d/f;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 773
    invoke-static {p1, v2, v3}, Lio/realm/h;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 774
    :cond_27
    const-class v0, Lco/uk/getmondo/d/d;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 775
    invoke-static {p1, v2, v3}, Lio/realm/c;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 776
    :cond_28
    const-class v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 777
    invoke-static {p1, v2, v3}, Lio/realm/ao;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 778
    :cond_29
    const-class v0, Lco/uk/getmondo/d/o;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 779
    invoke-static {p1, v2, v3}, Lio/realm/y;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 780
    :cond_2a
    const-class v0, Lco/uk/getmondo/d/g;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 781
    invoke-static {p1, v2, v3}, Lio/realm/j;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 782
    :cond_2b
    const-class v0, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 783
    invoke-static {p1, v2, v3}, Lio/realm/w;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 784
    :cond_2c
    const-class v0, Lco/uk/getmondo/d/v;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 785
    invoke-static {p1, v2, v3}, Lio/realm/ai;->a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 787
    :cond_2d
    invoke-static {v1}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 383
    sget-object v0, Lio/realm/DefaultRealmModuleMediator;->a:Ljava/util/Set;

    return-object v0
.end method

.method public b(Lio/realm/av;Lio/realm/bb;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lio/realm/bb;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 631
    instance-of v0, p2, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 633
    :goto_0
    const-class v1, Lco/uk/getmondo/d/p;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 634
    check-cast p2, Lco/uk/getmondo/d/p;

    invoke-static {p1, p2, p3}, Lio/realm/aa;->b(Lio/realm/av;Lco/uk/getmondo/d/p;Ljava/util/Map;)J

    .line 680
    :goto_1
    return-void

    .line 631
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 635
    :cond_1
    const-class v1, Lco/uk/getmondo/d/ag;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 636
    check-cast p2, Lco/uk/getmondo/d/ag;

    invoke-static {p1, p2, p3}, Lio/realm/bj;->b(Lio/realm/av;Lco/uk/getmondo/d/ag;Ljava/util/Map;)J

    goto :goto_1

    .line 637
    :cond_2
    const-class v1, Lco/uk/getmondo/d/l;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 638
    check-cast p2, Lco/uk/getmondo/d/l;

    invoke-static {p1, p2, p3}, Lio/realm/q;->b(Lio/realm/av;Lco/uk/getmondo/d/l;Ljava/util/Map;)J

    goto :goto_1

    .line 639
    :cond_3
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 640
    check-cast p2, Lco/uk/getmondo/payments/a/a/e;

    invoke-static {p1, p2, p3}, Lio/realm/aq;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;Ljava/util/Map;)J

    goto :goto_1

    .line 641
    :cond_4
    const-class v1, Lco/uk/getmondo/d/t;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 642
    check-cast p2, Lco/uk/getmondo/d/t;

    invoke-static {p1, p2, p3}, Lio/realm/ae;->b(Lio/realm/av;Lco/uk/getmondo/d/t;Ljava/util/Map;)J

    goto :goto_1

    .line 643
    :cond_5
    const-class v1, Lco/uk/getmondo/d/aa;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 644
    check-cast p2, Lco/uk/getmondo/d/aa;

    invoke-static {p1, p2, p3}, Lio/realm/as;->b(Lio/realm/av;Lco/uk/getmondo/d/aa;Ljava/util/Map;)J

    goto :goto_1

    .line 645
    :cond_6
    const-class v1, Lco/uk/getmondo/d/n;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 646
    check-cast p2, Lco/uk/getmondo/d/n;

    invoke-static {p1, p2, p3}, Lio/realm/u;->b(Lio/realm/av;Lco/uk/getmondo/d/n;Ljava/util/Map;)J

    goto :goto_1

    .line 647
    :cond_7
    const-class v1, Lco/uk/getmondo/d/u;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 648
    check-cast p2, Lco/uk/getmondo/d/u;

    invoke-static {p1, p2, p3}, Lio/realm/ag;->b(Lio/realm/av;Lco/uk/getmondo/d/u;Ljava/util/Map;)J

    goto :goto_1

    .line 649
    :cond_8
    const-class v1, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 650
    check-cast p2, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-static {p1, p2, p3}, Lio/realm/e;->b(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;Ljava/util/Map;)J

    goto :goto_1

    .line 651
    :cond_9
    const-class v1, Lco/uk/getmondo/d/m;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 652
    check-cast p2, Lco/uk/getmondo/d/m;

    invoke-static {p1, p2, p3}, Lio/realm/s;->b(Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 653
    :cond_a
    const-class v1, Lco/uk/getmondo/d/am;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 654
    check-cast p2, Lco/uk/getmondo/d/am;

    invoke-static {p1, p2, p3}, Lio/realm/bo;->b(Lio/realm/av;Lco/uk/getmondo/d/am;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 655
    :cond_b
    const-class v1, Lco/uk/getmondo/d/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 656
    check-cast p2, Lco/uk/getmondo/d/b;

    invoke-static {p1, p2, p3}, Lio/realm/a;->b(Lio/realm/av;Lco/uk/getmondo/d/b;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 657
    :cond_c
    const-class v1, Lco/uk/getmondo/d/r;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 658
    check-cast p2, Lco/uk/getmondo/d/r;

    invoke-static {p1, p2, p3}, Lio/realm/ac;->b(Lio/realm/av;Lco/uk/getmondo/d/r;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 659
    :cond_d
    const-class v1, Lco/uk/getmondo/d/aj;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 660
    check-cast p2, Lco/uk/getmondo/d/aj;

    invoke-static {p1, p2, p3}, Lio/realm/bm;->b(Lio/realm/av;Lco/uk/getmondo/d/aj;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 661
    :cond_e
    const-class v1, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 662
    check-cast p2, Lco/uk/getmondo/payments/a/a/a;

    invoke-static {p1, p2, p3}, Lio/realm/m;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/a;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 663
    :cond_f
    const-class v1, Lco/uk/getmondo/d/f;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 664
    check-cast p2, Lco/uk/getmondo/d/f;

    invoke-static {p1, p2, p3}, Lio/realm/h;->b(Lio/realm/av;Lco/uk/getmondo/d/f;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 665
    :cond_10
    const-class v1, Lco/uk/getmondo/d/d;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 666
    check-cast p2, Lco/uk/getmondo/d/d;

    invoke-static {p1, p2, p3}, Lio/realm/c;->b(Lio/realm/av;Lco/uk/getmondo/d/d;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 667
    :cond_11
    const-class v1, Lco/uk/getmondo/d/w;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 668
    check-cast p2, Lco/uk/getmondo/d/w;

    invoke-static {p1, p2, p3}, Lio/realm/ao;->b(Lio/realm/av;Lco/uk/getmondo/d/w;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 669
    :cond_12
    const-class v1, Lco/uk/getmondo/d/o;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 670
    check-cast p2, Lco/uk/getmondo/d/o;

    invoke-static {p1, p2, p3}, Lio/realm/y;->b(Lio/realm/av;Lco/uk/getmondo/d/o;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 671
    :cond_13
    const-class v1, Lco/uk/getmondo/d/g;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 672
    check-cast p2, Lco/uk/getmondo/d/g;

    invoke-static {p1, p2, p3}, Lio/realm/j;->b(Lio/realm/av;Lco/uk/getmondo/d/g;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 673
    :cond_14
    const-class v1, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 674
    check-cast p2, Lco/uk/getmondo/payments/a/a/b;

    invoke-static {p1, p2, p3}, Lio/realm/w;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 675
    :cond_15
    const-class v1, Lco/uk/getmondo/d/v;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 676
    check-cast p2, Lco/uk/getmondo/d/v;

    invoke-static {p1, p2, p3}, Lio/realm/ai;->b(Lio/realm/av;Lco/uk/getmondo/d/v;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 678
    :cond_16
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->c(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
