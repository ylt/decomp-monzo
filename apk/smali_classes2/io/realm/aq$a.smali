.class final Lio/realm/aq$a;
.super Lio/realm/internal/c;
.source "PaymentSeriesRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/aq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:J

.field f:J

.field g:J

.field h:J

.field i:J

.field j:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 49
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 50
    const-string v0, "id"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->a:J

    .line 51
    const-string v0, "accountId"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->b:J

    .line 52
    const-string v0, "scheme"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->c:J

    .line 53
    const-string v0, "schemaData"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->d:J

    .line 54
    const-string v0, "_amount"

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->e:J

    .line 55
    const-string v0, "_currency"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->f:J

    .line 56
    const-string v0, "_startDate"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->g:J

    .line 57
    const-string v0, "_nextIterationDate"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->h:J

    .line 58
    const-string v0, "_endDate"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->i:J

    .line 59
    const-string v0, "_intervalType"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/aq$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/aq$a;->j:J

    .line 60
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 64
    invoke-virtual {p0, p1, p0}, Lio/realm/aq$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 65
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lio/realm/aq$a;

    invoke-direct {v0, p0, p1}, Lio/realm/aq$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 74
    check-cast p1, Lio/realm/aq$a;

    .line 75
    check-cast p2, Lio/realm/aq$a;

    .line 76
    iget-wide v0, p1, Lio/realm/aq$a;->a:J

    iput-wide v0, p2, Lio/realm/aq$a;->a:J

    .line 77
    iget-wide v0, p1, Lio/realm/aq$a;->b:J

    iput-wide v0, p2, Lio/realm/aq$a;->b:J

    .line 78
    iget-wide v0, p1, Lio/realm/aq$a;->c:J

    iput-wide v0, p2, Lio/realm/aq$a;->c:J

    .line 79
    iget-wide v0, p1, Lio/realm/aq$a;->d:J

    iput-wide v0, p2, Lio/realm/aq$a;->d:J

    .line 80
    iget-wide v0, p1, Lio/realm/aq$a;->e:J

    iput-wide v0, p2, Lio/realm/aq$a;->e:J

    .line 81
    iget-wide v0, p1, Lio/realm/aq$a;->f:J

    iput-wide v0, p2, Lio/realm/aq$a;->f:J

    .line 82
    iget-wide v0, p1, Lio/realm/aq$a;->g:J

    iput-wide v0, p2, Lio/realm/aq$a;->g:J

    .line 83
    iget-wide v0, p1, Lio/realm/aq$a;->h:J

    iput-wide v0, p2, Lio/realm/aq$a;->h:J

    .line 84
    iget-wide v0, p1, Lio/realm/aq$a;->i:J

    iput-wide v0, p2, Lio/realm/aq$a;->i:J

    .line 85
    iget-wide v0, p1, Lio/realm/aq$a;->j:J

    iput-wide v0, p2, Lio/realm/aq$a;->j:J

    .line 86
    return-void
.end method
