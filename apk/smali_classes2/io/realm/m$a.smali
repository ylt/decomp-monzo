.class final Lio/realm/m$a;
.super Lio/realm/internal/c;
.source "DirectDebitRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:J

.field f:J

.field g:J

.field h:J

.field i:J

.field j:J

.field k:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 50
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 51
    const-string v0, "id"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->a:J

    .line 52
    const-string v0, "accountId"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->b:J

    .line 53
    const-string v0, "payerSortCode"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->c:J

    .line 54
    const-string v0, "payerAccountNumber"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->d:J

    .line 55
    const-string v0, "payerName"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->e:J

    .line 56
    const-string v0, "serviceUserNumber"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->f:J

    .line 57
    const-string v0, "serviceUserName"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->g:J

    .line 58
    const-string v0, "reference"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->h:J

    .line 59
    const-string v0, "active"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->i:J

    .line 60
    const-string v0, "_created"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->j:J

    .line 61
    const-string v0, "_lastCollected"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/m$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/m$a;->k:J

    .line 62
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 66
    invoke-virtual {p0, p1, p0}, Lio/realm/m$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 67
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lio/realm/m$a;

    invoke-direct {v0, p0, p1}, Lio/realm/m$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 76
    check-cast p1, Lio/realm/m$a;

    .line 77
    check-cast p2, Lio/realm/m$a;

    .line 78
    iget-wide v0, p1, Lio/realm/m$a;->a:J

    iput-wide v0, p2, Lio/realm/m$a;->a:J

    .line 79
    iget-wide v0, p1, Lio/realm/m$a;->b:J

    iput-wide v0, p2, Lio/realm/m$a;->b:J

    .line 80
    iget-wide v0, p1, Lio/realm/m$a;->c:J

    iput-wide v0, p2, Lio/realm/m$a;->c:J

    .line 81
    iget-wide v0, p1, Lio/realm/m$a;->d:J

    iput-wide v0, p2, Lio/realm/m$a;->d:J

    .line 82
    iget-wide v0, p1, Lio/realm/m$a;->e:J

    iput-wide v0, p2, Lio/realm/m$a;->e:J

    .line 83
    iget-wide v0, p1, Lio/realm/m$a;->f:J

    iput-wide v0, p2, Lio/realm/m$a;->f:J

    .line 84
    iget-wide v0, p1, Lio/realm/m$a;->g:J

    iput-wide v0, p2, Lio/realm/m$a;->g:J

    .line 85
    iget-wide v0, p1, Lio/realm/m$a;->h:J

    iput-wide v0, p2, Lio/realm/m$a;->h:J

    .line 86
    iget-wide v0, p1, Lio/realm/m$a;->i:J

    iput-wide v0, p2, Lio/realm/m$a;->i:J

    .line 87
    iget-wide v0, p1, Lio/realm/m$a;->j:J

    iput-wide v0, p2, Lio/realm/m$a;->j:J

    .line 88
    iget-wide v0, p1, Lio/realm/m$a;->k:J

    iput-wide v0, p2, Lio/realm/m$a;->k:J

    .line 89
    return-void
.end method
