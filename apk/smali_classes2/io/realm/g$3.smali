.class Lio/realm/g$3;
.super Ljava/lang/Object;
.source "BaseRealm.java"

# interfaces
.implements Lio/realm/aw$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/g;->a(Lio/realm/ay;Lio/realm/ba;Lio/realm/g$a;Lio/realm/exceptions/RealmMigrationNeededException;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lio/realm/ay;

.field final synthetic b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic c:Lio/realm/ba;

.field final synthetic d:Lio/realm/g$a;


# direct methods
.method constructor <init>(Lio/realm/ay;Ljava/util/concurrent/atomic/AtomicBoolean;Lio/realm/ba;Lio/realm/g$a;)V
    .locals 0

    .prologue
    .line 634
    iput-object p1, p0, Lio/realm/g$3;->a:Lio/realm/ay;

    iput-object p2, p0, Lio/realm/g$3;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p3, p0, Lio/realm/g$3;->c:Lio/realm/ba;

    iput-object p4, p0, Lio/realm/g$3;->d:Lio/realm/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    .line 637
    if-eqz p1, :cond_0

    .line 638
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot migrate a Realm file that is already open: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/g$3;->a:Lio/realm/ay;

    .line 639
    invoke-virtual {v2}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 642
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lio/realm/g$3;->a:Lio/realm/ay;

    invoke-virtual {v1}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 643
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 644
    iget-object v0, p0, Lio/realm/g$3;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 670
    :cond_1
    :goto_0
    return-void

    .line 648
    :cond_2
    iget-object v0, p0, Lio/realm/g$3;->c:Lio/realm/ba;

    if-nez v0, :cond_3

    iget-object v0, p0, Lio/realm/g$3;->a:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->e()Lio/realm/ba;

    move-result-object v0

    .line 649
    :goto_1
    const/4 v1, 0x0

    .line 653
    :try_start_0
    iget-object v2, p0, Lio/realm/g$3;->a:Lio/realm/ay;

    invoke-static {v2}, Lio/realm/o;->b(Lio/realm/ay;)Lio/realm/o;

    move-result-object v1

    .line 654
    invoke-virtual {v1}, Lio/realm/o;->b()V

    .line 655
    invoke-virtual {v1}, Lio/realm/o;->i()J

    move-result-wide v2

    .line 656
    iget-object v4, p0, Lio/realm/g$3;->a:Lio/realm/ay;

    invoke-virtual {v4}, Lio/realm/ay;->d()J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lio/realm/ba;->a(Lio/realm/o;JJ)V

    .line 657
    iget-object v0, p0, Lio/realm/g$3;->a:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lio/realm/o;->a(J)V

    .line 658
    invoke-virtual {v1}, Lio/realm/o;->c()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    if-eqz v1, :cond_1

    .line 666
    invoke-virtual {v1}, Lio/realm/o;->close()V

    .line 667
    iget-object v0, p0, Lio/realm/g$3;->d:Lio/realm/g$a;

    invoke-interface {v0}, Lio/realm/g$a;->a()V

    goto :goto_0

    .line 648
    :cond_3
    iget-object v0, p0, Lio/realm/g$3;->c:Lio/realm/ba;

    goto :goto_1

    .line 659
    :catch_0
    move-exception v0

    .line 660
    if-eqz v1, :cond_4

    .line 661
    :try_start_1
    invoke-virtual {v1}, Lio/realm/o;->d()V

    .line 663
    :cond_4
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 665
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 666
    invoke-virtual {v1}, Lio/realm/o;->close()V

    .line 667
    iget-object v1, p0, Lio/realm/g$3;->d:Lio/realm/g$a;

    invoke-interface {v1}, Lio/realm/g$a;->a()V

    :cond_5
    throw v0
.end method
