.class public Lio/realm/s;
.super Lco/uk/getmondo/d/m;
.source "FeedItemRealmProxy.java"

# interfaces
.implements Lio/realm/internal/l;
.implements Lio/realm/t;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/s$a;
    }
.end annotation


# static fields
.field private static final c:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lio/realm/s$a;

.field private b:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/d/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 97
    invoke-static {}, Lio/realm/s;->z()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/s;->c:Lio/realm/internal/OsObjectSchemaInfo;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 101
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    const-string v1, "accountId"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    const-string v1, "created"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    const-string v1, "appUri"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const-string v1, "goldenTicket"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v1, "monthlySpendingReport"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v1, "transaction"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    const-string v1, "kycRejected"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    const-string v1, "sddRejectedReason"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    const-string v1, "basicItemInfo"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    const-string v1, "isDismissable"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/s;->d:Ljava/util/List;

    .line 114
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Lco/uk/getmondo/d/m;-><init>()V

    .line 117
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 118
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/m;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1162
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1163
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 1248
    :goto_0
    return-wide v4

    .line 1165
    :cond_0
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 1166
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 1167
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/m;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/s$a;

    .line 1168
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 1169
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->l()Ljava/lang/String;

    move-result-object v2

    .line 1171
    if-nez v2, :cond_b

    .line 1172
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 1176
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_c

    .line 1177
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 1181
    :goto_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 1182
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->m()Ljava/lang/String;

    move-result-object v6

    .line 1183
    if-eqz v6, :cond_1

    .line 1184
    iget-wide v2, v9, Lio/realm/s$a;->b:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_1
    move-object v2, p1

    .line 1186
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->n()Ljava/util/Date;

    move-result-object v6

    .line 1187
    if-eqz v6, :cond_2

    .line 1188
    iget-wide v2, v9, Lio/realm/s$a;->c:J

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :cond_2
    move-object v2, p1

    .line 1190
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->o()Ljava/lang/String;

    move-result-object v6

    .line 1191
    if-eqz v6, :cond_3

    .line 1192
    iget-wide v2, v9, Lio/realm/s$a;->d:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_3
    move-object v2, p1

    .line 1194
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->p()Ljava/lang/String;

    move-result-object v6

    .line 1195
    if-eqz v6, :cond_4

    .line 1196
    iget-wide v2, v9, Lio/realm/s$a;->e:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_4
    move-object v2, p1

    .line 1199
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->q()Lco/uk/getmondo/d/o;

    move-result-object v3

    .line 1200
    if-eqz v3, :cond_5

    .line 1201
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1202
    if-nez v2, :cond_11

    .line 1203
    invoke-static {p0, v3, p2}, Lio/realm/y;->a(Lio/realm/av;Lco/uk/getmondo/d/o;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1205
    :goto_3
    iget-wide v2, v9, Lio/realm/s$a;->f:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :cond_5
    move-object v2, p1

    .line 1208
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->r()Lco/uk/getmondo/d/v;

    move-result-object v3

    .line 1209
    if-eqz v3, :cond_6

    .line 1210
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1211
    if-nez v2, :cond_10

    .line 1212
    invoke-static {p0, v3, p2}, Lio/realm/ai;->a(Lio/realm/av;Lco/uk/getmondo/d/v;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1214
    :goto_4
    iget-wide v2, v9, Lio/realm/s$a;->g:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :cond_6
    move-object v2, p1

    .line 1217
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->s()Lco/uk/getmondo/d/aj;

    move-result-object v3

    .line 1218
    if-eqz v3, :cond_7

    .line 1219
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1220
    if-nez v2, :cond_f

    .line 1221
    invoke-static {p0, v3, p2}, Lio/realm/bm;->a(Lio/realm/av;Lco/uk/getmondo/d/aj;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1223
    :goto_5
    iget-wide v2, v9, Lio/realm/s$a;->h:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :cond_7
    move-object v2, p1

    .line 1226
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->t()Lco/uk/getmondo/d/r;

    move-result-object v3

    .line 1227
    if-eqz v3, :cond_8

    .line 1228
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1229
    if-nez v2, :cond_e

    .line 1230
    invoke-static {p0, v3, p2}, Lio/realm/ac;->a(Lio/realm/av;Lco/uk/getmondo/d/r;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1232
    :goto_6
    iget-wide v2, v9, Lio/realm/s$a;->i:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :cond_8
    move-object v2, p1

    .line 1234
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->u()Ljava/lang/String;

    move-result-object v6

    .line 1235
    if-eqz v6, :cond_9

    .line 1236
    iget-wide v2, v9, Lio/realm/s$a;->j:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_9
    move-object v2, p1

    .line 1239
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->v()Lco/uk/getmondo/d/f;

    move-result-object v3

    .line 1240
    if-eqz v3, :cond_a

    .line 1241
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1242
    if-nez v2, :cond_d

    .line 1243
    invoke-static {p0, v3, p2}, Lio/realm/h;->a(Lio/realm/av;Lco/uk/getmondo/d/f;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1245
    :goto_7
    iget-wide v2, v9, Lio/realm/s$a;->k:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 1247
    :cond_a
    iget-wide v2, v9, Lio/realm/s$a;->l:J

    check-cast p1, Lio/realm/t;

    invoke-interface {p1}, Lio/realm/t;->w()Z

    move-result v6

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto/16 :goto_0

    .line 1174
    :cond_b
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 1179
    :cond_c
    invoke-static {v2}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_d
    move-object v6, v2

    goto :goto_7

    :cond_e
    move-object v6, v2

    goto :goto_6

    :cond_f
    move-object v6, v2

    goto :goto_5

    :cond_10
    move-object v6, v2

    goto/16 :goto_4

    :cond_11
    move-object v6, v2

    goto/16 :goto_3
.end method

.method public static a(Lco/uk/getmondo/d/m;IILjava/util/Map;)Lco/uk/getmondo/d/m;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/m;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/d/m;"
        }
    .end annotation

    .prologue
    .line 1572
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 1573
    :cond_0
    const/4 v0, 0x0

    .line 1612
    :goto_0
    return-object v0

    .line 1575
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 1577
    if-nez v0, :cond_2

    .line 1578
    new-instance v1, Lco/uk/getmondo/d/m;

    invoke-direct {v1}, Lco/uk/getmondo/d/m;-><init>()V

    .line 1579
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 1588
    check-cast v0, Lio/realm/t;

    .line 1589
    check-cast p0, Lio/realm/t;

    .line 1590
    invoke-interface {p0}, Lio/realm/t;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->a(Ljava/lang/String;)V

    .line 1591
    invoke-interface {p0}, Lio/realm/t;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->b(Ljava/lang/String;)V

    .line 1592
    invoke-interface {p0}, Lio/realm/t;->n()Ljava/util/Date;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->a(Ljava/util/Date;)V

    .line 1593
    invoke-interface {p0}, Lio/realm/t;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->c(Ljava/lang/String;)V

    .line 1594
    invoke-interface {p0}, Lio/realm/t;->p()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->d(Ljava/lang/String;)V

    .line 1597
    invoke-interface {p0}, Lio/realm/t;->q()Lco/uk/getmondo/d/o;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/y;->a(Lco/uk/getmondo/d/o;IILjava/util/Map;)Lco/uk/getmondo/d/o;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/o;)V

    .line 1600
    invoke-interface {p0}, Lio/realm/t;->r()Lco/uk/getmondo/d/v;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/ai;->a(Lco/uk/getmondo/d/v;IILjava/util/Map;)Lco/uk/getmondo/d/v;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/v;)V

    .line 1603
    invoke-interface {p0}, Lio/realm/t;->s()Lco/uk/getmondo/d/aj;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/bm;->a(Lco/uk/getmondo/d/aj;IILjava/util/Map;)Lco/uk/getmondo/d/aj;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/aj;)V

    .line 1606
    invoke-interface {p0}, Lio/realm/t;->t()Lco/uk/getmondo/d/r;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/ac;->a(Lco/uk/getmondo/d/r;IILjava/util/Map;)Lco/uk/getmondo/d/r;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/r;)V

    .line 1607
    invoke-interface {p0}, Lio/realm/t;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->e(Ljava/lang/String;)V

    .line 1610
    invoke-interface {p0}, Lio/realm/t;->v()Lco/uk/getmondo/d/f;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/h;->a(Lco/uk/getmondo/d/f;IILjava/util/Map;)Lco/uk/getmondo/d/f;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/f;)V

    .line 1611
    invoke-interface {p0}, Lio/realm/t;->w()Z

    move-result v2

    invoke-interface {v0, v2}, Lio/realm/t;->a(Z)V

    move-object v0, v1

    .line 1612
    goto/16 :goto_0

    .line 1582
    :cond_2
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_3

    .line 1583
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/d/m;

    goto/16 :goto_0

    .line 1585
    :cond_3
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/d/m;

    .line 1586
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto/16 :goto_1
.end method

.method static a(Lio/realm/av;Lco/uk/getmondo/d/m;Lco/uk/getmondo/d/m;Ljava/util/Map;)Lco/uk/getmondo/d/m;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/m;",
            "Lco/uk/getmondo/d/m;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/m;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1616
    move-object v0, p1

    check-cast v0, Lio/realm/t;

    .line 1617
    check-cast p2, Lio/realm/t;

    .line 1618
    invoke-interface {p2}, Lio/realm/t;->m()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->b(Ljava/lang/String;)V

    .line 1619
    invoke-interface {p2}, Lio/realm/t;->n()Ljava/util/Date;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->a(Ljava/util/Date;)V

    .line 1620
    invoke-interface {p2}, Lio/realm/t;->o()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->c(Ljava/lang/String;)V

    .line 1621
    invoke-interface {p2}, Lio/realm/t;->p()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->d(Ljava/lang/String;)V

    .line 1622
    invoke-interface {p2}, Lio/realm/t;->q()Lco/uk/getmondo/d/o;

    move-result-object v2

    .line 1623
    if-nez v2, :cond_0

    .line 1624
    invoke-interface {v0, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/o;)V

    .line 1633
    :goto_0
    invoke-interface {p2}, Lio/realm/t;->r()Lco/uk/getmondo/d/v;

    move-result-object v2

    .line 1634
    if-nez v2, :cond_2

    .line 1635
    invoke-interface {v0, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/v;)V

    .line 1644
    :goto_1
    invoke-interface {p2}, Lio/realm/t;->s()Lco/uk/getmondo/d/aj;

    move-result-object v2

    .line 1645
    if-nez v2, :cond_4

    .line 1646
    invoke-interface {v0, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/aj;)V

    .line 1655
    :goto_2
    invoke-interface {p2}, Lio/realm/t;->t()Lco/uk/getmondo/d/r;

    move-result-object v2

    .line 1656
    if-nez v2, :cond_6

    .line 1657
    invoke-interface {v0, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/r;)V

    .line 1666
    :goto_3
    invoke-interface {p2}, Lio/realm/t;->u()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->e(Ljava/lang/String;)V

    .line 1667
    invoke-interface {p2}, Lio/realm/t;->v()Lco/uk/getmondo/d/f;

    move-result-object v2

    .line 1668
    if-nez v2, :cond_8

    .line 1669
    invoke-interface {v0, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/f;)V

    .line 1678
    :goto_4
    invoke-interface {p2}, Lio/realm/t;->w()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/t;->a(Z)V

    .line 1679
    return-object p1

    .line 1626
    :cond_0
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/o;

    .line 1627
    if-eqz v1, :cond_1

    .line 1628
    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/o;)V

    goto :goto_0

    .line 1630
    :cond_1
    invoke-static {p0, v2, v3, p3}, Lio/realm/y;->a(Lio/realm/av;Lco/uk/getmondo/d/o;ZLjava/util/Map;)Lco/uk/getmondo/d/o;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/o;)V

    goto :goto_0

    .line 1637
    :cond_2
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/v;

    .line 1638
    if-eqz v1, :cond_3

    .line 1639
    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/v;)V

    goto :goto_1

    .line 1641
    :cond_3
    invoke-static {p0, v2, v3, p3}, Lio/realm/ai;->a(Lio/realm/av;Lco/uk/getmondo/d/v;ZLjava/util/Map;)Lco/uk/getmondo/d/v;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/v;)V

    goto :goto_1

    .line 1648
    :cond_4
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/aj;

    .line 1649
    if-eqz v1, :cond_5

    .line 1650
    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/aj;)V

    goto :goto_2

    .line 1652
    :cond_5
    invoke-static {p0, v2, v3, p3}, Lio/realm/bm;->a(Lio/realm/av;Lco/uk/getmondo/d/aj;ZLjava/util/Map;)Lco/uk/getmondo/d/aj;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/aj;)V

    goto :goto_2

    .line 1659
    :cond_6
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/r;

    .line 1660
    if-eqz v1, :cond_7

    .line 1661
    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/r;)V

    goto :goto_3

    .line 1663
    :cond_7
    invoke-static {p0, v2, v3, p3}, Lio/realm/ac;->a(Lio/realm/av;Lco/uk/getmondo/d/r;ZLjava/util/Map;)Lco/uk/getmondo/d/r;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/r;)V

    goto :goto_3

    .line 1671
    :cond_8
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/f;

    .line 1672
    if-eqz v1, :cond_9

    .line 1673
    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/f;)V

    goto :goto_4

    .line 1675
    :cond_9
    invoke-static {p0, v2, v3, p3}, Lio/realm/h;->a(Lio/realm/av;Lco/uk/getmondo/d/f;ZLjava/util/Map;)Lco/uk/getmondo/d/f;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/t;->a(Lco/uk/getmondo/d/f;)V

    goto :goto_4
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/m;ZLjava/util/Map;)Lco/uk/getmondo/d/m;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/m;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/m;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1035
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-wide v2, v2, Lio/realm/g;->c:J

    iget-wide v6, p0, Lio/realm/av;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 1036
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1038
    :cond_0
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1075
    :goto_0
    return-object p1

    .line 1041
    :cond_1
    sget-object v2, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v2}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/g$b;

    .line 1042
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/l;

    .line 1043
    if-eqz v3, :cond_2

    .line 1044
    check-cast v3, Lco/uk/getmondo/d/m;

    move-object p1, v3

    goto :goto_0

    .line 1047
    :cond_2
    const/4 v5, 0x0

    .line 1049
    if-eqz p2, :cond_6

    .line 1050
    const-class v3, Lco/uk/getmondo/d/m;

    invoke-virtual {p0, v3}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 1051
    invoke-virtual {v8}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v3, p1

    .line 1052
    check-cast v3, Lio/realm/t;

    invoke-interface {v3}, Lio/realm/t;->l()Ljava/lang/String;

    move-result-object v3

    .line 1054
    if-nez v3, :cond_3

    .line 1055
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v6

    .line 1059
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-eqz v3, :cond_4

    .line 1061
    :try_start_0
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    iget-object v3, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/m;

    invoke-virtual {v3, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 1062
    new-instance v4, Lio/realm/s;

    invoke-direct {v4}, Lio/realm/s;-><init>()V

    .line 1063
    move-object v0, v4

    check-cast v0, Lio/realm/internal/l;

    move-object v3, v0

    invoke-interface {p3, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1065
    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    move v2, p2

    .line 1072
    :goto_2
    if-eqz v2, :cond_5

    .line 1073
    invoke-static {p0, v4, p1, p3}, Lio/realm/s;->a(Lio/realm/av;Lco/uk/getmondo/d/m;Lco/uk/getmondo/d/m;Ljava/util/Map;)Lco/uk/getmondo/d/m;

    move-result-object p1

    goto :goto_0

    .line 1057
    :cond_3
    invoke-virtual {v8, v6, v7, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 1065
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    throw v3

    :cond_4
    move v2, v4

    move-object v4, v5

    .line 1068
    goto :goto_2

    .line 1075
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/s;->b(Lio/realm/av;Lco/uk/getmondo/d/m;ZLjava/util/Map;)Lco/uk/getmondo/d/m;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v2, p2

    move-object v4, v5

    goto :goto_2
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/s$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0xc

    .line 604
    const-string v0, "class_FeedItem"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 605
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'FeedItem\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 607
    :cond_0
    const-string v0, "class_FeedItem"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 608
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 609
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 610
    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 611
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 12 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 613
    :cond_1
    if-eqz p1, :cond_3

    .line 614
    const-string v0, "Field count is more than expected - expected 12 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 620
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 621
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 616
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 12 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 624
    :cond_4
    new-instance v0, Lio/realm/s$a;

    invoke-direct {v0, p0, v2}, Lio/realm/s$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 626
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 627
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 629
    :cond_5
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lio/realm/s$a;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 630
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 634
    :cond_6
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 635
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 637
    :cond_7
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_8

    .line 638
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'id\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 640
    :cond_8
    iget-wide v4, v0, Lio/realm/s$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_9

    .line 641
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@PrimaryKey field \'id\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 643
    :cond_9
    const-string v1, "id"

    invoke-virtual {v2, v1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->j(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 644
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 646
    :cond_a
    const-string v1, "accountId"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 647
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'accountId\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 649
    :cond_b
    const-string v1, "accountId"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_c

    .line 650
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'accountId\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 652
    :cond_c
    iget-wide v4, v0, Lio/realm/s$a;->b:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_d

    .line 653
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'accountId\' is required. Either set @Required to field \'accountId\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 655
    :cond_d
    const-string v1, "created"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 656
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'created\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 658
    :cond_e
    const-string v1, "created"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_f

    .line 659
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'Date\' for field \'created\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 661
    :cond_f
    iget-wide v4, v0, Lio/realm/s$a;->c:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_10

    .line 662
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'created\' is required. Either set @Required to field \'created\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 664
    :cond_10
    const-string v1, "type"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 665
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'type\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 667
    :cond_11
    const-string v1, "type"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_12

    .line 668
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'type\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 670
    :cond_12
    iget-wide v4, v0, Lio/realm/s$a;->d:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_13

    .line 671
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'type\' is required. Either set @Required to field \'type\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 673
    :cond_13
    const-string v1, "appUri"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 674
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'appUri\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 676
    :cond_14
    const-string v1, "appUri"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_15

    .line 677
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'appUri\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 679
    :cond_15
    iget-wide v4, v0, Lio/realm/s$a;->e:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_16

    .line 680
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'appUri\' is required. Either set @Required to field \'appUri\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 682
    :cond_16
    const-string v1, "goldenTicket"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 683
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'goldenTicket\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 685
    :cond_17
    const-string v1, "goldenTicket"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_18

    .line 686
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'GoldenTicket\' for field \'goldenTicket\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 688
    :cond_18
    const-string v1, "class_GoldenTicket"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 689
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_GoldenTicket\' for field \'goldenTicket\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 691
    :cond_19
    const-string v1, "class_GoldenTicket"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 692
    iget-wide v4, v0, Lio/realm/s$a;->f:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_1a

    .line 693
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'goldenTicket\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/s$a;->f:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 695
    :cond_1a
    const-string v1, "monthlySpendingReport"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 696
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'monthlySpendingReport\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 698
    :cond_1b
    const-string v1, "monthlySpendingReport"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_1c

    .line 699
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'MonthlySpendingReport\' for field \'monthlySpendingReport\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 701
    :cond_1c
    const-string v1, "class_MonthlySpendingReport"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 702
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_MonthlySpendingReport\' for field \'monthlySpendingReport\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 704
    :cond_1d
    const-string v1, "class_MonthlySpendingReport"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 705
    iget-wide v4, v0, Lio/realm/s$a;->g:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 706
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'monthlySpendingReport\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/s$a;->g:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 708
    :cond_1e
    const-string v1, "transaction"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 709
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'transaction\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 711
    :cond_1f
    const-string v1, "transaction"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_20

    .line 712
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'Transaction\' for field \'transaction\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 714
    :cond_20
    const-string v1, "class_Transaction"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 715
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_Transaction\' for field \'transaction\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 717
    :cond_21
    const-string v1, "class_Transaction"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 718
    iget-wide v4, v0, Lio/realm/s$a;->h:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_22

    .line 719
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'transaction\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/s$a;->h:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 721
    :cond_22
    const-string v1, "kycRejected"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    .line 722
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'kycRejected\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 724
    :cond_23
    const-string v1, "kycRejected"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_24

    .line 725
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'KycRejected\' for field \'kycRejected\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 727
    :cond_24
    const-string v1, "class_KycRejected"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_25

    .line 728
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_KycRejected\' for field \'kycRejected\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 730
    :cond_25
    const-string v1, "class_KycRejected"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 731
    iget-wide v4, v0, Lio/realm/s$a;->i:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 732
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'kycRejected\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/s$a;->i:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 734
    :cond_26
    const-string v1, "sddRejectedReason"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_27

    .line 735
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'sddRejectedReason\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 737
    :cond_27
    const-string v1, "sddRejectedReason"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_28

    .line 738
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'sddRejectedReason\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 740
    :cond_28
    iget-wide v4, v0, Lio/realm/s$a;->j:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_29

    .line 741
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'sddRejectedReason\' is required. Either set @Required to field \'sddRejectedReason\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 743
    :cond_29
    const-string v1, "basicItemInfo"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2a

    .line 744
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'basicItemInfo\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 746
    :cond_2a
    const-string v1, "basicItemInfo"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_2b

    .line 747
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'BasicItemInfo\' for field \'basicItemInfo\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 749
    :cond_2b
    const-string v1, "class_BasicItemInfo"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2c

    .line 750
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_BasicItemInfo\' for field \'basicItemInfo\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 752
    :cond_2c
    const-string v1, "class_BasicItemInfo"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 753
    iget-wide v4, v0, Lio/realm/s$a;->k:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_2d

    .line 754
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'basicItemInfo\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/s$a;->k:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 756
    :cond_2d
    const-string v1, "isDismissable"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2e

    .line 757
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'isDismissable\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 759
    :cond_2e
    const-string v1, "isDismissable"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_2f

    .line 760
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'isDismissable\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 762
    :cond_2f
    iget-wide v4, v0, Lio/realm/s$a;->l:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 763
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'isDismissable\' does support null values in the existing Realm file. Use corresponding boxed type for field \'isDismissable\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 766
    :cond_30
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1457
    const-class v2, Lco/uk/getmondo/d/m;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v13

    .line 1458
    invoke-virtual {v13}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    .line 1459
    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/m;

    invoke-virtual {v4, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lio/realm/s$a;

    .line 1460
    invoke-virtual {v13}, Lio/realm/internal/Table;->d()J

    move-result-wide v14

    .line 1462
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1463
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Lco/uk/getmondo/d/m;

    .line 1464
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1467
    instance-of v4, v12, Lio/realm/internal/l;

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v12

    .line 1468
    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v4, v12

    .line 1471
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->l()Ljava/lang/String;

    move-result-object v4

    .line 1473
    if-nez v4, :cond_3

    .line 1474
    invoke-static {v2, v3, v14, v15}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v6

    .line 1478
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 1479
    invoke-static {v13, v4}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v6

    .line 1481
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v12

    .line 1482
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->m()Ljava/lang/String;

    move-result-object v8

    .line 1483
    if-eqz v8, :cond_4

    .line 1484
    iget-wide v4, v11, Lio/realm/s$a;->b:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_2
    move-object v4, v12

    .line 1488
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->n()Ljava/util/Date;

    move-result-object v8

    .line 1489
    if-eqz v8, :cond_5

    .line 1490
    iget-wide v4, v11, Lio/realm/s$a;->c:J

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :goto_3
    move-object v4, v12

    .line 1494
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->o()Ljava/lang/String;

    move-result-object v8

    .line 1495
    if-eqz v8, :cond_6

    .line 1496
    iget-wide v4, v11, Lio/realm/s$a;->d:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_4
    move-object v4, v12

    .line 1500
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->p()Ljava/lang/String;

    move-result-object v8

    .line 1501
    if-eqz v8, :cond_7

    .line 1502
    iget-wide v4, v11, Lio/realm/s$a;->e:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_5
    move-object v4, v12

    .line 1507
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->q()Lco/uk/getmondo/d/o;

    move-result-object v5

    .line 1508
    if-eqz v5, :cond_8

    .line 1509
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1510
    if-nez v4, :cond_13

    .line 1511
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/y;->b(Lio/realm/av;Lco/uk/getmondo/d/o;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 1513
    :goto_6
    iget-wide v4, v11, Lio/realm/s$a;->f:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_7
    move-object v4, v12

    .line 1518
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->r()Lco/uk/getmondo/d/v;

    move-result-object v5

    .line 1519
    if-eqz v5, :cond_9

    .line 1520
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1521
    if-nez v4, :cond_12

    .line 1522
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/ai;->b(Lio/realm/av;Lco/uk/getmondo/d/v;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 1524
    :goto_8
    iget-wide v4, v11, Lio/realm/s$a;->g:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_9
    move-object v4, v12

    .line 1529
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->s()Lco/uk/getmondo/d/aj;

    move-result-object v5

    .line 1530
    if-eqz v5, :cond_a

    .line 1531
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1532
    if-nez v4, :cond_11

    .line 1533
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/bm;->b(Lio/realm/av;Lco/uk/getmondo/d/aj;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 1535
    :goto_a
    iget-wide v4, v11, Lio/realm/s$a;->h:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_b
    move-object v4, v12

    .line 1540
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->t()Lco/uk/getmondo/d/r;

    move-result-object v5

    .line 1541
    if-eqz v5, :cond_b

    .line 1542
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1543
    if-nez v4, :cond_10

    .line 1544
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/ac;->b(Lio/realm/av;Lco/uk/getmondo/d/r;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 1546
    :goto_c
    iget-wide v4, v11, Lio/realm/s$a;->i:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_d
    move-object v4, v12

    .line 1550
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->u()Ljava/lang/String;

    move-result-object v8

    .line 1551
    if-eqz v8, :cond_c

    .line 1552
    iget-wide v4, v11, Lio/realm/s$a;->j:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_e
    move-object v4, v12

    .line 1557
    check-cast v4, Lio/realm/t;

    invoke-interface {v4}, Lio/realm/t;->v()Lco/uk/getmondo/d/f;

    move-result-object v5

    .line 1558
    if-eqz v5, :cond_d

    .line 1559
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1560
    if-nez v4, :cond_f

    .line 1561
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/h;->b(Lio/realm/av;Lco/uk/getmondo/d/f;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 1563
    :goto_f
    iget-wide v4, v11, Lio/realm/s$a;->k:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 1567
    :goto_10
    iget-wide v4, v11, Lio/realm/s$a;->l:J

    check-cast v12, Lio/realm/t;

    invoke-interface {v12}, Lio/realm/t;->w()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto/16 :goto_0

    .line 1476
    :cond_3
    invoke-static {v2, v3, v14, v15, v4}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v6

    goto/16 :goto_1

    .line 1486
    :cond_4
    iget-wide v4, v11, Lio/realm/s$a;->b:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_2

    .line 1492
    :cond_5
    iget-wide v4, v11, Lio/realm/s$a;->c:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_3

    .line 1498
    :cond_6
    iget-wide v4, v11, Lio/realm/s$a;->d:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_4

    .line 1504
    :cond_7
    iget-wide v4, v11, Lio/realm/s$a;->e:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_5

    .line 1515
    :cond_8
    iget-wide v4, v11, Lio/realm/s$a;->f:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_7

    .line 1526
    :cond_9
    iget-wide v4, v11, Lio/realm/s$a;->g:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_9

    .line 1537
    :cond_a
    iget-wide v4, v11, Lio/realm/s$a;->h:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_b

    .line 1548
    :cond_b
    iget-wide v4, v11, Lio/realm/s$a;->i:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_d

    .line 1554
    :cond_c
    iget-wide v4, v11, Lio/realm/s$a;->j:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_e

    .line 1565
    :cond_d
    iget-wide v4, v11, Lio/realm/s$a;->k:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_10

    .line 1569
    :cond_e
    return-void

    :cond_f
    move-object v8, v4

    goto :goto_f

    :cond_10
    move-object v8, v4

    goto/16 :goto_c

    :cond_11
    move-object v8, v4

    goto/16 :goto_a

    :cond_12
    move-object v8, v4

    goto/16 :goto_8

    :cond_13
    move-object v8, v4

    goto/16 :goto_6
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/m;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/m;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1349
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1350
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 1453
    :goto_0
    return-wide v4

    .line 1352
    :cond_0
    const-class v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 1353
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 1354
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/m;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/s$a;

    .line 1355
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 1356
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->l()Ljava/lang/String;

    move-result-object v2

    .line 1358
    if-nez v2, :cond_2

    .line 1359
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 1363
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    .line 1364
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 1366
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 1367
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->m()Ljava/lang/String;

    move-result-object v6

    .line 1368
    if-eqz v6, :cond_3

    .line 1369
    iget-wide v2, v9, Lio/realm/s$a;->b:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_2
    move-object v2, p1

    .line 1373
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->n()Ljava/util/Date;

    move-result-object v6

    .line 1374
    if-eqz v6, :cond_4

    .line 1375
    iget-wide v2, v9, Lio/realm/s$a;->c:J

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :goto_3
    move-object v2, p1

    .line 1379
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->o()Ljava/lang/String;

    move-result-object v6

    .line 1380
    if-eqz v6, :cond_5

    .line 1381
    iget-wide v2, v9, Lio/realm/s$a;->d:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_4
    move-object v2, p1

    .line 1385
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->p()Ljava/lang/String;

    move-result-object v6

    .line 1386
    if-eqz v6, :cond_6

    .line 1387
    iget-wide v2, v9, Lio/realm/s$a;->e:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_5
    move-object v2, p1

    .line 1392
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->q()Lco/uk/getmondo/d/o;

    move-result-object v3

    .line 1393
    if-eqz v3, :cond_7

    .line 1394
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1395
    if-nez v2, :cond_11

    .line 1396
    invoke-static {p0, v3, p2}, Lio/realm/y;->b(Lio/realm/av;Lco/uk/getmondo/d/o;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1398
    :goto_6
    iget-wide v2, v9, Lio/realm/s$a;->f:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_7
    move-object v2, p1

    .line 1403
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->r()Lco/uk/getmondo/d/v;

    move-result-object v3

    .line 1404
    if-eqz v3, :cond_8

    .line 1405
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1406
    if-nez v2, :cond_10

    .line 1407
    invoke-static {p0, v3, p2}, Lio/realm/ai;->b(Lio/realm/av;Lco/uk/getmondo/d/v;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1409
    :goto_8
    iget-wide v2, v9, Lio/realm/s$a;->g:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_9
    move-object v2, p1

    .line 1414
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->s()Lco/uk/getmondo/d/aj;

    move-result-object v3

    .line 1415
    if-eqz v3, :cond_9

    .line 1416
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1417
    if-nez v2, :cond_f

    .line 1418
    invoke-static {p0, v3, p2}, Lio/realm/bm;->b(Lio/realm/av;Lco/uk/getmondo/d/aj;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1420
    :goto_a
    iget-wide v2, v9, Lio/realm/s$a;->h:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_b
    move-object v2, p1

    .line 1425
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->t()Lco/uk/getmondo/d/r;

    move-result-object v3

    .line 1426
    if-eqz v3, :cond_a

    .line 1427
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1428
    if-nez v2, :cond_e

    .line 1429
    invoke-static {p0, v3, p2}, Lio/realm/ac;->b(Lio/realm/av;Lco/uk/getmondo/d/r;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1431
    :goto_c
    iget-wide v2, v9, Lio/realm/s$a;->i:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_d
    move-object v2, p1

    .line 1435
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->u()Ljava/lang/String;

    move-result-object v6

    .line 1436
    if-eqz v6, :cond_b

    .line 1437
    iget-wide v2, v9, Lio/realm/s$a;->j:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_e
    move-object v2, p1

    .line 1442
    check-cast v2, Lio/realm/t;

    invoke-interface {v2}, Lio/realm/t;->v()Lco/uk/getmondo/d/f;

    move-result-object v3

    .line 1443
    if-eqz v3, :cond_c

    .line 1444
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1445
    if-nez v2, :cond_d

    .line 1446
    invoke-static {p0, v3, p2}, Lio/realm/h;->b(Lio/realm/av;Lco/uk/getmondo/d/f;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1448
    :goto_f
    iget-wide v2, v9, Lio/realm/s$a;->k:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 1452
    :goto_10
    iget-wide v2, v9, Lio/realm/s$a;->l:J

    check-cast p1, Lio/realm/t;

    invoke-interface {p1}, Lio/realm/t;->w()Z

    move-result v6

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto/16 :goto_0

    .line 1361
    :cond_2
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 1371
    :cond_3
    iget-wide v2, v9, Lio/realm/s$a;->b:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_2

    .line 1377
    :cond_4
    iget-wide v2, v9, Lio/realm/s$a;->c:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_3

    .line 1383
    :cond_5
    iget-wide v2, v9, Lio/realm/s$a;->d:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_4

    .line 1389
    :cond_6
    iget-wide v2, v9, Lio/realm/s$a;->e:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_5

    .line 1400
    :cond_7
    iget-wide v2, v9, Lio/realm/s$a;->f:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_7

    .line 1411
    :cond_8
    iget-wide v2, v9, Lio/realm/s$a;->g:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_9

    .line 1422
    :cond_9
    iget-wide v2, v9, Lio/realm/s$a;->h:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_b

    .line 1433
    :cond_a
    iget-wide v2, v9, Lio/realm/s$a;->i:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_d

    .line 1439
    :cond_b
    iget-wide v2, v9, Lio/realm/s$a;->j:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_e

    .line 1450
    :cond_c
    iget-wide v2, v9, Lio/realm/s$a;->k:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_10

    :cond_d
    move-object v6, v2

    goto :goto_f

    :cond_e
    move-object v6, v2

    goto/16 :goto_c

    :cond_f
    move-object v6, v2

    goto/16 :goto_a

    :cond_10
    move-object v6, v2

    goto/16 :goto_8

    :cond_11
    move-object v6, v2

    goto/16 :goto_6
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/m;ZLjava/util/Map;)Lco/uk/getmondo/d/m;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/m;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/m;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1080
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 1081
    if-eqz v0, :cond_0

    .line 1082
    check-cast v0, Lco/uk/getmondo/d/m;

    .line 1158
    :goto_0
    return-object v0

    .line 1086
    :cond_0
    const-class v1, Lco/uk/getmondo/d/m;

    move-object v0, p1

    check-cast v0, Lio/realm/t;

    invoke-interface {v0}, Lio/realm/t;->l()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lio/realm/av;->a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/m;

    move-object v1, v0

    .line 1087
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1089
    check-cast p1, Lio/realm/t;

    move-object v1, v0

    .line 1090
    check-cast v1, Lio/realm/t;

    .line 1092
    invoke-interface {p1}, Lio/realm/t;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->b(Ljava/lang/String;)V

    .line 1093
    invoke-interface {p1}, Lio/realm/t;->n()Ljava/util/Date;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->a(Ljava/util/Date;)V

    .line 1094
    invoke-interface {p1}, Lio/realm/t;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->c(Ljava/lang/String;)V

    .line 1095
    invoke-interface {p1}, Lio/realm/t;->p()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->d(Ljava/lang/String;)V

    .line 1097
    invoke-interface {p1}, Lio/realm/t;->q()Lco/uk/getmondo/d/o;

    move-result-object v3

    .line 1098
    if-nez v3, :cond_1

    .line 1099
    invoke-interface {v1, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/o;)V

    .line 1109
    :goto_1
    invoke-interface {p1}, Lio/realm/t;->r()Lco/uk/getmondo/d/v;

    move-result-object v3

    .line 1110
    if-nez v3, :cond_3

    .line 1111
    invoke-interface {v1, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/v;)V

    .line 1121
    :goto_2
    invoke-interface {p1}, Lio/realm/t;->s()Lco/uk/getmondo/d/aj;

    move-result-object v3

    .line 1122
    if-nez v3, :cond_5

    .line 1123
    invoke-interface {v1, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/aj;)V

    .line 1133
    :goto_3
    invoke-interface {p1}, Lio/realm/t;->t()Lco/uk/getmondo/d/r;

    move-result-object v3

    .line 1134
    if-nez v3, :cond_7

    .line 1135
    invoke-interface {v1, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/r;)V

    .line 1144
    :goto_4
    invoke-interface {p1}, Lio/realm/t;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->e(Ljava/lang/String;)V

    .line 1146
    invoke-interface {p1}, Lio/realm/t;->v()Lco/uk/getmondo/d/f;

    move-result-object v3

    .line 1147
    if-nez v3, :cond_9

    .line 1148
    invoke-interface {v1, v4}, Lio/realm/t;->a(Lco/uk/getmondo/d/f;)V

    .line 1157
    :goto_5
    invoke-interface {p1}, Lio/realm/t;->w()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/t;->a(Z)V

    goto :goto_0

    .line 1101
    :cond_1
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/o;

    .line 1102
    if-eqz v2, :cond_2

    .line 1103
    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/o;)V

    goto :goto_1

    .line 1105
    :cond_2
    invoke-static {p0, v3, p2, p3}, Lio/realm/y;->a(Lio/realm/av;Lco/uk/getmondo/d/o;ZLjava/util/Map;)Lco/uk/getmondo/d/o;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/o;)V

    goto :goto_1

    .line 1113
    :cond_3
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/v;

    .line 1114
    if-eqz v2, :cond_4

    .line 1115
    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/v;)V

    goto :goto_2

    .line 1117
    :cond_4
    invoke-static {p0, v3, p2, p3}, Lio/realm/ai;->a(Lio/realm/av;Lco/uk/getmondo/d/v;ZLjava/util/Map;)Lco/uk/getmondo/d/v;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/v;)V

    goto :goto_2

    .line 1125
    :cond_5
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/aj;

    .line 1126
    if-eqz v2, :cond_6

    .line 1127
    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/aj;)V

    goto :goto_3

    .line 1129
    :cond_6
    invoke-static {p0, v3, p2, p3}, Lio/realm/bm;->a(Lio/realm/av;Lco/uk/getmondo/d/aj;ZLjava/util/Map;)Lco/uk/getmondo/d/aj;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/aj;)V

    goto :goto_3

    .line 1137
    :cond_7
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/r;

    .line 1138
    if-eqz v2, :cond_8

    .line 1139
    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/r;)V

    goto :goto_4

    .line 1141
    :cond_8
    invoke-static {p0, v3, p2, p3}, Lio/realm/ac;->a(Lio/realm/av;Lco/uk/getmondo/d/r;ZLjava/util/Map;)Lco/uk/getmondo/d/r;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/r;)V

    goto :goto_4

    .line 1150
    :cond_9
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/f;

    .line 1151
    if-eqz v2, :cond_a

    .line 1152
    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/f;)V

    goto :goto_5

    .line 1154
    :cond_a
    invoke-static {p0, v3, p2, p3}, Lio/realm/h;->a(Lio/realm/av;Lco/uk/getmondo/d/f;ZLjava/util/Map;)Lco/uk/getmondo/d/f;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/t;->a(Lco/uk/getmondo/d/f;)V

    goto :goto_5
.end method

.method public static x()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 600
    sget-object v0, Lio/realm/s;->c:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 770
    const-string v0, "class_FeedItem"

    return-object v0
.end method

.method private static z()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 583
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "FeedItem"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 584
    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 585
    const-string v7, "accountId"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 586
    const-string v7, "created"

    sget-object v8, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 587
    const-string v7, "type"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 588
    const-string v7, "appUri"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 589
    const-string v1, "goldenTicket"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "GoldenTicket"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 590
    const-string v1, "monthlySpendingReport"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "MonthlySpendingReport"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 591
    const-string v1, "transaction"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "Transaction"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 592
    const-string v1, "kycRejected"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "KycRejected"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 593
    const-string v7, "sddRejectedReason"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 594
    const-string v1, "basicItemInfo"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "BasicItemInfo"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 595
    const-string v7, "isDismissable"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 596
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/aj;)V
    .locals 9

    .prologue
    .line 388
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 389
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 392
    :cond_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "transaction"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 396
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    move-object v6, v0

    .line 398
    :goto_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 399
    if-nez v6, :cond_2

    .line 401
    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v0, v0, Lio/realm/s$a;->h:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 404
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 405
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 407
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 408
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->h:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 414
    :cond_5
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 415
    if-nez p1, :cond_6

    .line 416
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 419
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 420
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 422
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 423
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_9
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->h:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Lco/uk/getmondo/d/f;)V
    .locals 9

    .prologue
    .line 520
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 521
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 558
    :cond_0
    :goto_0
    return-void

    .line 524
    :cond_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "basicItemInfo"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 527
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 528
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/f;

    move-object v6, v0

    .line 530
    :goto_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 531
    if-nez v6, :cond_2

    .line 533
    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v0, v0, Lio/realm/s$a;->k:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 536
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 539
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 540
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 542
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->k:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 546
    :cond_5
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 547
    if-nez p1, :cond_6

    .line 548
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->k:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 551
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 552
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 554
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 555
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 557
    :cond_9
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->k:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Lco/uk/getmondo/d/o;)V
    .locals 9

    .prologue
    .line 286
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 287
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "goldenTicket"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 294
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/o;

    move-object v6, v0

    .line 296
    :goto_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 297
    if-nez v6, :cond_2

    .line 299
    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v0, v0, Lio/realm/s$a;->f:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 302
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 303
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 305
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->f:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 312
    :cond_5
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 313
    if-nez p1, :cond_6

    .line 314
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 317
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 318
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 320
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 321
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_9
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->f:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Lco/uk/getmondo/d/r;)V
    .locals 9

    .prologue
    .line 439
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 440
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 443
    :cond_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "kycRejected"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 446
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 447
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/r;

    move-object v6, v0

    .line 449
    :goto_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 450
    if-nez v6, :cond_2

    .line 452
    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v0, v0, Lio/realm/s$a;->i:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 455
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 458
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 459
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->i:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 465
    :cond_5
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 466
    if-nez p1, :cond_6

    .line 467
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->i:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 470
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 471
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 473
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 474
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 476
    :cond_9
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->i:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Lco/uk/getmondo/d/v;)V
    .locals 9

    .prologue
    .line 337
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 338
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 341
    :cond_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "monthlySpendingReport"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 345
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/v;

    move-object v6, v0

    .line 347
    :goto_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 348
    if-nez v6, :cond_2

    .line 350
    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v0, v0, Lio/realm/s$a;->g:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 353
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 354
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 356
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->g:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 363
    :cond_5
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 364
    if-nez p1, :cond_6

    .line 365
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->g:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 368
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 369
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 371
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374
    :cond_9
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->g:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 149
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/util/Date;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 194
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 195
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 199
    if-nez p1, :cond_1

    .line 200
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 203
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v1, v1, Lio/realm/s$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/util/Date;Z)V

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 208
    if-nez p1, :cond_3

    .line 209
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 212
    :cond_3
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->c:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/util/Date;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 8

    .prologue
    .line 569
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 580
    :goto_0
    return-void

    .line 573
    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 574
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v2, Lio/realm/s$a;->l:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 578
    :cond_1
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 579
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->l:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 161
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 166
    if-nez p1, :cond_1

    .line 167
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->b:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 170
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v1, v1, Lio/realm/s$a;->b:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 174
    :cond_2
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 175
    if-nez p1, :cond_3

    .line 176
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 179
    :cond_3
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->b:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 224
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 229
    if-nez p1, :cond_1

    .line 230
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->d:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 233
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v1, v1, Lio/realm/s$a;->d:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 237
    :cond_2
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 238
    if-nez p1, :cond_3

    .line 239
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 242
    :cond_3
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->d:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 254
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 255
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 259
    if-nez p1, :cond_1

    .line 260
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->e:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 263
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v1, v1, Lio/realm/s$a;->e:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 267
    :cond_2
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 268
    if-nez p1, :cond_3

    .line 269
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 272
    :cond_3
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->e:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 488
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 489
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 507
    :goto_0
    return-void

    .line 492
    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 493
    if-nez p1, :cond_1

    .line 494
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v0, Lio/realm/s$a;->j:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 497
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v1, v1, Lio/realm/s$a;->j:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 501
    :cond_2
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 502
    if-nez p1, :cond_3

    .line 503
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->j:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 506
    :cond_3
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->j:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 138
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 4

    .prologue
    .line 155
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 156
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/util/Date;
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 186
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->b(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const/4 v0, 0x0

    .line 189
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->j(J)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public o()Ljava/lang/String;
    .locals 4

    .prologue
    .line 218
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 219
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 249
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public q()Lco/uk/getmondo/d/o;
    .locals 6

    .prologue
    .line 277
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 278
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    const/4 v0, 0x0

    .line 281
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/o;

    iget-object v2, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v4, v3, Lio/realm/s$a;->f:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/o;

    goto :goto_0
.end method

.method public r()Lco/uk/getmondo/d/v;
    .locals 6

    .prologue
    .line 328
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 329
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->g:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    const/4 v0, 0x0

    .line 332
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/v;

    iget-object v2, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v4, v3, Lio/realm/s$a;->g:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/v;

    goto :goto_0
.end method

.method public s()Lco/uk/getmondo/d/aj;
    .locals 6

    .prologue
    .line 379
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 380
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    const/4 v0, 0x0

    .line 383
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/aj;

    iget-object v2, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v4, v3, Lio/realm/s$a;->h:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    goto :goto_0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1742
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    return-object v0
.end method

.method public t()Lco/uk/getmondo/d/r;
    .locals 6

    .prologue
    .line 430
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 431
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->i:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    const/4 v0, 0x0

    .line 434
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/r;

    iget-object v2, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v4, v3, Lio/realm/s$a;->i:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/r;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1685
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1686
    const-string v0, "Invalid object"

    .line 1737
    :goto_0
    return-object v0

    .line 1688
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "FeedItem = proxy["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1689
    const-string v0, "{id:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1690
    invoke-virtual {p0}, Lio/realm/s;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lio/realm/s;->l()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1691
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1692
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1693
    const-string v0, "{accountId:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1694
    invoke-virtual {p0}, Lio/realm/s;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lio/realm/s;->m()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1695
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1696
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1697
    const-string v0, "{created:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1698
    invoke-virtual {p0}, Lio/realm/s;->n()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lio/realm/s;->n()Ljava/util/Date;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1699
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1700
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1701
    const-string v0, "{type:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1702
    invoke-virtual {p0}, Lio/realm/s;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lio/realm/s;->o()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1703
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1704
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1705
    const-string v0, "{appUri:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1706
    invoke-virtual {p0}, Lio/realm/s;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lio/realm/s;->p()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1707
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1708
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1709
    const-string v0, "{goldenTicket:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1710
    invoke-virtual {p0}, Lio/realm/s;->q()Lco/uk/getmondo/d/o;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v0, "GoldenTicket"

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1711
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1712
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1713
    const-string v0, "{monthlySpendingReport:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1714
    invoke-virtual {p0}, Lio/realm/s;->r()Lco/uk/getmondo/d/v;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v0, "MonthlySpendingReport"

    :goto_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1715
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1716
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1717
    const-string v0, "{transaction:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1718
    invoke-virtual {p0}, Lio/realm/s;->s()Lco/uk/getmondo/d/aj;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v0, "Transaction"

    :goto_8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1719
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1720
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1721
    const-string v0, "{kycRejected:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1722
    invoke-virtual {p0}, Lio/realm/s;->t()Lco/uk/getmondo/d/r;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string v0, "KycRejected"

    :goto_9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1723
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1724
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1725
    const-string v0, "{sddRejectedReason:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1726
    invoke-virtual {p0}, Lio/realm/s;->u()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lio/realm/s;->u()Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1727
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1728
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1729
    const-string v0, "{basicItemInfo:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1730
    invoke-virtual {p0}, Lio/realm/s;->v()Lco/uk/getmondo/d/f;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v0, "BasicItemInfo"

    :goto_b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1731
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1732
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1733
    const-string v0, "{isDismissable:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1734
    invoke-virtual {p0}, Lio/realm/s;->w()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1735
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1736
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1737
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1690
    :cond_1
    const-string v0, "null"

    goto/16 :goto_1

    .line 1694
    :cond_2
    const-string v0, "null"

    goto/16 :goto_2

    .line 1698
    :cond_3
    const-string v0, "null"

    goto/16 :goto_3

    .line 1702
    :cond_4
    const-string v0, "null"

    goto/16 :goto_4

    .line 1706
    :cond_5
    const-string v0, "null"

    goto/16 :goto_5

    .line 1710
    :cond_6
    const-string v0, "null"

    goto/16 :goto_6

    .line 1714
    :cond_7
    const-string v0, "null"

    goto/16 :goto_7

    .line 1718
    :cond_8
    const-string v0, "null"

    goto/16 :goto_8

    .line 1722
    :cond_9
    const-string v0, "null"

    goto/16 :goto_9

    .line 1726
    :cond_a
    const-string v0, "null"

    goto :goto_a

    .line 1730
    :cond_b
    const-string v0, "null"

    goto :goto_b
.end method

.method public u()Ljava/lang/String;
    .locals 4

    .prologue
    .line 482
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 483
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->j:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 125
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 126
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/s$a;

    iput-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    .line 127
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    .line 128
    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 129
    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 130
    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 131
    iget-object v1, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public v()Lco/uk/getmondo/d/f;
    .locals 6

    .prologue
    .line 511
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 512
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->k:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    const/4 v0, 0x0

    .line 515
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/f;

    iget-object v2, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v4, v3, Lio/realm/s$a;->k:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/f;

    goto :goto_0
.end method

.method public w()Z
    .locals 4

    .prologue
    .line 563
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 564
    iget-object v0, p0, Lio/realm/s;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/s;->a:Lio/realm/s$a;

    iget-wide v2, v1, Lio/realm/s$a;->l:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method
