.class final Lio/realm/be$a;
.super Lio/realm/internal/c;
.source "RealmObjectSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/be;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lio/realm/internal/Table;


# direct methods
.method constructor <init>(Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 698
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 699
    iput-object p1, p0, Lio/realm/be$a;->a:Lio/realm/internal/Table;

    .line 700
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lio/realm/be$a;->a:Lio/realm/internal/Table;

    invoke-virtual {v0, p1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected a(Z)Lio/realm/internal/c;
    .locals 2

    .prologue
    .line 724
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "DynamicColumnIndices cannot be copied"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 719
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "DynamicColumnIndices cannot be copied"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 730
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "DynamicColumnIndices cannot copy"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/lang/String;)Lio/realm/RealmFieldType;
    .locals 2

    .prologue
    .line 709
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "DynamicColumnIndices do not support \'getColumnType\'"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 714
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "DynamicColumnIndices do not support \'getLinkedTable\'"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
