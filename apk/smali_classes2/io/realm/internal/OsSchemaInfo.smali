.class public Lio/realm/internal/OsSchemaInfo;
.super Ljava/lang/Object;
.source "OsSchemaInfo.java"

# interfaces
.implements Lio/realm/internal/g;


# static fields
.field private static final b:J


# instance fields
.field private a:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Lio/realm/internal/OsSchemaInfo;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/OsSchemaInfo;->b:J

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lio/realm/internal/OsObjectSchemaInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 39
    const/4 v0, 0x0

    .line 40
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/OsObjectSchemaInfo;

    .line 41
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo;->getNativePtr()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 42
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-static {v2}, Lio/realm/internal/OsSchemaInfo;->nativeCreateFromList([J)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/OsSchemaInfo;->a:J

    .line 45
    sget-object v0, Lio/realm/internal/f;->a:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 46
    return-void
.end method

.method private static native nativeCreateFromList([J)J
.end method

.method private static native nativeGetFinalizerPtr()J
.end method


# virtual methods
.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 55
    sget-wide v0, Lio/realm/internal/OsSchemaInfo;->b:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lio/realm/internal/OsSchemaInfo;->a:J

    return-wide v0
.end method
