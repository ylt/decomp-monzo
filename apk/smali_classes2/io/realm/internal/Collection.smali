.class public Lio/realm/internal/Collection;
.super Ljava/lang/Object;
.source "Collection.java"

# interfaces
.implements Lio/realm/internal/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/Collection$f;,
        Lio/realm/internal/Collection$a;,
        Lio/realm/internal/Collection$e;,
        Lio/realm/internal/Collection$d;,
        Lio/realm/internal/Collection$b;,
        Lio/realm/internal/Collection$g;,
        Lio/realm/internal/Collection$c;
    }
.end annotation

.annotation build Lio/realm/internal/Keep;
.end annotation


# static fields
.field public static final AGGREGATE_FUNCTION_AVERAGE:B = 0x3t

.field public static final AGGREGATE_FUNCTION_MAXIMUM:B = 0x2t

.field public static final AGGREGATE_FUNCTION_MINIMUM:B = 0x1t

.field public static final AGGREGATE_FUNCTION_SUM:B = 0x4t

.field private static final CLOSED_REALM_MESSAGE:Ljava/lang/String; = "This Realm instance has already been closed, making it unusable."

.field public static final MODE_EMPTY:B = 0x0t

.field public static final MODE_LINKVIEW:B = 0x3t

.field public static final MODE_QUERY:B = 0x2t

.field public static final MODE_TABLE:B = 0x1t

.field public static final MODE_TABLEVIEW:B = 0x4t

.field private static final nativeFinalizerPtr:J


# instance fields
.field private final context:Lio/realm/internal/f;

.field private isSnapshot:Z

.field private loaded:Z

.field private final nativePtr:J

.field private final observerPairs:Lio/realm/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/internal/i",
            "<",
            "Lio/realm/internal/Collection$c;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedRealm:Lio/realm/internal/SharedRealm;

.field private final table:Lio/realm/internal/Table;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 260
    invoke-static {}, Lio/realm/internal/Collection;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/Collection;->nativeFinalizerPtr:J

    return-void
.end method

.method public constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/LinkView;Lio/realm/internal/SortDescriptor;)V
    .locals 4

    .prologue
    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/Collection;->isSnapshot:Z

    .line 266
    new-instance v0, Lio/realm/internal/i;

    invoke-direct {v0}, Lio/realm/internal/i;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    .line 365
    invoke-virtual {p1}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p2}, Lio/realm/internal/LinkView;->getNativePtr()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p3}, Lio/realm/internal/Collection;->nativeCreateResultsFromLinkView(JJLio/realm/internal/SortDescriptor;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    .line 368
    iput-object p1, p0, Lio/realm/internal/Collection;->sharedRealm:Lio/realm/internal/SharedRealm;

    .line 369
    iget-object v0, p1, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    iput-object v0, p0, Lio/realm/internal/Collection;->context:Lio/realm/internal/f;

    .line 370
    invoke-virtual {p2}, Lio/realm/internal/LinkView;->e()Lio/realm/internal/Table;

    move-result-object v0

    iput-object v0, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    .line 371
    iget-object v0, p0, Lio/realm/internal/Collection;->context:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/Collection;->loaded:Z

    .line 375
    return-void
.end method

.method private constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;J)V
    .locals 7

    .prologue
    .line 378
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;JZ)V

    .line 379
    return-void
.end method

.method private constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;JZ)V
    .locals 1

    .prologue
    .line 381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/Collection;->isSnapshot:Z

    .line 266
    new-instance v0, Lio/realm/internal/i;

    invoke-direct {v0}, Lio/realm/internal/i;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    .line 382
    iput-object p1, p0, Lio/realm/internal/Collection;->sharedRealm:Lio/realm/internal/SharedRealm;

    .line 383
    iget-object v0, p1, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    iput-object v0, p0, Lio/realm/internal/Collection;->context:Lio/realm/internal/f;

    .line 384
    iput-object p2, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    .line 385
    iput-wide p3, p0, Lio/realm/internal/Collection;->nativePtr:J

    .line 386
    iget-object v0, p0, Lio/realm/internal/Collection;->context:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 387
    iput-boolean p5, p0, Lio/realm/internal/Collection;->loaded:Z

    .line 388
    return-void
.end method

.method public constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/TableQuery;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 361
    invoke-direct {p0, p1, p2, v0, v0}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;)V

    .line 362
    return-void
.end method

.method public constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;)V
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;)V

    .line 358
    return-void
.end method

.method public constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    iput-boolean v6, p0, Lio/realm/internal/Collection;->isSnapshot:Z

    .line 266
    new-instance v0, Lio/realm/internal/i;

    invoke-direct {v0}, Lio/realm/internal/i;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    .line 343
    invoke-virtual {p2}, Lio/realm/internal/TableQuery;->b()V

    .line 345
    invoke-virtual {p1}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p2}, Lio/realm/internal/TableQuery;->getNativePtr()J

    move-result-wide v2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Collection;->nativeCreateResults(JJLio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    .line 349
    iput-object p1, p0, Lio/realm/internal/Collection;->sharedRealm:Lio/realm/internal/SharedRealm;

    .line 350
    iget-object v0, p1, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    iput-object v0, p0, Lio/realm/internal/Collection;->context:Lio/realm/internal/f;

    .line 351
    invoke-virtual {p2}, Lio/realm/internal/TableQuery;->a()Lio/realm/internal/Table;

    move-result-object v0

    iput-object v0, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    .line 352
    iget-object v0, p0, Lio/realm/internal/Collection;->context:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 353
    iput-boolean v6, p0, Lio/realm/internal/Collection;->loaded:Z

    .line 354
    return-void
.end method

.method static synthetic access$000(Lio/realm/internal/Collection;)Lio/realm/internal/SharedRealm;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lio/realm/internal/Collection;->sharedRealm:Lio/realm/internal/SharedRealm;

    return-object v0
.end method

.method static synthetic access$100(Lio/realm/internal/Collection;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lio/realm/internal/Collection;->isSnapshot:Z

    return v0
.end method

.method public static createBacklinksCollection(Lio/realm/internal/SharedRealm;Lio/realm/internal/UncheckedRow;Lio/realm/internal/Table;Ljava/lang/String;)Lio/realm/internal/Collection;
    .locals 8

    .prologue
    .line 333
    .line 334
    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    .line 335
    invoke-virtual {p1}, Lio/realm/internal/UncheckedRow;->getNativePtr()J

    move-result-wide v2

    .line 336
    invoke-virtual {p2}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v4

    .line 337
    invoke-virtual {p2, p3}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v6

    .line 333
    invoke-static/range {v0 .. v7}, Lio/realm/internal/Collection;->nativeCreateResultsFromBacklinks(JJJJ)J

    move-result-wide v4

    .line 338
    new-instance v1, Lio/realm/internal/Collection;

    const/4 v6, 0x1

    move-object v2, p0

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;JZ)V

    return-object v1
.end method

.method private static native nativeAggregate(JJB)Ljava/lang/Object;
.end method

.method private static native nativeClear(J)V
.end method

.method private static native nativeContains(JJ)Z
.end method

.method private static native nativeCreateResults(JJLio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;)J
.end method

.method private static native nativeCreateResultsFromBacklinks(JJJJ)J
.end method

.method private static native nativeCreateResultsFromLinkView(JJLio/realm/internal/SortDescriptor;)J
.end method

.method private static native nativeCreateSnapshot(J)J
.end method

.method private static native nativeDelete(JJ)V
.end method

.method private static native nativeDeleteFirst(J)Z
.end method

.method private static native nativeDeleteLast(J)Z
.end method

.method private static native nativeDistinct(JLio/realm/internal/SortDescriptor;)J
.end method

.method private static native nativeFirstRow(J)J
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private static native nativeGetMode(J)B
.end method

.method private static native nativeGetRow(JI)J
.end method

.method private static native nativeIndexOf(JJ)J
.end method

.method private static native nativeIndexOfBySourceRowIndex(JJ)J
.end method

.method private static native nativeIsValid(J)Z
.end method

.method private static native nativeLastRow(J)J
.end method

.method private static native nativeSize(J)J
.end method

.method private static native nativeSort(JLio/realm/internal/SortDescriptor;)J
.end method

.method private native nativeStartListening(J)V
.end method

.method private native nativeStopListening(J)V
.end method

.method private static native nativeWhere(J)J
.end method

.method private notifyChangeListeners(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 523
    cmp-long v0, p1, v4

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lio/realm/internal/Collection;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    :goto_0
    return-void

    .line 526
    :cond_0
    iget-boolean v0, p0, Lio/realm/internal/Collection;->loaded:Z

    .line 527
    const/4 v1, 0x1

    iput-boolean v1, p0, Lio/realm/internal/Collection;->loaded:Z

    .line 531
    iget-object v1, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    new-instance v2, Lio/realm/internal/Collection$b;

    cmp-long v3, p1, v4

    if-eqz v3, :cond_1

    if-nez v0, :cond_2

    .line 532
    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-direct {v2, v0}, Lio/realm/internal/Collection$b;-><init>(Lio/realm/al;)V

    .line 531
    invoke-virtual {v1, v2}, Lio/realm/internal/i;->a(Lio/realm/internal/i$a;)V

    goto :goto_0

    .line 532
    :cond_2
    new-instance v0, Lio/realm/internal/CollectionChangeSet;

    invoke-direct {v0, p1, p2}, Lio/realm/internal/CollectionChangeSet;-><init>(J)V

    goto :goto_1
.end method


# virtual methods
.method public addListener(Ljava/lang/Object;Lio/realm/am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lio/realm/am",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 489
    iget-object v0, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    invoke-virtual {v0}, Lio/realm/internal/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Collection;->nativeStartListening(J)V

    .line 492
    :cond_0
    new-instance v0, Lio/realm/internal/Collection$c;

    invoke-direct {v0, p1, p2}, Lio/realm/internal/Collection$c;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 493
    iget-object v1, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    invoke-virtual {v1, v0}, Lio/realm/internal/i;->a(Lio/realm/internal/i$b;)V

    .line 494
    return-void
.end method

.method public addListener(Ljava/lang/Object;Lio/realm/ax;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lio/realm/ax",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 497
    new-instance v0, Lio/realm/internal/Collection$g;

    invoke-direct {v0, p2}, Lio/realm/internal/Collection$g;-><init>(Lio/realm/ax;)V

    invoke-virtual {p0, p1, v0}, Lio/realm/internal/Collection;->addListener(Ljava/lang/Object;Lio/realm/am;)V

    .line 498
    return-void
.end method

.method public aggregateDate(Lio/realm/internal/Collection$a;J)Ljava/util/Date;
    .locals 4

    .prologue
    .line 443
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-virtual {p1}, Lio/realm/internal/Collection$a;->a()B

    move-result v2

    invoke-static {v0, v1, p2, p3, v2}, Lio/realm/internal/Collection;->nativeAggregate(JJB)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public aggregateNumber(Lio/realm/internal/Collection$a;J)Ljava/lang/Number;
    .locals 4

    .prologue
    .line 439
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-virtual {p1}, Lio/realm/internal/Collection$a;->a()B

    move-result v2

    invoke-static {v0, v1, p2, p3, v2}, Lio/realm/internal/Collection;->nativeAggregate(JJB)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    return-object v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 451
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeClear(J)V

    .line 452
    return-void
.end method

.method public contains(Lio/realm/internal/UncheckedRow;)Z
    .locals 4

    .prologue
    .line 463
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-virtual {p1}, Lio/realm/internal/UncheckedRow;->getNativePtr()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lio/realm/internal/Collection;->nativeContains(JJ)Z

    move-result v0

    return v0
.end method

.method public createSnapshot()Lio/realm/internal/Collection;
    .locals 6

    .prologue
    .line 391
    iget-boolean v0, p0, Lio/realm/internal/Collection;->isSnapshot:Z

    if-eqz v0, :cond_0

    .line 396
    :goto_0
    return-object p0

    .line 394
    :cond_0
    new-instance v0, Lio/realm/internal/Collection;

    iget-object v1, p0, Lio/realm/internal/Collection;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v2, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    iget-wide v4, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v4, v5}, Lio/realm/internal/Collection;->nativeCreateSnapshot(J)J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;J)V

    .line 395
    const/4 v1, 0x1

    iput-boolean v1, v0, Lio/realm/internal/Collection;->isSnapshot:Z

    move-object p0, v0

    .line 396
    goto :goto_0
.end method

.method public delete(J)V
    .locals 3

    .prologue
    .line 477
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/Collection;->nativeDelete(JJ)V

    .line 478
    return-void
.end method

.method public deleteFirst()Z
    .locals 2

    .prologue
    .line 481
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeDeleteFirst(J)Z

    move-result v0

    return v0
.end method

.method public deleteLast()Z
    .locals 2

    .prologue
    .line 485
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeDeleteLast(J)Z

    move-result v0

    return v0
.end method

.method public distinct(Lio/realm/internal/SortDescriptor;)Lio/realm/internal/Collection;
    .locals 6

    .prologue
    .line 459
    new-instance v0, Lio/realm/internal/Collection;

    iget-object v1, p0, Lio/realm/internal/Collection;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v2, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    iget-wide v4, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v4, v5, p1}, Lio/realm/internal/Collection;->nativeDistinct(JLio/realm/internal/SortDescriptor;)J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;J)V

    return-object v0
.end method

.method public firstUncheckedRow()Lio/realm/internal/UncheckedRow;
    .locals 4

    .prologue
    .line 414
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeFirstRow(J)J

    move-result-wide v0

    .line 415
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 416
    iget-object v2, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->g(J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    .line 418
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMode()Lio/realm/internal/Collection$f;
    .locals 2

    .prologue
    .line 536
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeGetMode(J)B

    move-result v0

    invoke-static {v0}, Lio/realm/internal/Collection$f;->a(B)Lio/realm/internal/Collection$f;

    move-result-object v0

    return-object v0
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 406
    sget-wide v0, Lio/realm/internal/Collection;->nativeFinalizerPtr:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 401
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    return-wide v0
.end method

.method public getTable()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    return-object v0
.end method

.method public getUncheckedRow(I)Lio/realm/internal/UncheckedRow;
    .locals 4

    .prologue
    .line 410
    iget-object v0, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    iget-wide v2, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v2, v3, p1}, Lio/realm/internal/Collection;->nativeGetRow(JI)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->g(J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(J)I
    .locals 5

    .prologue
    .line 472
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/Collection;->nativeIndexOfBySourceRowIndex(JJ)J

    move-result-wide v0

    .line 473
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public indexOf(Lio/realm/internal/UncheckedRow;)I
    .locals 4

    .prologue
    .line 467
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-virtual {p1}, Lio/realm/internal/UncheckedRow;->getNativePtr()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lio/realm/internal/Collection;->nativeIndexOf(JJ)J

    move-result-wide v0

    .line 468
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 551
    iget-boolean v0, p0, Lio/realm/internal/Collection;->loaded:Z

    return v0
.end method

.method public isValid()Z
    .locals 2

    .prologue
    .line 517
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeIsValid(J)Z

    move-result v0

    return v0
.end method

.method public lastUncheckedRow()Lio/realm/internal/UncheckedRow;
    .locals 4

    .prologue
    .line 422
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeLastRow(J)J

    move-result-wide v0

    .line 423
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 424
    iget-object v2, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->g(J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    .line 426
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load()V
    .locals 2

    .prologue
    .line 555
    iget-boolean v0, p0, Lio/realm/internal/Collection;->loaded:Z

    if-eqz v0, :cond_0

    .line 559
    :goto_0
    return-void

    .line 558
    :cond_0
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Collection;->notifyChangeListeners(J)V

    goto :goto_0
.end method

.method public removeAllListeners()V
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    invoke-virtual {v0}, Lio/realm/internal/i;->b()V

    .line 513
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Collection;->nativeStopListening(J)V

    .line 514
    return-void
.end method

.method public removeListener(Ljava/lang/Object;Lio/realm/am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lio/realm/am",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 501
    iget-object v0, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    invoke-virtual {v0, p1, p2}, Lio/realm/internal/i;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 502
    iget-object v0, p0, Lio/realm/internal/Collection;->observerPairs:Lio/realm/internal/i;

    invoke-virtual {v0}, Lio/realm/internal/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Collection;->nativeStopListening(J)V

    .line 505
    :cond_0
    return-void
.end method

.method public removeListener(Ljava/lang/Object;Lio/realm/ax;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lio/realm/ax",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 508
    new-instance v0, Lio/realm/internal/Collection$g;

    invoke-direct {v0, p2}, Lio/realm/internal/Collection$g;-><init>(Lio/realm/ax;)V

    invoke-virtual {p0, p1, v0}, Lio/realm/internal/Collection;->removeListener(Ljava/lang/Object;Lio/realm/am;)V

    .line 509
    return-void
.end method

.method public size()J
    .locals 2

    .prologue
    .line 447
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public sort(Lio/realm/internal/SortDescriptor;)Lio/realm/internal/Collection;
    .locals 6

    .prologue
    .line 455
    new-instance v0, Lio/realm/internal/Collection;

    iget-object v1, p0, Lio/realm/internal/Collection;->sharedRealm:Lio/realm/internal/SharedRealm;

    iget-object v2, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    iget-wide v4, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v4, v5, p1}, Lio/realm/internal/Collection;->nativeSort(JLio/realm/internal/SortDescriptor;)J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;J)V

    return-object v0
.end method

.method public where()Lio/realm/internal/TableQuery;
    .locals 5

    .prologue
    .line 434
    iget-wide v0, p0, Lio/realm/internal/Collection;->nativePtr:J

    invoke-static {v0, v1}, Lio/realm/internal/Collection;->nativeWhere(J)J

    move-result-wide v0

    .line 435
    new-instance v2, Lio/realm/internal/TableQuery;

    iget-object v3, p0, Lio/realm/internal/Collection;->context:Lio/realm/internal/f;

    iget-object v4, p0, Lio/realm/internal/Collection;->table:Lio/realm/internal/Table;

    invoke-direct {v2, v3, v4, v0, v1}, Lio/realm/internal/TableQuery;-><init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V

    return-object v2
.end method
