.class public Lio/realm/internal/CollectionChangeSet;
.super Ljava/lang/Object;
.source "CollectionChangeSet.java"

# interfaces
.implements Lio/realm/al;
.implements Lio/realm/internal/g;


# static fields
.field private static a:J


# instance fields
.field private final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Lio/realm/internal/CollectionChangeSet;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/CollectionChangeSet;->a:J

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-wide p1, p0, Lio/realm/internal/CollectionChangeSet;->b:J

    .line 47
    sget-object v0, Lio/realm/internal/f;->a:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 48
    return-void
.end method

.method private a([I)[Lio/realm/al$a;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 113
    if-nez p1, :cond_0

    .line 115
    new-array v0, v0, [Lio/realm/al$a;

    .line 122
    :goto_0
    return-object v0

    .line 118
    :cond_0
    array-length v1, p1

    div-int/lit8 v1, v1, 0x2

    new-array v1, v1, [Lio/realm/al$a;

    .line 119
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 120
    new-instance v2, Lio/realm/al$a;

    mul-int/lit8 v3, v0, 0x2

    aget v3, p1, v3

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x1

    aget v4, p1, v4

    invoke-direct {v2, v3, v4}, Lio/realm/al$a;-><init>(II)V

    aput-object v2, v1, v0

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 122
    goto :goto_0
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private static native nativeGetRanges(JI)[I
.end method


# virtual methods
.method public a()[Lio/realm/al$a;
    .locals 3

    .prologue
    .line 79
    iget-wide v0, p0, Lio/realm/internal/CollectionChangeSet;->b:J

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lio/realm/internal/CollectionChangeSet;->nativeGetRanges(JI)[I

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/internal/CollectionChangeSet;->a([I)[Lio/realm/al$a;

    move-result-object v0

    return-object v0
.end method

.method public b()[Lio/realm/al$a;
    .locals 3

    .prologue
    .line 87
    iget-wide v0, p0, Lio/realm/internal/CollectionChangeSet;->b:J

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lio/realm/internal/CollectionChangeSet;->nativeGetRanges(JI)[I

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/internal/CollectionChangeSet;->a([I)[Lio/realm/al$a;

    move-result-object v0

    return-object v0
.end method

.method public c()[Lio/realm/al$a;
    .locals 3

    .prologue
    .line 95
    iget-wide v0, p0, Lio/realm/internal/CollectionChangeSet;->b:J

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lio/realm/internal/CollectionChangeSet;->nativeGetRanges(JI)[I

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/internal/CollectionChangeSet;->a([I)[Lio/realm/al$a;

    move-result-object v0

    return-object v0
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 108
    sget-wide v0, Lio/realm/internal/CollectionChangeSet;->a:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lio/realm/internal/CollectionChangeSet;->b:J

    return-wide v0
.end method
