.class public Lio/realm/internal/OsObject;
.super Ljava/lang/Object;
.source "OsObject.java"

# interfaces
.implements Lio/realm/internal/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/OsObject$a;,
        Lio/realm/internal/OsObject$b;,
        Lio/realm/internal/OsObject$c;
    }
.end annotation

.annotation build Lio/realm/internal/KeepMember;
.end annotation


# static fields
.field private static final b:J


# instance fields
.field private final a:J

.field private c:Lio/realm/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/internal/i",
            "<",
            "Lio/realm/internal/OsObject$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Lio/realm/internal/OsObject;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/OsObject;->b:J

    return-void
.end method

.method public constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/UncheckedRow;)V
    .locals 4

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lio/realm/internal/i;

    invoke-direct {v0}, Lio/realm/internal/i;-><init>()V

    iput-object v0, p0, Lio/realm/internal/OsObject;->c:Lio/realm/internal/i;

    .line 98
    invoke-virtual {p1}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p2}, Lio/realm/internal/UncheckedRow;->getNativePtr()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lio/realm/internal/OsObject;->nativeCreate(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/OsObject;->a:J

    .line 99
    iget-object v0, p1, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 100
    return-void
.end method

.method public static a(Lio/realm/internal/Table;)Lio/realm/internal/UncheckedRow;
    .locals 8

    .prologue
    .line 155
    invoke-virtual {p0}, Lio/realm/internal/Table;->f()Lio/realm/internal/SharedRealm;

    move-result-object v0

    .line 156
    new-instance v1, Lio/realm/internal/UncheckedRow;

    iget-object v2, v0, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    .line 157
    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v4

    invoke-virtual {p0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lio/realm/internal/OsObject;->nativeCreateNewObject(JJ)J

    move-result-wide v4

    invoke-direct {v1, v2, p0, v4, v5}, Lio/realm/internal/UncheckedRow;-><init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V

    .line 156
    return-object v1
.end method

.method public static a(Lio/realm/internal/Table;Ljava/lang/Object;)Lio/realm/internal/UncheckedRow;
    .locals 11

    .prologue
    .line 189
    invoke-static {p0}, Lio/realm/internal/OsObject;->c(Lio/realm/internal/Table;)J

    move-result-wide v4

    .line 190
    invoke-virtual {p0, v4, v5}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v0

    .line 191
    invoke-virtual {p0}, Lio/realm/internal/Table;->f()Lio/realm/internal/SharedRealm;

    move-result-object v1

    .line 193
    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-ne v0, v2, :cond_1

    .line 194
    if-eqz p1, :cond_0

    instance-of v0, p1, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Primary key value is not a String: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    new-instance v7, Lio/realm/internal/UncheckedRow;

    iget-object v8, v1, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    .line 198
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    move-object v6, p1

    check-cast v6, Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lio/realm/internal/OsObject;->nativeCreateNewObjectWithStringPrimaryKey(JJJLjava/lang/String;)J

    move-result-wide v0

    invoke-direct {v7, v8, p0, v0, v1}, Lio/realm/internal/UncheckedRow;-><init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V

    move-object v0, v7

    .line 203
    :goto_0
    return-object v0

    .line 201
    :cond_1
    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-ne v0, v2, :cond_4

    .line 202
    if-nez p1, :cond_2

    const-wide/16 v6, 0x0

    .line 203
    :goto_1
    new-instance v9, Lio/realm/internal/UncheckedRow;

    iget-object v10, v1, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    .line 204
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    if-nez p1, :cond_3

    const/4 v8, 0x1

    :goto_2
    invoke-static/range {v0 .. v8}, Lio/realm/internal/OsObject;->nativeCreateNewObjectWithLongPrimaryKey(JJJJZ)J

    move-result-wide v0

    invoke-direct {v9, v10, p0, v0, v1}, Lio/realm/internal/UncheckedRow;-><init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V

    move-object v0, v9

    .line 203
    goto :goto_0

    .line 202
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 204
    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    .line 207
    :cond_4
    new-instance v1, Lio/realm/exceptions/RealmException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot check for duplicate rows for unsupported primary key type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static b(Lio/realm/internal/Table;)J
    .locals 4

    .prologue
    .line 168
    invoke-virtual {p0}, Lio/realm/internal/Table;->f()Lio/realm/internal/SharedRealm;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lio/realm/internal/OsObject;->nativeCreateRow(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(Lio/realm/internal/Table;Ljava/lang/Object;)J
    .locals 9

    .prologue
    .line 220
    invoke-static {p0}, Lio/realm/internal/OsObject;->c(Lio/realm/internal/Table;)J

    move-result-wide v4

    .line 221
    invoke-virtual {p0, v4, v5}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v0

    .line 222
    invoke-virtual {p0}, Lio/realm/internal/Table;->f()Lio/realm/internal/SharedRealm;

    move-result-object v1

    .line 224
    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-ne v0, v2, :cond_1

    .line 225
    if-eqz p1, :cond_0

    instance-of v0, p1, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 226
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Primary key value is not a String: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :cond_0
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    move-object v6, p1

    check-cast v6, Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lio/realm/internal/OsObject;->nativeCreateRowWithStringPrimaryKey(JJJLjava/lang/String;)J

    move-result-wide v0

    .line 233
    :goto_0
    return-wide v0

    .line 231
    :cond_1
    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-ne v0, v2, :cond_4

    .line 232
    if-nez p1, :cond_2

    const-wide/16 v6, 0x0

    .line 233
    :goto_1
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    if-nez p1, :cond_3

    const/4 v8, 0x1

    :goto_2
    invoke-static/range {v0 .. v8}, Lio/realm/internal/OsObject;->nativeCreateRowWithLongPrimaryKey(JJJJZ)J

    move-result-wide v0

    goto :goto_0

    .line 232
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 233
    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    .line 236
    :cond_4
    new-instance v1, Lio/realm/exceptions/RealmException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot check for duplicate rows for unsupported primary key type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static c(Lio/realm/internal/Table;)J
    .locals 4

    .prologue
    .line 173
    invoke-virtual {p0}, Lio/realm/internal/Table;->d()J

    move-result-wide v0

    .line 174
    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 175
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no primary key defined."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    return-wide v0
.end method

.method private static native nativeCreate(JJ)J
.end method

.method private static native nativeCreateNewObject(JJ)J
.end method

.method private static native nativeCreateNewObjectWithLongPrimaryKey(JJJJZ)J
.end method

.method private static native nativeCreateNewObjectWithStringPrimaryKey(JJJLjava/lang/String;)J
.end method

.method private static native nativeCreateRow(JJ)J
.end method

.method private static native nativeCreateRowWithLongPrimaryKey(JJJJZ)J
.end method

.method private static native nativeCreateRowWithStringPrimaryKey(JJJLjava/lang/String;)J
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private native nativeStartListening(J)V
.end method

.method private notifyChangeListeners([Ljava/lang/String;)V
    .locals 2
    .annotation build Lio/realm/internal/KeepMember;
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lio/realm/internal/OsObject;->c:Lio/realm/internal/i;

    new-instance v1, Lio/realm/internal/OsObject$a;

    invoke-direct {v1, p1}, Lio/realm/internal/OsObject$a;-><init>([Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lio/realm/internal/i;->a(Lio/realm/internal/i$a;)V

    .line 245
    return-void
.end method


# virtual methods
.method public a(Lio/realm/internal/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/i",
            "<",
            "Lio/realm/internal/OsObject$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lio/realm/internal/OsObject;->c:Lio/realm/internal/i;

    invoke-virtual {v0}, Lio/realm/internal/i;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "\'observerPairs\' is not empty. Listeners have been added before."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_0
    iput-object p1, p0, Lio/realm/internal/OsObject;->c:Lio/realm/internal/i;

    .line 142
    invoke-virtual {p1}, Lio/realm/internal/i;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    iget-wide v0, p0, Lio/realm/internal/OsObject;->a:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/OsObject;->nativeStartListening(J)V

    .line 145
    :cond_1
    return-void
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 109
    sget-wide v0, Lio/realm/internal/OsObject;->b:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lio/realm/internal/OsObject;->a:J

    return-wide v0
.end method
