.class public Lio/realm/internal/Table;
.super Ljava/lang/Object;
.source "Table.java"

# interfaces
.implements Lio/realm/internal/g;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:J


# instance fields
.field private final c:J

.field private final d:Lio/realm/internal/f;

.field private final e:Lio/realm/internal/SharedRealm;

.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    invoke-static {}, Lio/realm/internal/Util;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/realm/internal/Table;->a:Ljava/lang/String;

    .line 47
    invoke-static {}, Lio/realm/internal/Table;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/Table;->b:J

    return-void
.end method

.method constructor <init>(Lio/realm/internal/SharedRealm;J)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/internal/Table;->f:J

    .line 60
    iget-object v0, p1, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    iput-object v0, p0, Lio/realm/internal/Table;->d:Lio/realm/internal/f;

    .line 61
    iput-object p1, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    .line 62
    iput-wide p2, p0, Lio/realm/internal/Table;->c:J

    .line 63
    iget-object v0, p0, Lio/realm/internal/Table;->d:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 64
    return-void
.end method

.method constructor <init>(Lio/realm/internal/Table;J)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p1, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    invoke-direct {p0, v0, p2, p3}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/SharedRealm;J)V

    .line 57
    return-void
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 416
    new-instance v0, Lio/realm/exceptions/RealmPrimaryKeyConstraintException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value already exists: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmPrimaryKeyConstraintException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lio/realm/internal/SharedRealm;)Z
    .locals 4

    .prologue
    .line 644
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 645
    :cond_0
    invoke-static {}, Lio/realm/internal/Table;->m()V

    .line 647
    :cond_1
    const-string v0, "pk"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 648
    const/4 v0, 0x0

    .line 651
    :goto_0
    return v0

    .line 650
    :cond_2
    const-string v0, "pk"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 651
    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->f()J

    move-result-wide v2

    iget-wide v0, v0, Lio/realm/internal/Table;->c:J

    invoke-static {v2, v3, v0, v1}, Lio/realm/internal/Table;->nativeMigratePrimaryKeyTableIfNeeded(JJ)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Lio/realm/internal/SharedRealm;)Z
    .locals 2

    .prologue
    .line 655
    const-string v0, "pk"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 656
    const/4 v0, 0x0

    .line 659
    :goto_0
    return v0

    .line 658
    :cond_0
    const-string v0, "pk"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 659
    iget-wide v0, v0, Lio/realm/internal/Table;->c:J

    invoke-static {v0, v1}, Lio/realm/internal/Table;->nativePrimaryKeyTableNeedsMigration(J)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 841
    sget-object v0, Lio/realm/internal/Table;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 856
    if-nez p0, :cond_1

    const/4 p0, 0x0

    .line 860
    :cond_0
    :goto_0
    return-object p0

    .line 857
    :cond_1
    sget-object v0, Lio/realm/internal/Table;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 860
    sget-object v0, Lio/realm/internal/Table;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 864
    if-nez p0, :cond_1

    const/4 p0, 0x0

    .line 868
    :cond_0
    :goto_0
    return-object p0

    .line 865
    :cond_1
    sget-object v0, Lio/realm/internal/Table;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 868
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lio/realm/internal/Table;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x3f

    if-le v0, v1, :cond_0

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column names are currently limited to max 63 characters."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    return-void
.end method

.method private l()Lio/realm/internal/Table;
    .locals 6

    .prologue
    .line 607
    iget-object v0, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    if-nez v0, :cond_1

    .line 608
    const/4 v0, 0x0

    .line 624
    :cond_0
    :goto_0
    return-object v0

    .line 612
    :cond_1
    iget-object v0, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    const-string v1, "pk"

    invoke-virtual {v0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 613
    iget-object v0, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    const-string v1, "pk"

    invoke-virtual {v0, v1}, Lio/realm/internal/SharedRealm;->c(Ljava/lang/String;)Lio/realm/internal/Table;

    .line 616
    :cond_2
    iget-object v0, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    const-string v1, "pk"

    invoke-virtual {v0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 617
    invoke-virtual {v0}, Lio/realm/internal/Table;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 618
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 619
    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v2, "pk_table"

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->a(Lio/realm/RealmFieldType;Ljava/lang/String;)J

    move-result-wide v2

    .line 620
    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->i(J)V

    .line 621
    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v2, "pk_property"

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->a(Lio/realm/RealmFieldType;Ljava/lang/String;)J

    goto :goto_0
.end method

.method private l(J)Z
    .locals 3

    .prologue
    .line 324
    invoke-virtual {p0}, Lio/realm/internal/Table;->d()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static m()V
    .locals 2

    .prologue
    .line 821
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot modify managed objects outside of a write transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private m(J)Z
    .locals 3

    .prologue
    .line 360
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lio/realm/internal/Table;->d()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeAddColumn(JILjava/lang/String;Z)J
.end method

.method private native nativeAddSearchIndex(JJ)V
.end method

.method private native nativeClear(J)V
.end method

.method public static native nativeFindFirstInt(JJJ)J
.end method

.method public static native nativeFindFirstNull(JJ)J
.end method

.method public static native nativeFindFirstString(JJLjava/lang/String;)J
.end method

.method private native nativeGetColumnCount(J)J
.end method

.method private native nativeGetColumnIndex(JLjava/lang/String;)J
.end method

.method private native nativeGetColumnName(JJ)Ljava/lang/String;
.end method

.method private native nativeGetColumnType(JJ)I
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private native nativeGetLinkTarget(JJ)J
.end method

.method public static native nativeGetLinkView(JJJ)J
.end method

.method private native nativeGetName(J)Ljava/lang/String;
.end method

.method private native nativeHasSameSchema(JJ)Z
.end method

.method private native nativeHasSearchIndex(JJ)Z
.end method

.method private native nativeIsColumnNullable(JJ)Z
.end method

.method private static native nativeMigratePrimaryKeyTableIfNeeded(JJ)Z
.end method

.method private native nativeMoveLastOver(JJ)V
.end method

.method public static native nativeNullifyLink(JJJ)V
.end method

.method private static native nativePrimaryKeyTableNeedsMigration(J)Z
.end method

.method public static native nativeSetBoolean(JJJZZ)V
.end method

.method public static native nativeSetDouble(JJJDZ)V
.end method

.method public static native nativeSetLink(JJJJZ)V
.end method

.method public static native nativeSetLong(JJJJZ)V
.end method

.method public static native nativeSetNull(JJJZ)V
.end method

.method public static native nativeSetString(JJJLjava/lang/String;Z)V
.end method

.method public static native nativeSetTimestamp(JJJJZ)V
.end method

.method private native nativeSize(J)J
.end method

.method private native nativeWhere(J)J
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 248
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JLjava/lang/String;)J
    .locals 3

    .prologue
    .line 739
    if-nez p3, :cond_0

    .line 740
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 742
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-static {v0, v1, p1, p2, p3}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lio/realm/RealmFieldType;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/internal/Table;->a(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J
    .locals 7

    .prologue
    .line 105
    invoke-direct {p0, p2}, Lio/realm/internal/Table;->e(Ljava/lang/String;)V

    .line 106
    iget-wide v2, p0, Lio/realm/internal/Table;->c:J

    invoke-virtual {p1}, Lio/realm/RealmFieldType;->getNativeValue()I

    move-result v4

    move-object v1, p0

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeAddColumn(JILjava/lang/String;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 296
    if-nez p1, :cond_0

    .line 297
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column name can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/Table;->nativeGetColumnIndex(JLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method a(JJ)V
    .locals 5

    .prologue
    .line 392
    invoke-direct {p0, p1, p2}, Lio/realm/internal/Table;->l(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v0

    .line 394
    sget-object v1, Lio/realm/internal/Table$1;->a:[I

    invoke-virtual {v0}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 397
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v0

    .line 398
    cmp-long v2, v0, p3

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 399
    const-string v0, "null"

    invoke-static {v0}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 394
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(JJDZ)V
    .locals 9

    .prologue
    .line 532
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 533
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetDouble(JJJDZ)V

    .line 534
    return-void
.end method

.method a(JJJ)V
    .locals 5

    .prologue
    .line 382
    invoke-direct {p0, p1, p2}, Lio/realm/internal/Table;->l(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    invoke-virtual {p0, p1, p2, p5, p6}, Lio/realm/internal/Table;->b(JJ)J

    move-result-wide v0

    .line 384
    cmp-long v2, v0, p3

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 385
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    .line 388
    :cond_0
    return-void
.end method

.method public a(JJJZ)V
    .locals 9

    .prologue
    .line 516
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 517
    invoke-virtual/range {p0 .. p6}, Lio/realm/internal/Table;->a(JJJ)V

    .line 518
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 519
    return-void
.end method

.method a(JJLjava/lang/String;)V
    .locals 5

    .prologue
    .line 373
    invoke-direct {p0, p1, p2}, Lio/realm/internal/Table;->m(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    invoke-virtual {p0, p1, p2, p5}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v0

    .line 375
    cmp-long v2, v0, p3

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 376
    invoke-static {p5}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    .line 379
    :cond_0
    return-void
.end method

.method public a(JJLjava/lang/String;Z)V
    .locals 9

    .prologue
    .line 550
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 551
    if-nez p5, :cond_0

    .line 552
    invoke-virtual {p0, p1, p2, p3, p4}, Lio/realm/internal/Table;->a(JJ)V

    .line 553
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    move-wide v2, p1

    move-wide v4, p3

    move v6, p6

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    .line 558
    :goto_0
    return-void

    .line 555
    :cond_0
    invoke-virtual/range {p0 .. p5}, Lio/realm/internal/Table;->a(JJLjava/lang/String;)V

    .line 556
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto :goto_0
.end method

.method public a(JJLjava/util/Date;Z)V
    .locals 9

    .prologue
    .line 537
    if-nez p5, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null Date is not allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 538
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 539
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-wide v2, p1

    move-wide v4, p3

    move v8, p6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    .line 540
    return-void
.end method

.method public a(JJZ)V
    .locals 7

    .prologue
    .line 571
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 572
    invoke-virtual {p0, p1, p2, p3, p4}, Lio/realm/internal/Table;->a(JJ)V

    .line 573
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    .line 574
    return-void
.end method

.method public a(JJZZ)V
    .locals 9

    .prologue
    .line 522
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 523
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 524
    return-void
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 218
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeIsColumnNullable(JJ)Z

    move-result v0

    return v0
.end method

.method public a(Lio/realm/internal/Table;)Z
    .locals 4

    .prologue
    .line 831
    if-nez p1, :cond_0

    .line 832
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The argument cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 834
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    iget-wide v2, p1, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/Table;->nativeHasSameSchema(JJ)Z

    move-result v0

    return v0
.end method

.method public b(JJ)J
    .locals 7

    .prologue
    .line 716
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 286
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetColumnName(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 264
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 265
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeClear(J)V

    .line 266
    return-void
.end method

.method public b(JJJZ)V
    .locals 9

    .prologue
    .line 566
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 567
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 568
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 276
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeGetColumnCount(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public c(J)Lio/realm/RealmFieldType;
    .locals 3

    .prologue
    .line 309
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetColumnType(JJ)I

    move-result v0

    invoke-static {v0}, Lio/realm/RealmFieldType;->fromNativeValue(I)Lio/realm/RealmFieldType;

    move-result-object v0

    return-object v0
.end method

.method public d()J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v0, -0x2

    .line 333
    iget-wide v2, p0, Lio/realm/internal/Table;->f:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-wide v2, p0, Lio/realm/internal/Table;->f:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_2

    .line 334
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->f:J

    .line 349
    :cond_1
    :goto_0
    return-wide v0

    .line 336
    :cond_2
    invoke-direct {p0}, Lio/realm/internal/Table;->l()Lio/realm/internal/Table;

    move-result-object v2

    .line 337
    if-eqz v2, :cond_1

    .line 341
    invoke-virtual {p0}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v4

    .line 342
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 343
    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/UncheckedRow;->k(J)Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-virtual {p0, v0}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Table;->f:J

    .line 349
    :goto_1
    iget-wide v0, p0, Lio/realm/internal/Table;->f:J

    goto :goto_0

    .line 346
    :cond_3
    iput-wide v0, p0, Lio/realm/internal/Table;->f:J

    goto :goto_1
.end method

.method public d(J)V
    .locals 3

    .prologue
    .line 319
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 320
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMoveLastOver(JJ)V

    .line 321
    return-void
.end method

.method public e(J)Lio/realm/internal/Table;
    .locals 5

    .prologue
    .line 467
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetLinkTarget(JJ)J

    move-result-wide v0

    .line 469
    new-instance v2, Lio/realm/internal/Table;

    iget-object v3, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    invoke-direct {v2, v3, v0, v1}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/SharedRealm;J)V

    return-object v2
.end method

.method public e()Z
    .locals 4

    .prologue
    .line 369
    invoke-virtual {p0}, Lio/realm/internal/Table;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()Lio/realm/internal/SharedRealm;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    return-object v0
.end method

.method public f(J)Lio/realm/internal/UncheckedRow;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lio/realm/internal/Table;->d:Lio/realm/internal/f;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/UncheckedRow;->b(Lio/realm/internal/f;Lio/realm/internal/Table;J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public g(J)Lio/realm/internal/UncheckedRow;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lio/realm/internal/Table;->d:Lio/realm/internal/f;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/UncheckedRow;->c(Lio/realm/internal/f;Lio/realm/internal/Table;J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    return-object v0
.end method

.method g()Z
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/internal/Table;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 73
    sget-wide v0, Lio/realm/internal/Table;->b:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    return-wide v0
.end method

.method public h(J)Lio/realm/internal/CheckedRow;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lio/realm/internal/Table;->d:Lio/realm/internal/f;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/CheckedRow;->a(Lio/realm/internal/f;Lio/realm/internal/Table;J)Lio/realm/internal/CheckedRow;

    move-result-object v0

    return-object v0
.end method

.method h()V
    .locals 1

    .prologue
    .line 680
    invoke-virtual {p0}, Lio/realm/internal/Table;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    invoke-static {}, Lio/realm/internal/Table;->m()V

    .line 683
    :cond_0
    return-void
.end method

.method public i()Lio/realm/internal/TableQuery;
    .locals 4

    .prologue
    .line 710
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeWhere(J)J

    move-result-wide v0

    .line 712
    new-instance v2, Lio/realm/internal/TableQuery;

    iget-object v3, p0, Lio/realm/internal/Table;->d:Lio/realm/internal/f;

    invoke-direct {v2, v3, p0, v0, v1}, Lio/realm/internal/TableQuery;-><init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V

    return-object v2
.end method

.method public i(J)V
    .locals 3

    .prologue
    .line 577
    invoke-virtual {p0}, Lio/realm/internal/Table;->h()V

    .line 578
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAddSearchIndex(JJ)V

    .line 579
    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 772
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeGetName(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j(J)Z
    .locals 3

    .prologue
    .line 663
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeHasSearchIndex(JJ)Z

    move-result v0

    return v0
.end method

.method public k(J)J
    .locals 3

    .prologue
    .line 752
    iget-wide v0, p0, Lio/realm/internal/Table;->c:J

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 781
    invoke-virtual {p0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/realm/internal/Table;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method native nativeGetRowPtr(JJ)J
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 790
    invoke-virtual {p0}, Lio/realm/internal/Table;->c()J

    move-result-wide v2

    .line 791
    invoke-virtual {p0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    .line 792
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "The Table "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 793
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 794
    invoke-virtual {p0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 797
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/Table;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 798
    invoke-virtual {p0}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 799
    const-string v4, "has \'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\' field as a PrimaryKey, and "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 801
    :cond_1
    const-string v0, "contains "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 802
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 803
    const-string v0, " columns: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 805
    const/4 v0, 0x0

    :goto_0
    int-to-long v4, v0

    cmp-long v4, v4, v2

    if-gez v4, :cond_3

    .line 806
    if-eqz v0, :cond_2

    .line 807
    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 809
    :cond_2
    int-to-long v4, v0

    invoke-virtual {p0, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 805
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 811
    :cond_3
    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 813
    const-string v0, " And "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 814
    invoke-virtual {p0}, Lio/realm/internal/Table;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 815
    const-string v0, " rows."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 817
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
