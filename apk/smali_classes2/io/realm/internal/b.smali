.class public final Lio/realm/internal/b;
.super Ljava/lang/Object;
.source "ColumnIndices.java"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lio/realm/internal/c/a",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/lang/String;",
            ">;",
            "Lio/realm/internal/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Lio/realm/internal/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/realm/internal/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z

.field private e:J


# direct methods
.method public constructor <init>(JLjava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Lio/realm/internal/c/a",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/lang/String;",
            ">;",
            "Lio/realm/internal/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lio/realm/internal/b;-><init>(JLjava/util/Map;Z)V

    .line 61
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 62
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/c;

    .line 63
    iget-boolean v3, p0, Lio/realm/internal/b;->d:Z

    invoke-virtual {v1}, Lio/realm/internal/c;->a()Z

    move-result v4

    if-eq v3, v4, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ColumnInfo mutability does not match ColumnIndices"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/c/a;

    .line 67
    iget-object v3, p0, Lio/realm/internal/b;->b:Ljava/util/Map;

    iget-object v4, v0, Lio/realm/internal/c/a;->a:Ljava/lang/Object;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v3, p0, Lio/realm/internal/b;->c:Ljava/util/Map;

    iget-object v0, v0, Lio/realm/internal/c/a;->b:Ljava/lang/Object;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 70
    :cond_1
    return-void
.end method

.method private constructor <init>(JLjava/util/Map;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Lio/realm/internal/c/a",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/lang/String;",
            ">;",
            "Lio/realm/internal/c;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-wide p1, p0, Lio/realm/internal/b;->e:J

    .line 91
    iput-object p3, p0, Lio/realm/internal/b;->a:Ljava/util/Map;

    .line 92
    iput-boolean p4, p0, Lio/realm/internal/b;->d:Z

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lio/realm/internal/b;->b:Ljava/util/Map;

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lio/realm/internal/b;->c:Ljava/util/Map;

    .line 95
    return-void
.end method

.method public constructor <init>(Lio/realm/internal/b;Z)V
    .locals 5

    .prologue
    .line 79
    iget-wide v0, p1, Lio/realm/internal/b;->e:J

    new-instance v2, Ljava/util/HashMap;

    iget-object v3, p1, Lio/realm/internal/b;->a:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    invoke-direct {p0, v0, v1, v2, p2}, Lio/realm/internal/b;-><init>(JLjava/util/Map;Z)V

    .line 80
    iget-object v0, p1, Lio/realm/internal/b;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 81
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/c;

    invoke-virtual {v1, p2}, Lio/realm/internal/c;->a(Z)Lio/realm/internal/c;

    move-result-object v1

    .line 82
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/c/a;

    .line 83
    iget-object v3, p0, Lio/realm/internal/b;->b:Ljava/util/Map;

    iget-object v4, v0, Lio/realm/internal/c/a;->a:Ljava/lang/Object;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v3, p0, Lio/realm/internal/b;->c:Ljava/util/Map;

    iget-object v4, v0, Lio/realm/internal/c/a;->b:Ljava/lang/Object;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v3, p0, Lio/realm/internal/b;->a:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 87
    :cond_0
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lio/realm/internal/b;->e:J

    return-wide v0
.end method

.method public a(Ljava/lang/Class;)Lio/realm/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;)",
            "Lio/realm/internal/c;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lio/realm/internal/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/c;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lio/realm/internal/b;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/c;

    return-object v0
.end method

.method public a(Lio/realm/internal/b;)V
    .locals 4

    .prologue
    .line 157
    iget-boolean v0, p0, Lio/realm/internal/b;->d:Z

    if-nez v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Attempt to modify immutable cache"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    iget-object v0, p0, Lio/realm/internal/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 161
    iget-object v1, p1, Lio/realm/internal/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/c;

    .line 162
    if-nez v1, :cond_1

    .line 163
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to copy ColumnIndices cache for class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 165
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/c;

    invoke-virtual {v0, v1}, Lio/realm/internal/c;->a(Lio/realm/internal/c;)V

    goto :goto_0

    .line 167
    :cond_2
    iget-wide v0, p1, Lio/realm/internal/b;->e:J

    iput-wide v0, p0, Lio/realm/internal/b;->e:J

    .line 168
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 172
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "ColumnIndices["

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 173
    iget-wide v0, p0, Lio/realm/internal/b;->e:J

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    iget-boolean v0, p0, Lio/realm/internal/b;->d:Z

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    iget-object v0, p0, Lio/realm/internal/b;->b:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 176
    const/4 v0, 0x0

    .line 177
    iget-object v1, p0, Lio/realm/internal/b;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 178
    if-eqz v1, :cond_0

    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "->"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 180
    const/4 v0, 0x1

    move v1, v0

    .line 181
    goto :goto_0

    .line 183
    :cond_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
