.class final enum Lio/realm/internal/SharedRealm$b;
.super Ljava/lang/Enum;
.source "SharedRealm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/SharedRealm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/internal/SharedRealm$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/realm/internal/SharedRealm$b;

.field public static final enum b:Lio/realm/internal/SharedRealm$b;

.field public static final enum c:Lio/realm/internal/SharedRealm$b;

.field public static final enum d:Lio/realm/internal/SharedRealm$b;

.field public static final enum e:Lio/realm/internal/SharedRealm$b;

.field private static final synthetic g:[Lio/realm/internal/SharedRealm$b;


# instance fields
.field final f:B


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89
    new-instance v0, Lio/realm/internal/SharedRealm$b;

    const-string v1, "SCHEMA_MODE_AUTOMATIC"

    invoke-direct {v0, v1, v2, v2}, Lio/realm/internal/SharedRealm$b;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/SharedRealm$b;->a:Lio/realm/internal/SharedRealm$b;

    .line 90
    new-instance v0, Lio/realm/internal/SharedRealm$b;

    const-string v1, "SCHEMA_MODE_READONLY"

    invoke-direct {v0, v1, v3, v3}, Lio/realm/internal/SharedRealm$b;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/SharedRealm$b;->b:Lio/realm/internal/SharedRealm$b;

    .line 91
    new-instance v0, Lio/realm/internal/SharedRealm$b;

    const-string v1, "SCHEMA_MODE_RESET_FILE"

    invoke-direct {v0, v1, v4, v4}, Lio/realm/internal/SharedRealm$b;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/SharedRealm$b;->c:Lio/realm/internal/SharedRealm$b;

    .line 92
    new-instance v0, Lio/realm/internal/SharedRealm$b;

    const-string v1, "SCHEMA_MODE_ADDITIVE"

    invoke-direct {v0, v1, v5, v5}, Lio/realm/internal/SharedRealm$b;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/SharedRealm$b;->d:Lio/realm/internal/SharedRealm$b;

    .line 93
    new-instance v0, Lio/realm/internal/SharedRealm$b;

    const-string v1, "SCHEMA_MODE_MANUAL"

    invoke-direct {v0, v1, v6, v6}, Lio/realm/internal/SharedRealm$b;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/SharedRealm$b;->e:Lio/realm/internal/SharedRealm$b;

    .line 88
    const/4 v0, 0x5

    new-array v0, v0, [Lio/realm/internal/SharedRealm$b;

    sget-object v1, Lio/realm/internal/SharedRealm$b;->a:Lio/realm/internal/SharedRealm$b;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/internal/SharedRealm$b;->b:Lio/realm/internal/SharedRealm$b;

    aput-object v1, v0, v3

    sget-object v1, Lio/realm/internal/SharedRealm$b;->c:Lio/realm/internal/SharedRealm$b;

    aput-object v1, v0, v4

    sget-object v1, Lio/realm/internal/SharedRealm$b;->d:Lio/realm/internal/SharedRealm$b;

    aput-object v1, v0, v5

    sget-object v1, Lio/realm/internal/SharedRealm$b;->e:Lio/realm/internal/SharedRealm$b;

    aput-object v1, v0, v6

    sput-object v0, Lio/realm/internal/SharedRealm$b;->g:[Lio/realm/internal/SharedRealm$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 98
    iput-byte p3, p0, Lio/realm/internal/SharedRealm$b;->f:B

    .line 99
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/internal/SharedRealm$b;
    .locals 1

    .prologue
    .line 88
    const-class v0, Lio/realm/internal/SharedRealm$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/internal/SharedRealm$b;

    return-object v0
.end method

.method public static values()[Lio/realm/internal/SharedRealm$b;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lio/realm/internal/SharedRealm$b;->g:[Lio/realm/internal/SharedRealm$b;

    invoke-virtual {v0}, [Lio/realm/internal/SharedRealm$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/internal/SharedRealm$b;

    return-object v0
.end method


# virtual methods
.method public a()B
    .locals 1

    .prologue
    .line 102
    iget-byte v0, p0, Lio/realm/internal/SharedRealm$b;->f:B

    return v0
.end method
