.class Lio/realm/internal/Collection$g;
.super Ljava/lang/Object;
.source "Collection.java"

# interfaces
.implements Lio/realm/am;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/realm/am",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lio/realm/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ax",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/realm/ax;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/ax",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lio/realm/internal/Collection$g;->a:Lio/realm/ax;

    .line 61
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lio/realm/al;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lio/realm/al;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lio/realm/internal/Collection$g;->a:Lio/realm/ax;

    invoke-interface {v0, p1}, Lio/realm/ax;->a(Ljava/lang/Object;)V

    .line 66
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 70
    instance-of v0, p1, Lio/realm/internal/Collection$g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/internal/Collection$g;->a:Lio/realm/ax;

    check-cast p1, Lio/realm/internal/Collection$g;

    iget-object v1, p1, Lio/realm/internal/Collection$g;->a:Lio/realm/ax;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lio/realm/internal/Collection$g;->a:Lio/realm/ax;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
