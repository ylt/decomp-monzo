.class public final enum Lio/realm/internal/e;
.super Ljava/lang/Enum;
.source "InvalidRow.java"

# interfaces
.implements Lio/realm/internal/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/internal/e;",
        ">;",
        "Lio/realm/internal/n;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/realm/internal/e;

.field private static final synthetic b:[Lio/realm/internal/e;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    new-instance v0, Lio/realm/internal/e;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lio/realm/internal/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/realm/internal/e;->a:Lio/realm/internal/e;

    .line 29
    const/4 v0, 0x1

    new-array v0, v0, [Lio/realm/internal/e;

    sget-object v1, Lio/realm/internal/e;->a:Lio/realm/internal/e;

    aput-object v1, v0, v2

    sput-object v0, Lio/realm/internal/e;->b:[Lio/realm/internal/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private e()Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 183
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Object is no longer managed by Realm. Has it been deleted?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/internal/e;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lio/realm/internal/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/internal/e;

    return-object v0
.end method

.method public static values()[Lio/realm/internal/e;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lio/realm/internal/e;->b:[Lio/realm/internal/e;

    invoke-virtual {v0}, [Lio/realm/internal/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/internal/e;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public a(Ljava/lang/String;)J
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public a(JD)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public a(JJ)V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public a(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public a(JLjava/util/Date;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public a(J)Z
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public b()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public b(JJ)V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public b(J)Z
    .locals 1

    .prologue
    .line 159
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public c()J
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public d(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method

.method public e(J)Lio/realm/RealmFieldType;
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public f(J)J
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public g(J)Z
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public h(J)F
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public i(J)D
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public j(J)Ljava/util/Date;
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public k(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public l(J)[B
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public m(J)J
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public n(J)Lio/realm/internal/LinkView;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public o(J)V
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Lio/realm/internal/e;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
