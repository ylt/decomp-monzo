.class public Lio/realm/internal/TableQuery;
.super Ljava/lang/Object;
.source "TableQuery.java"

# interfaces
.implements Lio/realm/internal/g;


# static fields
.field private static final a:J


# instance fields
.field private final b:Lio/realm/internal/f;

.field private final c:Lio/realm/internal/Table;

.field private final d:J

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    invoke-static {}, Lio/realm/internal/TableQuery;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/TableQuery;->a:J

    return-void
.end method

.method public constructor <init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 48
    iput-object p1, p0, Lio/realm/internal/TableQuery;->b:Lio/realm/internal/f;

    .line 49
    iput-object p2, p0, Lio/realm/internal/TableQuery;->c:Lio/realm/internal/Table;

    .line 50
    iput-wide p3, p0, Lio/realm/internal/TableQuery;->d:J

    .line 51
    invoke-virtual {p1, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 52
    return-void
.end method

.method private native nativeBetweenTimestamp(J[JJJ)V
.end method

.method private native nativeContains(J[J[JLjava/lang/String;Z)V
.end method

.method private native nativeCount(JJJJ)J
.end method

.method private native nativeEndGroup(J)V
.end method

.method private native nativeEqual(J[J[JLjava/lang/String;Z)V
.end method

.method private native nativeEqual(J[J[JZ)V
.end method

.method private native nativeFind(JJ)J
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private native nativeGreaterEqualTimestamp(J[J[JJ)V
.end method

.method private native nativeGroup(J)V
.end method

.method private native nativeIsNotNull(J[J[J)V
.end method

.method private native nativeIsNull(J[J[J)V
.end method

.method private native nativeLess(J[J[JJ)V
.end method

.method private native nativeLessEqualTimestamp(J[J[JJ)V
.end method

.method private native nativeMaximumTimestamp(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeNot(J)V
.end method

.method private native nativeNotEqual(J[J[JLjava/lang/String;Z)V
.end method

.method private native nativeOr(J)V
.end method

.method private native nativeSumDouble(JJJJJ)D
.end method

.method private native nativeSumFloat(JJJJJ)D
.end method

.method private native nativeSumInt(JJJJJ)J
.end method

.method private native nativeValidateQuery(J)Ljava/lang/String;
.end method


# virtual methods
.method public a(J)J
    .locals 13

    .prologue
    const-wide/16 v8, -0x1

    .line 432
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->b()V

    .line 433
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumInt(JJJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lio/realm/internal/TableQuery;->c:Lio/realm/internal/Table;

    return-object v0
.end method

.method public a([JLjava/util/Date;Ljava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 8

    .prologue
    .line 296
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 297
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date values in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_1
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lio/realm/internal/TableQuery;->nativeBetweenTimestamp(J[JJJ)V

    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 301
    return-object p0
.end method

.method public a([J[J)Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 590
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeIsNull(J[J[J)V

    .line 591
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 592
    return-object p0
.end method

.method public a([J[JJ)Lio/realm/internal/TableQuery;
    .locals 9

    .prologue
    .line 133
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableQuery;->nativeLess(J[J[JJ)V

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 135
    return-object p0
.end method

.method public a([J[JLjava/lang/String;Lio/realm/l;)Lio/realm/internal/TableQuery;
    .locals 8

    .prologue
    .line 320
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-virtual {p4}, Lio/realm/l;->a()Z

    move-result v7

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableQuery;->nativeEqual(J[J[JLjava/lang/String;Z)V

    .line 321
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 322
    return-object p0
.end method

.method public a([J[JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 8

    .prologue
    .line 275
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableQuery;->nativeGreaterEqualTimestamp(J[J[JJ)V

    .line 277
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 278
    return-object p0
.end method

.method public a([J[JZ)Lio/realm/internal/TableQuery;
    .locals 7

    .prologue
    .line 241
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEqual(J[J[JZ)V

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 243
    return-object p0
.end method

.method public b(J)D
    .locals 13

    .prologue
    const-wide/16 v8, -0x1

    .line 474
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->b()V

    .line 475
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public b([J[J)Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 596
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeIsNotNull(J[J[J)V

    .line 597
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 598
    return-object p0
.end method

.method public b([J[JLjava/lang/String;Lio/realm/l;)Lio/realm/internal/TableQuery;
    .locals 8

    .prologue
    .line 333
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-virtual {p4}, Lio/realm/l;->a()Z

    move-result v7

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[J[JLjava/lang/String;Z)V

    .line 334
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 335
    return-object p0
.end method

.method public b([J[JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 8

    .prologue
    .line 289
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableQuery;->nativeLessEqualTimestamp(J[J[JJ)V

    .line 291
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 292
    return-object p0
.end method

.method b()V
    .locals 2

    .prologue
    .line 72
    iget-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    if-nez v0, :cond_0

    .line 73
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeValidateQuery(J)Ljava/lang/String;

    move-result-object v0

    .line 74
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 78
    :cond_0
    return-void

    .line 76
    :cond_1
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public c(J)D
    .locals 13

    .prologue
    const-wide/16 v8, -0x1

    .line 516
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->b()V

    .line 517
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public c()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeGroup(J)V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 85
    return-object p0
.end method

.method public c([J[JLjava/lang/String;Lio/realm/l;)Lio/realm/internal/TableQuery;
    .locals 8

    .prologue
    .line 381
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-virtual {p4}, Lio/realm/l;->a()Z

    move-result v7

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/TableQuery;->nativeContains(J[J[JLjava/lang/String;Z)V

    .line 382
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 383
    return-object p0
.end method

.method public d()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeEndGroup(J)V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 91
    return-object p0
.end method

.method public d(J)Ljava/util/Date;
    .locals 13

    .prologue
    const-wide/16 v8, -0x1

    .line 562
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->b()V

    .line 563
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v1

    .line 564
    if-eqz v1, :cond_0

    .line 565
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 567
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeOr(J)V

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 97
    return-object p0
.end method

.method public f()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 101
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeNot(J)V

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->e:Z

    .line 103
    return-object p0
.end method

.method public g()J
    .locals 4

    .prologue
    .line 416
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->b()V

    .line 417
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/TableQuery;->nativeFind(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 61
    sget-wide v0, Lio/realm/internal/TableQuery;->a:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->d:J

    return-wide v0
.end method

.method public h()J
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    .line 610
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->b()V

    .line 611
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->d:J

    const-wide/16 v4, 0x0

    move-object v1, p0

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeCount(JJJJ)J

    move-result-wide v0

    return-wide v0
.end method
