.class public Lio/realm/internal/SortDescriptor;
.super Ljava/lang/Object;
.source "SortDescriptor.java"


# annotations
.annotation build Lio/realm/internal/KeepMember;
.end annotation


# static fields
.field static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lio/realm/internal/Table;

.field private final d:[[J

.field private final e:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 43
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Lio/realm/RealmFieldType;

    sget-object v2, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    aput-object v2, v1, v4

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v2, v1, v5

    sget-object v2, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v2, v1, v6

    sget-object v2, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v2, v1, v7

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lio/realm/internal/SortDescriptor;->a:Ljava/util/Set;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v8, [Lio/realm/RealmFieldType;

    sget-object v2, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    aput-object v2, v1, v4

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v2, v1, v5

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v2, v1, v6

    sget-object v2, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v2, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lio/realm/internal/SortDescriptor;->b:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Lio/realm/internal/Table;[[J[Lio/realm/bl;)V
    .locals 3

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lio/realm/internal/SortDescriptor;->c:Lio/realm/internal/Table;

    .line 119
    iput-object p2, p0, Lio/realm/internal/SortDescriptor;->d:[[J

    .line 120
    if-eqz p3, :cond_0

    .line 121
    array-length v0, p3

    new-array v0, v0, [Z

    iput-object v0, p0, Lio/realm/internal/SortDescriptor;->e:[Z

    .line 122
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 123
    iget-object v1, p0, Lio/realm/internal/SortDescriptor;->e:[Z

    aget-object v2, p3, v0

    invoke-virtual {v2}, Lio/realm/bl;->a()Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/internal/SortDescriptor;->e:[Z

    .line 128
    :cond_1
    return-void
.end method

.method public static a(Lio/realm/internal/a/c$a;Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/bl;)Lio/realm/internal/SortDescriptor;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    new-array v0, v1, [Ljava/lang/String;

    aput-object p2, v0, v2

    new-array v1, v1, [Lio/realm/bl;

    aput-object p3, v1, v2

    invoke-static {p0, p1, v0, v1}, Lio/realm/internal/SortDescriptor;->a(Lio/realm/internal/a/c$a;Lio/realm/internal/Table;[Ljava/lang/String;[Lio/realm/bl;)Lio/realm/internal/SortDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/realm/internal/a/c$a;Lio/realm/internal/Table;[Ljava/lang/String;[Lio/realm/bl;)Lio/realm/internal/SortDescriptor;
    .locals 7

    .prologue
    .line 56
    if-eqz p3, :cond_0

    array-length v0, p3

    if-nez v0, :cond_1

    .line 57
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must provide at least one sort order."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_1
    array-length v0, p2

    array-length v1, p3

    if-eq v0, v1, :cond_2

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of fields and sort orders do not match."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_2
    sget-object v4, Lio/realm/internal/a/c;->d:Ljava/util/Set;

    sget-object v5, Lio/realm/internal/SortDescriptor;->a:Ljava/util/Set;

    const-string v6, "Sort is not supported"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lio/realm/internal/SortDescriptor;->a(Lio/realm/internal/a/c$a;Lio/realm/internal/Table;[Ljava/lang/String;[Lio/realm/bl;Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;)Lio/realm/internal/SortDescriptor;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lio/realm/internal/a/c$a;Lio/realm/internal/Table;[Ljava/lang/String;[Lio/realm/bl;Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;)Lio/realm/internal/SortDescriptor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/a/c$a;",
            "Lio/realm/internal/Table;",
            "[",
            "Ljava/lang/String;",
            "[",
            "Lio/realm/bl;",
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/internal/SortDescriptor;"
        }
    .end annotation

    .prologue
    .line 82
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 83
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must provide at least one field name."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_1
    array-length v0, p2

    new-array v1, v0, [[J

    .line 89
    const/4 v0, 0x0

    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_2

    .line 90
    aget-object v2, p2, v0

    const/4 v3, 0x0

    invoke-static {p0, p1, v2, p4, v3}, Lio/realm/internal/a/c;->a(Lio/realm/internal/a/c$a;Lio/realm/internal/Table;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lio/realm/internal/a/c;

    move-result-object v2

    .line 91
    aget-object v3, p2, v0

    invoke-static {v2, p5, p6, v3}, Lio/realm/internal/SortDescriptor;->a(Lio/realm/internal/a/c;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual {v2}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    aput-object v2, v1, v0

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_2
    new-instance v0, Lio/realm/internal/SortDescriptor;

    invoke-direct {v0, p1, v1, p3}, Lio/realm/internal/SortDescriptor;-><init>(Lio/realm/internal/Table;[[J[Lio/realm/bl;)V

    return-object v0
.end method

.method private static a(Lio/realm/internal/a/c;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/a/c;",
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    invoke-virtual {p0}, Lio/realm/internal/a/c;->e()Lio/realm/RealmFieldType;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s on \'%s\' field \'%s\' in \'%s\'."

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    .line 108
    invoke-virtual {p0}, Lio/realm/internal/a/c;->e()Lio/realm/RealmFieldType;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p0}, Lio/realm/internal/a/c;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p3, v3, v4

    .line 107
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    return-void
.end method

.method private getTablePtr()J
    .locals 2
    .annotation build Lio/realm/internal/KeepMember;
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lio/realm/internal/SortDescriptor;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method getAscendings()[Z
    .locals 1
    .annotation build Lio/realm/internal/KeepMember;
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lio/realm/internal/SortDescriptor;->e:[Z

    return-object v0
.end method

.method getColumnIndices()[[J
    .locals 1
    .annotation build Lio/realm/internal/KeepMember;
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lio/realm/internal/SortDescriptor;->d:[[J

    return-object v0
.end method
