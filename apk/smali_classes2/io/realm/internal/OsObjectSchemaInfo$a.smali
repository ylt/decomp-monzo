.class public Lio/realm/internal/OsObjectSchemaInfo$a;
.super Ljava/lang/Object;
.source "OsObjectSchemaInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/OsObjectSchemaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/realm/internal/Property;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/internal/OsObjectSchemaInfo$a;->b:Ljava/util/List;

    .line 42
    iput-object p1, p0, Lio/realm/internal/OsObjectSchemaInfo$a;->a:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lio/realm/internal/Property;

    invoke-direct {v0, p1, p2, p3}, Lio/realm/internal/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lio/realm/internal/OsObjectSchemaInfo$a;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-object p0
.end method

.method public a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;
    .locals 6

    .prologue
    .line 57
    new-instance v0, Lio/realm/internal/Property;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    .line 58
    iget-object v1, p0, Lio/realm/internal/OsObjectSchemaInfo$a;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    return-object p0
.end method

.method public a()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 8

    .prologue
    .line 76
    new-instance v1, Lio/realm/internal/OsObjectSchemaInfo;

    iget-object v0, p0, Lio/realm/internal/OsObjectSchemaInfo$a;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lio/realm/internal/OsObjectSchemaInfo;-><init>(Ljava/lang/String;Lio/realm/internal/OsObjectSchemaInfo$1;)V

    .line 77
    iget-object v0, p0, Lio/realm/internal/OsObjectSchemaInfo$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Property;

    .line 78
    invoke-static {v1}, Lio/realm/internal/OsObjectSchemaInfo;->a(Lio/realm/internal/OsObjectSchemaInfo;)J

    move-result-wide v4

    invoke-virtual {v0}, Lio/realm/internal/Property;->getNativePtr()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lio/realm/internal/OsObjectSchemaInfo;->a(JJ)V

    goto :goto_0

    .line 81
    :cond_0
    return-object v1
.end method
