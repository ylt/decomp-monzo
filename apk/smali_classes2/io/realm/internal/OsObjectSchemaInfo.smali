.class public Lio/realm/internal/OsObjectSchemaInfo;
.super Ljava/lang/Object;
.source "OsObjectSchemaInfo.java"

# interfaces
.implements Lio/realm/internal/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/OsObjectSchemaInfo$a;
    }
.end annotation


# static fields
.field private static final b:J


# instance fields
.field private a:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 86
    invoke-static {}, Lio/realm/internal/OsObjectSchemaInfo;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/OsObjectSchemaInfo;->b:J

    return-void
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-wide p1, p0, Lio/realm/internal/OsObjectSchemaInfo;->a:J

    .line 106
    sget-object v0, Lio/realm/internal/f;->a:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 107
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 95
    invoke-static {p1}, Lio/realm/internal/OsObjectSchemaInfo;->nativeCreateRealmObjectSchema(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lio/realm/internal/OsObjectSchemaInfo;-><init>(J)V

    .line 96
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lio/realm/internal/OsObjectSchemaInfo$1;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lio/realm/internal/OsObjectSchemaInfo;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lio/realm/internal/OsObjectSchemaInfo;)J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lio/realm/internal/OsObjectSchemaInfo;->a:J

    return-wide v0
.end method

.method static synthetic a(JJ)V
    .locals 0

    .prologue
    .line 29
    invoke-static {p0, p1, p2, p3}, Lio/realm/internal/OsObjectSchemaInfo;->nativeAddProperty(JJ)V

    return-void
.end method

.method private static native nativeAddProperty(JJ)V
.end method

.method private static native nativeCreateRealmObjectSchema(Ljava/lang/String;)J
.end method

.method private static native nativeGetFinalizerPtr()J
.end method


# virtual methods
.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 123
    sget-wide v0, Lio/realm/internal/OsObjectSchemaInfo;->b:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lio/realm/internal/OsObjectSchemaInfo;->a:J

    return-wide v0
.end method
