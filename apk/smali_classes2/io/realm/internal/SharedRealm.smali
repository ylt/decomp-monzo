.class public final Lio/realm/internal/SharedRealm;
.super Ljava/lang/Object;
.source "SharedRealm.java"

# interfaces
.implements Lio/realm/internal/g;
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/SharedRealm$c;,
        Lio/realm/internal/SharedRealm$b;,
        Lio/realm/internal/SharedRealm$a;
    }
.end annotation


# static fields
.field private static final f:J

.field private static volatile g:Ljava/io/File;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/internal/Collection;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/internal/Collection$d;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Lio/realm/internal/RealmNotifier;

.field public final d:Lio/realm/internal/a;

.field final e:Lio/realm/internal/f;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/internal/j;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Lio/realm/internal/SharedRealm$c;

.field private final j:Lio/realm/ay;

.field private final k:J

.field private l:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Lio/realm/internal/SharedRealm;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/SharedRealm;->f:J

    return-void
.end method

.method private constructor <init>(JLio/realm/ay;Lio/realm/internal/SharedRealm$c;)V
    .locals 7

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/internal/SharedRealm;->h:Ljava/util/List;

    .line 107
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/internal/SharedRealm;->a:Ljava/util/List;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/internal/SharedRealm;->b:Ljava/util/List;

    .line 183
    new-instance v2, Lio/realm/internal/android/a;

    invoke-direct {v2}, Lio/realm/internal/android/a;-><init>()V

    .line 184
    new-instance v0, Lio/realm/internal/android/AndroidRealmNotifier;

    invoke-direct {v0, p0, v2}, Lio/realm/internal/android/AndroidRealmNotifier;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/a;)V

    .line 186
    invoke-static {p1, p2, v0}, Lio/realm/internal/SharedRealm;->nativeGetSharedRealm(JLio/realm/internal/RealmNotifier;)J

    move-result-wide v4

    iput-wide v4, p0, Lio/realm/internal/SharedRealm;->k:J

    .line 187
    iput-object p3, p0, Lio/realm/internal/SharedRealm;->j:Lio/realm/ay;

    .line 189
    iput-object v2, p0, Lio/realm/internal/SharedRealm;->d:Lio/realm/internal/a;

    .line 190
    iput-object v0, p0, Lio/realm/internal/SharedRealm;->c:Lio/realm/internal/RealmNotifier;

    .line 191
    iput-object p4, p0, Lio/realm/internal/SharedRealm;->i:Lio/realm/internal/SharedRealm$c;

    .line 192
    new-instance v0, Lio/realm/internal/f;

    invoke-direct {v0}, Lio/realm/internal/f;-><init>()V

    iput-object v0, p0, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    .line 193
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 194
    if-nez p4, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    iput-wide v0, p0, Lio/realm/internal/SharedRealm;->l:J

    .line 195
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-interface {v2}, Lio/realm/internal/a;->a()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lio/realm/internal/SharedRealm;->nativeSetAutoRefresh(JZ)V

    .line 196
    return-void

    .line 194
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->e()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Lio/realm/ay;)Lio/realm/internal/SharedRealm;
    .locals 2

    .prologue
    .line 202
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lio/realm/internal/SharedRealm;->a(Lio/realm/ay;Lio/realm/internal/SharedRealm$c;Z)Lio/realm/internal/SharedRealm;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/realm/ay;Lio/realm/internal/SharedRealm$c;Z)Lio/realm/internal/SharedRealm;
    .locals 19

    .prologue
    .line 208
    invoke-static {}, Lio/realm/internal/h;->a()Lio/realm/internal/h;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lio/realm/internal/h;->b(Lio/realm/ay;)[Ljava/lang/Object;

    move-result-object v2

    .line 209
    const/4 v3, 0x0

    aget-object v15, v2, v3

    check-cast v15, Ljava/lang/String;

    .line 210
    const/4 v3, 0x1

    aget-object v13, v2, v3

    check-cast v13, Ljava/lang/String;

    .line 211
    const/4 v3, 0x2

    aget-object v14, v2, v3

    check-cast v14, Ljava/lang/String;

    .line 212
    const/4 v3, 0x3

    aget-object v16, v2, v3

    check-cast v16, Ljava/lang/String;

    .line 213
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const/4 v4, 0x4

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v17

    .line 214
    const/4 v3, 0x5

    aget-object v18, v2, v3

    check-cast v18, Ljava/lang/String;

    .line 220
    invoke-virtual/range {p0 .. p0}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v3

    .line 221
    invoke-virtual/range {p0 .. p0}, Lio/realm/ay;->c()[B

    move-result-object v4

    .line 222
    if-eqz v13, :cond_0

    sget-object v2, Lio/realm/internal/SharedRealm$b;->d:Lio/realm/internal/SharedRealm$b;

    invoke-virtual {v2}, Lio/realm/internal/SharedRealm$b;->a()B

    move-result v5

    .line 223
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lio/realm/ay;->g()Lio/realm/internal/SharedRealm$a;

    move-result-object v2

    sget-object v6, Lio/realm/internal/SharedRealm$a;->b:Lio/realm/internal/SharedRealm$a;

    if-ne v2, v6, :cond_1

    const/4 v6, 0x1

    :goto_1
    const/4 v7, 0x0

    .line 225
    invoke-virtual/range {p0 .. p0}, Lio/realm/ay;->d()J

    move-result-wide v8

    const/4 v10, 0x1

    .line 228
    invoke-virtual/range {p0 .. p0}, Lio/realm/ay;->l()Lio/realm/CompactOnLaunchCallback;

    move-result-object v12

    move/from16 v11, p2

    .line 219
    invoke-static/range {v3 .. v18}, Lio/realm/internal/SharedRealm;->nativeCreateConfig(Ljava/lang/String;[BBZZJZZLio/realm/CompactOnLaunchCallback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)J

    move-result-wide v4

    .line 237
    :try_start_0
    invoke-static {}, Lio/realm/internal/h;->a()Lio/realm/internal/h;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lio/realm/internal/h;->c(Lio/realm/ay;)V

    .line 239
    new-instance v2, Lio/realm/internal/SharedRealm;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v4, v5, v0, v1}, Lio/realm/internal/SharedRealm;-><init>(JLio/realm/ay;Lio/realm/internal/SharedRealm$c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    invoke-static {v4, v5}, Lio/realm/internal/SharedRealm;->nativeCloseConfig(J)V

    .line 239
    return-object v2

    .line 222
    :cond_0
    sget-object v2, Lio/realm/internal/SharedRealm$b;->e:Lio/realm/internal/SharedRealm$b;

    invoke-virtual {v2}, Lio/realm/internal/SharedRealm$b;->a()B

    move-result v5

    goto :goto_0

    .line 223
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 241
    :catchall_0
    move-exception v2

    invoke-static {v4, v5}, Lio/realm/internal/SharedRealm;->nativeCloseConfig(J)V

    throw v2
.end method

.method public static a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 45
    sget-object v0, Lio/realm/internal/SharedRealm;->g:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 63
    :goto_0
    return-void

    .line 49
    :cond_0
    if-nez p0, :cond_1

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'tempDirectory\' must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    .line 55
    new-instance v1, Lio/realm/internal/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to create temporary directory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lio/realm/internal/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_2
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    :cond_3
    invoke-static {v0}, Lio/realm/internal/SharedRealm;->nativeInit(Ljava/lang/String;)V

    .line 62
    sput-object p0, Lio/realm/internal/SharedRealm;->g:Ljava/io/File;

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 494
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/j;

    .line 495
    if-eqz v0, :cond_0

    .line 496
    invoke-virtual {v0}, Lio/realm/internal/j;->e()V

    goto :goto_0

    .line 499
    :cond_1
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 500
    return-void
.end method

.method private static native nativeBeginTransaction(J)V
.end method

.method private static native nativeCancelTransaction(J)V
.end method

.method private static native nativeCloseConfig(J)V
.end method

.method private static native nativeCloseSharedRealm(J)V
.end method

.method private static native nativeCommitTransaction(J)V
.end method

.method private static native nativeCreateConfig(Ljava/lang/String;[BBZZJZZLio/realm/CompactOnLaunchCallback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)J
.end method

.method private static native nativeCreateTable(JLjava/lang/String;)J
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private static native nativeGetSharedRealm(JLio/realm/internal/RealmNotifier;)J
.end method

.method private static native nativeGetTable(JLjava/lang/String;)J
.end method

.method private static native nativeGetTableName(JI)Ljava/lang/String;
.end method

.method private static native nativeGetVersion(J)J
.end method

.method private static native nativeHasTable(JLjava/lang/String;)Z
.end method

.method private static native nativeInit(Ljava/lang/String;)V
.end method

.method private static native nativeIsClosed(J)Z
.end method

.method private static native nativeIsInTransaction(J)Z
.end method

.method private static native nativeReadGroup(J)J
.end method

.method private static native nativeSetAutoRefresh(JZ)V
.end method

.method private static native nativeSetVersion(JJ)V
.end method

.method private static native nativeSize(J)J
.end method

.method private static native nativeUpdateSchema(JJJ)V
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 322
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1, p1}, Lio/realm/internal/SharedRealm;->nativeGetTableName(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Z)V

    .line 247
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 274
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/SharedRealm;->nativeSetVersion(JJ)V

    .line 275
    return-void
.end method

.method a(Lio/realm/internal/Collection$d;)V
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->b:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 446
    return-void
.end method

.method public a(Lio/realm/internal/OsSchemaInfo;J)V
    .locals 6

    .prologue
    .line 378
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-virtual {p1}, Lio/realm/internal/OsSchemaInfo;->getNativePtr()J

    move-result-wide v2

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lio/realm/internal/SharedRealm;->nativeUpdateSchema(JJJ)V

    .line 379
    return-void
.end method

.method a(Lio/realm/internal/j;)V
    .locals 3

    .prologue
    .line 483
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 484
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/j;

    .line 485
    if-eqz v1, :cond_1

    if-ne v1, p1, :cond_0

    .line 486
    :cond_1
    iget-object v1, p0, Lio/realm/internal/SharedRealm;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 489
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 252
    if-nez p1, :cond_0

    iget-object v0, p0, Lio/realm/internal/SharedRealm;->j:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Write transactions cannot be used when a Realm is marked as read-only."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->k()V

    .line 256
    invoke-direct {p0}, Lio/realm/internal/SharedRealm;->m()V

    .line 257
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1}, Lio/realm/internal/SharedRealm;->nativeBeginTransaction(J)V

    .line 258
    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->j()V

    .line 259
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 287
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1, p1}, Lio/realm/internal/SharedRealm;->nativeHasTable(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Lio/realm/internal/Table;
    .locals 3

    .prologue
    .line 298
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1, p1}, Lio/realm/internal/SharedRealm;->nativeGetTable(JLjava/lang/String;)J

    move-result-wide v0

    .line 299
    new-instance v2, Lio/realm/internal/Table;

    invoke-direct {v2, p0, v0, v1}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/SharedRealm;J)V

    return-object v2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 262
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1}, Lio/realm/internal/SharedRealm;->nativeCommitTransaction(J)V

    .line 263
    return-void
.end method

.method public c(Ljava/lang/String;)Lio/realm/internal/Table;
    .locals 4

    .prologue
    .line 310
    new-instance v0, Lio/realm/internal/Table;

    iget-wide v2, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v2, v3, p1}, Lio/realm/internal/SharedRealm;->nativeCreateTable(JLjava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/SharedRealm;J)V

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 266
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1}, Lio/realm/internal/SharedRealm;->nativeCancelTransaction(J)V

    .line 267
    return-void
.end method

.method public close()V
    .locals 4

    .prologue
    .line 407
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->c:Lio/realm/internal/RealmNotifier;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->c:Lio/realm/internal/RealmNotifier;

    invoke-virtual {v0}, Lio/realm/internal/RealmNotifier;->close()V

    .line 410
    :cond_0
    iget-object v1, p0, Lio/realm/internal/SharedRealm;->e:Lio/realm/internal/f;

    monitor-enter v1

    .line 411
    :try_start_0
    iget-wide v2, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v2, v3}, Lio/realm/internal/SharedRealm;->nativeCloseSharedRealm(J)V

    .line 414
    monitor-exit v1

    .line 415
    return-void

    .line 414
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 270
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1}, Lio/realm/internal/SharedRealm;->nativeIsInTransaction(J)Z

    move-result v0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 278
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1}, Lio/realm/internal/SharedRealm;->nativeGetVersion(J)J

    move-result-wide v0

    return-wide v0
.end method

.method f()J
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1}, Lio/realm/internal/SharedRealm;->nativeReadGroup(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 326
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1}, Lio/realm/internal/SharedRealm;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 424
    sget-wide v0, Lio/realm/internal/SharedRealm;->f:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 419
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    return-wide v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->j:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 348
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->k:J

    invoke-static {v0, v1}, Lio/realm/internal/SharedRealm;->nativeIsClosed(J)Z

    move-result v0

    return v0
.end method

.method public j()V
    .locals 4

    .prologue
    .line 428
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->i:Lio/realm/internal/SharedRealm$c;

    if-nez v0, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 432
    :cond_1
    iget-wide v0, p0, Lio/realm/internal/SharedRealm;->l:J

    .line 433
    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->e()J

    move-result-wide v2

    .line 434
    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 435
    iput-wide v2, p0, Lio/realm/internal/SharedRealm;->l:J

    .line 436
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->i:Lio/realm/internal/SharedRealm$c;

    invoke-interface {v0, v2, v3}, Lio/realm/internal/SharedRealm$c;->a(J)V

    goto :goto_0
.end method

.method k()V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 451
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Collection$d;

    .line 452
    if-eqz v0, :cond_0

    .line 453
    invoke-virtual {v0}, Lio/realm/internal/Collection$d;->a()V

    goto :goto_0

    .line 456
    :cond_1
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 457
    return-void
.end method

.method l()V
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 462
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Collection$d;

    .line 463
    if-eqz v0, :cond_0

    .line 464
    invoke-virtual {v0}, Lio/realm/internal/Collection$d;->b()V

    goto :goto_0

    .line 467
    :cond_1
    iget-object v0, p0, Lio/realm/internal/SharedRealm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 468
    return-void
.end method
