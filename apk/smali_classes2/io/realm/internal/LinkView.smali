.class public Lio/realm/internal/LinkView;
.super Ljava/lang/Object;
.source "LinkView.java"

# interfaces
.implements Lio/realm/internal/g;


# static fields
.field private static final e:J


# instance fields
.field final a:Lio/realm/internal/Table;

.field final b:J

.field private final c:Lio/realm/internal/f;

.field private final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    invoke-static {}, Lio/realm/internal/LinkView;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/LinkView;->e:J

    return-void
.end method

.method public constructor <init>(Lio/realm/internal/f;Lio/realm/internal/Table;JJ)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lio/realm/internal/LinkView;->c:Lio/realm/internal/f;

    .line 35
    iput-object p2, p0, Lio/realm/internal/LinkView;->a:Lio/realm/internal/Table;

    .line 36
    iput-wide p3, p0, Lio/realm/internal/LinkView;->b:J

    .line 37
    iput-wide p5, p0, Lio/realm/internal/LinkView;->d:J

    .line 39
    invoke-virtual {p1, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 40
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lio/realm/internal/LinkView;->a:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Changing Realm data can only be done from inside a write transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    return-void
.end method

.method public static native nativeAdd(JJ)V
.end method

.method public static native nativeClear(J)V
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private native nativeGetTargetRowIndex(JJ)J
.end method

.method private native nativeGetTargetTable(J)J
.end method

.method private native nativeInsert(JJJ)V
.end method

.method private native nativeIsAttached(J)Z
.end method

.method private native nativeIsEmpty(J)Z
.end method

.method private native nativeRemove(JJ)V
.end method

.method private native nativeSet(JJJ)V
.end method

.method private native nativeSize(J)J
.end method


# virtual methods
.method public a(J)J
    .locals 3

    .prologue
    .line 82
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/LinkView;->nativeGetTargetRowIndex(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0}, Lio/realm/internal/LinkView;->f()V

    .line 112
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    invoke-static {v0, v1}, Lio/realm/internal/LinkView;->nativeClear(J)V

    .line 113
    return-void
.end method

.method public a(JJ)V
    .locals 9

    .prologue
    .line 91
    invoke-direct {p0}, Lio/realm/internal/LinkView;->f()V

    .line 92
    iget-wide v2, p0, Lio/realm/internal/LinkView;->d:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/LinkView;->nativeInsert(JJJ)V

    .line 93
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0}, Lio/realm/internal/LinkView;->f()V

    .line 87
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/LinkView;->nativeAdd(JJ)V

    .line 88
    return-void
.end method

.method public b(JJ)V
    .locals 9

    .prologue
    .line 96
    invoke-direct {p0}, Lio/realm/internal/LinkView;->f()V

    .line 97
    iget-wide v2, p0, Lio/realm/internal/LinkView;->d:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/LinkView;->nativeSet(JJJ)V

    .line 98
    return-void
.end method

.method public c(J)V
    .locals 3

    .prologue
    .line 106
    invoke-direct {p0}, Lio/realm/internal/LinkView;->f()V

    .line 107
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/LinkView;->nativeRemove(JJ)V

    .line 108
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeIsEmpty(J)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 134
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeIsAttached(J)Z

    move-result v0

    return v0
.end method

.method public e()Lio/realm/internal/Table;
    .locals 4

    .prologue
    .line 154
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/LinkView;->nativeGetTargetTable(J)J

    move-result-wide v0

    .line 155
    new-instance v2, Lio/realm/internal/Table;

    iget-object v3, p0, Lio/realm/internal/LinkView;->a:Lio/realm/internal/Table;

    invoke-direct {v2, v3, v0, v1}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/Table;J)V

    return-object v2
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 50
    sget-wide v0, Lio/realm/internal/LinkView;->e:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lio/realm/internal/LinkView;->d:J

    return-wide v0
.end method
