.class Lio/realm/internal/a/b;
.super Lio/realm/internal/a/c;
.source "DynamicFieldDescriptor.java"


# instance fields
.field private final f:Lio/realm/internal/Table;


# direct methods
.method constructor <init>(Lio/realm/internal/Table;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/Table;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p2, p3, p4}, Lio/realm/internal/a/c;-><init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V

    .line 43
    iput-object p1, p0, Lio/realm/internal/a/b;->f:Lio/realm/internal/Table;

    .line 44
    return-void
.end method


# virtual methods
.method protected a(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 48
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    .line 49
    new-array v4, v9, [J

    .line 50
    iget-object v5, p0, Lio/realm/internal/a/b;->f:Lio/realm/internal/Table;

    move v6, v7

    move-object v2, v3

    move-object v1, v3

    .line 56
    :goto_0
    if-ge v6, v9, :cond_3

    .line 57
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 58
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid query: Field descriptor contains an empty field.  A field description may not begin with or contain adjacent periods (\'.\')."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_1
    invoke-virtual {v5}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v8

    .line 65
    invoke-virtual {v5, v0}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v10

    .line 66
    const-wide/16 v2, 0x0

    cmp-long v1, v10, v2

    if-gez v1, :cond_2

    .line 67
    new-instance v1, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Invalid query: field \'%s\' not found in table \'%s\'."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v7

    const/4 v0, 0x1

    aput-object v8, v4, v0

    .line 68
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 71
    :cond_2
    invoke-virtual {v5, v10, v11}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v3

    .line 72
    add-int/lit8 v1, v9, -0x1

    if-ge v6, v1, :cond_4

    .line 73
    invoke-virtual {p0, v8, v0, v3}, Lio/realm/internal/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lio/realm/RealmFieldType;)V

    .line 74
    invoke-virtual {v5, v10, v11}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v1

    .line 77
    :goto_1
    aput-wide v10, v4, v6

    .line 56
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-object v5, v1

    move-object v1, v8

    move-object v2, v0

    goto :goto_0

    .line 80
    :cond_3
    new-array v5, v9, [J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lio/realm/RealmFieldType;[J[J)V

    .line 81
    return-void

    :cond_4
    move-object v1, v5

    goto :goto_1
.end method
