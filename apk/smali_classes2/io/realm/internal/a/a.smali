.class Lio/realm/internal/a/a;
.super Lio/realm/internal/a/c;
.source "CachedFieldDescriptor.java"


# instance fields
.field private final f:Lio/realm/internal/a/c$a;

.field private final g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lio/realm/internal/a/c$a;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/a/c$a;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p3, p4, p5}, Lio/realm/internal/a/c;-><init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V

    .line 48
    iput-object p2, p0, Lio/realm/internal/a/a;->g:Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lio/realm/internal/a/a;->f:Lio/realm/internal/a/c$a;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    .line 55
    new-array v4, v8, [J

    .line 56
    new-array v5, v8, [J

    .line 57
    iget-object v1, p0, Lio/realm/internal/a/a;->g:Ljava/lang/String;

    .line 60
    const/4 v2, 0x0

    .line 61
    const/4 v3, 0x0

    .line 63
    const/4 v0, 0x0

    move-object v7, v1

    move v1, v0

    :goto_0
    if-ge v1, v8, :cond_6

    .line 64
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 65
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 66
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid query: Field descriptor contains an empty field.  A field description may not begin with or contain adjacent periods (\'.\')."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_1
    iget-object v2, p0, Lio/realm/internal/a/a;->f:Lio/realm/internal/a/c$a;

    invoke-interface {v2, v7}, Lio/realm/internal/a/c$a;->a(Ljava/lang/String;)Lio/realm/internal/c;

    move-result-object v2

    .line 71
    if-nez v2, :cond_2

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Invalid query: table \'%s\' not found in this schema."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    .line 73
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_2
    invoke-virtual {v2, v0}, Lio/realm/internal/c;->a(Ljava/lang/String;)J

    move-result-wide v10

    .line 77
    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-gez v3, :cond_3

    .line 78
    new-instance v1, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Invalid query: field \'%s\' not found in table \'%s\'."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v7, v4, v0

    .line 79
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 82
    :cond_3
    invoke-virtual {v2, v0}, Lio/realm/internal/c;->b(Ljava/lang/String;)Lio/realm/RealmFieldType;

    move-result-object v6

    .line 84
    add-int/lit8 v3, v8, -0x1

    if-ge v1, v3, :cond_4

    .line 85
    invoke-virtual {p0, v7, v0, v6}, Lio/realm/internal/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lio/realm/RealmFieldType;)V

    .line 87
    :cond_4
    invoke-virtual {v2, v0}, Lio/realm/internal/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 88
    aput-wide v10, v4, v1

    .line 89
    sget-object v2, Lio/realm/RealmFieldType;->LINKING_OBJECTS:Lio/realm/RealmFieldType;

    if-eq v6, v2, :cond_5

    .line 90
    const-wide/16 v2, 0x0

    .line 91
    :goto_1
    aput-wide v2, v5, v1

    .line 63
    add-int/lit8 v1, v1, 0x1

    move-object v3, v6

    move-object v2, v0

    goto :goto_0

    .line 91
    :cond_5
    iget-object v2, p0, Lio/realm/internal/a/a;->f:Lio/realm/internal/a/c$a;

    invoke-interface {v2, v7}, Lio/realm/internal/a/c$a;->b(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_1

    .line 94
    :cond_6
    iget-object v1, p0, Lio/realm/internal/a/a;->g:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lio/realm/RealmFieldType;[J[J)V

    .line 95
    return-void
.end method
