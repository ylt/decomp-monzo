.class Lio/realm/internal/Collection$c;
.super Lio/realm/internal/i$b;
.source "Collection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/realm/internal/i$b",
        "<TT;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lio/realm/internal/i$b;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 41
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lio/realm/al;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lio/realm/al;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lio/realm/internal/Collection$c;->b:Ljava/lang/Object;

    instance-of v0, v0, Lio/realm/am;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lio/realm/internal/Collection$c;->b:Ljava/lang/Object;

    check-cast v0, Lio/realm/am;

    invoke-interface {v0, p1, p2}, Lio/realm/am;->a(Ljava/lang/Object;Lio/realm/al;)V

    .line 53
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lio/realm/internal/Collection$c;->b:Ljava/lang/Object;

    instance-of v0, v0, Lio/realm/ax;

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lio/realm/internal/Collection$c;->b:Ljava/lang/Object;

    check-cast v0, Lio/realm/ax;

    invoke-interface {v0, p1}, Lio/realm/ax;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported listener type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/internal/Collection$c;->b:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
