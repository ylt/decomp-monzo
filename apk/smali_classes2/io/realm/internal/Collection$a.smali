.class public final enum Lio/realm/internal/Collection$a;
.super Ljava/lang/Enum;
.source "Collection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/internal/Collection$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/realm/internal/Collection$a;

.field public static final enum b:Lio/realm/internal/Collection$a;

.field public static final enum c:Lio/realm/internal/Collection$a;

.field public static final enum d:Lio/realm/internal/Collection$a;

.field private static final synthetic f:[Lio/realm/internal/Collection$a;


# instance fields
.field private final e:B


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 280
    new-instance v0, Lio/realm/internal/Collection$a;

    const-string v1, "MINIMUM"

    invoke-direct {v0, v1, v5, v2}, Lio/realm/internal/Collection$a;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/Collection$a;->a:Lio/realm/internal/Collection$a;

    .line 281
    new-instance v0, Lio/realm/internal/Collection$a;

    const-string v1, "MAXIMUM"

    invoke-direct {v0, v1, v2, v3}, Lio/realm/internal/Collection$a;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/Collection$a;->b:Lio/realm/internal/Collection$a;

    .line 282
    new-instance v0, Lio/realm/internal/Collection$a;

    const-string v1, "AVERAGE"

    invoke-direct {v0, v1, v3, v4}, Lio/realm/internal/Collection$a;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/Collection$a;->c:Lio/realm/internal/Collection$a;

    .line 283
    new-instance v0, Lio/realm/internal/Collection$a;

    const-string v1, "SUM"

    invoke-direct {v0, v1, v4, v6}, Lio/realm/internal/Collection$a;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lio/realm/internal/Collection$a;->d:Lio/realm/internal/Collection$a;

    .line 279
    new-array v0, v6, [Lio/realm/internal/Collection$a;

    sget-object v1, Lio/realm/internal/Collection$a;->a:Lio/realm/internal/Collection$a;

    aput-object v1, v0, v5

    sget-object v1, Lio/realm/internal/Collection$a;->b:Lio/realm/internal/Collection$a;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/internal/Collection$a;->c:Lio/realm/internal/Collection$a;

    aput-object v1, v0, v3

    sget-object v1, Lio/realm/internal/Collection$a;->d:Lio/realm/internal/Collection$a;

    aput-object v1, v0, v4

    sput-object v0, Lio/realm/internal/Collection$a;->f:[Lio/realm/internal/Collection$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    .prologue
    .line 287
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 288
    iput-byte p3, p0, Lio/realm/internal/Collection$a;->e:B

    .line 289
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/internal/Collection$a;
    .locals 1

    .prologue
    .line 279
    const-class v0, Lio/realm/internal/Collection$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Collection$a;

    return-object v0
.end method

.method public static values()[Lio/realm/internal/Collection$a;
    .locals 1

    .prologue
    .line 279
    sget-object v0, Lio/realm/internal/Collection$a;->f:[Lio/realm/internal/Collection$a;

    invoke-virtual {v0}, [Lio/realm/internal/Collection$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/internal/Collection$a;

    return-object v0
.end method


# virtual methods
.method public a()B
    .locals 1

    .prologue
    .line 292
    iget-byte v0, p0, Lio/realm/internal/Collection$a;->e:B

    return v0
.end method
