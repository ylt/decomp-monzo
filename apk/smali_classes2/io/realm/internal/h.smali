.class public Lio/realm/internal/h;
.super Ljava/lang/Object;
.source "ObjectServerFacade.java"


# static fields
.field private static final a:Lio/realm/internal/h;

.field private static b:Lio/realm/internal/h;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    new-instance v0, Lio/realm/internal/h;

    invoke-direct {v0}, Lio/realm/internal/h;-><init>()V

    sput-object v0, Lio/realm/internal/h;->a:Lio/realm/internal/h;

    .line 34
    const/4 v0, 0x0

    sput-object v0, Lio/realm/internal/h;->b:Lio/realm/internal/h;

    .line 40
    :try_start_0
    const-string v0, "io.realm.internal.SyncObjectServerFacade"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 42
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/h;

    sput-object v0, Lio/realm/internal/h;->b:Lio/realm/internal/h;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 53
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    new-instance v1, Lio/realm/exceptions/RealmException;

    const-string v2, "Failed to init SyncObjectServerFacade"

    invoke-direct {v1, v2, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 46
    :catch_1
    move-exception v0

    .line 47
    new-instance v1, Lio/realm/exceptions/RealmException;

    const-string v2, "Failed to init SyncObjectServerFacade"

    invoke-direct {v1, v2, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 48
    :catch_2
    move-exception v0

    .line 49
    new-instance v1, Lio/realm/exceptions/RealmException;

    const-string v2, "Failed to init SyncObjectServerFacade"

    invoke-direct {v1, v2, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 50
    :catch_3
    move-exception v0

    .line 51
    new-instance v1, Lio/realm/exceptions/RealmException;

    const-string v2, "Failed to init SyncObjectServerFacade"

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 43
    :catch_4
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lio/realm/internal/h;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lio/realm/internal/h;->b:Lio/realm/internal/h;

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Lio/realm/internal/h;->b:Lio/realm/internal/h;

    .line 85
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lio/realm/internal/h;->a:Lio/realm/internal/h;

    goto :goto_0
.end method

.method public static a(Z)Lio/realm/internal/h;
    .locals 1

    .prologue
    .line 74
    if-eqz p0, :cond_0

    .line 75
    sget-object v0, Lio/realm/internal/h;->b:Lio/realm/internal/h;

    .line 77
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lio/realm/internal/h;->a:Lio/realm/internal/h;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public a(Lio/realm/ay;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public b(Lio/realm/ay;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    return-object v0
.end method

.method public c(Lio/realm/ay;)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public d(Lio/realm/ay;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method

.method public e(Lio/realm/ay;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return-object v0
.end method

.method public f(Lio/realm/ay;)V
    .locals 0

    .prologue
    .line 108
    return-void
.end method
