.class public Lio/realm/internal/UncheckedRow;
.super Ljava/lang/Object;
.source "UncheckedRow.java"

# interfaces
.implements Lio/realm/internal/g;
.implements Lio/realm/internal/n;


# static fields
.field private static final a:J


# instance fields
.field private final b:Lio/realm/internal/f;

.field private final c:Lio/realm/internal/Table;

.field private final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    invoke-static {}, Lio/realm/internal/UncheckedRow;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/UncheckedRow;->a:J

    return-void
.end method

.method constructor <init>(Lio/realm/internal/UncheckedRow;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iget-object v0, p1, Lio/realm/internal/UncheckedRow;->b:Lio/realm/internal/f;

    iput-object v0, p0, Lio/realm/internal/UncheckedRow;->b:Lio/realm/internal/f;

    .line 51
    iget-object v0, p1, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    iput-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    .line 52
    iget-wide v0, p1, Lio/realm/internal/UncheckedRow;->d:J

    iput-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    .line 54
    return-void
.end method

.method constructor <init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lio/realm/internal/UncheckedRow;->b:Lio/realm/internal/f;

    .line 42
    iput-object p2, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    .line 43
    iput-wide p3, p0, Lio/realm/internal/UncheckedRow;->d:J

    .line 44
    invoke-virtual {p1, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 45
    return-void
.end method

.method static b(Lio/realm/internal/f;Lio/realm/internal/Table;J)Lio/realm/internal/UncheckedRow;
    .locals 4

    .prologue
    .line 75
    invoke-virtual {p1}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1, p2, p3}, Lio/realm/internal/Table;->nativeGetRowPtr(JJ)J

    move-result-wide v0

    .line 76
    new-instance v2, Lio/realm/internal/UncheckedRow;

    invoke-direct {v2, p0, p1, v0, v1}, Lio/realm/internal/UncheckedRow;-><init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V

    return-object v2
.end method

.method static c(Lio/realm/internal/f;Lio/realm/internal/Table;J)Lio/realm/internal/UncheckedRow;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lio/realm/internal/UncheckedRow;

    invoke-direct {v0, p0, p1, p2, p3}, Lio/realm/internal/UncheckedRow;-><init>(Lio/realm/internal/f;Lio/realm/internal/Table;J)V

    return-object v0
.end method

.method private static native nativeGetFinalizerPtr()J
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/UncheckedRow;->nativeGetColumnCount(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 117
    if-nez p1, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column name can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1}, Lio/realm/internal/UncheckedRow;->nativeGetColumnIndex(JLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JD)V
    .locals 9

    .prologue
    .line 214
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 215
    iget-wide v2, p0, Lio/realm/internal/UncheckedRow;->d:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/UncheckedRow;->nativeSetDouble(JJD)V

    .line 216
    return-void
.end method

.method public a(JJ)V
    .locals 9

    .prologue
    .line 195
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 196
    invoke-virtual {p0}, Lio/realm/internal/UncheckedRow;->b()Lio/realm/internal/Table;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/UncheckedRow;->c()J

    move-result-wide v4

    move-wide v2, p1

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJJ)V

    .line 197
    iget-wide v2, p0, Lio/realm/internal/UncheckedRow;->d:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/UncheckedRow;->nativeSetLong(JJJ)V

    .line 198
    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 236
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 237
    if-nez p3, :cond_0

    .line 238
    invoke-virtual {p0}, Lio/realm/internal/UncheckedRow;->b()Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/internal/UncheckedRow;->c()J

    move-result-wide v2

    invoke-virtual {v0, p1, p2, v2, v3}, Lio/realm/internal/Table;->a(JJ)V

    .line 239
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeSetNull(JJ)V

    .line 244
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/UncheckedRow;->b()Lio/realm/internal/Table;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/internal/UncheckedRow;->c()J

    move-result-wide v4

    move-wide v2, p1

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;)V

    .line 242
    iget-wide v2, p0, Lio/realm/internal/UncheckedRow;->d:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/UncheckedRow;->nativeSetString(JJLjava/lang/String;)V

    goto :goto_0
.end method

.method public a(JLjava/util/Date;)V
    .locals 9

    .prologue
    .line 220
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 221
    if-nez p3, :cond_0

    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null Date is not allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_0
    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 225
    iget-wide v2, p0, Lio/realm/internal/UncheckedRow;->d:J

    move-object v1, p0

    move-wide v4, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/UncheckedRow;->nativeSetTimestamp(JJJ)V

    .line 226
    return-void
.end method

.method public a(JZ)V
    .locals 7

    .prologue
    .line 202
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 203
    iget-wide v2, p0, Lio/realm/internal/UncheckedRow;->d:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/UncheckedRow;->nativeSetBoolean(JJZ)V

    .line 204
    return-void
.end method

.method public a(J[B)V
    .locals 7

    .prologue
    .line 248
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 249
    iget-wide v2, p0, Lio/realm/internal/UncheckedRow;->d:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/UncheckedRow;->nativeSetByteArray(JJ[B)V

    .line 250
    return-void
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 182
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeIsNullLink(JJ)Z

    move-result v0

    return v0
.end method

.method public b()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    return-object v0
.end method

.method public b(JJ)V
    .locals 9

    .prologue
    .line 254
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 255
    iget-wide v2, p0, Lio/realm/internal/UncheckedRow;->d:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/UncheckedRow;->nativeSetLink(JJJ)V

    .line 256
    return-void
.end method

.method public b(J)Z
    .locals 3

    .prologue
    .line 266
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeIsNull(JJ)Z

    move-result v0

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 137
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/UncheckedRow;->nativeGetIndex(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public c(J)V
    .locals 5

    .prologue
    .line 276
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 277
    invoke-virtual {p0}, Lio/realm/internal/UncheckedRow;->b()Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/internal/UncheckedRow;->c()J

    move-result-wide v2

    invoke-virtual {v0, p1, p2, v2, v3}, Lio/realm/internal/Table;->a(JJ)V

    .line 278
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeSetNull(JJ)V

    .line 279
    return-void
.end method

.method public d(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetColumnName(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 4

    .prologue
    .line 292
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/UncheckedRow;->nativeIsAttached(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(J)Lio/realm/RealmFieldType;
    .locals 3

    .prologue
    .line 125
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetColumnType(JJ)I

    move-result v0

    invoke-static {v0}, Lio/realm/RealmFieldType;->fromNativeValue(I)Lio/realm/RealmFieldType;

    move-result-object v0

    return-object v0
.end method

.method public f(J)J
    .locals 3

    .prologue
    .line 142
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetLong(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public g(J)Z
    .locals 3

    .prologue
    .line 147
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetBoolean(JJ)Z

    move-result v0

    return v0
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 63
    sget-wide v0, Lio/realm/internal/UncheckedRow;->a:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    return-wide v0
.end method

.method public h(J)F
    .locals 3

    .prologue
    .line 152
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetFloat(JJ)F

    move-result v0

    return v0
.end method

.method public i(J)D
    .locals 3

    .prologue
    .line 157
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetDouble(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public j(J)Ljava/util/Date;
    .locals 5

    .prologue
    .line 162
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v2, v3, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetTimestamp(JJ)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public k(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 167
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetString(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l(J)[B
    .locals 3

    .prologue
    .line 172
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetByteArray(JJ)[B

    move-result-object v0

    return-object v0
.end method

.method public m(J)J
    .locals 3

    .prologue
    .line 177
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetLink(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public n(J)Lio/realm/internal/LinkView;
    .locals 9

    .prologue
    .line 187
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeGetLinkView(JJ)J

    move-result-wide v6

    .line 188
    new-instance v1, Lio/realm/internal/LinkView;

    iget-object v2, p0, Lio/realm/internal/UncheckedRow;->b:Lio/realm/internal/f;

    iget-object v3, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    move-wide v4, p1

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/LinkView;-><init>(Lio/realm/internal/f;Lio/realm/internal/Table;JJ)V

    return-object v1
.end method

.method protected native nativeGetBoolean(JJ)Z
.end method

.method protected native nativeGetByteArray(JJ)[B
.end method

.method protected native nativeGetColumnCount(J)J
.end method

.method protected native nativeGetColumnIndex(JLjava/lang/String;)J
.end method

.method protected native nativeGetColumnName(JJ)Ljava/lang/String;
.end method

.method protected native nativeGetColumnType(JJ)I
.end method

.method protected native nativeGetDouble(JJ)D
.end method

.method protected native nativeGetFloat(JJ)F
.end method

.method protected native nativeGetIndex(J)J
.end method

.method protected native nativeGetLink(JJ)J
.end method

.method protected native nativeGetLinkView(JJ)J
.end method

.method protected native nativeGetLong(JJ)J
.end method

.method protected native nativeGetString(JJ)Ljava/lang/String;
.end method

.method protected native nativeGetTimestamp(JJ)J
.end method

.method protected native nativeIsAttached(J)Z
.end method

.method protected native nativeIsNull(JJ)Z
.end method

.method protected native nativeIsNullLink(JJ)Z
.end method

.method protected native nativeNullifyLink(JJ)V
.end method

.method protected native nativeSetBoolean(JJZ)V
.end method

.method protected native nativeSetByteArray(JJ[B)V
.end method

.method protected native nativeSetDouble(JJD)V
.end method

.method protected native nativeSetLink(JJJ)V
.end method

.method protected native nativeSetLong(JJJ)V
.end method

.method protected native nativeSetNull(JJ)V
.end method

.method protected native nativeSetString(JJLjava/lang/String;)V
.end method

.method protected native nativeSetTimestamp(JJJ)V
.end method

.method public o(J)V
    .locals 3

    .prologue
    .line 260
    iget-object v0, p0, Lio/realm/internal/UncheckedRow;->c:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->h()V

    .line 261
    iget-wide v0, p0, Lio/realm/internal/UncheckedRow;->d:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lio/realm/internal/UncheckedRow;->nativeNullifyLink(JJ)V

    .line 262
    return-void
.end method
