.class public abstract Lio/realm/internal/c;
.super Ljava/lang/Object;
.source "ColumnInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/c$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/realm/internal/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z


# direct methods
.method protected constructor <init>(I)V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/internal/c;-><init>(IZ)V

    .line 95
    return-void
.end method

.method private constructor <init>(IZ)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    .line 113
    iput-boolean p2, p0, Lio/realm/internal/c;->b:Z

    .line 114
    return-void
.end method

.method protected constructor <init>(Lio/realm/internal/c;Z)V
    .locals 2

    .prologue
    .line 104
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0, p2}, Lio/realm/internal/c;-><init>(IZ)V

    .line 106
    if-eqz p1, :cond_0

    .line 107
    iget-object v0, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    iget-object v1, p1, Lio/realm/internal/c;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 109
    :cond_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p1, Lio/realm/internal/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J
    .locals 5

    .prologue
    .line 224
    invoke-virtual {p1, p2}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v2

    .line 225
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    .line 226
    sget-object v0, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq p3, v0, :cond_1

    sget-object v0, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    if-eq p3, v0, :cond_1

    .line 227
    const/4 v0, 0x0

    .line 230
    :goto_0
    iget-object v1, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    new-instance v4, Lio/realm/internal/c$a;

    invoke-direct {v4, v2, v3, p3, v0}, Lio/realm/internal/c$a;-><init>(JLio/realm/RealmFieldType;Ljava/lang/String;)V

    invoke-interface {v1, p2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    :cond_0
    return-wide v2

    .line 228
    :cond_1
    invoke-virtual {p1, v2, v3}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/c$a;

    .line 132
    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, v0, Lio/realm/internal/c$a;->a:J

    goto :goto_0
.end method

.method protected abstract a(Z)Lio/realm/internal/c;
.end method

.method public a(Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 163
    iget-boolean v0, p0, Lio/realm/internal/c;->b:Z

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Attempt to modify an immutable ColumnInfo"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    if-nez p1, :cond_1

    .line 167
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Attempt to copy null ColumnInfo"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_1
    iget-object v0, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 171
    iget-object v0, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    iget-object v1, p1, Lio/realm/internal/c;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 172
    invoke-virtual {p0, p1, p0}, Lio/realm/internal/c;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 173
    return-void
.end method

.method protected abstract a(Lio/realm/internal/c;Lio/realm/internal/c;)V
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lio/realm/internal/c;->b:Z

    return v0
.end method

.method public b(Ljava/lang/String;)Lio/realm/RealmFieldType;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/c$a;

    .line 142
    if-nez v0, :cond_0

    sget-object v0, Lio/realm/RealmFieldType;->UNSUPPORTED_TABLE:Lio/realm/RealmFieldType;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lio/realm/internal/c$a;->b:Lio/realm/RealmFieldType;

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/c$a;

    .line 152
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lio/realm/internal/c$a;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 177
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "ColumnInfo["

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 178
    iget-boolean v0, p0, Lio/realm/internal/c;->b:Z

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    iget-object v0, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 180
    const/4 v0, 0x0

    .line 181
    iget-object v1, p0, Lio/realm/internal/c;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 182
    if-eqz v1, :cond_0

    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "->"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    const/4 v0, 0x1

    move v1, v0

    .line 185
    goto :goto_0

    .line 187
    :cond_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
