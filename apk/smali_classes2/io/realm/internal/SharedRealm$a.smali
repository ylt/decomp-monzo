.class public final enum Lio/realm/internal/SharedRealm$a;
.super Ljava/lang/Enum;
.source "SharedRealm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/SharedRealm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/internal/SharedRealm$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/realm/internal/SharedRealm$a;

.field public static final enum b:Lio/realm/internal/SharedRealm$a;

.field private static final synthetic d:[Lio/realm/internal/SharedRealm$a;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    new-instance v0, Lio/realm/internal/SharedRealm$a;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2, v2}, Lio/realm/internal/SharedRealm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/SharedRealm$a;->a:Lio/realm/internal/SharedRealm$a;

    .line 73
    new-instance v0, Lio/realm/internal/SharedRealm$a;

    const-string v1, "MEM_ONLY"

    invoke-direct {v0, v1, v3, v3}, Lio/realm/internal/SharedRealm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/realm/internal/SharedRealm$a;->b:Lio/realm/internal/SharedRealm$a;

    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [Lio/realm/internal/SharedRealm$a;

    sget-object v1, Lio/realm/internal/SharedRealm$a;->a:Lio/realm/internal/SharedRealm$a;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/internal/SharedRealm$a;->b:Lio/realm/internal/SharedRealm$a;

    aput-object v1, v0, v3

    sput-object v0, Lio/realm/internal/SharedRealm$a;->d:[Lio/realm/internal/SharedRealm$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput p3, p0, Lio/realm/internal/SharedRealm$a;->c:I

    .line 79
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/internal/SharedRealm$a;
    .locals 1

    .prologue
    .line 71
    const-class v0, Lio/realm/internal/SharedRealm$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/internal/SharedRealm$a;

    return-object v0
.end method

.method public static values()[Lio/realm/internal/SharedRealm$a;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lio/realm/internal/SharedRealm$a;->d:[Lio/realm/internal/SharedRealm$a;

    invoke-virtual {v0}, [Lio/realm/internal/SharedRealm$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/internal/SharedRealm$a;

    return-object v0
.end method
