.class public Lio/realm/internal/Property;
.super Ljava/lang/Object;
.source "Property.java"

# interfaces
.implements Lio/realm/internal/g;


# static fields
.field private static final b:J


# instance fields
.field private a:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lio/realm/internal/Property;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/Property;->b:J

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p2}, Lio/realm/RealmFieldType;->getNativeValue()I

    move-result v0

    invoke-static {p1, v0, p3}, Lio/realm/internal/Property;->nativeCreateProperty(Ljava/lang/String;ILjava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Property;->a:J

    .line 42
    sget-object v0, Lio/realm/internal/f;->a:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 43
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p2}, Lio/realm/RealmFieldType;->getNativeValue()I

    move-result v1

    if-nez p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1, v1, p3, p4, v0}, Lio/realm/internal/Property;->nativeCreateProperty(Ljava/lang/String;IZZZ)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Property;->a:J

    .line 37
    sget-object v0, Lio/realm/internal/f;->a:Lio/realm/internal/f;

    invoke-virtual {v0, p0}, Lio/realm/internal/f;->a(Lio/realm/internal/g;)V

    .line 38
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native nativeCreateProperty(Ljava/lang/String;ILjava/lang/String;)J
.end method

.method private static native nativeCreateProperty(Ljava/lang/String;IZZZ)J
.end method

.method private static native nativeGetFinalizerPtr()J
.end method


# virtual methods
.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 56
    sget-wide v0, Lio/realm/internal/Property;->b:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lio/realm/internal/Property;->a:J

    return-wide v0
.end method
