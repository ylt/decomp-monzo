.class public final enum Lio/realm/internal/Collection$f;
.super Ljava/lang/Enum;
.source "Collection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/internal/Collection$f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/realm/internal/Collection$f;

.field public static final enum b:Lio/realm/internal/Collection$f;

.field public static final enum c:Lio/realm/internal/Collection$f;

.field public static final enum d:Lio/realm/internal/Collection$f;

.field public static final enum e:Lio/realm/internal/Collection$f;

.field private static final synthetic f:[Lio/realm/internal/Collection$f;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 308
    new-instance v0, Lio/realm/internal/Collection$f;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v2}, Lio/realm/internal/Collection$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/realm/internal/Collection$f;->a:Lio/realm/internal/Collection$f;

    .line 309
    new-instance v0, Lio/realm/internal/Collection$f;

    const-string v1, "TABLE"

    invoke-direct {v0, v1, v3}, Lio/realm/internal/Collection$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/realm/internal/Collection$f;->b:Lio/realm/internal/Collection$f;

    .line 310
    new-instance v0, Lio/realm/internal/Collection$f;

    const-string v1, "QUERY"

    invoke-direct {v0, v1, v4}, Lio/realm/internal/Collection$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/realm/internal/Collection$f;->c:Lio/realm/internal/Collection$f;

    .line 311
    new-instance v0, Lio/realm/internal/Collection$f;

    const-string v1, "LINKVIEW"

    invoke-direct {v0, v1, v5}, Lio/realm/internal/Collection$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/realm/internal/Collection$f;->d:Lio/realm/internal/Collection$f;

    .line 312
    new-instance v0, Lio/realm/internal/Collection$f;

    const-string v1, "TABLEVIEW"

    invoke-direct {v0, v1, v6}, Lio/realm/internal/Collection$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/realm/internal/Collection$f;->e:Lio/realm/internal/Collection$f;

    .line 307
    const/4 v0, 0x5

    new-array v0, v0, [Lio/realm/internal/Collection$f;

    sget-object v1, Lio/realm/internal/Collection$f;->a:Lio/realm/internal/Collection$f;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/internal/Collection$f;->b:Lio/realm/internal/Collection$f;

    aput-object v1, v0, v3

    sget-object v1, Lio/realm/internal/Collection$f;->c:Lio/realm/internal/Collection$f;

    aput-object v1, v0, v4

    sget-object v1, Lio/realm/internal/Collection$f;->d:Lio/realm/internal/Collection$f;

    aput-object v1, v0, v5

    sget-object v1, Lio/realm/internal/Collection$f;->e:Lio/realm/internal/Collection$f;

    aput-object v1, v0, v6

    sput-object v0, Lio/realm/internal/Collection$f;->f:[Lio/realm/internal/Collection$f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 307
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(B)Lio/realm/internal/Collection$f;
    .locals 3

    .prologue
    .line 315
    packed-switch p0, :pswitch_data_0

    .line 327
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :pswitch_0
    sget-object v0, Lio/realm/internal/Collection$f;->a:Lio/realm/internal/Collection$f;

    .line 325
    :goto_0
    return-object v0

    .line 319
    :pswitch_1
    sget-object v0, Lio/realm/internal/Collection$f;->b:Lio/realm/internal/Collection$f;

    goto :goto_0

    .line 321
    :pswitch_2
    sget-object v0, Lio/realm/internal/Collection$f;->c:Lio/realm/internal/Collection$f;

    goto :goto_0

    .line 323
    :pswitch_3
    sget-object v0, Lio/realm/internal/Collection$f;->d:Lio/realm/internal/Collection$f;

    goto :goto_0

    .line 325
    :pswitch_4
    sget-object v0, Lio/realm/internal/Collection$f;->e:Lio/realm/internal/Collection$f;

    goto :goto_0

    .line 315
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/internal/Collection$f;
    .locals 1

    .prologue
    .line 307
    const-class v0, Lio/realm/internal/Collection$f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/internal/Collection$f;

    return-object v0
.end method

.method public static values()[Lio/realm/internal/Collection$f;
    .locals 1

    .prologue
    .line 307
    sget-object v0, Lio/realm/internal/Collection$f;->f:[Lio/realm/internal/Collection$f;

    invoke-virtual {v0}, [Lio/realm/internal/Collection$f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/internal/Collection$f;

    return-object v0
.end method
