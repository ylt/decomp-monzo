.class public Lio/realm/bo;
.super Lco/uk/getmondo/d/am;
.source "UserSettingsRealmProxy.java"

# interfaces
.implements Lio/realm/bp;
.implements Lio/realm/internal/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/bo$a;
    }
.end annotation


# static fields
.field private static final c:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lio/realm/bo$a;

.field private b:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/d/am;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    invoke-static {}, Lio/realm/bo;->m()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/bo;->c:Lio/realm/internal/OsObjectSchemaInfo;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 80
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const-string v1, "statusId"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    const-string v1, "monzoMeUsername"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    const-string v1, "inboundP2P"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    const-string v1, "prepaidAccountMigrated"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/bo;->d:Ljava/util/List;

    .line 86
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Lco/uk/getmondo/d/am;-><init>()V

    .line 89
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 90
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/am;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/am;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 572
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 610
    :goto_0
    return-wide v4

    .line 575
    :cond_0
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 576
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 577
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/am;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/bo$a;

    .line 578
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 579
    check-cast v2, Lio/realm/bp;

    invoke-interface {v2}, Lio/realm/bp;->e()Ljava/lang/String;

    move-result-object v2

    .line 581
    if-nez v2, :cond_4

    .line 582
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 586
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_5

    .line 587
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 591
    :goto_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 592
    check-cast v2, Lio/realm/bp;

    invoke-interface {v2}, Lio/realm/bp;->f()Ljava/lang/String;

    move-result-object v6

    .line 593
    if-eqz v6, :cond_1

    .line 594
    iget-wide v2, v9, Lio/realm/bo$a;->b:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_1
    move-object v2, p1

    .line 596
    check-cast v2, Lio/realm/bp;

    invoke-interface {v2}, Lio/realm/bp;->g()Ljava/lang/String;

    move-result-object v6

    .line 597
    if-eqz v6, :cond_2

    .line 598
    iget-wide v2, v9, Lio/realm/bo$a;->c:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_2
    move-object v2, p1

    .line 601
    check-cast v2, Lio/realm/bp;

    invoke-interface {v2}, Lio/realm/bp;->h()Lco/uk/getmondo/d/p;

    move-result-object v3

    .line 602
    if-eqz v3, :cond_3

    .line 603
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 604
    if-nez v2, :cond_6

    .line 605
    invoke-static {p0, v3, p2}, Lio/realm/aa;->a(Lio/realm/av;Lco/uk/getmondo/d/p;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 607
    :goto_3
    iget-wide v2, v9, Lio/realm/bo$a;->d:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 609
    :cond_3
    iget-wide v2, v9, Lio/realm/bo$a;->e:J

    check-cast p1, Lio/realm/bp;

    invoke-interface {p1}, Lio/realm/bp;->i()Z

    move-result v6

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto/16 :goto_0

    .line 584
    :cond_4
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto :goto_1

    .line 589
    :cond_5
    invoke-static {v2}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    move-object v6, v2

    goto :goto_3
.end method

.method public static a(Lco/uk/getmondo/d/am;IILjava/util/Map;)Lco/uk/getmondo/d/am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/am;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/d/am;"
        }
    .end annotation

    .prologue
    .line 762
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 763
    :cond_0
    const/4 v0, 0x0

    .line 787
    :goto_0
    return-object v0

    .line 765
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 767
    if-nez v0, :cond_2

    .line 768
    new-instance v1, Lco/uk/getmondo/d/am;

    invoke-direct {v1}, Lco/uk/getmondo/d/am;-><init>()V

    .line 769
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 778
    check-cast v0, Lio/realm/bp;

    .line 779
    check-cast p0, Lio/realm/bp;

    .line 780
    invoke-interface {p0}, Lio/realm/bp;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/bp;->a(Ljava/lang/String;)V

    .line 781
    invoke-interface {p0}, Lio/realm/bp;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/bp;->b(Ljava/lang/String;)V

    .line 782
    invoke-interface {p0}, Lio/realm/bp;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/bp;->c(Ljava/lang/String;)V

    .line 785
    invoke-interface {p0}, Lio/realm/bp;->h()Lco/uk/getmondo/d/p;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/aa;->a(Lco/uk/getmondo/d/p;IILjava/util/Map;)Lco/uk/getmondo/d/p;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/bp;->a(Lco/uk/getmondo/d/p;)V

    .line 786
    invoke-interface {p0}, Lio/realm/bp;->i()Z

    move-result v2

    invoke-interface {v0, v2}, Lio/realm/bp;->a(Z)V

    move-object v0, v1

    .line 787
    goto :goto_0

    .line 772
    :cond_2
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_3

    .line 773
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/d/am;

    goto :goto_0

    .line 775
    :cond_3
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/d/am;

    .line 776
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto :goto_1
.end method

.method static a(Lio/realm/av;Lco/uk/getmondo/d/am;Lco/uk/getmondo/d/am;Ljava/util/Map;)Lco/uk/getmondo/d/am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/am;",
            "Lco/uk/getmondo/d/am;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/am;"
        }
    .end annotation

    .prologue
    .line 791
    move-object v0, p1

    check-cast v0, Lio/realm/bp;

    .line 792
    check-cast p2, Lio/realm/bp;

    .line 793
    invoke-interface {p2}, Lio/realm/bp;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bp;->b(Ljava/lang/String;)V

    .line 794
    invoke-interface {p2}, Lio/realm/bp;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bp;->c(Ljava/lang/String;)V

    .line 795
    invoke-interface {p2}, Lio/realm/bp;->h()Lco/uk/getmondo/d/p;

    move-result-object v2

    .line 796
    if-nez v2, :cond_0

    .line 797
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lio/realm/bp;->a(Lco/uk/getmondo/d/p;)V

    .line 806
    :goto_0
    invoke-interface {p2}, Lio/realm/bp;->i()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/bp;->a(Z)V

    .line 807
    return-object p1

    .line 799
    :cond_0
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/p;

    .line 800
    if-eqz v1, :cond_1

    .line 801
    invoke-interface {v0, v1}, Lio/realm/bp;->a(Lco/uk/getmondo/d/p;)V

    goto :goto_0

    .line 803
    :cond_1
    const/4 v1, 0x1

    invoke-static {p0, v2, v1, p3}, Lio/realm/aa;->a(Lio/realm/av;Lco/uk/getmondo/d/p;ZLjava/util/Map;)Lco/uk/getmondo/d/p;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bp;->a(Lco/uk/getmondo/d/p;)V

    goto :goto_0
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/am;ZLjava/util/Map;)Lco/uk/getmondo/d/am;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/am;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/am;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 496
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-wide v2, v2, Lio/realm/g;->c:J

    iget-wide v6, p0, Lio/realm/av;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 497
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 499
    :cond_0
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 536
    :goto_0
    return-object p1

    .line 502
    :cond_1
    sget-object v2, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v2}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/g$b;

    .line 503
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/l;

    .line 504
    if-eqz v3, :cond_2

    .line 505
    check-cast v3, Lco/uk/getmondo/d/am;

    move-object p1, v3

    goto :goto_0

    .line 508
    :cond_2
    const/4 v5, 0x0

    .line 510
    if-eqz p2, :cond_6

    .line 511
    const-class v3, Lco/uk/getmondo/d/am;

    invoke-virtual {p0, v3}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 512
    invoke-virtual {v8}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v3, p1

    .line 513
    check-cast v3, Lio/realm/bp;

    invoke-interface {v3}, Lio/realm/bp;->e()Ljava/lang/String;

    move-result-object v3

    .line 515
    if-nez v3, :cond_3

    .line 516
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v6

    .line 520
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-eqz v3, :cond_4

    .line 522
    :try_start_0
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    iget-object v3, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/am;

    invoke-virtual {v3, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 523
    new-instance v4, Lio/realm/bo;

    invoke-direct {v4}, Lio/realm/bo;-><init>()V

    .line 524
    move-object v0, v4

    check-cast v0, Lio/realm/internal/l;

    move-object v3, v0

    invoke-interface {p3, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    move v2, p2

    .line 533
    :goto_2
    if-eqz v2, :cond_5

    .line 534
    invoke-static {p0, v4, p1, p3}, Lio/realm/bo;->a(Lio/realm/av;Lco/uk/getmondo/d/am;Lco/uk/getmondo/d/am;Ljava/util/Map;)Lco/uk/getmondo/d/am;

    move-result-object p1

    goto :goto_0

    .line 518
    :cond_3
    invoke-virtual {v8, v6, v7, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 526
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    throw v3

    :cond_4
    move v2, v4

    move-object v4, v5

    .line 529
    goto :goto_2

    .line 536
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/bo;->b(Lio/realm/av;Lco/uk/getmondo/d/am;ZLjava/util/Map;)Lco/uk/getmondo/d/am;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v2, p2

    move-object v4, v5

    goto :goto_2
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/bo$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0x5

    .line 272
    const-string v0, "class_UserSettings"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'UserSettings\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 275
    :cond_0
    const-string v0, "class_UserSettings"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 276
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 277
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 278
    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 279
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 5 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_1
    if-eqz p1, :cond_3

    .line 282
    const-string v0, "Field count is more than expected - expected 5 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 288
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 289
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 284
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 5 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 292
    :cond_4
    new-instance v0, Lio/realm/bo$a;

    invoke-direct {v0, p0, v2}, Lio/realm/bo$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 294
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 295
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 297
    :cond_5
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lio/realm/bo$a;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 298
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_6
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 303
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 305
    :cond_7
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_8

    .line 306
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'id\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_8
    iget-wide v4, v0, Lio/realm/bo$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_9

    .line 309
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@PrimaryKey field \'id\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_9
    const-string v1, "id"

    invoke-virtual {v2, v1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->j(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 312
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_a
    const-string v1, "statusId"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 315
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'statusId\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_b
    const-string v1, "statusId"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_c

    .line 318
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'statusId\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_c
    iget-wide v4, v0, Lio/realm/bo$a;->b:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_d

    .line 321
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'statusId\' is required. Either set @Required to field \'statusId\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_d
    const-string v1, "monzoMeUsername"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 324
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'monzoMeUsername\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_e
    const-string v1, "monzoMeUsername"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_f

    .line 327
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'monzoMeUsername\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_f
    iget-wide v4, v0, Lio/realm/bo$a;->c:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_10

    .line 330
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'monzoMeUsername\' is required. Either set @Required to field \'monzoMeUsername\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 332
    :cond_10
    const-string v1, "inboundP2P"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 333
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'inboundP2P\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_11
    const-string v1, "inboundP2P"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_12

    .line 336
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'InboundP2P\' for field \'inboundP2P\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_12
    const-string v1, "class_InboundP2P"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 339
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_InboundP2P\' for field \'inboundP2P\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_13
    const-string v1, "class_InboundP2P"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 342
    iget-wide v4, v0, Lio/realm/bo$a;->d:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_14

    .line 343
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'inboundP2P\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/bo$a;->d:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 345
    :cond_14
    const-string v1, "prepaidAccountMigrated"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 346
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'prepaidAccountMigrated\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_15
    const-string v1, "prepaidAccountMigrated"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_16

    .line 349
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'prepaidAccountMigrated\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 351
    :cond_16
    iget-wide v4, v0, Lio/realm/bo$a;->e:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 352
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'prepaidAccountMigrated\' does support null values in the existing Realm file. Use corresponding boxed type for field \'prepaidAccountMigrated\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 355
    :cond_17
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 709
    const-class v2, Lco/uk/getmondo/d/am;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v13

    .line 710
    invoke-virtual {v13}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    .line 711
    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/am;

    invoke-virtual {v4, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lio/realm/bo$a;

    .line 712
    invoke-virtual {v13}, Lio/realm/internal/Table;->d()J

    move-result-wide v14

    .line 714
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 715
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Lco/uk/getmondo/d/am;

    .line 716
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 719
    instance-of v4, v12, Lio/realm/internal/l;

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v12

    .line 720
    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v4, v12

    .line 723
    check-cast v4, Lio/realm/bp;

    invoke-interface {v4}, Lio/realm/bp;->e()Ljava/lang/String;

    move-result-object v4

    .line 725
    if-nez v4, :cond_3

    .line 726
    invoke-static {v2, v3, v14, v15}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v6

    .line 730
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 731
    invoke-static {v13, v4}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v6

    .line 733
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v12

    .line 734
    check-cast v4, Lio/realm/bp;

    invoke-interface {v4}, Lio/realm/bp;->f()Ljava/lang/String;

    move-result-object v8

    .line 735
    if-eqz v8, :cond_4

    .line 736
    iget-wide v4, v11, Lio/realm/bo$a;->b:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_2
    move-object v4, v12

    .line 740
    check-cast v4, Lio/realm/bp;

    invoke-interface {v4}, Lio/realm/bp;->g()Ljava/lang/String;

    move-result-object v8

    .line 741
    if-eqz v8, :cond_5

    .line 742
    iget-wide v4, v11, Lio/realm/bo$a;->c:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_3
    move-object v4, v12

    .line 747
    check-cast v4, Lio/realm/bp;

    invoke-interface {v4}, Lio/realm/bp;->h()Lco/uk/getmondo/d/p;

    move-result-object v5

    .line 748
    if-eqz v5, :cond_6

    .line 749
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 750
    if-nez v4, :cond_8

    .line 751
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/aa;->b(Lio/realm/av;Lco/uk/getmondo/d/p;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 753
    :goto_4
    iget-wide v4, v11, Lio/realm/bo$a;->d:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 757
    :goto_5
    iget-wide v4, v11, Lio/realm/bo$a;->e:J

    check-cast v12, Lio/realm/bp;

    invoke-interface {v12}, Lio/realm/bp;->i()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto/16 :goto_0

    .line 728
    :cond_3
    invoke-static {v2, v3, v14, v15, v4}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 738
    :cond_4
    iget-wide v4, v11, Lio/realm/bo$a;->b:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_2

    .line 744
    :cond_5
    iget-wide v4, v11, Lio/realm/bo$a;->c:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_3

    .line 755
    :cond_6
    iget-wide v4, v11, Lio/realm/bo$a;->d:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_5

    .line 759
    :cond_7
    return-void

    :cond_8
    move-object v8, v4

    goto :goto_4
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/am;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/am;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 663
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 705
    :goto_0
    return-wide v4

    .line 666
    :cond_0
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 667
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 668
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/am;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/bo$a;

    .line 669
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 670
    check-cast v2, Lio/realm/bp;

    invoke-interface {v2}, Lio/realm/bp;->e()Ljava/lang/String;

    move-result-object v2

    .line 672
    if-nez v2, :cond_2

    .line 673
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 677
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    .line 678
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 680
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 681
    check-cast v2, Lio/realm/bp;

    invoke-interface {v2}, Lio/realm/bp;->f()Ljava/lang/String;

    move-result-object v6

    .line 682
    if-eqz v6, :cond_3

    .line 683
    iget-wide v2, v9, Lio/realm/bo$a;->b:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_2
    move-object v2, p1

    .line 687
    check-cast v2, Lio/realm/bp;

    invoke-interface {v2}, Lio/realm/bp;->g()Ljava/lang/String;

    move-result-object v6

    .line 688
    if-eqz v6, :cond_4

    .line 689
    iget-wide v2, v9, Lio/realm/bo$a;->c:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_3
    move-object v2, p1

    .line 694
    check-cast v2, Lio/realm/bp;

    invoke-interface {v2}, Lio/realm/bp;->h()Lco/uk/getmondo/d/p;

    move-result-object v3

    .line 695
    if-eqz v3, :cond_5

    .line 696
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 697
    if-nez v2, :cond_6

    .line 698
    invoke-static {p0, v3, p2}, Lio/realm/aa;->b(Lio/realm/av;Lco/uk/getmondo/d/p;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 700
    :goto_4
    iget-wide v2, v9, Lio/realm/bo$a;->d:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 704
    :goto_5
    iget-wide v2, v9, Lio/realm/bo$a;->e:J

    check-cast p1, Lio/realm/bp;

    invoke-interface {p1}, Lio/realm/bp;->i()Z

    move-result v6

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    goto/16 :goto_0

    .line 675
    :cond_2
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto :goto_1

    .line 685
    :cond_3
    iget-wide v2, v9, Lio/realm/bo$a;->b:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_2

    .line 691
    :cond_4
    iget-wide v2, v9, Lio/realm/bo$a;->c:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_3

    .line 702
    :cond_5
    iget-wide v2, v9, Lio/realm/bo$a;->d:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_5

    :cond_6
    move-object v6, v2

    goto :goto_4
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/am;ZLjava/util/Map;)Lco/uk/getmondo/d/am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/am;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/am;"
        }
    .end annotation

    .prologue
    .line 541
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 542
    if-eqz v0, :cond_0

    .line 543
    check-cast v0, Lco/uk/getmondo/d/am;

    .line 568
    :goto_0
    return-object v0

    .line 547
    :cond_0
    const-class v1, Lco/uk/getmondo/d/am;

    move-object v0, p1

    check-cast v0, Lio/realm/bp;

    invoke-interface {v0}, Lio/realm/bp;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lio/realm/av;->a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/am;

    move-object v1, v0

    .line 548
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    check-cast p1, Lio/realm/bp;

    move-object v1, v0

    .line 551
    check-cast v1, Lio/realm/bp;

    .line 553
    invoke-interface {p1}, Lio/realm/bp;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bp;->b(Ljava/lang/String;)V

    .line 554
    invoke-interface {p1}, Lio/realm/bp;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bp;->c(Ljava/lang/String;)V

    .line 556
    invoke-interface {p1}, Lio/realm/bp;->h()Lco/uk/getmondo/d/p;

    move-result-object v3

    .line 557
    if-nez v3, :cond_1

    .line 558
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lio/realm/bp;->a(Lco/uk/getmondo/d/p;)V

    .line 567
    :goto_1
    invoke-interface {p1}, Lio/realm/bp;->i()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/bp;->a(Z)V

    goto :goto_0

    .line 560
    :cond_1
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/p;

    .line 561
    if-eqz v2, :cond_2

    .line 562
    invoke-interface {v1, v2}, Lio/realm/bp;->a(Lco/uk/getmondo/d/p;)V

    goto :goto_1

    .line 564
    :cond_2
    invoke-static {p0, v3, p2, p3}, Lio/realm/aa;->a(Lio/realm/av;Lco/uk/getmondo/d/p;ZLjava/util/Map;)Lco/uk/getmondo/d/p;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bp;->a(Lco/uk/getmondo/d/p;)V

    goto :goto_1
.end method

.method public static j()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 268
    sget-object v0, Lio/realm/bo;->c:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359
    const-string v0, "class_UserSettings"

    return-object v0
.end method

.method private static m()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 258
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "UserSettings"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 259
    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 260
    const-string v7, "statusId"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 261
    const-string v7, "monzoMeUsername"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 262
    const-string v1, "inboundP2P"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "InboundP2P"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 263
    const-string v7, "prepaidAccountMigrated"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 264
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/p;)V
    .locals 9

    .prologue
    .line 195
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 196
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "inboundP2P"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 203
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/p;

    move-object v6, v0

    .line 205
    :goto_1
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 206
    if-nez v6, :cond_2

    .line 208
    iget-object v0, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v0, v0, Lio/realm/bo$a;->d:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 211
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 212
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 214
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v0, Lio/realm/bo$a;->d:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 221
    :cond_5
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 222
    if-nez p1, :cond_6

    .line 223
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 226
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 227
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 229
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 230
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_9
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->d:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 121
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Z)V
    .locals 8

    .prologue
    .line 244
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    :goto_0
    return-void

    .line 248
    :cond_0
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 249
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v2, Lio/realm/bo$a;->e:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 253
    :cond_1
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 254
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->e:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 133
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 138
    if-nez p1, :cond_1

    .line 139
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v0, Lio/realm/bo$a;->b:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 142
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v1, v1, Lio/realm/bo$a;->b:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 147
    if-nez p1, :cond_3

    .line 148
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 151
    :cond_3
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->b:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 163
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 168
    if-nez p1, :cond_1

    .line 169
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v0, Lio/realm/bo$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 172
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v1, v1, Lio/realm/bo$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 177
    if-nez p1, :cond_3

    .line 178
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 181
    :cond_3
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->c:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 110
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 128
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 158
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lco/uk/getmondo/d/p;
    .locals 6

    .prologue
    .line 186
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 187
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    const/4 v0, 0x0

    .line 190
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/p;

    iget-object v2, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v4, v3, Lio/realm/bo$a;->d:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/p;

    goto :goto_0
.end method

.method public i()Z
    .locals 4

    .prologue
    .line 238
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 239
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    iget-wide v2, v1, Lio/realm/bo$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 842
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 813
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 814
    const-string v0, "Invalid object"

    .line 837
    :goto_0
    return-object v0

    .line 816
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "UserSettings = proxy["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 817
    const-string v0, "{id:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 818
    invoke-virtual {p0}, Lio/realm/bo;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lio/realm/bo;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 819
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 821
    const-string v0, "{statusId:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 822
    invoke-virtual {p0}, Lio/realm/bo;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lio/realm/bo;->f()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 823
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 825
    const-string v0, "{monzoMeUsername:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    invoke-virtual {p0}, Lio/realm/bo;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lio/realm/bo;->g()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 827
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 828
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 829
    const-string v0, "{inboundP2P:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 830
    invoke-virtual {p0}, Lio/realm/bo;->h()Lco/uk/getmondo/d/p;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "InboundP2P"

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 833
    const-string v0, "{prepaidAccountMigrated:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 834
    invoke-virtual {p0}, Lio/realm/bo;->i()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 835
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 836
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 837
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 818
    :cond_1
    const-string v0, "null"

    goto :goto_1

    .line 822
    :cond_2
    const-string v0, "null"

    goto :goto_2

    .line 826
    :cond_3
    const-string v0, "null"

    goto :goto_3

    .line 830
    :cond_4
    const-string v0, "null"

    goto :goto_4
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lio/realm/bo;->b:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 97
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 98
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/bo$a;

    iput-object v1, p0, Lio/realm/bo;->a:Lio/realm/bo$a;

    .line 99
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/bo;->b:Lio/realm/au;

    .line 100
    iget-object v1, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 101
    iget-object v1, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 102
    iget-object v1, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 103
    iget-object v1, p0, Lio/realm/bo;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method
