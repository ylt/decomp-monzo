.class public final enum Lio/realm/l;
.super Ljava/lang/Enum;
.source "Case.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/realm/l;

.field public static final enum b:Lio/realm/l;

.field private static final synthetic d:[Lio/realm/l;


# instance fields
.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lio/realm/l;

    const-string v1, "SENSITIVE"

    invoke-direct {v0, v1, v2, v3}, Lio/realm/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lio/realm/l;->a:Lio/realm/l;

    .line 29
    new-instance v0, Lio/realm/l;

    const-string v1, "INSENSITIVE"

    invoke-direct {v0, v1, v3, v2}, Lio/realm/l;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lio/realm/l;->b:Lio/realm/l;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Lio/realm/l;

    sget-object v1, Lio/realm/l;->a:Lio/realm/l;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/l;->b:Lio/realm/l;

    aput-object v1, v0, v3

    sput-object v0, Lio/realm/l;->d:[Lio/realm/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-boolean p3, p0, Lio/realm/l;->c:Z

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/l;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lio/realm/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/l;

    return-object v0
.end method

.method public static values()[Lio/realm/l;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lio/realm/l;->d:[Lio/realm/l;

    invoke-virtual {v0}, [Lio/realm/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/l;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lio/realm/l;->c:Z

    return v0
.end method
