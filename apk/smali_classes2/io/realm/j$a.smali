.class final Lio/realm/j$a;
.super Lio/realm/internal/c;
.source "CardRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:J

.field f:J

.field g:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 47
    const-string v0, "id"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/j$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/j$a;->a:J

    .line 48
    const-string v0, "accountId"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/j$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/j$a;->b:J

    .line 49
    const-string v0, "expires"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/j$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/j$a;->c:J

    .line 50
    const-string v0, "lastDigits"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/j$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/j$a;->d:J

    .line 51
    const-string v0, "processorToken"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/j$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/j$a;->e:J

    .line 52
    const-string v0, "cardStatus"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/j$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/j$a;->f:J

    .line 53
    const-string v0, "replacementOrdered"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/j$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/j$a;->g:J

    .line 54
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 58
    invoke-virtual {p0, p1, p0}, Lio/realm/j$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 59
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lio/realm/j$a;

    invoke-direct {v0, p0, p1}, Lio/realm/j$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 68
    check-cast p1, Lio/realm/j$a;

    .line 69
    check-cast p2, Lio/realm/j$a;

    .line 70
    iget-wide v0, p1, Lio/realm/j$a;->a:J

    iput-wide v0, p2, Lio/realm/j$a;->a:J

    .line 71
    iget-wide v0, p1, Lio/realm/j$a;->b:J

    iput-wide v0, p2, Lio/realm/j$a;->b:J

    .line 72
    iget-wide v0, p1, Lio/realm/j$a;->c:J

    iput-wide v0, p2, Lio/realm/j$a;->c:J

    .line 73
    iget-wide v0, p1, Lio/realm/j$a;->d:J

    iput-wide v0, p2, Lio/realm/j$a;->d:J

    .line 74
    iget-wide v0, p1, Lio/realm/j$a;->e:J

    iput-wide v0, p2, Lio/realm/j$a;->e:J

    .line 75
    iget-wide v0, p1, Lio/realm/j$a;->f:J

    iput-wide v0, p2, Lio/realm/j$a;->f:J

    .line 76
    iget-wide v0, p1, Lio/realm/j$a;->g:J

    iput-wide v0, p2, Lio/realm/j$a;->g:J

    .line 77
    return-void
.end method
