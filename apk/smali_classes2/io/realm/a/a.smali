.class public Lio/realm/a/a;
.super Ljava/lang/Object;
.source "RealmObservableFactory.java"

# interfaces
.implements Lio/realm/a/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/a/a$a;
    }
.end annotation


# instance fields
.field a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lio/realm/a/a$a",
            "<",
            "Lio/realm/bg;",
            ">;>;"
        }
    .end annotation
.end field

.field b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lio/realm/a/a$a",
            "<",
            "Lio/realm/az;",
            ">;>;"
        }
    .end annotation
.end field

.field c:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lio/realm/a/a$a",
            "<",
            "Lio/realm/bb;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lio/realm/a/a$1;

    invoke-direct {v0, p0}, Lio/realm/a/a$1;-><init>(Lio/realm/a/a;)V

    iput-object v0, p0, Lio/realm/a/a;->a:Ljava/lang/ThreadLocal;

    .line 58
    new-instance v0, Lio/realm/a/a$2;

    invoke-direct {v0, p0}, Lio/realm/a/a$2;-><init>(Lio/realm/a/a;)V

    iput-object v0, p0, Lio/realm/a/a;->b:Ljava/lang/ThreadLocal;

    .line 64
    new-instance v0, Lio/realm/a/a$3;

    invoke-direct {v0, p0}, Lio/realm/a/a$3;-><init>(Lio/realm/a/a;)V

    iput-object v0, p0, Lio/realm/a/a;->c:Ljava/lang/ThreadLocal;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 362
    instance-of v0, p1, Lio/realm/a/a;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 367
    const/16 v0, 0x25

    return v0
.end method
