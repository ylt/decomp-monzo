.class public Lio/realm/ae;
.super Lco/uk/getmondo/d/t;
.source "LocalSpendRealmProxy.java"

# interfaces
.implements Lio/realm/af;
.implements Lio/realm/internal/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/ae$a;
    }
.end annotation


# static fields
.field private static final c:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lio/realm/ae$a;

.field private b:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/d/t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    invoke-static {}, Lio/realm/ae;->f()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/ae;->c:Lio/realm/internal/OsObjectSchemaInfo;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 71
    const-string v1, "currency"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    const-string v1, "amountValue"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/ae;->d:Ljava/util/List;

    .line 74
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lco/uk/getmondo/d/t;-><init>()V

    .line 77
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 78
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/t;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/t;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 354
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 375
    :goto_0
    return-wide v4

    .line 357
    :cond_0
    const-class v0, Lco/uk/getmondo/d/t;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 358
    invoke-virtual {v6}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 359
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v3, Lco/uk/getmondo/d/t;

    invoke-virtual {v2, v3}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    check-cast v2, Lio/realm/ae$a;

    .line 360
    invoke-virtual {v6}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v3, p1

    .line 361
    check-cast v3, Lio/realm/af;

    invoke-interface {v3}, Lio/realm/af;->b()Ljava/lang/String;

    move-result-object v3

    .line 363
    if-nez v3, :cond_1

    .line 364
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 368
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-nez v7, :cond_2

    .line 369
    invoke-static {v6, v3}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 373
    :goto_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    iget-wide v2, v2, Lio/realm/ae$a;->b:J

    check-cast p1, Lio/realm/af;

    invoke-interface {p1}, Lio/realm/af;->c()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto :goto_0

    .line 366
    :cond_1
    invoke-static {v0, v1, v4, v5, v3}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto :goto_1

    .line 371
    :cond_2
    invoke-static {v3}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public static a(Lco/uk/getmondo/d/t;IILjava/util/Map;)Lco/uk/getmondo/d/t;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/t;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/d/t;"
        }
    .end annotation

    .prologue
    .line 464
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 465
    :cond_0
    const/4 v0, 0x0

    .line 484
    :goto_0
    return-object v0

    .line 467
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 469
    if-nez v0, :cond_2

    .line 470
    new-instance v1, Lco/uk/getmondo/d/t;

    invoke-direct {v1}, Lco/uk/getmondo/d/t;-><init>()V

    .line 471
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 480
    check-cast v0, Lio/realm/af;

    .line 481
    check-cast p0, Lio/realm/af;

    .line 482
    invoke-interface {p0}, Lio/realm/af;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/af;->a(Ljava/lang/String;)V

    .line 483
    invoke-interface {p0}, Lio/realm/af;->c()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/af;->a(J)V

    move-object v0, v1

    .line 484
    goto :goto_0

    .line 474
    :cond_2
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_3

    .line 475
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/d/t;

    goto :goto_0

    .line 477
    :cond_3
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/d/t;

    .line 478
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto :goto_1
.end method

.method static a(Lio/realm/av;Lco/uk/getmondo/d/t;Lco/uk/getmondo/d/t;Ljava/util/Map;)Lco/uk/getmondo/d/t;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/t;",
            "Lco/uk/getmondo/d/t;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/t;"
        }
    .end annotation

    .prologue
    .line 488
    move-object v0, p1

    check-cast v0, Lio/realm/af;

    .line 489
    check-cast p2, Lio/realm/af;

    .line 490
    invoke-interface {p2}, Lio/realm/af;->c()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/af;->a(J)V

    .line 491
    return-object p1
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/t;ZLjava/util/Map;)Lco/uk/getmondo/d/t;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/t;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/t;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 292
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-wide v2, v2, Lio/realm/g;->c:J

    iget-wide v6, p0, Lio/realm/av;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 293
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 295
    :cond_0
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 332
    :goto_0
    return-object p1

    .line 298
    :cond_1
    sget-object v2, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v2}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/g$b;

    .line 299
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/l;

    .line 300
    if-eqz v3, :cond_2

    .line 301
    check-cast v3, Lco/uk/getmondo/d/t;

    move-object p1, v3

    goto :goto_0

    .line 304
    :cond_2
    const/4 v5, 0x0

    .line 306
    if-eqz p2, :cond_6

    .line 307
    const-class v3, Lco/uk/getmondo/d/t;

    invoke-virtual {p0, v3}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 308
    invoke-virtual {v8}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v3, p1

    .line 309
    check-cast v3, Lio/realm/af;

    invoke-interface {v3}, Lio/realm/af;->b()Ljava/lang/String;

    move-result-object v3

    .line 311
    if-nez v3, :cond_3

    .line 312
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v6

    .line 316
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-eqz v3, :cond_4

    .line 318
    :try_start_0
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    iget-object v3, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/t;

    invoke-virtual {v3, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 319
    new-instance v4, Lio/realm/ae;

    invoke-direct {v4}, Lio/realm/ae;-><init>()V

    .line 320
    move-object v0, v4

    check-cast v0, Lio/realm/internal/l;

    move-object v3, v0

    invoke-interface {p3, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    move v2, p2

    .line 329
    :goto_2
    if-eqz v2, :cond_5

    .line 330
    invoke-static {p0, v4, p1, p3}, Lio/realm/ae;->a(Lio/realm/av;Lco/uk/getmondo/d/t;Lco/uk/getmondo/d/t;Ljava/util/Map;)Lco/uk/getmondo/d/t;

    move-result-object p1

    goto :goto_0

    .line 314
    :cond_3
    invoke-virtual {v8, v6, v7, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 322
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    throw v3

    :cond_4
    move v2, v4

    move-object v4, v5

    .line 325
    goto :goto_2

    .line 332
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/ae;->b(Lio/realm/av;Lco/uk/getmondo/d/t;ZLjava/util/Map;)Lco/uk/getmondo/d/t;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v2, p2

    move-object v4, v5

    goto :goto_2
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/ae$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0x2

    .line 146
    const-string v0, "class_LocalSpend"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'LocalSpend\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_0
    const-string v0, "class_LocalSpend"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 150
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 151
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 152
    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 153
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 2 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_1
    if-eqz p1, :cond_3

    .line 156
    const-string v0, "Field count is more than expected - expected 2 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 161
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 162
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 163
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 158
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 2 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_4
    new-instance v0, Lio/realm/ae$a;

    invoke-direct {v0, p0, v2}, Lio/realm/ae$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 168
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 169
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Primary key not defined for field \'currency\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_5
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lio/realm/ae$a;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 172
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field currency"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_6
    const-string v1, "currency"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 177
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'currency\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_7
    const-string v1, "currency"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_8

    .line 180
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'currency\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_8
    iget-wide v4, v0, Lio/realm/ae$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_9

    .line 183
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@PrimaryKey field \'currency\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_9
    const-string v1, "currency"

    invoke-virtual {v2, v1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->j(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 186
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Index not defined for field \'currency\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_a
    const-string v1, "amountValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 189
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'amountValue\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_b
    const-string v1, "amountValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_c

    .line 192
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'long\' for field \'amountValue\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_c
    iget-wide v4, v0, Lio/realm/ae$a;->b:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 195
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'amountValue\' does support null values in the existing Realm file. Use corresponding boxed type for field \'amountValue\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_d
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 434
    const-class v2, Lco/uk/getmondo/d/t;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v12

    .line 435
    invoke-virtual {v12}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    .line 436
    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/t;

    invoke-virtual {v4, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lio/realm/ae$a;

    .line 437
    invoke-virtual {v12}, Lio/realm/internal/Table;->d()J

    move-result-wide v14

    .line 439
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 440
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Lco/uk/getmondo/d/t;

    .line 441
    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 444
    instance-of v4, v8, Lio/realm/internal/l;

    if-eqz v4, :cond_1

    move-object v4, v8

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v4, v8

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v8

    .line 445
    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v8, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v4, v8

    .line 448
    check-cast v4, Lio/realm/af;

    invoke-interface {v4}, Lio/realm/af;->b()Ljava/lang/String;

    move-result-object v4

    .line 450
    if-nez v4, :cond_3

    .line 451
    invoke-static {v2, v3, v14, v15}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v6

    .line 455
    :goto_1
    const-wide/16 v16, -0x1

    cmp-long v5, v6, v16

    if-nez v5, :cond_2

    .line 456
    invoke-static {v12, v4}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v6

    .line 458
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v8, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    iget-wide v4, v11, Lio/realm/ae$a;->b:J

    check-cast v8, Lio/realm/af;

    invoke-interface {v8}, Lio/realm/af;->c()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 453
    :cond_3
    invoke-static {v2, v3, v14, v15, v4}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 461
    :cond_4
    return-void
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/t;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/t;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 411
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 430
    :goto_0
    return-wide v4

    .line 414
    :cond_0
    const-class v0, Lco/uk/getmondo/d/t;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 415
    invoke-virtual {v6}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 416
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v3, Lco/uk/getmondo/d/t;

    invoke-virtual {v2, v3}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    check-cast v2, Lio/realm/ae$a;

    .line 417
    invoke-virtual {v6}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v3, p1

    .line 418
    check-cast v3, Lio/realm/af;

    invoke-interface {v3}, Lio/realm/af;->b()Ljava/lang/String;

    move-result-object v3

    .line 420
    if-nez v3, :cond_2

    .line 421
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 425
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-nez v7, :cond_1

    .line 426
    invoke-static {v6, v3}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 428
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    iget-wide v2, v2, Lio/realm/ae$a;->b:J

    check-cast p1, Lio/realm/af;

    invoke-interface {p1}, Lio/realm/af;->c()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto :goto_0

    .line 423
    :cond_2
    invoke-static {v0, v1, v4, v5, v3}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto :goto_1
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/t;ZLjava/util/Map;)Lco/uk/getmondo/d/t;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/t;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/t;"
        }
    .end annotation

    .prologue
    .line 337
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 338
    if-eqz v0, :cond_0

    .line 339
    check-cast v0, Lco/uk/getmondo/d/t;

    .line 350
    :goto_0
    return-object v0

    .line 343
    :cond_0
    const-class v1, Lco/uk/getmondo/d/t;

    move-object v0, p1

    check-cast v0, Lio/realm/af;

    invoke-interface {v0}, Lio/realm/af;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lio/realm/av;->a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/t;

    move-object v1, v0

    .line 344
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    check-cast p1, Lio/realm/af;

    move-object v1, v0

    .line 347
    check-cast v1, Lio/realm/af;

    .line 349
    invoke-interface {p1}, Lio/realm/af;->c()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lio/realm/af;->a(J)V

    goto :goto_0
.end method

.method public static d()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lio/realm/ae;->c:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    const-string v0, "class_LocalSpend"

    return-object v0
.end method

.method private static f()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 135
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "LocalSpend"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 136
    const-string v1, "currency"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 137
    const-string v7, "amountValue"

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 138
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 9

    .prologue
    .line 121
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 126
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ae;->a:Lio/realm/ae$a;

    iget-wide v2, v2, Lio/realm/ae$a;->b:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->a(JJJZ)V

    goto :goto_0

    .line 130
    :cond_1
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 131
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ae;->a:Lio/realm/ae$a;

    iget-wide v2, v1, Lio/realm/ae$a;->b:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/n;->a(JJ)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 109
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'currency\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 97
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 98
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ae;->a:Lio/realm/ae$a;

    iget-wide v2, v1, Lio/realm/ae$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 4

    .prologue
    .line 115
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 116
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ae;->a:Lio/realm/ae$a;

    iget-wide v2, v1, Lio/realm/ae$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 514
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 497
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    const-string v0, "Invalid object"

    .line 509
    :goto_0
    return-object v0

    .line 500
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "LocalSpend = proxy["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 501
    const-string v0, "{currency:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502
    invoke-virtual {p0}, Lio/realm/ae;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lio/realm/ae;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 504
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    const-string v0, "{amountValue:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    invoke-virtual {p0}, Lio/realm/ae;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 507
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 502
    :cond_1
    const-string v0, "null"

    goto :goto_1
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lio/realm/ae;->b:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 85
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 86
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/ae$a;

    iput-object v1, p0, Lio/realm/ae;->a:Lio/realm/ae$a;

    .line 87
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/ae;->b:Lio/realm/au;

    .line 88
    iget-object v1, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 89
    iget-object v1, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 90
    iget-object v1, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 91
    iget-object v1, p0, Lio/realm/ae;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method
