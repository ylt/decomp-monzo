.class abstract Lio/realm/g;
.super Ljava/lang/Object;
.source "BaseRealm.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/g$c;,
        Lio/realm/g$b;,
        Lio/realm/g$a;
    }
.end annotation


# static fields
.field static volatile a:Landroid/content/Context;

.field static final b:Lio/realm/internal/async/a;

.field public static final g:Lio/realm/g$c;


# instance fields
.field final c:J

.field protected final d:Lio/realm/ay;

.field protected e:Lio/realm/internal/SharedRealm;

.field protected final f:Lio/realm/bh;

.field private h:Lio/realm/aw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lio/realm/internal/async/a;->a()Lio/realm/internal/async/a;

    move-result-object v0

    sput-object v0, Lio/realm/g;->b:Lio/realm/internal/async/a;

    .line 755
    new-instance v0, Lio/realm/g$c;

    invoke-direct {v0}, Lio/realm/g$c;-><init>()V

    sput-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    return-void
.end method

.method constructor <init>(Lio/realm/aw;)V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p1}, Lio/realm/aw;->a()Lio/realm/ay;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/realm/g;-><init>(Lio/realm/ay;)V

    .line 82
    iput-object p1, p0, Lio/realm/g;->h:Lio/realm/aw;

    .line 83
    return-void
.end method

.method constructor <init>(Lio/realm/ay;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/g;->c:J

    .line 88
    iput-object p1, p0, Lio/realm/g;->d:Lio/realm/ay;

    .line 89
    iput-object v0, p0, Lio/realm/g;->h:Lio/realm/aw;

    .line 92
    instance-of v1, p0, Lio/realm/av;

    if-nez v1, :cond_0

    .line 93
    :goto_0
    const/4 v1, 0x1

    .line 91
    invoke-static {p1, v0, v1}, Lio/realm/internal/SharedRealm;->a(Lio/realm/ay;Lio/realm/internal/SharedRealm$c;Z)Lio/realm/internal/SharedRealm;

    move-result-object v0

    iput-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    .line 101
    new-instance v0, Lio/realm/bh;

    invoke-direct {v0, p0}, Lio/realm/bh;-><init>(Lio/realm/g;)V

    iput-object v0, p0, Lio/realm/g;->f:Lio/realm/bh;

    .line 102
    return-void

    .line 93
    :cond_0
    new-instance v0, Lio/realm/g$1;

    invoke-direct {v0, p0}, Lio/realm/g$1;-><init>(Lio/realm/g;)V

    goto :goto_0
.end method

.method static synthetic a(Lio/realm/g;)Lio/realm/aw;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lio/realm/g;->h:Lio/realm/aw;

    return-object v0
.end method

.method protected static a(Lio/realm/ay;Lio/realm/ba;Lio/realm/g$a;Lio/realm/exceptions/RealmMigrationNeededException;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 622
    if-nez p0, :cond_0

    .line 623
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RealmConfiguration must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 625
    :cond_0
    invoke-virtual {p0}, Lio/realm/ay;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 626
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Manual migrations are not supported for synced Realms"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 628
    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lio/realm/ay;->e()Lio/realm/ba;

    move-result-object v0

    if-nez v0, :cond_2

    .line 629
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RealmMigration must be provided"

    invoke-direct {v0, v1, v2, p3}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 632
    :cond_2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 634
    new-instance v1, Lio/realm/g$3;

    invoke-direct {v1, p0, v0, p1, p2}, Lio/realm/g$3;-><init>(Lio/realm/ay;Ljava/util/concurrent/atomic/AtomicBoolean;Lio/realm/ba;Lio/realm/g$a;)V

    invoke-static {p0, v1}, Lio/realm/aw;->a(Lio/realm/ay;Lio/realm/aw$a;)V

    .line 673
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 674
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot migrate a Realm file which doesn\'t exist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 675
    invoke-virtual {p0}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 677
    :cond_3
    return-void
.end method

.method static a(Lio/realm/ay;)Z
    .locals 2

    .prologue
    .line 576
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 577
    new-instance v1, Lio/realm/g$2;

    invoke-direct {v1, p0, v0}, Lio/realm/g$2;-><init>(Lio/realm/ay;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-static {p0, v1}, Lio/realm/aw;->a(Lio/realm/ay;Lio/realm/aw$a;)V

    .line 591
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method


# virtual methods
.method a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;JZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 532
    iget-object v0, p0, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->a(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    .line 533
    invoke-virtual {v0, p2, p3}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v3

    .line 534
    iget-object v0, p0, Lio/realm/g;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v0

    iget-object v1, p0, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v1, p1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v1, p1

    move-object v2, p0

    move v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/m;->a(Ljava/lang/Class;Ljava/lang/Object;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/bb;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/String;",
            "J)TE;"
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    .line 542
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 543
    :goto_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v0, p2}, Lio/realm/bh;->a(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    move-object v2, v0

    .line 546
    :goto_1
    if-eqz v1, :cond_3

    .line 548
    new-instance v1, Lio/realm/p;

    .line 549
    cmp-long v0, p3, v6

    if-eqz v0, :cond_2

    invoke-virtual {v2, p3, p4}, Lio/realm/internal/Table;->h(J)Lio/realm/internal/CheckedRow;

    move-result-object v0

    :goto_2
    invoke-direct {v1, p0, v0}, Lio/realm/p;-><init>(Lio/realm/g;Lio/realm/internal/n;)V

    move-object v0, v1

    .line 557
    :goto_3
    return-object v0

    :cond_0
    move v1, v5

    .line 542
    goto :goto_0

    .line 543
    :cond_1
    iget-object v0, p0, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->a(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    .line 549
    :cond_2
    sget-object v0, Lio/realm/internal/e;->a:Lio/realm/internal/e;

    goto :goto_2

    .line 552
    :cond_3
    iget-object v0, p0, Lio/realm/g;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v0

    .line 553
    cmp-long v1, p3, v6

    if-eqz v1, :cond_4

    invoke-virtual {v2, p3, p4}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v3

    :goto_4
    iget-object v1, p0, Lio/realm/g;->f:Lio/realm/bh;

    .line 554
    invoke-virtual {v1, p1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    move-object v1, p1

    move-object v2, p0

    .line 552
    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/m;->a(Ljava/lang/Class;Ljava/lang/Object;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    goto :goto_3

    .line 553
    :cond_4
    sget-object v3, Lio/realm/internal/e;->a:Lio/realm/internal/e;

    goto :goto_4
.end method

.method a(Ljava/lang/Class;Ljava/lang/String;Lio/realm/internal/UncheckedRow;)Lio/realm/bb;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/String;",
            "Lio/realm/internal/UncheckedRow;",
            ")TE;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 518
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 521
    :goto_0
    if-eqz v0, :cond_1

    .line 523
    new-instance v0, Lio/realm/p;

    invoke-static {p3}, Lio/realm/internal/CheckedRow;->a(Lio/realm/internal/UncheckedRow;)Lio/realm/internal/CheckedRow;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lio/realm/p;-><init>(Lio/realm/g;Lio/realm/internal/n;)V

    .line 528
    :goto_1
    return-object v0

    :cond_0
    move v0, v5

    .line 518
    goto :goto_0

    .line 525
    :cond_1
    iget-object v0, p0, Lio/realm/g;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->h()Lio/realm/internal/m;

    move-result-object v0

    iget-object v1, p0, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v1, p1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    .line 526
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    move-object v1, p1

    move-object v2, p0

    move-object v3, p3

    .line 525
    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/m;->a(Ljava/lang/Class;Ljava/lang/Object;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    goto :goto_1
.end method

.method a(J)V
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0, p1, p2}, Lio/realm/internal/SharedRealm;->a(J)V

    .line 504
    return-void
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lio/realm/g;->e()V

    .line 348
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0, p1}, Lio/realm/internal/SharedRealm;->a(Z)V

    .line 349
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lio/realm/g;->e()V

    .line 154
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->d()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/realm/g;->a(Z)V

    .line 344
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 358
    invoke-virtual {p0}, Lio/realm/g;->e()V

    .line 359
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->b()V

    .line 360
    return-void
.end method

.method public close()V
    .locals 4

    .prologue
    .line 452
    iget-wide v0, p0, Lio/realm/g;->c:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 453
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm access from incorrect thread. Realm instance can only be closed on the thread it was created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_0
    iget-object v0, p0, Lio/realm/g;->h:Lio/realm/aw;

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lio/realm/g;->h:Lio/realm/aw;

    invoke-virtual {v0, p0}, Lio/realm/aw;->a(Lio/realm/g;)V

    .line 461
    :goto_0
    return-void

    .line 459
    :cond_1
    invoke-virtual {p0}, Lio/realm/g;->j()V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 371
    invoke-virtual {p0}, Lio/realm/g;->e()V

    .line 372
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->c()V

    .line 373
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 379
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This Realm instance has already been closed, making it unusable."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384
    :cond_1
    iget-wide v0, p0, Lio/realm/g;->c:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 385
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm access from incorrect thread. Realm objects can only be accessed on the thread they were created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_2
    return-void
.end method

.method protected f()V
    .locals 2

    .prologue
    .line 399
    invoke-virtual {p0}, Lio/realm/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Changing Realm data can only be done from inside a transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 681
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 682
    const-string v0, "Remember to call close() on all Realm instances. Realm %s is being finalized without being closed, this can lead to running out of native memory."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lio/realm/g;->d:Lio/realm/ay;

    .line 684
    invoke-virtual {v3}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 682
    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 686
    iget-object v0, p0, Lio/realm/g;->h:Lio/realm/aw;

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p0, Lio/realm/g;->h:Lio/realm/aw;

    invoke-virtual {v0}, Lio/realm/aw;->c()V

    .line 690
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 691
    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lio/realm/g;->d:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lio/realm/ay;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lio/realm/g;->d:Lio/realm/ay;

    return-object v0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 467
    iput-object v1, p0, Lio/realm/g;->h:Lio/realm/aw;

    .line 468
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->close()V

    .line 470
    iput-object v1, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    .line 472
    :cond_0
    iget-object v0, p0, Lio/realm/g;->f:Lio/realm/bh;

    if-eqz v0, :cond_1

    .line 473
    iget-object v0, p0, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v0}, Lio/realm/bh;->a()V

    .line 475
    :cond_1
    return-void
.end method

.method public k()Lio/realm/bh;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lio/realm/g;->f:Lio/realm/bh;

    return-object v0
.end method

.method public l()V
    .locals 3

    .prologue
    .line 566
    invoke-virtual {p0}, Lio/realm/g;->e()V

    .line 567
    iget-object v0, p0, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v0}, Lio/realm/bh;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/be;

    .line 568
    iget-object v2, p0, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v0}, Lio/realm/be;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/realm/bh;->a(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->b()V

    goto :goto_0

    .line 570
    :cond_0
    return-void
.end method

.method m()Lio/realm/internal/SharedRealm;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    return-object v0
.end method
