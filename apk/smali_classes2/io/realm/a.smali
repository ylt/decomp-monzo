.class public Lio/realm/a;
.super Lco/uk/getmondo/d/b;
.source "AccountBalanceRealmProxy.java"

# interfaces
.implements Lio/realm/b;
.implements Lio/realm/internal/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/a$a;
    }
.end annotation


# static fields
.field private static final c:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lio/realm/a$a;

.field private b:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/d/b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lio/realm/az;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lio/realm/a;->o()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/a;->c:Lio/realm/internal/OsObjectSchemaInfo;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 84
    const-string v1, "accountId"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    const-string v1, "balanceAmountValue"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    const-string v1, "balanceCurrency"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    const-string v1, "spentTodayAmountValue"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    const-string v1, "spentTodayCurrency"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    const-string v1, "localSpend"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/a;->e:Ljava/util/List;

    .line 91
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lco/uk/getmondo/d/b;-><init>()V

    .line 94
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 95
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/b;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/b;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 638
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 639
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 681
    :cond_0
    return-wide v4

    .line 641
    :cond_1
    const-class v0, Lco/uk/getmondo/d/b;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 642
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 643
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/b;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/a$a;

    .line 644
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 645
    check-cast v2, Lio/realm/b;

    invoke-interface {v2}, Lio/realm/b;->e()Ljava/lang/String;

    move-result-object v2

    .line 647
    if-nez v2, :cond_5

    .line 648
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 652
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_6

    .line 653
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 657
    :goto_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 658
    iget-wide v2, v9, Lio/realm/a$a;->b:J

    move-object v6, p1

    check-cast v6, Lio/realm/b;

    invoke-interface {v6}, Lio/realm/b;->f()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 659
    check-cast v2, Lio/realm/b;

    invoke-interface {v2}, Lio/realm/b;->g()Ljava/lang/String;

    move-result-object v6

    .line 660
    if-eqz v6, :cond_2

    .line 661
    iget-wide v2, v9, Lio/realm/a$a;->c:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 663
    :cond_2
    iget-wide v2, v9, Lio/realm/a$a;->d:J

    move-object v6, p1

    check-cast v6, Lio/realm/b;

    invoke-interface {v6}, Lio/realm/b;->h()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 664
    check-cast v2, Lio/realm/b;

    invoke-interface {v2}, Lio/realm/b;->i()Ljava/lang/String;

    move-result-object v6

    .line 665
    if-eqz v6, :cond_3

    .line 666
    iget-wide v2, v9, Lio/realm/a$a;->e:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 669
    :cond_3
    check-cast p1, Lio/realm/b;

    invoke-interface {p1}, Lio/realm/b;->j()Lio/realm/az;

    move-result-object v6

    .line 670
    if-eqz v6, :cond_0

    .line 671
    iget-wide v2, v9, Lio/realm/a$a;->f:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeGetLinkView(JJJ)J

    move-result-wide v2

    .line 672
    invoke-virtual {v6}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/t;

    .line 673
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 674
    if-nez v1, :cond_4

    .line 675
    invoke-static {p0, v0, p2}, Lio/realm/ae;->a(Lio/realm/av;Lco/uk/getmondo/d/t;Ljava/util/Map;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 677
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Lio/realm/internal/LinkView;->nativeAdd(JJ)V

    goto :goto_2

    .line 650
    :cond_5
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_0

    .line 655
    :cond_6
    invoke-static {v2}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static a(Lco/uk/getmondo/d/b;IILjava/util/Map;)Lco/uk/getmondo/d/b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/b;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/d/b;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 845
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    :cond_0
    move-object v0, v2

    .line 883
    :goto_0
    return-object v0

    .line 848
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 850
    if-nez v0, :cond_3

    .line 851
    new-instance v1, Lco/uk/getmondo/d/b;

    invoke-direct {v1}, Lco/uk/getmondo/d/b;-><init>()V

    .line 852
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 861
    check-cast v0, Lio/realm/b;

    .line 862
    check-cast p0, Lio/realm/b;

    .line 863
    invoke-interface {p0}, Lio/realm/b;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/b;->a(Ljava/lang/String;)V

    .line 864
    invoke-interface {p0}, Lio/realm/b;->f()J

    move-result-wide v4

    invoke-interface {v0, v4, v5}, Lio/realm/b;->a(J)V

    .line 865
    invoke-interface {p0}, Lio/realm/b;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/b;->b(Ljava/lang/String;)V

    .line 866
    invoke-interface {p0}, Lio/realm/b;->h()J

    move-result-wide v4

    invoke-interface {v0, v4, v5}, Lio/realm/b;->b(J)V

    .line 867
    invoke-interface {p0}, Lio/realm/b;->i()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/b;->c(Ljava/lang/String;)V

    .line 870
    if-ne p1, p2, :cond_5

    .line 871
    invoke-interface {v0, v2}, Lio/realm/b;->a(Lio/realm/az;)V

    :cond_2
    move-object v0, v1

    .line 883
    goto :goto_0

    .line 855
    :cond_3
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_4

    .line 856
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/d/b;

    goto :goto_0

    .line 858
    :cond_4
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/d/b;

    .line 859
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto :goto_1

    .line 873
    :cond_5
    invoke-interface {p0}, Lio/realm/b;->j()Lio/realm/az;

    move-result-object v3

    .line 874
    new-instance v4, Lio/realm/az;

    invoke-direct {v4}, Lio/realm/az;-><init>()V

    .line 875
    invoke-interface {v0, v4}, Lio/realm/b;->a(Lio/realm/az;)V

    .line 876
    add-int/lit8 v5, p1, 0x1

    .line 877
    invoke-virtual {v3}, Lio/realm/az;->size()I

    move-result v6

    .line 878
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v6, :cond_2

    .line 879
    invoke-virtual {v3, v2}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/t;

    invoke-static {v0, v5, p2, p3}, Lio/realm/ae;->a(Lco/uk/getmondo/d/t;IILjava/util/Map;)Lco/uk/getmondo/d/t;

    move-result-object v0

    .line 880
    invoke-virtual {v4, v0}, Lio/realm/az;->a(Lio/realm/bb;)Z

    .line 878
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2
.end method

.method static a(Lio/realm/av;Lco/uk/getmondo/d/b;Lco/uk/getmondo/d/b;Ljava/util/Map;)Lco/uk/getmondo/d/b;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/b;",
            "Lco/uk/getmondo/d/b;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/b;"
        }
    .end annotation

    .prologue
    .line 887
    move-object v0, p1

    check-cast v0, Lio/realm/b;

    .line 888
    check-cast p2, Lio/realm/b;

    .line 889
    invoke-interface {p2}, Lio/realm/b;->f()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/b;->a(J)V

    .line 890
    invoke-interface {p2}, Lio/realm/b;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/b;->b(Ljava/lang/String;)V

    .line 891
    invoke-interface {p2}, Lio/realm/b;->h()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/b;->b(J)V

    .line 892
    invoke-interface {p2}, Lio/realm/b;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/b;->c(Ljava/lang/String;)V

    .line 893
    invoke-interface {p2}, Lio/realm/b;->j()Lio/realm/az;

    move-result-object v3

    .line 894
    invoke-interface {v0}, Lio/realm/b;->j()Lio/realm/az;

    move-result-object v4

    .line 895
    invoke-virtual {v4}, Lio/realm/az;->clear()V

    .line 896
    if-eqz v3, :cond_1

    .line 897
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v3}, Lio/realm/az;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 898
    invoke-virtual {v3, v2}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/t;

    .line 899
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/t;

    .line 900
    if-eqz v1, :cond_0

    .line 901
    invoke-virtual {v4, v1}, Lio/realm/az;->a(Lio/realm/bb;)Z

    .line 897
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 903
    :cond_0
    const/4 v1, 0x1

    invoke-static {p0, v0, v1, p3}, Lio/realm/ae;->a(Lio/realm/av;Lco/uk/getmondo/d/t;ZLjava/util/Map;)Lco/uk/getmondo/d/t;

    move-result-object v0

    invoke-virtual {v4, v0}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_1

    .line 907
    :cond_1
    return-object p1
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/b;ZLjava/util/Map;)Lco/uk/getmondo/d/b;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/b;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/b;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 558
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-wide v2, v2, Lio/realm/g;->c:J

    iget-wide v6, p0, Lio/realm/av;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 559
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 561
    :cond_0
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 598
    :goto_0
    return-object p1

    .line 564
    :cond_1
    sget-object v2, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v2}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/g$b;

    .line 565
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/l;

    .line 566
    if-eqz v3, :cond_2

    .line 567
    check-cast v3, Lco/uk/getmondo/d/b;

    move-object p1, v3

    goto :goto_0

    .line 570
    :cond_2
    const/4 v5, 0x0

    .line 572
    if-eqz p2, :cond_6

    .line 573
    const-class v3, Lco/uk/getmondo/d/b;

    invoke-virtual {p0, v3}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 574
    invoke-virtual {v8}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v3, p1

    .line 575
    check-cast v3, Lio/realm/b;

    invoke-interface {v3}, Lio/realm/b;->e()Ljava/lang/String;

    move-result-object v3

    .line 577
    if-nez v3, :cond_3

    .line 578
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v6

    .line 582
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-eqz v3, :cond_4

    .line 584
    :try_start_0
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    iget-object v3, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/b;

    invoke-virtual {v3, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 585
    new-instance v4, Lio/realm/a;

    invoke-direct {v4}, Lio/realm/a;-><init>()V

    .line 586
    move-object v0, v4

    check-cast v0, Lio/realm/internal/l;

    move-object v3, v0

    invoke-interface {p3, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 588
    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    move v2, p2

    .line 595
    :goto_2
    if-eqz v2, :cond_5

    .line 596
    invoke-static {p0, v4, p1, p3}, Lio/realm/a;->a(Lio/realm/av;Lco/uk/getmondo/d/b;Lco/uk/getmondo/d/b;Ljava/util/Map;)Lco/uk/getmondo/d/b;

    move-result-object p1

    goto :goto_0

    .line 580
    :cond_3
    invoke-virtual {v8, v6, v7, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 588
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    throw v3

    :cond_4
    move v2, v4

    move-object v4, v5

    .line 591
    goto :goto_2

    .line 598
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/a;->b(Lio/realm/av;Lco/uk/getmondo/d/b;ZLjava/util/Map;)Lco/uk/getmondo/d/b;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v2, p2

    move-object v4, v5

    goto :goto_2
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/a$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0x6

    .line 302
    const-string v0, "class_AccountBalance"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'AccountBalance\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 305
    :cond_0
    const-string v0, "class_AccountBalance"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 306
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 307
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 308
    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 309
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 6 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_1
    if-eqz p1, :cond_3

    .line 312
    const-string v0, "Field count is more than expected - expected 6 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 318
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 319
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 314
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 6 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_4
    new-instance v0, Lio/realm/a$a;

    invoke-direct {v0, p0, v2}, Lio/realm/a$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 324
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 325
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Primary key not defined for field \'accountId\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_5
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lio/realm/a$a;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 328
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field accountId"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 332
    :cond_6
    const-string v1, "accountId"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 333
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'accountId\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_7
    const-string v1, "accountId"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_8

    .line 336
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'accountId\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_8
    iget-wide v4, v0, Lio/realm/a$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_9

    .line 339
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@PrimaryKey field \'accountId\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_9
    const-string v1, "accountId"

    invoke-virtual {v2, v1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->j(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 342
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Index not defined for field \'accountId\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 344
    :cond_a
    const-string v1, "balanceAmountValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 345
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'balanceAmountValue\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 347
    :cond_b
    const-string v1, "balanceAmountValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_c

    .line 348
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'long\' for field \'balanceAmountValue\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 350
    :cond_c
    iget-wide v4, v0, Lio/realm/a$a;->b:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 351
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'balanceAmountValue\' does support null values in the existing Realm file. Use corresponding boxed type for field \'balanceAmountValue\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 353
    :cond_d
    const-string v1, "balanceCurrency"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 354
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'balanceCurrency\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 356
    :cond_e
    const-string v1, "balanceCurrency"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_f

    .line 357
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'balanceCurrency\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_f
    iget-wide v4, v0, Lio/realm/a$a;->c:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_10

    .line 360
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'balanceCurrency\' is required. Either set @Required to field \'balanceCurrency\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 362
    :cond_10
    const-string v1, "spentTodayAmountValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 363
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'spentTodayAmountValue\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 365
    :cond_11
    const-string v1, "spentTodayAmountValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_12

    .line 366
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'long\' for field \'spentTodayAmountValue\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 368
    :cond_12
    iget-wide v4, v0, Lio/realm/a$a;->d:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 369
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'spentTodayAmountValue\' does support null values in the existing Realm file. Use corresponding boxed type for field \'spentTodayAmountValue\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 371
    :cond_13
    const-string v1, "spentTodayCurrency"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 372
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'spentTodayCurrency\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 374
    :cond_14
    const-string v1, "spentTodayCurrency"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_15

    .line 375
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'spentTodayCurrency\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 377
    :cond_15
    iget-wide v4, v0, Lio/realm/a$a;->e:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_16

    .line 378
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'spentTodayCurrency\' is required. Either set @Required to field \'spentTodayCurrency\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_16
    const-string v1, "localSpend"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 381
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'localSpend\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_17
    const-string v1, "localSpend"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_18

    .line 384
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'LocalSpend\' for field \'localSpend\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 386
    :cond_18
    const-string v1, "class_LocalSpend"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 387
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_LocalSpend\' for field \'localSpend\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 389
    :cond_19
    const-string v1, "class_LocalSpend"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 390
    iget-wide v4, v0, Lio/realm/a$a;->f:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v3

    invoke-virtual {v3, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 391
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmList type for field \'localSpend\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/a$a;->f:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 394
    :cond_1a
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 788
    const-class v2, Lco/uk/getmondo/d/b;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v13

    .line 789
    invoke-virtual {v13}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    .line 790
    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/b;

    invoke-virtual {v4, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lio/realm/a$a;

    .line 791
    invoke-virtual {v13}, Lio/realm/internal/Table;->d()J

    move-result-wide v14

    .line 793
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 794
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Lco/uk/getmondo/d/b;

    .line 795
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 798
    instance-of v4, v12, Lio/realm/internal/l;

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v12

    .line 799
    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v4, v12

    .line 802
    check-cast v4, Lio/realm/b;

    invoke-interface {v4}, Lio/realm/b;->e()Ljava/lang/String;

    move-result-object v4

    .line 804
    if-nez v4, :cond_4

    .line 805
    invoke-static {v2, v3, v14, v15}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v6

    .line 809
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 810
    invoke-static {v13, v4}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v6

    .line 812
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    iget-wide v4, v11, Lio/realm/a$a;->b:J

    move-object v8, v12

    check-cast v8, Lio/realm/b;

    invoke-interface {v8}, Lio/realm/b;->f()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v4, v12

    .line 814
    check-cast v4, Lio/realm/b;

    invoke-interface {v4}, Lio/realm/b;->g()Ljava/lang/String;

    move-result-object v8

    .line 815
    if-eqz v8, :cond_5

    .line 816
    iget-wide v4, v11, Lio/realm/a$a;->c:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 820
    :goto_2
    iget-wide v4, v11, Lio/realm/a$a;->d:J

    move-object v8, v12

    check-cast v8, Lio/realm/b;

    invoke-interface {v8}, Lio/realm/b;->h()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v4, v12

    .line 821
    check-cast v4, Lio/realm/b;

    invoke-interface {v4}, Lio/realm/b;->i()Ljava/lang/String;

    move-result-object v8

    .line 822
    if-eqz v8, :cond_6

    .line 823
    iget-wide v4, v11, Lio/realm/a$a;->e:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 828
    :goto_3
    iget-wide v4, v11, Lio/realm/a$a;->f:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeGetLinkView(JJJ)J

    move-result-wide v6

    .line 829
    invoke-static {v6, v7}, Lio/realm/internal/LinkView;->nativeClear(J)V

    .line 830
    check-cast v12, Lio/realm/b;

    invoke-interface {v12}, Lio/realm/b;->j()Lio/realm/az;

    move-result-object v4

    .line 831
    if-eqz v4, :cond_0

    .line 832
    invoke-virtual {v4}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/d/t;

    .line 833
    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 834
    if-nez v5, :cond_3

    .line 835
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v4, v1}, Lio/realm/ae;->b(Lio/realm/av;Lco/uk/getmondo/d/t;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 837
    :cond_3
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v6, v7, v4, v5}, Lio/realm/internal/LinkView;->nativeAdd(JJ)V

    goto :goto_4

    .line 807
    :cond_4
    invoke-static {v2, v3, v14, v15, v4}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v6

    goto/16 :goto_1

    .line 818
    :cond_5
    iget-wide v4, v11, Lio/realm/a$a;->c:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_2

    .line 825
    :cond_6
    iget-wide v4, v11, Lio/realm/a$a;->e:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_3

    .line 842
    :cond_7
    return-void
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/b;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/b;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 738
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 739
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 784
    :cond_0
    return-wide v4

    .line 741
    :cond_1
    const-class v0, Lco/uk/getmondo/d/b;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 742
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 743
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/b;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/a$a;

    .line 744
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 745
    check-cast v2, Lio/realm/b;

    invoke-interface {v2}, Lio/realm/b;->e()Ljava/lang/String;

    move-result-object v2

    .line 747
    if-nez v2, :cond_4

    .line 748
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 752
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_2

    .line 753
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 755
    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 756
    iget-wide v2, v9, Lio/realm/a$a;->b:J

    move-object v6, p1

    check-cast v6, Lio/realm/b;

    invoke-interface {v6}, Lio/realm/b;->f()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 757
    check-cast v2, Lio/realm/b;

    invoke-interface {v2}, Lio/realm/b;->g()Ljava/lang/String;

    move-result-object v6

    .line 758
    if-eqz v6, :cond_5

    .line 759
    iget-wide v2, v9, Lio/realm/a$a;->c:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 763
    :goto_1
    iget-wide v2, v9, Lio/realm/a$a;->d:J

    move-object v6, p1

    check-cast v6, Lio/realm/b;

    invoke-interface {v6}, Lio/realm/b;->h()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 764
    check-cast v2, Lio/realm/b;

    invoke-interface {v2}, Lio/realm/b;->i()Ljava/lang/String;

    move-result-object v6

    .line 765
    if-eqz v6, :cond_6

    .line 766
    iget-wide v2, v9, Lio/realm/a$a;->e:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 771
    :goto_2
    iget-wide v2, v9, Lio/realm/a$a;->f:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeGetLinkView(JJJ)J

    move-result-wide v2

    .line 772
    invoke-static {v2, v3}, Lio/realm/internal/LinkView;->nativeClear(J)V

    .line 773
    check-cast p1, Lio/realm/b;

    invoke-interface {p1}, Lio/realm/b;->j()Lio/realm/az;

    move-result-object v0

    .line 774
    if-eqz v0, :cond_0

    .line 775
    invoke-virtual {v0}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/t;

    .line 776
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 777
    if-nez v1, :cond_3

    .line 778
    invoke-static {p0, v0, p2}, Lio/realm/ae;->b(Lio/realm/av;Lco/uk/getmondo/d/t;Ljava/util/Map;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 780
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Lio/realm/internal/LinkView;->nativeAdd(JJ)V

    goto :goto_3

    .line 750
    :cond_4
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_0

    .line 761
    :cond_5
    iget-wide v2, v9, Lio/realm/a$a;->c:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_1

    .line 768
    :cond_6
    iget-wide v2, v9, Lio/realm/a$a;->e:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_2
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/b;ZLjava/util/Map;)Lco/uk/getmondo/d/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/b;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/b;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 603
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 604
    if-eqz v0, :cond_1

    .line 605
    check-cast v0, Lco/uk/getmondo/d/b;

    .line 634
    :cond_0
    return-object v0

    .line 609
    :cond_1
    const-class v1, Lco/uk/getmondo/d/b;

    move-object v0, p1

    check-cast v0, Lio/realm/b;

    invoke-interface {v0}, Lio/realm/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lio/realm/av;->a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/b;

    move-object v1, v0

    .line 610
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 612
    check-cast p1, Lio/realm/b;

    move-object v1, v0

    .line 613
    check-cast v1, Lio/realm/b;

    .line 615
    invoke-interface {p1}, Lio/realm/b;->f()J

    move-result-wide v4

    invoke-interface {v1, v4, v5}, Lio/realm/b;->a(J)V

    .line 616
    invoke-interface {p1}, Lio/realm/b;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lio/realm/b;->b(Ljava/lang/String;)V

    .line 617
    invoke-interface {p1}, Lio/realm/b;->h()J

    move-result-wide v4

    invoke-interface {v1, v4, v5}, Lio/realm/b;->b(J)V

    .line 618
    invoke-interface {p1}, Lio/realm/b;->i()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lio/realm/b;->c(Ljava/lang/String;)V

    .line 620
    invoke-interface {p1}, Lio/realm/b;->j()Lio/realm/az;

    move-result-object v4

    .line 621
    if-eqz v4, :cond_0

    .line 622
    invoke-interface {v1}, Lio/realm/b;->j()Lio/realm/az;

    move-result-object v5

    move v3, v2

    .line 623
    :goto_0
    invoke-virtual {v4}, Lio/realm/az;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 624
    invoke-virtual {v4, v3}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/t;

    .line 625
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/t;

    .line 626
    if-eqz v2, :cond_2

    .line 627
    invoke-virtual {v5, v2}, Lio/realm/az;->a(Lio/realm/bb;)Z

    .line 623
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 629
    :cond_2
    invoke-static {p0, v1, p2, p3}, Lio/realm/ae;->a(Lio/realm/av;Lco/uk/getmondo/d/t;ZLjava/util/Map;)Lco/uk/getmondo/d/t;

    move-result-object v1

    invoke-virtual {v5, v1}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_1
.end method

.method public static l()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lio/realm/a;->c:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    const-string v0, "class_AccountBalance"

    return-object v0
.end method

.method private static o()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 287
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "AccountBalance"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 288
    const-string v1, "accountId"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 289
    const-string v7, "balanceAmountValue"

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 290
    const-string v7, "balanceCurrency"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 291
    const-string v7, "spentTodayAmountValue"

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 292
    const-string v1, "spentTodayCurrency"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 293
    const-string v1, "localSpend"

    sget-object v2, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    const-string v3, "LocalSpend"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 294
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 9

    .prologue
    .line 138
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 143
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v2, Lio/realm/a$a;->b:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->a(JJJZ)V

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 148
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->b:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/n;->a(JJ)V

    goto :goto_0
.end method

.method public a(Lio/realm/az;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/t;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 249
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 284
    :cond_0
    return-void

    .line 252
    :cond_1
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "localSpend"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lio/realm/az;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 256
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    .line 258
    new-instance v2, Lio/realm/az;

    invoke-direct {v2}, Lio/realm/az;-><init>()V

    .line 259
    invoke-virtual {p1}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/t;

    .line 260
    if-eqz v1, :cond_2

    invoke-static {v1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 261
    :cond_2
    invoke-virtual {v2, v1}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_0

    .line 263
    :cond_3
    invoke-virtual {v0, v1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v1

    invoke-virtual {v2, v1}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_0

    :cond_4
    move-object p1, v2

    .line 269
    :cond_5
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 270
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->n(J)Lio/realm/internal/LinkView;

    move-result-object v2

    .line 271
    invoke-virtual {v2}, Lio/realm/internal/LinkView;->a()V

    .line 272
    if-eqz p1, :cond_0

    .line 275
    invoke-virtual {p1}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    .line 276
    invoke-static {v0}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 277
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Each element of \'value\' must be a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move-object v1, v0

    .line 279
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {v1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    iget-object v4, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eq v1, v4, :cond_8

    .line 280
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Each element of \'value\' must belong to the same Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_8
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/LinkView;->b(J)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 126
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'accountId\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(J)V
    .locals 9

    .prologue
    .line 190
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 195
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v2, Lio/realm/a$a;->d:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->a(JJJZ)V

    goto :goto_0

    .line 199
    :cond_1
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 200
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->d:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/n;->a(JJ)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 160
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 165
    if-nez p1, :cond_1

    .line 166
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v0, Lio/realm/a$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 169
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v1, v1, Lio/realm/a$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 173
    :cond_2
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 174
    if-nez p1, :cond_3

    .line 175
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 178
    :cond_3
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->c:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 212
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 217
    if-nez p1, :cond_1

    .line 218
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v0, Lio/realm/a$a;->e:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 221
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v1, v1, Lio/realm/a$a;->e:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 226
    if-nez p1, :cond_3

    .line 227
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 230
    :cond_3
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->e:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 115
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 133
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 155
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()J
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 185
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public i()Ljava/lang/String;
    .locals 4

    .prologue
    .line 206
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 207
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Lio/realm/az;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 237
    iget-object v0, p0, Lio/realm/a;->d:Lio/realm/az;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lio/realm/a;->d:Lio/realm/az;

    .line 242
    :goto_0
    return-object v0

    .line 240
    :cond_0
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    iget-wide v2, v1, Lio/realm/a$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->n(J)Lio/realm/internal/LinkView;

    move-result-object v0

    .line 241
    new-instance v1, Lio/realm/az;

    const-class v2, Lco/uk/getmondo/d/t;

    iget-object v3, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v3}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lio/realm/az;-><init>(Ljava/lang/Class;Lio/realm/internal/LinkView;Lio/realm/g;)V

    iput-object v1, p0, Lio/realm/a;->d:Lio/realm/az;

    .line 242
    iget-object v0, p0, Lio/realm/a;->d:Lio/realm/az;

    goto :goto_0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 946
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 913
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 914
    const-string v0, "Invalid object"

    .line 941
    :goto_0
    return-object v0

    .line 916
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "AccountBalance = proxy["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 917
    const-string v0, "{accountId:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 918
    invoke-virtual {p0}, Lio/realm/a;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lio/realm/a;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 919
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 920
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    const-string v0, "{balanceAmountValue:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 922
    invoke-virtual {p0}, Lio/realm/a;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 923
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 924
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 925
    const-string v0, "{balanceCurrency:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 926
    invoke-virtual {p0}, Lio/realm/a;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lio/realm/a;->g()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 927
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 928
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 929
    const-string v0, "{spentTodayAmountValue:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 930
    invoke-virtual {p0}, Lio/realm/a;->h()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 931
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 932
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 933
    const-string v0, "{spentTodayCurrency:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 934
    invoke-virtual {p0}, Lio/realm/a;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lio/realm/a;->i()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 935
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 936
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 937
    const-string v0, "{localSpend:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 938
    const-string v0, "RealmList<LocalSpend>["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/a;->j()Lio/realm/az;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/az;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 939
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 940
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 941
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 918
    :cond_1
    const-string v0, "null"

    goto/16 :goto_1

    .line 926
    :cond_2
    const-string v0, "null"

    goto :goto_2

    .line 934
    :cond_3
    const-string v0, "null"

    goto :goto_3
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lio/realm/a;->b:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 102
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 103
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/a$a;

    iput-object v1, p0, Lio/realm/a;->a:Lio/realm/a$a;

    .line 104
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/a;->b:Lio/realm/au;

    .line 105
    iget-object v1, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 106
    iget-object v1, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 107
    iget-object v1, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 108
    iget-object v1, p0, Lio/realm/a;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method
