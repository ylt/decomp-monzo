.class final Lio/realm/ac$a;
.super Lio/realm/internal/c;
.source "KycRejectedRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/ac;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 43
    const-string v0, "id"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/ac$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/ac$a;->a:J

    .line 44
    const-string v0, "rejectedReason"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/ac$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/ac$a;->b:J

    .line 45
    const-string v0, "customerRejectedMessage"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/ac$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/ac$a;->c:J

    .line 46
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 50
    invoke-virtual {p0, p1, p0}, Lio/realm/ac$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 51
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lio/realm/ac$a;

    invoke-direct {v0, p0, p1}, Lio/realm/ac$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 60
    check-cast p1, Lio/realm/ac$a;

    .line 61
    check-cast p2, Lio/realm/ac$a;

    .line 62
    iget-wide v0, p1, Lio/realm/ac$a;->a:J

    iput-wide v0, p2, Lio/realm/ac$a;->a:J

    .line 63
    iget-wide v0, p1, Lio/realm/ac$a;->b:J

    iput-wide v0, p2, Lio/realm/ac$a;->b:J

    .line 64
    iget-wide v0, p1, Lio/realm/ac$a;->c:J

    iput-wide v0, p2, Lio/realm/ac$a;->c:J

    .line 65
    return-void
.end method
