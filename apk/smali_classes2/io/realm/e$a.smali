.class final Lio/realm/e$a;
.super Lio/realm/internal/c;
.source "BankDetailsRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 43
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 44
    const-string v0, "id"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/e$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/e$a;->a:J

    .line 45
    const-string v0, "name"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/e$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/e$a;->b:J

    .line 46
    const-string v0, "sortCode"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/e$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/e$a;->c:J

    .line 47
    const-string v0, "accountNumber"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/e$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/e$a;->d:J

    .line 48
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 52
    invoke-virtual {p0, p1, p0}, Lio/realm/e$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 53
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lio/realm/e$a;

    invoke-direct {v0, p0, p1}, Lio/realm/e$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 62
    check-cast p1, Lio/realm/e$a;

    .line 63
    check-cast p2, Lio/realm/e$a;

    .line 64
    iget-wide v0, p1, Lio/realm/e$a;->a:J

    iput-wide v0, p2, Lio/realm/e$a;->a:J

    .line 65
    iget-wide v0, p1, Lio/realm/e$a;->b:J

    iput-wide v0, p2, Lio/realm/e$a;->b:J

    .line 66
    iget-wide v0, p1, Lio/realm/e$a;->c:J

    iput-wide v0, p2, Lio/realm/e$a;->c:J

    .line 67
    iget-wide v0, p1, Lio/realm/e$a;->d:J

    iput-wide v0, p2, Lio/realm/e$a;->d:J

    .line 68
    return-void
.end method
