.class public Lio/realm/bg;
.super Lio/realm/an;
.source "RealmResults.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/bb;",
        ">",
        "Lio/realm/an",
        "<TE;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lio/realm/g;Lio/realm/internal/Collection;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/g;",
            "Lio/realm/internal/Collection;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lio/realm/an;-><init>(Lio/realm/g;Lio/realm/internal/Collection;Ljava/lang/Class;)V

    .line 83
    return-void
.end method

.method constructor <init>(Lio/realm/g;Lio/realm/internal/Collection;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1, p2, p3}, Lio/realm/an;-><init>(Lio/realm/g;Lio/realm/internal/Collection;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method private a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 213
    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    .line 214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    iget-object v0, p0, Lio/realm/bg;->a:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 217
    iget-object v0, p0, Lio/realm/bg;->a:Lio/realm/g;

    iget-object v0, v0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    iget-object v0, v0, Lio/realm/internal/SharedRealm;->d:Lio/realm/internal/a;

    const-string v1, "Listeners cannot be used on current thread."

    invoke-interface {v0, v1}, Lio/realm/internal/a;->a(Ljava/lang/String;)V

    .line 218
    return-void
.end method


# virtual methods
.method public bridge synthetic a(I)Lio/realm/bb;
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->a(I)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(ILio/realm/bb;)Lio/realm/bb;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lio/realm/an;->a(ILio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public a(Lio/realm/am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/am",
            "<",
            "Lio/realm/bg",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 208
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/bg;->a(Ljava/lang/Object;Z)V

    .line 209
    iget-object v0, p0, Lio/realm/bg;->d:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->addListener(Ljava/lang/Object;Lio/realm/am;)V

    .line 210
    return-void
.end method

.method public bridge synthetic a()Z
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lio/realm/an;->a()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Lio/realm/bb;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->a(Lio/realm/bb;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lio/realm/an;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic b(I)Lio/realm/bb;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->b(I)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(ILio/realm/bb;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lio/realm/an;->b(ILio/realm/bb;)V

    return-void
.end method

.method public b(Lio/realm/am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/am",
            "<",
            "Lio/realm/bg",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 262
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lio/realm/bg;->a(Ljava/lang/Object;Z)V

    .line 263
    iget-object v0, p0, Lio/realm/bg;->d:Lio/realm/internal/Collection;

    invoke-virtual {v0, p0, p1}, Lio/realm/internal/Collection;->removeListener(Ljava/lang/Object;Lio/realm/am;)V

    .line 264
    return-void
.end method

.method public bridge synthetic b()Z
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lio/realm/an;->b()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lio/realm/bg;->a:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 116
    iget-object v0, p0, Lio/realm/bg;->d:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->isLoaded()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic clear()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0}, Lio/realm/an;->clear()V

    return-void
.end method

.method public bridge synthetic contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lio/realm/bg;->a:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 132
    iget-object v0, p0, Lio/realm/bg;->d:Lio/realm/internal/Collection;

    invoke-virtual {v0}, Lio/realm/internal/Collection;->load()V

    .line 133
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lio/realm/an;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lio/realm/an;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1}, Lio/realm/an;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic size()I
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lio/realm/an;->size()I

    move-result v0

    return v0
.end method
