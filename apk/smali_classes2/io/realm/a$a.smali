.class final Lio/realm/a$a;
.super Lio/realm/internal/c;
.source "AccountBalanceRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:J

.field f:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 46
    const-string v0, "accountId"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/a$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/a$a;->a:J

    .line 47
    const-string v0, "balanceAmountValue"

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/a$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/a$a;->b:J

    .line 48
    const-string v0, "balanceCurrency"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/a$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/a$a;->c:J

    .line 49
    const-string v0, "spentTodayAmountValue"

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/a$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/a$a;->d:J

    .line 50
    const-string v0, "spentTodayCurrency"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/a$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/a$a;->e:J

    .line 51
    const-string v0, "localSpend"

    sget-object v1, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/a$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/a$a;->f:J

    .line 52
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 56
    invoke-virtual {p0, p1, p0}, Lio/realm/a$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 57
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lio/realm/a$a;

    invoke-direct {v0, p0, p1}, Lio/realm/a$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 66
    check-cast p1, Lio/realm/a$a;

    .line 67
    check-cast p2, Lio/realm/a$a;

    .line 68
    iget-wide v0, p1, Lio/realm/a$a;->a:J

    iput-wide v0, p2, Lio/realm/a$a;->a:J

    .line 69
    iget-wide v0, p1, Lio/realm/a$a;->b:J

    iput-wide v0, p2, Lio/realm/a$a;->b:J

    .line 70
    iget-wide v0, p1, Lio/realm/a$a;->c:J

    iput-wide v0, p2, Lio/realm/a$a;->c:J

    .line 71
    iget-wide v0, p1, Lio/realm/a$a;->d:J

    iput-wide v0, p2, Lio/realm/a$a;->d:J

    .line 72
    iget-wide v0, p1, Lio/realm/a$a;->e:J

    iput-wide v0, p2, Lio/realm/a$a;->e:J

    .line 73
    iget-wide v0, p1, Lio/realm/a$a;->f:J

    iput-wide v0, p2, Lio/realm/a$a;->f:J

    .line 74
    return-void
.end method
