.class public final Lio/realm/au;
.super Ljava/lang/Object;
.source "ProxyState.java"

# interfaces
.implements Lio/realm/internal/j$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/au$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/bb;",
        ">",
        "Ljava/lang/Object;",
        "Lio/realm/internal/j$a;"
    }
.end annotation


# static fields
.field private static i:Lio/realm/au$a;


# instance fields
.field private a:Lio/realm/bb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Lio/realm/internal/n;

.field private d:Lio/realm/internal/OsObject;

.field private e:Lio/realm/g;

.field private f:Z

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lio/realm/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/internal/i",
            "<",
            "Lio/realm/internal/OsObject$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 83
    new-instance v0, Lio/realm/au$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lio/realm/au$a;-><init>(Lio/realm/au$1;)V

    sput-object v0, Lio/realm/au;->i:Lio/realm/au$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/au;->b:Z

    .line 81
    new-instance v0, Lio/realm/internal/i;

    invoke-direct {v0}, Lio/realm/internal/i;-><init>()V

    iput-object v0, p0, Lio/realm/au;->h:Lio/realm/internal/i;

    .line 85
    return-void
.end method

.method public constructor <init>(Lio/realm/bb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/au;->b:Z

    .line 81
    new-instance v0, Lio/realm/internal/i;

    invoke-direct {v0}, Lio/realm/internal/i;-><init>()V

    iput-object v0, p0, Lio/realm/au;->h:Lio/realm/internal/i;

    .line 88
    iput-object p1, p0, Lio/realm/au;->a:Lio/realm/bb;

    .line 89
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lio/realm/au;->h:Lio/realm/internal/i;

    sget-object v1, Lio/realm/au;->i:Lio/realm/au$a;

    invoke-virtual {v0, v1}, Lio/realm/internal/i;->a(Lio/realm/internal/i$a;)V

    .line 128
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lio/realm/au;->e:Lio/realm/g;

    iget-object v0, v0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/au;->e:Lio/realm/g;

    iget-object v0, v0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/realm/au;->c:Lio/realm/internal/n;

    invoke-interface {v0}, Lio/realm/internal/n;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v0, p0, Lio/realm/au;->d:Lio/realm/internal/OsObject;

    if-nez v0, :cond_0

    .line 173
    new-instance v1, Lio/realm/internal/OsObject;

    iget-object v0, p0, Lio/realm/au;->e:Lio/realm/g;

    iget-object v2, v0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    iget-object v0, p0, Lio/realm/au;->c:Lio/realm/internal/n;

    check-cast v0, Lio/realm/internal/UncheckedRow;

    invoke-direct {v1, v2, v0}, Lio/realm/internal/OsObject;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/UncheckedRow;)V

    iput-object v1, p0, Lio/realm/au;->d:Lio/realm/internal/OsObject;

    .line 174
    iget-object v0, p0, Lio/realm/au;->d:Lio/realm/internal/OsObject;

    iget-object v1, p0, Lio/realm/au;->h:Lio/realm/internal/i;

    invoke-virtual {v0, v1}, Lio/realm/internal/OsObject;->a(Lio/realm/internal/i;)V

    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/au;->h:Lio/realm/internal/i;

    goto :goto_0
.end method


# virtual methods
.method public a()Lio/realm/g;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lio/realm/au;->e:Lio/realm/g;

    return-object v0
.end method

.method public a(Lio/realm/g;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lio/realm/au;->e:Lio/realm/g;

    .line 97
    return-void
.end method

.method public a(Lio/realm/internal/n;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lio/realm/au;->c:Lio/realm/internal/n;

    .line 105
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    iput-object p1, p0, Lio/realm/au;->g:Ljava/util/List;

    .line 121
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 112
    iput-boolean p1, p0, Lio/realm/au;->f:Z

    .line 113
    return-void
.end method

.method public b()Lio/realm/internal/n;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lio/realm/au;->c:Lio/realm/internal/n;

    return-object v0
.end method

.method public b(Lio/realm/internal/n;)V
    .locals 1

    .prologue
    .line 192
    iput-object p1, p0, Lio/realm/au;->c:Lio/realm/internal/n;

    .line 194
    invoke-direct {p0}, Lio/realm/au;->g()V

    .line 195
    invoke-interface {p1}, Lio/realm/internal/n;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    invoke-direct {p0}, Lio/realm/au;->h()V

    .line 198
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lio/realm/au;->f:Z

    return v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lio/realm/au;->g:Ljava/util/List;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lio/realm/au;->b:Z

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/au;->b:Z

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/au;->g:Ljava/util/List;

    .line 165
    return-void
.end method
