.class public Lio/realm/ay;
.super Ljava/lang/Object;
.source "RealmConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/ay$a;
    }
.end annotation


# static fields
.field protected static final a:Lio/realm/internal/m;

.field private static final b:Ljava/lang/Object;

.field private static c:Ljava/lang/Boolean;


# instance fields
.field private final d:Ljava/io/File;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:[B

.field private final i:J

.field private final j:Lio/realm/ba;

.field private final k:Z

.field private final l:Lio/realm/internal/SharedRealm$a;

.field private final m:Lio/realm/internal/m;

.field private final n:Lio/realm/a/b;

.field private final o:Lio/realm/av$a;

.field private final p:Z

.field private final q:Lio/realm/CompactOnLaunchCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 74
    invoke-static {}, Lio/realm/av;->p()Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lio/realm/ay;->b:Ljava/lang/Object;

    .line 75
    sget-object v0, Lio/realm/ay;->b:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 76
    sget-object v0, Lio/realm/ay;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/realm/ay;->a(Ljava/lang/String;)Lio/realm/internal/m;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lio/realm/internal/m;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    new-instance v0, Ljava/lang/ExceptionInInitializerError;

    const-string v1, "RealmTransformer doesn\'t seem to be applied. Please update the project configuration to use the Realm Gradle plugin. See https://realm.io/news/android-installation-change/"

    invoke-direct {v0, v1}, Ljava/lang/ExceptionInInitializerError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    sput-object v0, Lio/realm/ay;->a:Lio/realm/internal/m;

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lio/realm/ay;->a:Lio/realm/internal/m;

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BJLio/realm/ba;ZLio/realm/internal/SharedRealm$a;Lio/realm/internal/m;Lio/realm/a/b;Lio/realm/av$a;ZLio/realm/CompactOnLaunchCallback;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lio/realm/ay;->d:Ljava/io/File;

    .line 120
    iput-object p2, p0, Lio/realm/ay;->e:Ljava/lang/String;

    .line 121
    iput-object p3, p0, Lio/realm/ay;->f:Ljava/lang/String;

    .line 122
    iput-object p4, p0, Lio/realm/ay;->g:Ljava/lang/String;

    .line 123
    iput-object p5, p0, Lio/realm/ay;->h:[B

    .line 124
    iput-wide p6, p0, Lio/realm/ay;->i:J

    .line 125
    iput-object p8, p0, Lio/realm/ay;->j:Lio/realm/ba;

    .line 126
    iput-boolean p9, p0, Lio/realm/ay;->k:Z

    .line 127
    iput-object p10, p0, Lio/realm/ay;->l:Lio/realm/internal/SharedRealm$a;

    .line 128
    iput-object p11, p0, Lio/realm/ay;->m:Lio/realm/internal/m;

    .line 129
    iput-object p12, p0, Lio/realm/ay;->n:Lio/realm/a/b;

    .line 130
    iput-object p13, p0, Lio/realm/ay;->o:Lio/realm/av$a;

    .line 131
    iput-boolean p14, p0, Lio/realm/ay;->p:Z

    .line 132
    iput-object p15, p0, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    .line 133
    return-void
.end method

.method private static a(Ljava/lang/String;)Lio/realm/internal/m;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 340
    const-string v0, "\\."

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 341
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    .line 342
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "io.realm.%s%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    const-string v0, "Mediator"

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 346
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    .line 348
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 349
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/m;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    return-object v0

    .line 350
    :catch_0
    move-exception v0

    .line 351
    new-instance v2, Lio/realm/exceptions/RealmException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not find "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 352
    :catch_1
    move-exception v0

    .line 353
    new-instance v2, Lio/realm/exceptions/RealmException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not create an instance of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 354
    :catch_2
    move-exception v0

    .line 355
    new-instance v2, Lio/realm/exceptions/RealmException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not create an instance of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 356
    :catch_3
    move-exception v0

    .line 357
    new-instance v2, Lio/realm/exceptions/RealmException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not create an instance of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected static a(Ljava/util/Set;Ljava/util/Set;)Lio/realm/internal/m;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/bb;",
            ">;>;)",
            "Lio/realm/internal/m;"
        }
    .end annotation

    .prologue
    .line 319
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 320
    new-instance v0, Lio/realm/internal/b/b;

    sget-object v1, Lio/realm/ay;->a:Lio/realm/internal/m;

    invoke-direct {v0, v1, p1}, Lio/realm/internal/b/b;-><init>(Lio/realm/internal/m;Ljava/util/Collection;)V

    .line 335
    :goto_0
    return-object v0

    .line 324
    :cond_0
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 325
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/realm/ay;->a(Ljava/lang/String;)Lio/realm/internal/m;

    move-result-object v0

    goto :goto_0

    .line 329
    :cond_1
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v1, v0, [Lio/realm/internal/m;

    .line 330
    const/4 v0, 0x0

    .line 331
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 332
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lio/realm/ay;->a(Ljava/lang/String;)Lio/realm/internal/m;

    move-result-object v3

    aput-object v3, v1, v0

    .line 333
    add-int/lit8 v0, v0, 0x1

    .line 334
    goto :goto_1

    .line 335
    :cond_2
    new-instance v0, Lio/realm/internal/b/a;

    invoke-direct {v0, v1}, Lio/realm/internal/b/a;-><init>([Lio/realm/internal/m;)V

    goto :goto_0
.end method

.method protected static a(Ljava/io/File;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 411
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 412
    :catch_0
    move-exception v0

    .line 413
    new-instance v1, Lio/realm/exceptions/RealmFileException;

    sget-object v2, Lio/realm/exceptions/RealmFileException$Kind;->ACCESS_ERROR:Lio/realm/exceptions/RealmFileException$Kind;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not resolve the canonical path to the Realm file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 414
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static declared-synchronized p()Z
    .locals 2

    .prologue
    .line 397
    const-class v1, Lio/realm/ay;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lio/realm/ay;->c:Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 399
    :try_start_1
    const-string v0, "rx.d"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 400
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lio/realm/ay;->c:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405
    :cond_0
    :goto_0
    :try_start_2
    sget-object v0, Lio/realm/ay;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 401
    :catch_0
    move-exception v0

    .line 402
    const/4 v0, 0x0

    :try_start_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lio/realm/ay;->c:Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic r()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lio/realm/ay;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/io/File;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lio/realm/ay;->d:Ljava/io/File;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lio/realm/ay;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c()[B
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lio/realm/ay;->h:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/ay;->h:[B

    iget-object v1, p0, Lio/realm/ay;->h:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 148
    iget-wide v0, p0, Lio/realm/ay;->i:J

    return-wide v0
.end method

.method public e()Lio/realm/ba;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lio/realm/ay;->j:Lio/realm/ba;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 268
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    .line 291
    :cond_0
    :goto_0
    return v0

    .line 269
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 271
    check-cast p1, Lio/realm/ay;

    .line 273
    iget-wide v2, p0, Lio/realm/ay;->i:J

    iget-wide v4, p1, Lio/realm/ay;->i:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 274
    iget-boolean v1, p0, Lio/realm/ay;->k:Z

    iget-boolean v2, p1, Lio/realm/ay;->k:Z

    if-ne v1, v2, :cond_0

    .line 275
    iget-object v1, p0, Lio/realm/ay;->d:Ljava/io/File;

    iget-object v2, p1, Lio/realm/ay;->d:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, Lio/realm/ay;->e:Ljava/lang/String;

    iget-object v2, p1, Lio/realm/ay;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iget-object v1, p0, Lio/realm/ay;->f:Ljava/lang/String;

    iget-object v2, p1, Lio/realm/ay;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278
    iget-object v1, p0, Lio/realm/ay;->h:[B

    iget-object v2, p1, Lio/realm/ay;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 279
    iget-object v1, p0, Lio/realm/ay;->l:Lio/realm/internal/SharedRealm$a;

    iget-object v2, p1, Lio/realm/ay;->l:Lio/realm/internal/SharedRealm$a;

    invoke-virtual {v1, v2}, Lio/realm/internal/SharedRealm$a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lio/realm/ay;->j:Lio/realm/ba;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lio/realm/ay;->j:Lio/realm/ba;

    iget-object v2, p1, Lio/realm/ay;->j:Lio/realm/ba;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    :cond_2
    iget-object v1, p0, Lio/realm/ay;->n:Lio/realm/a/b;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lio/realm/ay;->n:Lio/realm/a/b;

    iget-object v2, p1, Lio/realm/ay;->n:Lio/realm/a/b;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    :cond_3
    iget-object v1, p0, Lio/realm/ay;->o:Lio/realm/av$a;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lio/realm/ay;->o:Lio/realm/av$a;

    iget-object v2, p1, Lio/realm/ay;->o:Lio/realm/av$a;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 288
    :cond_4
    iget-boolean v1, p0, Lio/realm/ay;->p:Z

    iget-boolean v2, p1, Lio/realm/ay;->p:Z

    if-ne v1, v2, :cond_0

    .line 289
    iget-object v1, p0, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    iget-object v2, p1, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291
    :cond_5
    iget-object v0, p0, Lio/realm/ay;->m:Lio/realm/internal/m;

    iget-object v1, p1, Lio/realm/ay;->m:Lio/realm/internal/m;

    invoke-virtual {v0, v1}, Lio/realm/internal/m;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 280
    :cond_6
    iget-object v1, p1, Lio/realm/ay;->j:Lio/realm/ba;

    if-eqz v1, :cond_2

    goto/16 :goto_0

    .line 282
    :cond_7
    iget-object v1, p1, Lio/realm/ay;->n:Lio/realm/a/b;

    if-eqz v1, :cond_3

    goto/16 :goto_0

    .line 285
    :cond_8
    iget-object v1, p1, Lio/realm/ay;->o:Lio/realm/av$a;

    if-eqz v1, :cond_4

    goto/16 :goto_0

    .line 289
    :cond_9
    iget-object v1, p1, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    if-eqz v1, :cond_5

    goto/16 :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lio/realm/ay;->k:Z

    return v0
.end method

.method public g()Lio/realm/internal/SharedRealm$a;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lio/realm/ay;->l:Lio/realm/internal/SharedRealm$a;

    return-object v0
.end method

.method h()Lio/realm/internal/m;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lio/realm/ay;->m:Lio/realm/internal/m;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 297
    iget-object v0, p0, Lio/realm/ay;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->hashCode()I

    move-result v0

    .line 298
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lio/realm/ay;->e:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 299
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lio/realm/ay;->f:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 300
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lio/realm/ay;->h:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/ay;->h:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    .line 301
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lio/realm/ay;->i:J

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 302
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lio/realm/ay;->j:Lio/realm/ba;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/realm/ay;->j:Lio/realm/ba;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 303
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lio/realm/ay;->k:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    .line 304
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lio/realm/ay;->m:Lio/realm/internal/m;

    invoke-virtual {v3}, Lio/realm/internal/m;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 305
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Lio/realm/ay;->l:Lio/realm/internal/SharedRealm$a;

    invoke-virtual {v3}, Lio/realm/internal/SharedRealm$a;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 306
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lio/realm/ay;->n:Lio/realm/a/b;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lio/realm/ay;->n:Lio/realm/a/b;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    .line 307
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lio/realm/ay;->o:Lio/realm/av$a;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lio/realm/ay;->o:Lio/realm/av$a;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    .line 308
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lio/realm/ay;->p:Z

    if-eqz v3, :cond_6

    :goto_5
    add-int/2addr v0, v2

    .line 309
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 311
    return v0

    :cond_1
    move v0, v1

    .line 300
    goto :goto_0

    :cond_2
    move v0, v1

    .line 302
    goto :goto_1

    :cond_3
    move v0, v1

    .line 303
    goto :goto_2

    :cond_4
    move v0, v1

    .line 306
    goto :goto_3

    :cond_5
    move v0, v1

    .line 307
    goto :goto_4

    :cond_6
    move v2, v1

    .line 308
    goto :goto_5
.end method

.method i()Lio/realm/av$a;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lio/realm/ay;->o:Lio/realm/av$a;

    return-object v0
.end method

.method j()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lio/realm/ay;->g:Ljava/lang/String;

    invoke-static {v0}, Lio/realm/internal/Util;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lio/realm/ay;->g:Ljava/lang/String;

    return-object v0
.end method

.method public l()Lio/realm/CompactOnLaunchCallback;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lio/realm/ay;->f:Ljava/lang/String;

    return-object v0
.end method

.method n()Z
    .locals 2

    .prologue
    .line 237
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lio/realm/ay;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lio/realm/ay;->p:Z

    return v0
.end method

.method q()Z
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 365
    const-string v0, "realmDirectory: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lio/realm/ay;->d:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    const-string v0, "realmFileName : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lio/realm/ay;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    const-string v0, "canonicalPath: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lio/realm/ay;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    const-string v0, "key: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "[length: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lio/realm/ay;->h:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    const-string v0, "schemaVersion: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lio/realm/ay;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    const-string v0, "migration: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lio/realm/ay;->j:Lio/realm/ba;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 376
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    const-string v0, "deleteRealmIfMigrationNeeded: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lio/realm/ay;->k:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 378
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    const-string v0, "durability: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lio/realm/ay;->l:Lio/realm/internal/SharedRealm$a;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 380
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    const-string v0, "schemaMediator: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lio/realm/ay;->m:Lio/realm/internal/m;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 382
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    const-string v0, "readOnly: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lio/realm/ay;->p:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 384
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    const-string v0, "compactOnLaunch: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lio/realm/ay;->q:Lio/realm/CompactOnLaunchCallback;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 387
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 371
    :cond_0
    const/16 v0, 0x40

    goto/16 :goto_0
.end method
