.class public Lio/realm/y;
.super Lco/uk/getmondo/d/o;
.source "GoldenTicketRealmProxy.java"

# interfaces
.implements Lio/realm/internal/l;
.implements Lio/realm/z;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/y$a;
    }
.end annotation


# static fields
.field private static final c:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lio/realm/y$a;

.field private b:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/d/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    invoke-static {}, Lio/realm/y;->e()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/y;->c:Lio/realm/internal/OsObjectSchemaInfo;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 68
    const-string v1, "ticketId"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/y;->d:Ljava/util/List;

    .line 70
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lco/uk/getmondo/d/o;-><init>()V

    .line 73
    iget-object v0, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 74
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/o;Ljava/util/Map;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/o;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 303
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v0

    .line 323
    :goto_0
    return-wide v0

    .line 306
    :cond_0
    const-class v0, Lco/uk/getmondo/d/o;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v2

    .line 307
    invoke-virtual {v2}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v4

    .line 308
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v1, Lco/uk/getmondo/d/o;

    invoke-virtual {v0, v1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v0

    check-cast v0, Lio/realm/y$a;

    .line 309
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v0, p1

    .line 310
    check-cast v0, Lio/realm/z;

    invoke-interface {v0}, Lio/realm/z;->b()Ljava/lang/String;

    move-result-object v3

    .line 312
    if-nez v3, :cond_1

    .line 313
    invoke-static {v4, v5, v6, v7}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v0

    .line 317
    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_2

    .line 318
    invoke-static {v2, v3}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v0

    .line 322
    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 315
    :cond_1
    invoke-static {v4, v5, v6, v7, v3}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v0

    goto :goto_1

    .line 320
    :cond_2
    invoke-static {v3}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public static a(Lco/uk/getmondo/d/o;IILjava/util/Map;)Lco/uk/getmondo/d/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/o;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/d/o;"
        }
    .end annotation

    .prologue
    .line 409
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 410
    :cond_0
    const/4 v0, 0x0

    .line 428
    :goto_0
    return-object v0

    .line 412
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 414
    if-nez v0, :cond_2

    .line 415
    new-instance v1, Lco/uk/getmondo/d/o;

    invoke-direct {v1}, Lco/uk/getmondo/d/o;-><init>()V

    .line 416
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 425
    check-cast v0, Lio/realm/z;

    .line 426
    check-cast p0, Lio/realm/z;

    .line 427
    invoke-interface {p0}, Lio/realm/z;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/z;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 428
    goto :goto_0

    .line 419
    :cond_2
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_3

    .line 420
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/d/o;

    goto :goto_0

    .line 422
    :cond_3
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/d/o;

    .line 423
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto :goto_1
.end method

.method static a(Lio/realm/av;Lco/uk/getmondo/d/o;Lco/uk/getmondo/d/o;Ljava/util/Map;)Lco/uk/getmondo/d/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/o;",
            "Lco/uk/getmondo/d/o;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/o;"
        }
    .end annotation

    .prologue
    .line 432
    move-object v0, p1

    check-cast v0, Lio/realm/z;

    .line 433
    check-cast p2, Lio/realm/z;

    .line 434
    return-object p1
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/o;ZLjava/util/Map;)Lco/uk/getmondo/d/o;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/o;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/o;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 242
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-wide v2, v2, Lio/realm/g;->c:J

    iget-wide v6, p0, Lio/realm/av;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 243
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 245
    :cond_0
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 282
    :goto_0
    return-object p1

    .line 248
    :cond_1
    sget-object v2, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v2}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/g$b;

    .line 249
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/l;

    .line 250
    if-eqz v3, :cond_2

    .line 251
    check-cast v3, Lco/uk/getmondo/d/o;

    move-object p1, v3

    goto :goto_0

    .line 254
    :cond_2
    const/4 v5, 0x0

    .line 256
    if-eqz p2, :cond_6

    .line 257
    const-class v3, Lco/uk/getmondo/d/o;

    invoke-virtual {p0, v3}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 258
    invoke-virtual {v8}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v3, p1

    .line 259
    check-cast v3, Lio/realm/z;

    invoke-interface {v3}, Lio/realm/z;->b()Ljava/lang/String;

    move-result-object v3

    .line 261
    if-nez v3, :cond_3

    .line 262
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v6

    .line 266
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-eqz v3, :cond_4

    .line 268
    :try_start_0
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    iget-object v3, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/o;

    invoke-virtual {v3, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 269
    new-instance v4, Lio/realm/y;

    invoke-direct {v4}, Lio/realm/y;-><init>()V

    .line 270
    move-object v0, v4

    check-cast v0, Lio/realm/internal/l;

    move-object v3, v0

    invoke-interface {p3, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    move v2, p2

    .line 279
    :goto_2
    if-eqz v2, :cond_5

    .line 280
    invoke-static {p0, v4, p1, p3}, Lio/realm/y;->a(Lio/realm/av;Lco/uk/getmondo/d/o;Lco/uk/getmondo/d/o;Ljava/util/Map;)Lco/uk/getmondo/d/o;

    move-result-object p1

    goto :goto_0

    .line 264
    :cond_3
    invoke-virtual {v8, v6, v7, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 272
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    throw v3

    :cond_4
    move v2, v4

    move-object v4, v5

    .line 275
    goto :goto_2

    .line 282
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/y;->b(Lio/realm/av;Lco/uk/getmondo/d/o;ZLjava/util/Map;)Lco/uk/getmondo/d/o;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v2, p2

    move-object v4, v5

    goto :goto_2
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/y$a;
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    .line 119
    const-string v0, "class_GoldenTicket"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'GoldenTicket\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    const-string v0, "class_GoldenTicket"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 124
    cmp-long v0, v4, v8

    if-eqz v0, :cond_2

    .line 125
    cmp-long v0, v4, v8

    if-gez v0, :cond_1

    .line 126
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 1 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_1
    if-eqz p1, :cond_3

    .line 129
    const-string v0, "Field count is more than expected - expected 1 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 135
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 136
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    add-long/2addr v0, v8

    goto :goto_0

    .line 131
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 1 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_4
    new-instance v0, Lio/realm/y$a;

    invoke-direct {v0, p0, v2}, Lio/realm/y$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 141
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 142
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Primary key not defined for field \'ticketId\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_5
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lio/realm/y$a;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 145
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field ticketId"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_6
    const-string v1, "ticketId"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 150
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'ticketId\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_7
    const-string v1, "ticketId"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_8

    .line 153
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'ticketId\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_8
    iget-wide v4, v0, Lio/realm/y$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_9

    .line 156
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@PrimaryKey field \'ticketId\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_9
    const-string v1, "ticketId"

    invoke-virtual {v2, v1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->j(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 159
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Index not defined for field \'ticketId\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_a
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 380
    const-class v0, Lco/uk/getmondo/d/o;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v4

    .line 381
    invoke-virtual {v4}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v6

    .line 382
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v1, Lco/uk/getmondo/d/o;

    invoke-virtual {v0, v1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v0

    check-cast v0, Lio/realm/y$a;

    .line 383
    invoke-virtual {v4}, Lio/realm/internal/Table;->d()J

    move-result-wide v8

    .line 385
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 386
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/o;

    .line 387
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    instance-of v1, v0, Lio/realm/internal/l;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lio/realm/internal/l;

    invoke-interface {v1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lio/realm/internal/l;

    invoke-interface {v1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 391
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {v1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 394
    check-cast v1, Lio/realm/z;

    invoke-interface {v1}, Lio/realm/z;->b()Ljava/lang/String;

    move-result-object v1

    .line 396
    if-nez v1, :cond_3

    .line 397
    invoke-static {v6, v7, v8, v9}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v2

    .line 401
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v5, v2, v10

    if-nez v5, :cond_2

    .line 402
    invoke-static {v4, v1}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v2

    .line 404
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 399
    :cond_3
    invoke-static {v6, v7, v8, v9, v1}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v2

    goto :goto_1

    .line 406
    :cond_4
    return-void
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/o;Ljava/util/Map;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/o;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 358
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v0

    .line 376
    :goto_0
    return-wide v0

    .line 361
    :cond_0
    const-class v0, Lco/uk/getmondo/d/o;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v2

    .line 362
    invoke-virtual {v2}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v4

    .line 363
    iget-object v0, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v1, Lco/uk/getmondo/d/o;

    invoke-virtual {v0, v1}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v0

    check-cast v0, Lio/realm/y$a;

    .line 364
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v0, p1

    .line 365
    check-cast v0, Lio/realm/z;

    invoke-interface {v0}, Lio/realm/z;->b()Ljava/lang/String;

    move-result-object v3

    .line 367
    if-nez v3, :cond_2

    .line 368
    invoke-static {v4, v5, v6, v7}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v0

    .line 372
    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_1

    .line 373
    invoke-static {v2, v3}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v0

    .line 375
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 370
    :cond_2
    invoke-static {v4, v5, v6, v7, v3}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v0

    goto :goto_1
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/o;ZLjava/util/Map;)Lco/uk/getmondo/d/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/o;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/o;"
        }
    .end annotation

    .prologue
    .line 287
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 288
    if-eqz v0, :cond_0

    .line 289
    check-cast v0, Lco/uk/getmondo/d/o;

    .line 299
    :goto_0
    return-object v0

    .line 293
    :cond_0
    const-class v1, Lco/uk/getmondo/d/o;

    move-object v0, p1

    check-cast v0, Lio/realm/z;

    invoke-interface {v0}, Lio/realm/z;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lio/realm/av;->a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/o;

    move-object v1, v0

    .line 294
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    check-cast p1, Lio/realm/z;

    move-object v1, v0

    .line 297
    check-cast v1, Lio/realm/z;

    goto :goto_0
.end method

.method public static c()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lio/realm/y;->c:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    const-string v0, "class_GoldenTicket"

    return-object v0
.end method

.method private static e()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 109
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "GoldenTicket"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 110
    const-string v1, "ticketId"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const/4 v5, 0x0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 111
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 105
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'ticketId\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 94
    iget-object v0, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/y;->a:Lio/realm/y$a;

    iget-wide v2, v1, Lio/realm/y$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 453
    iget-object v0, p0, Lio/realm/y;->b:Lio/realm/au;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 440
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 441
    const-string v0, "Invalid object"

    .line 448
    :goto_0
    return-object v0

    .line 443
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "GoldenTicket = proxy["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 444
    const-string v0, "{ticketId:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    invoke-virtual {p0}, Lio/realm/y;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lio/realm/y;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 446
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 445
    :cond_1
    const-string v0, "null"

    goto :goto_1
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lio/realm/y;->b:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 81
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 82
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/y$a;

    iput-object v1, p0, Lio/realm/y;->a:Lio/realm/y$a;

    .line 83
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/y;->b:Lio/realm/au;

    .line 84
    iget-object v1, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 85
    iget-object v1, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 86
    iget-object v1, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 87
    iget-object v1, p0, Lio/realm/y;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method
