.class public final Lio/realm/g$b;
.super Ljava/lang/Object;
.source "BaseRealm.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private a:Lio/realm/g;

.field private b:Lio/realm/internal/n;

.field private c:Lio/realm/internal/c;

.field private d:Z

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()Lio/realm/g;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lio/realm/g$b;->a:Lio/realm/g;

    return-object v0
.end method

.method public a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/g;",
            "Lio/realm/internal/n;",
            "Lio/realm/internal/c;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 711
    iput-object p1, p0, Lio/realm/g$b;->a:Lio/realm/g;

    .line 712
    iput-object p2, p0, Lio/realm/g$b;->b:Lio/realm/internal/n;

    .line 713
    iput-object p3, p0, Lio/realm/g$b;->c:Lio/realm/internal/c;

    .line 714
    iput-boolean p4, p0, Lio/realm/g$b;->d:Z

    .line 715
    iput-object p5, p0, Lio/realm/g$b;->e:Ljava/util/List;

    .line 716
    return-void
.end method

.method public b()Lio/realm/internal/n;
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lio/realm/g$b;->b:Lio/realm/internal/n;

    return-object v0
.end method

.method public c()Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Lio/realm/g$b;->c:Lio/realm/internal/c;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 731
    iget-boolean v0, p0, Lio/realm/g$b;->d:Z

    return v0
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 735
    iget-object v0, p0, Lio/realm/g$b;->e:Ljava/util/List;

    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 739
    iput-object v1, p0, Lio/realm/g$b;->a:Lio/realm/g;

    .line 740
    iput-object v1, p0, Lio/realm/g$b;->b:Lio/realm/internal/n;

    .line 741
    iput-object v1, p0, Lio/realm/g$b;->c:Lio/realm/internal/c;

    .line 742
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/g$b;->d:Z

    .line 743
    iput-object v1, p0, Lio/realm/g$b;->e:Ljava/util/List;

    .line 744
    return-void
.end method
