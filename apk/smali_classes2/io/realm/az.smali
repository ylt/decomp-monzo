.class public Lio/realm/az;
.super Ljava/util/AbstractList;
.source "RealmList.java"

# interfaces
.implements Lio/realm/OrderedRealmCollection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/az$b;,
        Lio/realm/az$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/bb;",
        ">",
        "Ljava/util/AbstractList",
        "<TE;>;",
        "Lio/realm/OrderedRealmCollection",
        "<TE;>;"
    }
.end annotation


# instance fields
.field protected a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field protected b:Ljava/lang/String;

.field final c:Lio/realm/internal/LinkView;

.field protected d:Lio/realm/g;

.field private final e:Lio/realm/internal/Collection;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 76
    iput-object v0, p0, Lio/realm/az;->e:Lio/realm/internal/Collection;

    .line 77
    iput-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    .line 79
    return-void
.end method

.method constructor <init>(Ljava/lang/Class;Lio/realm/internal/LinkView;Lio/realm/g;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lio/realm/internal/LinkView;",
            "Lio/realm/g;",
            ")V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 108
    new-instance v0, Lio/realm/internal/Collection;

    iget-object v1, p3, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/LinkView;Lio/realm/internal/SortDescriptor;)V

    iput-object v0, p0, Lio/realm/az;->e:Lio/realm/internal/Collection;

    .line 109
    iput-object p1, p0, Lio/realm/az;->a:Ljava/lang/Class;

    .line 110
    iput-object p2, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    .line 111
    iput-object p3, p0, Lio/realm/az;->d:Lio/realm/g;

    .line 112
    return-void
.end method

.method static synthetic a(Lio/realm/az;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lio/realm/az;->modCount:I

    return v0
.end method

.method private a(ZLio/realm/bb;)Lio/realm/bb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTE;)TE;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 478
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    invoke-direct {p0}, Lio/realm/az;->e()V

    .line 480
    iget-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 481
    invoke-virtual {p0, v1}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v0

    .line 490
    :goto_0
    return-object v0

    .line 483
    :cond_0
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 484
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0

    .line 487
    :cond_1
    if-eqz p1, :cond_2

    .line 488
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "The list is empty."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v0, p2

    .line 490
    goto :goto_0
.end method

.method static synthetic b(Lio/realm/az;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lio/realm/az;->modCount:I

    return v0
.end method

.method private b(Lio/realm/bb;)Lio/realm/bb;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 246
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 247
    check-cast v0, Lio/realm/internal/l;

    .line 249
    instance-of v1, v0, Lio/realm/p;

    if-eqz v1, :cond_4

    .line 250
    iget-object v1, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    invoke-virtual {v1}, Lio/realm/internal/LinkView;->e()Lio/realm/internal/Table;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v1

    .line 251
    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-object v3, p0, Lio/realm/az;->d:Lio/realm/g;

    if-ne v2, v3, :cond_2

    move-object v0, p1

    .line 252
    check-cast v0, Lio/realm/p;

    invoke-virtual {v0}, Lio/realm/p;->b()Ljava/lang/String;

    move-result-object v0

    .line 253
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-object p1

    .line 258
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "The object has a different type from list\'s. Type of the list is \'%s\', type of object is \'%s\'."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v0, v5, v1

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 262
    :cond_2
    iget-object v1, p0, Lio/realm/az;->d:Lio/realm/g;

    iget-wide v2, v1, Lio/realm/g;->c:J

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-wide v0, v0, Lio/realm/g;->c:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_3

    .line 266
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot copy DynamicRealmObject between Realm instances."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot copy an object to a Realm instance created in another thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :cond_4
    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 273
    iget-object v1, p0, Lio/realm/az;->d:Lio/realm/g;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eq v1, v0, :cond_0

    .line 274
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot copy an object from another Realm instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_5
    iget-object v0, p0, Lio/realm/az;->d:Lio/realm/g;

    check-cast v0, Lio/realm/av;

    .line 283
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 284
    invoke-virtual {v0, p1}, Lio/realm/av;->b(Lio/realm/bb;)Lio/realm/bb;

    move-result-object p1

    goto/16 :goto_0

    .line 286
    :cond_6
    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object p1

    goto/16 :goto_0
.end method

.method static synthetic c(Lio/realm/az;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lio/realm/az;->modCount:I

    return v0
.end method

.method private c(Lio/realm/bb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 789
    if-nez p1, :cond_0

    .line 790
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RealmList does not accept null values"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 792
    :cond_0
    return-void
.end method

.method static synthetic d(Lio/realm/az;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lio/realm/az;->modCount:I

    return v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lio/realm/az;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lio/realm/az;->modCount:I

    return v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 802
    iget-object v0, p0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 803
    iget-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 804
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Realm instance has been closed or this object or its parent has been deleted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 806
    :cond_1
    return-void
.end method


# virtual methods
.method public a(I)Lio/realm/bb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 346
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    invoke-direct {p0}, Lio/realm/az;->e()V

    .line 348
    invoke-virtual {p0, p1}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/LinkView;->c(J)V

    .line 353
    :goto_0
    iget v1, p0, Lio/realm/az;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lio/realm/az;->modCount:I

    .line 354
    return-object v0

    .line 351
    :cond_0
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0
.end method

.method public a(ILio/realm/bb;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 168
    invoke-direct {p0, p2}, Lio/realm/az;->c(Lio/realm/bb;)V

    .line 169
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    invoke-direct {p0}, Lio/realm/az;->e()V

    .line 171
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lio/realm/az;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 172
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/az;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_1
    invoke-direct {p0, p2}, Lio/realm/az;->b(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 175
    iget-object v1, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lio/realm/internal/LinkView;->a(JJ)V

    .line 179
    :goto_0
    iget v0, p0, Lio/realm/az;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/az;->modCount:I

    .line 180
    return-void

    .line 177
    :cond_2
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lio/realm/az;->d:Lio/realm/g;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lio/realm/bb;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lio/realm/az;->c(Lio/realm/bb;)V

    .line 200
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    invoke-direct {p0}, Lio/realm/az;->e()V

    .line 202
    invoke-direct {p0, p1}, Lio/realm/az;->b(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 203
    iget-object v1, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/LinkView;->b(J)V

    .line 207
    :goto_0
    iget v0, p0, Lio/realm/az;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/az;->modCount:I

    .line 208
    const/4 v0, 0x1

    return v0

    .line 205
    :cond_0
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public synthetic add(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 55
    check-cast p2, Lio/realm/bb;

    invoke-virtual {p0, p1, p2}, Lio/realm/az;->a(ILio/realm/bb;)V

    return-void
.end method

.method public synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 55
    check-cast p1, Lio/realm/bb;

    invoke-virtual {p0, p1}, Lio/realm/az;->a(Lio/realm/bb;)Z

    move-result v0

    return v0
.end method

.method public b()Lio/realm/bb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 466
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lio/realm/az;->a(ZLio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lio/realm/bb;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 452
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    invoke-direct {p0}, Lio/realm/az;->e()V

    .line 454
    iget-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/LinkView;->a(J)J

    move-result-wide v0

    .line 455
    iget-object v2, p0, Lio/realm/az;->d:Lio/realm/g;

    iget-object v3, p0, Lio/realm/az;->a:Ljava/lang/Class;

    iget-object v4, p0, Lio/realm/az;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0, v1}, Lio/realm/g;->a(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/bb;

    move-result-object v0

    .line 457
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0
.end method

.method public b(ILio/realm/bb;)Lio/realm/bb;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 230
    invoke-direct {p0, p2}, Lio/realm/az;->c(Lio/realm/bb;)V

    .line 232
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-direct {p0}, Lio/realm/az;->e()V

    .line 234
    invoke-direct {p0, p2}, Lio/realm/az;->b(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 235
    invoke-virtual {p0, p1}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v1

    .line 236
    iget-object v2, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    int-to-long v4, p1

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    invoke-virtual {v2, v4, v5, v6, v7}, Lio/realm/internal/LinkView;->b(JJ)V

    move-object v0, v1

    .line 241
    :goto_0
    return-object v0

    .line 239
    :cond_0
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 711
    const/4 v0, 0x1

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    invoke-direct {p0}, Lio/realm/az;->e()V

    .line 328
    iget-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->a()V

    .line 332
    :goto_0
    iget v0, p0, Lio/realm/az;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/az;->modCount:I

    .line 333
    return-void

    .line 330
    :cond_0
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 734
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 735
    iget-object v0, p0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 738
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 739
    check-cast v0, Lio/realm/internal/l;

    .line 740
    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    sget-object v2, Lio/realm/internal/e;->a:Lio/realm/internal/e;

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 752
    :goto_0
    return v0

    .line 745
    :cond_0
    invoke-virtual {p0}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    .line 746
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 747
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 750
    goto :goto_0

    .line 752
    :cond_3
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 761
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 762
    new-instance v0, Lio/realm/az$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lio/realm/az$a;-><init>(Lio/realm/az;Lio/realm/az$1;)V

    .line 764
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 773
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/realm/az;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 781
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 782
    new-instance v0, Lio/realm/az$b;

    invoke-direct {v0, p0, p1}, Lio/realm/az$b;-><init>(Lio/realm/az;I)V

    .line 784
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic remove(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lio/realm/az;->a(I)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 376
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 377
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects can only be removed from inside a write transaction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 379
    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 401
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects can only be removed from inside a write transaction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :cond_0
    invoke-super {p0, p1}, Ljava/util/AbstractList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    check-cast p2, Lio/realm/bb;

    invoke-virtual {p0, p1, p2}, Lio/realm/az;->b(ILio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 4

    .prologue
    .line 589
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 590
    invoke-direct {p0}, Lio/realm/az;->e()V

    .line 591
    iget-object v0, p0, Lio/realm/az;->c:Lio/realm/internal/LinkView;

    invoke-virtual {v0}, Lio/realm/internal/LinkView;->b()J

    move-result-wide v0

    .line 592
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    long-to-int v0, v0

    .line 594
    :goto_0
    return v0

    .line 592
    :cond_0
    const v0, 0x7fffffff

    goto :goto_0

    .line 594
    :cond_1
    iget-object v0, p0, Lio/realm/az;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 832
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 833
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/realm/az;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 834
    const-string v0, "@["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 835
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lio/realm/az;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 836
    const-string v0, "invalid"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 849
    :cond_0
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 850
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 833
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 838
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lio/realm/az;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 839
    invoke-virtual {p0}, Lio/realm/az;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 840
    invoke-virtual {p0, v1}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 844
    :goto_2
    invoke-virtual {p0}, Lio/realm/az;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_3

    .line 845
    const/16 v0, 0x2c

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 838
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 842
    :cond_4
    invoke-virtual {p0, v1}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2
.end method
