.class Lio/realm/az$a;
.super Ljava/lang/Object;
.source "RealmList.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/az;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field a:I

.field b:I

.field c:I

.field final synthetic d:Lio/realm/az;


# direct methods
.method private constructor <init>(Lio/realm/az;)V
    .locals 1

    .prologue
    .line 1016
    iput-object p1, p0, Lio/realm/az$a;->d:Lio/realm/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020
    const/4 v0, 0x0

    iput v0, p0, Lio/realm/az$a;->a:I

    .line 1027
    const/4 v0, -0x1

    iput v0, p0, Lio/realm/az$a;->b:I

    .line 1034
    iget-object v0, p0, Lio/realm/az$a;->d:Lio/realm/az;

    invoke-static {v0}, Lio/realm/az;->a(Lio/realm/az;)I

    move-result v0

    iput v0, p0, Lio/realm/az$a;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lio/realm/az;Lio/realm/az$1;)V
    .locals 0

    .prologue
    .line 1016
    invoke-direct {p0, p1}, Lio/realm/az$a;-><init>(Lio/realm/az;)V

    return-void
.end method


# virtual methods
.method public a()Lio/realm/bb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1051
    iget-object v0, p0, Lio/realm/az$a;->d:Lio/realm/az;

    iget-object v0, v0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1052
    invoke-virtual {p0}, Lio/realm/az$a;->b()V

    .line 1053
    iget v0, p0, Lio/realm/az$a;->a:I

    .line 1055
    :try_start_0
    iget-object v1, p0, Lio/realm/az$a;->d:Lio/realm/az;

    invoke-virtual {v1, v0}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v1

    .line 1056
    iput v0, p0, Lio/realm/az$a;->b:I

    .line 1057
    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lio/realm/az$a;->a:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1058
    return-object v1

    .line 1059
    :catch_0
    move-exception v1

    .line 1060
    invoke-virtual {p0}, Lio/realm/az$a;->b()V

    .line 1061
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot access index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " when size is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lio/realm/az$a;->d:Lio/realm/az;

    invoke-virtual {v2}, Lio/realm/az;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Remember to check hasNext() before using next()."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method final b()V
    .locals 2

    .prologue
    .line 1095
    iget-object v0, p0, Lio/realm/az$a;->d:Lio/realm/az;

    invoke-static {v0}, Lio/realm/az;->c(Lio/realm/az;)I

    move-result v0

    iget v1, p0, Lio/realm/az$a;->c:I

    if-eq v0, v1, :cond_0

    .line 1096
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1098
    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 1041
    iget-object v0, p0, Lio/realm/az$a;->d:Lio/realm/az;

    iget-object v0, v0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1042
    invoke-virtual {p0}, Lio/realm/az$a;->b()V

    .line 1043
    iget v0, p0, Lio/realm/az$a;->a:I

    iget-object v1, p0, Lio/realm/az$a;->d:Lio/realm/az;

    invoke-virtual {v1}, Lio/realm/az;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1016
    invoke-virtual {p0}, Lio/realm/az$a;->a()Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 1070
    iget-object v0, p0, Lio/realm/az$a;->d:Lio/realm/az;

    iget-object v0, v0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1071
    iget v0, p0, Lio/realm/az$a;->b:I

    if-gez v0, :cond_0

    .line 1072
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call remove() twice. Must call next() in between."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074
    :cond_0
    invoke-virtual {p0}, Lio/realm/az$a;->b()V

    .line 1077
    :try_start_0
    iget-object v0, p0, Lio/realm/az$a;->d:Lio/realm/az;

    iget v1, p0, Lio/realm/az$a;->b:I

    invoke-virtual {v0, v1}, Lio/realm/az;->a(I)Lio/realm/bb;

    .line 1078
    iget v0, p0, Lio/realm/az$a;->b:I

    iget v1, p0, Lio/realm/az$a;->a:I

    if-ge v0, v1, :cond_1

    .line 1079
    iget v0, p0, Lio/realm/az$a;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lio/realm/az$a;->a:I

    .line 1081
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lio/realm/az$a;->b:I

    .line 1082
    iget-object v0, p0, Lio/realm/az$a;->d:Lio/realm/az;

    invoke-static {v0}, Lio/realm/az;->b(Lio/realm/az;)I

    move-result v0

    iput v0, p0, Lio/realm/az$a;->c:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1086
    return-void

    .line 1083
    :catch_0
    move-exception v0

    .line 1084
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0
.end method
