.class Lio/realm/az$b;
.super Lio/realm/az$a;
.source "RealmList.java"

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/az;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/realm/az",
        "<TE;>.a;",
        "Ljava/util/ListIterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field final synthetic e:Lio/realm/az;


# direct methods
.method constructor <init>(Lio/realm/az;I)V
    .locals 3

    .prologue
    .line 1103
    iput-object p1, p0, Lio/realm/az$b;->e:Lio/realm/az;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lio/realm/az$a;-><init>(Lio/realm/az;Lio/realm/az$1;)V

    .line 1104
    if-ltz p2, :cond_0

    invoke-virtual {p1}, Lio/realm/az;->size()I

    move-result v0

    if-gt p2, v0, :cond_0

    .line 1105
    iput p2, p0, Lio/realm/az$b;->a:I

    .line 1109
    return-void

    .line 1107
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting location must be a valid index: [0, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lio/realm/az;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]. Index was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lio/realm/bb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1157
    iget-object v0, p0, Lio/realm/az$b;->e:Lio/realm/az;

    iget-object v0, v0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1158
    iget v0, p0, Lio/realm/az$b;->b:I

    if-gez v0, :cond_0

    .line 1159
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1161
    :cond_0
    invoke-virtual {p0}, Lio/realm/az$b;->b()V

    .line 1164
    :try_start_0
    iget-object v0, p0, Lio/realm/az$b;->e:Lio/realm/az;

    iget v1, p0, Lio/realm/az$b;->b:I

    invoke-virtual {v0, v1, p1}, Lio/realm/az;->b(ILio/realm/bb;)Lio/realm/bb;

    .line 1165
    iget-object v0, p0, Lio/realm/az$b;->e:Lio/realm/az;

    invoke-static {v0}, Lio/realm/az;->d(Lio/realm/az;)I

    move-result v0

    iput v0, p0, Lio/realm/az$b;->c:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1169
    return-void

    .line 1166
    :catch_0
    move-exception v0

    .line 1167
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0
.end method

.method public synthetic add(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1101
    check-cast p1, Lio/realm/bb;

    invoke-virtual {p0, p1}, Lio/realm/az$b;->b(Lio/realm/bb;)V

    return-void
.end method

.method public b(Lio/realm/bb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1179
    iget-object v0, p0, Lio/realm/az$b;->e:Lio/realm/az;

    iget-object v0, v0, Lio/realm/az;->d:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1180
    invoke-virtual {p0}, Lio/realm/az$b;->b()V

    .line 1182
    :try_start_0
    iget v0, p0, Lio/realm/az$b;->a:I

    .line 1183
    iget-object v1, p0, Lio/realm/az$b;->e:Lio/realm/az;

    invoke-virtual {v1, v0, p1}, Lio/realm/az;->a(ILio/realm/bb;)V

    .line 1184
    const/4 v1, -0x1

    iput v1, p0, Lio/realm/az$b;->b:I

    .line 1185
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/realm/az$b;->a:I

    .line 1186
    iget-object v0, p0, Lio/realm/az$b;->e:Lio/realm/az;

    invoke-static {v0}, Lio/realm/az;->e(Lio/realm/az;)I

    move-result v0

    iput v0, p0, Lio/realm/az$b;->c:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1190
    return-void

    .line 1187
    :catch_0
    move-exception v0

    .line 1188
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0
.end method

.method public c()Lio/realm/bb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1124
    invoke-virtual {p0}, Lio/realm/az$b;->b()V

    .line 1125
    iget v0, p0, Lio/realm/az$b;->a:I

    add-int/lit8 v0, v0, -0x1

    .line 1127
    :try_start_0
    iget-object v1, p0, Lio/realm/az$b;->e:Lio/realm/az;

    invoke-virtual {v1, v0}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v1

    .line 1128
    iput v0, p0, Lio/realm/az$b;->a:I

    iput v0, p0, Lio/realm/az$b;->b:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1129
    return-object v1

    .line 1130
    :catch_0
    move-exception v1

    .line 1131
    invoke-virtual {p0}, Lio/realm/az$b;->b()V

    .line 1132
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot access index less than zero. This was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Remember to check hasPrevious() before using previous()."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public hasPrevious()Z
    .locals 1

    .prologue
    .line 1116
    iget v0, p0, Lio/realm/az$b;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nextIndex()I
    .locals 1

    .prologue
    .line 1141
    iget v0, p0, Lio/realm/az$b;->a:I

    return v0
.end method

.method public synthetic previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1101
    invoke-virtual {p0}, Lio/realm/az$b;->c()Lio/realm/bb;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    .prologue
    .line 1149
    iget v0, p0, Lio/realm/az$b;->a:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public synthetic set(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1101
    check-cast p1, Lio/realm/bb;

    invoke-virtual {p0, p1}, Lio/realm/az$b;->a(Lio/realm/bb;)V

    return-void
.end method
