.class public abstract Lio/realm/bc;
.super Ljava/lang/Object;
.source "RealmObject.java"

# interfaces
.implements Lio/realm/bb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lio/realm/bb;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;)V"
        }
    .end annotation

    .prologue
    .line 95
    instance-of v0, p0, Lio/realm/internal/l;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Object not managed by Realm, so it cannot be removed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_0
    check-cast p0, Lio/realm/internal/l;

    .line 101
    invoke-interface {p0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    if-nez v0, :cond_1

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Object malformed: missing object in Realm. Make sure to instantiate RealmObjects with Realm.createObject()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_1
    invoke-interface {p0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-nez v0, :cond_2

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Object malformed: missing Realm. Make sure to instantiate RealmObjects with Realm.createObject()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_2
    invoke-interface {p0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 109
    invoke-interface {p0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 110
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lio/realm/internal/Table;->d(J)V

    .line 111
    invoke-interface {p0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    sget-object v1, Lio/realm/internal/e;->a:Lio/realm/internal/e;

    invoke-virtual {v0, v1}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 112
    return-void
.end method

.method public static b(Lio/realm/bb;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 145
    instance-of v1, p0, Lio/realm/internal/l;

    if-eqz v1, :cond_0

    .line 146
    check-cast p0, Lio/realm/internal/l;

    .line 147
    invoke-interface {p0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    .line 148
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lio/realm/internal/n;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v0

    .line 148
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lio/realm/bb;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(TE;)Z"
        }
    .end annotation

    .prologue
    .line 286
    instance-of v0, p0, Lio/realm/internal/l;

    return v0
.end method
