.class final Lio/realm/q$a;
.super Lio/realm/internal/c;
.source "FeatureFlagsRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 42
    const-string v0, "currentAccountP2pEnabled"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/q$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/q$a;->a:J

    .line 43
    const-string v0, "potsEnabled"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/q$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/q$a;->b:J

    .line 44
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 48
    invoke-virtual {p0, p1, p0}, Lio/realm/q$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 49
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lio/realm/q$a;

    invoke-direct {v0, p0, p1}, Lio/realm/q$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 58
    check-cast p1, Lio/realm/q$a;

    .line 59
    check-cast p2, Lio/realm/q$a;

    .line 60
    iget-wide v0, p1, Lio/realm/q$a;->a:J

    iput-wide v0, p2, Lio/realm/q$a;->a:J

    .line 61
    iget-wide v0, p1, Lio/realm/q$a;->b:J

    iput-wide v0, p2, Lio/realm/q$a;->b:J

    .line 62
    return-void
.end method
