.class final Lio/realm/s$a;
.super Lio/realm/internal/c;
.source "FeedItemRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field e:J

.field f:J

.field g:J

.field h:J

.field i:J

.field j:J

.field k:J

.field l:J


# direct methods
.method constructor <init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V
    .locals 2

    .prologue
    .line 51
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lio/realm/internal/c;-><init>(I)V

    .line 52
    const-string v0, "id"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->a:J

    .line 53
    const-string v0, "accountId"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->b:J

    .line 54
    const-string v0, "created"

    sget-object v1, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->c:J

    .line 55
    const-string v0, "type"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->d:J

    .line 56
    const-string v0, "appUri"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->e:J

    .line 57
    const-string v0, "goldenTicket"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->f:J

    .line 58
    const-string v0, "monthlySpendingReport"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->g:J

    .line 59
    const-string v0, "transaction"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->h:J

    .line 60
    const-string v0, "kycRejected"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->i:J

    .line 61
    const-string v0, "sddRejectedReason"

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->j:J

    .line 62
    const-string v0, "basicItemInfo"

    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->k:J

    .line 63
    const-string v0, "isDismissable"

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    invoke-virtual {p0, p2, v0, v1}, Lio/realm/s$a;->a(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/RealmFieldType;)J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/s$a;->l:J

    .line 64
    return-void
.end method

.method constructor <init>(Lio/realm/internal/c;Z)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lio/realm/internal/c;-><init>(Lio/realm/internal/c;Z)V

    .line 68
    invoke-virtual {p0, p1, p0}, Lio/realm/s$a;->a(Lio/realm/internal/c;Lio/realm/internal/c;)V

    .line 69
    return-void
.end method


# virtual methods
.method protected final a(Z)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lio/realm/s$a;

    invoke-direct {v0, p0, p1}, Lio/realm/s$a;-><init>(Lio/realm/internal/c;Z)V

    return-object v0
.end method

.method protected final a(Lio/realm/internal/c;Lio/realm/internal/c;)V
    .locals 2

    .prologue
    .line 78
    check-cast p1, Lio/realm/s$a;

    .line 79
    check-cast p2, Lio/realm/s$a;

    .line 80
    iget-wide v0, p1, Lio/realm/s$a;->a:J

    iput-wide v0, p2, Lio/realm/s$a;->a:J

    .line 81
    iget-wide v0, p1, Lio/realm/s$a;->b:J

    iput-wide v0, p2, Lio/realm/s$a;->b:J

    .line 82
    iget-wide v0, p1, Lio/realm/s$a;->c:J

    iput-wide v0, p2, Lio/realm/s$a;->c:J

    .line 83
    iget-wide v0, p1, Lio/realm/s$a;->d:J

    iput-wide v0, p2, Lio/realm/s$a;->d:J

    .line 84
    iget-wide v0, p1, Lio/realm/s$a;->e:J

    iput-wide v0, p2, Lio/realm/s$a;->e:J

    .line 85
    iget-wide v0, p1, Lio/realm/s$a;->f:J

    iput-wide v0, p2, Lio/realm/s$a;->f:J

    .line 86
    iget-wide v0, p1, Lio/realm/s$a;->g:J

    iput-wide v0, p2, Lio/realm/s$a;->g:J

    .line 87
    iget-wide v0, p1, Lio/realm/s$a;->h:J

    iput-wide v0, p2, Lio/realm/s$a;->h:J

    .line 88
    iget-wide v0, p1, Lio/realm/s$a;->i:J

    iput-wide v0, p2, Lio/realm/s$a;->i:J

    .line 89
    iget-wide v0, p1, Lio/realm/s$a;->j:J

    iput-wide v0, p2, Lio/realm/s$a;->j:J

    .line 90
    iget-wide v0, p1, Lio/realm/s$a;->k:J

    iput-wide v0, p2, Lio/realm/s$a;->k:J

    .line 91
    iget-wide v0, p1, Lio/realm/s$a;->l:J

    iput-wide v0, p2, Lio/realm/s$a;->l:J

    .line 92
    return-void
.end method
