.class public Lio/realm/p;
.super Lio/realm/bc;
.source "DynamicRealmObject.java"

# interfaces
.implements Lio/realm/internal/l;


# instance fields
.field private final a:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lio/realm/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/realm/g;Lio/realm/internal/n;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lio/realm/bc;-><init>()V

    .line 40
    new-instance v0, Lio/realm/au;

    invoke-direct {v0, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    .line 75
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0, p1}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 76
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0, p2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 77
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 78
    return-void
.end method


# virtual methods
.method public a()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 408
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 410
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->a()J

    move-result-wide v0

    long-to-int v0, v0

    new-array v1, v0, [Ljava/lang/String;

    .line 411
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 412
    iget-object v2, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    int-to-long v4, v0

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->d(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 411
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 414
    :cond_0
    return-object v1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 782
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 844
    iget-object v2, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->e()V

    .line 846
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 868
    :cond_0
    :goto_0
    return v1

    .line 849
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 853
    check-cast p1, Lio/realm/p;

    .line 855
    iget-object v2, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    .line 856
    iget-object v3, p1, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v3}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v3

    .line 857
    if-eqz v2, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 861
    :cond_2
    iget-object v2, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v2

    .line 862
    iget-object v3, p1, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v3}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v3

    invoke-interface {v3}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v3

    .line 864
    if-eqz v2, :cond_5

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 868
    :cond_3
    iget-object v2, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/n;->c()J

    move-result-wide v2

    iget-object v4, p1, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v4}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_6

    :goto_1
    move v1, v0

    goto :goto_0

    .line 857
    :cond_4
    if-eqz v3, :cond_2

    goto :goto_0

    .line 864
    :cond_5
    if-eqz v3, :cond_3

    goto :goto_0

    :cond_6
    move v0, v1

    .line 868
    goto :goto_1
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 829
    iget-object v1, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/g;->e()V

    .line 831
    iget-object v1, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v1

    .line 832
    iget-object v2, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v2

    .line 833
    iget-object v3, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v3}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v3

    invoke-interface {v3}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 836
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int/lit16 v1, v1, 0x20f

    .line 837
    mul-int/lit8 v1, v1, 0x1f

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    :cond_0
    add-int/2addr v0, v1

    .line 838
    mul-int/lit8 v0, v0, 0x1f

    const/16 v1, 0x20

    ushr-long v2, v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 839
    return v0

    :cond_1
    move v1, v0

    .line 836
    goto :goto_0
.end method

.method public s_()Lio/realm/au;
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 873
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 875
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 876
    const-string v0, "Invalid object"

    .line 928
    :goto_0
    return-object v0

    .line 879
    :cond_0
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v0

    .line 880
    new-instance v3, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = dynamic["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 881
    invoke-virtual {p0}, Lio/realm/p;->a()[Ljava/lang/String;

    move-result-object v4

    .line 882
    array-length v5, v4

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_7

    aget-object v0, v4, v1

    .line 883
    iget-object v6, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v6}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v6

    invoke-interface {v6, v0}, Lio/realm/internal/n;->a(Ljava/lang/String;)J

    move-result-wide v6

    .line 884
    iget-object v8, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v8}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v8

    invoke-interface {v8, v6, v7}, Lio/realm/internal/n;->e(J)Lio/realm/RealmFieldType;

    move-result-object v8

    .line 885
    const-string v9, "{"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 886
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, ":"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 887
    sget-object v0, Lio/realm/p$1;->a:[I

    invoke-virtual {v8}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v8

    aget v0, v0, v8

    packed-switch v0, :pswitch_data_0

    .line 921
    const-string v0, "?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 924
    :goto_2
    const-string v0, "},"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 882
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 889
    :pswitch_0
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->b(J)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "null"

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_3

    .line 892
    :pswitch_1
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->b(J)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "null"

    :goto_4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->f(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_4

    .line 895
    :pswitch_2
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->b(J)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "null"

    :goto_5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->h(J)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_5

    .line 898
    :pswitch_3
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->b(J)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "null"

    :goto_6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->i(J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_6

    .line 901
    :pswitch_4
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 904
    :pswitch_5
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->l(J)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 907
    :pswitch_6
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->b(J)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "null"

    :goto_7
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_5
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->j(J)Ljava/util/Date;

    move-result-object v0

    goto :goto_7

    .line 910
    :pswitch_7
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0, v6, v7}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 911
    const-string v0, "null"

    .line 910
    :goto_8
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 912
    :cond_6
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 915
    :pswitch_8
    iget-object v0, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->k()Ljava/lang/String;

    move-result-object v0

    .line 916
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "RealmList<%s>[%s]"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v0, v10, v2

    const/4 v0, 0x1

    iget-object v11, p0, Lio/realm/p;->a:Lio/realm/au;

    invoke-virtual {v11}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v11

    invoke-interface {v11, v6, v7}, Lio/realm/internal/n;->n(J)Lio/realm/internal/LinkView;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/LinkView;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v0

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 926
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const-string v2, ""

    invoke-virtual {v3, v0, v1, v2}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 927
    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 928
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 887
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public u_()V
    .locals 0

    .prologue
    .line 979
    return-void
.end method
