.class Lio/realm/bi;
.super Ljava/lang/Object;
.source "SchemaConnector.java"

# interfaces
.implements Lio/realm/internal/a/c$a;


# instance fields
.field private final a:Lio/realm/bh;


# direct methods
.method public constructor <init>(Lio/realm/bh;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lio/realm/bi;->a:Lio/realm/bh;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/realm/internal/c;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lio/realm/bi;->a:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->b(Ljava/lang/String;)Lio/realm/internal/c;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lio/realm/bi;->a:Lio/realm/bh;

    invoke-virtual {v0}, Lio/realm/bh;->d()Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lio/realm/bi;->a:Lio/realm/bh;

    invoke-virtual {v0, p1}, Lio/realm/bh;->a(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    return-wide v0
.end method
