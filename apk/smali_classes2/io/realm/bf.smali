.class public Lio/realm/bf;
.super Ljava/lang/Object;
.source "RealmQuery.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lio/realm/bb;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lio/realm/internal/Table;

.field private final b:Lio/realm/g;

.field private final c:Lio/realm/internal/TableQuery;

.field private final d:Lio/realm/be;

.field private e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:Lio/realm/internal/LinkView;


# direct methods
.method private constructor <init>(Lio/realm/av;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p1, p0, Lio/realm/bf;->b:Lio/realm/g;

    .line 121
    iput-object p2, p0, Lio/realm/bf;->e:Ljava/lang/Class;

    .line 122
    invoke-virtual {p1}, Lio/realm/av;->k()Lio/realm/bh;

    move-result-object v0

    invoke-virtual {v0, p2}, Lio/realm/bh;->b(Ljava/lang/Class;)Lio/realm/be;

    move-result-object v0

    iput-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    .line 123
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    invoke-virtual {v0}, Lio/realm/be;->b()Lio/realm/internal/Table;

    move-result-object v0

    iput-object v0, p0, Lio/realm/bf;->a:Lio/realm/internal/Table;

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/bf;->g:Lio/realm/internal/LinkView;

    .line 125
    iget-object v0, p0, Lio/realm/bf;->a:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->i()Lio/realm/internal/TableQuery;

    move-result-object v0

    iput-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    .line 126
    return-void
.end method

.method public static a(Lio/realm/av;Ljava/lang/Class;)Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/bb;",
            ">(",
            "Lio/realm/av;",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lio/realm/bf;

    invoke-direct {v0, p0, p1}, Lio/realm/bf;-><init>(Lio/realm/av;Ljava/lang/Class;)V

    return-object v0
.end method

.method private a(Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;Z)Lio/realm/bg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/TableQuery;",
            "Lio/realm/internal/SortDescriptor;",
            "Lio/realm/internal/SortDescriptor;",
            "Z)",
            "Lio/realm/bg",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1970
    new-instance v1, Lio/realm/internal/Collection;

    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    iget-object v0, v0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    invoke-direct {v1, v0, p1, p2, p3}, Lio/realm/internal/Collection;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;)V

    .line 1971
    invoke-direct {p0}, Lio/realm/bf;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1972
    new-instance v0, Lio/realm/bg;

    iget-object v2, p0, Lio/realm/bf;->b:Lio/realm/g;

    iget-object v3, p0, Lio/realm/bf;->f:Ljava/lang/String;

    invoke-direct {v0, v2, v1, v3}, Lio/realm/bg;-><init>(Lio/realm/g;Lio/realm/internal/Collection;Ljava/lang/String;)V

    .line 1976
    :goto_0
    if-eqz p4, :cond_0

    .line 1977
    invoke-virtual {v0}, Lio/realm/bg;->d()Z

    .line 1979
    :cond_0
    return-object v0

    .line 1974
    :cond_1
    new-instance v0, Lio/realm/bg;

    iget-object v2, p0, Lio/realm/bf;->b:Lio/realm/g;

    iget-object v3, p0, Lio/realm/bf;->e:Ljava/lang/Class;

    invoke-direct {v0, v2, v1, v3}, Lio/realm/bg;-><init>(Lio/realm/g;Lio/realm/internal/Collection;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 440
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x1

    new-array v1, v1, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 441
    if-nez p2, :cond_0

    .line 442
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/realm/internal/TableQuery;->a([J[J)Lio/realm/internal/TableQuery;

    .line 446
    :goto_0
    return-object p0

    .line 444
    :cond_0
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lio/realm/internal/TableQuery;->a([J[JZ)Lio/realm/internal/TableQuery;

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/l;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 256
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x1

    new-array v1, v1, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0, p2, p3}, Lio/realm/internal/TableQuery;->a([J[JLjava/lang/String;Lio/realm/l;)Lio/realm/internal/TableQuery;

    .line 258
    return-object p0
.end method

.method private i()Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1437
    iget-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->c()Lio/realm/internal/TableQuery;

    .line 1438
    return-object p0
.end method

.method private j()Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1454
    iget-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->d()Lio/realm/internal/TableQuery;

    .line 1455
    return-object p0
.end method

.method private k()Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1470
    iget-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->e()Lio/realm/internal/TableQuery;

    .line 1471
    return-object p0
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 1850
    iget-object v0, p0, Lio/realm/bf;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()J
    .locals 2

    .prologue
    .line 1983
    iget-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method private n()Lio/realm/bi;
    .locals 2

    .prologue
    .line 1987
    new-instance v0, Lio/realm/bi;

    iget-object v1, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v1}, Lio/realm/g;->k()Lio/realm/bh;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/bi;-><init>(Lio/realm/bh;)V

    return-object v0
.end method


# virtual methods
.method public a()Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1431
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1433
    invoke-direct {p0}, Lio/realm/bf;->i()Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lio/realm/bf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 203
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x0

    new-array v1, v1, [Lio/realm/RealmFieldType;

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/realm/internal/TableQuery;->a([J[J)Lio/realm/internal/TableQuery;

    .line 207
    return-object p0
.end method

.method public a(Ljava/lang/String;I)Lio/realm/bf;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1062
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1064
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x1

    new-array v1, v1, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 1065
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    int-to-long v4, p2

    invoke-virtual {v1, v2, v0, v4, v5}, Lio/realm/internal/TableQuery;->a([J[JJ)Lio/realm/internal/TableQuery;

    .line 1066
    return-object p0
.end method

.method public a(Ljava/lang/String;J)Lio/realm/bf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1078
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1080
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x1

    new-array v1, v1, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 1081
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0, p2, p3}, Lio/realm/internal/TableQuery;->a([J[JJ)Lio/realm/internal/TableQuery;

    .line 1082
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 434
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 436
    invoke-direct {p0, p1, p2}, Lio/realm/bf;->b(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 237
    sget-object v0, Lio/realm/l;->a:Lio/realm/l;

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/l;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 252
    invoke-direct {p0, p1, p2, p3}, Lio/realm/bf;->d(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/util/Date;)Lio/realm/bf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1046
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1048
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x1

    new-array v1, v1, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 1049
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0, p2}, Lio/realm/internal/TableQuery;->a([J[JLjava/util/Date;)Lio/realm/internal/TableQuery;

    .line 1050
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)Lio/realm/bf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1291
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1293
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x1

    new-array v1, v1, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 1294
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v0

    invoke-virtual {v1, v0, p2, p3}, Lio/realm/internal/TableQuery;->a([JLjava/util/Date;Ljava/util/Date;)Lio/realm/internal/TableQuery;

    .line 1295
    return-object p0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 479
    sget-object v0, Lio/realm/l;->a:Lio/realm/l;

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/bf;->a(Ljava/lang/String;[Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Lio/realm/l;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 493
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 495
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 496
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Non-empty \'values\' must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 498
    :cond_1
    invoke-direct {p0}, Lio/realm/bf;->i()Lio/realm/bf;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p2, v1

    invoke-direct {v0, p1, v1, p3}, Lio/realm/bf;->d(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    .line 499
    const/4 v0, 0x1

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_2

    .line 500
    invoke-direct {p0}, Lio/realm/bf;->k()Lio/realm/bf;

    move-result-object v1

    aget-object v2, p2, v0

    invoke-direct {v1, p1, v2, p3}, Lio/realm/bf;->d(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    .line 499
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 502
    :cond_2
    invoke-direct {p0}, Lio/realm/bf;->j()Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lio/realm/bl;)Lio/realm/bg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/bl;",
            ")",
            "Lio/realm/bg",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1792
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1794
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    iget-object v0, v0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    iget-object v0, v0, Lio/realm/internal/SharedRealm;->d:Lio/realm/internal/a;

    const-string v1, "Async query cannot be created on current thread."

    invoke-interface {v0, v1}, Lio/realm/internal/a;->a(Ljava/lang/String;)V

    .line 1795
    invoke-direct {p0}, Lio/realm/bf;->n()Lio/realm/bi;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v1}, Lio/realm/internal/TableQuery;->a()Lio/realm/internal/Table;

    move-result-object v1

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/SortDescriptor;->a(Lio/realm/internal/a/c$a;Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/bl;)Lio/realm/internal/SortDescriptor;

    move-result-object v0

    .line 1796
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v0, v2, v3}, Lio/realm/bf;->a(Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;Z)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method

.method public b()Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1448
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1450
    invoke-direct {p0}, Lio/realm/bf;->j()Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lio/realm/bf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 221
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x0

    new-array v1, v1, [Lio/realm/RealmFieldType;

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lio/realm/internal/TableQuery;->b([J[J)Lio/realm/internal/TableQuery;

    .line 225
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 690
    sget-object v0, Lio/realm/l;->a:Lio/realm/l;

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/bf;->b(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/l;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 703
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 705
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    new-array v1, v4, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 706
    invoke-virtual {v0}, Lio/realm/internal/a/c;->a()I

    move-result v1

    if-le v1, v4, :cond_0

    invoke-virtual {p3}, Lio/realm/l;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 707
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Link queries cannot be case insensitive - coming soon."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 709
    :cond_0
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0, p2, p3}, Lio/realm/internal/TableQuery;->b([J[JLjava/lang/String;Lio/realm/l;)Lio/realm/internal/TableQuery;

    .line 710
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/util/Date;)Lio/realm/bf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1206
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1208
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x1

    new-array v1, v1, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 1209
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0, p2}, Lio/realm/internal/TableQuery;->b([J[JLjava/util/Date;)Lio/realm/internal/TableQuery;

    .line 1210
    return-object p0
.end method

.method public c()Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1464
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1466
    invoke-direct {p0}, Lio/realm/bf;->k()Lio/realm/bf;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Lio/realm/l;)Lio/realm/bf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lio/realm/l;",
            ")",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1321
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1323
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    const/4 v1, 0x1

    new-array v1, v1, [Lio/realm/RealmFieldType;

    const/4 v2, 0x0

    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Lio/realm/be;->a(Ljava/lang/String;[Lio/realm/RealmFieldType;)Lio/realm/internal/a/c;

    move-result-object v0

    .line 1324
    iget-object v1, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/a/c;->b()[J

    move-result-object v2

    invoke-virtual {v0}, Lio/realm/internal/a/c;->c()[J

    move-result-object v0

    invoke-virtual {v1, v2, v0, p2, p3}, Lio/realm/internal/TableQuery;->c([J[JLjava/lang/String;Lio/realm/l;)Lio/realm/internal/TableQuery;

    .line 1325
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/Number;
    .locals 6

    .prologue
    .line 1593
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1595
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    invoke-virtual {v0, p1}, Lio/realm/be;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 1596
    sget-object v2, Lio/realm/bf$1;->a:[I

    iget-object v3, p0, Lio/realm/bf;->a:Lio/realm/internal/Table;

    invoke-virtual {v3, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1604
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Field \'%s\': type mismatch - %s expected."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    const-string v5, "int, float or double"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1598
    :pswitch_0
    iget-object v2, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->a(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1602
    :goto_0
    return-object v0

    .line 1600
    :pswitch_1
    iget-object v2, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->b(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 1602
    :pswitch_2
    iget-object v2, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->c(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 1596
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public d()Lio/realm/bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1480
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1482
    iget-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->f()Lio/realm/internal/TableQuery;

    .line 1483
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ljava/util/Date;
    .locals 3

    .prologue
    .line 1714
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1716
    iget-object v0, p0, Lio/realm/bf;->d:Lio/realm/be;

    invoke-virtual {v0, p1}, Lio/realm/be;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 1717
    iget-object v2, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/TableQuery;->d(J)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 1727
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1729
    iget-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    invoke-virtual {v0}, Lio/realm/internal/TableQuery;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Lio/realm/bg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bg",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1741
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1743
    iget-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, v2, v1}, Lio/realm/bf;->a(Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;Z)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method

.method public g()Lio/realm/bg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/bg",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1755
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1757
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    iget-object v0, v0, Lio/realm/g;->e:Lio/realm/internal/SharedRealm;

    iget-object v0, v0, Lio/realm/internal/SharedRealm;->d:Lio/realm/internal/a;

    const-string v1, "Async query cannot be created on current thread."

    invoke-interface {v0, v1}, Lio/realm/internal/a;->a(Ljava/lang/String;)V

    .line 1758
    iget-object v0, p0, Lio/realm/bf;->c:Lio/realm/internal/TableQuery;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v2, v1}, Lio/realm/bf;->a(Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Lio/realm/internal/SortDescriptor;Z)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method

.method public h()Lio/realm/bb;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1913
    iget-object v0, p0, Lio/realm/bf;->b:Lio/realm/g;

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 1915
    invoke-direct {p0}, Lio/realm/bf;->m()J

    move-result-wide v0

    .line 1916
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lio/realm/bf;->b:Lio/realm/g;

    iget-object v3, p0, Lio/realm/bf;->e:Ljava/lang/Class;

    iget-object v4, p0, Lio/realm/bf;->f:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0, v1}, Lio/realm/g;->a(Ljava/lang/Class;Ljava/lang/String;J)Lio/realm/bb;

    move-result-object v0

    goto :goto_0
.end method
