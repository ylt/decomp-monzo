.class public Lio/realm/bm;
.super Lco/uk/getmondo/d/aj;
.source "TransactionRealmProxy.java"

# interfaces
.implements Lio/realm/bn;
.implements Lio/realm/internal/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/bm$a;
    }
.end annotation


# static fields
.field private static final c:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lio/realm/bm$a;

.field private b:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lio/realm/az;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 139
    invoke-static {}, Lio/realm/bm;->aj()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/bm;->c:Lio/realm/internal/OsObjectSchemaInfo;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 144
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    const-string v1, "amountValue"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    const-string v1, "amountCurrency"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    const-string v1, "amountLocalValue"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    const-string v1, "amountLocalCurrency"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    const-string v1, "created"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    const-string v1, "createdDateFormatted"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    const-string v1, "updated"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    const-string v1, "merchantDescription"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    const-string v1, "description"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    const-string v1, "declineReason"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    const-string v1, "notes"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    const-string v1, "hideAmount"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    const-string v1, "settled"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    const-string v1, "merchant"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    const-string v1, "category"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    const-string v1, "fromAtm"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    const-string v1, "peerToPeer"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    const-string v1, "topUp"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    const-string v1, "includeInSpending"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    const-string v1, "attachments"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v1, "peer"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v1, "bankDetails"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    const-string v1, "fromMonzoMe"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    const-string v1, "scheme"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    const-string v1, "bacsDirectDebitInstructionId"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/bm;->e:Ljava/util/List;

    .line 171
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Lco/uk/getmondo/d/aj;-><init>()V

    .line 174
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 175
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/aj;Ljava/util/Map;)J
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/aj;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1911
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1912
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 2028
    :cond_0
    :goto_0
    return-wide v4

    .line 1914
    :cond_1
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 1915
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 1916
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/bm$a;

    .line 1917
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 1918
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->H()Ljava/lang/String;

    move-result-object v2

    .line 1920
    if-nez v2, :cond_e

    .line 1921
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 1925
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_f

    .line 1926
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 1930
    :goto_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931
    iget-wide v2, v9, Lio/realm/bm$a;->b:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->I()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 1932
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->J()Ljava/lang/String;

    move-result-object v6

    .line 1933
    if-eqz v6, :cond_2

    .line 1934
    iget-wide v2, v9, Lio/realm/bm$a;->c:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1936
    :cond_2
    iget-wide v2, v9, Lio/realm/bm$a;->d:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->K()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 1937
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->L()Ljava/lang/String;

    move-result-object v6

    .line 1938
    if-eqz v6, :cond_3

    .line 1939
    iget-wide v2, v9, Lio/realm/bm$a;->e:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_3
    move-object v2, p1

    .line 1941
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->M()Ljava/util/Date;

    move-result-object v6

    .line 1942
    if-eqz v6, :cond_4

    .line 1943
    iget-wide v2, v9, Lio/realm/bm$a;->f:J

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :cond_4
    move-object v2, p1

    .line 1945
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->N()Ljava/lang/String;

    move-result-object v6

    .line 1946
    if-eqz v6, :cond_5

    .line 1947
    iget-wide v2, v9, Lio/realm/bm$a;->g:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_5
    move-object v2, p1

    .line 1949
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->O()Ljava/util/Date;

    move-result-object v6

    .line 1950
    if-eqz v6, :cond_6

    .line 1951
    iget-wide v2, v9, Lio/realm/bm$a;->h:J

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :cond_6
    move-object v2, p1

    .line 1953
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->P()Ljava/lang/String;

    move-result-object v6

    .line 1954
    if-eqz v6, :cond_7

    .line 1955
    iget-wide v2, v9, Lio/realm/bm$a;->i:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_7
    move-object v2, p1

    .line 1957
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->Q()Ljava/lang/String;

    move-result-object v6

    .line 1958
    if-eqz v6, :cond_8

    .line 1959
    iget-wide v2, v9, Lio/realm/bm$a;->j:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_8
    move-object v2, p1

    .line 1961
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->R()Ljava/lang/String;

    move-result-object v6

    .line 1962
    if-eqz v6, :cond_9

    .line 1963
    iget-wide v2, v9, Lio/realm/bm$a;->k:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_9
    move-object v2, p1

    .line 1965
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->S()Ljava/lang/String;

    move-result-object v6

    .line 1966
    if-eqz v6, :cond_a

    .line 1967
    iget-wide v2, v9, Lio/realm/bm$a;->l:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1969
    :cond_a
    iget-wide v2, v9, Lio/realm/bm$a;->m:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->T()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 1970
    iget-wide v2, v9, Lio/realm/bm$a;->n:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->U()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v2, p1

    .line 1972
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->V()Lco/uk/getmondo/d/u;

    move-result-object v3

    .line 1973
    if-eqz v3, :cond_b

    .line 1974
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1975
    if-nez v2, :cond_16

    .line 1976
    invoke-static {p0, v3, p2}, Lio/realm/ag;->a(Lio/realm/av;Lco/uk/getmondo/d/u;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1978
    :goto_3
    iget-wide v2, v9, Lio/realm/bm$a;->o:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :cond_b
    move-object v2, p1

    .line 1980
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->W()Ljava/lang/String;

    move-result-object v6

    .line 1981
    if-eqz v6, :cond_c

    .line 1982
    iget-wide v2, v9, Lio/realm/bm$a;->p:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1984
    :cond_c
    iget-wide v2, v9, Lio/realm/bm$a;->q:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->X()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 1985
    iget-wide v2, v9, Lio/realm/bm$a;->r:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->Y()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 1986
    iget-wide v2, v9, Lio/realm/bm$a;->s:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->Z()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 1987
    iget-wide v2, v9, Lio/realm/bm$a;->t:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->aa()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v2, p1

    .line 1989
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->ab()Lio/realm/az;

    move-result-object v6

    .line 1990
    if-eqz v6, :cond_10

    .line 1991
    iget-wide v2, v9, Lio/realm/bm$a;->u:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeGetLinkView(JJJ)J

    move-result-wide v10

    .line 1992
    invoke-virtual {v6}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/d;

    .line 1993
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 1994
    if-nez v3, :cond_d

    .line 1995
    invoke-static {p0, v2, p2}, Lio/realm/c;->a(Lio/realm/av;Lco/uk/getmondo/d/d;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1997
    :cond_d
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v10, v11, v2, v3}, Lio/realm/internal/LinkView;->nativeAdd(JJ)V

    goto :goto_4

    .line 1923
    :cond_e
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 1928
    :cond_f
    invoke-static {v2}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_10
    move-object v2, p1

    .line 2002
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v3

    .line 2003
    if-eqz v3, :cond_11

    .line 2004
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 2005
    if-nez v2, :cond_15

    .line 2006
    invoke-static {p0, v3, p2}, Lio/realm/as;->a(Lio/realm/av;Lco/uk/getmondo/d/aa;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 2008
    :goto_5
    iget-wide v2, v9, Lio/realm/bm$a;->v:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :cond_11
    move-object v2, p1

    .line 2011
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v3

    .line 2012
    if-eqz v3, :cond_12

    .line 2013
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 2014
    if-nez v2, :cond_14

    .line 2015
    invoke-static {p0, v3, p2}, Lio/realm/e;->a(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 2017
    :goto_6
    iget-wide v2, v9, Lio/realm/bm$a;->w:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 2019
    :cond_12
    iget-wide v2, v9, Lio/realm/bm$a;->x:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->ae()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v2, p1

    .line 2020
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->af()Ljava/lang/String;

    move-result-object v6

    .line 2021
    if-eqz v6, :cond_13

    .line 2022
    iget-wide v2, v9, Lio/realm/bm$a;->y:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2024
    :cond_13
    check-cast p1, Lio/realm/bn;

    invoke-interface {p1}, Lio/realm/bn;->ag()Ljava/lang/String;

    move-result-object v6

    .line 2025
    if-eqz v6, :cond_0

    .line 2026
    iget-wide v2, v9, Lio/realm/bm$a;->z:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    :cond_14
    move-object v6, v2

    goto :goto_6

    :cond_15
    move-object v6, v2

    goto :goto_5

    :cond_16
    move-object v6, v2

    goto/16 :goto_3
.end method

.method public static a(Lco/uk/getmondo/d/aj;IILjava/util/Map;)Lco/uk/getmondo/d/aj;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/aj;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/d/aj;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2466
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2530
    :goto_0
    return-object v0

    .line 2469
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 2471
    if-nez v0, :cond_3

    .line 2472
    new-instance v1, Lco/uk/getmondo/d/aj;

    invoke-direct {v1}, Lco/uk/getmondo/d/aj;-><init>()V

    .line 2473
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 2482
    check-cast v0, Lio/realm/bn;

    .line 2483
    check-cast p0, Lio/realm/bn;

    .line 2484
    invoke-interface {p0}, Lio/realm/bn;->H()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->c(Ljava/lang/String;)V

    .line 2485
    invoke-interface {p0}, Lio/realm/bn;->I()J

    move-result-wide v4

    invoke-interface {v0, v4, v5}, Lio/realm/bn;->a(J)V

    .line 2486
    invoke-interface {p0}, Lio/realm/bn;->J()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->d(Ljava/lang/String;)V

    .line 2487
    invoke-interface {p0}, Lio/realm/bn;->K()J

    move-result-wide v4

    invoke-interface {v0, v4, v5}, Lio/realm/bn;->b(J)V

    .line 2488
    invoke-interface {p0}, Lio/realm/bn;->L()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->e(Ljava/lang/String;)V

    .line 2489
    invoke-interface {p0}, Lio/realm/bn;->M()Ljava/util/Date;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->a(Ljava/util/Date;)V

    .line 2490
    invoke-interface {p0}, Lio/realm/bn;->N()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->f(Ljava/lang/String;)V

    .line 2491
    invoke-interface {p0}, Lio/realm/bn;->O()Ljava/util/Date;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->b(Ljava/util/Date;)V

    .line 2492
    invoke-interface {p0}, Lio/realm/bn;->P()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->g(Ljava/lang/String;)V

    .line 2493
    invoke-interface {p0}, Lio/realm/bn;->Q()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->h(Ljava/lang/String;)V

    .line 2494
    invoke-interface {p0}, Lio/realm/bn;->R()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->i(Ljava/lang/String;)V

    .line 2495
    invoke-interface {p0}, Lio/realm/bn;->S()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->j(Ljava/lang/String;)V

    .line 2496
    invoke-interface {p0}, Lio/realm/bn;->T()Z

    move-result v3

    invoke-interface {v0, v3}, Lio/realm/bn;->a(Z)V

    .line 2497
    invoke-interface {p0}, Lio/realm/bn;->U()Z

    move-result v3

    invoke-interface {v0, v3}, Lio/realm/bn;->b(Z)V

    .line 2500
    invoke-interface {p0}, Lio/realm/bn;->V()Lco/uk/getmondo/d/u;

    move-result-object v3

    add-int/lit8 v4, p1, 0x1

    invoke-static {v3, v4, p2, p3}, Lio/realm/ag;->a(Lco/uk/getmondo/d/u;IILjava/util/Map;)Lco/uk/getmondo/d/u;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->a(Lco/uk/getmondo/d/u;)V

    .line 2501
    invoke-interface {p0}, Lio/realm/bn;->W()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lio/realm/bn;->k(Ljava/lang/String;)V

    .line 2502
    invoke-interface {p0}, Lio/realm/bn;->X()Z

    move-result v3

    invoke-interface {v0, v3}, Lio/realm/bn;->c(Z)V

    .line 2503
    invoke-interface {p0}, Lio/realm/bn;->Y()Z

    move-result v3

    invoke-interface {v0, v3}, Lio/realm/bn;->d(Z)V

    .line 2504
    invoke-interface {p0}, Lio/realm/bn;->Z()Z

    move-result v3

    invoke-interface {v0, v3}, Lio/realm/bn;->e(Z)V

    .line 2505
    invoke-interface {p0}, Lio/realm/bn;->aa()Z

    move-result v3

    invoke-interface {v0, v3}, Lio/realm/bn;->f(Z)V

    .line 2508
    if-ne p1, p2, :cond_5

    .line 2509
    invoke-interface {v0, v2}, Lio/realm/bn;->a(Lio/realm/az;)V

    .line 2523
    :cond_2
    invoke-interface {p0}, Lio/realm/bn;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/as;->a(Lco/uk/getmondo/d/aa;IILjava/util/Map;)Lco/uk/getmondo/d/aa;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/bn;->a(Lco/uk/getmondo/d/aa;)V

    .line 2526
    invoke-interface {p0}, Lio/realm/bn;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/e;->a(Lco/uk/getmondo/payments/send/data/a/a;IILjava/util/Map;)Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/bn;->a(Lco/uk/getmondo/payments/send/data/a/a;)V

    .line 2527
    invoke-interface {p0}, Lio/realm/bn;->ae()Z

    move-result v2

    invoke-interface {v0, v2}, Lio/realm/bn;->g(Z)V

    .line 2528
    invoke-interface {p0}, Lio/realm/bn;->af()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/bn;->l(Ljava/lang/String;)V

    .line 2529
    invoke-interface {p0}, Lio/realm/bn;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/bn;->m(Ljava/lang/String;)V

    move-object v0, v1

    .line 2530
    goto/16 :goto_0

    .line 2476
    :cond_3
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_4

    .line 2477
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/d/aj;

    goto/16 :goto_0

    .line 2479
    :cond_4
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/d/aj;

    .line 2480
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto/16 :goto_1

    .line 2511
    :cond_5
    invoke-interface {p0}, Lio/realm/bn;->ab()Lio/realm/az;

    move-result-object v4

    .line 2512
    new-instance v5, Lio/realm/az;

    invoke-direct {v5}, Lio/realm/az;-><init>()V

    .line 2513
    invoke-interface {v0, v5}, Lio/realm/bn;->a(Lio/realm/az;)V

    .line 2514
    add-int/lit8 v6, p1, 0x1

    .line 2515
    invoke-virtual {v4}, Lio/realm/az;->size()I

    move-result v7

    .line 2516
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_2

    .line 2517
    invoke-virtual {v4, v3}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/d;

    invoke-static {v2, v6, p2, p3}, Lio/realm/c;->a(Lco/uk/getmondo/d/d;IILjava/util/Map;)Lco/uk/getmondo/d/d;

    move-result-object v2

    .line 2518
    invoke-virtual {v5, v2}, Lio/realm/az;->a(Lio/realm/bb;)Z

    .line 2516
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2
.end method

.method static a(Lio/realm/av;Lco/uk/getmondo/d/aj;Lco/uk/getmondo/d/aj;Ljava/util/Map;)Lco/uk/getmondo/d/aj;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/aj;",
            "Lco/uk/getmondo/d/aj;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/aj;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2534
    move-object v0, p1

    check-cast v0, Lio/realm/bn;

    .line 2535
    check-cast p2, Lio/realm/bn;

    .line 2536
    invoke-interface {p2}, Lio/realm/bn;->I()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/bn;->a(J)V

    .line 2537
    invoke-interface {p2}, Lio/realm/bn;->J()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->d(Ljava/lang/String;)V

    .line 2538
    invoke-interface {p2}, Lio/realm/bn;->K()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/bn;->b(J)V

    .line 2539
    invoke-interface {p2}, Lio/realm/bn;->L()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->e(Ljava/lang/String;)V

    .line 2540
    invoke-interface {p2}, Lio/realm/bn;->M()Ljava/util/Date;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->a(Ljava/util/Date;)V

    .line 2541
    invoke-interface {p2}, Lio/realm/bn;->N()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->f(Ljava/lang/String;)V

    .line 2542
    invoke-interface {p2}, Lio/realm/bn;->O()Ljava/util/Date;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->b(Ljava/util/Date;)V

    .line 2543
    invoke-interface {p2}, Lio/realm/bn;->P()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->g(Ljava/lang/String;)V

    .line 2544
    invoke-interface {p2}, Lio/realm/bn;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->h(Ljava/lang/String;)V

    .line 2545
    invoke-interface {p2}, Lio/realm/bn;->R()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->i(Ljava/lang/String;)V

    .line 2546
    invoke-interface {p2}, Lio/realm/bn;->S()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->j(Ljava/lang/String;)V

    .line 2547
    invoke-interface {p2}, Lio/realm/bn;->T()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/bn;->a(Z)V

    .line 2548
    invoke-interface {p2}, Lio/realm/bn;->U()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/bn;->b(Z)V

    .line 2549
    invoke-interface {p2}, Lio/realm/bn;->V()Lco/uk/getmondo/d/u;

    move-result-object v2

    .line 2550
    if-nez v2, :cond_0

    .line 2551
    invoke-interface {v0, v7}, Lio/realm/bn;->a(Lco/uk/getmondo/d/u;)V

    .line 2560
    :goto_0
    invoke-interface {p2}, Lio/realm/bn;->W()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->k(Ljava/lang/String;)V

    .line 2561
    invoke-interface {p2}, Lio/realm/bn;->X()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/bn;->c(Z)V

    .line 2562
    invoke-interface {p2}, Lio/realm/bn;->Y()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/bn;->d(Z)V

    .line 2563
    invoke-interface {p2}, Lio/realm/bn;->Z()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/bn;->e(Z)V

    .line 2564
    invoke-interface {p2}, Lio/realm/bn;->aa()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/bn;->f(Z)V

    .line 2565
    invoke-interface {p2}, Lio/realm/bn;->ab()Lio/realm/az;

    move-result-object v4

    .line 2566
    invoke-interface {v0}, Lio/realm/bn;->ab()Lio/realm/az;

    move-result-object v5

    .line 2567
    invoke-virtual {v5}, Lio/realm/az;->clear()V

    .line 2568
    if-eqz v4, :cond_3

    .line 2569
    const/4 v1, 0x0

    move v3, v1

    :goto_1
    invoke-virtual {v4}, Lio/realm/az;->size()I

    move-result v1

    if-ge v3, v1, :cond_3

    .line 2570
    invoke-virtual {v4, v3}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/d;

    .line 2571
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/d;

    .line 2572
    if-eqz v2, :cond_2

    .line 2573
    invoke-virtual {v5, v2}, Lio/realm/az;->a(Lio/realm/bb;)Z

    .line 2569
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2553
    :cond_0
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/u;

    .line 2554
    if-eqz v1, :cond_1

    .line 2555
    invoke-interface {v0, v1}, Lio/realm/bn;->a(Lco/uk/getmondo/d/u;)V

    goto :goto_0

    .line 2557
    :cond_1
    invoke-static {p0, v2, v6, p3}, Lio/realm/ag;->a(Lio/realm/av;Lco/uk/getmondo/d/u;ZLjava/util/Map;)Lco/uk/getmondo/d/u;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->a(Lco/uk/getmondo/d/u;)V

    goto :goto_0

    .line 2575
    :cond_2
    invoke-static {p0, v1, v6, p3}, Lio/realm/c;->a(Lio/realm/av;Lco/uk/getmondo/d/d;ZLjava/util/Map;)Lco/uk/getmondo/d/d;

    move-result-object v1

    invoke-virtual {v5, v1}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_2

    .line 2579
    :cond_3
    invoke-interface {p2}, Lio/realm/bn;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v2

    .line 2580
    if-nez v2, :cond_4

    .line 2581
    invoke-interface {v0, v7}, Lio/realm/bn;->a(Lco/uk/getmondo/d/aa;)V

    .line 2590
    :goto_3
    invoke-interface {p2}, Lio/realm/bn;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v2

    .line 2591
    if-nez v2, :cond_6

    .line 2592
    invoke-interface {v0, v7}, Lio/realm/bn;->a(Lco/uk/getmondo/payments/send/data/a/a;)V

    .line 2601
    :goto_4
    invoke-interface {p2}, Lio/realm/bn;->ae()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/bn;->g(Z)V

    .line 2602
    invoke-interface {p2}, Lio/realm/bn;->af()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->l(Ljava/lang/String;)V

    .line 2603
    invoke-interface {p2}, Lio/realm/bn;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->m(Ljava/lang/String;)V

    .line 2604
    return-object p1

    .line 2583
    :cond_4
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/aa;

    .line 2584
    if-eqz v1, :cond_5

    .line 2585
    invoke-interface {v0, v1}, Lio/realm/bn;->a(Lco/uk/getmondo/d/aa;)V

    goto :goto_3

    .line 2587
    :cond_5
    invoke-static {p0, v2, v6, p3}, Lio/realm/as;->a(Lio/realm/av;Lco/uk/getmondo/d/aa;ZLjava/util/Map;)Lco/uk/getmondo/d/aa;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->a(Lco/uk/getmondo/d/aa;)V

    goto :goto_3

    .line 2594
    :cond_6
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/payments/send/data/a/a;

    .line 2595
    if-eqz v1, :cond_7

    .line 2596
    invoke-interface {v0, v1}, Lio/realm/bn;->a(Lco/uk/getmondo/payments/send/data/a/a;)V

    goto :goto_4

    .line 2598
    :cond_7
    invoke-static {p0, v2, v6, p3}, Lio/realm/e;->a(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;ZLjava/util/Map;)Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/bn;->a(Lco/uk/getmondo/payments/send/data/a/a;)V

    goto :goto_4
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/d/aj;ZLjava/util/Map;)Lco/uk/getmondo/d/aj;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/aj;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/aj;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1778
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-wide v2, v2, Lio/realm/g;->c:J

    iget-wide v6, p0, Lio/realm/av;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 1779
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1781
    :cond_0
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1818
    :goto_0
    return-object p1

    .line 1784
    :cond_1
    sget-object v2, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v2}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/g$b;

    .line 1785
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/l;

    .line 1786
    if-eqz v3, :cond_2

    .line 1787
    check-cast v3, Lco/uk/getmondo/d/aj;

    move-object p1, v3

    goto :goto_0

    .line 1790
    :cond_2
    const/4 v5, 0x0

    .line 1792
    if-eqz p2, :cond_6

    .line 1793
    const-class v3, Lco/uk/getmondo/d/aj;

    invoke-virtual {p0, v3}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 1794
    invoke-virtual {v8}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v3, p1

    .line 1795
    check-cast v3, Lio/realm/bn;

    invoke-interface {v3}, Lio/realm/bn;->H()Ljava/lang/String;

    move-result-object v3

    .line 1797
    if-nez v3, :cond_3

    .line 1798
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v6

    .line 1802
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-eqz v3, :cond_4

    .line 1804
    :try_start_0
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    iget-object v3, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/aj;

    invoke-virtual {v3, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 1805
    new-instance v4, Lio/realm/bm;

    invoke-direct {v4}, Lio/realm/bm;-><init>()V

    .line 1806
    move-object v0, v4

    check-cast v0, Lio/realm/internal/l;

    move-object v3, v0

    invoke-interface {p3, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1808
    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    move v2, p2

    .line 1815
    :goto_2
    if-eqz v2, :cond_5

    .line 1816
    invoke-static {p0, v4, p1, p3}, Lio/realm/bm;->a(Lio/realm/av;Lco/uk/getmondo/d/aj;Lco/uk/getmondo/d/aj;Ljava/util/Map;)Lco/uk/getmondo/d/aj;

    move-result-object p1

    goto :goto_0

    .line 1800
    :cond_3
    invoke-virtual {v8, v6, v7, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 1808
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    throw v3

    :cond_4
    move v2, v4

    move-object v4, v5

    .line 1811
    goto :goto_2

    .line 1818
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/bm;->b(Lio/realm/av;Lco/uk/getmondo/d/aj;ZLjava/util/Map;)Lco/uk/getmondo/d/aj;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v2, p2

    move-object v4, v5

    goto :goto_2
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/bm$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0x1a

    .line 1015
    const-string v0, "class_Transaction"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1016
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'Transaction\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1018
    :cond_0
    const-string v0, "class_Transaction"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 1019
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 1020
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 1021
    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 1022
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 26 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1024
    :cond_1
    if-eqz p1, :cond_3

    .line 1025
    const-string v0, "Field count is more than expected - expected 26 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1030
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1031
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 1032
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1031
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 1027
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 26 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1035
    :cond_4
    new-instance v0, Lio/realm/bm$a;

    invoke-direct {v0, p0, v2}, Lio/realm/bm$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 1037
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1038
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1040
    :cond_5
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lio/realm/bm$a;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 1041
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1045
    :cond_6
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1046
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1048
    :cond_7
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_8

    .line 1049
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'id\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1051
    :cond_8
    iget-wide v4, v0, Lio/realm/bm$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1052
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@PrimaryKey field \'id\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1054
    :cond_9
    const-string v1, "id"

    invoke-virtual {v2, v1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->j(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1055
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1057
    :cond_a
    const-string v1, "amountValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1058
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'amountValue\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1060
    :cond_b
    const-string v1, "amountValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_c

    .line 1061
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'long\' for field \'amountValue\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1063
    :cond_c
    iget-wide v4, v0, Lio/realm/bm$a;->b:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1064
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'amountValue\' does support null values in the existing Realm file. Use corresponding boxed type for field \'amountValue\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1066
    :cond_d
    const-string v1, "amountCurrency"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1067
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'amountCurrency\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1069
    :cond_e
    const-string v1, "amountCurrency"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_f

    .line 1070
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'amountCurrency\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1072
    :cond_f
    iget-wide v4, v0, Lio/realm/bm$a;->c:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_10

    .line 1073
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'amountCurrency\' is required. Either set @Required to field \'amountCurrency\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1075
    :cond_10
    const-string v1, "amountLocalValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 1076
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'amountLocalValue\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1078
    :cond_11
    const-string v1, "amountLocalValue"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_12

    .line 1079
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'long\' for field \'amountLocalValue\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1081
    :cond_12
    iget-wide v4, v0, Lio/realm/bm$a;->d:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1082
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'amountLocalValue\' does support null values in the existing Realm file. Use corresponding boxed type for field \'amountLocalValue\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1084
    :cond_13
    const-string v1, "amountLocalCurrency"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 1085
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'amountLocalCurrency\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1087
    :cond_14
    const-string v1, "amountLocalCurrency"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_15

    .line 1088
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'amountLocalCurrency\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1090
    :cond_15
    iget-wide v4, v0, Lio/realm/bm$a;->e:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_16

    .line 1091
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'amountLocalCurrency\' is required. Either set @Required to field \'amountLocalCurrency\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1093
    :cond_16
    const-string v1, "created"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1094
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'created\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1096
    :cond_17
    const-string v1, "created"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_18

    .line 1097
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'Date\' for field \'created\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1099
    :cond_18
    iget-wide v4, v0, Lio/realm/bm$a;->f:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_19

    .line 1100
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'created\' is required. Either set @Required to field \'created\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1102
    :cond_19
    const-string v1, "createdDateFormatted"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    .line 1103
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'createdDateFormatted\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1105
    :cond_1a
    const-string v1, "createdDateFormatted"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_1b

    .line 1106
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'createdDateFormatted\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1108
    :cond_1b
    iget-wide v4, v0, Lio/realm/bm$a;->g:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 1109
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'createdDateFormatted\' is required. Either set @Required to field \'createdDateFormatted\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1111
    :cond_1c
    const-string v1, "updated"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 1112
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'updated\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1114
    :cond_1d
    const-string v1, "updated"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_1e

    .line 1115
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'Date\' for field \'updated\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1117
    :cond_1e
    iget-wide v4, v0, Lio/realm/bm$a;->h:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 1118
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'updated\' is required. Either set @Required to field \'updated\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1120
    :cond_1f
    const-string v1, "merchantDescription"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_20

    .line 1121
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'merchantDescription\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1123
    :cond_20
    const-string v1, "merchantDescription"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_21

    .line 1124
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'merchantDescription\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1126
    :cond_21
    iget-wide v4, v0, Lio/realm/bm$a;->i:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_22

    .line 1127
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'merchantDescription\' is required. Either set @Required to field \'merchantDescription\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1129
    :cond_22
    const-string v1, "description"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    .line 1130
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'description\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1132
    :cond_23
    const-string v1, "description"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_24

    .line 1133
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'description\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1135
    :cond_24
    iget-wide v4, v0, Lio/realm/bm$a;->j:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_25

    .line 1136
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'description\' is required. Either set @Required to field \'description\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1138
    :cond_25
    const-string v1, "declineReason"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_26

    .line 1139
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'declineReason\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1141
    :cond_26
    const-string v1, "declineReason"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_27

    .line 1142
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'declineReason\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1144
    :cond_27
    iget-wide v4, v0, Lio/realm/bm$a;->k:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_28

    .line 1145
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'declineReason\' is required. Either set @Required to field \'declineReason\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1147
    :cond_28
    const-string v1, "notes"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_29

    .line 1148
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'notes\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1150
    :cond_29
    const-string v1, "notes"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_2a

    .line 1151
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'notes\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1153
    :cond_2a
    iget-wide v4, v0, Lio/realm/bm$a;->l:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_2b

    .line 1154
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'notes\' is required. Either set @Required to field \'notes\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1156
    :cond_2b
    const-string v1, "hideAmount"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2c

    .line 1157
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'hideAmount\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1159
    :cond_2c
    const-string v1, "hideAmount"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_2d

    .line 1160
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'hideAmount\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1162
    :cond_2d
    iget-wide v4, v0, Lio/realm/bm$a;->m:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 1163
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'hideAmount\' does support null values in the existing Realm file. Use corresponding boxed type for field \'hideAmount\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1165
    :cond_2e
    const-string v1, "settled"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2f

    .line 1166
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'settled\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1168
    :cond_2f
    const-string v1, "settled"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_30

    .line 1169
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'settled\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1171
    :cond_30
    iget-wide v4, v0, Lio/realm/bm$a;->n:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 1172
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'settled\' does support null values in the existing Realm file. Use corresponding boxed type for field \'settled\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1174
    :cond_31
    const-string v1, "merchant"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_32

    .line 1175
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'merchant\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1177
    :cond_32
    const-string v1, "merchant"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_33

    .line 1178
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'Merchant\' for field \'merchant\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1180
    :cond_33
    const-string v1, "class_Merchant"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_34

    .line 1181
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_Merchant\' for field \'merchant\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1183
    :cond_34
    const-string v1, "class_Merchant"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 1184
    iget-wide v4, v0, Lio/realm/bm$a;->o:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_35

    .line 1185
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'merchant\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/bm$a;->o:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 1187
    :cond_35
    const-string v1, "category"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_36

    .line 1188
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'category\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1190
    :cond_36
    const-string v1, "category"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_37

    .line 1191
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'category\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1193
    :cond_37
    iget-wide v4, v0, Lio/realm/bm$a;->p:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_38

    .line 1194
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'category\' is required. Either set @Required to field \'category\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1196
    :cond_38
    const-string v1, "fromAtm"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_39

    .line 1197
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'fromAtm\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1199
    :cond_39
    const-string v1, "fromAtm"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_3a

    .line 1200
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'fromAtm\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1202
    :cond_3a
    iget-wide v4, v0, Lio/realm/bm$a;->q:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 1203
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'fromAtm\' does support null values in the existing Realm file. Use corresponding boxed type for field \'fromAtm\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1205
    :cond_3b
    const-string v1, "peerToPeer"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3c

    .line 1206
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'peerToPeer\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1208
    :cond_3c
    const-string v1, "peerToPeer"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_3d

    .line 1209
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'peerToPeer\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1211
    :cond_3d
    iget-wide v4, v0, Lio/realm/bm$a;->r:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 1212
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'peerToPeer\' does support null values in the existing Realm file. Use corresponding boxed type for field \'peerToPeer\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1214
    :cond_3e
    const-string v1, "topUp"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3f

    .line 1215
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'topUp\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1217
    :cond_3f
    const-string v1, "topUp"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_40

    .line 1218
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'topUp\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1220
    :cond_40
    iget-wide v4, v0, Lio/realm/bm$a;->s:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 1221
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'topUp\' does support null values in the existing Realm file. Use corresponding boxed type for field \'topUp\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1223
    :cond_41
    const-string v1, "includeInSpending"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_42

    .line 1224
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'includeInSpending\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1226
    :cond_42
    const-string v1, "includeInSpending"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_43

    .line 1227
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'includeInSpending\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1229
    :cond_43
    iget-wide v4, v0, Lio/realm/bm$a;->t:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_44

    .line 1230
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'includeInSpending\' does support null values in the existing Realm file. Use corresponding boxed type for field \'includeInSpending\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1232
    :cond_44
    const-string v1, "attachments"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_45

    .line 1233
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'attachments\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1235
    :cond_45
    const-string v1, "attachments"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_46

    .line 1236
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'Attachment\' for field \'attachments\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1238
    :cond_46
    const-string v1, "class_Attachment"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_47

    .line 1239
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_Attachment\' for field \'attachments\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1241
    :cond_47
    const-string v1, "class_Attachment"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 1242
    iget-wide v4, v0, Lio/realm/bm$a;->u:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_48

    .line 1243
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmList type for field \'attachments\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/bm$a;->u:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 1245
    :cond_48
    const-string v1, "peer"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_49

    .line 1246
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'peer\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1248
    :cond_49
    const-string v1, "peer"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_4a

    .line 1249
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'Peer\' for field \'peer\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1251
    :cond_4a
    const-string v1, "class_Peer"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4b

    .line 1252
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_Peer\' for field \'peer\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1254
    :cond_4b
    const-string v1, "class_Peer"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 1255
    iget-wide v4, v0, Lio/realm/bm$a;->v:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_4c

    .line 1256
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'peer\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/bm$a;->v:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 1258
    :cond_4c
    const-string v1, "bankDetails"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4d

    .line 1259
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'bankDetails\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1261
    :cond_4d
    const-string v1, "bankDetails"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_4e

    .line 1262
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'BankDetails\' for field \'bankDetails\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1264
    :cond_4e
    const-string v1, "class_BankDetails"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4f

    .line 1265
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_BankDetails\' for field \'bankDetails\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1267
    :cond_4f
    const-string v1, "class_BankDetails"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 1268
    iget-wide v4, v0, Lio/realm/bm$a;->w:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_50

    .line 1269
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'bankDetails\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/bm$a;->w:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 1271
    :cond_50
    const-string v1, "fromMonzoMe"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_51

    .line 1272
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'fromMonzoMe\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1274
    :cond_51
    const-string v1, "fromMonzoMe"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_52

    .line 1275
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'boolean\' for field \'fromMonzoMe\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1277
    :cond_52
    iget-wide v4, v0, Lio/realm/bm$a;->x:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_53

    .line 1278
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'fromMonzoMe\' does support null values in the existing Realm file. Use corresponding boxed type for field \'fromMonzoMe\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1280
    :cond_53
    const-string v1, "scheme"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_54

    .line 1281
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'scheme\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1283
    :cond_54
    const-string v1, "scheme"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_55

    .line 1284
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'scheme\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1286
    :cond_55
    iget-wide v4, v0, Lio/realm/bm$a;->y:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_56

    .line 1287
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'scheme\' is required. Either set @Required to field \'scheme\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1289
    :cond_56
    const-string v1, "bacsDirectDebitInstructionId"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_57

    .line 1290
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'bacsDirectDebitInstructionId\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1292
    :cond_57
    const-string v1, "bacsDirectDebitInstructionId"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_58

    .line 1293
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'bacsDirectDebitInstructionId\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1295
    :cond_58
    iget-wide v4, v0, Lio/realm/bm$a;->z:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_59

    .line 1296
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'bacsDirectDebitInstructionId\' is required. Either set @Required to field \'bacsDirectDebitInstructionId\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1299
    :cond_59
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2309
    const-class v2, Lco/uk/getmondo/d/aj;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v13

    .line 2310
    invoke-virtual {v13}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    .line 2311
    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/d/aj;

    invoke-virtual {v4, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lio/realm/bm$a;

    .line 2312
    invoke-virtual {v13}, Lio/realm/internal/Table;->d()J

    move-result-wide v14

    .line 2314
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 2315
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Lco/uk/getmondo/d/aj;

    .line 2316
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2319
    instance-of v4, v12, Lio/realm/internal/l;

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v12

    .line 2320
    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v4, v12

    .line 2323
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->H()Ljava/lang/String;

    move-result-object v4

    .line 2325
    if-nez v4, :cond_4

    .line 2326
    invoke-static {v2, v3, v14, v15}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v6

    .line 2330
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 2331
    invoke-static {v13, v4}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v6

    .line 2333
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334
    iget-wide v4, v11, Lio/realm/bm$a;->b:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->I()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v4, v12

    .line 2335
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->J()Ljava/lang/String;

    move-result-object v8

    .line 2336
    if-eqz v8, :cond_5

    .line 2337
    iget-wide v4, v11, Lio/realm/bm$a;->c:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2341
    :goto_2
    iget-wide v4, v11, Lio/realm/bm$a;->d:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->K()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v4, v12

    .line 2342
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->L()Ljava/lang/String;

    move-result-object v8

    .line 2343
    if-eqz v8, :cond_6

    .line 2344
    iget-wide v4, v11, Lio/realm/bm$a;->e:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_3
    move-object v4, v12

    .line 2348
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->M()Ljava/util/Date;

    move-result-object v8

    .line 2349
    if-eqz v8, :cond_7

    .line 2350
    iget-wide v4, v11, Lio/realm/bm$a;->f:J

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :goto_4
    move-object v4, v12

    .line 2354
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->N()Ljava/lang/String;

    move-result-object v8

    .line 2355
    if-eqz v8, :cond_8

    .line 2356
    iget-wide v4, v11, Lio/realm/bm$a;->g:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_5
    move-object v4, v12

    .line 2360
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->O()Ljava/util/Date;

    move-result-object v8

    .line 2361
    if-eqz v8, :cond_9

    .line 2362
    iget-wide v4, v11, Lio/realm/bm$a;->h:J

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :goto_6
    move-object v4, v12

    .line 2366
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->P()Ljava/lang/String;

    move-result-object v8

    .line 2367
    if-eqz v8, :cond_a

    .line 2368
    iget-wide v4, v11, Lio/realm/bm$a;->i:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_7
    move-object v4, v12

    .line 2372
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->Q()Ljava/lang/String;

    move-result-object v8

    .line 2373
    if-eqz v8, :cond_b

    .line 2374
    iget-wide v4, v11, Lio/realm/bm$a;->j:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_8
    move-object v4, v12

    .line 2378
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->R()Ljava/lang/String;

    move-result-object v8

    .line 2379
    if-eqz v8, :cond_c

    .line 2380
    iget-wide v4, v11, Lio/realm/bm$a;->k:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_9
    move-object v4, v12

    .line 2384
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->S()Ljava/lang/String;

    move-result-object v8

    .line 2385
    if-eqz v8, :cond_d

    .line 2386
    iget-wide v4, v11, Lio/realm/bm$a;->l:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2390
    :goto_a
    iget-wide v4, v11, Lio/realm/bm$a;->m:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->T()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2391
    iget-wide v4, v11, Lio/realm/bm$a;->n:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->U()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v4, v12

    .line 2393
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->V()Lco/uk/getmondo/d/u;

    move-result-object v5

    .line 2394
    if-eqz v5, :cond_e

    .line 2395
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 2396
    if-nez v4, :cond_18

    .line 2397
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/ag;->b(Lio/realm/av;Lco/uk/getmondo/d/u;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 2399
    :goto_b
    iget-wide v4, v11, Lio/realm/bm$a;->o:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_c
    move-object v4, v12

    .line 2403
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->W()Ljava/lang/String;

    move-result-object v8

    .line 2404
    if-eqz v8, :cond_f

    .line 2405
    iget-wide v4, v11, Lio/realm/bm$a;->p:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2409
    :goto_d
    iget-wide v4, v11, Lio/realm/bm$a;->q:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->X()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2410
    iget-wide v4, v11, Lio/realm/bm$a;->r:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->Y()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2411
    iget-wide v4, v11, Lio/realm/bm$a;->s:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->Z()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2412
    iget-wide v4, v11, Lio/realm/bm$a;->t:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->aa()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2414
    iget-wide v4, v11, Lio/realm/bm$a;->u:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeGetLinkView(JJJ)J

    move-result-wide v8

    .line 2415
    invoke-static {v8, v9}, Lio/realm/internal/LinkView;->nativeClear(J)V

    move-object v4, v12

    .line 2416
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->ab()Lio/realm/az;

    move-result-object v4

    .line 2417
    if-eqz v4, :cond_10

    .line 2418
    invoke-virtual {v4}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/d/d;

    .line 2419
    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 2420
    if-nez v5, :cond_3

    .line 2421
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v4, v1}, Lio/realm/c;->b(Lio/realm/av;Lco/uk/getmondo/d/d;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 2423
    :cond_3
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v8, v9, v4, v5}, Lio/realm/internal/LinkView;->nativeAdd(JJ)V

    goto :goto_e

    .line 2328
    :cond_4
    invoke-static {v2, v3, v14, v15, v4}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v6

    goto/16 :goto_1

    .line 2339
    :cond_5
    iget-wide v4, v11, Lio/realm/bm$a;->c:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_2

    .line 2346
    :cond_6
    iget-wide v4, v11, Lio/realm/bm$a;->e:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_3

    .line 2352
    :cond_7
    iget-wide v4, v11, Lio/realm/bm$a;->f:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_4

    .line 2358
    :cond_8
    iget-wide v4, v11, Lio/realm/bm$a;->g:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_5

    .line 2364
    :cond_9
    iget-wide v4, v11, Lio/realm/bm$a;->h:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_6

    .line 2370
    :cond_a
    iget-wide v4, v11, Lio/realm/bm$a;->i:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_7

    .line 2376
    :cond_b
    iget-wide v4, v11, Lio/realm/bm$a;->j:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_8

    .line 2382
    :cond_c
    iget-wide v4, v11, Lio/realm/bm$a;->k:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_9

    .line 2388
    :cond_d
    iget-wide v4, v11, Lio/realm/bm$a;->l:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_a

    .line 2401
    :cond_e
    iget-wide v4, v11, Lio/realm/bm$a;->o:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_c

    .line 2407
    :cond_f
    iget-wide v4, v11, Lio/realm/bm$a;->p:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_d

    :cond_10
    move-object v4, v12

    .line 2428
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v5

    .line 2429
    if-eqz v5, :cond_11

    .line 2430
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 2431
    if-nez v4, :cond_17

    .line 2432
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/as;->b(Lio/realm/av;Lco/uk/getmondo/d/aa;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 2434
    :goto_f
    iget-wide v4, v11, Lio/realm/bm$a;->v:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_10
    move-object v4, v12

    .line 2439
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v5

    .line 2440
    if-eqz v5, :cond_12

    .line 2441
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 2442
    if-nez v4, :cond_16

    .line 2443
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/e;->b(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 2445
    :goto_11
    iget-wide v4, v11, Lio/realm/bm$a;->w:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 2449
    :goto_12
    iget-wide v4, v11, Lio/realm/bm$a;->x:J

    move-object v8, v12

    check-cast v8, Lio/realm/bn;

    invoke-interface {v8}, Lio/realm/bn;->ae()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v4, v12

    .line 2450
    check-cast v4, Lio/realm/bn;

    invoke-interface {v4}, Lio/realm/bn;->af()Ljava/lang/String;

    move-result-object v8

    .line 2451
    if-eqz v8, :cond_13

    .line 2452
    iget-wide v4, v11, Lio/realm/bm$a;->y:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2456
    :goto_13
    check-cast v12, Lio/realm/bn;

    invoke-interface {v12}, Lio/realm/bn;->ag()Ljava/lang/String;

    move-result-object v8

    .line 2457
    if-eqz v8, :cond_14

    .line 2458
    iget-wide v4, v11, Lio/realm/bm$a;->z:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 2436
    :cond_11
    iget-wide v4, v11, Lio/realm/bm$a;->v:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_10

    .line 2447
    :cond_12
    iget-wide v4, v11, Lio/realm/bm$a;->w:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_12

    .line 2454
    :cond_13
    iget-wide v4, v11, Lio/realm/bm$a;->y:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_13

    .line 2460
    :cond_14
    iget-wide v4, v11, Lio/realm/bm$a;->z:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_0

    .line 2463
    :cond_15
    return-void

    :cond_16
    move-object v8, v4

    goto :goto_11

    :cond_17
    move-object v8, v4

    goto :goto_f

    :cond_18
    move-object v8, v4

    goto/16 :goto_b
.end method

.method public static ah()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 1011
    sget-object v0, Lio/realm/bm;->c:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static ai()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1303
    const-string v0, "class_Transaction"

    return-object v0
.end method

.method private static aj()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 980
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "Transaction"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 981
    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 982
    const-string v7, "amountValue"

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 983
    const-string v7, "amountCurrency"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 984
    const-string v7, "amountLocalValue"

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 985
    const-string v7, "amountLocalCurrency"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 986
    const-string v7, "created"

    sget-object v8, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 987
    const-string v7, "createdDateFormatted"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 988
    const-string v7, "updated"

    sget-object v8, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 989
    const-string v7, "merchantDescription"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 990
    const-string v7, "description"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 991
    const-string v7, "declineReason"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 992
    const-string v7, "notes"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 993
    const-string v7, "hideAmount"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 994
    const-string v7, "settled"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 995
    const-string v1, "merchant"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "Merchant"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 996
    const-string v7, "category"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 997
    const-string v7, "fromAtm"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 998
    const-string v7, "peerToPeer"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 999
    const-string v7, "topUp"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 1000
    const-string v7, "includeInSpending"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 1001
    const-string v1, "attachments"

    sget-object v2, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    const-string v4, "Attachment"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 1002
    const-string v1, "peer"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "Peer"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 1003
    const-string v1, "bankDetails"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "BankDetails"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 1004
    const-string v7, "fromMonzoMe"

    sget-object v8, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 1005
    const-string v1, "scheme"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 1006
    const-string v1, "bacsDirectDebitInstructionId"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 1007
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/aj;Ljava/util/Map;)J
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/aj;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2159
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2160
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 2305
    :goto_0
    return-wide v4

    .line 2162
    :cond_0
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 2163
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 2164
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/d/aj;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/bm$a;

    .line 2165
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 2166
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->H()Ljava/lang/String;

    move-result-object v2

    .line 2168
    if-nez v2, :cond_3

    .line 2169
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 2173
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    .line 2174
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 2176
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2177
    iget-wide v2, v9, Lio/realm/bm$a;->b:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->I()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 2178
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->J()Ljava/lang/String;

    move-result-object v6

    .line 2179
    if-eqz v6, :cond_4

    .line 2180
    iget-wide v2, v9, Lio/realm/bm$a;->c:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2184
    :goto_2
    iget-wide v2, v9, Lio/realm/bm$a;->d:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->K()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 2185
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->L()Ljava/lang/String;

    move-result-object v6

    .line 2186
    if-eqz v6, :cond_5

    .line 2187
    iget-wide v2, v9, Lio/realm/bm$a;->e:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_3
    move-object v2, p1

    .line 2191
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->M()Ljava/util/Date;

    move-result-object v6

    .line 2192
    if-eqz v6, :cond_6

    .line 2193
    iget-wide v2, v9, Lio/realm/bm$a;->f:J

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :goto_4
    move-object v2, p1

    .line 2197
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->N()Ljava/lang/String;

    move-result-object v6

    .line 2198
    if-eqz v6, :cond_7

    .line 2199
    iget-wide v2, v9, Lio/realm/bm$a;->g:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_5
    move-object v2, p1

    .line 2203
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->O()Ljava/util/Date;

    move-result-object v6

    .line 2204
    if-eqz v6, :cond_8

    .line 2205
    iget-wide v2, v9, Lio/realm/bm$a;->h:J

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    :goto_6
    move-object v2, p1

    .line 2209
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->P()Ljava/lang/String;

    move-result-object v6

    .line 2210
    if-eqz v6, :cond_9

    .line 2211
    iget-wide v2, v9, Lio/realm/bm$a;->i:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_7
    move-object v2, p1

    .line 2215
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->Q()Ljava/lang/String;

    move-result-object v6

    .line 2216
    if-eqz v6, :cond_a

    .line 2217
    iget-wide v2, v9, Lio/realm/bm$a;->j:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_8
    move-object v2, p1

    .line 2221
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->R()Ljava/lang/String;

    move-result-object v6

    .line 2222
    if-eqz v6, :cond_b

    .line 2223
    iget-wide v2, v9, Lio/realm/bm$a;->k:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_9
    move-object v2, p1

    .line 2227
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->S()Ljava/lang/String;

    move-result-object v6

    .line 2228
    if-eqz v6, :cond_c

    .line 2229
    iget-wide v2, v9, Lio/realm/bm$a;->l:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2233
    :goto_a
    iget-wide v2, v9, Lio/realm/bm$a;->m:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->T()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2234
    iget-wide v2, v9, Lio/realm/bm$a;->n:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->U()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v2, p1

    .line 2236
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->V()Lco/uk/getmondo/d/u;

    move-result-object v3

    .line 2237
    if-eqz v3, :cond_d

    .line 2238
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 2239
    if-nez v2, :cond_16

    .line 2240
    invoke-static {p0, v3, p2}, Lio/realm/ag;->b(Lio/realm/av;Lco/uk/getmondo/d/u;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 2242
    :goto_b
    iget-wide v2, v9, Lio/realm/bm$a;->o:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_c
    move-object v2, p1

    .line 2246
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->W()Ljava/lang/String;

    move-result-object v6

    .line 2247
    if-eqz v6, :cond_e

    .line 2248
    iget-wide v2, v9, Lio/realm/bm$a;->p:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2252
    :goto_d
    iget-wide v2, v9, Lio/realm/bm$a;->q:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->X()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2253
    iget-wide v2, v9, Lio/realm/bm$a;->r:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->Y()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2254
    iget-wide v2, v9, Lio/realm/bm$a;->s:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->Z()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2255
    iget-wide v2, v9, Lio/realm/bm$a;->t:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->aa()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 2257
    iget-wide v2, v9, Lio/realm/bm$a;->u:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeGetLinkView(JJJ)J

    move-result-wide v6

    .line 2258
    invoke-static {v6, v7}, Lio/realm/internal/LinkView;->nativeClear(J)V

    move-object v2, p1

    .line 2259
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->ab()Lio/realm/az;

    move-result-object v2

    .line 2260
    if-eqz v2, :cond_f

    .line 2261
    invoke-virtual {v2}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/d;

    .line 2262
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 2263
    if-nez v3, :cond_2

    .line 2264
    invoke-static {p0, v2, p2}, Lio/realm/c;->b(Lio/realm/av;Lco/uk/getmondo/d/d;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2266
    :cond_2
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v6, v7, v2, v3}, Lio/realm/internal/LinkView;->nativeAdd(JJ)V

    goto :goto_e

    .line 2171
    :cond_3
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 2182
    :cond_4
    iget-wide v2, v9, Lio/realm/bm$a;->c:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_2

    .line 2189
    :cond_5
    iget-wide v2, v9, Lio/realm/bm$a;->e:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_3

    .line 2195
    :cond_6
    iget-wide v2, v9, Lio/realm/bm$a;->f:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_4

    .line 2201
    :cond_7
    iget-wide v2, v9, Lio/realm/bm$a;->g:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_5

    .line 2207
    :cond_8
    iget-wide v2, v9, Lio/realm/bm$a;->h:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_6

    .line 2213
    :cond_9
    iget-wide v2, v9, Lio/realm/bm$a;->i:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_7

    .line 2219
    :cond_a
    iget-wide v2, v9, Lio/realm/bm$a;->j:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_8

    .line 2225
    :cond_b
    iget-wide v2, v9, Lio/realm/bm$a;->k:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_9

    .line 2231
    :cond_c
    iget-wide v2, v9, Lio/realm/bm$a;->l:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_a

    .line 2244
    :cond_d
    iget-wide v2, v9, Lio/realm/bm$a;->o:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_c

    .line 2250
    :cond_e
    iget-wide v2, v9, Lio/realm/bm$a;->p:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_d

    :cond_f
    move-object v2, p1

    .line 2271
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v3

    .line 2272
    if-eqz v3, :cond_10

    .line 2273
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 2274
    if-nez v2, :cond_15

    .line 2275
    invoke-static {p0, v3, p2}, Lio/realm/as;->b(Lio/realm/av;Lco/uk/getmondo/d/aa;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 2277
    :goto_f
    iget-wide v2, v9, Lio/realm/bm$a;->v:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    :goto_10
    move-object v2, p1

    .line 2282
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v3

    .line 2283
    if-eqz v3, :cond_11

    .line 2284
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 2285
    if-nez v2, :cond_14

    .line 2286
    invoke-static {p0, v3, p2}, Lio/realm/e;->b(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 2288
    :goto_11
    iget-wide v2, v9, Lio/realm/bm$a;->w:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 2292
    :goto_12
    iget-wide v2, v9, Lio/realm/bm$a;->x:J

    move-object v6, p1

    check-cast v6, Lio/realm/bn;

    invoke-interface {v6}, Lio/realm/bn;->ae()Z

    move-result v6

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    move-object v2, p1

    .line 2293
    check-cast v2, Lio/realm/bn;

    invoke-interface {v2}, Lio/realm/bn;->af()Ljava/lang/String;

    move-result-object v6

    .line 2294
    if-eqz v6, :cond_12

    .line 2295
    iget-wide v2, v9, Lio/realm/bm$a;->y:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 2299
    :goto_13
    check-cast p1, Lio/realm/bn;

    invoke-interface {p1}, Lio/realm/bn;->ag()Ljava/lang/String;

    move-result-object v6

    .line 2300
    if-eqz v6, :cond_13

    .line 2301
    iget-wide v2, v9, Lio/realm/bm$a;->z:J

    move v7, v8

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 2279
    :cond_10
    iget-wide v2, v9, Lio/realm/bm$a;->v:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_10

    .line 2290
    :cond_11
    iget-wide v2, v9, Lio/realm/bm$a;->w:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_12

    .line 2297
    :cond_12
    iget-wide v2, v9, Lio/realm/bm$a;->y:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_13

    .line 2303
    :cond_13
    iget-wide v2, v9, Lio/realm/bm$a;->z:J

    move v6, v8

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_0

    :cond_14
    move-object v6, v2

    goto :goto_11

    :cond_15
    move-object v6, v2

    goto :goto_f

    :cond_16
    move-object v6, v2

    goto/16 :goto_b
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/d/aj;ZLjava/util/Map;)Lco/uk/getmondo/d/aj;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/d/aj;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/d/aj;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 1823
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 1824
    if-eqz v0, :cond_0

    .line 1825
    check-cast v0, Lco/uk/getmondo/d/aj;

    .line 1907
    :goto_0
    return-object v0

    .line 1829
    :cond_0
    const-class v1, Lco/uk/getmondo/d/aj;

    move-object v0, p1

    check-cast v0, Lio/realm/bn;

    invoke-interface {v0}, Lio/realm/bn;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v3, v2}, Lio/realm/av;->a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aj;

    move-object v1, v0

    .line 1830
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1832
    check-cast p1, Lio/realm/bn;

    move-object v1, v0

    .line 1833
    check-cast v1, Lio/realm/bn;

    .line 1835
    invoke-interface {p1}, Lio/realm/bn;->I()J

    move-result-wide v4

    invoke-interface {v1, v4, v5}, Lio/realm/bn;->a(J)V

    .line 1836
    invoke-interface {p1}, Lio/realm/bn;->J()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->d(Ljava/lang/String;)V

    .line 1837
    invoke-interface {p1}, Lio/realm/bn;->K()J

    move-result-wide v4

    invoke-interface {v1, v4, v5}, Lio/realm/bn;->b(J)V

    .line 1838
    invoke-interface {p1}, Lio/realm/bn;->L()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->e(Ljava/lang/String;)V

    .line 1839
    invoke-interface {p1}, Lio/realm/bn;->M()Ljava/util/Date;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->a(Ljava/util/Date;)V

    .line 1840
    invoke-interface {p1}, Lio/realm/bn;->N()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->f(Ljava/lang/String;)V

    .line 1841
    invoke-interface {p1}, Lio/realm/bn;->O()Ljava/util/Date;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->b(Ljava/util/Date;)V

    .line 1842
    invoke-interface {p1}, Lio/realm/bn;->P()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->g(Ljava/lang/String;)V

    .line 1843
    invoke-interface {p1}, Lio/realm/bn;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->h(Ljava/lang/String;)V

    .line 1844
    invoke-interface {p1}, Lio/realm/bn;->R()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->i(Ljava/lang/String;)V

    .line 1845
    invoke-interface {p1}, Lio/realm/bn;->S()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->j(Ljava/lang/String;)V

    .line 1846
    invoke-interface {p1}, Lio/realm/bn;->T()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/bn;->a(Z)V

    .line 1847
    invoke-interface {p1}, Lio/realm/bn;->U()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/bn;->b(Z)V

    .line 1849
    invoke-interface {p1}, Lio/realm/bn;->V()Lco/uk/getmondo/d/u;

    move-result-object v4

    .line 1850
    if-nez v4, :cond_1

    .line 1851
    invoke-interface {v1, v7}, Lio/realm/bn;->a(Lco/uk/getmondo/d/u;)V

    .line 1860
    :goto_1
    invoke-interface {p1}, Lio/realm/bn;->W()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->k(Ljava/lang/String;)V

    .line 1861
    invoke-interface {p1}, Lio/realm/bn;->X()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/bn;->c(Z)V

    .line 1862
    invoke-interface {p1}, Lio/realm/bn;->Y()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/bn;->d(Z)V

    .line 1863
    invoke-interface {p1}, Lio/realm/bn;->Z()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/bn;->e(Z)V

    .line 1864
    invoke-interface {p1}, Lio/realm/bn;->aa()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/bn;->f(Z)V

    .line 1866
    invoke-interface {p1}, Lio/realm/bn;->ab()Lio/realm/az;

    move-result-object v5

    .line 1867
    if-eqz v5, :cond_4

    .line 1868
    invoke-interface {v1}, Lio/realm/bn;->ab()Lio/realm/az;

    move-result-object v6

    move v4, v3

    .line 1869
    :goto_2
    invoke-virtual {v5}, Lio/realm/az;->size()I

    move-result v2

    if-ge v4, v2, :cond_4

    .line 1870
    invoke-virtual {v5, v4}, Lio/realm/az;->b(I)Lio/realm/bb;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/d;

    .line 1871
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/d/d;

    .line 1872
    if-eqz v3, :cond_3

    .line 1873
    invoke-virtual {v6, v3}, Lio/realm/az;->a(Lio/realm/bb;)Z

    .line 1869
    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 1853
    :cond_1
    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/u;

    .line 1854
    if-eqz v2, :cond_2

    .line 1855
    invoke-interface {v1, v2}, Lio/realm/bn;->a(Lco/uk/getmondo/d/u;)V

    goto :goto_1

    .line 1857
    :cond_2
    invoke-static {p0, v4, p2, p3}, Lio/realm/ag;->a(Lio/realm/av;Lco/uk/getmondo/d/u;ZLjava/util/Map;)Lco/uk/getmondo/d/u;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->a(Lco/uk/getmondo/d/u;)V

    goto :goto_1

    .line 1875
    :cond_3
    invoke-static {p0, v2, p2, p3}, Lio/realm/c;->a(Lio/realm/av;Lco/uk/getmondo/d/d;ZLjava/util/Map;)Lco/uk/getmondo/d/d;

    move-result-object v2

    invoke-virtual {v6, v2}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_3

    .line 1881
    :cond_4
    invoke-interface {p1}, Lio/realm/bn;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v3

    .line 1882
    if-nez v3, :cond_5

    .line 1883
    invoke-interface {v1, v7}, Lio/realm/bn;->a(Lco/uk/getmondo/d/aa;)V

    .line 1893
    :goto_4
    invoke-interface {p1}, Lio/realm/bn;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v3

    .line 1894
    if-nez v3, :cond_7

    .line 1895
    invoke-interface {v1, v7}, Lio/realm/bn;->a(Lco/uk/getmondo/payments/send/data/a/a;)V

    .line 1904
    :goto_5
    invoke-interface {p1}, Lio/realm/bn;->ae()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/bn;->g(Z)V

    .line 1905
    invoke-interface {p1}, Lio/realm/bn;->af()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->l(Ljava/lang/String;)V

    .line 1906
    invoke-interface {p1}, Lio/realm/bn;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->m(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1885
    :cond_5
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/aa;

    .line 1886
    if-eqz v2, :cond_6

    .line 1887
    invoke-interface {v1, v2}, Lio/realm/bn;->a(Lco/uk/getmondo/d/aa;)V

    goto :goto_4

    .line 1889
    :cond_6
    invoke-static {p0, v3, p2, p3}, Lio/realm/as;->a(Lio/realm/av;Lco/uk/getmondo/d/aa;ZLjava/util/Map;)Lco/uk/getmondo/d/aa;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->a(Lco/uk/getmondo/d/aa;)V

    goto :goto_4

    .line 1897
    :cond_7
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/payments/send/data/a/a;

    .line 1898
    if-eqz v2, :cond_8

    .line 1899
    invoke-interface {v1, v2}, Lio/realm/bn;->a(Lco/uk/getmondo/payments/send/data/a/a;)V

    goto :goto_5

    .line 1901
    :cond_8
    invoke-static {p0, v3, p2, p3}, Lio/realm/e;->a(Lio/realm/av;Lco/uk/getmondo/payments/send/data/a/a;ZLjava/util/Map;)Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/bn;->a(Lco/uk/getmondo/payments/send/data/a/a;)V

    goto :goto_5
.end method


# virtual methods
.method public H()Ljava/lang/String;
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 195
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I()J
    .locals 4

    .prologue
    .line 212
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 213
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public J()Ljava/lang/String;
    .locals 4

    .prologue
    .line 234
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 235
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public K()J
    .locals 4

    .prologue
    .line 264
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 265
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public L()Ljava/lang/String;
    .locals 4

    .prologue
    .line 286
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 287
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public M()Ljava/util/Date;
    .locals 4

    .prologue
    .line 316
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 317
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->b(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    const/4 v0, 0x0

    .line 320
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->j(J)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public N()Ljava/lang/String;
    .locals 4

    .prologue
    .line 349
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 350
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->g:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public O()Ljava/util/Date;
    .locals 4

    .prologue
    .line 379
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 380
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->b(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    const/4 v0, 0x0

    .line 383
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->j(J)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public P()Ljava/lang/String;
    .locals 4

    .prologue
    .line 412
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 413
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->i:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public Q()Ljava/lang/String;
    .locals 4

    .prologue
    .line 442
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 443
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->j:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public R()Ljava/lang/String;
    .locals 4

    .prologue
    .line 472
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 473
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->k:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public S()Ljava/lang/String;
    .locals 4

    .prologue
    .line 502
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 503
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->l:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public T()Z
    .locals 4

    .prologue
    .line 532
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 533
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->m:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public U()Z
    .locals 4

    .prologue
    .line 554
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 555
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->n:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public V()Lco/uk/getmondo/d/u;
    .locals 6

    .prologue
    .line 575
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 576
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->o:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    const/4 v0, 0x0

    .line 579
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/u;

    iget-object v2, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v4, v3, Lio/realm/bm$a;->o:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/u;

    goto :goto_0
.end method

.method public W()Ljava/lang/String;
    .locals 4

    .prologue
    .line 627
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 628
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->p:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public X()Z
    .locals 4

    .prologue
    .line 657
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 658
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->q:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public Y()Z
    .locals 4

    .prologue
    .line 679
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 680
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->r:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 4

    .prologue
    .line 701
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 702
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->s:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public a(J)V
    .locals 9

    .prologue
    .line 218
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 223
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->b:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->a(JJJZ)V

    goto :goto_0

    .line 227
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 228
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->b:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/n;->a(JJ)V

    goto :goto_0
.end method

.method public a(Lco/uk/getmondo/d/aa;)V
    .locals 9

    .prologue
    .line 806
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 807
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 844
    :cond_0
    :goto_0
    return-void

    .line 810
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "peer"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 813
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 814
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aa;

    move-object v6, v0

    .line 816
    :goto_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 817
    if-nez v6, :cond_2

    .line 819
    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v0, v0, Lio/realm/bm$a;->v:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 822
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 823
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 825
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 826
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 828
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->v:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 832
    :cond_5
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 833
    if-nez p1, :cond_6

    .line 834
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->v:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 837
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 838
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 840
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 841
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 843
    :cond_9
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->v:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Lco/uk/getmondo/d/u;)V
    .locals 9

    .prologue
    .line 584
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 585
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 622
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "merchant"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 591
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 592
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/u;

    move-object v6, v0

    .line 594
    :goto_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 595
    if-nez v6, :cond_2

    .line 597
    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v0, v0, Lio/realm/bm$a;->o:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 600
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 601
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 603
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 604
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 606
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->o:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 610
    :cond_5
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 611
    if-nez p1, :cond_6

    .line 612
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->o:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 615
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 616
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 618
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 619
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 621
    :cond_9
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->o:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/a;)V
    .locals 9

    .prologue
    .line 857
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 858
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 861
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "bankDetails"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 864
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 865
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/a;

    move-object v6, v0

    .line 867
    :goto_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 868
    if-nez v6, :cond_2

    .line 870
    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v0, v0, Lio/realm/bm$a;->w:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 873
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 874
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 876
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 877
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 879
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->w:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 883
    :cond_5
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 884
    if-nez p1, :cond_6

    .line 885
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->w:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 888
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 889
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 891
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 892
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 894
    :cond_9
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->w:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Lio/realm/az;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 757
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 758
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 793
    :cond_0
    return-void

    .line 761
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "attachments"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 764
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lio/realm/az;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 765
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    .line 767
    new-instance v2, Lio/realm/az;

    invoke-direct {v2}, Lio/realm/az;-><init>()V

    .line 768
    invoke-virtual {p1}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/d;

    .line 769
    if-eqz v1, :cond_2

    invoke-static {v1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 770
    :cond_2
    invoke-virtual {v2, v1}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_0

    .line 772
    :cond_3
    invoke-virtual {v0, v1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v1

    invoke-virtual {v2, v1}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_0

    :cond_4
    move-object p1, v2

    .line 778
    :cond_5
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 779
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->u:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->n(J)Lio/realm/internal/LinkView;

    move-result-object v2

    .line 780
    invoke-virtual {v2}, Lio/realm/internal/LinkView;->a()V

    .line 781
    if-eqz p1, :cond_0

    .line 784
    invoke-virtual {p1}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/bb;

    .line 785
    invoke-static {v0}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 786
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Each element of \'value\' must be a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move-object v1, v0

    .line 788
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {v1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    iget-object v4, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eq v1, v4, :cond_8

    .line 789
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Each element of \'value\' must belong to the same Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 791
    :cond_8
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/LinkView;->b(J)V

    goto :goto_1
.end method

.method public a(Ljava/util/Date;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 325
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    :goto_0
    return-void

    .line 329
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 330
    if-nez p1, :cond_1

    .line 331
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->f:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 334
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->f:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/util/Date;Z)V

    goto :goto_0

    .line 338
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 339
    if-nez p1, :cond_3

    .line 340
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 343
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->f:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/util/Date;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 8

    .prologue
    .line 538
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 549
    :goto_0
    return-void

    .line 542
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 543
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->m:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 547
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 548
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->m:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public aa()Z
    .locals 4

    .prologue
    .line 723
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 724
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->t:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public ab()Lio/realm/az;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 744
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 746
    iget-object v0, p0, Lio/realm/bm;->d:Lio/realm/az;

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lio/realm/bm;->d:Lio/realm/az;

    .line 751
    :goto_0
    return-object v0

    .line 749
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->u:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->n(J)Lio/realm/internal/LinkView;

    move-result-object v0

    .line 750
    new-instance v1, Lio/realm/az;

    const-class v2, Lco/uk/getmondo/d/d;

    iget-object v3, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v3}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lio/realm/az;-><init>(Ljava/lang/Class;Lio/realm/internal/LinkView;Lio/realm/g;)V

    iput-object v1, p0, Lio/realm/bm;->d:Lio/realm/az;

    .line 751
    iget-object v0, p0, Lio/realm/bm;->d:Lio/realm/az;

    goto :goto_0
.end method

.method public ac()Lco/uk/getmondo/d/aa;
    .locals 6

    .prologue
    .line 797
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 798
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->v:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799
    const/4 v0, 0x0

    .line 801
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/aa;

    iget-object v2, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v4, v3, Lio/realm/bm$a;->v:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aa;

    goto :goto_0
.end method

.method public ad()Lco/uk/getmondo/payments/send/data/a/a;
    .locals 6

    .prologue
    .line 848
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 849
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->w:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 850
    const/4 v0, 0x0

    .line 852
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/payments/send/data/a/a;

    iget-object v2, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v4, v3, Lio/realm/bm$a;->w:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/a;

    goto :goto_0
.end method

.method public ae()Z
    .locals 4

    .prologue
    .line 900
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 901
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->x:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->g(J)Z

    move-result v0

    return v0
.end method

.method public af()Ljava/lang/String;
    .locals 4

    .prologue
    .line 922
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 923
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->y:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ag()Ljava/lang/String;
    .locals 4

    .prologue
    .line 952
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 953
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->z:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)V
    .locals 9

    .prologue
    .line 270
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    :goto_0
    return-void

    .line 274
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 275
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->d:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->a(JJJZ)V

    goto :goto_0

    .line 279
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 280
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->d:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/n;->a(JJ)V

    goto :goto_0
.end method

.method public b(Ljava/util/Date;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 388
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 389
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    :goto_0
    return-void

    .line 392
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 393
    if-nez p1, :cond_1

    .line 394
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->h:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 397
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->h:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/util/Date;Z)V

    goto :goto_0

    .line 401
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 402
    if-nez p1, :cond_3

    .line 403
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 406
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->h:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/util/Date;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 8

    .prologue
    .line 560
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 561
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 571
    :goto_0
    return-void

    .line 564
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 565
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->n:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 569
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 570
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->n:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 206
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(Z)V
    .locals 8

    .prologue
    .line 663
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 664
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 674
    :goto_0
    return-void

    .line 667
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 668
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->q:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 672
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 673
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->q:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 240
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 245
    if-nez p1, :cond_1

    .line 246
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 249
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 253
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 254
    if-nez p1, :cond_3

    .line 255
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 258
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->c:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public d(Z)V
    .locals 8

    .prologue
    .line 685
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 686
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 696
    :goto_0
    return-void

    .line 689
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 690
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->r:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 694
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 695
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->r:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 292
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 297
    if-nez p1, :cond_1

    .line 298
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->e:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 301
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->e:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 305
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 306
    if-nez p1, :cond_3

    .line 307
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 310
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->e:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public e(Z)V
    .locals 8

    .prologue
    .line 707
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 708
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    :goto_0
    return-void

    .line 711
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 712
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->s:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 716
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 717
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->s:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 355
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 374
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 360
    if-nez p1, :cond_1

    .line 361
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->g:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 364
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->g:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 368
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 369
    if-nez p1, :cond_3

    .line 370
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->g:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 373
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->g:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public f(Z)V
    .locals 8

    .prologue
    .line 729
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 730
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 740
    :goto_0
    return-void

    .line 733
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 734
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->t:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 738
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 739
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->t:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public g(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 418
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 419
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    :goto_0
    return-void

    .line 422
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 423
    if-nez p1, :cond_1

    .line 424
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->i:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 427
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->i:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 431
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 432
    if-nez p1, :cond_3

    .line 433
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->i:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 436
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->i:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public g(Z)V
    .locals 8

    .prologue
    .line 906
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 907
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 917
    :goto_0
    return-void

    .line 910
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 911
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v2, Lio/realm/bm$a;->x:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v7, 0x1

    move v6, p1

    invoke-virtual/range {v1 .. v7}, Lio/realm/internal/Table;->a(JJZZ)V

    goto :goto_0

    .line 915
    :cond_1
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 916
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->x:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JZ)V

    goto :goto_0
.end method

.method public h(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 448
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 449
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 467
    :goto_0
    return-void

    .line 452
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 453
    if-nez p1, :cond_1

    .line 454
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->j:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 457
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->j:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 461
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 462
    if-nez p1, :cond_3

    .line 463
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->j:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 466
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->j:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public i(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 478
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 479
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    :goto_0
    return-void

    .line 482
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 483
    if-nez p1, :cond_1

    .line 484
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->k:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 487
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->k:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 491
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 492
    if-nez p1, :cond_3

    .line 493
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->k:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 496
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->k:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public j(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 508
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 509
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 527
    :goto_0
    return-void

    .line 512
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 513
    if-nez p1, :cond_1

    .line 514
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->l:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 517
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->l:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 521
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 522
    if-nez p1, :cond_3

    .line 523
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->l:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 526
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->l:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public k(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 633
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 634
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 652
    :goto_0
    return-void

    .line 637
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 638
    if-nez p1, :cond_1

    .line 639
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->p:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 642
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->p:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 646
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 647
    if-nez p1, :cond_3

    .line 648
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->p:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 651
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->p:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public l(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 928
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 929
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 947
    :goto_0
    return-void

    .line 932
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 933
    if-nez p1, :cond_1

    .line 934
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->y:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 937
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->y:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 941
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 942
    if-nez p1, :cond_3

    .line 943
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->y:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 946
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->y:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public m(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 958
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 959
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 977
    :goto_0
    return-void

    .line 962
    :cond_0
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 963
    if-nez p1, :cond_1

    .line 964
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v0, Lio/realm/bm$a;->z:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 967
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v1, v1, Lio/realm/bm$a;->z:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 971
    :cond_2
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 972
    if-nez p1, :cond_3

    .line 973
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->z:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 976
    :cond_3
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    iget-wide v2, v1, Lio/realm/bm$a;->z:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2723
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2610
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2611
    const-string v0, "Invalid object"

    .line 2718
    :goto_0
    return-object v0

    .line 2613
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Transaction = proxy["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2614
    const-string v0, "{id:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2615
    invoke-virtual {p0}, Lio/realm/bm;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lio/realm/bm;->H()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2617
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2618
    const-string v0, "{amountValue:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2619
    invoke-virtual {p0}, Lio/realm/bm;->I()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2620
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2621
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2622
    const-string v0, "{amountCurrency:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2623
    invoke-virtual {p0}, Lio/realm/bm;->J()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lio/realm/bm;->J()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2624
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2625
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2626
    const-string v0, "{amountLocalValue:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2627
    invoke-virtual {p0}, Lio/realm/bm;->K()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2628
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2629
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2630
    const-string v0, "{amountLocalCurrency:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2631
    invoke-virtual {p0}, Lio/realm/bm;->L()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lio/realm/bm;->L()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2632
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2633
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2634
    const-string v0, "{created:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2635
    invoke-virtual {p0}, Lio/realm/bm;->M()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lio/realm/bm;->M()Ljava/util/Date;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2636
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2637
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2638
    const-string v0, "{createdDateFormatted:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2639
    invoke-virtual {p0}, Lio/realm/bm;->N()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lio/realm/bm;->N()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2640
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2641
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2642
    const-string v0, "{updated:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2643
    invoke-virtual {p0}, Lio/realm/bm;->O()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lio/realm/bm;->O()Ljava/util/Date;

    move-result-object v0

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2644
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2645
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2646
    const-string v0, "{merchantDescription:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2647
    invoke-virtual {p0}, Lio/realm/bm;->P()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lio/realm/bm;->P()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2648
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2649
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2650
    const-string v0, "{description:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2651
    invoke-virtual {p0}, Lio/realm/bm;->Q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lio/realm/bm;->Q()Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2652
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2653
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2654
    const-string v0, "{declineReason:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2655
    invoke-virtual {p0}, Lio/realm/bm;->R()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lio/realm/bm;->R()Ljava/lang/String;

    move-result-object v0

    :goto_9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2656
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2657
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2658
    const-string v0, "{notes:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2659
    invoke-virtual {p0}, Lio/realm/bm;->S()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lio/realm/bm;->S()Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2660
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2661
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2662
    const-string v0, "{hideAmount:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2663
    invoke-virtual {p0}, Lio/realm/bm;->T()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2664
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2665
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2666
    const-string v0, "{settled:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2667
    invoke-virtual {p0}, Lio/realm/bm;->U()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2668
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2669
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2670
    const-string v0, "{merchant:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2671
    invoke-virtual {p0}, Lio/realm/bm;->V()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v0, "Merchant"

    :goto_b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2672
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2673
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2674
    const-string v0, "{category:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2675
    invoke-virtual {p0}, Lio/realm/bm;->W()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lio/realm/bm;->W()Ljava/lang/String;

    move-result-object v0

    :goto_c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2676
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2677
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2678
    const-string v0, "{fromAtm:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2679
    invoke-virtual {p0}, Lio/realm/bm;->X()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2680
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2681
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2682
    const-string v0, "{peerToPeer:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2683
    invoke-virtual {p0}, Lio/realm/bm;->Y()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2684
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2685
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2686
    const-string v0, "{topUp:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2687
    invoke-virtual {p0}, Lio/realm/bm;->Z()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2688
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2689
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2690
    const-string v0, "{includeInSpending:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2691
    invoke-virtual {p0}, Lio/realm/bm;->aa()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2692
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2693
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2694
    const-string v0, "{attachments:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2695
    const-string v0, "RealmList<Attachment>["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/bm;->ab()Lio/realm/az;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/az;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2696
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2697
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2698
    const-string v0, "{peer:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2699
    invoke-virtual {p0}, Lio/realm/bm;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v0

    if-eqz v0, :cond_d

    const-string v0, "Peer"

    :goto_d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2700
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2701
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2702
    const-string v0, "{bankDetails:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2703
    invoke-virtual {p0}, Lio/realm/bm;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_e

    const-string v0, "BankDetails"

    :goto_e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2704
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2705
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2706
    const-string v0, "{fromMonzoMe:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2707
    invoke-virtual {p0}, Lio/realm/bm;->ae()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2708
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2709
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2710
    const-string v0, "{scheme:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2711
    invoke-virtual {p0}, Lio/realm/bm;->af()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lio/realm/bm;->af()Ljava/lang/String;

    move-result-object v0

    :goto_f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2712
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2713
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2714
    const-string v0, "{bacsDirectDebitInstructionId:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2715
    invoke-virtual {p0}, Lio/realm/bm;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lio/realm/bm;->ag()Ljava/lang/String;

    move-result-object v0

    :goto_10
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2716
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2717
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2718
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2615
    :cond_1
    const-string v0, "null"

    goto/16 :goto_1

    .line 2623
    :cond_2
    const-string v0, "null"

    goto/16 :goto_2

    .line 2631
    :cond_3
    const-string v0, "null"

    goto/16 :goto_3

    .line 2635
    :cond_4
    const-string v0, "null"

    goto/16 :goto_4

    .line 2639
    :cond_5
    const-string v0, "null"

    goto/16 :goto_5

    .line 2643
    :cond_6
    const-string v0, "null"

    goto/16 :goto_6

    .line 2647
    :cond_7
    const-string v0, "null"

    goto/16 :goto_7

    .line 2651
    :cond_8
    const-string v0, "null"

    goto/16 :goto_8

    .line 2655
    :cond_9
    const-string v0, "null"

    goto/16 :goto_9

    .line 2659
    :cond_a
    const-string v0, "null"

    goto/16 :goto_a

    .line 2671
    :cond_b
    const-string v0, "null"

    goto/16 :goto_b

    .line 2675
    :cond_c
    const-string v0, "null"

    goto/16 :goto_c

    .line 2699
    :cond_d
    const-string v0, "null"

    goto/16 :goto_d

    .line 2703
    :cond_e
    const-string v0, "null"

    goto/16 :goto_e

    .line 2711
    :cond_f
    const-string v0, "null"

    goto :goto_f

    .line 2715
    :cond_10
    const-string v0, "null"

    goto :goto_10
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lio/realm/bm;->b:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 189
    :goto_0
    return-void

    .line 182
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 183
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/bm$a;

    iput-object v1, p0, Lio/realm/bm;->a:Lio/realm/bm$a;

    .line 184
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    .line 185
    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 186
    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 187
    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 188
    iget-object v1, p0, Lio/realm/bm;->b:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method
