.class public Lio/realm/aq;
.super Lco/uk/getmondo/payments/a/a/e;
.source "PaymentSeriesRealmProxy.java"

# interfaces
.implements Lio/realm/ar;
.implements Lio/realm/internal/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/aq$a;
    }
.end annotation


# static fields
.field private static final d:Lio/realm/internal/OsObjectSchemaInfo;

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lio/realm/aq$a;

.field private c:Lio/realm/au;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/au",
            "<",
            "Lco/uk/getmondo/payments/a/a/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 91
    invoke-static {}, Lio/realm/aq;->r()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    sput-object v0, Lio/realm/aq;->d:Lio/realm/internal/OsObjectSchemaInfo;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 95
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    const-string v1, "accountId"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    const-string v1, "scheme"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    const-string v1, "schemaData"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    const-string v1, "_amount"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    const-string v1, "_currency"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    const-string v1, "_startDate"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    const-string v1, "_nextIterationDate"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    const-string v1, "_endDate"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v1, "_intervalType"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/aq;->e:Ljava/util/List;

    .line 106
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Lco/uk/getmondo/payments/a/a/e;-><init>()V

    .line 109
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->f()V

    .line 110
    return-void
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/e;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 867
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 868
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 925
    :cond_0
    :goto_0
    return-wide v4

    .line 870
    :cond_1
    const-class v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 871
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 872
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/aq$a;

    .line 873
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 874
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->f()Ljava/lang/String;

    move-result-object v2

    .line 876
    if-nez v2, :cond_9

    .line 877
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 881
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_a

    .line 882
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 886
    :goto_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 887
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->g()Ljava/lang/String;

    move-result-object v6

    .line 888
    if-eqz v6, :cond_2

    .line 889
    iget-wide v2, v9, Lio/realm/aq$a;->b:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_2
    move-object v2, p1

    .line 891
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->h()Ljava/lang/String;

    move-result-object v6

    .line 892
    if-eqz v6, :cond_3

    .line 893
    iget-wide v2, v9, Lio/realm/aq$a;->c:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_3
    move-object v2, p1

    .line 896
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v3

    .line 897
    if-eqz v3, :cond_4

    .line 898
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 899
    if-nez v2, :cond_b

    .line 900
    invoke-static {p0, v3, p2}, Lio/realm/w;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 902
    :goto_3
    iget-wide v2, v9, Lio/realm/aq$a;->d:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 904
    :cond_4
    iget-wide v2, v9, Lio/realm/aq$a;->e:J

    move-object v6, p1

    check-cast v6, Lio/realm/ar;

    invoke-interface {v6}, Lio/realm/ar;->j()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 905
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->k()Ljava/lang/String;

    move-result-object v6

    .line 906
    if-eqz v6, :cond_5

    .line 907
    iget-wide v2, v9, Lio/realm/aq$a;->f:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_5
    move-object v2, p1

    .line 909
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->l()Ljava/lang/String;

    move-result-object v6

    .line 910
    if-eqz v6, :cond_6

    .line 911
    iget-wide v2, v9, Lio/realm/aq$a;->g:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_6
    move-object v2, p1

    .line 913
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->m()Ljava/lang/String;

    move-result-object v6

    .line 914
    if-eqz v6, :cond_7

    .line 915
    iget-wide v2, v9, Lio/realm/aq$a;->h:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :cond_7
    move-object v2, p1

    .line 917
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->n()Ljava/lang/String;

    move-result-object v6

    .line 918
    if-eqz v6, :cond_8

    .line 919
    iget-wide v2, v9, Lio/realm/aq$a;->i:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 921
    :cond_8
    check-cast p1, Lio/realm/ar;

    invoke-interface {p1}, Lio/realm/ar;->o()Ljava/lang/String;

    move-result-object v6

    .line 922
    if-eqz v6, :cond_0

    .line 923
    iget-wide v2, v9, Lio/realm/aq$a;->j:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 879
    :cond_9
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 884
    :cond_a
    invoke-static {v2}, Lio/realm/internal/Table;->a(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_b
    move-object v6, v2

    goto :goto_3
.end method

.method public static a(Lco/uk/getmondo/payments/a/a/e;IILjava/util/Map;)Lco/uk/getmondo/payments/a/a/e;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/payments/a/a/e;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l$a",
            "<",
            "Lio/realm/bb;",
            ">;>;)",
            "Lco/uk/getmondo/payments/a/a/e;"
        }
    .end annotation

    .prologue
    .line 1157
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 1158
    :cond_0
    const/4 v0, 0x0

    .line 1187
    :goto_0
    return-object v0

    .line 1160
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l$a;

    .line 1162
    if-nez v0, :cond_2

    .line 1163
    new-instance v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-direct {v1}, Lco/uk/getmondo/payments/a/a/e;-><init>()V

    .line 1164
    new-instance v0, Lio/realm/internal/l$a;

    invoke-direct {v0, p1, v1}, Lio/realm/internal/l$a;-><init>(ILio/realm/bb;)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    move-object v0, v1

    .line 1173
    check-cast v0, Lio/realm/ar;

    .line 1174
    check-cast p0, Lio/realm/ar;

    .line 1175
    invoke-interface {p0}, Lio/realm/ar;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->a(Ljava/lang/String;)V

    .line 1176
    invoke-interface {p0}, Lio/realm/ar;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->b(Ljava/lang/String;)V

    .line 1177
    invoke-interface {p0}, Lio/realm/ar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->c(Ljava/lang/String;)V

    .line 1180
    invoke-interface {p0}, Lio/realm/ar;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v2, v3, p2, p3}, Lio/realm/w;->a(Lco/uk/getmondo/payments/a/a/b;IILjava/util/Map;)Lco/uk/getmondo/payments/a/a/b;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->a(Lco/uk/getmondo/payments/a/a/b;)V

    .line 1181
    invoke-interface {p0}, Lio/realm/ar;->j()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/ar;->a(J)V

    .line 1182
    invoke-interface {p0}, Lio/realm/ar;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->d(Ljava/lang/String;)V

    .line 1183
    invoke-interface {p0}, Lio/realm/ar;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->e(Ljava/lang/String;)V

    .line 1184
    invoke-interface {p0}, Lio/realm/ar;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->f(Ljava/lang/String;)V

    .line 1185
    invoke-interface {p0}, Lio/realm/ar;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->g(Ljava/lang/String;)V

    .line 1186
    invoke-interface {p0}, Lio/realm/ar;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lio/realm/ar;->h(Ljava/lang/String;)V

    move-object v0, v1

    .line 1187
    goto :goto_0

    .line 1167
    :cond_2
    iget v1, v0, Lio/realm/internal/l$a;->a:I

    if-lt p1, v1, :cond_3

    .line 1168
    iget-object v0, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    goto :goto_0

    .line 1170
    :cond_3
    iget-object v1, v0, Lio/realm/internal/l$a;->b:Lio/realm/bb;

    check-cast v1, Lco/uk/getmondo/payments/a/a/e;

    .line 1171
    iput p1, v0, Lio/realm/internal/l$a;->a:I

    goto :goto_1
.end method

.method static a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;Lco/uk/getmondo/payments/a/a/e;Ljava/util/Map;)Lco/uk/getmondo/payments/a/a/e;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/e;",
            "Lco/uk/getmondo/payments/a/a/e;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/payments/a/a/e;"
        }
    .end annotation

    .prologue
    .line 1191
    move-object v0, p1

    check-cast v0, Lio/realm/ar;

    .line 1192
    check-cast p2, Lio/realm/ar;

    .line 1193
    invoke-interface {p2}, Lio/realm/ar;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/ar;->b(Ljava/lang/String;)V

    .line 1194
    invoke-interface {p2}, Lio/realm/ar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/ar;->c(Ljava/lang/String;)V

    .line 1195
    invoke-interface {p2}, Lio/realm/ar;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v2

    .line 1196
    if-nez v2, :cond_0

    .line 1197
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lio/realm/ar;->a(Lco/uk/getmondo/payments/a/a/b;)V

    .line 1206
    :goto_0
    invoke-interface {p2}, Lio/realm/ar;->j()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lio/realm/ar;->a(J)V

    .line 1207
    invoke-interface {p2}, Lio/realm/ar;->k()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/ar;->d(Ljava/lang/String;)V

    .line 1208
    invoke-interface {p2}, Lio/realm/ar;->l()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/ar;->e(Ljava/lang/String;)V

    .line 1209
    invoke-interface {p2}, Lio/realm/ar;->m()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/ar;->f(Ljava/lang/String;)V

    .line 1210
    invoke-interface {p2}, Lio/realm/ar;->n()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/ar;->g(Ljava/lang/String;)V

    .line 1211
    invoke-interface {p2}, Lio/realm/ar;->o()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/ar;->h(Ljava/lang/String;)V

    .line 1212
    return-object p1

    .line 1199
    :cond_0
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/payments/a/a/b;

    .line 1200
    if-eqz v1, :cond_1

    .line 1201
    invoke-interface {v0, v1}, Lio/realm/ar;->a(Lco/uk/getmondo/payments/a/a/b;)V

    goto :goto_0

    .line 1203
    :cond_1
    const/4 v1, 0x1

    invoke-static {p0, v2, v1, p3}, Lio/realm/w;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/ar;->a(Lco/uk/getmondo/payments/a/a/b;)V

    goto :goto_0
.end method

.method public static a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/e;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/e;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/payments/a/a/e;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 786
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    iget-wide v2, v2, Lio/realm/g;->c:J

    iget-wide v6, p0, Lio/realm/av;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 787
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 789
    :cond_0
    instance-of v2, p1, Lio/realm/internal/l;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 826
    :goto_0
    return-object p1

    .line 792
    :cond_1
    sget-object v2, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v2}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/g$b;

    .line 793
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/internal/l;

    .line 794
    if-eqz v3, :cond_2

    .line 795
    check-cast v3, Lco/uk/getmondo/payments/a/a/e;

    move-object p1, v3

    goto :goto_0

    .line 798
    :cond_2
    const/4 v5, 0x0

    .line 800
    if-eqz p2, :cond_6

    .line 801
    const-class v3, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p0, v3}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 802
    invoke-virtual {v8}, Lio/realm/internal/Table;->d()J

    move-result-wide v6

    move-object v3, p1

    .line 803
    check-cast v3, Lio/realm/ar;

    invoke-interface {v3}, Lio/realm/ar;->f()Ljava/lang/String;

    move-result-object v3

    .line 805
    if-nez v3, :cond_3

    .line 806
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->k(J)J

    move-result-wide v6

    .line 810
    :goto_1
    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-eqz v3, :cond_4

    .line 812
    :try_start_0
    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->f(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    iget-object v3, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v3, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/g$b;->a(Lio/realm/g;Lio/realm/internal/n;Lio/realm/internal/c;ZLjava/util/List;)V

    .line 813
    new-instance v4, Lio/realm/aq;

    invoke-direct {v4}, Lio/realm/aq;-><init>()V

    .line 814
    move-object v0, v4

    check-cast v0, Lio/realm/internal/l;

    move-object v3, v0

    invoke-interface {p3, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 816
    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    move v2, p2

    .line 823
    :goto_2
    if-eqz v2, :cond_5

    .line 824
    invoke-static {p0, v4, p1, p3}, Lio/realm/aq;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;Lco/uk/getmondo/payments/a/a/e;Ljava/util/Map;)Lco/uk/getmondo/payments/a/a/e;

    move-result-object p1

    goto :goto_0

    .line 808
    :cond_3
    invoke-virtual {v8, v6, v7, v3}, Lio/realm/internal/Table;->a(JLjava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    .line 816
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/g$b;->f()V

    throw v3

    :cond_4
    move v2, v4

    move-object v4, v5

    .line 819
    goto :goto_2

    .line 826
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/aq;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/e;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v2, p2

    move-object v4, v5

    goto :goto_2
.end method

.method public static a(Lio/realm/internal/SharedRealm;Z)Lio/realm/aq$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0xa

    .line 447
    const-string v0, "class_PaymentSeries"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The \'PaymentSeries\' class is missing from the schema for this Realm."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 450
    :cond_0
    const-string v0, "class_PaymentSeries"

    invoke-virtual {p0, v0}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 451
    invoke-virtual {v2}, Lio/realm/internal/Table;->c()J

    move-result-wide v4

    .line 452
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    .line 453
    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 454
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is less than expected - expected 10 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_1
    if-eqz p1, :cond_3

    .line 457
    const-string v0, "Field count is more than expected - expected 10 but was %1$d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lio/realm/log/RealmLog;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 462
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 463
    const-wide/16 v0, 0x0

    :goto_0
    cmp-long v6, v0, v4

    if-gez v6, :cond_4

    .line 464
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->c(J)Lio/realm/RealmFieldType;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 459
    :cond_3
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Field count is more than expected - expected 10 but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_4
    new-instance v0, Lio/realm/aq$a;

    invoke-direct {v0, p0, v2}, Lio/realm/aq$a;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/Table;)V

    .line 469
    invoke-virtual {v2}, Lio/realm/internal/Table;->e()Z

    move-result v1

    if-nez v1, :cond_5

    .line 470
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 472
    :cond_5
    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lio/realm/aq$a;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 473
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_6
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 478
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 480
    :cond_7
    const-string v1, "id"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_8

    .line 481
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'id\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 483
    :cond_8
    iget-wide v4, v0, Lio/realm/aq$a;->a:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_9

    .line 484
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@PrimaryKey field \'id\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 486
    :cond_9
    const-string v1, "id"

    invoke-virtual {v2, v1}, Lio/realm/internal/Table;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->j(J)Z

    move-result v1

    if-nez v1, :cond_a

    .line 487
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 489
    :cond_a
    const-string v1, "accountId"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 490
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'accountId\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 492
    :cond_b
    const-string v1, "accountId"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_c

    .line 493
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'accountId\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 495
    :cond_c
    iget-wide v4, v0, Lio/realm/aq$a;->b:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_d

    .line 496
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'accountId\' is required. Either set @Required to field \'accountId\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 498
    :cond_d
    const-string v1, "scheme"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 499
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'scheme\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 501
    :cond_e
    const-string v1, "scheme"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_f

    .line 502
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'scheme\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 504
    :cond_f
    iget-wide v4, v0, Lio/realm/aq$a;->c:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_10

    .line 505
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'scheme\' is required. Either set @Required to field \'scheme\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 507
    :cond_10
    const-string v1, "schemaData"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 508
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'schemaData\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 510
    :cond_11
    const-string v1, "schemaData"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_12

    .line 511
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'FpsSchemaData\' for field \'schemaData\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 513
    :cond_12
    const-string v1, "class_FpsSchemaData"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 514
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing class \'class_FpsSchemaData\' for field \'schemaData\'"

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 516
    :cond_13
    const-string v1, "class_FpsSchemaData"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->b(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v1

    .line 517
    iget-wide v4, v0, Lio/realm/aq$a;->d:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v4

    invoke-virtual {v4, v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/Table;)Z

    move-result v4

    if-nez v4, :cond_14

    .line 518
    new-instance v3, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid RealmObject for field \'schemaData\': \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lio/realm/aq$a;->d:J

    invoke-virtual {v2, v6, v7}, Lio/realm/internal/Table;->e(J)Lio/realm/internal/Table;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' expected - was \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lio/realm/internal/Table;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 520
    :cond_14
    const-string v1, "_amount"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 521
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'_amount\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 523
    :cond_15
    const-string v1, "_amount"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_16

    .line 524
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'long\' for field \'_amount\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 526
    :cond_16
    iget-wide v4, v0, Lio/realm/aq$a;->e:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 527
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'_amount\' does support null values in the existing Realm file. Use corresponding boxed type for field \'_amount\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 529
    :cond_17
    const-string v1, "_currency"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_18

    .line 530
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'_currency\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 532
    :cond_18
    const-string v1, "_currency"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_19

    .line 533
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'_currency\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 535
    :cond_19
    iget-wide v4, v0, Lio/realm/aq$a;->f:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_1a

    .line 536
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'_currency\' is required. Either set @Required to field \'_currency\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 538
    :cond_1a
    const-string v1, "_startDate"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 539
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'_startDate\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 541
    :cond_1b
    const-string v1, "_startDate"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_1c

    .line 542
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'_startDate\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 544
    :cond_1c
    iget-wide v4, v0, Lio/realm/aq$a;->g:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 545
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'_startDate\' is required. Either set @Required to field \'_startDate\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_1d
    const-string v1, "_nextIterationDate"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 548
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'_nextIterationDate\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 550
    :cond_1e
    const-string v1, "_nextIterationDate"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_1f

    .line 551
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'_nextIterationDate\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 553
    :cond_1f
    iget-wide v4, v0, Lio/realm/aq$a;->h:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_20

    .line 554
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'_nextIterationDate\' is required. Either set @Required to field \'_nextIterationDate\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 556
    :cond_20
    const-string v1, "_endDate"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 557
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'_endDate\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 559
    :cond_21
    const-string v1, "_endDate"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v4, :cond_22

    .line 560
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'_endDate\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 562
    :cond_22
    iget-wide v4, v0, Lio/realm/aq$a;->i:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_23

    .line 563
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'_endDate\' is required. Either set @Required to field \'_endDate\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 565
    :cond_23
    const-string v1, "_intervalType"

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_24

    .line 566
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Missing field \'_intervalType\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 568
    :cond_24
    const-string v1, "_intervalType"

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v1, v3, :cond_25

    .line 569
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid type \'String\' for field \'_intervalType\' in existing Realm file."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 571
    :cond_25
    iget-wide v4, v0, Lio/realm/aq$a;->j:J

    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->a(J)Z

    move-result v1

    if-nez v1, :cond_26

    .line 572
    new-instance v0, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->h()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Field \'_intervalType\' is required. Either set @Required to field \'_intervalType\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v0, v1, v2}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 575
    :cond_26
    return-object v0
.end method

.method public static a(Lio/realm/av;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/bb;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1074
    const-class v2, Lco/uk/getmondo/payments/a/a/e;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v13

    .line 1075
    invoke-virtual {v13}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    .line 1076
    move-object/from16 v0, p0

    iget-object v4, v0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v5, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v4, v5}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lio/realm/aq$a;

    .line 1077
    invoke-virtual {v13}, Lio/realm/internal/Table;->d()J

    move-result-wide v14

    .line 1079
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1080
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Lco/uk/getmondo/payments/a/a/e;

    .line 1081
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1084
    instance-of v4, v12, Lio/realm/internal/l;

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v4, v12

    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v4, v12

    .line 1085
    check-cast v4, Lio/realm/internal/l;

    invoke-interface {v4}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v4, v12

    .line 1088
    check-cast v4, Lio/realm/ar;

    invoke-interface {v4}, Lio/realm/ar;->f()Ljava/lang/String;

    move-result-object v4

    .line 1090
    if-nez v4, :cond_3

    .line 1091
    invoke-static {v2, v3, v14, v15}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v6

    .line 1095
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 1096
    invoke-static {v13, v4}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v6

    .line 1098
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v12, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v12

    .line 1099
    check-cast v4, Lio/realm/ar;

    invoke-interface {v4}, Lio/realm/ar;->g()Ljava/lang/String;

    move-result-object v8

    .line 1100
    if-eqz v8, :cond_4

    .line 1101
    iget-wide v4, v11, Lio/realm/aq$a;->b:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_2
    move-object v4, v12

    .line 1105
    check-cast v4, Lio/realm/ar;

    invoke-interface {v4}, Lio/realm/ar;->h()Ljava/lang/String;

    move-result-object v8

    .line 1106
    if-eqz v8, :cond_5

    .line 1107
    iget-wide v4, v11, Lio/realm/aq$a;->c:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_3
    move-object v4, v12

    .line 1112
    check-cast v4, Lio/realm/ar;

    invoke-interface {v4}, Lio/realm/ar;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v5

    .line 1113
    if-eqz v5, :cond_6

    .line 1114
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1115
    if-nez v4, :cond_d

    .line 1116
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v5, v1}, Lio/realm/w;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;Ljava/util/Map;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v8, v4

    .line 1118
    :goto_4
    iget-wide v4, v11, Lio/realm/aq$a;->d:J

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 1122
    :goto_5
    iget-wide v4, v11, Lio/realm/aq$a;->e:J

    move-object v8, v12

    check-cast v8, Lio/realm/ar;

    invoke-interface {v8}, Lio/realm/ar;->j()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v4, v12

    .line 1123
    check-cast v4, Lio/realm/ar;

    invoke-interface {v4}, Lio/realm/ar;->k()Ljava/lang/String;

    move-result-object v8

    .line 1124
    if-eqz v8, :cond_7

    .line 1125
    iget-wide v4, v11, Lio/realm/aq$a;->f:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_6
    move-object v4, v12

    .line 1129
    check-cast v4, Lio/realm/ar;

    invoke-interface {v4}, Lio/realm/ar;->l()Ljava/lang/String;

    move-result-object v8

    .line 1130
    if-eqz v8, :cond_8

    .line 1131
    iget-wide v4, v11, Lio/realm/aq$a;->g:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_7
    move-object v4, v12

    .line 1135
    check-cast v4, Lio/realm/ar;

    invoke-interface {v4}, Lio/realm/ar;->m()Ljava/lang/String;

    move-result-object v8

    .line 1136
    if-eqz v8, :cond_9

    .line 1137
    iget-wide v4, v11, Lio/realm/aq$a;->h:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_8
    move-object v4, v12

    .line 1141
    check-cast v4, Lio/realm/ar;

    invoke-interface {v4}, Lio/realm/ar;->n()Ljava/lang/String;

    move-result-object v8

    .line 1142
    if-eqz v8, :cond_a

    .line 1143
    iget-wide v4, v11, Lio/realm/aq$a;->i:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1147
    :goto_9
    check-cast v12, Lio/realm/ar;

    invoke-interface {v12}, Lio/realm/ar;->o()Ljava/lang/String;

    move-result-object v8

    .line 1148
    if-eqz v8, :cond_b

    .line 1149
    iget-wide v4, v11, Lio/realm/aq$a;->j:J

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 1093
    :cond_3
    invoke-static {v2, v3, v14, v15, v4}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v6

    goto/16 :goto_1

    .line 1103
    :cond_4
    iget-wide v4, v11, Lio/realm/aq$a;->b:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_2

    .line 1109
    :cond_5
    iget-wide v4, v11, Lio/realm/aq$a;->c:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_3

    .line 1120
    :cond_6
    iget-wide v4, v11, Lio/realm/aq$a;->d:J

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_5

    .line 1127
    :cond_7
    iget-wide v4, v11, Lio/realm/aq$a;->f:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_6

    .line 1133
    :cond_8
    iget-wide v4, v11, Lio/realm/aq$a;->g:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_7

    .line 1139
    :cond_9
    iget-wide v4, v11, Lio/realm/aq$a;->h:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_8

    .line 1145
    :cond_a
    iget-wide v4, v11, Lio/realm/aq$a;->i:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_9

    .line 1151
    :cond_b
    iget-wide v4, v11, Lio/realm/aq$a;->j:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_0

    .line 1154
    :cond_c
    return-void

    :cond_d
    move-object v8, v4

    goto/16 :goto_4
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;Ljava/util/Map;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/e;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 998
    instance-of v0, p1, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lio/realm/av;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 999
    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    .line 1070
    :goto_0
    return-wide v4

    .line 1001
    :cond_0
    const-class v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p0, v0}, Lio/realm/av;->c(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v3

    .line 1002
    invoke-virtual {v3}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    .line 1003
    iget-object v2, p0, Lio/realm/av;->f:Lio/realm/bh;

    const-class v4, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v2, v4}, Lio/realm/bh;->c(Ljava/lang/Class;)Lio/realm/internal/c;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lio/realm/aq$a;

    .line 1004
    invoke-virtual {v3}, Lio/realm/internal/Table;->d()J

    move-result-wide v4

    move-object v2, p1

    .line 1005
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->f()Ljava/lang/String;

    move-result-object v2

    .line 1007
    if-nez v2, :cond_2

    .line 1008
    invoke-static {v0, v1, v4, v5}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v4

    .line 1012
    :goto_1
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    .line 1013
    invoke-static {v3, v2}, Lio/realm/internal/OsObject;->b(Lio/realm/internal/Table;Ljava/lang/Object;)J

    move-result-wide v4

    .line 1015
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 1016
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->g()Ljava/lang/String;

    move-result-object v6

    .line 1017
    if-eqz v6, :cond_3

    .line 1018
    iget-wide v2, v9, Lio/realm/aq$a;->b:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_2
    move-object v2, p1

    .line 1022
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->h()Ljava/lang/String;

    move-result-object v6

    .line 1023
    if-eqz v6, :cond_4

    .line 1024
    iget-wide v2, v9, Lio/realm/aq$a;->c:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_3
    move-object v2, p1

    .line 1029
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v3

    .line 1030
    if-eqz v3, :cond_5

    .line 1031
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1032
    if-nez v2, :cond_b

    .line 1033
    invoke-static {p0, v3, p2}, Lio/realm/w;->b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;Ljava/util/Map;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v6, v2

    .line 1035
    :goto_4
    iget-wide v2, v9, Lio/realm/aq$a;->d:J

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 1039
    :goto_5
    iget-wide v2, v9, Lio/realm/aq$a;->e:J

    move-object v6, p1

    check-cast v6, Lio/realm/ar;

    invoke-interface {v6}, Lio/realm/ar;->j()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object v2, p1

    .line 1040
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->k()Ljava/lang/String;

    move-result-object v6

    .line 1041
    if-eqz v6, :cond_6

    .line 1042
    iget-wide v2, v9, Lio/realm/aq$a;->f:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_6
    move-object v2, p1

    .line 1046
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->l()Ljava/lang/String;

    move-result-object v6

    .line 1047
    if-eqz v6, :cond_7

    .line 1048
    iget-wide v2, v9, Lio/realm/aq$a;->g:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_7
    move-object v2, p1

    .line 1052
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->m()Ljava/lang/String;

    move-result-object v6

    .line 1053
    if-eqz v6, :cond_8

    .line 1054
    iget-wide v2, v9, Lio/realm/aq$a;->h:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    :goto_8
    move-object v2, p1

    .line 1058
    check-cast v2, Lio/realm/ar;

    invoke-interface {v2}, Lio/realm/ar;->n()Ljava/lang/String;

    move-result-object v6

    .line 1059
    if-eqz v6, :cond_9

    .line 1060
    iget-wide v2, v9, Lio/realm/aq$a;->i:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 1064
    :goto_9
    check-cast p1, Lio/realm/ar;

    invoke-interface {p1}, Lio/realm/ar;->o()Ljava/lang/String;

    move-result-object v6

    .line 1065
    if-eqz v6, :cond_a

    .line 1066
    iget-wide v2, v9, Lio/realm/aq$a;->j:J

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 1010
    :cond_2
    invoke-static {v0, v1, v4, v5, v2}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v4

    goto/16 :goto_1

    .line 1020
    :cond_3
    iget-wide v2, v9, Lio/realm/aq$a;->b:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_2

    .line 1026
    :cond_4
    iget-wide v2, v9, Lio/realm/aq$a;->c:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_3

    .line 1037
    :cond_5
    iget-wide v2, v9, Lio/realm/aq$a;->d:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto :goto_5

    .line 1044
    :cond_6
    iget-wide v2, v9, Lio/realm/aq$a;->f:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_6

    .line 1050
    :cond_7
    iget-wide v2, v9, Lio/realm/aq$a;->g:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_7

    .line 1056
    :cond_8
    iget-wide v2, v9, Lio/realm/aq$a;->h:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_8

    .line 1062
    :cond_9
    iget-wide v2, v9, Lio/realm/aq$a;->i:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_9

    .line 1068
    :cond_a
    iget-wide v2, v9, Lio/realm/aq$a;->j:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto/16 :goto_0

    :cond_b
    move-object v6, v2

    goto/16 :goto_4
.end method

.method public static b(Lio/realm/av;Lco/uk/getmondo/payments/a/a/e;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/e;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            "Lco/uk/getmondo/payments/a/a/e;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/bb;",
            "Lio/realm/internal/l;",
            ">;)",
            "Lco/uk/getmondo/payments/a/a/e;"
        }
    .end annotation

    .prologue
    .line 831
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/l;

    .line 832
    if-eqz v0, :cond_0

    .line 833
    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    .line 863
    :goto_0
    return-object v0

    .line 837
    :cond_0
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    move-object v0, p1

    check-cast v0, Lio/realm/ar;

    invoke-interface {v0}, Lio/realm/ar;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lio/realm/av;->a(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    move-object v1, v0

    .line 838
    check-cast v1, Lio/realm/internal/l;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    check-cast p1, Lio/realm/ar;

    move-object v1, v0

    .line 841
    check-cast v1, Lio/realm/ar;

    .line 843
    invoke-interface {p1}, Lio/realm/ar;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/ar;->b(Ljava/lang/String;)V

    .line 844
    invoke-interface {p1}, Lio/realm/ar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/ar;->c(Ljava/lang/String;)V

    .line 846
    invoke-interface {p1}, Lio/realm/ar;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v3

    .line 847
    if-nez v3, :cond_1

    .line 848
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lio/realm/ar;->a(Lco/uk/getmondo/payments/a/a/b;)V

    .line 857
    :goto_1
    invoke-interface {p1}, Lio/realm/ar;->j()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lio/realm/ar;->a(J)V

    .line 858
    invoke-interface {p1}, Lio/realm/ar;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/ar;->d(Ljava/lang/String;)V

    .line 859
    invoke-interface {p1}, Lio/realm/ar;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/ar;->e(Ljava/lang/String;)V

    .line 860
    invoke-interface {p1}, Lio/realm/ar;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/ar;->f(Ljava/lang/String;)V

    .line 861
    invoke-interface {p1}, Lio/realm/ar;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/ar;->g(Ljava/lang/String;)V

    .line 862
    invoke-interface {p1}, Lio/realm/ar;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/ar;->h(Ljava/lang/String;)V

    goto :goto_0

    .line 850
    :cond_1
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/payments/a/a/b;

    .line 851
    if-eqz v2, :cond_2

    .line 852
    invoke-interface {v1, v2}, Lio/realm/ar;->a(Lco/uk/getmondo/payments/a/a/b;)V

    goto :goto_1

    .line 854
    :cond_2
    invoke-static {p0, v3, p2, p3}, Lio/realm/w;->a(Lio/realm/av;Lco/uk/getmondo/payments/a/a/b;ZLjava/util/Map;)Lco/uk/getmondo/payments/a/a/b;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/ar;->a(Lco/uk/getmondo/payments/a/a/b;)V

    goto :goto_1
.end method

.method public static p()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 1

    .prologue
    .line 443
    sget-object v0, Lio/realm/aq;->d:Lio/realm/internal/OsObjectSchemaInfo;

    return-object v0
.end method

.method public static q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 579
    const-string v0, "class_PaymentSeries"

    return-object v0
.end method

.method private static r()Lio/realm/internal/OsObjectSchemaInfo;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 428
    new-instance v0, Lio/realm/internal/OsObjectSchemaInfo$a;

    const-string v1, "PaymentSeries"

    invoke-direct {v0, v1}, Lio/realm/internal/OsObjectSchemaInfo$a;-><init>(Ljava/lang/String;)V

    .line 429
    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 430
    const-string v7, "accountId"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 431
    const-string v7, "scheme"

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 432
    const-string v1, "schemaData"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v4, "FpsSchemaData"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;Ljava/lang/String;)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 433
    const-string v7, "_amount"

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move-object v6, v0

    move v9, v5

    move v10, v5

    move v11, v3

    invoke-virtual/range {v6 .. v11}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 434
    const-string v1, "_currency"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 435
    const-string v1, "_startDate"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 436
    const-string v1, "_nextIterationDate"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 437
    const-string v1, "_endDate"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 438
    const-string v1, "_intervalType"

    sget-object v2, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    move v3, v5

    move v4, v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/internal/OsObjectSchemaInfo$a;->a(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)Lio/realm/internal/OsObjectSchemaInfo$a;

    .line 439
    invoke-virtual {v0}, Lio/realm/internal/OsObjectSchemaInfo$a;->a()Lio/realm/internal/OsObjectSchemaInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 9

    .prologue
    .line 264
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 268
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    .line 269
    invoke-interface {v0}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v2, Lio/realm/aq$a;->e:J

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    const/4 v8, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->a(JJJZ)V

    goto :goto_0

    .line 273
    :cond_1
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 274
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->e:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/n;->a(JJ)V

    goto :goto_0
.end method

.method public a(Lco/uk/getmondo/payments/a/a/b;)V
    .locals 9

    .prologue
    .line 215
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 216
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->d()Ljava/util/List;

    move-result-object v0

    const-string v1, "schemaData"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    if-eqz p1, :cond_a

    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 223
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    check-cast v0, Lio/realm/av;

    invoke-virtual {v0, p1}, Lio/realm/av;->a(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/b;

    move-object v6, v0

    .line 225
    :goto_1
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 226
    if-nez v6, :cond_2

    .line 228
    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v0, v0, Lio/realm/aq$a;->d:J

    invoke-interface {v4, v0, v1}, Lio/realm/internal/n;->o(J)V

    goto :goto_0

    .line 231
    :cond_2
    invoke-static {v6}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v6

    .line 234
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_4

    .line 235
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_4
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v0, Lio/realm/aq$a;->d:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    check-cast v6, Lio/realm/internal/l;

    invoke-interface {v6}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    invoke-interface {v0}, Lio/realm/internal/n;->c()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->b(JJJZ)V

    goto :goto_0

    .line 241
    :cond_5
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 242
    if-nez p1, :cond_6

    .line 243
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->o(J)V

    goto/16 :goto_0

    .line 246
    :cond_6
    invoke-static {p1}, Lio/realm/bc;->c(Lio/realm/bb;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 247
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' is not a valid managed object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, p1

    .line 249
    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v1}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'value\' belongs to a different Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_9
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->d:J

    check-cast p1, Lio/realm/internal/l;

    invoke-interface {p1}, Lio/realm/internal/l;->s_()Lio/realm/au;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/n;->b(JJ)V

    goto/16 :goto_0

    :cond_a
    move-object v6, p1

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 141
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 153
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 158
    if-nez p1, :cond_1

    .line 159
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v0, Lio/realm/aq$a;->b:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 162
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v1, v1, Lio/realm/aq$a;->b:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 166
    :cond_2
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 167
    if-nez p1, :cond_3

    .line 168
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 171
    :cond_3
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->b:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 183
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 188
    if-nez p1, :cond_1

    .line 189
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v0, Lio/realm/aq$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 192
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v1, v1, Lio/realm/aq$a;->c:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 196
    :cond_2
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 197
    if-nez p1, :cond_3

    .line 198
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 201
    :cond_3
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->c:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 286
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    :goto_0
    return-void

    .line 290
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 291
    if-nez p1, :cond_1

    .line 292
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v0, Lio/realm/aq$a;->f:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 295
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v1, v1, Lio/realm/aq$a;->f:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 299
    :cond_2
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 300
    if-nez p1, :cond_3

    .line 301
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 304
    :cond_3
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->f:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 316
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    :goto_0
    return-void

    .line 320
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 321
    if-nez p1, :cond_1

    .line 322
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v0, Lio/realm/aq$a;->g:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 325
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v1, v1, Lio/realm/aq$a;->g:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 329
    :cond_2
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 330
    if-nez p1, :cond_3

    .line 331
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->g:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 334
    :cond_3
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->g:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 129
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 130
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->a:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 346
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 347
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    :goto_0
    return-void

    .line 350
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 351
    if-nez p1, :cond_1

    .line 352
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v0, Lio/realm/aq$a;->h:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 355
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v1, v1, Lio/realm/aq$a;->h:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 359
    :cond_2
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 360
    if-nez p1, :cond_3

    .line 361
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 364
    :cond_3
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->h:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 148
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->b:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 376
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 381
    if-nez p1, :cond_1

    .line 382
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v0, Lio/realm/aq$a;->i:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 385
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v1, v1, Lio/realm/aq$a;->i:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 389
    :cond_2
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 390
    if-nez p1, :cond_3

    .line 391
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->i:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 394
    :cond_3
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->i:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 177
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 178
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->c:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 406
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 407
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    :goto_0
    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v4

    .line 411
    if-nez p1, :cond_1

    .line 412
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v0, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v0, Lio/realm/aq$a;->j:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lio/realm/internal/Table;->a(JJZ)V

    goto :goto_0

    .line 415
    :cond_1
    invoke-interface {v4}, Lio/realm/internal/n;->b()Lio/realm/internal/Table;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v1, v1, Lio/realm/aq$a;->j:J

    invoke-interface {v4}, Lio/realm/internal/n;->c()J

    move-result-wide v3

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lio/realm/internal/Table;->a(JJLjava/lang/String;Z)V

    goto :goto_0

    .line 419
    :cond_2
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 420
    if-nez p1, :cond_3

    .line 421
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->j:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->c(J)V

    goto :goto_0

    .line 424
    :cond_3
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->j:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/n;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public i()Lco/uk/getmondo/payments/a/a/b;
    .locals 6

    .prologue
    .line 206
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 207
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->d:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/payments/a/a/b;

    iget-object v2, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v2}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v2

    iget-object v3, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v4, v3, Lio/realm/aq$a;->d:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/n;->m(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/g;->a(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/b;

    goto :goto_0
.end method

.method public j()J
    .locals 4

    .prologue
    .line 258
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 259
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->e:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->f(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public k()Ljava/lang/String;
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 281
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->f:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 310
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 311
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->g:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 4

    .prologue
    .line 340
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 341
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->h:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 4

    .prologue
    .line 370
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 371
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->i:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 4

    .prologue
    .line 400
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->a()Lio/realm/g;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/g;->e()V

    .line 401
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/au;->b()Lio/realm/internal/n;

    move-result-object v0

    iget-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    iget-wide v2, v1, Lio/realm/aq$a;->j:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/n;->k(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s_()Lio/realm/au;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/au",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1267
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1218
    invoke-static {p0}, Lio/realm/bc;->b(Lio/realm/bb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1219
    const-string v0, "Invalid object"

    .line 1262
    :goto_0
    return-object v0

    .line 1221
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "PaymentSeries = proxy["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1222
    const-string v0, "{id:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1223
    invoke-virtual {p0}, Lio/realm/aq;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lio/realm/aq;->f()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1224
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1225
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1226
    const-string v0, "{accountId:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1227
    invoke-virtual {p0}, Lio/realm/aq;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lio/realm/aq;->g()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1228
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1229
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1230
    const-string v0, "{scheme:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1231
    invoke-virtual {p0}, Lio/realm/aq;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lio/realm/aq;->h()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1232
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1233
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1234
    const-string v0, "{schemaData:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1235
    invoke-virtual {p0}, Lio/realm/aq;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "FpsSchemaData"

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1236
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1237
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1238
    const-string v0, "{_amount:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1239
    invoke-virtual {p0}, Lio/realm/aq;->j()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1240
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1241
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1242
    const-string v0, "{_currency:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1243
    invoke-virtual {p0}, Lio/realm/aq;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lio/realm/aq;->k()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1244
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1245
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1246
    const-string v0, "{_startDate:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1247
    invoke-virtual {p0}, Lio/realm/aq;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lio/realm/aq;->l()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1248
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1249
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1250
    const-string v0, "{_nextIterationDate:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1251
    invoke-virtual {p0}, Lio/realm/aq;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lio/realm/aq;->m()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1252
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1253
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1254
    const-string v0, "{_endDate:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1255
    invoke-virtual {p0}, Lio/realm/aq;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lio/realm/aq;->n()Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1256
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1257
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1258
    const-string v0, "{_intervalType:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1259
    invoke-virtual {p0}, Lio/realm/aq;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lio/realm/aq;->o()Ljava/lang/String;

    move-result-object v0

    :goto_9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1260
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1261
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1262
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1223
    :cond_1
    const-string v0, "null"

    goto/16 :goto_1

    .line 1227
    :cond_2
    const-string v0, "null"

    goto/16 :goto_2

    .line 1231
    :cond_3
    const-string v0, "null"

    goto/16 :goto_3

    .line 1235
    :cond_4
    const-string v0, "null"

    goto/16 :goto_4

    .line 1243
    :cond_5
    const-string v0, "null"

    goto/16 :goto_5

    .line 1247
    :cond_6
    const-string v0, "null"

    goto :goto_6

    .line 1251
    :cond_7
    const-string v0, "null"

    goto :goto_7

    .line 1255
    :cond_8
    const-string v0, "null"

    goto :goto_8

    .line 1259
    :cond_9
    const-string v0, "null"

    goto :goto_9
.end method

.method public u_()V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lio/realm/aq;->c:Lio/realm/au;

    if-eqz v0, :cond_0

    .line 124
    :goto_0
    return-void

    .line 117
    :cond_0
    sget-object v0, Lio/realm/g;->g:Lio/realm/g$c;

    invoke-virtual {v0}, Lio/realm/g$c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g$b;

    .line 118
    invoke-virtual {v0}, Lio/realm/g$b;->c()Lio/realm/internal/c;

    move-result-object v1

    check-cast v1, Lio/realm/aq$a;

    iput-object v1, p0, Lio/realm/aq;->b:Lio/realm/aq$a;

    .line 119
    new-instance v1, Lio/realm/au;

    invoke-direct {v1, p0}, Lio/realm/au;-><init>(Lio/realm/bb;)V

    iput-object v1, p0, Lio/realm/aq;->c:Lio/realm/au;

    .line 120
    iget-object v1, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->a()Lio/realm/g;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/g;)V

    .line 121
    iget-object v1, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->b()Lio/realm/internal/n;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Lio/realm/internal/n;)V

    .line 122
    iget-object v1, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/au;->a(Z)V

    .line 123
    iget-object v1, p0, Lio/realm/aq;->c:Lio/realm/au;

    invoke-virtual {v0}, Lio/realm/g$b;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/au;->a(Ljava/util/List;)V

    goto :goto_0
.end method
