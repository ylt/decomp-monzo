.class public final enum Lio/realm/bl;
.super Ljava/lang/Enum;
.source "Sort.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/realm/bl;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/realm/bl;

.field public static final enum b:Lio/realm/bl;

.field private static final synthetic d:[Lio/realm/bl;


# instance fields
.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lio/realm/bl;

    const-string v1, "ASCENDING"

    invoke-direct {v0, v1, v2, v3}, Lio/realm/bl;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lio/realm/bl;->a:Lio/realm/bl;

    .line 26
    new-instance v0, Lio/realm/bl;

    const-string v1, "DESCENDING"

    invoke-direct {v0, v1, v3, v2}, Lio/realm/bl;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lio/realm/bl;->b:Lio/realm/bl;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lio/realm/bl;

    sget-object v1, Lio/realm/bl;->a:Lio/realm/bl;

    aput-object v1, v0, v2

    sget-object v1, Lio/realm/bl;->b:Lio/realm/bl;

    aput-object v1, v0, v3

    sput-object v0, Lio/realm/bl;->d:[Lio/realm/bl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput-boolean p3, p0, Lio/realm/bl;->c:Z

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/realm/bl;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lio/realm/bl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/realm/bl;

    return-object v0
.end method

.method public static values()[Lio/realm/bl;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lio/realm/bl;->d:[Lio/realm/bl;

    invoke-virtual {v0}, [Lio/realm/bl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/realm/bl;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lio/realm/bl;->c:Z

    return v0
.end method
