.class final Lio/realm/aw;
.super Ljava/lang/Object;
.source "RealmCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/aw$b;,
        Lio/realm/aw$c;,
        Lio/realm/aw$a;
    }
.end annotation


# static fields
.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lio/realm/aw;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lio/realm/aw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lio/realm/aw$b;",
            "Lio/realm/aw$c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private c:Lio/realm/ay;

.field private final d:[Lio/realm/internal/b;

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lio/realm/aw;->e:Ljava/util/List;

    .line 213
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lio/realm/aw;->g:Ljava/util/Collection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    const/4 v1, 0x4

    new-array v1, v1, [Lio/realm/internal/b;

    iput-object v1, p0, Lio/realm/aw;->d:[Lio/realm/internal/b;

    .line 210
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lio/realm/aw;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 219
    iput-object p1, p0, Lio/realm/aw;->b:Ljava/lang/String;

    .line 220
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, Lio/realm/aw$b;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lio/realm/aw;->a:Ljava/util/EnumMap;

    .line 221
    invoke-static {}, Lio/realm/aw$b;->values()[Lio/realm/aw$b;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 222
    iget-object v4, p0, Lio/realm/aw;->a:Ljava/util/EnumMap;

    new-instance v5, Lio/realm/aw$c;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lio/realm/aw$c;-><init>(Lio/realm/aw$1;)V

    invoke-virtual {v4, v3, v5}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 224
    :cond_0
    return-void
.end method

.method private static a([Lio/realm/internal/b;Lio/realm/internal/b;)I
    .locals 8

    .prologue
    .line 641
    const-wide v2, 0x7fffffffffffffffL

    .line 642
    const/4 v0, -0x1

    .line 643
    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    .line 644
    aget-object v4, p0, v1

    if-nez v4, :cond_0

    .line 645
    aput-object p1, p0, v1

    .line 656
    :goto_1
    return v1

    .line 649
    :cond_0
    aget-object v4, p0, v1

    .line 650
    invoke-virtual {v4}, Lio/realm/internal/b;->a()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-gtz v5, :cond_1

    .line 651
    invoke-virtual {v4}, Lio/realm/internal/b;->a()J

    move-result-wide v2

    move v0, v1

    .line 643
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 655
    :cond_2
    aput-object p1, p0, v0

    move v1, v0

    .line 656
    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Z)Lio/realm/aw;
    .locals 5

    .prologue
    .line 227
    const/4 v1, 0x0

    .line 228
    sget-object v2, Lio/realm/aw;->e:Ljava/util/List;

    monitor-enter v2

    .line 229
    :try_start_0
    sget-object v0, Lio/realm/aw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 231
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/aw;

    .line 233
    if-nez v0, :cond_1

    .line 235
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move-object v0, v1

    :cond_0
    :goto_1
    move-object v1, v0

    .line 239
    goto :goto_0

    .line 236
    :cond_1
    iget-object v4, v0, Lio/realm/aw;->b:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move-object v0, v1

    goto :goto_1

    .line 241
    :cond_2
    if-nez v1, :cond_3

    if-eqz p1, :cond_3

    .line 242
    new-instance v1, Lio/realm/aw;

    invoke-direct {v1, p0}, Lio/realm/aw;-><init>(Ljava/lang/String;)V

    .line 243
    sget-object v0, Lio/realm/aw;->e:Ljava/util/List;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    :cond_3
    monitor-exit v2

    .line 246
    return-object v1

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static a(Lio/realm/ay;Ljava/lang/Class;)Lio/realm/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lio/realm/g;",
            ">(",
            "Lio/realm/ay;",
            "Ljava/lang/Class",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 282
    invoke-virtual {p0}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lio/realm/aw;->a(Ljava/lang/String;Z)Lio/realm/aw;

    move-result-object v0

    .line 284
    invoke-direct {v0, p0, p1}, Lio/realm/aw;->b(Lio/realm/ay;Ljava/lang/Class;)Lio/realm/g;

    move-result-object v0

    return-object v0
.end method

.method static a([Lio/realm/internal/b;J)Lio/realm/internal/b;
    .locals 5

    .prologue
    .line 620
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 621
    aget-object v0, p0, v1

    .line 622
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lio/realm/internal/b;->a()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 626
    :goto_1
    return-object v0

    .line 620
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 626
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private declared-synchronized a(Lio/realm/aw$a;)V
    .locals 1

    .prologue
    .line 490
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lio/realm/aw;->d()I

    move-result v0

    invoke-interface {p1, v0}, Lio/realm/aw$a;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 491
    monitor-exit p0

    return-void

    .line 490
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lio/realm/ay;)V
    .locals 4

    .prologue
    .line 439
    iget-object v0, p0, Lio/realm/aw;->c:Lio/realm/ay;

    invoke-virtual {v0, p1}, Lio/realm/ay;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    return-void

    .line 445
    :cond_0
    iget-object v0, p0, Lio/realm/aw;->c:Lio/realm/ay;

    invoke-virtual {v0}, Lio/realm/ay;->c()[B

    move-result-object v0

    invoke-virtual {p1}, Lio/realm/ay;->c()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 446
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong key used to decrypt Realm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 450
    :cond_1
    invoke-virtual {p1}, Lio/realm/ay;->e()Lio/realm/ba;

    move-result-object v0

    .line 451
    iget-object v1, p0, Lio/realm/aw;->c:Lio/realm/ay;

    invoke-virtual {v1}, Lio/realm/ay;->e()Lio/realm/ba;

    move-result-object v1

    .line 452
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 454
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 455
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Configurations cannot be different if used to open the same file. The most likely cause is that equals() and hashCode() are not overridden in the migration class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 458
    invoke-virtual {p1}, Lio/realm/ay;->e()Lio/realm/ba;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Configurations cannot be different if used to open the same file. \nCached configuration: \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/realm/aw;->c:Lio/realm/ay;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\nNew configuration: \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static a(Lio/realm/ay;Lio/realm/aw$a;)V
    .locals 3

    .prologue
    .line 479
    sget-object v1, Lio/realm/aw;->e:Ljava/util/List;

    monitor-enter v1

    .line 480
    :try_start_0
    invoke-virtual {p0}, Lio/realm/ay;->m()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lio/realm/aw;->a(Ljava/lang/String;Z)Lio/realm/aw;

    move-result-object v0

    .line 481
    if-nez v0, :cond_0

    .line 482
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lio/realm/aw$a;->a(I)V

    .line 483
    monitor-exit v1

    .line 487
    :goto_0
    return-void

    .line 485
    :cond_0
    invoke-direct {v0, p1}, Lio/realm/aw;->a(Lio/realm/aw$a;)V

    .line 486
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/io/File;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 548
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    :cond_0
    return-void

    .line 556
    :cond_1
    :try_start_0
    sget-object v0, Lio/realm/g;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 557
    if-nez v1, :cond_4

    .line 558
    :try_start_1
    new-instance v0, Lio/realm/exceptions/RealmFileException;

    sget-object v3, Lio/realm/exceptions/RealmFileException$Kind;->ACCESS_ERROR:Lio/realm/exceptions/RealmFileException$Kind;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid input stream to the asset file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 568
    :catch_0
    move-exception v0

    move-object v3, v1

    move-object v1, v2

    .line 569
    :goto_0
    :try_start_2
    new-instance v4, Lio/realm/exceptions/RealmFileException;

    sget-object v5, Lio/realm/exceptions/RealmFileException$Kind;->ACCESS_ERROR:Lio/realm/exceptions/RealmFileException$Kind;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not resolve the path to the asset file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6, v0}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 572
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v3, :cond_2

    .line 574
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 579
    :cond_2
    :goto_2
    if-eqz v1, :cond_3

    .line 581
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 587
    :cond_3
    :goto_3
    throw v0

    .line 562
    :cond_4
    :try_start_5
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 563
    const/16 v0, 0x1000

    :try_start_6
    new-array v0, v0, [B

    .line 565
    :goto_4
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-le v4, v5, :cond_5

    .line 566
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_4

    .line 568
    :catch_1
    move-exception v0

    move-object v8, v3

    move-object v3, v1

    move-object v1, v8

    goto :goto_0

    .line 572
    :cond_5
    if-eqz v1, :cond_8

    .line 574
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    move-object v1, v2

    .line 579
    :goto_5
    if-eqz v3, :cond_6

    .line 581
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 592
    :cond_6
    :goto_6
    if-eqz v1, :cond_0

    .line 593
    new-instance v0, Lio/realm/exceptions/RealmFileException;

    sget-object v2, Lio/realm/exceptions/RealmFileException$Kind;->ACCESS_ERROR:Lio/realm/exceptions/RealmFileException$Kind;

    invoke-direct {v0, v2, v1}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/Throwable;)V

    throw v0

    .line 575
    :catch_2
    move-exception v2

    move-object v1, v2

    .line 576
    goto :goto_5

    .line 582
    :catch_3
    move-exception v0

    .line 584
    if-nez v1, :cond_7

    :goto_7
    move-object v1, v0

    .line 587
    goto :goto_6

    .line 582
    :catch_4
    move-exception v1

    .line 584
    if-nez v2, :cond_3

    goto :goto_3

    .line 575
    :catch_5
    move-exception v2

    goto :goto_2

    .line 572
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v3, v1

    move-object v1, v2

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v8, v3

    move-object v3, v1

    move-object v1, v8

    goto :goto_1

    .line 568
    :catch_6
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_7

    :cond_8
    move-object v1, v2

    goto :goto_5
.end method

.method private declared-synchronized b(Lio/realm/ay;Ljava/lang/Class;)Lio/realm/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lio/realm/g;",
            ">(",
            "Lio/realm/ay;",
            "Ljava/lang/Class",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 290
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/realm/aw;->a:Ljava/util/EnumMap;

    invoke-static {p2}, Lio/realm/aw$b;->a(Ljava/lang/Class;)Lio/realm/aw$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/aw$c;

    .line 292
    invoke-direct {p0}, Lio/realm/aw;->d()I

    move-result v1

    if-nez v1, :cond_7

    .line 293
    invoke-static {p1}, Lio/realm/aw;->b(Lio/realm/ay;)V

    .line 294
    invoke-virtual {p1}, Lio/realm/ay;->n()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v3

    .line 298
    :try_start_1
    invoke-static {p1}, Lio/realm/internal/SharedRealm;->a(Lio/realm/ay;)Lio/realm/internal/SharedRealm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 303
    if-nez v3, :cond_0

    .line 305
    :try_start_2
    invoke-static {}, Lio/realm/internal/h;->a()Lio/realm/internal/h;

    move-result-object v3

    invoke-virtual {v3, p1}, Lio/realm/internal/h;->f(Lio/realm/ay;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 318
    :cond_0
    :try_start_3
    invoke-static {v1}, Lio/realm/internal/Table;->b(Lio/realm/internal/SharedRealm;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 319
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->a()V

    .line 320
    invoke-static {v1}, Lio/realm/internal/Table;->a(Lio/realm/internal/SharedRealm;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 321
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 328
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 329
    :try_start_4
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->close()V

    .line 334
    :cond_2
    iput-object p1, p0, Lio/realm/aw;->c:Lio/realm/ay;

    .line 340
    :goto_1
    invoke-static {v0}, Lio/realm/aw$c;->a(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_4

    .line 344
    const-class v1, Lio/realm/av;

    if-ne p2, v1, :cond_8

    .line 346
    invoke-static {p0}, Lio/realm/av;->a(Lio/realm/aw;)Lio/realm/av;

    move-result-object v1

    .line 354
    :goto_2
    invoke-static {v0}, Lio/realm/aw$c;->a(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 355
    invoke-static {v0}, Lio/realm/aw$c;->b(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 357
    const-class v2, Lio/realm/av;

    if-ne p2, v2, :cond_3

    invoke-static {v0}, Lio/realm/aw$c;->c(Lio/realm/aw$c;)I

    move-result v2

    if-nez v2, :cond_3

    .line 359
    iget-object v2, p0, Lio/realm/aw;->d:[Lio/realm/internal/b;

    iget-object v1, v1, Lio/realm/g;->f:Lio/realm/bh;

    invoke-virtual {v1}, Lio/realm/bh;->c()Lio/realm/internal/b;

    move-result-object v1

    invoke-static {v2, v1}, Lio/realm/aw;->a([Lio/realm/internal/b;Lio/realm/internal/b;)I

    .line 362
    :cond_3
    invoke-static {v0}, Lio/realm/aw$c;->d(Lio/realm/aw$c;)I

    .line 365
    :cond_4
    invoke-static {v0}, Lio/realm/aw$c;->b(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 366
    invoke-static {v0}, Lio/realm/aw$c;->b(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 369
    invoke-static {v0}, Lio/realm/aw$c;->a(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/g;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit p0

    return-object v0

    .line 306
    :catch_0
    move-exception v0

    .line 311
    :try_start_5
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 313
    :try_start_6
    invoke-static {p1}, Lio/realm/av;->c(Lio/realm/ay;)Z

    .line 314
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 328
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_5

    .line 329
    :try_start_7
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->close()V

    :cond_5
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 290
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 323
    :cond_6
    :try_start_8
    invoke-virtual {v1}, Lio/realm/internal/SharedRealm;->c()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_0

    .line 328
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 337
    :cond_7
    :try_start_9
    invoke-direct {p0, p1}, Lio/realm/aw;->a(Lio/realm/ay;)V

    goto/16 :goto_1

    .line 347
    :cond_8
    const-class v1, Lio/realm/o;

    if-ne p2, v1, :cond_9

    .line 348
    invoke-static {p0}, Lio/realm/o;->a(Lio/realm/aw;)Lio/realm/o;

    move-result-object v1

    goto :goto_2

    .line 350
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The type of Realm class must be Realm or DynamicRealm."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1
.end method

.method private static b(Lio/realm/ay;)V
    .locals 3

    .prologue
    .line 531
    invoke-virtual {p0}, Lio/realm/ay;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lio/realm/ay;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/ay;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 534
    invoke-virtual {p0}, Lio/realm/ay;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lio/realm/aw;->a(Ljava/lang/String;Ljava/io/File;)V

    .line 538
    :cond_0
    invoke-virtual {p0}, Lio/realm/ay;->q()Z

    move-result v0

    invoke-static {v0}, Lio/realm/internal/h;->a(Z)Lio/realm/internal/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lio/realm/internal/h;->d(Lio/realm/ay;)Ljava/lang/String;

    move-result-object v0

    .line 539
    invoke-static {v0}, Lio/realm/internal/Util;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 540
    invoke-virtual {p0}, Lio/realm/ay;->q()Z

    move-result v1

    invoke-static {v1}, Lio/realm/internal/h;->a(Z)Lio/realm/internal/h;

    move-result-object v1

    invoke-virtual {v1, p0}, Lio/realm/internal/h;->e(Lio/realm/ay;)Ljava/lang/String;

    move-result-object v1

    .line 542
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 543
    invoke-static {v0, v2}, Lio/realm/aw;->a(Ljava/lang/String;Ljava/io/File;)V

    .line 545
    :cond_1
    return-void
.end method

.method private d()I
    .locals 3

    .prologue
    .line 671
    const/4 v0, 0x0

    .line 672
    iget-object v1, p0, Lio/realm/aw;->a:Ljava/util/EnumMap;

    invoke-virtual {v1}, Ljava/util/EnumMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/aw$c;

    .line 673
    invoke-static {v0}, Lio/realm/aw$c;->c(Lio/realm/aw$c;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 674
    goto :goto_0

    .line 676
    :cond_0
    return v1
.end method


# virtual methods
.method public a()Lio/realm/ay;
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lio/realm/aw;->c:Lio/realm/ay;

    return-object v0
.end method

.method declared-synchronized a(Lio/realm/av;)V
    .locals 2

    .prologue
    .line 499
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/realm/aw;->a:Ljava/util/EnumMap;

    sget-object v1, Lio/realm/aw$b;->a:Lio/realm/aw$b;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/aw$c;

    .line 500
    invoke-static {v0}, Lio/realm/aw$c;->a(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    .line 510
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 505
    :cond_1
    :try_start_1
    iget-object v0, p0, Lio/realm/aw;->d:[Lio/realm/internal/b;

    .line 506
    invoke-virtual {p1, v0}, Lio/realm/av;->a([Lio/realm/internal/b;)Lio/realm/internal/b;

    move-result-object v1

    .line 507
    if-eqz v1, :cond_0

    .line 508
    invoke-static {v0, v1}, Lio/realm/aw;->a([Lio/realm/internal/b;Lio/realm/internal/b;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 499
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lio/realm/g;)V
    .locals 5

    .prologue
    .line 379
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lio/realm/g;->g()Ljava/lang/String;

    move-result-object v2

    .line 380
    iget-object v0, p0, Lio/realm/aw;->a:Ljava/util/EnumMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lio/realm/aw$b;->a(Ljava/lang/Class;)Lio/realm/aw$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/aw$c;

    .line 381
    invoke-static {v0}, Lio/realm/aw$c;->b(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 382
    if-nez v1, :cond_0

    .line 383
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 386
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gtz v3, :cond_2

    .line 387
    const-string v0, "%s has been closed already. refCount is %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v2, 0x1

    aput-object v1, v3, v2

    invoke-static {v0, v3}, Lio/realm/log/RealmLog;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 392
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 394
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_5

    .line 397
    invoke-static {v0}, Lio/realm/aw$c;->b(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 398
    invoke-static {v0}, Lio/realm/aw$c;->a(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 401
    invoke-static {v0}, Lio/realm/aw$c;->e(Lio/realm/aw$c;)I

    .line 402
    invoke-static {v0}, Lio/realm/aw$c;->c(Lio/realm/aw$c;)I

    move-result v1

    if-gez v1, :cond_3

    .line 404
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Global reference counter of Realm"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " got corrupted."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 379
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 409
    :cond_3
    :try_start_2
    instance-of v1, p1, Lio/realm/av;

    if-eqz v1, :cond_4

    invoke-static {v0}, Lio/realm/aw$c;->c(Lio/realm/aw$c;)I

    move-result v0

    if-nez v0, :cond_4

    .line 411
    iget-object v0, p0, Lio/realm/aw;->d:[Lio/realm/internal/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 415
    :cond_4
    invoke-virtual {p1}, Lio/realm/g;->j()V

    .line 418
    invoke-direct {p0}, Lio/realm/aw;->d()I

    move-result v0

    if-nez v0, :cond_1

    .line 422
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/aw;->c:Lio/realm/ay;

    .line 423
    invoke-virtual {p1}, Lio/realm/g;->h()Lio/realm/ay;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/ay;->q()Z

    move-result v0

    invoke-static {v0}, Lio/realm/internal/h;->a(Z)Lio/realm/internal/h;

    move-result-object v0

    .line 424
    invoke-virtual {p1}, Lio/realm/g;->h()Lio/realm/ay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/realm/internal/h;->a(Lio/realm/ay;)V

    goto :goto_0

    .line 428
    :cond_5
    invoke-static {v0}, Lio/realm/aw$c;->b(Lio/realm/aw$c;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public b()[Lio/realm/internal/b;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lio/realm/aw;->d:[Lio/realm/internal/b;

    return-object v0
.end method

.method c()V
    .locals 2

    .prologue
    .line 684
    iget-object v0, p0, Lio/realm/aw;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 685
    sget-object v0, Lio/realm/aw;->g:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 687
    :cond_0
    return-void
.end method
