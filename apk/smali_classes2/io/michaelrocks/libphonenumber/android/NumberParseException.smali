.class public Lio/michaelrocks/libphonenumber/android/NumberParseException;
.super Ljava/lang/Exception;
.source "NumberParseException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/michaelrocks/libphonenumber/android/NumberParseException$a;
    }
.end annotation


# instance fields
.field private a:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 62
    iput-object p2, p0, Lio/michaelrocks/libphonenumber/android/NumberParseException;->b:Ljava/lang/String;

    .line 63
    iput-object p1, p0, Lio/michaelrocks/libphonenumber/android/NumberParseException;->a:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    .line 64
    return-void
.end method


# virtual methods
.method public a()Lio/michaelrocks/libphonenumber/android/NumberParseException$a;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/NumberParseException;->a:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/michaelrocks/libphonenumber/android/NumberParseException;->a:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lio/michaelrocks/libphonenumber/android/NumberParseException;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
