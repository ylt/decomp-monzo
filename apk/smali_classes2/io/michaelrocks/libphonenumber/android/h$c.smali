.class public final enum Lio/michaelrocks/libphonenumber/android/h$c;
.super Ljava/lang/Enum;
.source "PhoneNumberUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/michaelrocks/libphonenumber/android/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/michaelrocks/libphonenumber/android/h$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/michaelrocks/libphonenumber/android/h$c;

.field public static final enum b:Lio/michaelrocks/libphonenumber/android/h$c;

.field public static final enum c:Lio/michaelrocks/libphonenumber/android/h$c;

.field public static final enum d:Lio/michaelrocks/libphonenumber/android/h$c;

.field public static final enum e:Lio/michaelrocks/libphonenumber/android/h$c;

.field public static final enum f:Lio/michaelrocks/libphonenumber/android/h$c;

.field private static final synthetic g:[Lio/michaelrocks/libphonenumber/android/h$c;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 449
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$c;

    const-string v1, "IS_POSSIBLE"

    invoke-direct {v0, v1, v3}, Lio/michaelrocks/libphonenumber/android/h$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->a:Lio/michaelrocks/libphonenumber/android/h$c;

    .line 455
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$c;

    const-string v1, "IS_POSSIBLE_LOCAL_ONLY"

    invoke-direct {v0, v1, v4}, Lio/michaelrocks/libphonenumber/android/h$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->b:Lio/michaelrocks/libphonenumber/android/h$c;

    .line 457
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$c;

    const-string v1, "INVALID_COUNTRY_CODE"

    invoke-direct {v0, v1, v5}, Lio/michaelrocks/libphonenumber/android/h$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->c:Lio/michaelrocks/libphonenumber/android/h$c;

    .line 459
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$c;

    const-string v1, "TOO_SHORT"

    invoke-direct {v0, v1, v6}, Lio/michaelrocks/libphonenumber/android/h$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->d:Lio/michaelrocks/libphonenumber/android/h$c;

    .line 467
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$c;

    const-string v1, "INVALID_LENGTH"

    invoke-direct {v0, v1, v7}, Lio/michaelrocks/libphonenumber/android/h$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->e:Lio/michaelrocks/libphonenumber/android/h$c;

    .line 469
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$c;

    const-string v1, "TOO_LONG"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->f:Lio/michaelrocks/libphonenumber/android/h$c;

    .line 447
    const/4 v0, 0x6

    new-array v0, v0, [Lio/michaelrocks/libphonenumber/android/h$c;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$c;->a:Lio/michaelrocks/libphonenumber/android/h$c;

    aput-object v1, v0, v3

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$c;->b:Lio/michaelrocks/libphonenumber/android/h$c;

    aput-object v1, v0, v4

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$c;->c:Lio/michaelrocks/libphonenumber/android/h$c;

    aput-object v1, v0, v5

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$c;->d:Lio/michaelrocks/libphonenumber/android/h$c;

    aput-object v1, v0, v6

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$c;->e:Lio/michaelrocks/libphonenumber/android/h$c;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$c;->f:Lio/michaelrocks/libphonenumber/android/h$c;

    aput-object v2, v0, v1

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->g:[Lio/michaelrocks/libphonenumber/android/h$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 447
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/h$c;
    .locals 1

    .prologue
    .line 447
    const-class v0, Lio/michaelrocks/libphonenumber/android/h$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/michaelrocks/libphonenumber/android/h$c;

    return-object v0
.end method

.method public static values()[Lio/michaelrocks/libphonenumber/android/h$c;
    .locals 1

    .prologue
    .line 447
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->g:[Lio/michaelrocks/libphonenumber/android/h$c;

    invoke-virtual {v0}, [Lio/michaelrocks/libphonenumber/android/h$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/michaelrocks/libphonenumber/android/h$c;

    return-object v0
.end method
