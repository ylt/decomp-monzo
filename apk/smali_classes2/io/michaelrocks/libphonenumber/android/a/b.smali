.class public final Lio/michaelrocks/libphonenumber/android/a/b;
.super Ljava/lang/Object;
.source "RegexBasedMatcher.java"

# interfaces
.implements Lio/michaelrocks/libphonenumber/android/a/a;


# instance fields
.field private final a:Lio/michaelrocks/libphonenumber/android/a/c;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lio/michaelrocks/libphonenumber/android/a/c;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lio/michaelrocks/libphonenumber/android/a/c;-><init>(I)V

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/a/b;->a:Lio/michaelrocks/libphonenumber/android/a/c;

    .line 36
    return-void
.end method

.method public static a()Lio/michaelrocks/libphonenumber/android/a/a;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lio/michaelrocks/libphonenumber/android/a/b;

    invoke-direct {v0}, Lio/michaelrocks/libphonenumber/android/a/b;-><init>()V

    return-object v0
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;Z)Z
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v1

    if-nez v1, :cond_1

    .line 53
    const/4 p2, 0x0

    .line 55
    :cond_0
    :goto_0
    return p2

    :cond_1
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$d;Z)Z
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$d;->a()Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 47
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lio/michaelrocks/libphonenumber/android/a/b;->a:Lio/michaelrocks/libphonenumber/android/a/c;

    invoke-virtual {v1, v0}, Lio/michaelrocks/libphonenumber/android/a/c;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-static {p1, v0, p3}, Lio/michaelrocks/libphonenumber/android/a/b;->a(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;Z)Z

    move-result v0

    goto :goto_0
.end method
