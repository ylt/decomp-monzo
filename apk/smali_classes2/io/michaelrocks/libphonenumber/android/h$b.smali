.class public final enum Lio/michaelrocks/libphonenumber/android/h$b;
.super Ljava/lang/Enum;
.source "PhoneNumberUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/michaelrocks/libphonenumber/android/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/michaelrocks/libphonenumber/android/h$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum b:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum c:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum d:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum e:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum f:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum g:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum h:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum i:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum j:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum k:Lio/michaelrocks/libphonenumber/android/h$b;

.field public static final enum l:Lio/michaelrocks/libphonenumber/android/h$b;

.field private static final synthetic m:[Lio/michaelrocks/libphonenumber/android/h$b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 404
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "FIXED_LINE"

    invoke-direct {v0, v1, v3}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->a:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 405
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v4}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->b:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 408
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "FIXED_LINE_OR_MOBILE"

    invoke-direct {v0, v1, v5}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->c:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 410
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "TOLL_FREE"

    invoke-direct {v0, v1, v6}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->d:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 411
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "PREMIUM_RATE"

    invoke-direct {v0, v1, v7}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->e:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 415
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "SHARED_COST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->f:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 417
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "VOIP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->g:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 421
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "PERSONAL_NUMBER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->h:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 422
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "PAGER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->i:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 425
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "UAN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->j:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 427
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "VOICEMAIL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->k:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 430
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$b;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->l:Lio/michaelrocks/libphonenumber/android/h$b;

    .line 403
    const/16 v0, 0xc

    new-array v0, v0, [Lio/michaelrocks/libphonenumber/android/h$b;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$b;->a:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v1, v0, v3

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$b;->b:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v1, v0, v4

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$b;->c:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v1, v0, v5

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$b;->d:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v1, v0, v6

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$b;->e:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->f:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->g:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->h:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->i:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->j:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->k:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->l:Lio/michaelrocks/libphonenumber/android/h$b;

    aput-object v2, v0, v1

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->m:[Lio/michaelrocks/libphonenumber/android/h$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 403
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/h$b;
    .locals 1

    .prologue
    .line 403
    const-class v0, Lio/michaelrocks/libphonenumber/android/h$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/michaelrocks/libphonenumber/android/h$b;

    return-object v0
.end method

.method public static values()[Lio/michaelrocks/libphonenumber/android/h$b;
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->m:[Lio/michaelrocks/libphonenumber/android/h$b;

    invoke-virtual {v0}, [Lio/michaelrocks/libphonenumber/android/h$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/michaelrocks/libphonenumber/android/h$b;

    return-object v0
.end method
