.class public Lio/michaelrocks/libphonenumber/android/h;
.super Ljava/lang/Object;
.source "PhoneNumberUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/michaelrocks/libphonenumber/android/h$c;,
        Lio/michaelrocks/libphonenumber/android/h$b;,
        Lio/michaelrocks/libphonenumber/android/h$a;
    }
.end annotation


# static fields
.field static final a:Ljava/util/regex/Pattern;

.field static final b:Ljava/util/regex/Pattern;

.field static final c:Ljava/util/regex/Pattern;

.field static final d:Ljava/lang/String;

.field static final e:Ljava/util/regex/Pattern;

.field private static final f:Ljava/util/logging/Logger;

.field private static final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private static final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private static final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Ljava/util/regex/Pattern;

.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/util/regex/Pattern;

.field private static final q:Ljava/util/regex/Pattern;

.field private static final r:Ljava/util/regex/Pattern;

.field private static final s:Ljava/util/regex/Pattern;

.field private static final t:Ljava/lang/String;

.field private static final u:Ljava/lang/String;

.field private static final v:Ljava/util/regex/Pattern;

.field private static final w:Ljava/util/regex/Pattern;

.field private static final x:Ljava/util/regex/Pattern;

.field private static final y:Ljava/util/regex/Pattern;


# instance fields
.field private final A:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final B:Lio/michaelrocks/libphonenumber/android/a/a;

.field private final C:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final D:Lio/michaelrocks/libphonenumber/android/a/c;

.field private final E:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final F:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Lio/michaelrocks/libphonenumber/android/f;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x39

    const/16 v7, 0x37

    const/16 v5, 0x36

    const/16 v4, 0x34

    const/16 v6, 0x2d

    .line 59
    const-class v0, Lio/michaelrocks/libphonenumber/android/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->f:Ljava/util/logging/Logger;

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 127
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->g:Ljava/util/Map;

    .line 131
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 132
    const/16 v1, 0x56

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 134
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lio/michaelrocks/libphonenumber/android/h;->h:Ljava/util/Set;

    .line 136
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 137
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 140
    const/16 v2, 0x3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 141
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 142
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->i:Ljava/util/Set;

    .line 146
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 147
    const/16 v0, 0x30

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    const/16 v0, 0x31

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x31

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const/16 v0, 0x33

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x33

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const/16 v0, 0x35

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x35

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    const/16 v0, 0x38

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x38

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    new-instance v0, Ljava/util/HashMap;

    const/16 v2, 0x28

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 159
    const/16 v2, 0x41

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    const/16 v2, 0x42

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    const/16 v2, 0x43

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    const/16 v2, 0x44

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x33

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    const/16 v2, 0x45

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x33

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x33

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    const/16 v2, 0x47

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    const/16 v2, 0x48

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    const/16 v2, 0x49

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    const/16 v2, 0x4a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x35

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    const/16 v2, 0x4b

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x35

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    const/16 v2, 0x4c

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x35

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    const/16 v2, 0x4d

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    const/16 v2, 0x4e

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    const/16 v2, 0x4f

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    const/16 v2, 0x50

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const/16 v2, 0x51

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const/16 v2, 0x52

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    const/16 v2, 0x53

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    const/16 v2, 0x54

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x38

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    const/16 v2, 0x55

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x38

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    const/16 v2, 0x56

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x38

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    const/16 v2, 0x57

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    const/16 v2, 0x58

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    const/16 v2, 0x59

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    const/16 v2, 0x5a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->k:Ljava/util/Map;

    .line 187
    new-instance v0, Ljava/util/HashMap;

    const/16 v2, 0x64

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 188
    sget-object v2, Lio/michaelrocks/libphonenumber/android/h;->k:Ljava/util/Map;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 189
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 190
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->l:Ljava/util/Map;

    .line 192
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 193
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 194
    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x2b

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x2a

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x23

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->j:Ljava/util/Map;

    .line 199
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 201
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 202
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 205
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 207
    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    const v0, 0xff0d

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    const/16 v0, 0x2010

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    const/16 v0, 0x2011

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    const/16 v0, 0x2012

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    const/16 v0, 0x2013

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    const/16 v0, 0x2014

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    const/16 v0, 0x2015

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    const/16 v0, 0x2212

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    const/16 v0, 0x2f

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    const v0, 0xff0f

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    const/16 v0, 0x20

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    const/16 v0, 0x3000

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    const/16 v0, 0x2060

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    const/16 v0, 0x2e

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    const v0, 0xff0e

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->m:Ljava/util/Map;

    .line 232
    const-string v0, "[\\d]+(?:[~\u2053\u223c\uff5e][\\d]+)?"

    .line 233
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->n:Ljava/util/regex/Pattern;

    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->k:Ljava/util/Map;

    .line 247
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[, \\[\\]]"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->k:Ljava/util/Map;

    .line 248
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 249
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[, \\[\\]]"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->o:Ljava/lang/String;

    .line 251
    const-string v0, "[+\uff0b]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->a:Ljava/util/regex/Pattern;

    .line 252
    const-string v0, "[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->p:Ljava/util/regex/Pattern;

    .line 253
    const-string v0, "(\\p{Nd})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->q:Ljava/util/regex/Pattern;

    .line 262
    const-string v0, "[+\uff0b\\p{Nd}]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->r:Ljava/util/regex/Pattern;

    .line 270
    const-string v0, "[\\\\/] *x"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->b:Ljava/util/regex/Pattern;

    .line 276
    const-string v0, "[[\\P{N}&&\\P{L}]&&[^#]]+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->c:Ljava/util/regex/Pattern;

    .line 280
    const-string v0, "(?:.*?[A-Za-z]){3}.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->s:Ljava/util/regex/Pattern;

    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\\p{Nd}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->t:Ljava/lang/String;

    .line 319
    const-string v0, "x\uff58#\uff03~\uff5e"

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 325
    invoke-static {v1}, Lio/michaelrocks/libphonenumber/android/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lio/michaelrocks/libphonenumber/android/h;->u:Ljava/lang/String;

    .line 326
    invoke-static {v0}, Lio/michaelrocks/libphonenumber/android/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->d:Ljava/lang/String;

    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")$"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x42

    .line 354
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->v:Ljava/util/regex/Pattern;

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x42

    .line 359
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->w:Ljava/util/regex/Pattern;

    .line 361
    const-string v0, "(\\D+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->e:Ljava/util/regex/Pattern;

    .line 367
    const-string v0, "(\\$\\d)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->x:Ljava/util/regex/Pattern;

    .line 377
    const-string v0, "\\(?\\$1\\)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h;->y:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Lio/michaelrocks/libphonenumber/android/f;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/michaelrocks/libphonenumber/android/f;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 588
    invoke-static {}, Lio/michaelrocks/libphonenumber/android/a/b;->a()Lio/michaelrocks/libphonenumber/android/a/a;

    move-result-object v0

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->B:Lio/michaelrocks/libphonenumber/android/a/a;

    .line 593
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x23

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->C:Ljava/util/Set;

    .line 598
    new-instance v0, Lio/michaelrocks/libphonenumber/android/a/c;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lio/michaelrocks/libphonenumber/android/a/c;-><init>(I)V

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->D:Lio/michaelrocks/libphonenumber/android/a/c;

    .line 603
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x140

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->E:Ljava/util/Set;

    .line 607
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->F:Ljava/util/Set;

    .line 615
    iput-object p1, p0, Lio/michaelrocks/libphonenumber/android/h;->z:Lio/michaelrocks/libphonenumber/android/f;

    .line 616
    iput-object p2, p0, Lio/michaelrocks/libphonenumber/android/h;->A:Ljava/util/Map;

    .line 617
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 618
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 621
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v5, :cond_0

    const-string v3, "001"

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 623
    iget-object v1, p0, Lio/michaelrocks/libphonenumber/android/h;->F:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 626
    :cond_0
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->E:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 632
    :cond_1
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->E:Ljava/util/Set;

    const-string v1, "001"

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 633
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h;->f:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 636
    :cond_2
    iget-object v1, p0, Lio/michaelrocks/libphonenumber/android/h;->C:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 637
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;)Lio/michaelrocks/libphonenumber/android/h$c;
    .locals 1

    .prologue
    .line 2500
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->l:Lio/michaelrocks/libphonenumber/android/h$b;

    invoke-direct {p0, p1, p2, v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$b;)Lio/michaelrocks/libphonenumber/android/h$c;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$b;)Lio/michaelrocks/libphonenumber/android/h$c;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2511
    invoke-virtual {p0, p2, p3}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$b;)Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v1

    .line 2518
    invoke-virtual {v1}, Lio/michaelrocks/libphonenumber/android/i$d;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2519
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->a()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    invoke-virtual {v0}, Lio/michaelrocks/libphonenumber/android/i$d;->b()Ljava/util/List;

    move-result-object v0

    .line 2521
    :goto_0
    invoke-virtual {v1}, Lio/michaelrocks/libphonenumber/android/i$d;->d()Ljava/util/List;

    move-result-object v1

    .line 2523
    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->c:Lio/michaelrocks/libphonenumber/android/h$b;

    if-ne p3, v2, :cond_a

    .line 2524
    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->a:Lio/michaelrocks/libphonenumber/android/h$b;

    invoke-virtual {p0, p2, v2}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$b;)Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v2

    invoke-static {v2}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/i$d;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2527
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$b;->b:Lio/michaelrocks/libphonenumber/android/h$b;

    invoke-direct {p0, p1, p2, v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$b;)Lio/michaelrocks/libphonenumber/android/h$c;

    move-result-object v0

    .line 2576
    :goto_1
    return-object v0

    .line 2519
    :cond_0
    invoke-virtual {v1}, Lio/michaelrocks/libphonenumber/android/i$d;->b()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 2529
    :cond_1
    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$b;->b:Lio/michaelrocks/libphonenumber/android/h$b;

    invoke-virtual {p0, p2, v2}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$b;)Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v3

    .line 2530
    invoke-static {v3}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/i$d;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2532
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2536
    invoke-virtual {v3}, Lio/michaelrocks/libphonenumber/android/i$d;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2537
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->a()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    invoke-virtual {v0}, Lio/michaelrocks/libphonenumber/android/i$d;->b()Ljava/util/List;

    move-result-object v0

    .line 2536
    :goto_2
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2541
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2543
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2544
    invoke-virtual {v3}, Lio/michaelrocks/libphonenumber/android/i$d;->d()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 2556
    :goto_3
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_4

    .line 2557
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->e:Lio/michaelrocks/libphonenumber/android/h$c;

    goto :goto_1

    .line 2538
    :cond_2
    invoke-virtual {v3}, Lio/michaelrocks/libphonenumber/android/i$d;->b()Ljava/util/List;

    move-result-object v0

    goto :goto_2

    .line 2546
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2547
    invoke-virtual {v3}, Lio/michaelrocks/libphonenumber/android/i$d;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2548
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object v1, v0

    goto :goto_3

    .line 2560
    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 2563
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2564
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->b:Lio/michaelrocks/libphonenumber/android/h$c;

    goto :goto_1

    .line 2567
    :cond_5
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2568
    if-ne v0, v3, :cond_6

    .line 2569
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->a:Lio/michaelrocks/libphonenumber/android/h$c;

    goto :goto_1

    .line 2570
    :cond_6
    if-le v0, v3, :cond_7

    .line 2571
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->d:Lio/michaelrocks/libphonenumber/android/h$c;

    goto/16 :goto_1

    .line 2572
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, v3, :cond_8

    .line 2573
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->f:Lio/michaelrocks/libphonenumber/android/h$c;

    goto/16 :goto_1

    .line 2576
    :cond_8
    const/4 v0, 0x1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->a:Lio/michaelrocks/libphonenumber/android/h$c;

    goto/16 :goto_1

    :cond_9
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$c;->e:Lio/michaelrocks/libphonenumber/android/h$c;

    goto/16 :goto_1

    :cond_a
    move-object v2, v0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;)Lio/michaelrocks/libphonenumber/android/h;
    .locals 2

    .prologue
    .line 1087
    if-nez p0, :cond_0

    .line 1088
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context could not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1090
    :cond_0
    new-instance v0, Lio/michaelrocks/libphonenumber/android/b;

    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/michaelrocks/libphonenumber/android/b;-><init>(Landroid/content/res/AssetManager;)V

    invoke-static {v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/d;)Lio/michaelrocks/libphonenumber/android/h;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/michaelrocks/libphonenumber/android/d;)Lio/michaelrocks/libphonenumber/android/h;
    .locals 2

    .prologue
    .line 1126
    if-nez p0, :cond_0

    .line 1127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "metadataLoader could not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1129
    :cond_0
    new-instance v0, Lio/michaelrocks/libphonenumber/android/g;

    invoke-direct {v0, p0}, Lio/michaelrocks/libphonenumber/android/g;-><init>(Lio/michaelrocks/libphonenumber/android/d;)V

    invoke-static {v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/f;)Lio/michaelrocks/libphonenumber/android/h;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lio/michaelrocks/libphonenumber/android/f;)Lio/michaelrocks/libphonenumber/android/h;
    .locals 2

    .prologue
    .line 1106
    if-nez p0, :cond_0

    .line 1107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "metadataSource could not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1109
    :cond_0
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h;

    .line 1110
    invoke-static {}, Lio/michaelrocks/libphonenumber/android/c;->a()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lio/michaelrocks/libphonenumber/android/h;-><init>(Lio/michaelrocks/libphonenumber/android/f;Ljava/util/Map;)V

    .line 1109
    return-object v0
.end method

.method private a(ILjava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;
    .locals 1

    .prologue
    .line 1340
    const-string v0, "001"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1341
    invoke-virtual {p0, p1}, Lio/michaelrocks/libphonenumber/android/h;->a(I)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v0

    .line 1340
    :goto_0
    return-object v0

    .line 1342
    :cond_0
    invoke-virtual {p0, p2}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 670
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h;->r:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 671
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 672
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 674
    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 675
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 676
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-interface {v0, v5, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 677
    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stripped trailing characters: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 680
    :cond_0
    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 681
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 682
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-interface {v0, v5, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 686
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/util/Map;Z)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Ljava/lang/Character;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 948
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 949
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 950
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 951
    invoke-static {v3}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 952
    if-eqz v0, :cond_1

    .line 953
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 949
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 954
    :cond_1
    if-nez p2, :cond_0

    .line 955
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 959
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/i$a;Lio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1904
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$a;->b()Ljava/lang/String;

    move-result-object v0

    .line 1905
    iget-object v1, p0, Lio/michaelrocks/libphonenumber/android/h;->D:Lio/michaelrocks/libphonenumber/android/a/c;

    .line 1906
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/michaelrocks/libphonenumber/android/a/c;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1907
    const-string v2, ""

    .line 1908
    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$a;->c:Lio/michaelrocks/libphonenumber/android/h$a;

    if-ne p3, v2, :cond_2

    if-eqz p4, :cond_2

    .line 1909
    invoke-interface {p4}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 1910
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 1912
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$a;->e()Ljava/lang/String;

    move-result-object v2

    .line 1913
    const-string v3, "$CC"

    invoke-virtual {v2, v3, p4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 1916
    sget-object v3, Lio/michaelrocks/libphonenumber/android/h;->x:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1917
    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1918
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1932
    :goto_0
    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$a;->d:Lio/michaelrocks/libphonenumber/android/h$a;

    if-ne p3, v1, :cond_1

    .line 1934
    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->p:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1935
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1936
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1939
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1941
    :cond_1
    return-object v0

    .line 1921
    :cond_2
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$a;->d()Ljava/lang/String;

    move-result-object v2

    .line 1922
    sget-object v3, Lio/michaelrocks/libphonenumber/android/h$a;->c:Lio/michaelrocks/libphonenumber/android/h$a;

    if-ne p3, v3, :cond_3

    if-eqz v2, :cond_3

    .line 1924
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 1925
    sget-object v3, Lio/michaelrocks/libphonenumber/android/h;->x:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1927
    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1929
    :cond_3
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1851
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->t()Ljava/util/List;

    move-result-object v0

    .line 1866
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$a;->c:Lio/michaelrocks/libphonenumber/android/h$a;

    if-ne p3, v0, :cond_1

    .line 1867
    :cond_0
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->r()Ljava/util/List;

    move-result-object v0

    .line 1869
    :goto_0
    invoke-virtual {p0, v0, p1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/util/List;Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$a;

    move-result-object v0

    .line 1870
    if-nez v0, :cond_2

    :goto_1
    return-object p1

    .line 1868
    :cond_1
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->t()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1872
    :cond_2
    invoke-direct {p0, p1, v0, p3, p4}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/i$a;Lio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method static a(Ljava/lang/CharSequence;Z)Ljava/lang/StringBuilder;
    .locals 5

    .prologue
    .line 747
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 748
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 749
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 750
    const/16 v3, 0xa

    invoke-static {v2, v3}, Ljava/lang/Character;->digit(CI)I

    move-result v3

    .line 751
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 752
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 748
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 753
    :cond_1
    if-eqz p1, :cond_0

    .line 754
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 757
    :cond_2
    return-object v1
.end method

.method static a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 726
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h;->s:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 727
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 728
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h;->l:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Ljava/util/Map;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v0, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 732
    :goto_0
    return-object p0

    .line 730
    :cond_0
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-static {p0}, Lio/michaelrocks/libphonenumber/android/h;->c(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v0, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private a(ILio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    const/16 v3, 0x2b

    const/4 v2, 0x0

    .line 1832
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$1;->b:[I

    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/h$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1845
    :goto_0
    return-void

    .line 1834
    :pswitch_0
    invoke-virtual {p3, v2, p1}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1837
    :pswitch_1
    const-string v0, " "

    invoke-virtual {p3, v2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, p1}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1840
    :pswitch_2
    const-string v0, "-"

    invoke-virtual {p3, v2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, p1}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "tel:"

    .line 1841
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1832
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lio/michaelrocks/libphonenumber/android/j$a;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 2110
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2111
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$a;->d:Lio/michaelrocks/libphonenumber/android/h$a;

    if-ne p3, v0, :cond_1

    .line 2112
    const-string v0, ";ext="

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2121
    :cond_0
    :goto_0
    return-void

    .line 2114
    :cond_1
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2115
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2117
    :cond_2
    const-string v0, " ext. "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method static a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/j$a;)V
    .locals 4

    .prologue
    const/16 v3, 0x30

    const/4 v1, 0x1

    .line 3123
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-ne v0, v3, :cond_1

    .line 3124
    invoke-virtual {p1, v1}, Lio/michaelrocks/libphonenumber/android/j$a;->a(Z)Lio/michaelrocks/libphonenumber/android/j$a;

    move v0, v1

    .line 3128
    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 3129
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_0

    .line 3130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3132
    :cond_0
    if-eq v0, v1, :cond_1

    .line 3133
    invoke-virtual {p1, v0}, Lio/michaelrocks/libphonenumber/android/j$a;->b(I)Lio/michaelrocks/libphonenumber/android/j$a;

    .line 3136
    :cond_1
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljava/lang/String;ZZLio/michaelrocks/libphonenumber/android/j$a;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/michaelrocks/libphonenumber/android/NumberParseException;
        }
    .end annotation

    .prologue
    .line 3150
    if-nez p1, :cond_0

    .line 3151
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->b:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "The phone number supplied was null."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 3153
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0xfa

    if-le v0, v1, :cond_1

    .line 3154
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->e:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "The string supplied was too long to parse."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 3158
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 3159
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3160
    invoke-direct {p0, v0, v1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 3162
    invoke-static {v1}, Lio/michaelrocks/libphonenumber/android/h;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3163
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->b:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "The string supplied did not seem to be a phone number."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 3169
    :cond_2
    if-eqz p4, :cond_3

    invoke-direct {p0, v1, p2}, Lio/michaelrocks/libphonenumber/android/h;->b(Ljava/lang/CharSequence;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3170
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->a:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "Missing or invalid default region."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 3174
    :cond_3
    if-eqz p3, :cond_4

    .line 3175
    invoke-virtual {p5, v0}, Lio/michaelrocks/libphonenumber/android/j$a;->b(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a;

    .line 3179
    :cond_4
    invoke-virtual {p0, v1}, Lio/michaelrocks/libphonenumber/android/h;->b(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    .line 3180
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 3181
    invoke-virtual {p5, v0}, Lio/michaelrocks/libphonenumber/android/j$a;->a(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a;

    .line 3184
    :cond_5
    invoke-virtual {p0, p2}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v2

    .line 3187
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, p0

    move v4, p3

    move-object v5, p5

    .line 3193
    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;Ljava/lang/StringBuilder;ZLio/michaelrocks/libphonenumber/android/j$a;)I
    :try_end_0
    .catch Lio/michaelrocks/libphonenumber/android/NumberParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 3211
    :cond_6
    if-eqz v0, :cond_9

    .line 3212
    invoke-virtual {p0, v0}, Lio/michaelrocks/libphonenumber/android/h;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 3213
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 3215
    invoke-direct {p0, v0, v1}, Lio/michaelrocks/libphonenumber/android/h;->a(ILjava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v2

    .line 3228
    :cond_7
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_b

    .line 3229
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->d:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "The string supplied is too short to be a phone number."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 3195
    :catch_0
    move-exception v0

    .line 3196
    sget-object v4, Lio/michaelrocks/libphonenumber/android/h;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 3197
    invoke-virtual {v0}, Lio/michaelrocks/libphonenumber/android/NumberParseException;->a()Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    move-result-object v5

    sget-object v6, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->a:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    if-ne v5, v6, :cond_8

    .line 3198
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 3200
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v5

    move-object v4, p0

    move-object v6, v2

    move-object v7, v3

    move v8, p3

    move-object v9, p5

    invoke-virtual/range {v4 .. v9}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;Ljava/lang/StringBuilder;ZLio/michaelrocks/libphonenumber/android/j$a;)I

    move-result v0

    .line 3203
    if-nez v0, :cond_6

    .line 3204
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->a:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "Could not interpret numbers after plus-sign."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 3208
    :cond_8
    new-instance v1, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    invoke-virtual {v0}, Lio/michaelrocks/libphonenumber/android/NumberParseException;->a()Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    move-result-object v2

    invoke-virtual {v0}, Lio/michaelrocks/libphonenumber/android/NumberParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v1

    .line 3220
    :cond_9
    invoke-static {v1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 3221
    if-eqz p2, :cond_a

    .line 3222
    invoke-virtual {v2}, Lio/michaelrocks/libphonenumber/android/i$b;->l()I

    move-result v0

    .line 3223
    invoke-virtual {p5, v0}, Lio/michaelrocks/libphonenumber/android/j$a;->a(I)Lio/michaelrocks/libphonenumber/android/j$a;

    goto :goto_0

    .line 3224
    :cond_a
    if-eqz p3, :cond_7

    .line 3225
    invoke-virtual {p5}, Lio/michaelrocks/libphonenumber/android/j$a;->m()Lio/michaelrocks/libphonenumber/android/j$a;

    goto :goto_0

    .line 3232
    :cond_b
    if-eqz v2, :cond_d

    .line 3233
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 3234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 3235
    invoke-virtual {p0, v0, v2, v1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/StringBuilder;Lio/michaelrocks/libphonenumber/android/i$b;Ljava/lang/StringBuilder;)Z

    .line 3239
    invoke-direct {p0, v0, v2}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;)Lio/michaelrocks/libphonenumber/android/h$c;

    move-result-object v2

    sget-object v4, Lio/michaelrocks/libphonenumber/android/h$c;->d:Lio/michaelrocks/libphonenumber/android/h$c;

    if-eq v2, v4, :cond_d

    .line 3241
    if-eqz p3, :cond_c

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_c

    .line 3242
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p5, v1}, Lio/michaelrocks/libphonenumber/android/j$a;->c(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a;

    :cond_c
    move-object v3, v0

    .line 3246
    :cond_d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 3247
    const/4 v1, 0x2

    if-ge v0, v1, :cond_e

    .line 3248
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->d:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "The string supplied is too short to be a phone number."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 3251
    :cond_e
    const/16 v1, 0x11

    if-le v0, v1, :cond_f

    .line 3252
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->e:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "The string supplied is too long to be a phone number."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 3255
    :cond_f
    invoke-static {v3, p5}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/j$a;)V

    .line 3256
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p5, v0, v1}, Lio/michaelrocks/libphonenumber/android/j$a;->a(J)Lio/michaelrocks/libphonenumber/android/j$a;

    .line 3257
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    .line 3264
    const-string v0, ";phone-context="

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 3265
    if-ltz v1, :cond_4

    .line 3266
    const-string v0, ";phone-context="

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    .line 3269
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 3270
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2b

    if-ne v2, v3, :cond_0

    .line 3274
    const/16 v2, 0x3b

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 3275
    if-lez v2, :cond_2

    .line 3276
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3286
    :cond_0
    :goto_0
    const-string v0, "tel:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 3287
    if-ltz v0, :cond_3

    const-string v2, "tel:"

    .line 3288
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    .line 3289
    :goto_1
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3298
    :goto_2
    const-string v0, ";isub="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 3299
    if-lez v0, :cond_1

    .line 3300
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 3306
    :cond_1
    return-void

    .line 3278
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3288
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 3293
    :cond_4
    invoke-static {p1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private static a(Lio/michaelrocks/libphonenumber/android/i$d;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1001
    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/i$d;->c()I

    move-result v2

    if-ne v2, v1, :cond_0

    invoke-virtual {p0, v0}, Lio/michaelrocks/libphonenumber/android/i$d;->a(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method private a(Ljava/util/regex/Pattern;Ljava/lang/StringBuilder;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2840
    invoke-virtual {p1, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 2841
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2842
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    .line 2845
    sget-object v3, Lio/michaelrocks/libphonenumber/android/h;->q:Ljava/util/regex/Pattern;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 2846
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2847
    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lio/michaelrocks/libphonenumber/android/h;->c(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 2848
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2855
    :cond_0
    :goto_0
    return v0

    .line 2852
    :cond_1
    invoke-virtual {p2, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move v0, v1

    .line 2853
    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ";ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]|int|anexo|\uff49\uff4e\uff54)[:\\.\uff0e]?[ \u00a0\\t,-]*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(\\p{Nd}{1,7})"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#?|[- ]+("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\\p{Nd}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{1,5})#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/lang/CharSequence;)Z
    .locals 2

    .prologue
    .line 702
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 703
    const/4 v0, 0x0

    .line 706
    :goto_0
    return v0

    .line 705
    :cond_0
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h;->w:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 706
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method

.method private b(Ljava/lang/CharSequence;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2987
    invoke-direct {p0, p2}, Lio/michaelrocks/libphonenumber/android/h;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2989
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lio/michaelrocks/libphonenumber/android/h;->a:Ljava/util/regex/Pattern;

    .line 2990
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2991
    :cond_0
    const/4 v0, 0x0

    .line 2994
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 743
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(I)Z
    .locals 2

    .prologue
    .line 1172
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->A:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1165
    if-eqz p1, :cond_0

    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->E:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;Ljava/lang/StringBuilder;ZLio/michaelrocks/libphonenumber/android/j$a;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/michaelrocks/libphonenumber/android/NumberParseException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2771
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2832
    :goto_0
    return v0

    .line 2774
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2776
    const-string v0, "NonMatch"

    .line 2777
    if-eqz p2, :cond_1

    .line 2778
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->m()Ljava/lang/String;

    move-result-object v0

    .line 2782
    :cond_1
    invoke-virtual {p0, v2, v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a$a;

    move-result-object v0

    .line 2783
    if-eqz p4, :cond_2

    .line 2784
    invoke-virtual {p5, v0}, Lio/michaelrocks/libphonenumber/android/j$a;->a(Lio/michaelrocks/libphonenumber/android/j$a$a;)Lio/michaelrocks/libphonenumber/android/j$a;

    .line 2786
    :cond_2
    sget-object v3, Lio/michaelrocks/libphonenumber/android/j$a$a;->d:Lio/michaelrocks/libphonenumber/android/j$a$a;

    if-eq v0, v3, :cond_5

    .line 2787
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_3

    .line 2788
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->c:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "Phone number had an IDD, but after this was not long enough to be a viable phone number."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 2792
    :cond_3
    invoke-virtual {p0, v2, p3}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)I

    move-result v0

    .line 2793
    if-eqz v0, :cond_4

    .line 2794
    invoke-virtual {p5, v0}, Lio/michaelrocks/libphonenumber/android/j$a;->a(I)Lio/michaelrocks/libphonenumber/android/j$a;

    goto :goto_0

    .line 2800
    :cond_4
    new-instance v0, Lio/michaelrocks/libphonenumber/android/NumberParseException;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/NumberParseException$a;->a:Lio/michaelrocks/libphonenumber/android/NumberParseException$a;

    const-string v2, "Country calling code supplied was not recognised."

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/NumberParseException;-><init>(Lio/michaelrocks/libphonenumber/android/NumberParseException$a;Ljava/lang/String;)V

    throw v0

    .line 2802
    :cond_5
    if-eqz p2, :cond_9

    .line 2806
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->l()I

    move-result v0

    .line 2807
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 2808
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2809
    invoke-virtual {v4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2810
    new-instance v5, Ljava/lang/StringBuilder;

    .line 2811
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2812
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->a()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v3

    .line 2813
    const/4 v4, 0x0

    invoke-virtual {p0, v5, p2, v4}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/StringBuilder;Lio/michaelrocks/libphonenumber/android/i$b;Ljava/lang/StringBuilder;)Z

    .line 2818
    iget-object v4, p0, Lio/michaelrocks/libphonenumber/android/h;->B:Lio/michaelrocks/libphonenumber/android/a/a;

    invoke-interface {v4, v2, v3, v1}, Lio/michaelrocks/libphonenumber/android/a/a;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$d;Z)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lio/michaelrocks/libphonenumber/android/h;->B:Lio/michaelrocks/libphonenumber/android/a/a;

    .line 2819
    invoke-interface {v4, v5, v3, v1}, Lio/michaelrocks/libphonenumber/android/a/a;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$d;Z)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2820
    :cond_6
    invoke-direct {p0, v2, p2}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$b;)Lio/michaelrocks/libphonenumber/android/h$c;

    move-result-object v2

    sget-object v3, Lio/michaelrocks/libphonenumber/android/h$c;->f:Lio/michaelrocks/libphonenumber/android/h$c;

    if-ne v2, v3, :cond_9

    .line 2821
    :cond_7
    invoke-virtual {p3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 2822
    if-eqz p4, :cond_8

    .line 2823
    sget-object v1, Lio/michaelrocks/libphonenumber/android/j$a$a;->c:Lio/michaelrocks/libphonenumber/android/j$a$a;

    invoke-virtual {p5, v1}, Lio/michaelrocks/libphonenumber/android/j$a;->a(Lio/michaelrocks/libphonenumber/android/j$a$a;)Lio/michaelrocks/libphonenumber/android/j$a;

    .line 2825
    :cond_8
    invoke-virtual {p5, v0}, Lio/michaelrocks/libphonenumber/android/j$a;->a(I)Lio/michaelrocks/libphonenumber/android/j$a;

    goto/16 :goto_0

    .line 2831
    :cond_9
    invoke-virtual {p5, v1}, Lio/michaelrocks/libphonenumber/android/j$a;->a(I)Lio/michaelrocks/libphonenumber/android/j$a;

    move v0, v1

    .line 2832
    goto/16 :goto_0
.end method

.method a(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2719
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v2, 0x30

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 2732
    :goto_0
    return v0

    .line 2724
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    .line 2725
    const/4 v0, 0x1

    move v2, v0

    :goto_1
    const/4 v0, 0x3

    if-gt v2, v0, :cond_3

    if-gt v2, v3, :cond_3

    .line 2726
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2727
    iget-object v4, p0, Lio/michaelrocks/libphonenumber/android/h;->A:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2728
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2725
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2732
    goto :goto_0
.end method

.method a(Ljava/util/List;Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lio/michaelrocks/libphonenumber/android/i$a;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lio/michaelrocks/libphonenumber/android/i$a;"
        }
    .end annotation

    .prologue
    .line 1877
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/michaelrocks/libphonenumber/android/i$a;

    .line 1878
    invoke-virtual {v0}, Lio/michaelrocks/libphonenumber/android/i$a;->c()I

    move-result v2

    .line 1879
    if-eqz v2, :cond_1

    iget-object v3, p0, Lio/michaelrocks/libphonenumber/android/h;->D:Lio/michaelrocks/libphonenumber/android/a/c;

    add-int/lit8 v2, v2, -0x1

    .line 1881
    invoke-virtual {v0, v2}, Lio/michaelrocks/libphonenumber/android/i$a;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 1879
    invoke-virtual {v3, v2}, Lio/michaelrocks/libphonenumber/android/a/c;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 1881
    invoke-virtual {v2, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1882
    :cond_1
    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/h;->D:Lio/michaelrocks/libphonenumber/android/a/c;

    invoke-virtual {v0}, Lio/michaelrocks/libphonenumber/android/i$a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/michaelrocks/libphonenumber/android/a/c;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1883
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1888
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(I)Lio/michaelrocks/libphonenumber/android/i$b;
    .locals 2

    .prologue
    .line 2227
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->A:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2228
    const/4 v0, 0x0

    .line 2230
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->z:Lio/michaelrocks/libphonenumber/android/f;

    invoke-interface {v0, p1}, Lio/michaelrocks/libphonenumber/android/f;->a(I)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v0

    goto :goto_0
.end method

.method a(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;
    .locals 1

    .prologue
    .line 2220
    invoke-direct {p0, p1}, Lio/michaelrocks/libphonenumber/android/h;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2221
    const/4 v0, 0x0

    .line 2223
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->z:Lio/michaelrocks/libphonenumber/android/f;

    invoke-interface {v0, p1}, Lio/michaelrocks/libphonenumber/android/f;->a(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v0

    goto :goto_0
.end method

.method a(Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$b;)Lio/michaelrocks/libphonenumber/android/i$d;
    .locals 2

    .prologue
    .line 2124
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$1;->c:[I

    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/h$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2147
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->a()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2126
    :pswitch_0
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->e()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2128
    :pswitch_1
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->d()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2130
    :pswitch_2
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->c()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2133
    :pswitch_3
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->b()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2135
    :pswitch_4
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->f()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2137
    :pswitch_5
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->h()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2139
    :pswitch_6
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->g()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2141
    :pswitch_7
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->i()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2143
    :pswitch_8
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->j()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2145
    :pswitch_9
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/i$b;->k()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v0

    goto :goto_0

    .line 2124
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method a(Ljava/lang/StringBuilder;Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a$a;
    .locals 2

    .prologue
    .line 2874
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 2875
    sget-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->d:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 2888
    :goto_0
    return-object v0

    .line 2878
    :cond_0
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 2879
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2880
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 2882
    invoke-static {p1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 2883
    sget-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->a:Lio/michaelrocks/libphonenumber/android/j$a$a;

    goto :goto_0

    .line 2886
    :cond_1
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->D:Lio/michaelrocks/libphonenumber/android/a/c;

    invoke-virtual {v0, p2}, Lio/michaelrocks/libphonenumber/android/a/c;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 2887
    invoke-static {p1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 2888
    invoke-direct {p0, v0, p1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/util/regex/Pattern;Ljava/lang/StringBuilder;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->b:Lio/michaelrocks/libphonenumber/android/j$a$a;

    goto :goto_0

    :cond_2
    sget-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->d:Lio/michaelrocks/libphonenumber/android/j$a$a;

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/michaelrocks/libphonenumber/android/NumberParseException;
        }
    .end annotation

    .prologue
    .line 3032
    new-instance v0, Lio/michaelrocks/libphonenumber/android/j$a;

    invoke-direct {v0}, Lio/michaelrocks/libphonenumber/android/j$a;-><init>()V

    .line 3033
    invoke-virtual {p0, p1, p2, v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/j$a;)V

    .line 3034
    return-object v0
.end method

.method public a(Lio/michaelrocks/libphonenumber/android/j$a;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1816
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1817
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->h()I

    move-result v1

    if-lez v1, :cond_0

    .line 1818
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->h()I

    move-result v1

    new-array v1, v1, [C

    .line 1819
    const/16 v2, 0x30

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([CC)V

    .line 1820
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1822
    :cond_0
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1823
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lio/michaelrocks/libphonenumber/android/j$a;Lio/michaelrocks/libphonenumber/android/h$a;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1190
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1196
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->j()Ljava/lang/String;

    move-result-object v0

    .line 1197
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 1203
    :goto_0
    return-object v0

    .line 1201
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1202
    invoke-virtual {p0, p1, p2, v0}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/j$a;Lio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/StringBuilder;)V

    .line 1203
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lio/michaelrocks/libphonenumber/android/j$a;Lio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 1213
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1214
    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->a()I

    move-result v0

    .line 1215
    invoke-virtual {p0, p1}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/j$a;)Ljava/lang/String;

    move-result-object v1

    .line 1217
    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$a;->a:Lio/michaelrocks/libphonenumber/android/h$a;

    if-ne p2, v2, :cond_0

    .line 1220
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1221
    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$a;->a:Lio/michaelrocks/libphonenumber/android/h$a;

    invoke-direct {p0, v0, v1, p3}, Lio/michaelrocks/libphonenumber/android/h;->a(ILio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/StringBuilder;)V

    .line 1240
    :goto_0
    return-void

    .line 1225
    :cond_0
    invoke-direct {p0, v0}, Lio/michaelrocks/libphonenumber/android/h;->c(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1226
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1232
    :cond_1
    invoke-virtual {p0, v0}, Lio/michaelrocks/libphonenumber/android/h;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 1236
    invoke-direct {p0, v0, v2}, Lio/michaelrocks/libphonenumber/android/h;->a(ILjava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v2

    .line 1237
    invoke-direct {p0, v1, v2, p2}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1238
    invoke-direct {p0, p1, v2, p2, p3}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/j$a;Lio/michaelrocks/libphonenumber/android/i$b;Lio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/StringBuilder;)V

    .line 1239
    invoke-direct {p0, v0, p2, p3}, Lio/michaelrocks/libphonenumber/android/h;->a(ILio/michaelrocks/libphonenumber/android/h$a;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/j$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/michaelrocks/libphonenumber/android/NumberParseException;
        }
    .end annotation

    .prologue
    .line 3043
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZLio/michaelrocks/libphonenumber/android/j$a;)V

    .line 3044
    return-void
.end method

.method a(Ljava/lang/StringBuilder;Lio/michaelrocks/libphonenumber/android/i$b;Ljava/lang/StringBuilder;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2905
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 2906
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->p()Ljava/lang/String;

    move-result-object v3

    .line 2907
    if-eqz v2, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 2951
    :cond_0
    :goto_0
    return v0

    .line 2912
    :cond_1
    iget-object v4, p0, Lio/michaelrocks/libphonenumber/android/h;->D:Lio/michaelrocks/libphonenumber/android/a/c;

    invoke-virtual {v4, v3}, Lio/michaelrocks/libphonenumber/android/a/c;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 2913
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2914
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->a()Lio/michaelrocks/libphonenumber/android/i$d;

    move-result-object v4

    .line 2916
    iget-object v5, p0, Lio/michaelrocks/libphonenumber/android/h;->B:Lio/michaelrocks/libphonenumber/android/a/a;

    invoke-interface {v5, p1, v4, v0}, Lio/michaelrocks/libphonenumber/android/a/a;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$d;Z)Z

    move-result v5

    .line 2920
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v6

    .line 2921
    invoke-virtual {p2}, Lio/michaelrocks/libphonenumber/android/i$b;->q()Ljava/lang/String;

    move-result-object v7

    .line 2922
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_2

    .line 2923
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_5

    .line 2925
    :cond_2
    if-eqz v5, :cond_3

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/h;->B:Lio/michaelrocks/libphonenumber/android/a/a;

    .line 2927
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2926
    invoke-interface {v2, v5, v4, v0}, Lio/michaelrocks/libphonenumber/android/a/a;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$d;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2930
    :cond_3
    if-eqz p3, :cond_4

    if-lez v6, :cond_4

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2931
    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2933
    :cond_4
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move v0, v1

    .line 2934
    goto :goto_0

    .line 2938
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2939
    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v0, v2, v7}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 2940
    if-eqz v5, :cond_6

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/h;->B:Lio/michaelrocks/libphonenumber/android/a/a;

    .line 2941
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v4, v0}, Lio/michaelrocks/libphonenumber/android/a/a;->a(Ljava/lang/CharSequence;Lio/michaelrocks/libphonenumber/android/i$d;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2944
    :cond_6
    if-eqz p3, :cond_7

    if-le v6, v1, :cond_7

    .line 2945
    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2947
    :cond_7
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 2948
    goto/16 :goto_0
.end method

.method public b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2344
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/h;->A:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2345
    if-nez v0, :cond_0

    const-string v0, "ZZ"

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method b(Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2963
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h;->v:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 2966
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/michaelrocks/libphonenumber/android/h;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2968
    const/4 v0, 0x1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v2

    :goto_0
    if-gt v0, v2, :cond_1

    .line 2969
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2972
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 2973
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 2978
    :goto_1
    return-object v0

    .line 2968
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2978
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
