.class final Lio/michaelrocks/libphonenumber/android/g;
.super Ljava/lang/Object;
.source "MultiFileMetadataSourceImpl.java"

# interfaces
.implements Lio/michaelrocks/libphonenumber/android/f;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lio/michaelrocks/libphonenumber/android/e;

.field private final e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lio/michaelrocks/libphonenumber/android/i$b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lio/michaelrocks/libphonenumber/android/i$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/michaelrocks/libphonenumber/android/d;)V
    .locals 3

    .prologue
    .line 66
    const-string v0, "/io/michaelrocks/libphonenumber/android/data/PhoneNumberMetadataProto"

    const-string v1, "/io/michaelrocks/libphonenumber/android/data/PhoneNumberAlternateFormatsProto"

    const-string v2, "/io/michaelrocks/libphonenumber/android/data/ShortNumberMetadataProto"

    invoke-direct {p0, v0, v1, v2, p1}, Lio/michaelrocks/libphonenumber/android/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/d;)V

    .line 68
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lio/michaelrocks/libphonenumber/android/d;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/g;->e:Ljava/util/concurrent/ConcurrentHashMap;

    .line 53
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/g;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 58
    iput-object p1, p0, Lio/michaelrocks/libphonenumber/android/g;->a:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lio/michaelrocks/libphonenumber/android/g;->b:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lio/michaelrocks/libphonenumber/android/g;->c:Ljava/lang/String;

    .line 61
    new-instance v0, Lio/michaelrocks/libphonenumber/android/e;

    invoke-direct {v0, p4}, Lio/michaelrocks/libphonenumber/android/e;-><init>(Lio/michaelrocks/libphonenumber/android/d;)V

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/g;->d:Lio/michaelrocks/libphonenumber/android/e;

    .line 62
    return-void
.end method

.method private b(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    invoke-static {}, Lio/michaelrocks/libphonenumber/android/c;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 101
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    const-string v3, "001"

    .line 102
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 101
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 102
    goto :goto_0
.end method


# virtual methods
.method public a(I)Lio/michaelrocks/libphonenumber/android/i$b;
    .locals 4

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lio/michaelrocks/libphonenumber/android/g;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/g;->d:Lio/michaelrocks/libphonenumber/android/e;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/g;->f:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, p0, Lio/michaelrocks/libphonenumber/android/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lio/michaelrocks/libphonenumber/android/e;->a(Ljava/lang/Object;Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/g;->d:Lio/michaelrocks/libphonenumber/android/e;

    iget-object v1, p0, Lio/michaelrocks/libphonenumber/android/g;->e:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/g;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, Lio/michaelrocks/libphonenumber/android/e;->a(Ljava/lang/Object;Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/i$b;

    move-result-object v0

    return-object v0
.end method
