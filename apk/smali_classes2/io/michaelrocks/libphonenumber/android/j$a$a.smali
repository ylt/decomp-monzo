.class public final enum Lio/michaelrocks/libphonenumber/android/j$a$a;
.super Ljava/lang/Enum;
.source "Phonenumber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/michaelrocks/libphonenumber/android/j$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/michaelrocks/libphonenumber/android/j$a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/michaelrocks/libphonenumber/android/j$a$a;

.field public static final enum b:Lio/michaelrocks/libphonenumber/android/j$a$a;

.field public static final enum c:Lio/michaelrocks/libphonenumber/android/j$a$a;

.field public static final enum d:Lio/michaelrocks/libphonenumber/android/j$a$a;

.field public static final enum e:Lio/michaelrocks/libphonenumber/android/j$a$a;

.field private static final synthetic f:[Lio/michaelrocks/libphonenumber/android/j$a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lio/michaelrocks/libphonenumber/android/j$a$a;

    const-string v1, "FROM_NUMBER_WITH_PLUS_SIGN"

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/j$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->a:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 34
    new-instance v0, Lio/michaelrocks/libphonenumber/android/j$a$a;

    const-string v1, "FROM_NUMBER_WITH_IDD"

    invoke-direct {v0, v1, v3}, Lio/michaelrocks/libphonenumber/android/j$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->b:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 35
    new-instance v0, Lio/michaelrocks/libphonenumber/android/j$a$a;

    const-string v1, "FROM_NUMBER_WITHOUT_PLUS_SIGN"

    invoke-direct {v0, v1, v4}, Lio/michaelrocks/libphonenumber/android/j$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->c:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 36
    new-instance v0, Lio/michaelrocks/libphonenumber/android/j$a$a;

    const-string v1, "FROM_DEFAULT_COUNTRY"

    invoke-direct {v0, v1, v5}, Lio/michaelrocks/libphonenumber/android/j$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->d:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 37
    new-instance v0, Lio/michaelrocks/libphonenumber/android/j$a$a;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v6}, Lio/michaelrocks/libphonenumber/android/j$a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->e:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [Lio/michaelrocks/libphonenumber/android/j$a$a;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/j$a$a;->a:Lio/michaelrocks/libphonenumber/android/j$a$a;

    aput-object v1, v0, v2

    sget-object v1, Lio/michaelrocks/libphonenumber/android/j$a$a;->b:Lio/michaelrocks/libphonenumber/android/j$a$a;

    aput-object v1, v0, v3

    sget-object v1, Lio/michaelrocks/libphonenumber/android/j$a$a;->c:Lio/michaelrocks/libphonenumber/android/j$a$a;

    aput-object v1, v0, v4

    sget-object v1, Lio/michaelrocks/libphonenumber/android/j$a$a;->d:Lio/michaelrocks/libphonenumber/android/j$a$a;

    aput-object v1, v0, v5

    sget-object v1, Lio/michaelrocks/libphonenumber/android/j$a$a;->e:Lio/michaelrocks/libphonenumber/android/j$a$a;

    aput-object v1, v0, v6

    sput-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->f:[Lio/michaelrocks/libphonenumber/android/j$a$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a$a;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lio/michaelrocks/libphonenumber/android/j$a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/michaelrocks/libphonenumber/android/j$a$a;

    return-object v0
.end method

.method public static values()[Lio/michaelrocks/libphonenumber/android/j$a$a;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->f:[Lio/michaelrocks/libphonenumber/android/j$a$a;

    invoke-virtual {v0}, [Lio/michaelrocks/libphonenumber/android/j$a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/michaelrocks/libphonenumber/android/j$a$a;

    return-object v0
.end method
