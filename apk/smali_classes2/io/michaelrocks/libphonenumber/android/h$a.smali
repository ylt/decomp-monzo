.class public final enum Lio/michaelrocks/libphonenumber/android/h$a;
.super Ljava/lang/Enum;
.source "PhoneNumberUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/michaelrocks/libphonenumber/android/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lio/michaelrocks/libphonenumber/android/h$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lio/michaelrocks/libphonenumber/android/h$a;

.field public static final enum b:Lio/michaelrocks/libphonenumber/android/h$a;

.field public static final enum c:Lio/michaelrocks/libphonenumber/android/h$a;

.field public static final enum d:Lio/michaelrocks/libphonenumber/android/h$a;

.field private static final synthetic e:[Lio/michaelrocks/libphonenumber/android/h$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 394
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$a;

    const-string v1, "E164"

    invoke-direct {v0, v1, v2}, Lio/michaelrocks/libphonenumber/android/h$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$a;->a:Lio/michaelrocks/libphonenumber/android/h$a;

    .line 395
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$a;

    const-string v1, "INTERNATIONAL"

    invoke-direct {v0, v1, v3}, Lio/michaelrocks/libphonenumber/android/h$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$a;->b:Lio/michaelrocks/libphonenumber/android/h$a;

    .line 396
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$a;

    const-string v1, "NATIONAL"

    invoke-direct {v0, v1, v4}, Lio/michaelrocks/libphonenumber/android/h$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$a;->c:Lio/michaelrocks/libphonenumber/android/h$a;

    .line 397
    new-instance v0, Lio/michaelrocks/libphonenumber/android/h$a;

    const-string v1, "RFC3966"

    invoke-direct {v0, v1, v5}, Lio/michaelrocks/libphonenumber/android/h$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$a;->d:Lio/michaelrocks/libphonenumber/android/h$a;

    .line 393
    const/4 v0, 0x4

    new-array v0, v0, [Lio/michaelrocks/libphonenumber/android/h$a;

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$a;->a:Lio/michaelrocks/libphonenumber/android/h$a;

    aput-object v1, v0, v2

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$a;->b:Lio/michaelrocks/libphonenumber/android/h$a;

    aput-object v1, v0, v3

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$a;->c:Lio/michaelrocks/libphonenumber/android/h$a;

    aput-object v1, v0, v4

    sget-object v1, Lio/michaelrocks/libphonenumber/android/h$a;->d:Lio/michaelrocks/libphonenumber/android/h$a;

    aput-object v1, v0, v5

    sput-object v0, Lio/michaelrocks/libphonenumber/android/h$a;->e:[Lio/michaelrocks/libphonenumber/android/h$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 393
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/h$a;
    .locals 1

    .prologue
    .line 393
    const-class v0, Lio/michaelrocks/libphonenumber/android/h$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/michaelrocks/libphonenumber/android/h$a;

    return-object v0
.end method

.method public static values()[Lio/michaelrocks/libphonenumber/android/h$a;
    .locals 1

    .prologue
    .line 393
    sget-object v0, Lio/michaelrocks/libphonenumber/android/h$a;->e:[Lio/michaelrocks/libphonenumber/android/h$a;

    invoke-virtual {v0}, [Lio/michaelrocks/libphonenumber/android/h$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/michaelrocks/libphonenumber/android/h$a;

    return-object v0
.end method
