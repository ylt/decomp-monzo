.class public Lio/michaelrocks/libphonenumber/android/j$a;
.super Ljava/lang/Object;
.source "Phonenumber.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/michaelrocks/libphonenumber/android/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/michaelrocks/libphonenumber/android/j$a$a;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:I

.field private c:Z

.field private d:J

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Lio/michaelrocks/libphonenumber/android/j$a$a;

.field private o:Z

.field private p:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->b:I

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->d:J

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->f:Ljava/lang/String;

    .line 97
    iput-boolean v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->h:Z

    .line 113
    const/4 v0, 0x1

    iput v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->j:I

    .line 129
    const-string v0, ""

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->l:Ljava/lang/String;

    .line 167
    const-string v0, ""

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->p:Ljava/lang/String;

    .line 41
    sget-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->e:Lio/michaelrocks/libphonenumber/android/j$a$a;

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->n:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 42
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->b:I

    return v0
.end method

.method public a(I)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->a:Z

    .line 51
    iput p1, p0, Lio/michaelrocks/libphonenumber/android/j$a;->b:I

    .line 52
    return-object p0
.end method

.method public a(J)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->c:Z

    .line 67
    iput-wide p1, p0, Lio/michaelrocks/libphonenumber/android/j$a;->d:J

    .line 68
    return-object p0
.end method

.method public a(Lio/michaelrocks/libphonenumber/android/j$a$a;)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 152
    if-nez p1, :cond_0

    .line 153
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 155
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->m:Z

    .line 156
    iput-object p1, p0, Lio/michaelrocks/libphonenumber/android/j$a;->n:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 157
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 82
    if-nez p1, :cond_0

    .line 83
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 85
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->e:Z

    .line 86
    iput-object p1, p0, Lio/michaelrocks/libphonenumber/android/j$a;->f:Ljava/lang/String;

    .line 87
    return-object p0
.end method

.method public a(Z)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->g:Z

    .line 102
    iput-boolean p1, p0, Lio/michaelrocks/libphonenumber/android/j$a;->h:Z

    .line 103
    return-object p0
.end method

.method public a(Lio/michaelrocks/libphonenumber/android/j$a;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 225
    if-nez p1, :cond_1

    move v0, v1

    .line 231
    :cond_0
    :goto_0
    return v0

    .line 228
    :cond_1
    if-eq p0, p1, :cond_0

    .line 231
    iget v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->b:I

    iget v3, p1, Lio/michaelrocks/libphonenumber/android/j$a;->b:I

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->d:J

    iget-wide v4, p1, Lio/michaelrocks/libphonenumber/android/j$a;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->f:Ljava/lang/String;

    iget-object v3, p1, Lio/michaelrocks/libphonenumber/android/j$a;->f:Ljava/lang/String;

    .line 232
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->h:Z

    iget-boolean v3, p1, Lio/michaelrocks/libphonenumber/android/j$a;->h:Z

    if-ne v2, v3, :cond_2

    iget v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->j:I

    iget v3, p1, Lio/michaelrocks/libphonenumber/android/j$a;->j:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->l:Ljava/lang/String;

    iget-object v3, p1, Lio/michaelrocks/libphonenumber/android/j$a;->l:Ljava/lang/String;

    .line 234
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->n:Lio/michaelrocks/libphonenumber/android/j$a$a;

    iget-object v3, p1, Lio/michaelrocks/libphonenumber/android/j$a;->n:Lio/michaelrocks/libphonenumber/android/j$a$a;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->p:Ljava/lang/String;

    iget-object v3, p1, Lio/michaelrocks/libphonenumber/android/j$a;->p:Ljava/lang/String;

    .line 235
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 236
    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->n()Z

    move-result v2

    invoke-virtual {p1}, Lio/michaelrocks/libphonenumber/android/j$a;->n()Z

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->d:J

    return-wide v0
.end method

.method public b(I)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->i:Z

    .line 118
    iput p1, p0, Lio/michaelrocks/libphonenumber/android/j$a;->j:I

    .line 119
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 133
    if-nez p1, :cond_0

    .line 134
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 136
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->k:Z

    .line 137
    iput-object p1, p0, Lio/michaelrocks/libphonenumber/android/j$a;->l:Ljava/lang/String;

    .line 138
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 171
    if-nez p1, :cond_0

    .line 172
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 174
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->o:Z

    .line 175
    iput-object p1, p0, Lio/michaelrocks/libphonenumber/android/j$a;->p:Ljava/lang/String;

    .line 176
    return-object p0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->e:Z

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->g:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 241
    instance-of v0, p1, Lio/michaelrocks/libphonenumber/android/j$a;

    if-eqz v0, :cond_0

    check-cast p1, Lio/michaelrocks/libphonenumber/android/j$a;

    invoke-virtual {p0, p1}, Lio/michaelrocks/libphonenumber/android/j$a;->a(Lio/michaelrocks/libphonenumber/android/j$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->h:Z

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->i:Z

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->j:I

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 250
    .line 251
    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->a()I

    move-result v0

    add-int/lit16 v0, v0, 0x87d

    .line 252
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 253
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 254
    mul-int/lit8 v3, v0, 0x35

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 255
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->h()I

    move-result v3

    add-int/2addr v0, v3

    .line 256
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 257
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->l()Lio/michaelrocks/libphonenumber/android/j$a$a;

    move-result-object v3

    invoke-virtual {v3}, Lio/michaelrocks/libphonenumber/android/j$a$a;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 258
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 259
    mul-int/lit8 v0, v0, 0x35

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->n()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 260
    return v0

    :cond_0
    move v0, v2

    .line 254
    goto :goto_0

    :cond_1
    move v1, v2

    .line 259
    goto :goto_1
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->k:Z

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->m:Z

    return v0
.end method

.method public l()Lio/michaelrocks/libphonenumber/android/j$a$a;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->n:Lio/michaelrocks/libphonenumber/android/j$a$a;

    return-object v0
.end method

.method public m()Lio/michaelrocks/libphonenumber/android/j$a;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->m:Z

    .line 161
    sget-object v0, Lio/michaelrocks/libphonenumber/android/j$a$a;->e:Lio/michaelrocks/libphonenumber/android/j$a$a;

    iput-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->n:Lio/michaelrocks/libphonenumber/android/j$a$a;

    .line 162
    return-object p0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->o:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lio/michaelrocks/libphonenumber/android/j$a;->p:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 266
    const-string v1, "Country Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 267
    const-string v1, " National Number: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 268
    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 269
    const-string v1, " Leading Zero(s): true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_0
    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 272
    const-string v1, " Number of leading zeros: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 274
    :cond_1
    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 275
    const-string v1, " Extension: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    :cond_2
    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 278
    const-string v1, " Country Code Source: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->n:Lio/michaelrocks/libphonenumber/android/j$a$a;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 280
    :cond_3
    invoke-virtual {p0}, Lio/michaelrocks/libphonenumber/android/j$a;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 281
    const-string v1, " Preferred Domestic Carrier Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lio/michaelrocks/libphonenumber/android/j$a;->p:Ljava/lang/String;

    .line 282
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
