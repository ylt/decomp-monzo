.class Le/a/a/a/d$a;
.super Ljava/lang/Object;
.source "PhotoViewAttacher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le/a/a/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Le/a/a/a/d;

.field private final b:F

.field private final c:F

.field private final d:J

.field private final e:F

.field private final f:F


# direct methods
.method public constructor <init>(Le/a/a/a/d;FFFF)V
    .locals 2

    .prologue
    .line 1068
    iput-object p1, p0, Le/a/a/a/d$a;->a:Le/a/a/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069
    iput p4, p0, Le/a/a/a/d$a;->b:F

    .line 1070
    iput p5, p0, Le/a/a/a/d$a;->c:F

    .line 1071
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Le/a/a/a/d$a;->d:J

    .line 1072
    iput p2, p0, Le/a/a/a/d$a;->e:F

    .line 1073
    iput p3, p0, Le/a/a/a/d$a;->f:F

    .line 1074
    return-void
.end method

.method private a()F
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1096
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Le/a/a/a/d$a;->d:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    mul-float/2addr v0, v4

    iget-object v1, p0, Le/a/a/a/d$a;->a:Le/a/a/a/d;

    iget v1, v1, Le/a/a/a/d;->a:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1097
    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1098
    iget-object v1, p0, Le/a/a/a/d$a;->a:Le/a/a/a/d;

    invoke-static {v1}, Le/a/a/a/d;->c(Le/a/a/a/d;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 1099
    return v0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1078
    iget-object v0, p0, Le/a/a/a/d$a;->a:Le/a/a/a/d;

    invoke-virtual {v0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 1079
    if-nez v0, :cond_1

    .line 1093
    :cond_0
    :goto_0
    return-void

    .line 1083
    :cond_1
    invoke-direct {p0}, Le/a/a/a/d$a;->a()F

    move-result v1

    .line 1084
    iget v2, p0, Le/a/a/a/d$a;->e:F

    iget v3, p0, Le/a/a/a/d$a;->f:F

    iget v4, p0, Le/a/a/a/d$a;->e:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    .line 1085
    iget-object v3, p0, Le/a/a/a/d$a;->a:Le/a/a/a/d;

    invoke-virtual {v3}, Le/a/a/a/d;->g()F

    move-result v3

    div-float/2addr v2, v3

    .line 1087
    iget-object v3, p0, Le/a/a/a/d$a;->a:Le/a/a/a/d;

    iget v4, p0, Le/a/a/a/d$a;->b:F

    iget v5, p0, Le/a/a/a/d$a;->c:F

    invoke-virtual {v3, v2, v4, v5}, Le/a/a/a/d;->a(FFF)V

    .line 1090
    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 1091
    invoke-static {v0, p0}, Le/a/a/a/a;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
