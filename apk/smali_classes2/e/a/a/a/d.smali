.class public Le/a/a/a/d;
.super Ljava/lang/Object;
.source "PhotoViewAttacher.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Le/a/a/a/a/e;
.implements Le/a/a/a/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le/a/a/a/d$b;,
        Le/a/a/a/d$a;,
        Le/a/a/a/d$f;,
        Le/a/a/a/d$g;,
        Le/a/a/a/d$d;,
        Le/a/a/a/d$e;,
        Le/a/a/a/d$c;
    }
.end annotation


# static fields
.field static b:I

.field private static final c:Z


# instance fields
.field private A:I

.field private B:Le/a/a/a/d$b;

.field private C:I

.field private D:F

.field private E:Z

.field private F:Landroid/widget/ImageView$ScaleType;

.field a:I

.field private d:Landroid/view/animation/Interpolator;

.field private e:F

.field private f:F

.field private g:F

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroid/view/GestureDetector;

.field private l:Le/a/a/a/a/d;

.field private final m:Landroid/graphics/Matrix;

.field private final n:Landroid/graphics/Matrix;

.field private final o:Landroid/graphics/Matrix;

.field private final p:Landroid/graphics/RectF;

.field private final q:[F

.field private r:Le/a/a/a/d$c;

.field private s:Le/a/a/a/d$d;

.field private t:Le/a/a/a/d$g;

.field private u:Landroid/view/View$OnLongClickListener;

.field private v:Le/a/a/a/d$e;

.field private w:Le/a/a/a/d$f;

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "PhotoViewAttacher"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Le/a/a/a/d;->c:Z

    .line 68
    const/4 v0, 0x1

    sput v0, Le/a/a/a/d;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Le/a/a/a/d;-><init>(Landroid/widget/ImageView;Z)V

    .line 159
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Le/a/a/a/d;->d:Landroid/view/animation/Interpolator;

    .line 61
    const/16 v0, 0xc8

    iput v0, p0, Le/a/a/a/d;->a:I

    .line 70
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Le/a/a/a/d;->e:F

    .line 71
    const/high16 v0, 0x3fe00000    # 1.75f

    iput v0, p0, Le/a/a/a/d;->f:F

    .line 72
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Le/a/a/a/d;->g:F

    .line 74
    iput-boolean v1, p0, Le/a/a/a/d;->h:Z

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Le/a/a/a/d;->i:Z

    .line 135
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    .line 136
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Le/a/a/a/d;->n:Landroid/graphics/Matrix;

    .line 137
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    .line 138
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Le/a/a/a/d;->p:Landroid/graphics/RectF;

    .line 139
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Le/a/a/a/d;->q:[F

    .line 151
    const/4 v0, 0x2

    iput v0, p0, Le/a/a/a/d;->C:I

    .line 155
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    iput-object v0, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    .line 162
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Le/a/a/a/d;->j:Ljava/lang/ref/WeakReference;

    .line 164
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setDrawingCacheEnabled(Z)V

    .line 165
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 167
    invoke-virtual {p1}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 172
    :cond_0
    invoke-static {p1}, Le/a/a/a/d;->b(Landroid/widget/ImageView;)V

    .line 174
    invoke-virtual {p1}, Landroid/widget/ImageView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    :goto_0
    return-void

    .line 179
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 178
    invoke-static {v0, p0}, Le/a/a/a/a/f;->a(Landroid/content/Context;Le/a/a/a/a/e;)Le/a/a/a/a/d;

    move-result-object v0

    iput-object v0, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    .line 181
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Le/a/a/a/d$1;

    invoke-direct {v2, p0}, Le/a/a/a/d$1;-><init>(Le/a/a/a/d;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Le/a/a/a/d;->k:Landroid/view/GestureDetector;

    .line 211
    iget-object v0, p0, Le/a/a/a/d;->k:Landroid/view/GestureDetector;

    new-instance v1, Le/a/a/a/b;

    invoke-direct {v1, p0}, Le/a/a/a/b;-><init>(Le/a/a/a/d;)V

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Le/a/a/a/d;->D:F

    .line 215
    invoke-virtual {p0, p2}, Le/a/a/a/d;->a(Z)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Matrix;I)F
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Le/a/a/a/d;->q:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 851
    iget-object v0, p0, Le/a/a/a/d;->q:[F

    aget v0, v0, p2

    return v0
.end method

.method private a(Landroid/graphics/Matrix;)Landroid/graphics/RectF;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 811
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 813
    if-eqz v0, :cond_0

    .line 814
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 815
    if-eqz v0, :cond_0

    .line 816
    iget-object v1, p0, Le/a/a/a/d;->p:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    .line 817
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    .line 816
    invoke-virtual {v1, v3, v3, v2, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 818
    iget-object v0, p0, Le/a/a/a/d;->p:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 819
    iget-object v0, p0, Le/a/a/a/d;->p:Landroid/graphics/RectF;

    .line 822
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Le/a/a/a/d;)Landroid/view/View$OnLongClickListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Le/a/a/a/d;->u:Landroid/view/View$OnLongClickListener;

    return-object v0
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 887
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 888
    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 892
    :cond_1
    invoke-direct {p0, v0}, Le/a/a/a/d;->c(Landroid/widget/ImageView;)I

    move-result v1

    int-to-float v1, v1

    .line 893
    invoke-direct {p0, v0}, Le/a/a/a/d;->d(Landroid/widget/ImageView;)I

    move-result v0

    int-to-float v2, v0

    .line 894
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 895
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    .line 897
    iget-object v0, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 899
    int-to-float v0, v3

    div-float v0, v1, v0

    .line 900
    int-to-float v5, v4

    div-float v5, v2, v5

    .line 902
    iget-object v6, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    sget-object v7, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    if-ne v6, v7, :cond_2

    .line 903
    iget-object v0, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    int-to-float v3, v3

    sub-float/2addr v1, v3

    div-float/2addr v1, v9

    int-to-float v3, v4

    sub-float/2addr v2, v3

    div-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 949
    :goto_1
    invoke-direct {p0}, Le/a/a/a/d;->q()V

    goto :goto_0

    .line 906
    :cond_2
    iget-object v6, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    sget-object v7, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    if-ne v6, v7, :cond_3

    .line 907
    invoke-static {v0, v5}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 908
    iget-object v5, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 909
    iget-object v5, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    int-to-float v3, v3

    mul-float/2addr v3, v0

    sub-float/2addr v1, v3

    div-float/2addr v1, v9

    int-to-float v3, v4

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    div-float/2addr v0, v9

    invoke-virtual {v5, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_1

    .line 912
    :cond_3
    iget-object v6, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    sget-object v7, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    if-ne v6, v7, :cond_4

    .line 913
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 914
    iget-object v5, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 915
    iget-object v5, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    int-to-float v3, v3

    mul-float/2addr v3, v0

    sub-float/2addr v1, v3

    div-float/2addr v1, v9

    int-to-float v3, v4

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    div-float/2addr v0, v9

    invoke-virtual {v5, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_1

    .line 919
    :cond_4
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v5, v3

    int-to-float v6, v4

    invoke-direct {v0, v8, v8, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 920
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v8, v8, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 922
    iget v1, p0, Le/a/a/a/d;->D:F

    float-to-int v1, v1

    rem-int/lit16 v1, v1, 0xb4

    if-eqz v1, :cond_5

    .line 923
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v1, v4

    int-to-float v2, v3

    invoke-direct {v0, v8, v8, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 926
    :cond_5
    sget-object v1, Le/a/a/a/d$2;->a:[I

    iget-object v2, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    .line 933
    :pswitch_0
    iget-object v1, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->START:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v0, v5, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto :goto_1

    .line 928
    :pswitch_1
    iget-object v1, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    .line 929
    invoke-virtual {v1, v0, v5, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto :goto_1

    .line 937
    :pswitch_2
    iget-object v1, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->END:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v0, v5, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto/16 :goto_1

    .line 941
    :pswitch_3
    iget-object v1, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v0, v5, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto/16 :goto_1

    .line 926
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Le/a/a/a/d;Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Le/a/a/a/d;->b(Landroid/graphics/Matrix;)V

    return-void
.end method

.method private static a(Landroid/widget/ImageView;)Z
    .locals 1

    .prologue
    .line 92
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Le/a/a/a/d;)Le/a/a/a/d$f;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Le/a/a/a/d;->w:Le/a/a/a/d$f;

    return-object v0
.end method

.method private b(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 865
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 866
    if-eqz v0, :cond_0

    .line 868
    invoke-direct {p0}, Le/a/a/a/d;->o()V

    .line 869
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 872
    iget-object v0, p0, Le/a/a/a/d;->r:Le/a/a/a/d$c;

    if-eqz v0, :cond_0

    .line 873
    invoke-direct {p0, p1}, Le/a/a/a/d;->a(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v0

    .line 874
    if-eqz v0, :cond_0

    .line 875
    iget-object v1, p0, Le/a/a/a/d;->r:Le/a/a/a/d$c;

    invoke-interface {v1, v0}, Le/a/a/a/d$c;->a(Landroid/graphics/RectF;)V

    .line 879
    :cond_0
    return-void
.end method

.method private static b(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 121
    if-eqz p0, :cond_0

    instance-of v0, p0, Le/a/a/a/c;

    if-nez v0, :cond_0

    .line 122
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView$ScaleType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 126
    :cond_0
    return-void
.end method

.method private static b(Landroid/widget/ImageView$ScaleType;)Z
    .locals 3

    .prologue
    .line 99
    if-nez p0, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 109
    :goto_0
    return v0

    .line 103
    :cond_0
    sget-object v0, Le/a/a/a/d$2;->a:[I

    invoke-virtual {p0}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 109
    const/4 v0, 0x1

    goto :goto_0

    .line 105
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/widget/ImageView$ScaleType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported in PhotoView"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private c(Landroid/widget/ImageView;)I
    .locals 2

    .prologue
    .line 953
    if-nez p1, :cond_0

    .line 954
    const/4 v0, 0x0

    .line 955
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method static synthetic c(Le/a/a/a/d;)Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Le/a/a/a/d;->d:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method private d(Landroid/widget/ImageView;)I
    .locals 2

    .prologue
    .line 959
    if-nez p1, :cond_0

    .line 960
    const/4 v0, 0x0

    .line 961
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method static synthetic d(Le/a/a/a/d;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic e(Le/a/a/a/d;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Le/a/a/a/d;->l()Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k()Z
    .locals 1

    .prologue
    .line 50
    sget-boolean v0, Le/a/a/a/d;->c:Z

    return v0
.end method

.method private l()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 702
    iget-object v0, p0, Le/a/a/a/d;->n:Landroid/graphics/Matrix;

    iget-object v1, p0, Le/a/a/a/d;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 703
    iget-object v0, p0, Le/a/a/a/d;->n:Landroid/graphics/Matrix;

    iget-object v1, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 704
    iget-object v0, p0, Le/a/a/a/d;->n:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Le/a/a/a/d;->B:Le/a/a/a/d$b;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Le/a/a/a/d;->B:Le/a/a/a/d$b;

    invoke-virtual {v0}, Le/a/a/a/d$b;->a()V

    .line 710
    const/4 v0, 0x0

    iput-object v0, p0, Le/a/a/a/d;->B:Le/a/a/a/d$b;

    .line 712
    :cond_0
    return-void
.end method

.method private n()V
    .locals 1

    .prologue
    .line 722
    invoke-direct {p0}, Le/a/a/a/d;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 723
    invoke-direct {p0}, Le/a/a/a/d;->l()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {p0, v0}, Le/a/a/a/d;->b(Landroid/graphics/Matrix;)V

    .line 725
    :cond_0
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 728
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 734
    if-eqz v0, :cond_0

    instance-of v1, v0, Le/a/a/a/c;

    if-nez v1, :cond_0

    .line 735
    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView$ScaleType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 736
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ImageView\'s ScaleType has been changed since attaching a PhotoViewAttacher. You should call setScaleType on the PhotoViewAttacher instead of on the ImageView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 740
    :cond_0
    return-void
.end method

.method private p()Z
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 743
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v4

    .line 744
    if-nez v4, :cond_0

    move v0, v2

    .line 801
    :goto_0
    return v0

    .line 748
    :cond_0
    invoke-direct {p0}, Le/a/a/a/d;->l()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {p0, v0}, Le/a/a/a/d;->a(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v5

    .line 749
    if-nez v5, :cond_1

    move v0, v2

    .line 750
    goto :goto_0

    .line 753
    :cond_1
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v6

    .line 756
    invoke-direct {p0, v4}, Le/a/a/a/d;->d(Landroid/widget/ImageView;)I

    move-result v7

    .line 757
    int-to-float v8, v7

    cmpg-float v8, v0, v8

    if-gtz v8, :cond_2

    .line 758
    sget-object v8, Le/a/a/a/d$2;->a:[I

    iget-object v9, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v9}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 766
    int-to-float v7, v7

    sub-float v0, v7, v0

    div-float/2addr v0, v10

    iget v7, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v7

    .line 775
    :goto_1
    invoke-direct {p0, v4}, Le/a/a/a/d;->c(Landroid/widget/ImageView;)I

    move-result v4

    .line 776
    int-to-float v7, v4

    cmpg-float v7, v6, v7

    if-gtz v7, :cond_4

    .line 777
    sget-object v1, Le/a/a/a/d$2;->a:[I

    iget-object v2, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 785
    int-to-float v1, v4

    sub-float/2addr v1, v6

    div-float/2addr v1, v10

    iget v2, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    .line 788
    :goto_2
    const/4 v2, 0x2

    iput v2, p0, Le/a/a/a/d;->C:I

    .line 800
    :goto_3
    iget-object v2, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move v0, v3

    .line 801
    goto :goto_0

    .line 760
    :pswitch_0
    iget v0, v5, Landroid/graphics/RectF;->top:F

    neg-float v0, v0

    .line 761
    goto :goto_1

    .line 763
    :pswitch_1
    int-to-float v7, v7

    sub-float v0, v7, v0

    iget v7, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v7

    .line 764
    goto :goto_1

    .line 769
    :cond_2
    iget v0, v5, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 770
    iget v0, v5, Landroid/graphics/RectF;->top:F

    neg-float v0, v0

    goto :goto_1

    .line 771
    :cond_3
    iget v0, v5, Landroid/graphics/RectF;->bottom:F

    int-to-float v8, v7

    cmpg-float v0, v0, v8

    if-gez v0, :cond_7

    .line 772
    int-to-float v0, v7

    iget v7, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v7

    goto :goto_1

    .line 779
    :pswitch_2
    iget v1, v5, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    .line 780
    goto :goto_2

    .line 782
    :pswitch_3
    int-to-float v1, v4

    sub-float/2addr v1, v6

    iget v2, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    .line 783
    goto :goto_2

    .line 789
    :cond_4
    iget v6, v5, Landroid/graphics/RectF;->left:F

    cmpl-float v6, v6, v1

    if-lez v6, :cond_5

    .line 790
    iput v2, p0, Le/a/a/a/d;->C:I

    .line 791
    iget v1, v5, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    goto :goto_3

    .line 792
    :cond_5
    iget v2, v5, Landroid/graphics/RectF;->right:F

    int-to-float v6, v4

    cmpg-float v2, v2, v6

    if-gez v2, :cond_6

    .line 793
    int-to-float v1, v4

    iget v2, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v2

    .line 794
    iput v3, p0, Le/a/a/a/d;->C:I

    goto :goto_3

    .line 796
    :cond_6
    const/4 v2, -0x1

    iput v2, p0, Le/a/a/a/d;->C:I

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_1

    .line 758
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 777
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private q()V
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 859
    iget v0, p0, Le/a/a/a/d;->D:F

    invoke-virtual {p0, v0}, Le/a/a/a/d;->a(F)V

    .line 860
    invoke-direct {p0}, Le/a/a/a/d;->l()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {p0, v0}, Le/a/a/a/d;->b(Landroid/graphics/Matrix;)V

    .line 861
    invoke-direct {p0}, Le/a/a/a/d;->p()Z

    .line 862
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    iget-object v0, p0, Le/a/a/a/d;->j:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 281
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Le/a/a/a/d;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 256
    if-eqz v0, :cond_2

    .line 258
    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 259
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 260
    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 264
    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 267
    invoke-direct {p0}, Le/a/a/a/d;->m()V

    .line 270
    :cond_2
    iget-object v0, p0, Le/a/a/a/d;->k:Landroid/view/GestureDetector;

    if-eqz v0, :cond_3

    .line 271
    iget-object v0, p0, Le/a/a/a/d;->k:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 275
    :cond_3
    iput-object v3, p0, Le/a/a/a/d;->r:Le/a/a/a/d$c;

    .line 276
    iput-object v3, p0, Le/a/a/a/d;->s:Le/a/a/a/d$d;

    .line 277
    iput-object v3, p0, Le/a/a/a/d;->t:Le/a/a/a/d$g;

    .line 280
    iput-object v3, p0, Le/a/a/a/d;->j:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    const/high16 v1, 0x43b40000    # 360.0f

    rem-float v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 327
    invoke-direct {p0}, Le/a/a/a/d;->n()V

    .line 328
    return-void
.end method

.method public a(FF)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 374
    iget-object v0, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    invoke-interface {v0}, Le/a/a/a/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    sget-boolean v0, Le/a/a/a/d;->c:Z

    if-eqz v0, :cond_2

    .line 379
    invoke-static {}, Le/a/a/a/b/a;->a()Le/a/a/a/b/b;

    move-result-object v0

    const-string v1, "PhotoViewAttacher"

    const-string v2, "onDrag: dx: %.2f. dy: %.2f"

    new-array v3, v7, [Ljava/lang/Object;

    .line 380
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 379
    invoke-interface {v0, v1, v2}, Le/a/a/a/b/b;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_2
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 384
    iget-object v1, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 385
    invoke-direct {p0}, Le/a/a/a/d;->n()V

    .line 396
    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 397
    iget-boolean v1, p0, Le/a/a/a/d;->h:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    invoke-interface {v1}, Le/a/a/a/a/d;->a()Z

    move-result v1

    if-nez v1, :cond_5

    iget-boolean v1, p0, Le/a/a/a/d;->i:Z

    if-nez v1, :cond_5

    .line 398
    iget v1, p0, Le/a/a/a/d;->C:I

    if-eq v1, v7, :cond_4

    iget v1, p0, Le/a/a/a/d;->C:I

    if-nez v1, :cond_3

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-gez v1, :cond_4

    :cond_3
    iget v1, p0, Le/a/a/a/d;->C:I

    if-ne v1, v5, :cond_0

    const/high16 v1, -0x40800000    # -1.0f

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    .line 401
    :cond_4
    if-eqz v0, :cond_0

    .line 402
    invoke-interface {v0, v6}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 406
    :cond_5
    if-eqz v0, :cond_0

    .line 407
    invoke-interface {v0, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0
.end method

.method public a(FFF)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 465
    sget-boolean v0, Le/a/a/a/d;->c:Z

    if-eqz v0, :cond_0

    .line 466
    invoke-static {}, Le/a/a/a/b/a;->a()Le/a/a/a/b/b;

    move-result-object v0

    const-string v1, "PhotoViewAttacher"

    const-string v2, "onScale: scale: %.2f. fX: %.2f. fY: %.2f"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 469
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    .line 468
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 466
    invoke-interface {v0, v1, v2}, Le/a/a/a/b/b;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_0
    invoke-virtual {p0}, Le/a/a/a/d;->g()F

    move-result v0

    iget v1, p0, Le/a/a/a/d;->g:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    cmpg-float v0, p1, v6

    if-gez v0, :cond_4

    :cond_1
    invoke-virtual {p0}, Le/a/a/a/d;->g()F

    move-result v0

    iget v1, p0, Le/a/a/a/d;->e:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_2

    cmpl-float v0, p1, v6

    if-lez v0, :cond_4

    .line 473
    :cond_2
    iget-object v0, p0, Le/a/a/a/d;->v:Le/a/a/a/d$e;

    if-eqz v0, :cond_3

    .line 474
    iget-object v0, p0, Le/a/a/a/d;->v:Le/a/a/a/d$e;

    invoke-interface {v0, p1, p2, p3}, Le/a/a/a/d$e;->a(FFF)V

    .line 476
    :cond_3
    iget-object v0, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 477
    invoke-direct {p0}, Le/a/a/a/d;->n()V

    .line 479
    :cond_4
    return-void
.end method

.method public a(FFFF)V
    .locals 6

    .prologue
    .line 415
    sget-boolean v0, Le/a/a/a/d;->c:Z

    if-eqz v0, :cond_0

    .line 416
    invoke-static {}, Le/a/a/a/b/a;->a()Le/a/a/a/b/b;

    move-result-object v0

    const-string v1, "PhotoViewAttacher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFling. sX: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sY: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Vx: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Vy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Le/a/a/a/b/b;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    :cond_0
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 422
    new-instance v1, Le/a/a/a/d$b;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Le/a/a/a/d$b;-><init>(Le/a/a/a/d;Landroid/content/Context;)V

    iput-object v1, p0, Le/a/a/a/d;->B:Le/a/a/a/d$b;

    .line 423
    iget-object v1, p0, Le/a/a/a/d;->B:Le/a/a/a/d$b;

    invoke-direct {p0, v0}, Le/a/a/a/d;->c(Landroid/widget/ImageView;)I

    move-result v2

    .line 424
    invoke-direct {p0, v0}, Le/a/a/a/d;->d(Landroid/widget/ImageView;)I

    move-result v3

    float-to-int v4, p3

    float-to-int v5, p4

    .line 423
    invoke-virtual {v1, v2, v3, v4, v5}, Le/a/a/a/d$b;->a(IIII)V

    .line 425
    iget-object v1, p0, Le/a/a/a/d;->B:Le/a/a/a/d$b;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 426
    return-void
.end method

.method public a(FFFZ)V
    .locals 7

    .prologue
    .line 622
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v6

    .line 624
    if-eqz v6, :cond_1

    .line 626
    iget v0, p0, Le/a/a/a/d;->e:F

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Le/a/a/a/d;->g:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    .line 628
    :cond_0
    invoke-static {}, Le/a/a/a/b/a;->a()Le/a/a/a/b/b;

    move-result-object v0

    const-string v1, "PhotoViewAttacher"

    const-string v2, "Scale must be within the range of minScale and maxScale"

    .line 629
    invoke-interface {v0, v1, v2}, Le/a/a/a/b/b;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    :cond_1
    :goto_0
    return-void

    .line 634
    :cond_2
    if-eqz p4, :cond_3

    .line 635
    new-instance v0, Le/a/a/a/d$a;

    invoke-virtual {p0}, Le/a/a/a/d;->g()F

    move-result v2

    move-object v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Le/a/a/a/d$a;-><init>(Le/a/a/a/d;FFFF)V

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 638
    :cond_3
    iget-object v0, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 639
    invoke-direct {p0}, Le/a/a/a/d;->n()V

    goto :goto_0
.end method

.method public a(Landroid/widget/ImageView$ScaleType;)V
    .locals 1

    .prologue
    .line 654
    invoke-static {p1}, Le/a/a/a/d;->b(Landroid/widget/ImageView$ScaleType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    if-eq p1, v0, :cond_0

    .line 655
    iput-object p1, p0, Le/a/a/a/d;->F:Landroid/widget/ImageView$ScaleType;

    .line 658
    invoke-virtual {p0}, Le/a/a/a/d;->j()V

    .line 660
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 664
    iput-boolean p1, p0, Le/a/a/a/d;->E:Z

    .line 665
    invoke-virtual {p0}, Le/a/a/a/d;->j()V

    .line 666
    return-void
.end method

.method public b()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 285
    invoke-direct {p0}, Le/a/a/a/d;->p()Z

    .line 286
    invoke-direct {p0}, Le/a/a/a/d;->l()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {p0, v0}, Le/a/a/a/d;->a(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/widget/ImageView;
    .locals 4

    .prologue
    .line 331
    const/4 v0, 0x0

    .line 333
    iget-object v1, p0, Le/a/a/a/d;->j:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 334
    iget-object v0, p0, Le/a/a/a/d;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 338
    :cond_0
    if-nez v0, :cond_1

    .line 339
    invoke-virtual {p0}, Le/a/a/a/d;->a()V

    .line 340
    invoke-static {}, Le/a/a/a/b/a;->a()Le/a/a/a/b/b;

    move-result-object v1

    const-string v2, "PhotoViewAttacher"

    const-string v3, "ImageView no longer exists. You should not use this PhotoViewAttacher any more."

    invoke-interface {v1, v2, v3}, Le/a/a/a/b/b;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_1
    return-object v0
.end method

.method public d()F
    .locals 1

    .prologue
    .line 349
    iget v0, p0, Le/a/a/a/d;->e:F

    return v0
.end method

.method public e()F
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Le/a/a/a/d;->f:F

    return v0
.end method

.method public f()F
    .locals 1

    .prologue
    .line 359
    iget v0, p0, Le/a/a/a/d;->g:F

    return v0
.end method

.method public g()F
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 364
    iget-object v0, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Le/a/a/a/d;->a(Landroid/graphics/Matrix;I)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iget-object v1, p0, Le/a/a/a/d;->o:Landroid/graphics/Matrix;

    const/4 v2, 0x3

    invoke-direct {p0, v1, v2}, Le/a/a/a/d;->a(Landroid/graphics/Matrix;I)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method h()Le/a/a/a/d$d;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Le/a/a/a/d;->s:Le/a/a/a/d$d;

    return-object v0
.end method

.method i()Le/a/a/a/d$g;
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Le/a/a/a/d;->t:Le/a/a/a/d$g;

    return-object v0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 669
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 671
    if-eqz v0, :cond_0

    .line 672
    iget-boolean v1, p0, Le/a/a/a/d;->E:Z

    if-eqz v1, :cond_1

    .line 674
    invoke-static {v0}, Le/a/a/a/d;->b(Landroid/widget/ImageView;)V

    .line 677
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Le/a/a/a/d;->a(Landroid/graphics/drawable/Drawable;)V

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    invoke-direct {p0}, Le/a/a/a/d;->q()V

    goto :goto_0
.end method

.method public onGlobalLayout()V
    .locals 6

    .prologue
    .line 430
    invoke-virtual {p0}, Le/a/a/a/d;->c()Landroid/widget/ImageView;

    move-result-object v0

    .line 432
    if-eqz v0, :cond_1

    .line 433
    iget-boolean v1, p0, Le/a/a/a/d;->E:Z

    if-eqz v1, :cond_2

    .line 434
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v1

    .line 435
    invoke-virtual {v0}, Landroid/widget/ImageView;->getRight()I

    move-result v2

    .line 436
    invoke-virtual {v0}, Landroid/widget/ImageView;->getBottom()I

    move-result v3

    .line 437
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v4

    .line 446
    iget v5, p0, Le/a/a/a/d;->x:I

    if-ne v1, v5, :cond_0

    iget v5, p0, Le/a/a/a/d;->z:I

    if-ne v3, v5, :cond_0

    iget v5, p0, Le/a/a/a/d;->A:I

    if-ne v4, v5, :cond_0

    iget v5, p0, Le/a/a/a/d;->y:I

    if-eq v2, v5, :cond_1

    .line 449
    :cond_0
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Le/a/a/a/d;->a(Landroid/graphics/drawable/Drawable;)V

    .line 452
    iput v1, p0, Le/a/a/a/d;->x:I

    .line 453
    iput v2, p0, Le/a/a/a/d;->y:I

    .line 454
    iput v3, p0, Le/a/a/a/d;->z:I

    .line 455
    iput v4, p0, Le/a/a/a/d;->A:I

    .line 461
    :cond_1
    :goto_0
    return-void

    .line 458
    :cond_2
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Le/a/a/a/d;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 484
    .line 486
    iget-boolean v0, p0, Le/a/a/a/d;->E:Z

    if-eqz v0, :cond_7

    move-object v0, p1

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Le/a/a/a/d;->a(Landroid/widget/ImageView;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 487
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 488
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v6

    .line 519
    :goto_0
    iget-object v1, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    if-eqz v1, :cond_2

    .line 520
    iget-object v0, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    invoke-interface {v0}, Le/a/a/a/a/d;->a()Z

    move-result v1

    .line 521
    iget-object v0, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    invoke-interface {v0}, Le/a/a/a/a/d;->b()Z

    move-result v3

    .line 523
    iget-object v0, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    invoke-interface {v0, p2}, Le/a/a/a/a/d;->c(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 525
    if-nez v1, :cond_5

    iget-object v1, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    invoke-interface {v1}, Le/a/a/a/a/d;->a()Z

    move-result v1

    if-nez v1, :cond_5

    move v2, v7

    .line 526
    :goto_1
    if-nez v3, :cond_6

    iget-object v1, p0, Le/a/a/a/d;->l:Le/a/a/a/a/d;

    invoke-interface {v1}, Le/a/a/a/a/d;->b()Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v7

    .line 528
    :goto_2
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    move v6, v7

    :cond_1
    iput-boolean v6, p0, Le/a/a/a/d;->i:Z

    .line 532
    :cond_2
    iget-object v1, p0, Le/a/a/a/d;->k:Landroid/view/GestureDetector;

    if-eqz v1, :cond_3

    iget-object v1, p0, Le/a/a/a/d;->k:Landroid/view/GestureDetector;

    invoke-virtual {v1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v7

    .line 538
    :cond_3
    :goto_3
    return v0

    .line 492
    :pswitch_1
    if-eqz v0, :cond_4

    .line 493
    invoke-interface {v0, v7}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 500
    :goto_4
    invoke-direct {p0}, Le/a/a/a/d;->m()V

    move v0, v6

    .line 501
    goto :goto_0

    .line 495
    :cond_4
    invoke-static {}, Le/a/a/a/b/a;->a()Le/a/a/a/b/b;

    move-result-object v0

    const-string v1, "PhotoViewAttacher"

    const-string v2, "onTouch getParent() returned null"

    invoke-interface {v0, v1, v2}, Le/a/a/a/b/b;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 507
    :pswitch_2
    invoke-virtual {p0}, Le/a/a/a/d;->g()F

    move-result v0

    iget v1, p0, Le/a/a/a/d;->e:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 508
    invoke-virtual {p0}, Le/a/a/a/d;->b()Landroid/graphics/RectF;

    move-result-object v1

    .line 509
    if-eqz v1, :cond_0

    .line 510
    new-instance v0, Le/a/a/a/d$a;

    invoke-virtual {p0}, Le/a/a/a/d;->g()F

    move-result v2

    iget v3, p0, Le/a/a/a/d;->e:F

    .line 511
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Le/a/a/a/d$a;-><init>(Le/a/a/a/d;FFFF)V

    .line 510
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    move v0, v7

    .line 512
    goto :goto_0

    :cond_5
    move v2, v6

    .line 525
    goto :goto_1

    :cond_6
    move v1, v6

    .line 526
    goto :goto_2

    :cond_7
    move v0, v6

    goto :goto_3

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
