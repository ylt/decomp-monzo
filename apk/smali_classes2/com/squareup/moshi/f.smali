.class abstract Lcom/squareup/moshi/f;
.super Lcom/squareup/moshi/i;
.source "CollectionJsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Ljava/util/Collection",
        "<TT;>;T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/moshi/i",
        "<TC;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/squareup/moshi/i$a;


# instance fields
.field private final b:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/squareup/moshi/f$1;

    invoke-direct {v0}, Lcom/squareup/moshi/f$1;-><init>()V

    sput-object v0, Lcom/squareup/moshi/f;->a:Lcom/squareup/moshi/i$a;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/moshi/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/i",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/squareup/moshi/i;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/moshi/f;->b:Lcom/squareup/moshi/i;

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/moshi/i;Lcom/squareup/moshi/f$1;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/moshi/f;-><init>(Lcom/squareup/moshi/i;)V

    return-void
.end method

.method static a(Ljava/lang/reflect/Type;Lcom/squareup/moshi/v;)Lcom/squareup/moshi/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/squareup/moshi/v;",
            ")",
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/util/Collection",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 51
    const-class v0, Ljava/util/Collection;

    invoke-static {p0, v0}, Lcom/squareup/moshi/y;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 52
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/v;->a(Ljava/lang/reflect/Type;)Lcom/squareup/moshi/i;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/squareup/moshi/f$2;

    invoke-direct {v1, v0}, Lcom/squareup/moshi/f$2;-><init>(Lcom/squareup/moshi/i;)V

    return-object v1
.end method

.method static b(Ljava/lang/reflect/Type;Lcom/squareup/moshi/v;)Lcom/squareup/moshi/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/squareup/moshi/v;",
            ")",
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 61
    const-class v0, Ljava/util/Collection;

    invoke-static {p0, v0}, Lcom/squareup/moshi/y;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 62
    invoke-virtual {p1, v0}, Lcom/squareup/moshi/v;->a(Ljava/lang/reflect/Type;)Lcom/squareup/moshi/i;

    move-result-object v0

    .line 63
    new-instance v1, Lcom/squareup/moshi/f$3;

    invoke-direct {v1, v0}, Lcom/squareup/moshi/f$3;-><init>(Lcom/squareup/moshi/i;)V

    return-object v1
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/f;->b(Lcom/squareup/moshi/k;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method abstract a()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TC;"
        }
    .end annotation
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    check-cast p2, Ljava/util/Collection;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/moshi/f;->a(Lcom/squareup/moshi/p;Ljava/util/Collection;)V

    return-void
.end method

.method public a(Lcom/squareup/moshi/p;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/p;",
            "TC;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->a()Lcom/squareup/moshi/p;

    .line 84
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 85
    iget-object v2, p0, Lcom/squareup/moshi/f;->b:Lcom/squareup/moshi/i;

    invoke-virtual {v2, p1, v1}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->b()Lcom/squareup/moshi/p;

    .line 88
    return-void
.end method

.method public b(Lcom/squareup/moshi/k;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/k;",
            ")TC;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/squareup/moshi/f;->a()Ljava/util/Collection;

    move-result-object v0

    .line 74
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->c()V

    .line 75
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/squareup/moshi/f;->b:Lcom/squareup/moshi/i;

    invoke-virtual {v1, p1}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->d()V

    .line 79
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/moshi/f;->b:Lcom/squareup/moshi/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".collection()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
