.class Lcom/squareup/moshi/i$3;
.super Lcom/squareup/moshi/i;
.source "JsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/moshi/i;->e()Lcom/squareup/moshi/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/squareup/moshi/i;

.field final synthetic b:Lcom/squareup/moshi/i;


# direct methods
.method constructor <init>(Lcom/squareup/moshi/i;Lcom/squareup/moshi/i;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/squareup/moshi/i$3;->b:Lcom/squareup/moshi/i;

    iput-object p2, p0, Lcom/squareup/moshi/i$3;->a:Lcom/squareup/moshi/i;

    invoke-direct {p0}, Lcom/squareup/moshi/i;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/k;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->a()Z

    move-result v1

    .line 150
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/moshi/k;->a(Z)V

    .line 152
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/i$3;->a:Lcom/squareup/moshi/i;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 154
    invoke-virtual {p1, v1}, Lcom/squareup/moshi/k;->a(Z)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Lcom/squareup/moshi/k;->a(Z)V

    throw v0
.end method

.method public a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/p;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->g()Z

    move-result v1

    .line 159
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/moshi/p;->b(Z)V

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/i$3;->a:Lcom/squareup/moshi/i;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    invoke-virtual {p1, v1}, Lcom/squareup/moshi/p;->b(Z)V

    .line 165
    return-void

    .line 163
    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Lcom/squareup/moshi/p;->b(Z)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/moshi/i$3;->a:Lcom/squareup/moshi/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".lenient()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
