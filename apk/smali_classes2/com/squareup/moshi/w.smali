.class final Lcom/squareup/moshi/w;
.super Ljava/lang/Object;
.source "StandardJsonAdapters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/moshi/w$b;,
        Lcom/squareup/moshi/w$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/squareup/moshi/i$a;

.field static final b:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/squareup/moshi/w$1;

    invoke-direct {v0}, Lcom/squareup/moshi/w$1;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->a:Lcom/squareup/moshi/i$a;

    .line 74
    new-instance v0, Lcom/squareup/moshi/w$3;

    invoke-direct {v0}, Lcom/squareup/moshi/w$3;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->b:Lcom/squareup/moshi/i;

    .line 88
    new-instance v0, Lcom/squareup/moshi/w$4;

    invoke-direct {v0}, Lcom/squareup/moshi/w$4;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->c:Lcom/squareup/moshi/i;

    .line 102
    new-instance v0, Lcom/squareup/moshi/w$5;

    invoke-direct {v0}, Lcom/squareup/moshi/w$5;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->d:Lcom/squareup/moshi/i;

    .line 121
    new-instance v0, Lcom/squareup/moshi/w$6;

    invoke-direct {v0}, Lcom/squareup/moshi/w$6;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->e:Lcom/squareup/moshi/i;

    .line 135
    new-instance v0, Lcom/squareup/moshi/w$7;

    invoke-direct {v0}, Lcom/squareup/moshi/w$7;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->f:Lcom/squareup/moshi/i;

    .line 160
    new-instance v0, Lcom/squareup/moshi/w$8;

    invoke-direct {v0}, Lcom/squareup/moshi/w$8;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->g:Lcom/squareup/moshi/i;

    .line 174
    new-instance v0, Lcom/squareup/moshi/w$9;

    invoke-direct {v0}, Lcom/squareup/moshi/w$9;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->h:Lcom/squareup/moshi/i;

    .line 188
    new-instance v0, Lcom/squareup/moshi/w$10;

    invoke-direct {v0}, Lcom/squareup/moshi/w$10;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->i:Lcom/squareup/moshi/i;

    .line 202
    new-instance v0, Lcom/squareup/moshi/w$2;

    invoke-direct {v0}, Lcom/squareup/moshi/w$2;-><init>()V

    sput-object v0, Lcom/squareup/moshi/w;->j:Lcom/squareup/moshi/i;

    return-void
.end method

.method static a(Lcom/squareup/moshi/k;Ljava/lang/String;II)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/squareup/moshi/k;->o()I

    move-result v0

    .line 67
    if-lt v0, p2, :cond_0

    if-le v0, p3, :cond_1

    .line 68
    :cond_0
    new-instance v1, Lcom/squareup/moshi/JsonDataException;

    const-string v2, "Expected %s but was %s at path %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 69
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/squareup/moshi/k;->r()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 71
    :cond_1
    return v0
.end method
