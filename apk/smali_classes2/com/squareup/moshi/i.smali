.class public abstract Lcom/squareup/moshi/i;
.super Ljava/lang/Object;
.source "JsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/moshi/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lc/e;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/e;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 35
    invoke-static {p1}, Lcom/squareup/moshi/k;->a(Lc/e;)Lcom/squareup/moshi/k;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/k;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lcom/squareup/moshi/o;

    invoke-direct {v0, p1}, Lcom/squareup/moshi/o;-><init>(Ljava/lang/Object;)V

    .line 86
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public abstract a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/p;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final c()Lcom/squareup/moshi/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/moshi/i",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 97
    .line 98
    new-instance v0, Lcom/squareup/moshi/i$1;

    invoke-direct {v0, p0, p0}, Lcom/squareup/moshi/i$1;-><init>(Lcom/squareup/moshi/i;Lcom/squareup/moshi/i;)V

    return-object v0
.end method

.method public final d()Lcom/squareup/moshi/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/moshi/i",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 122
    .line 123
    new-instance v0, Lcom/squareup/moshi/i$2;

    invoke-direct {v0, p0, p0}, Lcom/squareup/moshi/i$2;-><init>(Lcom/squareup/moshi/i;Lcom/squareup/moshi/i;)V

    return-object v0
.end method

.method public final e()Lcom/squareup/moshi/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/moshi/i",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 146
    .line 147
    new-instance v0, Lcom/squareup/moshi/i$3;

    invoke-direct {v0, p0, p0}, Lcom/squareup/moshi/i$3;-><init>(Lcom/squareup/moshi/i;Lcom/squareup/moshi/i;)V

    return-object v0
.end method

.method public final f()Lcom/squareup/moshi/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/moshi/i",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 179
    .line 180
    new-instance v0, Lcom/squareup/moshi/i$4;

    invoke-direct {v0, p0, p0}, Lcom/squareup/moshi/i$4;-><init>(Lcom/squareup/moshi/i;Lcom/squareup/moshi/i;)V

    return-object v0
.end method
