.class abstract Lcom/squareup/moshi/b$a;
.super Ljava/lang/Object;
.source "AdapterMethodsFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "a"
.end annotation


# instance fields
.field final d:Ljava/lang/reflect/Type;

.field final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field final f:Ljava/lang/Object;

.field final g:Ljava/lang/reflect/Method;

.field final h:I

.field final i:[Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/squareup/moshi/i",
            "<*>;"
        }
    .end annotation
.end field

.field final j:Z


# direct methods
.method constructor <init>(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/Object;Ljava/lang/reflect/Method;IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/util/Set",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Object;",
            "Ljava/lang/reflect/Method;",
            "IIZ)V"
        }
    .end annotation

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    invoke-static {p1}, Lcom/squareup/moshi/y;->d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/b$a;->d:Ljava/lang/reflect/Type;

    .line 287
    iput-object p2, p0, Lcom/squareup/moshi/b$a;->e:Ljava/util/Set;

    .line 288
    iput-object p3, p0, Lcom/squareup/moshi/b$a;->f:Ljava/lang/Object;

    .line 289
    iput-object p4, p0, Lcom/squareup/moshi/b$a;->g:Ljava/lang/reflect/Method;

    .line 290
    iput p6, p0, Lcom/squareup/moshi/b$a;->h:I

    .line 291
    sub-int v0, p5, p6

    new-array v0, v0, [Lcom/squareup/moshi/i;

    iput-object v0, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    .line 292
    iput-boolean p7, p0, Lcom/squareup/moshi/b$a;->j:Z

    .line 293
    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 317
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 322
    iget-object v0, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 323
    aput-object p1, v0, v4

    .line 324
    iget-object v1, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    array-length v3, v3

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 327
    :try_start_0
    iget-object v1, p0, Lcom/squareup/moshi/b$a;->g:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/squareup/moshi/b$a;->f:Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 328
    :catch_0
    move-exception v0

    .line 329
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 336
    iget-object v0, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 337
    aput-object p1, v0, v4

    .line 338
    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 339
    iget-object v1, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    array-length v3, v3

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 342
    :try_start_0
    iget-object v1, p0, Lcom/squareup/moshi/b$a;->g:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/squareup/moshi/b$a;->f:Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 343
    :catch_0
    move-exception v0

    .line 344
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/i$a;)V
    .locals 9

    .prologue
    .line 296
    iget-object v0, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/squareup/moshi/b$a;->g:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getGenericParameterTypes()[Ljava/lang/reflect/Type;

    move-result-object v2

    .line 298
    iget-object v0, p0, Lcom/squareup/moshi/b$a;->g:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v3

    .line 299
    iget v0, p0, Lcom/squareup/moshi/b$a;->h:I

    array-length v4, v2

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 300
    aget-object v0, v2, v1

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v5, 0x0

    aget-object v0, v0, v5

    .line 301
    aget-object v5, v3, v1

    invoke-static {v5}, Lcom/squareup/moshi/z;->a([Ljava/lang/annotation/Annotation;)Ljava/util/Set;

    move-result-object v5

    .line 302
    iget-object v6, p0, Lcom/squareup/moshi/b$a;->i:[Lcom/squareup/moshi/i;

    iget v7, p0, Lcom/squareup/moshi/b$a;->h:I

    sub-int v7, v1, v7

    iget-object v8, p0, Lcom/squareup/moshi/b$a;->d:Ljava/lang/reflect/Type;

    .line 303
    invoke-static {v8, v0}, Lcom/squareup/moshi/y;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/squareup/moshi/b$a;->e:Ljava/util/Set;

    invoke-interface {v8, v5}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 304
    invoke-virtual {p1, p2, v0, v5}, Lcom/squareup/moshi/v;->a(Lcom/squareup/moshi/i$a;Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/i;

    move-result-object v0

    .line 305
    :goto_1
    aput-object v0, v6, v7

    .line 299
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 305
    :cond_0
    invoke-virtual {p1, v0, v5}, Lcom/squareup/moshi/v;->a(Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/i;

    move-result-object v0

    goto :goto_1

    .line 308
    :cond_1
    return-void
.end method

.method public a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 1
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 312
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
