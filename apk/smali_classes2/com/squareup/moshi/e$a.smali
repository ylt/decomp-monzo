.class Lcom/squareup/moshi/e$a;
.super Ljava/lang/Object;
.source "ClassJsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/reflect/Field;

.field final c:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/reflect/Field;Lcom/squareup/moshi/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Field;",
            "Lcom/squareup/moshi/i",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iput-object p1, p0, Lcom/squareup/moshi/e$a;->a:Ljava/lang/String;

    .line 200
    iput-object p2, p0, Lcom/squareup/moshi/e$a;->b:Ljava/lang/reflect/Field;

    .line 201
    iput-object p3, p0, Lcom/squareup/moshi/e$a;->c:Lcom/squareup/moshi/i;

    .line 202
    return-void
.end method


# virtual methods
.method a(Lcom/squareup/moshi/k;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/squareup/moshi/e$a;->c:Lcom/squareup/moshi/i;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lcom/squareup/moshi/e$a;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 207
    return-void
.end method

.method a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/squareup/moshi/e$a;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 212
    iget-object v1, p0, Lcom/squareup/moshi/e$a;->c:Lcom/squareup/moshi/i;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V

    .line 213
    return-void
.end method
