.class final Lcom/squareup/moshi/w$b;
.super Lcom/squareup/moshi/i;
.source "StandardJsonAdapters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/moshi/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/i",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/squareup/moshi/v;


# direct methods
.method constructor <init>(Lcom/squareup/moshi/v;)V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/squareup/moshi/i;-><init>()V

    .line 271
    iput-object p1, p0, Lcom/squareup/moshi/w$b;->a:Lcom/squareup/moshi/v;

    .line 272
    return-void
.end method

.method private a(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 297
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-class p1, Ljava/util/Map;

    .line 299
    :cond_0
    :goto_0
    return-object p1

    .line 298
    :cond_1
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class p1, Ljava/util/Collection;

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->q()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 279
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 280
    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 282
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->c()Lcom/squareup/moshi/p;

    .line 283
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->d()Lcom/squareup/moshi/p;

    .line 287
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/squareup/moshi/w$b;->a:Lcom/squareup/moshi/v;

    invoke-direct {p0, v0}, Lcom/squareup/moshi/w$b;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Lcom/squareup/moshi/z;->a:Ljava/util/Set;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/moshi/v;->a(Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/i;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    const-string v0, "JsonAdapter(Object)"

    return-object v0
.end method
