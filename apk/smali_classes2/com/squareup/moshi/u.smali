.class final Lcom/squareup/moshi/u;
.super Lcom/squareup/moshi/i;
.source "MapJsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/moshi/i",
        "<",
        "Ljava/util/Map",
        "<TK;TV;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/squareup/moshi/i$a;


# instance fields
.field private final b:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/squareup/moshi/u$1;

    invoke-direct {v0}, Lcom/squareup/moshi/u$1;-><init>()V

    sput-object v0, Lcom/squareup/moshi/u;->a:Lcom/squareup/moshi/i$a;

    return-void
.end method

.method constructor <init>(Lcom/squareup/moshi/v;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/squareup/moshi/i;-><init>()V

    .line 46
    invoke-virtual {p1, p2}, Lcom/squareup/moshi/v;->a(Ljava/lang/reflect/Type;)Lcom/squareup/moshi/i;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/u;->b:Lcom/squareup/moshi/i;

    .line 47
    invoke-virtual {p1, p3}, Lcom/squareup/moshi/v;->a(Ljava/lang/reflect/Type;)Lcom/squareup/moshi/i;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/u;->c:Lcom/squareup/moshi/i;

    .line 48
    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/squareup/moshi/u;->b(Lcom/squareup/moshi/k;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    check-cast p2, Ljava/util/Map;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/moshi/u;->a(Lcom/squareup/moshi/p;Ljava/util/Map;)V

    return-void
.end method

.method public a(Lcom/squareup/moshi/p;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/p;",
            "Ljava/util/Map",
            "<TK;TV;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->c()Lcom/squareup/moshi/p;

    .line 52
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 53
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 54
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Map key is null at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/moshi/p;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->i()V

    .line 57
    iget-object v2, p0, Lcom/squareup/moshi/u;->b:Lcom/squareup/moshi/i;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V

    .line 58
    iget-object v2, p0, Lcom/squareup/moshi/u;->c:Lcom/squareup/moshi/i;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->d()Lcom/squareup/moshi/p;

    .line 61
    return-void
.end method

.method public b(Lcom/squareup/moshi/k;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/k;",
            ")",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lcom/squareup/moshi/t;

    invoke-direct {v0}, Lcom/squareup/moshi/t;-><init>()V

    .line 65
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->e()V

    .line 66
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->s()V

    .line 68
    iget-object v1, p0, Lcom/squareup/moshi/u;->b:Lcom/squareup/moshi/i;

    invoke-virtual {v1, p1}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lcom/squareup/moshi/u;->c:Lcom/squareup/moshi/i;

    invoke-virtual {v2, p1}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;

    move-result-object v2

    .line 70
    invoke-virtual {v0, v1, v2}, Lcom/squareup/moshi/t;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 71
    if-eqz v3, :cond_0

    .line 72
    new-instance v0, Lcom/squareup/moshi/JsonDataException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Map key \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\' has multiple values at path "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " and "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->f()V

    .line 77
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JsonAdapter("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/moshi/u;->b:Lcom/squareup/moshi/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/moshi/u;->c:Lcom/squareup/moshi/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
