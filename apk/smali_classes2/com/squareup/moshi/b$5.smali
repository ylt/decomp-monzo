.class final Lcom/squareup/moshi/b$5;
.super Lcom/squareup/moshi/b$a;
.source "AdapterMethodsFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/moshi/b;->b(Ljava/lang/Object;Ljava/lang/reflect/Method;)Lcom/squareup/moshi/b$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field a:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:[Ljava/lang/reflect/Type;

.field final synthetic c:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/Object;Ljava/lang/reflect/Method;IIZ[Ljava/lang/reflect/Type;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 238
    iput-object p8, p0, Lcom/squareup/moshi/b$5;->b:[Ljava/lang/reflect/Type;

    iput-object p9, p0, Lcom/squareup/moshi/b$5;->c:Ljava/util/Set;

    invoke-direct/range {p0 .. p7}, Lcom/squareup/moshi/b$a;-><init>(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/Object;Ljava/lang/reflect/Method;IIZ)V

    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/squareup/moshi/b$5;->a:Lcom/squareup/moshi/i;

    invoke-virtual {v0, p2}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;

    move-result-object v0

    .line 249
    invoke-virtual {p0, v0}, Lcom/squareup/moshi/b$5;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/i$a;)V
    .locals 2

    .prologue
    .line 242
    invoke-super {p0, p1, p2}, Lcom/squareup/moshi/b$a;->a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/i$a;)V

    .line 243
    iget-object v0, p0, Lcom/squareup/moshi/b$5;->b:[Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/squareup/moshi/b$5;->c:Ljava/util/Set;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/moshi/v;->a(Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/i;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/b$5;->a:Lcom/squareup/moshi/i;

    .line 244
    return-void
.end method
