.class final Lcom/squareup/moshi/b$3;
.super Lcom/squareup/moshi/b$a;
.source "AdapterMethodsFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/moshi/b;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;)Lcom/squareup/moshi/b$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/reflect/Type;

.field final synthetic b:Ljava/util/Set;

.field private c:Lcom/squareup/moshi/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/i",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/Object;Ljava/lang/reflect/Method;IIZLjava/lang/reflect/Type;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 171
    iput-object p8, p0, Lcom/squareup/moshi/b$3;->a:Ljava/lang/reflect/Type;

    iput-object p9, p0, Lcom/squareup/moshi/b$3;->b:Ljava/util/Set;

    invoke-direct/range {p0 .. p7}, Lcom/squareup/moshi/b$a;-><init>(Ljava/lang/reflect/Type;Ljava/util/Set;Ljava/lang/Object;Ljava/lang/reflect/Method;IIZ)V

    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/i$a;)V
    .locals 2

    .prologue
    .line 175
    invoke-super {p0, p1, p2}, Lcom/squareup/moshi/b$a;->a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/i$a;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/moshi/b$3;->a:Ljava/lang/reflect/Type;

    iget-object v1, p0, Lcom/squareup/moshi/b$3;->b:Ljava/util/Set;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/moshi/v;->a(Ljava/lang/reflect/Type;Ljava/util/Set;)Lcom/squareup/moshi/i;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/b$3;->c:Lcom/squareup/moshi/i;

    .line 177
    return-void
.end method

.method public a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    .line 181
    invoke-virtual {p0, p3}, Lcom/squareup/moshi/b$3;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/squareup/moshi/b$3;->c:Lcom/squareup/moshi/i;

    invoke-virtual {v1, p2, v0}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V

    .line 183
    return-void
.end method
