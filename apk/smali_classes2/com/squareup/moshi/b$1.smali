.class Lcom/squareup/moshi/b$1;
.super Lcom/squareup/moshi/i;
.source "AdapterMethodsFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/moshi/b;->a(Ljava/lang/reflect/Type;Ljava/util/Set;Lcom/squareup/moshi/v;)Lcom/squareup/moshi/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/moshi/i",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/squareup/moshi/b$a;

.field final synthetic b:Lcom/squareup/moshi/i;

.field final synthetic c:Lcom/squareup/moshi/v;

.field final synthetic d:Lcom/squareup/moshi/b$a;

.field final synthetic e:Ljava/util/Set;

.field final synthetic f:Ljava/lang/reflect/Type;

.field final synthetic g:Lcom/squareup/moshi/b;


# direct methods
.method constructor <init>(Lcom/squareup/moshi/b;Lcom/squareup/moshi/b$a;Lcom/squareup/moshi/i;Lcom/squareup/moshi/v;Lcom/squareup/moshi/b$a;Ljava/util/Set;Ljava/lang/reflect/Type;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/squareup/moshi/b$1;->g:Lcom/squareup/moshi/b;

    iput-object p2, p0, Lcom/squareup/moshi/b$1;->a:Lcom/squareup/moshi/b$a;

    iput-object p3, p0, Lcom/squareup/moshi/b$1;->b:Lcom/squareup/moshi/i;

    iput-object p4, p0, Lcom/squareup/moshi/b$1;->c:Lcom/squareup/moshi/v;

    iput-object p5, p0, Lcom/squareup/moshi/b$1;->d:Lcom/squareup/moshi/b$a;

    iput-object p6, p0, Lcom/squareup/moshi/b$1;->e:Ljava/util/Set;

    iput-object p7, p0, Lcom/squareup/moshi/b$1;->f:Ljava/lang/reflect/Type;

    invoke-direct {p0}, Lcom/squareup/moshi/i;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/squareup/moshi/b$1;->d:Lcom/squareup/moshi/b$a;

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/moshi/b$1;->b:Lcom/squareup/moshi/i;

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/k;)Ljava/lang/Object;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/b$1;->d:Lcom/squareup/moshi/b$a;

    iget-boolean v0, v0, Lcom/squareup/moshi/b$a;->j:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/moshi/k;->h()Lcom/squareup/moshi/k$b;

    move-result-object v0

    sget-object v1, Lcom/squareup/moshi/k$b;->i:Lcom/squareup/moshi/k$b;

    if-ne v0, v1, :cond_1

    .line 83
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->l()Ljava/lang/Object;

    .line 84
    const/4 v0, 0x0

    goto :goto_0

    .line 87
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/b$1;->d:Lcom/squareup/moshi/b$a;

    iget-object v1, p0, Lcom/squareup/moshi/b$1;->c:Lcom/squareup/moshi/v;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/moshi/b$a;->a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/k;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 90
    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 91
    :cond_2
    new-instance v1, Lcom/squareup/moshi/JsonDataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/moshi/k;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/squareup/moshi/b$1;->a:Lcom/squareup/moshi/b$a;

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/squareup/moshi/b$1;->b:Lcom/squareup/moshi/i;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/moshi/i;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V

    .line 77
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/squareup/moshi/b$1;->a:Lcom/squareup/moshi/b$a;

    iget-boolean v0, v0, Lcom/squareup/moshi/b$a;->j:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->e()Lcom/squareup/moshi/p;

    goto :goto_0

    .line 70
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/b$1;->a:Lcom/squareup/moshi/b$a;

    iget-object v1, p0, Lcom/squareup/moshi/b$1;->c:Lcom/squareup/moshi/v;

    invoke-virtual {v0, v1, p1, p2}, Lcom/squareup/moshi/b$a;->a(Lcom/squareup/moshi/v;Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 73
    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 74
    :cond_2
    new-instance v1, Lcom/squareup/moshi/JsonDataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/moshi/p;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/squareup/moshi/JsonDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JsonAdapter"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/moshi/b$1;->e:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/moshi/b$1;->f:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
