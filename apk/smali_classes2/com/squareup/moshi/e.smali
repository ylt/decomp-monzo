.class final Lcom/squareup/moshi/e;
.super Lcom/squareup/moshi/i;
.source "ClassJsonAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/moshi/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/moshi/i",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/squareup/moshi/i$a;


# instance fields
.field private final b:Lcom/squareup/moshi/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/moshi/d",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:[Lcom/squareup/moshi/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/squareup/moshi/e$a",
            "<*>;"
        }
    .end annotation
.end field

.field private final d:Lcom/squareup/moshi/k$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/squareup/moshi/e$1;

    invoke-direct {v0}, Lcom/squareup/moshi/e$1;-><init>()V

    sput-object v0, Lcom/squareup/moshi/e;->a:Lcom/squareup/moshi/i$a;

    return-void
.end method

.method constructor <init>(Lcom/squareup/moshi/d;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/d",
            "<TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/squareup/moshi/e$a",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/squareup/moshi/i;-><init>()V

    .line 134
    iput-object p1, p0, Lcom/squareup/moshi/e;->b:Lcom/squareup/moshi/d;

    .line 135
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/moshi/e$a;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/moshi/e$a;

    iput-object v0, p0, Lcom/squareup/moshi/e;->c:[Lcom/squareup/moshi/e$a;

    .line 137
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 136
    invoke-static {v0}, Lcom/squareup/moshi/k$a;->a([Ljava/lang/String;)Lcom/squareup/moshi/k$a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/moshi/e;->d:Lcom/squareup/moshi/k$a;

    .line 138
    return-void
.end method

.method static a(Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 122
    const-string v1, "android."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "java."

    .line 123
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "javax."

    .line 124
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "kotlin."

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "scala."

    .line 126
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/squareup/moshi/k;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/k;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/squareup/moshi/e;->b:Lcom/squareup/moshi/d;

    invoke-virtual {v0}, Lcom/squareup/moshi/d;->a()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 156
    :try_start_1
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->e()V

    .line 157
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 158
    iget-object v1, p0, Lcom/squareup/moshi/e;->d:Lcom/squareup/moshi/k$a;

    invoke-virtual {p1, v1}, Lcom/squareup/moshi/k;->a(Lcom/squareup/moshi/k$a;)I

    move-result v1

    .line 160
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 161
    iget-object v2, p0, Lcom/squareup/moshi/e;->c:[Lcom/squareup/moshi/e$a;

    aget-object v1, v2, v1

    .line 167
    invoke-virtual {v1, p1, v0}, Lcom/squareup/moshi/e$a;->a(Lcom/squareup/moshi/k;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 144
    :catch_1
    move-exception v0

    .line 145
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 146
    :catch_2
    move-exception v0

    .line 147
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v0

    .line 148
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 149
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 150
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 151
    :catch_3
    move-exception v0

    .line 152
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 163
    :cond_2
    :try_start_2
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->i()Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->p()V

    goto :goto_0

    .line 169
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/moshi/k;->f()V
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_0

    .line 170
    return-object v0
.end method

.method public a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/moshi/p;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->c()Lcom/squareup/moshi/p;

    .line 179
    iget-object v1, p0, Lcom/squareup/moshi/e;->c:[Lcom/squareup/moshi/e$a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 180
    iget-object v4, v3, Lcom/squareup/moshi/e$a;->a:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/squareup/moshi/p;->a(Ljava/lang/String;)Lcom/squareup/moshi/p;

    .line 181
    invoke-virtual {v3, p1, p2}, Lcom/squareup/moshi/e$a;->a(Lcom/squareup/moshi/p;Ljava/lang/Object;)V

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/moshi/p;->d()Lcom/squareup/moshi/p;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    return-void

    .line 184
    :catch_0
    move-exception v0

    .line 185
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JsonAdapter("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/moshi/e;->b:Lcom/squareup/moshi/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
