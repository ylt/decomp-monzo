.class public Lcom/stripe/android/model/SourceOwner;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "SourceOwner.java"


# static fields
.field private static final FIELD_ADDRESS:Ljava/lang/String; = "address"

.field private static final FIELD_EMAIL:Ljava/lang/String; = "email"

.field private static final FIELD_NAME:Ljava/lang/String; = "name"

.field private static final FIELD_PHONE:Ljava/lang/String; = "phone"

.field private static final FIELD_VERIFIED_ADDRESS:Ljava/lang/String; = "verified_address"

.field private static final FIELD_VERIFIED_EMAIL:Ljava/lang/String; = "verified_email"

.field private static final FIELD_VERIFIED_NAME:Ljava/lang/String; = "verified_name"

.field private static final FIELD_VERIFIED_PHONE:Ljava/lang/String; = "verified_phone"

.field private static final VERIFIED:Ljava/lang/String; = "verified_"


# instance fields
.field private mAddress:Lcom/stripe/android/model/SourceAddress;

.field private mEmail:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mPhone:Ljava/lang/String;

.field private mVerifiedAddress:Lcom/stripe/android/model/SourceAddress;

.field private mVerifiedEmail:Ljava/lang/String;

.field private mVerifiedName:Ljava/lang/String;

.field private mVerifiedPhone:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/stripe/android/model/SourceAddress;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/SourceAddress;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mAddress:Lcom/stripe/android/model/SourceAddress;

    .line 51
    iput-object p2, p0, Lcom/stripe/android/model/SourceOwner;->mEmail:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/stripe/android/model/SourceOwner;->mName:Ljava/lang/String;

    .line 53
    iput-object p4, p0, Lcom/stripe/android/model/SourceOwner;->mPhone:Ljava/lang/String;

    .line 54
    iput-object p5, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedAddress:Lcom/stripe/android/model/SourceAddress;

    .line 55
    iput-object p6, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedEmail:Ljava/lang/String;

    .line 56
    iput-object p7, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedName:Ljava/lang/String;

    .line 57
    iput-object p8, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedPhone:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceOwner;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 181
    if-nez p0, :cond_0

    .line 210
    :goto_0
    return-object v0

    .line 194
    :cond_0
    const-string v1, "address"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 195
    if-eqz v1, :cond_2

    .line 196
    invoke-static {v1}, Lcom/stripe/android/model/SourceAddress;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceAddress;

    move-result-object v1

    .line 198
    :goto_1
    const-string v2, "email"

    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 199
    const-string v3, "name"

    invoke-static {p0, v3}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 200
    const-string v4, "phone"

    invoke-static {p0, v4}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 202
    const-string v5, "verified_address"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 203
    if-eqz v5, :cond_1

    .line 204
    invoke-static {v5}, Lcom/stripe/android/model/SourceAddress;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceAddress;

    move-result-object v5

    .line 206
    :goto_2
    const-string v0, "verified_email"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 207
    const-string v0, "verified_name"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 208
    const-string v0, "verified_phone"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 210
    new-instance v0, Lcom/stripe/android/model/SourceOwner;

    invoke-direct/range {v0 .. v8}, Lcom/stripe/android/model/SourceOwner;-><init>(Lcom/stripe/android/model/SourceAddress;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/model/SourceAddress;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v5, v0

    goto :goto_2

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method public static fromString(Ljava/lang/String;)Lcom/stripe/android/model/SourceOwner;
    .locals 1

    .prologue
    .line 173
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/stripe/android/model/SourceOwner;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceOwner;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAddress()Lcom/stripe/android/model/SourceAddress;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mAddress:Lcom/stripe/android/model/SourceAddress;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mPhone:Ljava/lang/String;

    return-object v0
.end method

.method public getVerifiedAddress()Lcom/stripe/android/model/SourceAddress;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedAddress:Lcom/stripe/android/model/SourceAddress;

    return-object v0
.end method

.method public getVerifiedEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getVerifiedName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedName:Ljava/lang/String;

    return-object v0
.end method

.method public getVerifiedPhone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedPhone:Ljava/lang/String;

    return-object v0
.end method

.method setAddress(Lcom/stripe/android/model/SourceAddress;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mAddress:Lcom/stripe/android/model/SourceAddress;

    .line 94
    return-void
.end method

.method setEmail(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mEmail:Ljava/lang/String;

    .line 98
    return-void
.end method

.method setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mName:Ljava/lang/String;

    .line 102
    return-void
.end method

.method setPhone(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mPhone:Ljava/lang/String;

    .line 106
    return-void
.end method

.method setVerifiedAddress(Lcom/stripe/android/model/SourceAddress;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedAddress:Lcom/stripe/android/model/SourceAddress;

    .line 110
    return-void
.end method

.method setVerifiedEmail(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedEmail:Ljava/lang/String;

    .line 114
    return-void
.end method

.method setVerifiedName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedName:Ljava/lang/String;

    .line 118
    return-void
.end method

.method setVerifiedPhone(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedPhone:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 147
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 148
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mAddress:Lcom/stripe/android/model/SourceAddress;

    if-nez v0, :cond_2

    move-object v0, v1

    .line 149
    :goto_0
    iget-object v3, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedAddress:Lcom/stripe/android/model/SourceAddress;

    if-nez v3, :cond_3

    .line 153
    :goto_1
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 154
    const-string v3, "address"

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 156
    :cond_0
    const-string v0, "email"

    iget-object v3, p0, Lcom/stripe/android/model/SourceOwner;->mEmail:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v0, "name"

    iget-object v3, p0, Lcom/stripe/android/model/SourceOwner;->mName:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v0, "phone"

    iget-object v3, p0, Lcom/stripe/android/model/SourceOwner;->mPhone:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 160
    const-string v0, "verified_address"

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 162
    :cond_1
    const-string v0, "verified_email"

    iget-object v1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedEmail:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v0, "verified_name"

    iget-object v1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedName:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v0, "verified_phone"

    iget-object v1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedPhone:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_2
    return-object v2

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/stripe/android/model/SourceOwner;->mAddress:Lcom/stripe/android/model/SourceAddress;

    invoke-virtual {v0}, Lcom/stripe/android/model/SourceAddress;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedAddress:Lcom/stripe/android/model/SourceAddress;

    .line 151
    invoke-virtual {v1}, Lcom/stripe/android/model/SourceAddress;->toJson()Lorg/json/JSONObject;

    move-result-object v1

    goto :goto_1

    .line 165
    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 128
    iget-object v1, p0, Lcom/stripe/android/model/SourceOwner;->mAddress:Lcom/stripe/android/model/SourceAddress;

    if-eqz v1, :cond_0

    .line 129
    const-string v1, "address"

    iget-object v2, p0, Lcom/stripe/android/model/SourceOwner;->mAddress:Lcom/stripe/android/model/SourceAddress;

    invoke-virtual {v2}, Lcom/stripe/android/model/SourceAddress;->toMap()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :cond_0
    const-string v1, "email"

    iget-object v2, p0, Lcom/stripe/android/model/SourceOwner;->mEmail:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    const-string v1, "name"

    iget-object v2, p0, Lcom/stripe/android/model/SourceOwner;->mName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const-string v1, "phone"

    iget-object v2, p0, Lcom/stripe/android/model/SourceOwner;->mPhone:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v1, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedAddress:Lcom/stripe/android/model/SourceAddress;

    if-eqz v1, :cond_1

    .line 135
    const-string v1, "verified_address"

    iget-object v2, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedAddress:Lcom/stripe/android/model/SourceAddress;

    invoke-virtual {v2}, Lcom/stripe/android/model/SourceAddress;->toMap()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    :cond_1
    const-string v1, "verified_email"

    iget-object v2, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedEmail:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string v1, "verified_name"

    iget-object v2, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string v1, "verified_phone"

    iget-object v2, p0, Lcom/stripe/android/model/SourceOwner;->mVerifiedPhone:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    invoke-static {v0}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 141
    return-object v0
.end method
