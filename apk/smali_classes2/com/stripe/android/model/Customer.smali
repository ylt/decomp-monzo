.class public Lcom/stripe/android/model/Customer;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "Customer.java"


# static fields
.field private static final FIELD_DATA:Ljava/lang/String; = "data"

.field private static final FIELD_DEFAULT_SOURCE:Ljava/lang/String; = "default_source"

.field private static final FIELD_HAS_MORE:Ljava/lang/String; = "has_more"

.field private static final FIELD_ID:Ljava/lang/String; = "id"

.field private static final FIELD_OBJECT:Ljava/lang/String; = "object"

.field private static final FIELD_SHIPPING:Ljava/lang/String; = "shipping"

.field private static final FIELD_SOURCES:Ljava/lang/String; = "sources"

.field private static final FIELD_TOTAL_COUNT:Ljava/lang/String; = "total_count"

.field private static final FIELD_URL:Ljava/lang/String; = "url"

.field private static final VALUE_CUSTOMER:Ljava/lang/String; = "customer"

.field private static final VALUE_LIST:Ljava/lang/String; = "list"


# instance fields
.field private mDefaultSource:Ljava/lang/String;

.field private mHasMore:Ljava/lang/Boolean;

.field private mId:Ljava/lang/String;

.field private mShippingInformation:Lcom/stripe/android/model/ShippingInformation;

.field private mSources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/stripe/android/model/CustomerSource;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalCount:Ljava/lang/Integer;

.field private mUrl:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/stripe/android/model/Customer;->mSources:Ljava/util/List;

    .line 53
    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/Customer;
    .locals 5

    .prologue
    .line 147
    const-string v0, "object"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    const-string v1, "customer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    const/4 v0, 0x0

    .line 172
    :goto_0
    return-object v0

    .line 151
    :cond_0
    new-instance v1, Lcom/stripe/android/model/Customer;

    invoke-direct {v1}, Lcom/stripe/android/model/Customer;-><init>()V

    .line 152
    const-string v0, "id"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/stripe/android/model/Customer;->mId:Ljava/lang/String;

    .line 153
    const-string v0, "default_source"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/stripe/android/model/Customer;->mDefaultSource:Ljava/lang/String;

    .line 154
    const-string v0, "shipping"

    .line 155
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/model/ShippingInformation;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/ShippingInformation;

    move-result-object v0

    iput-object v0, v1, Lcom/stripe/android/model/Customer;->mShippingInformation:Lcom/stripe/android/model/ShippingInformation;

    .line 156
    const-string v0, "sources"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_2

    const-string v2, "list"

    const-string v3, "object"

    invoke-static {v0, v3}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 158
    const-string v2, "has_more"

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optBoolean(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/stripe/android/model/Customer;->mHasMore:Ljava/lang/Boolean;

    .line 159
    const-string v2, "total_count"

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optInteger(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/stripe/android/model/Customer;->mTotalCount:Ljava/lang/Integer;

    .line 160
    const-string v2, "url"

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/stripe/android/model/Customer;->mUrl:Ljava/lang/String;

    .line 161
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 162
    const-string v3, "data"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 163
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 166
    :try_start_0
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-static {v4}, Lcom/stripe/android/model/CustomerSource;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/CustomerSource;

    move-result-object v4

    .line 167
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 170
    :cond_1
    iput-object v2, v1, Lcom/stripe/android/model/Customer;->mSources:Ljava/util/List;

    :cond_2
    move-object v0, v1

    .line 172
    goto :goto_0

    .line 168
    :catch_0
    move-exception v4

    goto :goto_2
.end method

.method public static fromString(Ljava/lang/String;)Lcom/stripe/android/model/Customer;
    .locals 1

    .prologue
    .line 138
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-static {v0}, Lcom/stripe/android/model/Customer;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/Customer;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDefaultSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/stripe/android/model/Customer;->mDefaultSource:Ljava/lang/String;

    return-object v0
.end method

.method public getHasMore()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/stripe/android/model/Customer;->mHasMore:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/stripe/android/model/Customer;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getShippingInformation()Lcom/stripe/android/model/ShippingInformation;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/stripe/android/model/Customer;->mShippingInformation:Lcom/stripe/android/model/ShippingInformation;

    return-object v0
.end method

.method public getSources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/stripe/android/model/CustomerSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/stripe/android/model/Customer;->mSources:Ljava/util/List;

    return-object v0
.end method

.method public getTotalCount()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/stripe/android/model/Customer;->mTotalCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/stripe/android/model/Customer;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 87
    const-string v1, "id"

    iget-object v2, p0, Lcom/stripe/android/model/Customer;->mId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v1, "object"

    const-string v2, "customer"

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v1, "default_source"

    iget-object v2, p0, Lcom/stripe/android/model/Customer;->mDefaultSource:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v1, "shipping"

    iget-object v2, p0, Lcom/stripe/android/model/Customer;->mShippingInformation:Lcom/stripe/android/model/ShippingInformation;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/StripeJsonModel;->putStripeJsonModelIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 93
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 94
    const-string v2, "object"

    const-string v3, "list"

    invoke-static {v1, v2, v3}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v2, "has_more"

    iget-object v3, p0, Lcom/stripe/android/model/Customer;->mHasMore:Ljava/lang/Boolean;

    invoke-static {v1, v2, v3}, Lcom/stripe/android/util/StripeJsonUtils;->putBooleanIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 96
    const-string v2, "total_count"

    iget-object v3, p0, Lcom/stripe/android/model/Customer;->mTotalCount:Ljava/lang/Integer;

    invoke-static {v1, v2, v3}, Lcom/stripe/android/util/StripeJsonUtils;->putIntegerIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 97
    const-string v2, "data"

    iget-object v3, p0, Lcom/stripe/android/model/Customer;->mSources:Ljava/util/List;

    invoke-static {v1, v2, v3}, Lcom/stripe/android/model/Customer;->putStripeJsonModelListIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 98
    const-string v2, "url"

    iget-object v3, p0, Lcom/stripe/android/model/Customer;->mUrl:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v2, "sources"

    invoke-static {v0, v2, v1}, Lcom/stripe/android/util/StripeJsonUtils;->putObjectIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 102
    return-object v0
.end method

.method public toMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 109
    const-string v1, "id"

    iget-object v2, p0, Lcom/stripe/android/model/Customer;->mId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    const-string v1, "object"

    const-string v2, "customer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    const-string v1, "default_source"

    iget-object v2, p0, Lcom/stripe/android/model/Customer;->mDefaultSource:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    const-string v1, "shipping"

    iget-object v2, p0, Lcom/stripe/android/model/Customer;->mShippingInformation:Lcom/stripe/android/model/ShippingInformation;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/StripeJsonModel;->putStripeJsonModelMapIfNotNull(Ljava/util/Map;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 118
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 119
    const-string v2, "has_more"

    iget-object v3, p0, Lcom/stripe/android/model/Customer;->mHasMore:Ljava/lang/Boolean;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    const-string v2, "total_count"

    iget-object v3, p0, Lcom/stripe/android/model/Customer;->mTotalCount:Ljava/lang/Integer;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    const-string v2, "object"

    const-string v3, "list"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    const-string v2, "url"

    iget-object v3, p0, Lcom/stripe/android/model/Customer;->mUrl:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string v2, "data"

    iget-object v3, p0, Lcom/stripe/android/model/Customer;->mSources:Ljava/util/List;

    invoke-static {v1, v2, v3}, Lcom/stripe/android/model/StripeJsonModel;->putStripeJsonModelListIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/util/List;)V

    .line 127
    invoke-static {v1}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 129
    const-string v2, "sources"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-static {v0}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 132
    return-object v0
.end method
