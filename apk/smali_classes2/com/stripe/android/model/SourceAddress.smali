.class public Lcom/stripe/android/model/SourceAddress;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "SourceAddress.java"


# static fields
.field private static final FIELD_CITY:Ljava/lang/String; = "city"

.field private static final FIELD_COUNTRY:Ljava/lang/String; = "country"

.field private static final FIELD_LINE_1:Ljava/lang/String; = "line1"

.field private static final FIELD_LINE_2:Ljava/lang/String; = "line2"

.field private static final FIELD_POSTAL_CODE:Ljava/lang/String; = "postal_code"

.field private static final FIELD_STATE:Ljava/lang/String; = "state"


# instance fields
.field private mCity:Ljava/lang/String;

.field private mCountry:Ljava/lang/String;

.field private mLine1:Ljava/lang/String;

.field private mLine2:Ljava/lang/String;

.field private mPostalCode:Ljava/lang/String;

.field private mState:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/stripe/android/model/SourceAddress;->mCity:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/stripe/android/model/SourceAddress;->mCountry:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/stripe/android/model/SourceAddress;->mLine1:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/stripe/android/model/SourceAddress;->mLine2:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Lcom/stripe/android/model/SourceAddress;->mPostalCode:Ljava/lang/String;

    .line 47
    iput-object p6, p0, Lcom/stripe/android/model/SourceAddress;->mState:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceAddress;
    .locals 7

    .prologue
    .line 135
    if-nez p0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 146
    :goto_0
    return-object v0

    .line 139
    :cond_0
    const-string v0, "city"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 140
    const-string v0, "country"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    const-string v0, "line1"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 142
    const-string v0, "line2"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 143
    const-string v0, "postal_code"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 144
    const-string v0, "state"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 146
    new-instance v0, Lcom/stripe/android/model/SourceAddress;

    invoke-direct/range {v0 .. v6}, Lcom/stripe/android/model/SourceAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/stripe/android/model/SourceAddress;
    .locals 1

    .prologue
    .line 127
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/stripe/android/model/SourceAddress;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceAddress;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/stripe/android/model/SourceAddress;->mCity:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/stripe/android/model/SourceAddress;->mCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getLine1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/stripe/android/model/SourceAddress;->mLine1:Ljava/lang/String;

    return-object v0
.end method

.method public getLine2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/stripe/android/model/SourceAddress;->mLine2:Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/stripe/android/model/SourceAddress;->mPostalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/stripe/android/model/SourceAddress;->mState:Ljava/lang/String;

    return-object v0
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/stripe/android/model/SourceAddress;->mCity:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setCountry(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/stripe/android/model/SourceAddress;->mCountry:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setLine1(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/stripe/android/model/SourceAddress;->mLine1:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setLine2(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/stripe/android/model/SourceAddress;->mLine2:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/stripe/android/model/SourceAddress;->mPostalCode:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/stripe/android/model/SourceAddress;->mState:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 114
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 115
    const-string v1, "city"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mCity:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v1, "country"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mCountry:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v1, "line1"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mLine1:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v1, "line2"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mLine2:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v1, "postal_code"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mPostalCode:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v1, "state"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mState:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-object v0
.end method

.method public toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 102
    const-string v1, "city"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mCity:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const-string v1, "country"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mCountry:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    const-string v1, "line1"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mLine1:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v1, "line2"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mLine2:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v1, "postal_code"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mPostalCode:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v1, "state"

    iget-object v2, p0, Lcom/stripe/android/model/SourceAddress;->mState:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    return-object v0
.end method
