.class public Lcom/stripe/android/model/CustomerSource;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "CustomerSource.java"

# interfaces
.implements Lcom/stripe/android/model/StripePaymentSource;


# instance fields
.field private mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;


# direct methods
.method private constructor <init>(Lcom/stripe/android/model/StripePaymentSource;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    .line 23
    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/CustomerSource;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 63
    if-nez p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-object v0

    .line 67
    :cond_1
    const-string v1, "object"

    invoke-static {p0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 69
    const-string v2, "card"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 70
    invoke-static {p0}, Lcom/stripe/android/model/Card;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/Card;

    move-result-object v1

    .line 75
    :goto_1
    if-eqz v1, :cond_0

    .line 78
    new-instance v0, Lcom/stripe/android/model/CustomerSource;

    invoke-direct {v0, v1}, Lcom/stripe/android/model/CustomerSource;-><init>(Lcom/stripe/android/model/StripePaymentSource;)V

    goto :goto_0

    .line 71
    :cond_2
    const-string v2, "source"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 72
    invoke-static {p0}, Lcom/stripe/android/model/Source;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/Source;

    move-result-object v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public static fromString(Ljava/lang/String;)Lcom/stripe/android/model/CustomerSource;
    .locals 1

    .prologue
    .line 55
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/stripe/android/model/CustomerSource;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/CustomerSource;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public asCard()Lcom/stripe/android/model/Card;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    instance-of v0, v0, Lcom/stripe/android/model/Card;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    check-cast v0, Lcom/stripe/android/model/Card;

    .line 49
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public asSource()Lcom/stripe/android/model/Source;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    instance-of v0, v0, Lcom/stripe/android/model/Source;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    check-cast v0, Lcom/stripe/android/model/Source;

    .line 41
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    invoke-interface {v0}, Lcom/stripe/android/model/StripePaymentSource;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getStripePaymentSource()Lcom/stripe/android/model/StripePaymentSource;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    return-object v0
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    instance-of v0, v0, Lcom/stripe/android/model/Source;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    check-cast v0, Lcom/stripe/android/model/Source;

    invoke-virtual {v0}, Lcom/stripe/android/model/Source;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    .line 101
    :goto_0
    return-object v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    instance-of v0, v0, Lcom/stripe/android/model/Card;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    check-cast v0, Lcom/stripe/android/model/Card;

    invoke-virtual {v0}, Lcom/stripe/android/model/Card;->toJson()Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    goto :goto_0
.end method

.method public toMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    instance-of v0, v0, Lcom/stripe/android/model/Source;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    check-cast v0, Lcom/stripe/android/model/Source;

    invoke-virtual {v0}, Lcom/stripe/android/model/Source;->toMap()Ljava/util/Map;

    move-result-object v0

    .line 90
    :goto_0
    return-object v0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    instance-of v0, v0, Lcom/stripe/android/model/Card;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/stripe/android/model/CustomerSource;->mStripePaymentSource:Lcom/stripe/android/model/StripePaymentSource;

    check-cast v0, Lcom/stripe/android/model/Card;

    invoke-virtual {v0}, Lcom/stripe/android/model/Card;->toMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 90
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto :goto_0
.end method
