.class public Lcom/stripe/android/model/Source;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "Source.java"

# interfaces
.implements Lcom/stripe/android/model/StripePaymentSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/model/Source$SourceFlow;,
        Lcom/stripe/android/model/Source$Usage;,
        Lcom/stripe/android/model/Source$SourceStatus;,
        Lcom/stripe/android/model/Source$SourceType;
    }
.end annotation


# static fields
.field public static final BANCONTACT:Ljava/lang/String; = "bancontact"

.field public static final BITCOIN:Ljava/lang/String; = "bitcoin"

.field public static final CANCELED:Ljava/lang/String; = "canceled"

.field public static final CARD:Ljava/lang/String; = "card"

.field public static final CHARGEABLE:Ljava/lang/String; = "chargeable"

.field public static final CODE_VERIFICATION:Ljava/lang/String; = "code_verification"

.field public static final CONSUMED:Ljava/lang/String; = "consumed"

.field static final EURO:Ljava/lang/String; = "eur"

.field public static final FAILED:Ljava/lang/String; = "failed"

.field static final FIELD_AMOUNT:Ljava/lang/String; = "amount"

.field static final FIELD_CLIENT_SECRET:Ljava/lang/String; = "client_secret"

.field static final FIELD_CODE_VERIFICATION:Ljava/lang/String; = "code_verification"

.field static final FIELD_CREATED:Ljava/lang/String; = "created"

.field static final FIELD_CURRENCY:Ljava/lang/String; = "currency"

.field static final FIELD_FLOW:Ljava/lang/String; = "flow"

.field static final FIELD_ID:Ljava/lang/String; = "id"

.field static final FIELD_LIVEMODE:Ljava/lang/String; = "livemode"

.field static final FIELD_METADATA:Ljava/lang/String; = "metadata"

.field static final FIELD_OBJECT:Ljava/lang/String; = "object"

.field static final FIELD_OWNER:Ljava/lang/String; = "owner"

.field static final FIELD_RECEIVER:Ljava/lang/String; = "receiver"

.field static final FIELD_REDIRECT:Ljava/lang/String; = "redirect"

.field static final FIELD_STATUS:Ljava/lang/String; = "status"

.field static final FIELD_TYPE:Ljava/lang/String; = "type"

.field static final FIELD_USAGE:Ljava/lang/String; = "usage"

.field public static final GIROPAY:Ljava/lang/String; = "giropay"

.field public static final IDEAL:Ljava/lang/String; = "ideal"

.field public static final MODELED_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NONE:Ljava/lang/String; = "none"

.field public static final PENDING:Ljava/lang/String; = "pending"

.field public static final RECEIVER:Ljava/lang/String; = "receiver"

.field public static final REDIRECT:Ljava/lang/String; = "redirect"

.field public static final REUSABLE:Ljava/lang/String; = "reusable"

.field public static final SEPA_DEBIT:Ljava/lang/String; = "sepa_debit"

.field public static final SINGLE_USE:Ljava/lang/String; = "single_use"

.field public static final SOFORT:Ljava/lang/String; = "sofort"

.field public static final THREE_D_SECURE:Ljava/lang/String; = "three_d_secure"

.field public static final UNKNOWN:Ljava/lang/String; = "unknown"

.field static final USD:Ljava/lang/String; = "usd"

.field static final VALUE_SOURCE:Ljava/lang/String; = "source"


# instance fields
.field private mAmount:Ljava/lang/Long;

.field private mClientSecret:Ljava/lang/String;

.field private mCodeVerification:Lcom/stripe/android/model/SourceCodeVerification;

.field private mCreated:Ljava/lang/Long;

.field private mCurrency:Ljava/lang/String;

.field private mFlow:Ljava/lang/String;

.field private mId:Ljava/lang/String;

.field private mLiveMode:Ljava/lang/Boolean;

.field private mMetaData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOwner:Lcom/stripe/android/model/SourceOwner;

.field private mReceiver:Lcom/stripe/android/model/SourceReceiver;

.field private mRedirect:Lcom/stripe/android/model/SourceRedirect;

.field private mSourceTypeData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mSourceTypeModel:Lcom/stripe/android/model/StripeSourceTypeModel;

.field private mStatus:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mTypeRaw:Ljava/lang/String;

.field private mUsage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/stripe/android/model/Source;->MODELED_TYPES:Ljava/util/Set;

    .line 59
    sget-object v0, Lcom/stripe/android/model/Source;->MODELED_TYPES:Ljava/util/Set;

    const-string v1, "card"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v0, Lcom/stripe/android/model/Source;->MODELED_TYPES:Ljava/util/Set;

    const-string v1, "sepa_debit"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/stripe/android/model/SourceCodeVerification;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Lcom/stripe/android/model/SourceOwner;Lcom/stripe/android/model/SourceReceiver;Lcom/stripe/android/model/SourceRedirect;Ljava/lang/String;Ljava/util/Map;Lcom/stripe/android/model/StripeSourceTypeModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/stripe/android/model/SourceCodeVerification;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/stripe/android/model/SourceOwner;",
            "Lcom/stripe/android/model/SourceReceiver;",
            "Lcom/stripe/android/model/SourceRedirect;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/stripe/android/model/StripeSourceTypeModel;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    .line 159
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mId:Ljava/lang/String;

    .line 160
    iput-object p2, p0, Lcom/stripe/android/model/Source;->mAmount:Ljava/lang/Long;

    .line 161
    iput-object p3, p0, Lcom/stripe/android/model/Source;->mClientSecret:Ljava/lang/String;

    .line 162
    iput-object p4, p0, Lcom/stripe/android/model/Source;->mCodeVerification:Lcom/stripe/android/model/SourceCodeVerification;

    .line 163
    iput-object p5, p0, Lcom/stripe/android/model/Source;->mCreated:Ljava/lang/Long;

    .line 164
    iput-object p6, p0, Lcom/stripe/android/model/Source;->mCurrency:Ljava/lang/String;

    .line 165
    iput-object p7, p0, Lcom/stripe/android/model/Source;->mFlow:Ljava/lang/String;

    .line 166
    iput-object p8, p0, Lcom/stripe/android/model/Source;->mLiveMode:Ljava/lang/Boolean;

    .line 167
    iput-object p9, p0, Lcom/stripe/android/model/Source;->mMetaData:Ljava/util/Map;

    .line 168
    iput-object p10, p0, Lcom/stripe/android/model/Source;->mOwner:Lcom/stripe/android/model/SourceOwner;

    .line 169
    iput-object p11, p0, Lcom/stripe/android/model/Source;->mReceiver:Lcom/stripe/android/model/SourceReceiver;

    .line 170
    iput-object p12, p0, Lcom/stripe/android/model/Source;->mRedirect:Lcom/stripe/android/model/SourceRedirect;

    .line 171
    iput-object p13, p0, Lcom/stripe/android/model/Source;->mStatus:Ljava/lang/String;

    .line 172
    iput-object p14, p0, Lcom/stripe/android/model/Source;->mSourceTypeData:Ljava/util/Map;

    .line 173
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/stripe/android/model/Source;->mSourceTypeModel:Lcom/stripe/android/model/StripeSourceTypeModel;

    .line 174
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/stripe/android/model/Source;->mType:Ljava/lang/String;

    .line 175
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/stripe/android/model/Source;->mTypeRaw:Ljava/lang/String;

    .line 176
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/stripe/android/model/Source;->mUsage:Ljava/lang/String;

    .line 177
    return-void
.end method

.method static asSourceFlow(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    const-string v0, "redirect"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    const-string v0, "redirect"

    .line 589
    :goto_0
    return-object v0

    .line 582
    :cond_0
    const-string v0, "receiver"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 583
    const-string v0, "receiver"

    goto :goto_0

    .line 584
    :cond_1
    const-string v0, "code_verification"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 585
    const-string v0, "code_verification"

    goto :goto_0

    .line 586
    :cond_2
    const-string v0, "none"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 587
    const-string v0, "none"

    goto :goto_0

    .line 589
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static asSourceStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 526
    const-string v0, "pending"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    const-string v0, "pending"

    .line 537
    :goto_0
    return-object v0

    .line 528
    :cond_0
    const-string v0, "chargeable"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529
    const-string v0, "chargeable"

    goto :goto_0

    .line 530
    :cond_1
    const-string v0, "consumed"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 531
    const-string v0, "consumed"

    goto :goto_0

    .line 532
    :cond_2
    const-string v0, "canceled"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 533
    const-string v0, "canceled"

    goto :goto_0

    .line 534
    :cond_3
    const-string v0, "failed"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 535
    const-string v0, "failed"

    goto :goto_0

    .line 537
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static asSourceType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 543
    const-string v0, "bitcoin"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544
    const-string v0, "bitcoin"

    .line 563
    :goto_0
    return-object v0

    .line 545
    :cond_0
    const-string v0, "card"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    const-string v0, "card"

    goto :goto_0

    .line 547
    :cond_1
    const-string v0, "three_d_secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548
    const-string v0, "three_d_secure"

    goto :goto_0

    .line 549
    :cond_2
    const-string v0, "giropay"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 550
    const-string v0, "giropay"

    goto :goto_0

    .line 551
    :cond_3
    const-string v0, "sepa_debit"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 552
    const-string v0, "sepa_debit"

    goto :goto_0

    .line 553
    :cond_4
    const-string v0, "ideal"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 554
    const-string v0, "ideal"

    goto :goto_0

    .line 555
    :cond_5
    const-string v0, "sofort"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 556
    const-string v0, "sofort"

    goto :goto_0

    .line 557
    :cond_6
    const-string v0, "bancontact"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 558
    const-string v0, "bancontact"

    goto :goto_0

    .line 559
    :cond_7
    const-string v0, "unknown"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 560
    const-string v0, "unknown"

    goto :goto_0

    .line 563
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static asUsage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 569
    const-string v0, "reusable"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    const-string v0, "reusable"

    .line 574
    :goto_0
    return-object v0

    .line 571
    :cond_0
    const-string v0, "single_use"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 572
    const-string v0, "single_use"

    goto :goto_0

    .line 574
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/Source;
    .locals 21

    .prologue
    .line 416
    if-eqz p0, :cond_0

    const-string v2, "source"

    const-string v3, "object"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 417
    :cond_0
    const/4 v2, 0x0

    .line 468
    :goto_0
    return-object v2

    .line 420
    :cond_1
    const-string v2, "id"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 421
    const-string v2, "amount"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optLong(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    .line 422
    const-string v2, "client_secret"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 423
    const-string v2, "code_verification"

    const-class v6, Lcom/stripe/android/model/SourceCodeVerification;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v6}, Lcom/stripe/android/model/Source;->optStripeJsonModel(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Lcom/stripe/android/model/StripeJsonModel;

    move-result-object v6

    check-cast v6, Lcom/stripe/android/model/SourceCodeVerification;

    .line 427
    const-string v2, "created"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optLong(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    .line 428
    const-string v2, "currency"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 429
    const-string v2, "flow"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/model/Source;->asSourceFlow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 430
    const-string v2, "livemode"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 431
    const-string v2, "metadata"

    .line 432
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/util/StripeJsonUtils;->jsonObjectToStringMap(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v11

    .line 433
    const-string v2, "owner"

    const-class v12, Lcom/stripe/android/model/SourceOwner;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v12}, Lcom/stripe/android/model/Source;->optStripeJsonModel(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Lcom/stripe/android/model/StripeJsonModel;

    move-result-object v12

    check-cast v12, Lcom/stripe/android/model/SourceOwner;

    .line 434
    const-string v2, "receiver"

    const-class v13, Lcom/stripe/android/model/SourceReceiver;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v13}, Lcom/stripe/android/model/Source;->optStripeJsonModel(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Lcom/stripe/android/model/StripeJsonModel;

    move-result-object v13

    check-cast v13, Lcom/stripe/android/model/SourceReceiver;

    .line 438
    const-string v2, "redirect"

    const-class v14, Lcom/stripe/android/model/SourceRedirect;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v14}, Lcom/stripe/android/model/Source;->optStripeJsonModel(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Lcom/stripe/android/model/StripeJsonModel;

    move-result-object v14

    check-cast v14, Lcom/stripe/android/model/SourceRedirect;

    .line 442
    const-string v2, "status"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/model/Source;->asSourceStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 444
    const-string v2, "type"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 445
    if-nez v19, :cond_2

    .line 448
    const-string v19, "unknown"

    .line 451
    :cond_2
    invoke-static/range {v19 .. v19}, Lcom/stripe/android/model/Source;->asSourceType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 452
    if-nez v18, :cond_3

    .line 453
    const-string v18, "unknown"

    .line 460
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/util/StripeJsonUtils;->jsonObjectToMap(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v16

    .line 461
    sget-object v2, Lcom/stripe/android/model/Source;->MODELED_TYPES:Ljava/util/Set;

    .line 462
    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-class v2, Lcom/stripe/android/model/StripeSourceTypeModel;

    .line 463
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->optStripeJsonModel(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Lcom/stripe/android/model/StripeJsonModel;

    move-result-object v2

    check-cast v2, Lcom/stripe/android/model/StripeSourceTypeModel;

    move-object/from16 v17, v2

    .line 466
    :goto_1
    const-string v2, "usage"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/model/Source;->asUsage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 468
    new-instance v2, Lcom/stripe/android/model/Source;

    invoke-direct/range {v2 .. v20}, Lcom/stripe/android/model/Source;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/stripe/android/model/SourceCodeVerification;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Lcom/stripe/android/model/SourceOwner;Lcom/stripe/android/model/SourceReceiver;Lcom/stripe/android/model/SourceRedirect;Ljava/lang/String;Ljava/util/Map;Lcom/stripe/android/model/StripeSourceTypeModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 463
    :cond_4
    const/16 v17, 0x0

    goto :goto_1
.end method

.method public static fromString(Ljava/lang/String;)Lcom/stripe/android/model/Source;
    .locals 1

    .prologue
    .line 408
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/stripe/android/model/Source;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/Source;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 410
    :goto_0
    return-object v0

    .line 409
    :catch_0
    move-exception v0

    .line 410
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static optStripeJsonModel(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Lcom/stripe/android/model/StripeJsonModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/stripe/android/model/StripeJsonModel;",
            ">(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 494
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 519
    :goto_0
    return-object v0

    .line 498
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 500
    :pswitch_0
    const-string v0, "code_verification"

    .line 502
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 501
    invoke-static {v0}, Lcom/stripe/android/model/SourceCodeVerification;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceCodeVerification;

    move-result-object v0

    .line 500
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/model/StripeJsonModel;

    goto :goto_0

    .line 498
    :sswitch_0
    const-string v2, "code_verification"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "owner"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "receiver"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v2, "redirect"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_4
    const-string v2, "card"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_5
    const-string v2, "sepa_debit"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x5

    goto :goto_1

    .line 504
    :pswitch_1
    const-string v0, "owner"

    .line 505
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/model/SourceOwner;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceOwner;

    move-result-object v0

    .line 504
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/model/StripeJsonModel;

    goto :goto_0

    .line 507
    :pswitch_2
    const-string v0, "receiver"

    .line 508
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/model/SourceReceiver;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceReceiver;

    move-result-object v0

    .line 507
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/model/StripeJsonModel;

    goto :goto_0

    .line 510
    :pswitch_3
    const-string v0, "redirect"

    .line 511
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/model/SourceRedirect;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceRedirect;

    move-result-object v0

    .line 510
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/model/StripeJsonModel;

    goto/16 :goto_0

    .line 513
    :pswitch_4
    const-string v0, "card"

    .line 514
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/model/SourceCardData;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v0

    .line 513
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/model/StripeJsonModel;

    goto/16 :goto_0

    .line 516
    :pswitch_5
    const-string v0, "sepa_debit"

    .line 517
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/model/SourceSepaDebitData;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceSepaDebitData;

    move-result-object v0

    .line 516
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/model/StripeJsonModel;

    goto/16 :goto_0

    .line 498
    nop

    :sswitch_data_0
    .sparse-switch
        -0x30341611 -> :sswitch_2
        -0x2e430824 -> :sswitch_3
        0x2e7b10 -> :sswitch_4
        0x653f2b3 -> :sswitch_1
        0x604b5b2d -> :sswitch_0
        0x618aa970 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public getAmount()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mAmount:Ljava/lang/Long;

    return-object v0
.end method

.method public getClientSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mClientSecret:Ljava/lang/String;

    return-object v0
.end method

.method public getCodeVerification()Lcom/stripe/android/model/SourceCodeVerification;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mCodeVerification:Lcom/stripe/android/model/SourceCodeVerification;

    return-object v0
.end method

.method public getCreated()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mCreated:Ljava/lang/Long;

    return-object v0
.end method

.method public getCurrency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public getFlow()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mFlow:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getMetaData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mMetaData:Ljava/util/Map;

    return-object v0
.end method

.method public getOwner()Lcom/stripe/android/model/SourceOwner;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mOwner:Lcom/stripe/android/model/SourceOwner;

    return-object v0
.end method

.method public getReceiver()Lcom/stripe/android/model/SourceReceiver;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mReceiver:Lcom/stripe/android/model/SourceReceiver;

    return-object v0
.end method

.method public getRedirect()Lcom/stripe/android/model/SourceRedirect;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mRedirect:Lcom/stripe/android/model/SourceRedirect;

    return-object v0
.end method

.method public getSourceTypeData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mSourceTypeData:Ljava/util/Map;

    return-object v0
.end method

.method public getSourceTypeModel()Lcom/stripe/android/model/StripeSourceTypeModel;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mSourceTypeModel:Lcom/stripe/android/model/StripeSourceTypeModel;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeRaw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mTypeRaw:Ljava/lang/String;

    return-object v0
.end method

.method public getUsage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mUsage:Ljava/lang/String;

    return-object v0
.end method

.method public isLiveMode()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/stripe/android/model/Source;->mLiveMode:Ljava/lang/Boolean;

    return-object v0
.end method

.method public setAmount(J)V
    .locals 1

    .prologue
    .line 275
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/stripe/android/model/Source;->mAmount:Ljava/lang/Long;

    .line 276
    return-void
.end method

.method public setClientSecret(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mClientSecret:Ljava/lang/String;

    .line 280
    return-void
.end method

.method public setCodeVerification(Lcom/stripe/android/model/SourceCodeVerification;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mCodeVerification:Lcom/stripe/android/model/SourceCodeVerification;

    .line 284
    return-void
.end method

.method public setCreated(J)V
    .locals 1

    .prologue
    .line 287
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/stripe/android/model/Source;->mCreated:Ljava/lang/Long;

    .line 288
    return-void
.end method

.method public setCurrency(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mCurrency:Ljava/lang/String;

    .line 292
    return-void
.end method

.method public setFlow(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mFlow:Ljava/lang/String;

    .line 296
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mId:Ljava/lang/String;

    .line 272
    return-void
.end method

.method public setLiveMode(Z)V
    .locals 1

    .prologue
    .line 299
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/stripe/android/model/Source;->mLiveMode:Ljava/lang/Boolean;

    .line 300
    return-void
.end method

.method public setMetaData(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 303
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mMetaData:Ljava/util/Map;

    .line 304
    return-void
.end method

.method public setOwner(Lcom/stripe/android/model/SourceOwner;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mOwner:Lcom/stripe/android/model/SourceOwner;

    .line 308
    return-void
.end method

.method public setReceiver(Lcom/stripe/android/model/SourceReceiver;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mReceiver:Lcom/stripe/android/model/SourceReceiver;

    .line 312
    return-void
.end method

.method public setRedirect(Lcom/stripe/android/model/SourceRedirect;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mRedirect:Lcom/stripe/android/model/SourceRedirect;

    .line 316
    return-void
.end method

.method public setSourceTypeData(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 323
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mSourceTypeData:Ljava/util/Map;

    .line 324
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mStatus:Ljava/lang/String;

    .line 320
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mType:Ljava/lang/String;

    .line 333
    return-void
.end method

.method public setTypeRaw(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 327
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mTypeRaw:Ljava/lang/String;

    .line 328
    const-string v0, "unknown"

    invoke-virtual {p0, v0}, Lcom/stripe/android/model/Source;->setType(Ljava/lang/String;)V

    .line 329
    return-void
.end method

.method public setUsage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/stripe/android/model/Source;->mUsage:Ljava/lang/String;

    .line 337
    return-void
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 371
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 373
    :try_start_0
    const-string v1, "id"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v1, "object"

    const-string v2, "source"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 375
    const-string v1, "amount"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mAmount:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 376
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mClientSecret:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v1, "code_verification"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mCodeVerification:Lcom/stripe/android/model/SourceCodeVerification;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->putStripeJsonModelIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 378
    const-string v1, "created"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mCreated:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 379
    const-string v1, "currency"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mCurrency:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v1, "flow"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mFlow:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v1, "livemode"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mLiveMode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 383
    iget-object v1, p0, Lcom/stripe/android/model/Source;->mMetaData:Ljava/util/Map;

    invoke-static {v1}, Lcom/stripe/android/util/StripeJsonUtils;->mapToJsonObject(Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v1

    .line 384
    if-eqz v1, :cond_0

    .line 385
    const-string v2, "metadata"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 388
    :cond_0
    iget-object v1, p0, Lcom/stripe/android/model/Source;->mSourceTypeData:Ljava/util/Map;

    invoke-static {v1}, Lcom/stripe/android/util/StripeJsonUtils;->mapToJsonObject(Ljava/util/Map;)Lorg/json/JSONObject;

    move-result-object v1

    .line 390
    if-eqz v1, :cond_1

    .line 391
    iget-object v2, p0, Lcom/stripe/android/model/Source;->mTypeRaw:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 394
    :cond_1
    const-string v1, "owner"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mOwner:Lcom/stripe/android/model/SourceOwner;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->putStripeJsonModelIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 395
    const-string v1, "receiver"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mReceiver:Lcom/stripe/android/model/SourceReceiver;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->putStripeJsonModelIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 396
    const-string v1, "redirect"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mRedirect:Lcom/stripe/android/model/SourceRedirect;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->putStripeJsonModelIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 397
    const-string v1, "status"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mStatus:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    const-string v1, "type"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mTypeRaw:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const-string v1, "usage"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mUsage:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    :goto_0
    return-object v0

    .line 401
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 343
    const-string v1, "id"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    const-string v1, "amount"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mAmount:Ljava/lang/Long;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mClientSecret:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    const-string v1, "code_verification"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mCodeVerification:Lcom/stripe/android/model/SourceCodeVerification;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->putStripeJsonModelMapIfNotNull(Ljava/util/Map;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 349
    const-string v1, "created"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mCreated:Ljava/lang/Long;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    const-string v1, "currency"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mCurrency:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    const-string v1, "flow"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mFlow:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    const-string v1, "livemode"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mLiveMode:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    const-string v1, "metadata"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mMetaData:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    const-string v1, "owner"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mOwner:Lcom/stripe/android/model/SourceOwner;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->putStripeJsonModelMapIfNotNull(Ljava/util/Map;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 356
    const-string v1, "receiver"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mReceiver:Lcom/stripe/android/model/SourceReceiver;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->putStripeJsonModelMapIfNotNull(Ljava/util/Map;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 357
    const-string v1, "redirect"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mRedirect:Lcom/stripe/android/model/SourceRedirect;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/Source;->putStripeJsonModelMapIfNotNull(Ljava/util/Map;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 359
    iget-object v1, p0, Lcom/stripe/android/model/Source;->mTypeRaw:Ljava/lang/String;

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mSourceTypeData:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    const-string v1, "status"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mStatus:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    const-string v1, "type"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mTypeRaw:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    const-string v1, "usage"

    iget-object v2, p0, Lcom/stripe/android/model/Source;->mUsage:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    invoke-static {v0}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 365
    return-object v0
.end method
