.class public Lcom/stripe/android/model/SourceRedirect;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "SourceRedirect.java"


# static fields
.field public static final FAILED:Ljava/lang/String; = "failed"

.field static final FIELD_RETURN_URL:Ljava/lang/String; = "return_url"

.field static final FIELD_STATUS:Ljava/lang/String; = "status"

.field static final FIELD_URL:Ljava/lang/String; = "url"

.field public static final PENDING:Ljava/lang/String; = "pending"

.field public static final SUCCEEDED:Ljava/lang/String; = "succeeded"


# instance fields
.field private mReturnUrl:Ljava/lang/String;

.field private mStatus:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/stripe/android/model/SourceRedirect;->mReturnUrl:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/stripe/android/model/SourceRedirect;->mStatus:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lcom/stripe/android/model/SourceRedirect;->mUrl:Ljava/lang/String;

    .line 48
    return-void
.end method

.method private static asStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const-string v0, "pending"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    const-string v0, "pending"

    .line 129
    :goto_0
    return-object v0

    .line 123
    :cond_0
    const-string v0, "succeeded"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    const-string v0, "succeeded"

    goto :goto_0

    .line 125
    :cond_1
    const-string v0, "failed"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    const-string v0, "failed"

    goto :goto_0

    .line 129
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceRedirect;
    .locals 4

    .prologue
    .line 108
    if-nez p0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 115
    :goto_0
    return-object v0

    .line 112
    :cond_0
    const-string v0, "return_url"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 113
    const-string v0, "status"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/model/SourceRedirect;->asStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 114
    const-string v0, "url"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 115
    new-instance v0, Lcom/stripe/android/model/SourceRedirect;

    invoke-direct {v0, v1, v2, v3}, Lcom/stripe/android/model/SourceRedirect;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/stripe/android/model/SourceRedirect;
    .locals 1

    .prologue
    .line 99
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 100
    invoke-static {v0}, Lcom/stripe/android/model/SourceRedirect;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceRedirect;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getReturnUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/stripe/android/model/SourceRedirect;->mReturnUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/stripe/android/model/SourceRedirect;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/stripe/android/model/SourceRedirect;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public setReturnUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/stripe/android/model/SourceRedirect;->mReturnUrl:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/stripe/android/model/SourceRedirect;->mStatus:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/stripe/android/model/SourceRedirect;->mUrl:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 89
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 90
    const-string v1, "return_url"

    iget-object v2, p0, Lcom/stripe/android/model/SourceRedirect;->mReturnUrl:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v1, "status"

    iget-object v2, p0, Lcom/stripe/android/model/SourceRedirect;->mStatus:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v1, "url"

    iget-object v2, p0, Lcom/stripe/android/model/SourceRedirect;->mUrl:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    return-object v0
.end method

.method public toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 79
    const-string v1, "return_url"

    iget-object v2, p0, Lcom/stripe/android/model/SourceRedirect;->mReturnUrl:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    const-string v1, "status"

    iget-object v2, p0, Lcom/stripe/android/model/SourceRedirect;->mStatus:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    const-string v1, "url"

    iget-object v2, p0, Lcom/stripe/android/model/SourceRedirect;->mUrl:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    invoke-static {v0}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 83
    return-object v0
.end method
