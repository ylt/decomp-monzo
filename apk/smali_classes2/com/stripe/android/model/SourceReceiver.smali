.class public Lcom/stripe/android/model/SourceReceiver;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "SourceReceiver.java"


# static fields
.field private static final FIELD_ADDRESS:Ljava/lang/String; = "address"

.field private static final FIELD_AMOUNT_CHARGED:Ljava/lang/String; = "amount_charged"

.field private static final FIELD_AMOUNT_RECEIVED:Ljava/lang/String; = "amount_received"

.field private static final FIELD_AMOUNT_RETURNED:Ljava/lang/String; = "amount_returned"


# instance fields
.field private mAddress:Ljava/lang/String;

.field private mAmountCharged:J

.field private mAmountReceived:J

.field private mAmountReturned:J


# direct methods
.method constructor <init>(Ljava/lang/String;JJJ)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/stripe/android/model/SourceReceiver;->mAddress:Ljava/lang/String;

    .line 37
    iput-wide p2, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountCharged:J

    .line 38
    iput-wide p4, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReceived:J

    .line 39
    iput-wide p6, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReturned:J

    .line 40
    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceReceiver;
    .locals 8

    .prologue
    .line 115
    if-nez p0, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 120
    :goto_0
    return-object v0

    .line 119
    :cond_0
    const-string v0, "address"

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    new-instance v0, Lcom/stripe/android/model/SourceReceiver;

    const-string v2, "amount_charged"

    .line 121
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "amount_received"

    .line 122
    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v6, "amount_returned"

    .line 123
    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/stripe/android/model/SourceReceiver;-><init>(Ljava/lang/String;JJJ)V

    goto :goto_0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/stripe/android/model/SourceReceiver;
    .locals 1

    .prologue
    .line 106
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 107
    invoke-static {v0}, Lcom/stripe/android/model/SourceReceiver;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceReceiver;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    .line 108
    :catch_0
    move-exception v0

    .line 109
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/stripe/android/model/SourceReceiver;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getAmountCharged()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountCharged:J

    return-wide v0
.end method

.method public getAmountReceived()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReceived:J

    return-wide v0
.end method

.method public getAmountReturned()J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReturned:J

    return-wide v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/stripe/android/model/SourceReceiver;->mAddress:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setAmountCharged(J)V
    .locals 1

    .prologue
    .line 55
    iput-wide p1, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountCharged:J

    .line 56
    return-void
.end method

.method public setAmountReceived(J)V
    .locals 1

    .prologue
    .line 63
    iput-wide p1, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReceived:J

    .line 64
    return-void
.end method

.method public setAmountReturned(J)V
    .locals 1

    .prologue
    .line 71
    iput-wide p1, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReturned:J

    .line 72
    return-void
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 91
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 92
    const-string v1, "address"

    iget-object v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAddress:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :try_start_0
    const-string v1, "amount_charged"

    iget-wide v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountCharged:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 95
    const-string v1, "amount_received"

    iget-wide v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReceived:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 96
    const-string v1, "amount_returned"

    iget-wide v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReturned:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-object v0

    .line 97
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public toMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/stripe/android/model/SourceReceiver;->mAddress:Ljava/lang/String;

    invoke-static {v1}, Lcom/stripe/android/util/StripeTextUtils;->isBlank(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    const-string v1, "address"

    iget-object v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAddress:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    :cond_0
    const-string v1, "address"

    iget-object v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAddress:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    const-string v1, "amount_charged"

    iget-wide v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountCharged:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    const-string v1, "amount_received"

    iget-wide v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReceived:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const-string v1, "amount_returned"

    iget-wide v2, p0, Lcom/stripe/android/model/SourceReceiver;->mAmountReturned:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    return-object v0
.end method
