.class public Lcom/stripe/android/model/SourceCardData;
.super Lcom/stripe/android/model/StripeSourceTypeModel;
.source "SourceCardData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/model/SourceCardData$ThreeDSecureStatus;
    }
.end annotation


# static fields
.field public static final FIELD_ADDRESS_LINE1_CHECK:Ljava/lang/String; = "address_line1_check"

.field public static final FIELD_ADDRESS_ZIP_CHECK:Ljava/lang/String; = "address_zip_check"

.field public static final FIELD_BRAND:Ljava/lang/String; = "brand"

.field public static final FIELD_COUNTRY:Ljava/lang/String; = "country"

.field public static final FIELD_CVC_CHECK:Ljava/lang/String; = "cvc_check"

.field public static final FIELD_DYNAMIC_LAST4:Ljava/lang/String; = "dynamic_last4"

.field public static final FIELD_EXP_MONTH:Ljava/lang/String; = "exp_month"

.field public static final FIELD_EXP_YEAR:Ljava/lang/String; = "exp_year"

.field public static final FIELD_FUNDING:Ljava/lang/String; = "funding"

.field public static final FIELD_LAST4:Ljava/lang/String; = "last4"

.field public static final FIELD_THREE_D_SECURE:Ljava/lang/String; = "three_d_secure"

.field public static final FIELD_TOKENIZATION_METHOD:Ljava/lang/String; = "tokenization_method"

.field public static final NOT_SUPPORTED:Ljava/lang/String; = "not_supported"

.field public static final OPTIONAL:Ljava/lang/String; = "optional"

.field public static final REQUIRED:Ljava/lang/String; = "required"

.field public static final UNKNOWN:Ljava/lang/String; = "unknown"


# instance fields
.field private mAddressLine1Check:Ljava/lang/String;

.field private mAddressZipCheck:Ljava/lang/String;

.field private mBrand:Ljava/lang/String;

.field private mCountry:Ljava/lang/String;

.field private mCvcCheck:Ljava/lang/String;

.field private mDynamicLast4:Ljava/lang/String;

.field private mExpiryMonth:Ljava/lang/Integer;

.field private mExpiryYear:Ljava/lang/Integer;

.field private mFunding:Ljava/lang/String;

.field private mLast4:Ljava/lang/String;

.field private mThreeDSecureStatus:Ljava/lang/String;

.field private mTokenizationMethod:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/stripe/android/model/StripeSourceTypeModel;-><init>()V

    .line 71
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "address_line1_check"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "address_zip_check"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "brand"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "country"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "cvc_check"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "dynamic_last4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "exp_month"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "exp_year"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "funding"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "last4"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "three_d_secure"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "tokenization_method"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/stripe/android/model/SourceCardData;->addStandardFields([Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method static asThreeDSecureStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    invoke-static {p0}, Lcom/stripe/android/util/StripeJsonUtils;->nullIfNullOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 293
    if-nez v0, :cond_0

    .line 294
    const/4 v0, 0x0

    .line 304
    :goto_0
    return-object v0

    .line 297
    :cond_0
    const-string v0, "required"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    const-string v0, "required"

    goto :goto_0

    .line 299
    :cond_1
    const-string v0, "optional"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 300
    const-string v0, "optional"

    goto :goto_0

    .line 301
    :cond_2
    const-string v0, "not_supported"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 302
    const-string v0, "not_supported"

    goto :goto_0

    .line 304
    :cond_3
    const-string v0, "unknown"

    goto :goto_0
.end method

.method static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceCardData;
    .locals 3

    .prologue
    .line 252
    if-nez p0, :cond_1

    .line 253
    const/4 v0, 0x0

    .line 277
    :cond_0
    :goto_0
    return-object v0

    .line 256
    :cond_1
    new-instance v0, Lcom/stripe/android/model/SourceCardData;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceCardData;-><init>()V

    .line 257
    const-string v1, "address_line1_check"

    invoke-static {p0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/stripe/android/model/SourceCardData;->setAddressLine1Check(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "address_zip_check"

    .line 258
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setAddressZipCheck(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "brand"

    .line 259
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/util/StripeTextUtils;->asCardBrand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setBrand(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "country"

    .line 260
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setCountry(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "cvc_check"

    .line 261
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setCvcCheck(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "dynamic_last4"

    .line 262
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setDynamicLast4(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "exp_month"

    .line 263
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optInteger(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setExpiryMonth(Ljava/lang/Integer;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "exp_year"

    .line 264
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optInteger(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setExpiryYear(Ljava/lang/Integer;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "funding"

    .line 265
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/util/StripeTextUtils;->asFundingType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setFunding(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "last4"

    .line 266
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setLast4(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "three_d_secure"

    .line 267
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/model/SourceCardData;->asThreeDSecureStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setThreeDSecureStatus(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    move-result-object v1

    const-string v2, "tokenization_method"

    .line 269
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceCardData;->setTokenizationMethod(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;

    .line 271
    iget-object v1, v0, Lcom/stripe/android/model/SourceCardData;->mStandardFields:Ljava/util/Set;

    .line 272
    invoke-static {p0, v1}, Lcom/stripe/android/model/SourceCardData;->jsonObjectToMapWithoutKeys(Lorg/json/JSONObject;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v1

    .line 273
    if-eqz v1, :cond_0

    .line 274
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceCardData;->setAdditionalFields(Ljava/util/Map;)V

    goto/16 :goto_0
.end method

.method static fromString(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 1

    .prologue
    .line 283
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/stripe/android/model/SourceCardData;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceCardData;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 285
    :goto_0
    return-object v0

    .line 284
    :catch_0
    move-exception v0

    .line 285
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setAddressLine1Check(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mAddressLine1Check:Ljava/lang/String;

    .line 192
    return-object p0
.end method

.method private setAddressZipCheck(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mAddressZipCheck:Ljava/lang/String;

    .line 197
    return-object p0
.end method

.method private setBrand(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mBrand:Ljava/lang/String;

    .line 202
    return-object p0
.end method

.method private setCountry(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mCountry:Ljava/lang/String;

    .line 207
    return-object p0
.end method

.method private setCvcCheck(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mCvcCheck:Ljava/lang/String;

    .line 212
    return-object p0
.end method

.method private setDynamicLast4(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mDynamicLast4:Ljava/lang/String;

    .line 217
    return-object p0
.end method

.method private setExpiryMonth(Ljava/lang/Integer;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mExpiryMonth:Ljava/lang/Integer;

    .line 222
    return-object p0
.end method

.method private setExpiryYear(Ljava/lang/Integer;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mExpiryYear:Ljava/lang/Integer;

    .line 227
    return-object p0
.end method

.method private setFunding(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mFunding:Ljava/lang/String;

    .line 232
    return-object p0
.end method

.method private setLast4(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mLast4:Ljava/lang/String;

    .line 237
    return-object p0
.end method

.method private setThreeDSecureStatus(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mThreeDSecureStatus:Ljava/lang/String;

    .line 242
    return-object p0
.end method

.method private setTokenizationMethod(Ljava/lang/String;)Lcom/stripe/android/model/SourceCardData;
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/stripe/android/model/SourceCardData;->mTokenizationMethod:Ljava/lang/String;

    .line 247
    return-object p0
.end method


# virtual methods
.method public bridge synthetic getAdditionalFields()Ljava/util/Map;
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Lcom/stripe/android/model/StripeSourceTypeModel;->getAdditionalFields()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAddressLine1Check()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mAddressLine1Check:Ljava/lang/String;

    return-object v0
.end method

.method public getAddressZipCheck()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mAddressZipCheck:Ljava/lang/String;

    return-object v0
.end method

.method public getBrand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mBrand:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getCvcCheck()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mCvcCheck:Ljava/lang/String;

    return-object v0
.end method

.method public getDynamicLast4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mDynamicLast4:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryMonth()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mExpiryMonth:Ljava/lang/Integer;

    return-object v0
.end method

.method public getExpiryYear()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mExpiryYear:Ljava/lang/Integer;

    return-object v0
.end method

.method public getFunding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mFunding:Ljava/lang/String;

    return-object v0
.end method

.method public getLast4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mLast4:Ljava/lang/String;

    return-object v0
.end method

.method public getThreeDSecureStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mThreeDSecureStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenizationMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/stripe/android/model/SourceCardData;->mTokenizationMethod:Ljava/lang/String;

    return-object v0
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 152
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 153
    const-string v1, "address_line1_check"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mAddressLine1Check:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v1, "address_zip_check"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mAddressZipCheck:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v1, "brand"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mBrand:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v1, "country"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mCountry:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v1, "dynamic_last4"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mDynamicLast4:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v1, "exp_month"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mExpiryMonth:Ljava/lang/Integer;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putIntegerIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 159
    const-string v1, "exp_year"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mExpiryYear:Ljava/lang/Integer;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putIntegerIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 160
    const-string v1, "funding"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mFunding:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v1, "last4"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mLast4:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v1, "three_d_secure"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mThreeDSecureStatus:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v1, "tokenization_method"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mTokenizationMethod:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/stripe/android/model/SourceCardData;->mAdditionalFields:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/stripe/android/model/SourceCardData;->putAdditionalFieldsIntoJsonObject(Lorg/json/JSONObject;Ljava/util/Map;)V

    .line 166
    return-object v0
.end method

.method public toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 173
    const-string v1, "address_line1_check"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mAddressLine1Check:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    const-string v1, "address_zip_check"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mAddressZipCheck:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const-string v1, "brand"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mBrand:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const-string v1, "country"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mCountry:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    const-string v1, "dynamic_last4"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mDynamicLast4:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    const-string v1, "exp_month"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mExpiryMonth:Ljava/lang/Integer;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    const-string v1, "exp_year"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mExpiryYear:Ljava/lang/Integer;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    const-string v1, "funding"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mFunding:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    const-string v1, "last4"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mLast4:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    const-string v1, "three_d_secure"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mThreeDSecureStatus:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    const-string v1, "tokenization_method"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCardData;->mTokenizationMethod:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v1, p0, Lcom/stripe/android/model/SourceCardData;->mAdditionalFields:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/stripe/android/model/SourceCardData;->putAdditionalFieldsIntoMap(Ljava/util/Map;Ljava/util/Map;)V

    .line 186
    invoke-static {v0}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 187
    return-object v0
.end method
