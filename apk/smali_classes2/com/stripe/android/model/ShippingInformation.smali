.class public Lcom/stripe/android/model/ShippingInformation;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "ShippingInformation.java"


# static fields
.field private static final FIELD_ADDRESS:Ljava/lang/String; = "address"

.field private static final FIELD_NAME:Ljava/lang/String; = "name"

.field private static final FIELD_PHONE:Ljava/lang/String; = "phone"


# instance fields
.field private mAddress:Lcom/stripe/android/model/SourceAddress;

.field private mName:Ljava/lang/String;

.field private mPhone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/ShippingInformation;
    .locals 2

    .prologue
    .line 46
    if-nez p0, :cond_0

    .line 47
    const/4 v0, 0x0

    .line 55
    :goto_0
    return-object v0

    .line 50
    :cond_0
    new-instance v0, Lcom/stripe/android/model/ShippingInformation;

    invoke-direct {v0}, Lcom/stripe/android/model/ShippingInformation;-><init>()V

    .line 51
    const-string v1, "name"

    invoke-static {p0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/stripe/android/model/ShippingInformation;->mName:Ljava/lang/String;

    .line 52
    const-string v1, "phone"

    invoke-static {p0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/stripe/android/model/ShippingInformation;->mPhone:Ljava/lang/String;

    .line 53
    const-string v1, "address"

    .line 54
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/stripe/android/model/SourceAddress;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceAddress;

    move-result-object v1

    iput-object v1, v0, Lcom/stripe/android/model/ShippingInformation;->mAddress:Lcom/stripe/android/model/SourceAddress;

    goto :goto_0
.end method


# virtual methods
.method public getAddress()Lcom/stripe/android/model/SourceAddress;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/stripe/android/model/ShippingInformation;->mAddress:Lcom/stripe/android/model/SourceAddress;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/stripe/android/model/ShippingInformation;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/stripe/android/model/ShippingInformation;->mPhone:Ljava/lang/String;

    return-object v0
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 62
    const-string v1, "name"

    iget-object v2, p0, Lcom/stripe/android/model/ShippingInformation;->mName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v1, "phone"

    iget-object v2, p0, Lcom/stripe/android/model/ShippingInformation;->mPhone:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v1, "address"

    iget-object v2, p0, Lcom/stripe/android/model/ShippingInformation;->mAddress:Lcom/stripe/android/model/SourceAddress;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/ShippingInformation;->putStripeJsonModelIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 65
    return-object v0
.end method

.method public toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 72
    const-string v1, "name"

    iget-object v2, p0, Lcom/stripe/android/model/ShippingInformation;->mName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const-string v1, "phone"

    iget-object v2, p0, Lcom/stripe/android/model/ShippingInformation;->mPhone:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    const-string v1, "address"

    iget-object v2, p0, Lcom/stripe/android/model/ShippingInformation;->mAddress:Lcom/stripe/android/model/SourceAddress;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/model/ShippingInformation;->putStripeJsonModelMapIfNotNull(Ljava/util/Map;Ljava/lang/String;Lcom/stripe/android/model/StripeJsonModel;)V

    .line 75
    invoke-static {v0}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 76
    return-object v0
.end method
