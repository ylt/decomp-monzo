.class public Lcom/stripe/android/model/SourceCodeVerification;
.super Lcom/stripe/android/model/StripeJsonModel;
.source "SourceCodeVerification.java"


# static fields
.field static final FAILED:Ljava/lang/String; = "failed"

.field private static final FIELD_ATTEMPTS_REMAINING:Ljava/lang/String; = "attempts_remaining"

.field private static final FIELD_STATUS:Ljava/lang/String; = "status"

.field private static final INVALID_ATTEMPTS_REMAINING:I = -0x1

.field static final PENDING:Ljava/lang/String; = "pending"

.field static final SUCCEEDED:Ljava/lang/String; = "succeeded"


# instance fields
.field private mAttemptsRemaining:I

.field private mStatus:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/stripe/android/model/StripeJsonModel;-><init>()V

    .line 47
    iput p1, p0, Lcom/stripe/android/model/SourceCodeVerification;->mAttemptsRemaining:I

    .line 48
    iput-object p2, p0, Lcom/stripe/android/model/SourceCodeVerification;->mStatus:Ljava/lang/String;

    .line 49
    return-void
.end method

.method private static asStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    const-string v0, "pending"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "pending"

    .line 123
    :goto_0
    return-object v0

    .line 117
    :cond_0
    const-string v0, "succeeded"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    const-string v0, "succeeded"

    goto :goto_0

    .line 119
    :cond_1
    const-string v0, "failed"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    const-string v0, "failed"

    goto :goto_0

    .line 123
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceCodeVerification;
    .locals 3

    .prologue
    .line 103
    if-nez p0, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 107
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/stripe/android/model/SourceCodeVerification;

    const-string v1, "attempts_remaining"

    const/4 v2, -0x1

    .line 108
    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "status"

    .line 109
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/stripe/android/model/SourceCodeVerification;->asStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/stripe/android/model/SourceCodeVerification;-><init>(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/stripe/android/model/SourceCodeVerification;
    .locals 1

    .prologue
    .line 95
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/stripe/android/model/SourceCodeVerification;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceCodeVerification;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 97
    :goto_0
    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method getAttemptsRemaining()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/stripe/android/model/SourceCodeVerification;->mAttemptsRemaining:I

    return v0
.end method

.method getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/stripe/android/model/SourceCodeVerification;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method setAttemptsRemaining(I)V
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/stripe/android/model/SourceCodeVerification;->mAttemptsRemaining:I

    .line 57
    return-void
.end method

.method setStatus(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/stripe/android/model/SourceCodeVerification;->mStatus:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 82
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 85
    :try_start_0
    const-string v1, "attempts_remaining"

    iget v2, p0, Lcom/stripe/android/model/SourceCodeVerification;->mAttemptsRemaining:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 86
    const-string v1, "status"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCodeVerification;->mStatus:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    return-object v0

    .line 87
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 72
    const-string v1, "attempts_remaining"

    iget v2, p0, Lcom/stripe/android/model/SourceCodeVerification;->mAttemptsRemaining:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v1, p0, Lcom/stripe/android/model/SourceCodeVerification;->mStatus:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 74
    const-string v1, "status"

    iget-object v2, p0, Lcom/stripe/android/model/SourceCodeVerification;->mStatus:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    :cond_0
    return-object v0
.end method
