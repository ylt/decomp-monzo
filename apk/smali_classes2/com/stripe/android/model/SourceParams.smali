.class public Lcom/stripe/android/model/SourceParams;
.super Ljava/lang/Object;
.source "SourceParams.java"


# static fields
.field static final API_PARAM_AMOUNT:Ljava/lang/String; = "amount"

.field static final API_PARAM_CLIENT_SECRET:Ljava/lang/String; = "client_secret"

.field static final API_PARAM_CURRENCY:Ljava/lang/String; = "currency"

.field static final API_PARAM_METADATA:Ljava/lang/String; = "metadata"

.field static final API_PARAM_OWNER:Ljava/lang/String; = "owner"

.field static final API_PARAM_REDIRECT:Ljava/lang/String; = "redirect"

.field static final API_PARAM_TOKEN:Ljava/lang/String; = "token"

.field static final API_PARAM_TYPE:Ljava/lang/String; = "type"

.field static final FIELD_ADDRESS:Ljava/lang/String; = "address"

.field static final FIELD_BANK:Ljava/lang/String; = "bank"

.field static final FIELD_CARD:Ljava/lang/String; = "card"

.field static final FIELD_CITY:Ljava/lang/String; = "city"

.field static final FIELD_COUNTRY:Ljava/lang/String; = "country"

.field static final FIELD_CVC:Ljava/lang/String; = "cvc"

.field static final FIELD_EMAIL:Ljava/lang/String; = "email"

.field static final FIELD_EXP_MONTH:Ljava/lang/String; = "exp_month"

.field static final FIELD_EXP_YEAR:Ljava/lang/String; = "exp_year"

.field static final FIELD_IBAN:Ljava/lang/String; = "iban"

.field static final FIELD_LINE_1:Ljava/lang/String; = "line1"

.field static final FIELD_LINE_2:Ljava/lang/String; = "line2"

.field static final FIELD_NAME:Ljava/lang/String; = "name"

.field static final FIELD_NUMBER:Ljava/lang/String; = "number"

.field static final FIELD_POSTAL_CODE:Ljava/lang/String; = "postal_code"

.field static final FIELD_RETURN_URL:Ljava/lang/String; = "return_url"

.field static final FIELD_STATE:Ljava/lang/String; = "state"

.field static final FIELD_STATEMENT_DESCRIPTOR:Ljava/lang/String; = "statement_descriptor"


# instance fields
.field private mAmount:Ljava/lang/Long;

.field private mApiParameterMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrency:Ljava/lang/String;

.field private mMetaData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOwner:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mRedirect:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mToken:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mTypeRaw:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createBancontactParams(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    const-string v1, "bancontact"

    .line 78
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "eur"

    .line 79
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setCurrency(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 80
    invoke-virtual {v0, p0, p1}, Lcom/stripe/android/model/SourceParams;->setAmount(J)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "name"

    .line 81
    invoke-static {v1, p2}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setOwner(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "return_url"

    .line 82
    invoke-static {v1, p3}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setRedirect(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 84
    if-eqz p4, :cond_0

    .line 85
    const-string v1, "statement_descriptor"

    .line 86
    invoke-static {v1, p4}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setApiParameterMap(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 90
    :cond_0
    return-object v0
.end method

.method public static createBitcoinParams(JLjava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    const-string v1, "bitcoin"

    .line 118
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 119
    invoke-virtual {v0, p0, p1}, Lcom/stripe/android/model/SourceParams;->setAmount(J)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 120
    invoke-virtual {v0, p2}, Lcom/stripe/android/model/SourceParams;->setCurrency(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "email"

    .line 121
    invoke-static {v1, p3}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setOwner(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 117
    return-object v0
.end method

.method public static createCardParams(Lcom/stripe/android/model/Card;)Lcom/stripe/android/model/SourceParams;
    .locals 5

    .prologue
    .line 132
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    const-string v1, "card"

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 136
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 137
    const-string v2, "number"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getNumber()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string v2, "exp_month"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getExpMonth()Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string v2, "exp_year"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getExpYear()Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    const-string v2, "cvc"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getCVC()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    invoke-static {v1}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 143
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setApiParameterMap(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 145
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 146
    const-string v2, "line1"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getAddressLine1()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    const-string v2, "line2"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getAddressLine2()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    const-string v2, "city"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getAddressCity()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    const-string v2, "country"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getAddressCountry()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const-string v2, "state"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getAddressState()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    const-string v2, "postal_code"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getAddressZip()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    invoke-static {v1}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 155
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 156
    const-string v3, "name"

    invoke-virtual {p0}, Lcom/stripe/android/model/Card;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 158
    const-string v3, "address"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    :cond_0
    invoke-static {v2}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 161
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 162
    invoke-virtual {v0, v2}, Lcom/stripe/android/model/SourceParams;->setOwner(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 165
    :cond_1
    return-object v0
.end method

.method public static createCustomParams()Lcom/stripe/android/model/SourceParams;
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    return-object v0
.end method

.method public static createGiropayParams(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 2

    .prologue
    .line 183
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    const-string v1, "giropay"

    .line 184
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "eur"

    .line 185
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setCurrency(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 186
    invoke-virtual {v0, p0, p1}, Lcom/stripe/android/model/SourceParams;->setAmount(J)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "name"

    .line 187
    invoke-static {v1, p2}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setOwner(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "return_url"

    .line 188
    invoke-static {v1, p3}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setRedirect(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 190
    if-eqz p4, :cond_0

    .line 191
    const-string v1, "statement_descriptor"

    .line 192
    invoke-static {v1, p4}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 193
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setApiParameterMap(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 196
    :cond_0
    return-object v0
.end method

.method public static createIdealParams(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 4

    .prologue
    .line 216
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    const-string v1, "ideal"

    .line 217
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "eur"

    .line 218
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setCurrency(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 219
    invoke-virtual {v0, p0, p1}, Lcom/stripe/android/model/SourceParams;->setAmount(J)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "name"

    .line 220
    invoke-static {v1, p2}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setOwner(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "return_url"

    .line 221
    invoke-static {v1, p3}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setRedirect(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 223
    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    .line 224
    const-string v1, "statement_descriptor"

    const-string v2, "bank"

    .line 225
    invoke-static {v1, p4, v2, p5}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 228
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setApiParameterMap(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 231
    :cond_0
    return-object v0
.end method

.method public static createRetrieveSourceParams(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 338
    const-string v1, "client_secret"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    return-object v0
.end method

.method public static createSepaDebitParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 4

    .prologue
    .line 253
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    const-string v1, "sepa_debit"

    .line 254
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "eur"

    .line 255
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setCurrency(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 257
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 258
    const-string v2, "line1"

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    const-string v2, "city"

    invoke-interface {v1, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    const-string v2, "postal_code"

    invoke-interface {v1, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    const-string v2, "country"

    invoke-interface {v1, v2, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 264
    const-string v3, "name"

    invoke-interface {v2, v3, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    const-string v3, "address"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    invoke-virtual {v0, v2}, Lcom/stripe/android/model/SourceParams;->setOwner(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v1

    const-string v2, "iban"

    invoke-static {v2, p1}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/stripe/android/model/SourceParams;->setApiParameterMap(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 268
    return-object v0
.end method

.method private static createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 548
    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    return-object v0
.end method

.method private static createSimpleMap(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 557
    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    return-object v0
.end method

.method public static createSofortParams(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 4

    .prologue
    .line 286
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    const-string v1, "sofort"

    .line 287
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "eur"

    .line 288
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setCurrency(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 289
    invoke-virtual {v0, p0, p1}, Lcom/stripe/android/model/SourceParams;->setAmount(J)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "return_url"

    .line 290
    invoke-static {v1, p2}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setRedirect(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 292
    const-string v1, "country"

    invoke-static {v1, p3}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 293
    if-eqz p4, :cond_0

    .line 294
    const-string v2, "statement_descriptor"

    invoke-interface {v1, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    :cond_0
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setApiParameterMap(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 299
    return-object v0
.end method

.method public static createThreeDSecureParams(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 2

    .prologue
    .line 317
    new-instance v0, Lcom/stripe/android/model/SourceParams;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceParams;-><init>()V

    const-string v1, "three_d_secure"

    .line 318
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 319
    invoke-virtual {v0, p2}, Lcom/stripe/android/model/SourceParams;->setCurrency(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 320
    invoke-virtual {v0, p0, p1}, Lcom/stripe/android/model/SourceParams;->setAmount(J)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    const-string v1, "return_url"

    .line 321
    invoke-static {v1, p3}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setRedirect(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    move-result-object v0

    .line 322
    const-string v1, "card"

    invoke-static {v1, p4}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceParams;->setApiParameterMap(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 323
    return-object v0
.end method


# virtual methods
.method public getAmount()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mAmount:Ljava/lang/Long;

    return-object v0
.end method

.method public getApiParameterMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 355
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mApiParameterMap:Ljava/util/Map;

    return-object v0
.end method

.method public getCurrency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public getMetaData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 403
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mMetaData:Ljava/util/Map;

    return-object v0
.end method

.method public getOwner()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mOwner:Ljava/util/Map;

    return-object v0
.end method

.method public getRedirect()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mRedirect:Ljava/util/Map;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeRaw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mTypeRaw:Ljava/lang/String;

    return-object v0
.end method

.method public setAmount(J)Lcom/stripe/android/model/SourceParams;
    .locals 1

    .prologue
    .line 413
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/stripe/android/model/SourceParams;->mAmount:Ljava/lang/Long;

    .line 414
    return-object p0
.end method

.method public setApiParameterMap(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/stripe/android/model/SourceParams;"
        }
    .end annotation

    .prologue
    .line 423
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mApiParameterMap:Ljava/util/Map;

    .line 424
    return-object p0
.end method

.method public setCurrency(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 0

    .prologue
    .line 432
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mCurrency:Ljava/lang/String;

    .line 433
    return-object p0
.end method

.method public setMetaData(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/stripe/android/model/SourceParams;"
        }
    .end annotation

    .prologue
    .line 507
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mMetaData:Ljava/util/Map;

    .line 508
    return-object p0
.end method

.method public setOwner(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/stripe/android/model/SourceParams;"
        }
    .end annotation

    .prologue
    .line 441
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mOwner:Ljava/util/Map;

    .line 442
    return-object p0
.end method

.method public setRedirect(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/stripe/android/model/SourceParams;"
        }
    .end annotation

    .prologue
    .line 453
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mRedirect:Ljava/util/Map;

    .line 454
    return-object p0
.end method

.method public setReturnUrl(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mRedirect:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 464
    const-string v0, "return_url"

    invoke-static {v0, p1}, Lcom/stripe/android/model/SourceParams;->createSimpleMap(Ljava/lang/String;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/stripe/android/model/SourceParams;->setRedirect(Ljava/util/Map;)Lcom/stripe/android/model/SourceParams;

    .line 468
    :goto_0
    return-object p0

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mRedirect:Ljava/util/Map;

    const-string v1, "return_url"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setToken(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mToken:Ljava/lang/String;

    .line 519
    return-object p0
.end method

.method public setType(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mType:Ljava/lang/String;

    .line 480
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mTypeRaw:Ljava/lang/String;

    .line 481
    return-object p0
.end method

.method public setTypeRaw(Ljava/lang/String;)Lcom/stripe/android/model/SourceParams;
    .locals 1

    .prologue
    .line 492
    invoke-static {p1}, Lcom/stripe/android/model/Source;->asSourceType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stripe/android/model/SourceParams;->mType:Ljava/lang/String;

    .line 493
    iget-object v0, p0, Lcom/stripe/android/model/SourceParams;->mType:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 494
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/stripe/android/model/SourceParams;->mType:Ljava/lang/String;

    .line 496
    :cond_0
    iput-object p1, p0, Lcom/stripe/android/model/SourceParams;->mTypeRaw:Ljava/lang/String;

    .line 497
    return-object p0
.end method

.method public toParamMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 530
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 532
    const-string v1, "type"

    iget-object v2, p0, Lcom/stripe/android/model/SourceParams;->mTypeRaw:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    iget-object v1, p0, Lcom/stripe/android/model/SourceParams;->mTypeRaw:Ljava/lang/String;

    iget-object v2, p0, Lcom/stripe/android/model/SourceParams;->mApiParameterMap:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    const-string v1, "amount"

    iget-object v2, p0, Lcom/stripe/android/model/SourceParams;->mAmount:Ljava/lang/Long;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    const-string v1, "currency"

    iget-object v2, p0, Lcom/stripe/android/model/SourceParams;->mCurrency:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    const-string v1, "owner"

    iget-object v2, p0, Lcom/stripe/android/model/SourceParams;->mOwner:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    const-string v1, "redirect"

    iget-object v2, p0, Lcom/stripe/android/model/SourceParams;->mRedirect:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    const-string v1, "metadata"

    iget-object v2, p0, Lcom/stripe/android/model/SourceParams;->mMetaData:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    const-string v1, "token"

    iget-object v2, p0, Lcom/stripe/android/model/SourceParams;->mToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    invoke-static {v0}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 541
    return-object v0
.end method
