.class public Lcom/stripe/android/model/BankAccount;
.super Ljava/lang/Object;
.source "BankAccount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/model/BankAccount$BankAccountType;
    }
.end annotation


# static fields
.field public static final TYPE_COMPANY:Ljava/lang/String; = "company"

.field public static final TYPE_INDIVIDUAL:Ljava/lang/String; = "individual"


# instance fields
.field private mAccountHolderName:Ljava/lang/String;

.field private mAccountHolderType:Ljava/lang/String;

.field private mAccountNumber:Ljava/lang/String;

.field private mBankName:Ljava/lang/String;

.field private mCountryCode:Ljava/lang/String;

.field private mCurrency:Ljava/lang/String;

.field private mFingerprint:Ljava/lang/String;

.field private mLast4:Ljava/lang/String;

.field private mRoutingNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/stripe/android/model/BankAccount;->mAccountNumber:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/stripe/android/model/BankAccount;->mCountryCode:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lcom/stripe/android/model/BankAccount;->mCurrency:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/stripe/android/model/BankAccount;->mRoutingNumber:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/stripe/android/model/BankAccount;->mAccountHolderName:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Lcom/stripe/android/model/BankAccount;->mAccountHolderType:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lcom/stripe/android/model/BankAccount;->mBankName:Ljava/lang/String;

    .line 80
    iput-object p4, p0, Lcom/stripe/android/model/BankAccount;->mCountryCode:Ljava/lang/String;

    .line 81
    iput-object p5, p0, Lcom/stripe/android/model/BankAccount;->mCurrency:Ljava/lang/String;

    .line 82
    iput-object p6, p0, Lcom/stripe/android/model/BankAccount;->mFingerprint:Ljava/lang/String;

    .line 83
    iput-object p7, p0, Lcom/stripe/android/model/BankAccount;->mLast4:Ljava/lang/String;

    .line 84
    iput-object p8, p0, Lcom/stripe/android/model/BankAccount;->mRoutingNumber:Ljava/lang/String;

    .line 85
    return-void
.end method


# virtual methods
.method public getAccountHolderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mAccountHolderName:Ljava/lang/String;

    return-object v0
.end method

.method public getAccountHolderType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mAccountHolderType:Ljava/lang/String;

    return-object v0
.end method

.method public getAccountNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mAccountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getBankName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mBankName:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public getFingerprint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mFingerprint:Ljava/lang/String;

    return-object v0
.end method

.method public getLast4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mLast4:Ljava/lang/String;

    return-object v0
.end method

.method public getRoutingNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/stripe/android/model/BankAccount;->mRoutingNumber:Ljava/lang/String;

    return-object v0
.end method

.method public setAccountHolderName(Ljava/lang/String;)Lcom/stripe/android/model/BankAccount;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/stripe/android/model/BankAccount;->mAccountHolderName:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method public setAccountHolderType(Ljava/lang/String;)Lcom/stripe/android/model/BankAccount;
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/stripe/android/model/BankAccount;->mAccountHolderType:Ljava/lang/String;

    .line 112
    return-object p0
.end method
