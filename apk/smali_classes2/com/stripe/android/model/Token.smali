.class public Lcom/stripe/android/model/Token;
.super Ljava/lang/Object;
.source "Token.java"

# interfaces
.implements Lcom/stripe/android/model/StripePaymentSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/model/Token$TokenType;
    }
.end annotation


# static fields
.field public static final TYPE_BANK_ACCOUNT:Ljava/lang/String; = "bank_account"

.field public static final TYPE_CARD:Ljava/lang/String; = "card"

.field public static final TYPE_PII:Ljava/lang/String; = "pii"


# instance fields
.field private final mBankAccount:Lcom/stripe/android/model/BankAccount;

.field private final mCard:Lcom/stripe/android/model/Card;

.field private final mCreated:Ljava/util/Date;

.field private final mId:Ljava/lang/String;

.field private final mLivemode:Z

.field private final mType:Ljava/lang/String;

.field private final mUsed:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/util/Date;Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/stripe/android/model/Token;->mId:Ljava/lang/String;

    .line 78
    const-string v0, "pii"

    iput-object v0, p0, Lcom/stripe/android/model/Token;->mType:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lcom/stripe/android/model/Token;->mCreated:Ljava/util/Date;

    .line 80
    iput-object v1, p0, Lcom/stripe/android/model/Token;->mCard:Lcom/stripe/android/model/Card;

    .line 81
    iput-object v1, p0, Lcom/stripe/android/model/Token;->mBankAccount:Lcom/stripe/android/model/BankAccount;

    .line 82
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/stripe/android/model/Token;->mUsed:Z

    .line 83
    iput-boolean p2, p0, Lcom/stripe/android/model/Token;->mLivemode:Z

    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLjava/util/Date;Ljava/lang/Boolean;Lcom/stripe/android/model/BankAccount;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/stripe/android/model/Token;->mId:Ljava/lang/String;

    .line 59
    const-string v0, "bank_account"

    iput-object v0, p0, Lcom/stripe/android/model/Token;->mType:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/stripe/android/model/Token;->mCreated:Ljava/util/Date;

    .line 61
    iput-boolean p2, p0, Lcom/stripe/android/model/Token;->mLivemode:Z

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/stripe/android/model/Token;->mCard:Lcom/stripe/android/model/Card;

    .line 63
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/stripe/android/model/Token;->mUsed:Z

    .line 64
    iput-object p5, p0, Lcom/stripe/android/model/Token;->mBankAccount:Lcom/stripe/android/model/BankAccount;

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLjava/util/Date;Ljava/lang/Boolean;Lcom/stripe/android/model/Card;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/stripe/android/model/Token;->mId:Ljava/lang/String;

    .line 40
    const-string v0, "card"

    iput-object v0, p0, Lcom/stripe/android/model/Token;->mType:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/stripe/android/model/Token;->mCreated:Ljava/util/Date;

    .line 42
    iput-boolean p2, p0, Lcom/stripe/android/model/Token;->mLivemode:Z

    .line 43
    iput-object p5, p0, Lcom/stripe/android/model/Token;->mCard:Lcom/stripe/android/model/Card;

    .line 44
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/stripe/android/model/Token;->mUsed:Z

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/stripe/android/model/Token;->mBankAccount:Lcom/stripe/android/model/BankAccount;

    .line 46
    return-void
.end method


# virtual methods
.method public getBankAccount()Lcom/stripe/android/model/BankAccount;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/stripe/android/model/Token;->mBankAccount:Lcom/stripe/android/model/BankAccount;

    return-object v0
.end method

.method public getCard()Lcom/stripe/android/model/Card;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/stripe/android/model/Token;->mCard:Lcom/stripe/android/model/Card;

    return-object v0
.end method

.method public getCreated()Ljava/util/Date;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/stripe/android/model/Token;->mCreated:Ljava/util/Date;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/stripe/android/model/Token;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getLivemode()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/stripe/android/model/Token;->mLivemode:Z

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/stripe/android/model/Token;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getUsed()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/stripe/android/model/Token;->mUsed:Z

    return v0
.end method
