.class public Lcom/stripe/android/model/SourceSepaDebitData;
.super Lcom/stripe/android/model/StripeSourceTypeModel;
.source "SourceSepaDebitData.java"


# static fields
.field private static final FIELD_BANK_CODE:Ljava/lang/String; = "bank_code"

.field private static final FIELD_BRANCH_CODE:Ljava/lang/String; = "branch_code"

.field private static final FIELD_COUNTRY:Ljava/lang/String; = "country"

.field private static final FIELD_FINGERPRINT:Ljava/lang/String; = "fingerprint"

.field private static final FIELD_LAST4:Ljava/lang/String; = "last4"

.field private static final FIELD_MANDATE_REFERENCE:Ljava/lang/String; = "mandate_reference"

.field private static final FIELD_MANDATE_URL:Ljava/lang/String; = "mandate_url"


# instance fields
.field private mBankCode:Ljava/lang/String;

.field private mBranchCode:Ljava/lang/String;

.field private mCountry:Ljava/lang/String;

.field private mFingerPrint:Ljava/lang/String;

.field private mLast4:Ljava/lang/String;

.field private mMandateReference:Ljava/lang/String;

.field private mMandateUrl:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/stripe/android/model/StripeSourceTypeModel;-><init>()V

    .line 41
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "bank_code"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "branch_code"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "country"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "fingerprint"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "last4"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mandate_reference"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mandate_url"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/stripe/android/model/SourceSepaDebitData;->addStandardFields([Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public static fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 3

    .prologue
    .line 53
    if-nez p0, :cond_1

    .line 54
    const/4 v0, 0x0

    .line 71
    :cond_0
    :goto_0
    return-object v0

    .line 57
    :cond_1
    new-instance v0, Lcom/stripe/android/model/SourceSepaDebitData;

    invoke-direct {v0}, Lcom/stripe/android/model/SourceSepaDebitData;-><init>()V

    .line 58
    const-string v1, "bank_code"

    invoke-static {p0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/stripe/android/model/SourceSepaDebitData;->setBankCode(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;

    move-result-object v1

    const-string v2, "branch_code"

    .line 59
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceSepaDebitData;->setBranchCode(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;

    move-result-object v1

    const-string v2, "country"

    .line 60
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceSepaDebitData;->setCountry(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;

    move-result-object v1

    const-string v2, "fingerprint"

    .line 61
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceSepaDebitData;->setFingerPrint(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;

    move-result-object v1

    const-string v2, "last4"

    .line 62
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceSepaDebitData;->setLast4(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;

    move-result-object v1

    const-string v2, "mandate_reference"

    .line 63
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceSepaDebitData;->setMandateReference(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;

    move-result-object v1

    const-string v2, "mandate_url"

    .line 64
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stripe/android/model/SourceSepaDebitData;->setMandateUrl(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;

    .line 66
    iget-object v1, v0, Lcom/stripe/android/model/SourceSepaDebitData;->mStandardFields:Ljava/util/Set;

    .line 67
    invoke-static {p0, v1}, Lcom/stripe/android/model/SourceSepaDebitData;->jsonObjectToMapWithoutKeys(Lorg/json/JSONObject;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v1

    .line 68
    if-eqz v1, :cond_0

    .line 69
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/SourceSepaDebitData;->setAdditionalFields(Ljava/util/Map;)V

    goto :goto_0
.end method

.method static fromString(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 1

    .prologue
    .line 139
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/stripe/android/model/SourceSepaDebitData;->fromJson(Lorg/json/JSONObject;)Lcom/stripe/android/model/SourceSepaDebitData;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setBankCode(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mBankCode:Ljava/lang/String;

    .line 147
    return-object p0
.end method

.method private setBranchCode(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mBranchCode:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method private setCountry(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mCountry:Ljava/lang/String;

    .line 157
    return-object p0
.end method

.method private setFingerPrint(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mFingerPrint:Ljava/lang/String;

    .line 162
    return-object p0
.end method

.method private setLast4(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mLast4:Ljava/lang/String;

    .line 167
    return-object p0
.end method

.method private setMandateReference(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mMandateReference:Ljava/lang/String;

    .line 172
    return-object p0
.end method

.method private setMandateUrl(Ljava/lang/String;)Lcom/stripe/android/model/SourceSepaDebitData;
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mMandateUrl:Ljava/lang/String;

    .line 177
    return-object p0
.end method


# virtual methods
.method public bridge synthetic getAdditionalFields()Ljava/util/Map;
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/stripe/android/model/StripeSourceTypeModel;->getAdditionalFields()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getBankCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mBankCode:Ljava/lang/String;

    return-object v0
.end method

.method public getBranchCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mBranchCode:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getFingerPrint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mFingerPrint:Ljava/lang/String;

    return-object v0
.end method

.method public getLast4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mLast4:Ljava/lang/String;

    return-object v0
.end method

.method public getMandateReference()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mMandateReference:Ljava/lang/String;

    return-object v0
.end method

.method public getMandateUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mMandateUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toJson()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 105
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 106
    const-string v1, "bank_code"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mBankCode:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v1, "branch_code"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mBranchCode:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v1, "country"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mCountry:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v1, "fingerprint"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mFingerPrint:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v1, "last4"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mLast4:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v1, "mandate_reference"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mMandateReference:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v1, "mandate_url"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mMandateUrl:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/stripe/android/util/StripeJsonUtils;->putStringIfNotNull(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mAdditionalFields:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/stripe/android/model/SourceSepaDebitData;->putAdditionalFieldsIntoJsonObject(Lorg/json/JSONObject;Ljava/util/Map;)V

    .line 115
    return-object v0
.end method

.method public toMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 122
    const-string v1, "bank_code"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mBankCode:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string v1, "branch_code"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mBranchCode:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    const-string v1, "country"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mCountry:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    const-string v1, "fingerprint"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mFingerPrint:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    const-string v1, "last4"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mLast4:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const-string v1, "mandate_reference"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mMandateReference:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const-string v1, "mandate_url"

    iget-object v2, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mMandateUrl:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    iget-object v1, p0, Lcom/stripe/android/model/SourceSepaDebitData;->mAdditionalFields:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/stripe/android/model/SourceSepaDebitData;->putAdditionalFieldsIntoMap(Ljava/util/Map;Ljava/util/Map;)V

    .line 131
    invoke-static {v0}, Lcom/stripe/android/util/StripeNetworkUtils;->removeNullAndEmptyParams(Ljava/util/Map;)V

    .line 132
    return-object v0
.end method
