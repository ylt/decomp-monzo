.class public Lcom/stripe/android/view/LockableHorizontalScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "LockableHorizontalScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/view/LockableHorizontalScrollView$LockableScrollChangedListener;
    }
.end annotation


# instance fields
.field private mLockableScrollChangedListener:Lcom/stripe/android/view/LockableHorizontalScrollView$LockableScrollChangedListener;

.field private mScrollable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method


# virtual methods
.method public isScrollable()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mScrollable:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mScrollable:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 70
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mScrollable:Z

    if-eqz v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public scrollTo(II)V
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mScrollable:Z

    if-nez v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_0
.end method

.method setScrollChangedListener(Lcom/stripe/android/view/LockableHorizontalScrollView$LockableScrollChangedListener;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mLockableScrollChangedListener:Lcom/stripe/android/view/LockableHorizontalScrollView$LockableScrollChangedListener;

    .line 81
    return-void
.end method

.method public setScrollable(Z)V
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mScrollable:Z

    .line 45
    invoke-virtual {p0, p1}, Lcom/stripe/android/view/LockableHorizontalScrollView;->setSmoothScrollingEnabled(Z)V

    .line 46
    return-void
.end method

.method wrappedSmoothScrollBy(II)V
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mScrollable:Z

    if-nez v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/stripe/android/view/LockableHorizontalScrollView;->smoothScrollBy(II)V

    .line 96
    iget-object v0, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mLockableScrollChangedListener:Lcom/stripe/android/view/LockableHorizontalScrollView$LockableScrollChangedListener;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/stripe/android/view/LockableHorizontalScrollView;->mLockableScrollChangedListener:Lcom/stripe/android/view/LockableHorizontalScrollView$LockableScrollChangedListener;

    invoke-interface {v0, p1, p2}, Lcom/stripe/android/view/LockableHorizontalScrollView$LockableScrollChangedListener;->onSmoothScrollBy(II)V

    goto :goto_0
.end method
