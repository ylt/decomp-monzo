.class Lcom/stripe/android/view/CardInputWidget$13;
.super Landroid/view/animation/Animation;
.source "CardInputWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/view/CardInputWidget;->scrollRight()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stripe/android/view/CardInputWidget;


# direct methods
.method constructor <init>(Lcom/stripe/android/view/CardInputWidget;)V
    .locals 0

    .prologue
    .line 659
    iput-object p1, p0, Lcom/stripe/android/view/CardInputWidget$13;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2

    .prologue
    .line 662
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 663
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$13;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 664
    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$300(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardNumberEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 665
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$13;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 666
    invoke-static {v1}, Lcom/stripe/android/view/CardInputWidget;->access$1100(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    move-result-object v1

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->hiddenCardWidth:I

    mul-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 667
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$13;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v1}, Lcom/stripe/android/view/CardInputWidget;->access$300(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardNumberEditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/stripe/android/view/CardNumberEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 668
    return-void
.end method
