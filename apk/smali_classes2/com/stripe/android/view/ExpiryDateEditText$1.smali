.class Lcom/stripe/android/view/ExpiryDateEditText$1;
.super Ljava/lang/Object;
.source "ExpiryDateEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/view/ExpiryDateEditText;->listenForTextChanges()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field ignoreChanges:Z

.field latestChangeStart:I

.field latestInsertionSize:I

.field parts:[Ljava/lang/String;

.field final synthetic this$0:Lcom/stripe/android/view/ExpiryDateEditText;


# direct methods
.method constructor <init>(Lcom/stripe/android/view/ExpiryDateEditText;)V
    .locals 1

    .prologue
    .line 88
    iput-object p1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->ignoreChanges:Z

    .line 92
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 169
    .line 170
    iget-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-static {v0}, Lcom/stripe/android/util/DateUtils;->isValidMonth(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 179
    :goto_0
    iget-object v3, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v4, :cond_2

    .line 180
    iget-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-static {v0}, Lcom/stripe/android/view/ExpiryDateEditText;->access$000(Lcom/stripe/android/view/ExpiryDateEditText;)Z

    move-result v3

    .line 181
    iget-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    iget-object v4, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/stripe/android/view/ExpiryDateEditText;->access$100(Lcom/stripe/android/view/ExpiryDateEditText;[Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-static {v0}, Lcom/stripe/android/view/ExpiryDateEditText;->access$000(Lcom/stripe/android/view/ExpiryDateEditText;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 185
    :goto_1
    if-nez v3, :cond_0

    iget-object v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-static {v1}, Lcom/stripe/android/view/ExpiryDateEditText;->access$000(Lcom/stripe/android/view/ExpiryDateEditText;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-static {v1}, Lcom/stripe/android/view/ExpiryDateEditText;->access$200(Lcom/stripe/android/view/ExpiryDateEditText;)Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-static {v1}, Lcom/stripe/android/view/ExpiryDateEditText;->access$200(Lcom/stripe/android/view/ExpiryDateEditText;)Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;->onExpiryDateComplete()V

    .line 192
    :cond_0
    :goto_2
    iget-object v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-virtual {v1, v0}, Lcom/stripe/android/view/ExpiryDateEditText;->setShouldShowError(Z)V

    .line 193
    return-void

    :cond_1
    move v0, v2

    .line 184
    goto :goto_1

    .line 189
    :cond_2
    iget-object v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-static {v1, v2}, Lcom/stripe/android/view/ExpiryDateEditText;->access$002(Lcom/stripe/android/view/ExpiryDateEditText;Z)Z

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->ignoreChanges:Z

    if-eqz v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    iput p2, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestChangeStart:I

    .line 100
    iput p4, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestInsertionSize:I

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 105
    iget-boolean v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->ignoreChanges:Z

    if-eqz v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    const-string v4, ""

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v2, :cond_5

    iget v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestChangeStart:I

    if-nez v1, :cond_5

    iget v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestInsertionSize:I

    if-ne v1, v2, :cond_5

    .line 116
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 117
    const/16 v4, 0x30

    if-eq v1, v4, :cond_1

    const/16 v4, 0x31

    if-eq v1, v4, :cond_1

    .line 122
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    iget v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestInsertionSize:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestInsertionSize:I

    .line 136
    :cond_1
    :goto_1
    invoke-static {v0}, Lcom/stripe/android/util/DateUtils;->separateDateStringParts(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-static {v1}, Lcom/stripe/android/util/DateUtils;->isValidMonth(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v2

    .line 142
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    iget-object v5, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    aget-object v5, v5, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    iget-object v5, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v5, v6, :cond_2

    iget v5, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestInsertionSize:I

    if-lez v5, :cond_2

    if-eqz v1, :cond_3

    .line 146
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v6, :cond_4

    .line 147
    :cond_3
    const-string v0, "/"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :cond_4
    iget-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->parts:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    .line 154
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    iget v5, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestChangeStart:I

    iget v6, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestInsertionSize:I

    .line 153
    invoke-virtual {v1, v4, v5, v6}, Lcom/stripe/android/view/ExpiryDateEditText;->updateSelectionIndex(III)I

    move-result v1

    .line 158
    iput-boolean v2, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->ignoreChanges:Z

    .line 159
    iget-object v2, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-virtual {v2, v0}, Lcom/stripe/android/view/ExpiryDateEditText;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->this$0:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/ExpiryDateEditText;->setSelection(I)V

    .line 161
    iput-boolean v3, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->ignoreChanges:Z

    goto/16 :goto_0

    .line 125
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v6, :cond_1

    iget v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestChangeStart:I

    if-ne v1, v6, :cond_1

    iget v1, p0, Lcom/stripe/android/view/ExpiryDateEditText$1;->latestInsertionSize:I

    if-nez v1, :cond_1

    .line 132
    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_2
.end method
