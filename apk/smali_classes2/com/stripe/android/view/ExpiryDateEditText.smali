.class public Lcom/stripe/android/view/ExpiryDateEditText;
.super Lcom/stripe/android/view/StripeEditText;
.source "ExpiryDateEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;
    }
.end annotation


# static fields
.field static final INVALID_INPUT:I = -0x1

.field private static final MAX_INPUT_LENGTH:I = 0x5


# instance fields
.field private mExpiryDateEditListener:Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;

.field private mIsDateValid:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/stripe/android/view/StripeEditText;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-direct {p0}, Lcom/stripe/android/view/ExpiryDateEditText;->listenForTextChanges()V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/stripe/android/view/StripeEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-direct {p0}, Lcom/stripe/android/view/ExpiryDateEditText;->listenForTextChanges()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/stripe/android/view/StripeEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-direct {p0}, Lcom/stripe/android/view/ExpiryDateEditText;->listenForTextChanges()V

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/stripe/android/view/ExpiryDateEditText;)Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/stripe/android/view/ExpiryDateEditText;->mIsDateValid:Z

    return v0
.end method

.method static synthetic access$002(Lcom/stripe/android/view/ExpiryDateEditText;Z)Z
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/stripe/android/view/ExpiryDateEditText;->mIsDateValid:Z

    return p1
.end method

.method static synthetic access$100(Lcom/stripe/android/view/ExpiryDateEditText;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/stripe/android/view/ExpiryDateEditText;->updateInputValues([Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/stripe/android/view/ExpiryDateEditText;)Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/stripe/android/view/ExpiryDateEditText;->mExpiryDateEditListener:Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;

    return-object v0
.end method

.method private listenForTextChanges()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/stripe/android/view/ExpiryDateEditText$1;

    invoke-direct {v0, p0}, Lcom/stripe/android/view/ExpiryDateEditText$1;-><init>(Lcom/stripe/android/view/ExpiryDateEditText;)V

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/ExpiryDateEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 195
    return-void
.end method

.method private updateInputValues([Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 235
    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, v3, :cond_0

    move v0, v1

    .line 245
    :goto_0
    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v2, v3, :cond_1

    .line 255
    :goto_1
    invoke-static {v0, v1}, Lcom/stripe/android/util/DateUtils;->isExpiryDataValid(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/stripe/android/view/ExpiryDateEditText;->mIsDateValid:Z

    .line 256
    return-void

    .line 239
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 240
    :catch_0
    move-exception v0

    move v0, v1

    .line 241
    goto :goto_0

    .line 249
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/stripe/android/util/DateUtils;->convertTwoDigitYearToFour(I)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    goto :goto_1

    .line 250
    :catch_1
    move-exception v2

    goto :goto_1
.end method


# virtual methods
.method public getValidDateFields()[I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-boolean v1, p0, Lcom/stripe/android/view/ExpiryDateEditText;->mIsDateValid:Z

    if-nez v1, :cond_0

    .line 80
    :goto_0
    return-object v0

    .line 67
    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 68
    invoke-virtual {p0}, Lcom/stripe/android/view/ExpiryDateEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 69
    invoke-static {v2}, Lcom/stripe/android/util/DateUtils;->separateDateStringParts(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 72
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    aget-object v4, v2, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v1, v3

    .line 73
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/stripe/android/util/DateUtils;->convertTwoDigitYearToFour(I)I

    move-result v2

    aput v2, v1, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 80
    goto :goto_0

    .line 74
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public isDateValid()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/stripe/android/view/ExpiryDateEditText;->mIsDateValid:Z

    return v0
.end method

.method public setExpiryDateEditListener(Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/stripe/android/view/ExpiryDateEditText;->mExpiryDateEditListener:Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;

    .line 85
    return-void
.end method

.method updateSelectionIndex(III)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 211
    .line 214
    if-gt p2, v3, :cond_3

    add-int v2, p2, p3

    if-lt v2, v3, :cond_3

    move v2, v0

    .line 220
    :goto_0
    if-nez p3, :cond_0

    const/4 v3, 0x3

    if-ne p2, v3, :cond_0

    move v1, v0

    .line 224
    :cond_0
    add-int v0, p2, p3

    add-int/2addr v0, v2

    .line 225
    if-eqz v1, :cond_1

    if-lez v0, :cond_1

    .line 226
    add-int/lit8 v0, v0, -0x1

    .line 229
    :cond_1
    if-gt v0, p1, :cond_2

    move p1, v0

    :cond_2
    return p1

    :cond_3
    move v2, v1

    goto :goto_0
.end method
