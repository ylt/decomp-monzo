.class Lcom/stripe/android/view/CardInputWidget$4;
.super Ljava/lang/Object;
.source "CardInputWidget.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/view/CardInputWidget;->initView(Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stripe/android/view/CardInputWidget;


# direct methods
.method constructor <init>(Lcom/stripe/android/view/CardInputWidget;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/stripe/android/view/CardInputWidget$4;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 510
    if-eqz p2, :cond_0

    .line 511
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$4;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$200(Lcom/stripe/android/view/CardInputWidget;)V

    .line 512
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$4;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$100(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardInputWidget$CardInputListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$4;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$100(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardInputWidget$CardInputListener;

    move-result-object v0

    const-string v1, "focus_cvc"

    invoke-interface {v0, v1}, Lcom/stripe/android/view/CardInputWidget$CardInputListener;->onFocusChange(Ljava/lang/String;)V

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$4;->this$0:Lcom/stripe/android/view/CardInputWidget;

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$4;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 517
    invoke-static {v1}, Lcom/stripe/android/view/CardInputWidget;->access$300(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardNumberEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/stripe/android/view/CardNumberEditText;->getCardBrand()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget$4;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 519
    invoke-static {v2}, Lcom/stripe/android/view/CardInputWidget;->access$400(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/StripeEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/stripe/android/view/StripeEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 516
    invoke-static {v0, v1, p2, v2}, Lcom/stripe/android/view/CardInputWidget;->access$500(Lcom/stripe/android/view/CardInputWidget;Ljava/lang/String;ZLjava/lang/String;)V

    .line 520
    return-void
.end method
