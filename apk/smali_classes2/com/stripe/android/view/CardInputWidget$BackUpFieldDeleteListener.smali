.class Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;
.super Ljava/lang/Object;
.source "CardInputWidget.java"

# interfaces
.implements Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stripe/android/view/CardInputWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackUpFieldDeleteListener"
.end annotation


# instance fields
.field private backUpTarget:Lcom/stripe/android/view/StripeEditText;

.field final synthetic this$0:Lcom/stripe/android/view/CardInputWidget;


# direct methods
.method constructor <init>(Lcom/stripe/android/view/CardInputWidget;Lcom/stripe/android/view/StripeEditText;)V
    .locals 0

    .prologue
    .line 940
    iput-object p1, p0, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 941
    iput-object p2, p0, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;->backUpTarget:Lcom/stripe/android/view/StripeEditText;

    .line 942
    return-void
.end method


# virtual methods
.method public onDeleteEmpty()V
    .locals 4

    .prologue
    .line 946
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;->backUpTarget:Lcom/stripe/android/view/StripeEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/StripeEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 947
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 948
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;->backUpTarget:Lcom/stripe/android/view/StripeEditText;

    const/4 v2, 0x0

    .line 949
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 948
    invoke-virtual {v1, v0}, Lcom/stripe/android/view/StripeEditText;->setText(Ljava/lang/CharSequence;)V

    .line 951
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;->backUpTarget:Lcom/stripe/android/view/StripeEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/StripeEditText;->requestFocus()Z

    .line 952
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;->backUpTarget:Lcom/stripe/android/view/StripeEditText;

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;->backUpTarget:Lcom/stripe/android/view/StripeEditText;

    invoke-virtual {v1}, Lcom/stripe/android/view/StripeEditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setSelection(I)V

    .line 953
    return-void
.end method
