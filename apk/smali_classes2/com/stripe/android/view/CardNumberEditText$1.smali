.class Lcom/stripe/android/view/CardNumberEditText$1;
.super Ljava/lang/Object;
.source "CardNumberEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/view/CardNumberEditText;->listenForTextChanges()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field latestChangeStart:I

.field latestInsertionSize:I

.field final synthetic this$0:Lcom/stripe/android/view/CardNumberEditText;


# direct methods
.method constructor <init>(Lcom/stripe/android/view/CardNumberEditText;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 205
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    iget-object v3, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v3}, Lcom/stripe/android/view/CardNumberEditText;->access$200(Lcom/stripe/android/view/CardNumberEditText;)I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 206
    iget-object v2, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v2}, Lcom/stripe/android/view/CardNumberEditText;->access$300(Lcom/stripe/android/view/CardNumberEditText;)Z

    move-result v2

    .line 207
    iget-object v3, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/stripe/android/util/CardUtils;->isValidCardNumber(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v3, v4}, Lcom/stripe/android/view/CardNumberEditText;->access$302(Lcom/stripe/android/view/CardNumberEditText;Z)Z

    .line 208
    iget-object v3, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    iget-object v4, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v4}, Lcom/stripe/android/view/CardNumberEditText;->access$300(Lcom/stripe/android/view/CardNumberEditText;)Z

    move-result v4

    if-nez v4, :cond_1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/stripe/android/view/CardNumberEditText;->setShouldShowError(Z)V

    .line 209
    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v0}, Lcom/stripe/android/view/CardNumberEditText;->access$300(Lcom/stripe/android/view/CardNumberEditText;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v0}, Lcom/stripe/android/view/CardNumberEditText;->access$400(Lcom/stripe/android/view/CardNumberEditText;)Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v0}, Lcom/stripe/android/view/CardNumberEditText;->access$400(Lcom/stripe/android/view/CardNumberEditText;)Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;->onCardNumberComplete()V

    .line 218
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 208
    goto :goto_0

    .line 213
    :cond_2
    iget-object v2, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    iget-object v3, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v3}, Lcom/stripe/android/view/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    .line 214
    invoke-virtual {v3}, Lcom/stripe/android/view/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/stripe/android/util/CardUtils;->isValidCardNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 213
    :goto_2
    invoke-static {v2, v0}, Lcom/stripe/android/view/CardNumberEditText;->access$302(Lcom/stripe/android/view/CardNumberEditText;Z)Z

    .line 216
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/CardNumberEditText;->setShouldShowError(Z)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 214
    goto :goto_2
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v0}, Lcom/stripe/android/view/CardNumberEditText;->access$000(Lcom/stripe/android/view/CardNumberEditText;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    iput p2, p0, Lcom/stripe/android/view/CardNumberEditText$1;->latestChangeStart:I

    .line 153
    iput p4, p0, Lcom/stripe/android/view/CardNumberEditText$1;->latestInsertionSize:I

    .line 155
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 159
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v0}, Lcom/stripe/android/view/CardNumberEditText;->access$000(Lcom/stripe/android/view/CardNumberEditText;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    const/4 v0, 0x4

    if-ge p2, v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/stripe/android/view/CardNumberEditText;->access$100(Lcom/stripe/android/view/CardNumberEditText;Ljava/lang/String;)V

    .line 167
    :cond_2
    const/16 v0, 0x10

    if-gt p2, v0, :cond_0

    .line 172
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/util/StripeTextUtils;->removeSpacesAndHyphens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    if-eqz v0, :cond_0

    .line 177
    iget-object v2, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    iget-object v2, v2, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/stripe/android/util/CardUtils;->separateCardNumberGroups(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 179
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 180
    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_3

    .line 181
    aget-object v4, v2, v0

    if-nez v4, :cond_4

    .line 191
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    iget-object v2, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    .line 193
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    iget v4, p0, Lcom/stripe/android/view/CardNumberEditText$1;->latestChangeStart:I

    iget v5, p0, Lcom/stripe/android/view/CardNumberEditText$1;->latestInsertionSize:I

    .line 192
    invoke-virtual {v2, v3, v4, v5}, Lcom/stripe/android/view/CardNumberEditText;->updateSelectionIndex(III)I

    move-result v2

    .line 197
    iget-object v3, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/stripe/android/view/CardNumberEditText;->access$002(Lcom/stripe/android/view/CardNumberEditText;Z)Z

    .line 198
    iget-object v3, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v3, v0}, Lcom/stripe/android/view/CardNumberEditText;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0, v2}, Lcom/stripe/android/view/CardNumberEditText;->setSelection(I)V

    .line 200
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText$1;->this$0:Lcom/stripe/android/view/CardNumberEditText;

    invoke-static {v0, v1}, Lcom/stripe/android/view/CardNumberEditText;->access$002(Lcom/stripe/android/view/CardNumberEditText;Z)Z

    goto :goto_0

    .line 185
    :cond_4
    if-eqz v0, :cond_5

    .line 186
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 188
    :cond_5
    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
