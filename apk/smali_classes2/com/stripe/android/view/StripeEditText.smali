.class public Lcom/stripe/android/view/StripeEditText;
.super Landroid/support/v7/widget/n;
.source "StripeEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/view/StripeEditText$SoftDeleteInputConnection;,
        Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;,
        Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;
    }
.end annotation


# instance fields
.field private mAfterTextChangedListener:Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;

.field private mCachedColorStateList:Landroid/content/res/ColorStateList;

.field private mDefaultErrorColorResId:I

.field private mDeleteEmptyListener:Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;

.field private mErrorColor:I

.field private mShouldShowError:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/support/v7/widget/n;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-direct {p0}, Lcom/stripe/android/view/StripeEditText;->initView()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/n;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-direct {p0}, Lcom/stripe/android/view/StripeEditText;->initView()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/n;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-direct {p0}, Lcom/stripe/android/view/StripeEditText;->initView()V

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/stripe/android/view/StripeEditText;)Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/stripe/android/view/StripeEditText;->mAfterTextChangedListener:Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/stripe/android/view/StripeEditText;)Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/stripe/android/view/StripeEditText;->mDeleteEmptyListener:Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;

    return-object v0
.end method

.method private determineDefaultErrorColor()V
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/stripe/android/view/StripeEditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/stripe/android/view/StripeEditText;->mCachedColorStateList:Landroid/content/res/ColorStateList;

    .line 176
    iget-object v0, p0, Lcom/stripe/android/view/StripeEditText;->mCachedColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    .line 177
    invoke-static {v0}, Lcom/stripe/android/view/StripeEditText;->isColorDark(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    sget v0, Lcom/stripe/android/R$color;->error_text_light_theme:I

    iput v0, p0, Lcom/stripe/android/view/StripeEditText;->mDefaultErrorColorResId:I

    .line 184
    :goto_0
    return-void

    .line 182
    :cond_0
    sget v0, Lcom/stripe/android/R$color;->error_text_dark_theme:I

    iput v0, p0, Lcom/stripe/android/view/StripeEditText;->mDefaultErrorColorResId:I

    goto :goto_0
.end method

.method private initView()V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/stripe/android/view/StripeEditText;->listenForTextChanges()V

    .line 169
    invoke-direct {p0}, Lcom/stripe/android/view/StripeEditText;->listenForDeleteEmpty()V

    .line 170
    invoke-direct {p0}, Lcom/stripe/android/view/StripeEditText;->determineDefaultErrorColor()V

    .line 171
    invoke-virtual {p0}, Lcom/stripe/android/view/StripeEditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/stripe/android/view/StripeEditText;->mCachedColorStateList:Landroid/content/res/ColorStateList;

    .line 172
    return-void
.end method

.method static isColorDark(I)Z
    .locals 6

    .prologue
    .line 154
    const-wide v0, 0x3fd322d0e5604189L    # 0.299

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide v2, 0x3fe2c8b439581062L    # 0.587

    .line 155
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide v2, 0x3fbd2f1a9fbe76c9L    # 0.114

    .line 156
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 159
    const-wide v2, 0x406fe00000000000L    # 255.0

    div-double/2addr v0, v2

    .line 160
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 161
    const/4 v0, 0x0

    .line 163
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private listenForDeleteEmpty()V
    .locals 1

    .prologue
    .line 209
    new-instance v0, Lcom/stripe/android/view/StripeEditText$2;

    invoke-direct {v0, p0}, Lcom/stripe/android/view/StripeEditText$2;-><init>(Lcom/stripe/android/view/StripeEditText;)V

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/StripeEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 221
    return-void
.end method

.method private listenForTextChanges()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/stripe/android/view/StripeEditText$1;

    invoke-direct {v0, p0}, Lcom/stripe/android/view/StripeEditText$1;-><init>(Lcom/stripe/android/view/StripeEditText;)V

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/StripeEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 205
    return-void
.end method


# virtual methods
.method public getCachedColorStateList()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/stripe/android/view/StripeEditText;->mCachedColorStateList:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public getDefaultErrorColorInt()I
    .locals 3

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/stripe/android/view/StripeEditText;->determineDefaultErrorColor()V

    .line 119
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/stripe/android/view/StripeEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/stripe/android/view/StripeEditText;->mDefaultErrorColorResId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    .line 127
    :goto_0
    return v0

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/stripe/android/view/StripeEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/stripe/android/view/StripeEditText;->mDefaultErrorColorResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public getShouldShowError()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/stripe/android/view/StripeEditText;->mShouldShowError:Z

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lcom/stripe/android/view/StripeEditText$SoftDeleteInputConnection;

    invoke-super {p0, p1}, Landroid/support/v7/widget/n;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/stripe/android/view/StripeEditText$SoftDeleteInputConnection;-><init>(Lcom/stripe/android/view/StripeEditText;Landroid/view/inputmethod/InputConnection;Z)V

    return-object v0
.end method

.method public setAfterTextChangedListener(Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/stripe/android/view/StripeEditText;->mAfterTextChangedListener:Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;

    .line 66
    return-void
.end method

.method public setDeleteEmptyListener(Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/stripe/android/view/StripeEditText;->mDeleteEmptyListener:Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;

    .line 75
    return-void
.end method

.method public setErrorColor(I)V
    .locals 0

    .prologue
    .line 136
    iput p1, p0, Lcom/stripe/android/view/StripeEditText;->mErrorColor:I

    .line 137
    return-void
.end method

.method public setShouldShowError(Z)V
    .locals 1

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/stripe/android/view/StripeEditText;->mShouldShowError:Z

    .line 86
    iget-boolean v0, p0, Lcom/stripe/android/view/StripeEditText;->mShouldShowError:Z

    if-eqz v0, :cond_0

    .line 87
    iget v0, p0, Lcom/stripe/android/view/StripeEditText;->mErrorColor:I

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/StripeEditText;->setTextColor(I)V

    .line 92
    :goto_0
    invoke-virtual {p0}, Lcom/stripe/android/view/StripeEditText;->refreshDrawableState()V

    .line 93
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/StripeEditText;->mCachedColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/StripeEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method
