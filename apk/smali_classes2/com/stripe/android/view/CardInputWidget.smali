.class public Lcom/stripe/android/view/CardInputWidget;
.super Landroid/widget/LinearLayout;
.source "CardInputWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/view/CardInputWidget$AnimationEndListener;,
        Lcom/stripe/android/view/CardInputWidget$PlacementParameters;,
        Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;,
        Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;,
        Lcom/stripe/android/view/CardInputWidget$CardInputListener;,
        Lcom/stripe/android/view/CardInputWidget$FocusField;
    }
.end annotation


# static fields
.field private static final ANIMATION_LENGTH:J = 0x96L

.field public static final BRAND_RESOURCE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final CVC_PLACEHOLDER_AMEX:Ljava/lang/String; = "2345"

.field private static final CVC_PLACEHOLDER_COMMON:Ljava/lang/String; = "CVC"

.field private static final DEFAULT_READER_ID:I = 0x28757b2

.field private static final EXTRA_CARD_VIEWED:Ljava/lang/String; = "extra_card_viewed"

.field private static final EXTRA_SUPER_STATE:Ljava/lang/String; = "extra_super_state"

.field public static final FOCUS_CARD:Ljava/lang/String; = "focus_card"

.field public static final FOCUS_CVC:Ljava/lang/String; = "focus_cvc"

.field public static final FOCUS_EXPIRY:Ljava/lang/String; = "focus_expiry"

.field private static final FULL_SIZING_CARD_TEXT:Ljava/lang/String; = "4242 4242 4242 4242"

.field private static final FULL_SIZING_DATE_TEXT:Ljava/lang/String; = "MM/YY"

.field private static final HIDDEN_TEXT_AMEX:Ljava/lang/String; = "3434 343434 "

.field private static final HIDDEN_TEXT_COMMON:Ljava/lang/String; = "4242 4242 4242 "

.field private static final PEEK_TEXT_AMEX:Ljava/lang/String; = "34343"

.field private static final PEEK_TEXT_COMMON:Ljava/lang/String; = "4242"

.field private static final PEEK_TEXT_DINERS:Ljava/lang/String; = "88"


# instance fields
.field private mCardHintText:Ljava/lang/String;

.field private mCardIconImageView:Landroid/widget/ImageView;

.field private mCardInputListener:Lcom/stripe/android/view/CardInputWidget$CardInputListener;

.field private mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

.field private mCardNumberIsViewed:Z

.field private mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

.field private mDimensionOverrides:Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;

.field private mErrorColorInt:I

.field private mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

.field private mFrameLayout:Landroid/widget/FrameLayout;

.field private mInitFlag:Z

.field private mIsAmEx:Z

.field private mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

.field private mTintColorInt:I

.field private mTotalLengthInPixels:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/stripe/android/view/CardInputWidget$1;

    invoke-direct {v0}, Lcom/stripe/android/view/CardInputWidget$1;-><init>()V

    sput-object v0, Lcom/stripe/android/view/CardInputWidget;->BRAND_RESOURCE_MAP:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->initView(Landroid/util/AttributeSet;)V

    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    .line 124
    invoke-direct {p0, p2}, Lcom/stripe/android/view/CardInputWidget;->initView(Landroid/util/AttributeSet;)V

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    .line 129
    invoke-direct {p0, p2}, Lcom/stripe/android/view/CardInputWidget;->initView(Landroid/util/AttributeSet;)V

    .line 130
    return-void
.end method

.method static synthetic access$000(Lcom/stripe/android/view/CardInputWidget;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/stripe/android/view/CardInputWidget;->scrollLeft()V

    return-void
.end method

.method static synthetic access$100(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardInputWidget$CardInputListener;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardInputListener:Lcom/stripe/android/view/CardInputWidget$CardInputListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/ExpiryDateEditText;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardInputWidget$PlacementParameters;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    return-object v0
.end method

.method static synthetic access$200(Lcom/stripe/android/view/CardInputWidget;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/stripe/android/view/CardInputWidget;->scrollRight()V

    return-void
.end method

.method static synthetic access$300(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardNumberEditText;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    return-object v0
.end method

.method static synthetic access$400(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/StripeEditText;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    return-object v0
.end method

.method static synthetic access$500(Lcom/stripe/android/view/CardInputWidget;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/stripe/android/view/CardInputWidget;->updateIconCvc(Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/stripe/android/view/CardInputWidget;->isCvcMaximalLength(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$702(Lcom/stripe/android/view/CardInputWidget;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/stripe/android/view/CardInputWidget;->mIsAmEx:Z

    return p1
.end method

.method static synthetic access$800(Lcom/stripe/android/view/CardInputWidget;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/stripe/android/view/CardInputWidget;->updateIcon(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/stripe/android/view/CardInputWidget;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/stripe/android/view/CardInputWidget;->updateCvc(Ljava/lang/String;)V

    return-void
.end method

.method private applyTint(Z)V
    .locals 3

    .prologue
    .line 834
    if-nez p1, :cond_0

    const-string v0, "Unknown"

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v1}, Lcom/stripe/android/view/CardNumberEditText;->getCardBrand()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 835
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 836
    invoke-static {v0}, Landroid/support/v4/a/a/a;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 837
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget v2, p0, Lcom/stripe/android/view/CardInputWidget;->mTintColorInt:I

    invoke-static {v1, v2}, Landroid/support/v4/a/a/a;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 838
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mCardIconImageView:Landroid/widget/ImageView;

    invoke-static {v0}, Landroid/support/v4/a/a/a;->h(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 840
    :cond_1
    return-void
.end method

.method private getCvcPlaceHolderForBrand(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 815
    const-string v0, "American Express"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816
    const-string v0, "2345"

    .line 818
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "CVC"

    goto :goto_0
.end method

.method private getDesiredWidthInPixels(Ljava/lang/String;Lcom/stripe/android/view/StripeEditText;)I
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mDimensionOverrides:Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;

    if-nez v0, :cond_0

    .line 420
    invoke-virtual {p2}, Lcom/stripe/android/view/StripeEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    float-to-int v0, v0

    .line 419
    :goto_0
    return v0

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mDimensionOverrides:Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;

    .line 421
    invoke-interface {v0, p1, p2}, Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;->getPixelWidth(Ljava/lang/String;Landroid/widget/EditText;)I

    move-result v0

    goto :goto_0
.end method

.method private getFrameWidth()I
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mDimensionOverrides:Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mFrameLayout:Landroid/widget/FrameLayout;

    .line 426
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v0

    .line 425
    :goto_0
    return v0

    .line 426
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mDimensionOverrides:Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;

    .line 427
    invoke-interface {v0}, Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;->getFrameWidth()I

    move-result v0

    goto :goto_0
.end method

.method private getHiddenTextForBrand(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 806
    const-string v0, "American Express"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 807
    const-string v0, "3434 343434 "

    .line 809
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "4242 4242 4242 "

    goto :goto_0
.end method

.method private getPeekCardTextForBrand(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 824
    const-string v0, "American Express"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825
    const-string v0, "34343"

    .line 829
    :goto_0
    return-object v0

    .line 826
    :cond_0
    const-string v0, "Diners Club"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 827
    const-string v0, "88"

    goto :goto_0

    .line 829
    :cond_1
    const-string v0, "4242"

    goto :goto_0
.end method

.method private initView(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 431
    invoke-virtual {p0}, Lcom/stripe/android/view/CardInputWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/stripe/android/R$layout;->card_input_widget:I

    invoke-static {v0, v1, p0}, Lcom/stripe/android/view/CardInputWidget;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 435
    invoke-virtual {p0}, Lcom/stripe/android/view/CardInputWidget;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 436
    const v0, 0x28757b2

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->setId(I)V

    .line 439
    :cond_0
    invoke-virtual {p0, v2}, Lcom/stripe/android/view/CardInputWidget;->setOrientation(I)V

    .line 440
    invoke-virtual {p0}, Lcom/stripe/android/view/CardInputWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/stripe/android/R$dimen;->card_widget_min_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->setMinimumWidth(I)V

    .line 441
    new-instance v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    invoke-direct {v0, p0}, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    iput-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    .line 442
    sget v0, Lcom/stripe/android/R$id;->iv_card_icon:I

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardIconImageView:Landroid/widget/ImageView;

    .line 443
    sget v0, Lcom/stripe/android/R$id;->et_card_number:I

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/view/CardNumberEditText;

    iput-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    .line 444
    sget v0, Lcom/stripe/android/R$id;->et_expiry_date:I

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/view/ExpiryDateEditText;

    iput-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    .line 445
    sget v0, Lcom/stripe/android/R$id;->et_cvc_number:I

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/stripe/android/view/StripeEditText;

    iput-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    .line 447
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    .line 449
    sget v0, Lcom/stripe/android/R$id;->frame_container:I

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mFrameLayout:Landroid/widget/FrameLayout;

    .line 450
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->getDefaultErrorColorInt()I

    move-result v0

    iput v0, p0, Lcom/stripe/android/view/CardInputWidget;->mErrorColorInt:I

    .line 451
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iput v0, p0, Lcom/stripe/android/view/CardInputWidget;->mTintColorInt:I

    .line 452
    if-eqz p1, :cond_1

    .line 453
    invoke-virtual {p0}, Lcom/stripe/android/view/CardInputWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/stripe/android/R$styleable;->CardInputView:[I

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 459
    :try_start_0
    sget v0, Lcom/stripe/android/R$styleable;->CardInputView_cardTextErrorColor:I

    iget v2, p0, Lcom/stripe/android/view/CardInputWidget;->mErrorColorInt:I

    .line 460
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/stripe/android/view/CardInputWidget;->mErrorColorInt:I

    .line 461
    sget v0, Lcom/stripe/android/R$styleable;->CardInputView_cardTint:I

    iget v2, p0, Lcom/stripe/android/view/CardInputWidget;->mTintColorInt:I

    .line 462
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/stripe/android/view/CardInputWidget;->mTintColorInt:I

    .line 463
    sget v0, Lcom/stripe/android/R$styleable;->CardInputView_cardHintText:I

    .line 464
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardHintText:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardHintText:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 471
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mCardHintText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/CardNumberEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 473
    :cond_2
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    iget v1, p0, Lcom/stripe/android/view/CardInputWidget;->mErrorColorInt:I

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/CardNumberEditText;->setErrorColor(I)V

    .line 474
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    iget v1, p0, Lcom/stripe/android/view/CardInputWidget;->mErrorColorInt:I

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/ExpiryDateEditText;->setErrorColor(I)V

    .line 475
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    iget v1, p0, Lcom/stripe/android/view/CardInputWidget;->mErrorColorInt:I

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setErrorColor(I)V

    .line 477
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$2;

    invoke-direct {v1, p0}, Lcom/stripe/android/view/CardInputWidget$2;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/CardNumberEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 489
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$3;

    invoke-direct {v1, p0}, Lcom/stripe/android/view/CardInputWidget$3;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/ExpiryDateEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 501
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-direct {v1, p0, v2}, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;-><init>(Lcom/stripe/android/view/CardInputWidget;Lcom/stripe/android/view/StripeEditText;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/ExpiryDateEditText;->setDeleteEmptyListener(Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;)V

    .line 504
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-direct {v1, p0, v2}, Lcom/stripe/android/view/CardInputWidget$BackUpFieldDeleteListener;-><init>(Lcom/stripe/android/view/CardInputWidget;Lcom/stripe/android/view/StripeEditText;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setDeleteEmptyListener(Lcom/stripe/android/view/StripeEditText$DeleteEmptyListener;)V

    .line 507
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$4;

    invoke-direct {v1, p0}, Lcom/stripe/android/view/CardInputWidget$4;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 523
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$5;

    invoke-direct {v1, p0}, Lcom/stripe/android/view/CardInputWidget$5;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setAfterTextChangedListener(Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;)V

    .line 537
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$6;

    invoke-direct {v1, p0}, Lcom/stripe/android/view/CardInputWidget$6;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/CardNumberEditText;->setCardNumberCompleteListener(Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;)V

    .line 548
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$7;

    invoke-direct {v1, p0}, Lcom/stripe/android/view/CardInputWidget$7;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/CardNumberEditText;->setCardBrandChangeListener(Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;)V

    .line 558
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    new-instance v1, Lcom/stripe/android/view/CardInputWidget$8;

    invoke-direct {v1, p0}, Lcom/stripe/android/view/CardInputWidget$8;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/ExpiryDateEditText;->setExpiryDateEditListener(Lcom/stripe/android/view/ExpiryDateEditText$ExpiryDateEditListener;)V

    .line 568
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->requestFocus()Z

    .line 569
    return-void

    .line 466
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private static isCvcMaximalLength(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 793
    if-nez p1, :cond_1

    move v0, v1

    .line 800
    :cond_0
    :goto_0
    return v0

    .line 797
    :cond_1
    const-string v2, "American Express"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 798
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 800
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private scrollLeft()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x96

    const/4 v5, 0x1

    .line 572
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mInitFlag:Z

    if-nez v0, :cond_1

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v1, v0

    .line 578
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    add-int/2addr v0, v1

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateCvcSeparation:I

    add-int/2addr v2, v0

    .line 582
    invoke-virtual {p0, v5}, Lcom/stripe/android/view/CardInputWidget;->updateSpaceSizes(Z)V

    .line 584
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    .line 585
    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 586
    new-instance v3, Lcom/stripe/android/view/CardInputWidget$9;

    invoke-direct {v3, p0, v0}, Lcom/stripe/android/view/CardInputWidget$9;-><init>(Lcom/stripe/android/view/CardInputWidget;I)V

    .line 597
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v4, v4, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v0, v4

    .line 599
    new-instance v4, Lcom/stripe/android/view/CardInputWidget$10;

    invoke-direct {v4, p0, v0, v1}, Lcom/stripe/android/view/CardInputWidget$10;-><init>(Lcom/stripe/android/view/CardInputWidget;II)V

    .line 613
    sub-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 614
    new-instance v1, Lcom/stripe/android/view/CardInputWidget$11;

    invoke-direct {v1, p0, v0, v2}, Lcom/stripe/android/view/CardInputWidget$11;-><init>(Lcom/stripe/android/view/CardInputWidget;II)V

    .line 630
    new-instance v0, Lcom/stripe/android/view/CardInputWidget$12;

    invoke-direct {v0, p0}, Lcom/stripe/android/view/CardInputWidget$12;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 637
    invoke-virtual {v3, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 638
    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 639
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 641
    new-instance v0, Landroid/view/animation/AnimationSet;

    invoke-direct {v0, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 642
    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 643
    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 644
    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 645
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 646
    iput-boolean v5, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    goto :goto_0
.end method

.method private scrollRight()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide/16 v6, 0x96

    .line 650
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mInitFlag:Z

    if-nez v0, :cond_1

    .line 730
    :cond_0
    :goto_0
    return-void

    .line 654
    :cond_1
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v0, v1

    .line 657
    invoke-virtual {p0, v8}, Lcom/stripe/android/view/CardInputWidget;->updateSpaceSizes(Z)V

    .line 659
    new-instance v1, Lcom/stripe/android/view/CardInputWidget$13;

    invoke-direct {v1, p0}, Lcom/stripe/android/view/CardInputWidget$13;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    .line 671
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v2, v3

    .line 675
    new-instance v3, Lcom/stripe/android/view/CardInputWidget$14;

    invoke-direct {v3, p0, v2, v0}, Lcom/stripe/android/view/CardInputWidget$14;-><init>(Lcom/stripe/android/view/CardInputWidget;II)V

    .line 689
    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v4, v4, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    iget-object v5, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v5, v5, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v5, v5, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v5, v5, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateCvcSeparation:I

    add-int/2addr v4, v5

    .line 694
    sub-int/2addr v0, v2

    add-int/2addr v0, v4

    .line 696
    new-instance v2, Lcom/stripe/android/view/CardInputWidget$15;

    invoke-direct {v2, p0, v4, v0}, Lcom/stripe/android/view/CardInputWidget$15;-><init>(Lcom/stripe/android/view/CardInputWidget;II)V

    .line 712
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 713
    invoke-virtual {v3, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 714
    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 716
    new-instance v0, Lcom/stripe/android/view/CardInputWidget$16;

    invoke-direct {v0, p0}, Lcom/stripe/android/view/CardInputWidget$16;-><init>(Lcom/stripe/android/view/CardInputWidget;)V

    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 723
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x1

    invoke-direct {v0, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 724
    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 725
    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 726
    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 728
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 729
    iput-boolean v8, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    goto :goto_0
.end method

.method private setLayoutValues(IILcom/stripe/android/view/StripeEditText;)V
    .locals 1

    .prologue
    .line 411
    .line 412
    invoke-virtual {p3}, Lcom/stripe/android/view/StripeEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 413
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 414
    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 415
    invoke-virtual {p3, v0}, Lcom/stripe/android/view/StripeEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 416
    return-void
.end method

.method static shouldIconShowBrand(Ljava/lang/String;ZLjava/lang/String;)Z
    .locals 1

    .prologue
    .line 755
    if-nez p1, :cond_0

    .line 756
    const/4 v0, 0x1

    .line 758
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p2}, Lcom/stripe/android/view/CardInputWidget;->isCvcMaximalLength(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private updateCvc(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 843
    const-string v0, "American Express"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    new-array v1, v1, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 846
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    sget v1, Lcom/stripe/android/R$string;->cvc_amex_hint:I

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setHint(I)V

    .line 852
    :goto_0
    return-void

    .line 848
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    new-array v1, v1, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 850
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    sget v1, Lcom/stripe/android/R$string;->cvc_number_hint:I

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setHint(I)V

    goto :goto_0
.end method

.method private updateIcon(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 855
    const-string v0, "Unknown"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    invoke-virtual {p0}, Lcom/stripe/android/view/CardInputWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/stripe/android/R$drawable;->ic_unknown:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 857
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mCardIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 858
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->applyTint(Z)V

    .line 862
    :goto_0
    return-void

    .line 860
    :cond_0
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mCardIconImageView:Landroid/widget/ImageView;

    sget-object v0, Lcom/stripe/android/view/CardInputWidget;->BRAND_RESOURCE_MAP:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private updateIconCvc(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 868
    invoke-static {p1, p2, p3}, Lcom/stripe/android/view/CardInputWidget;->shouldIconShowBrand(Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 869
    invoke-direct {p0, p1}, Lcom/stripe/android/view/CardInputWidget;->updateIcon(Ljava/lang/String;)V

    .line 873
    :goto_0
    return-void

    .line 871
    :cond_0
    const-string v0, "American Express"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->updateIconForCvcEntry(Z)V

    goto :goto_0
.end method

.method private updateIconForCvcEntry(Z)V
    .locals 2

    .prologue
    .line 876
    if-eqz p1, :cond_0

    .line 877
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardIconImageView:Landroid/widget/ImageView;

    sget v1, Lcom/stripe/android/R$drawable;->ic_cvc_amex:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 881
    :goto_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->applyTint(Z)V

    .line 882
    return-void

    .line 879
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardIconImageView:Landroid/widget/ImageView;

    sget v1, Lcom/stripe/android/R$drawable;->ic_cvc:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    .line 210
    invoke-virtual {v0}, Lcom/stripe/android/view/ExpiryDateEditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    .line 211
    invoke-virtual {v0}, Lcom/stripe/android/view/StripeEditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/stripe/android/view/CardInputWidget;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->requestFocus()Z

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/StripeEditText;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/ExpiryDateEditText;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/CardNumberEditText;->setText(Ljava/lang/CharSequence;)V

    .line 218
    return-void
.end method

.method public getCard()Lcom/stripe/android/model/Card;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->getCardNumber()Ljava/lang/String;

    move-result-object v2

    .line 142
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/ExpiryDateEditText;->getValidDateFields()[I

    move-result-object v3

    .line 143
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    array-length v0, v3

    const/4 v4, 0x2

    if-eq v0, v4, :cond_1

    :cond_0
    move-object v0, v1

    .line 154
    :goto_0
    return-object v0

    .line 148
    :cond_1
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mIsAmEx:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    .line 149
    :goto_1
    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    invoke-virtual {v4}, Lcom/stripe/android/view/StripeEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 150
    invoke-static {v4}, Lcom/stripe/android/util/StripeTextUtils;->isBlank(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v5, v0, :cond_4

    :cond_2
    move-object v0, v1

    .line 151
    goto :goto_0

    .line 148
    :cond_3
    const/4 v0, 0x3

    goto :goto_1

    .line 154
    :cond_4
    new-instance v0, Lcom/stripe/android/model/Card;

    const/4 v1, 0x0

    aget v1, v3, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v5, 0x1

    aget v3, v3, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/stripe/android/model/Card;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    const-string v1, "CardInputView"

    .line 155
    invoke-virtual {v0, v1}, Lcom/stripe/android/model/Card;->addLoggingToken(Ljava/lang/String;)Lcom/stripe/android/model/Card;

    move-result-object v0

    goto :goto_0
.end method

.method getFocusRequestOnTouch(I)Lcom/stripe/android/view/StripeEditText;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 290
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v1

    .line 292
    iget-boolean v2, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    if-eqz v2, :cond_3

    .line 296
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    add-int/2addr v1, v2

    if-ge p1, v1, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-object v0

    .line 299
    :cond_1
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardTouchBufferLimit:I

    if-ge p1, v1, :cond_2

    .line 301
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    goto :goto_0

    .line 302
    :cond_2
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateStartPosition:I

    if-ge p1, v1, :cond_0

    .line 304
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    goto :goto_0

    .line 312
    :cond_3
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    add-int/2addr v1, v2

    if-lt p1, v1, :cond_0

    .line 315
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardTouchBufferLimit:I

    if-ge p1, v1, :cond_4

    .line 317
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    goto :goto_0

    .line 318
    :cond_4
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateStartPosition:I

    if-ge p1, v1, :cond_5

    .line 320
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    goto :goto_0

    .line 321
    :cond_5
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateStartPosition:I

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    add-int/2addr v1, v2

    if-lt p1, v1, :cond_0

    .line 324
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateRightTouchBufferLimit:I

    if-ge p1, v1, :cond_6

    .line 326
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    goto :goto_0

    .line 327
    :cond_6
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cvcStartPosition:I

    if-ge p1, v1, :cond_0

    .line 329
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    goto :goto_0
.end method

.method getPlacementParameters()Lcom/stripe/android/view/CardInputWidget$PlacementParameters;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 222
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 231
    :goto_0
    return v0

    .line 226
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->getFocusRequestOnTouch(I)Lcom/stripe/android/view/StripeEditText;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_1

    .line 228
    invoke-virtual {v0}, Lcom/stripe/android/view/StripeEditText;->requestFocus()Z

    .line 229
    const/4 v0, 0x1

    goto :goto_0

    .line 231
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 763
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 764
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mInitFlag:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/stripe/android/view/CardInputWidget;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    .line 765
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mInitFlag:Z

    .line 766
    invoke-direct {p0}, Lcom/stripe/android/view/CardInputWidget;->getFrameWidth()I

    move-result v0

    iput v0, p0, Lcom/stripe/android/view/CardInputWidget;->mTotalLengthInPixels:I

    .line 768
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->updateSpaceSizes(Z)V

    .line 770
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 772
    :goto_0
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-direct {p0, v1, v0, v2}, Lcom/stripe/android/view/CardInputWidget;->setLayoutValues(IILcom/stripe/android/view/StripeEditText;)V

    .line 775
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v0, v1

    .line 778
    :goto_1
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-direct {p0, v1, v0, v2}, Lcom/stripe/android/view/CardInputWidget;->setLayoutValues(IILcom/stripe/android/view/StripeEditText;)V

    .line 780
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/stripe/android/view/CardInputWidget;->mTotalLengthInPixels:I

    .line 786
    :goto_2
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cvcWidth:I

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    invoke-direct {p0, v1, v0, v2}, Lcom/stripe/android/view/CardInputWidget;->setLayoutValues(IILcom/stripe/android/view/StripeEditText;)V

    .line 788
    :cond_0
    return-void

    .line 770
    :cond_1
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->hiddenCardWidth:I

    mul-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 775
    :cond_2
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 780
    :cond_3
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateCvcSeparation:I

    add-int/2addr v0, v1

    goto :goto_2
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5

    .prologue
    .line 244
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 245
    check-cast p1, Landroid/os/Bundle;

    .line 246
    const-string v0, "extra_card_viewed"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    .line 247
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->updateSpaceSizes(Z)V

    .line 248
    invoke-direct {p0}, Lcom/stripe/android/view/CardInputWidget;->getFrameWidth()I

    move-result v0

    iput v0, p0, Lcom/stripe/android/view/CardInputWidget;->mTotalLengthInPixels:I

    .line 250
    iget-boolean v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    if-eqz v0, :cond_0

    .line 251
    const/4 v2, 0x0

    .line 252
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v1, v0

    .line 254
    iget v0, p0, Lcom/stripe/android/view/CardInputWidget;->mTotalLengthInPixels:I

    .line 264
    :goto_0
    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-direct {p0, v3, v2, v4}, Lcom/stripe/android/view/CardInputWidget;->setLayoutValues(IILcom/stripe/android/view/StripeEditText;)V

    .line 265
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-direct {p0, v2, v1, v3}, Lcom/stripe/android/view/CardInputWidget;->setLayoutValues(IILcom/stripe/android/view/StripeEditText;)V

    .line 266
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cvcWidth:I

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    invoke-direct {p0, v1, v0, v2}, Lcom/stripe/android/view/CardInputWidget;->setLayoutValues(IILcom/stripe/android/view/StripeEditText;)V

    .line 268
    const-string v0, "extra_super_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 272
    :goto_1
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->hiddenCardWidth:I

    mul-int/lit8 v2, v0, -0x1

    .line 257
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v1, v0

    .line 259
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v0, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    add-int/2addr v0, v1

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateCvcSeparation:I

    add-int/2addr v0, v3

    goto :goto_0

    .line 270
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 236
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 237
    const-string v1, "extra_super_state"

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 238
    const-string v1, "extra_card_viewed"

    iget-boolean v2, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 239
    return-object v0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 734
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    .line 735
    if-eqz p1, :cond_0

    .line 736
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->applyTint(Z)V

    .line 738
    :cond_0
    return-void
.end method

.method public setCardInputListener(Lcom/stripe/android/view/CardInputWidget$CardInputListener;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/stripe/android/view/CardInputWidget;->mCardInputListener:Lcom/stripe/android/view/CardInputWidget$CardInputListener;

    .line 165
    return-void
.end method

.method public setCardNumber(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0, p1}, Lcom/stripe/android/view/CardNumberEditText;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->isCardNumberValid()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardInputWidget;->setCardNumberIsViewed(Z)V

    .line 175
    return-void

    .line 174
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setCardNumberIsViewed(Z)V
    .locals 0

    .prologue
    .line 343
    iput-boolean p1, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberIsViewed:Z

    .line 344
    return-void
.end method

.method public setCvcCode(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    invoke-virtual {v0, p1}, Lcom/stripe/android/view/StripeEditText;->setText(Ljava/lang/CharSequence;)V

    .line 203
    return-void
.end method

.method setDimensionOverrideSettings(Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/stripe/android/view/CardInputWidget;->mDimensionOverrides:Lcom/stripe/android/view/CardInputWidget$DimensionOverrideSettings;

    .line 339
    return-void
.end method

.method public setExpiryDate(II)V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    invoke-static {p1, p2}, Lcom/stripe/android/util/DateUtils;->createDateStringFromIntegerInput(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stripe/android/view/ExpiryDateEditText;->setText(Ljava/lang/CharSequence;)V

    .line 193
    return-void
.end method

.method updateSpaceSizes(Z)V
    .locals 6

    .prologue
    .line 354
    invoke-direct {p0}, Lcom/stripe/android/view/CardInputWidget;->getFrameWidth()I

    move-result v0

    .line 355
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v1

    .line 356
    if-nez v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 361
    :cond_0
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    const-string v3, "4242 4242 4242 4242"

    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    .line 362
    invoke-direct {p0, v3, v4}, Lcom/stripe/android/view/CardInputWidget;->getDesiredWidthInPixels(Ljava/lang/String;Lcom/stripe/android/view/StripeEditText;)I

    move-result v3

    iput v3, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    .line 364
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    const-string v3, "MM/YY"

    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mExpiryDateEditText:Lcom/stripe/android/view/ExpiryDateEditText;

    .line 365
    invoke-direct {p0, v3, v4}, Lcom/stripe/android/view/CardInputWidget;->getDesiredWidthInPixels(Ljava/lang/String;Lcom/stripe/android/view/StripeEditText;)I

    move-result v3

    iput v3, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    .line 367
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-virtual {v2}, Lcom/stripe/android/view/CardNumberEditText;->getCardBrand()Ljava/lang/String;

    move-result-object v2

    .line 368
    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    .line 369
    invoke-direct {p0, v2}, Lcom/stripe/android/view/CardInputWidget;->getHiddenTextForBrand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-direct {p0, v4, v5}, Lcom/stripe/android/view/CardInputWidget;->getDesiredWidthInPixels(Ljava/lang/String;Lcom/stripe/android/view/StripeEditText;)I

    move-result v4

    iput v4, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->hiddenCardWidth:I

    .line 371
    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    .line 372
    invoke-direct {p0, v2}, Lcom/stripe/android/view/CardInputWidget;->getCvcPlaceHolderForBrand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/stripe/android/view/CardInputWidget;->mCvcNumberEditText:Lcom/stripe/android/view/StripeEditText;

    invoke-direct {p0, v4, v5}, Lcom/stripe/android/view/CardInputWidget;->getDesiredWidthInPixels(Ljava/lang/String;Lcom/stripe/android/view/StripeEditText;)I

    move-result v4

    iput v4, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cvcWidth:I

    .line 374
    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    .line 375
    invoke-direct {p0, v2}, Lcom/stripe/android/view/CardInputWidget;->getPeekCardTextForBrand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mCardNumberEditText:Lcom/stripe/android/view/CardNumberEditText;

    invoke-direct {p0, v2, v4}, Lcom/stripe/android/view/CardInputWidget;->getDesiredWidthInPixels(Ljava/lang/String;Lcom/stripe/android/view/StripeEditText;)I

    move-result v2

    iput v2, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    .line 377
    if-eqz p1, :cond_1

    .line 378
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    sub-int/2addr v0, v3

    iput v0, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    .line 380
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    add-int/2addr v2, v1

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardTouchBufferLimit:I

    .line 382
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardWidth:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateStartPosition:I

    goto :goto_0

    .line 385
    :cond_1
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    div-int/lit8 v3, v0, 0x2

    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v4, v4, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v4, v4, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    .line 388
    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cvcWidth:I

    sub-int/2addr v0, v3

    iput v0, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateCvcSeparation:I

    .line 394
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    add-int/2addr v2, v1

    iget-object v3, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v3, v3, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardTouchBufferLimit:I

    .line 397
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->peekCardWidth:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cardDateSeparation:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateStartPosition:I

    .line 400
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateStartPosition:I

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateCvcSeparation:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateRightTouchBufferLimit:I

    .line 404
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateStartPosition:I

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateWidth:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget;->mPlacementParameters:Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    iget v2, v2, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->dateCvcSeparation:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cvcStartPosition:I

    goto/16 :goto_0
.end method
