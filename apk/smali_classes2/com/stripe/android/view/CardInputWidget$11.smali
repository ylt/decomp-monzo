.class Lcom/stripe/android/view/CardInputWidget$11;
.super Landroid/view/animation/Animation;
.source "CardInputWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/view/CardInputWidget;->scrollLeft()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stripe/android/view/CardInputWidget;

.field final synthetic val$cvcDestination:I

.field final synthetic val$cvcStartPosition:I


# direct methods
.method constructor <init>(Lcom/stripe/android/view/CardInputWidget;II)V
    .locals 0

    .prologue
    .line 614
    iput-object p1, p0, Lcom/stripe/android/view/CardInputWidget$11;->this$0:Lcom/stripe/android/view/CardInputWidget;

    iput p2, p0, Lcom/stripe/android/view/CardInputWidget$11;->val$cvcDestination:I

    iput p3, p0, Lcom/stripe/android/view/CardInputWidget$11;->val$cvcStartPosition:I

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 617
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 618
    iget v0, p0, Lcom/stripe/android/view/CardInputWidget$11;->val$cvcDestination:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    iget v2, p0, Lcom/stripe/android/view/CardInputWidget$11;->val$cvcStartPosition:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v1, v0

    .line 621
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$11;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 622
    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$400(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/StripeEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stripe/android/view/StripeEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 623
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 624
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 625
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$11;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v1}, Lcom/stripe/android/view/CardInputWidget;->access$1100(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardInputWidget$PlacementParameters;

    move-result-object v1

    iget v1, v1, Lcom/stripe/android/view/CardInputWidget$PlacementParameters;->cvcWidth:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 626
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$11;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v1}, Lcom/stripe/android/view/CardInputWidget;->access$400(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/StripeEditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/stripe/android/view/StripeEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 627
    return-void
.end method
