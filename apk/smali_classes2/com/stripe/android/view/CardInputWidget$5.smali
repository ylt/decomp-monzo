.class Lcom/stripe/android/view/CardInputWidget$5;
.super Ljava/lang/Object;
.source "CardInputWidget.java"

# interfaces
.implements Lcom/stripe/android/view/StripeEditText$AfterTextChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/view/CardInputWidget;->initView(Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stripe/android/view/CardInputWidget;


# direct methods
.method constructor <init>(Lcom/stripe/android/view/CardInputWidget;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/stripe/android/view/CardInputWidget$5;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 526
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$5;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$100(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardInputWidget$CardInputListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$5;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 527
    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$300(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardNumberEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stripe/android/view/CardNumberEditText;->getCardBrand()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/stripe/android/view/CardInputWidget;->access$600(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$5;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$100(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardInputWidget$CardInputListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/stripe/android/view/CardInputWidget$CardInputListener;->onCvcComplete()V

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$5;->this$0:Lcom/stripe/android/view/CardInputWidget;

    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$5;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 531
    invoke-static {v1}, Lcom/stripe/android/view/CardInputWidget;->access$300(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/CardNumberEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/stripe/android/view/CardNumberEditText;->getCardBrand()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/stripe/android/view/CardInputWidget$5;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 532
    invoke-static {v2}, Lcom/stripe/android/view/CardInputWidget;->access$400(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/StripeEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/stripe/android/view/StripeEditText;->hasFocus()Z

    move-result v2

    .line 530
    invoke-static {v0, v1, v2, p1}, Lcom/stripe/android/view/CardInputWidget;->access$500(Lcom/stripe/android/view/CardInputWidget;Ljava/lang/String;ZLjava/lang/String;)V

    .line 534
    return-void
.end method
