.class public Lcom/stripe/android/view/CardNumberEditText;
.super Lcom/stripe/android/view/StripeEditText;
.source "CardNumberEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;,
        Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;
    }
.end annotation


# static fields
.field private static final MAX_LENGTH_AMEX_DINERS:I = 0x11

.field private static final MAX_LENGTH_COMMON:I = 0x13

.field private static final SPACES_ARRAY_AMEX:[Ljava/lang/Integer;

.field private static final SPACES_ARRAY_COMMON:[Ljava/lang/Integer;

.field private static final SPACE_SET_AMEX:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final SPACE_SET_COMMON:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCardBrand:Ljava/lang/String;

.field private mCardBrandChangeListener:Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;

.field private mCardNumberCompleteListener:Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;

.field private mIgnoreChanges:Z

.field private mIsCardNumberValid:Z

.field private mLengthMax:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    sput-object v0, Lcom/stripe/android/view/CardNumberEditText;->SPACES_ARRAY_COMMON:[Ljava/lang/Integer;

    .line 32
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/stripe/android/view/CardNumberEditText;->SPACES_ARRAY_COMMON:[Ljava/lang/Integer;

    .line 33
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/stripe/android/view/CardNumberEditText;->SPACE_SET_COMMON:Ljava/util/Set;

    .line 35
    new-array v0, v4, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    sput-object v0, Lcom/stripe/android/view/CardNumberEditText;->SPACES_ARRAY_AMEX:[Ljava/lang/Integer;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/stripe/android/view/CardNumberEditText;->SPACES_ARRAY_AMEX:[Ljava/lang/Integer;

    .line 37
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/stripe/android/view/CardNumberEditText;->SPACE_SET_AMEX:Ljava/util/Set;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1}, Lcom/stripe/android/view/StripeEditText;-><init>(Landroid/content/Context;)V

    .line 39
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    .line 42
    const/16 v0, 0x13

    iput v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    .line 43
    iput-boolean v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mIgnoreChanges:Z

    .line 44
    iput-boolean v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mIsCardNumberValid:Z

    .line 48
    invoke-direct {p0}, Lcom/stripe/android/view/CardNumberEditText;->listenForTextChanges()V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/stripe/android/view/StripeEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    .line 42
    const/16 v0, 0x13

    iput v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    .line 43
    iput-boolean v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mIgnoreChanges:Z

    .line 44
    iput-boolean v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mIsCardNumberValid:Z

    .line 53
    invoke-direct {p0}, Lcom/stripe/android/view/CardNumberEditText;->listenForTextChanges()V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/stripe/android/view/StripeEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    .line 42
    const/16 v0, 0x13

    iput v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    .line 43
    iput-boolean v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mIgnoreChanges:Z

    .line 44
    iput-boolean v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mIsCardNumberValid:Z

    .line 58
    invoke-direct {p0}, Lcom/stripe/android/view/CardNumberEditText;->listenForTextChanges()V

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/stripe/android/view/CardNumberEditText;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mIgnoreChanges:Z

    return v0
.end method

.method static synthetic access$002(Lcom/stripe/android/view/CardNumberEditText;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/stripe/android/view/CardNumberEditText;->mIgnoreChanges:Z

    return p1
.end method

.method static synthetic access$100(Lcom/stripe/android/view/CardNumberEditText;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/stripe/android/view/CardNumberEditText;->updateCardBrandFromNumber(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/stripe/android/view/CardNumberEditText;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    return v0
.end method

.method static synthetic access$300(Lcom/stripe/android/view/CardNumberEditText;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mIsCardNumberValid:Z

    return v0
.end method

.method static synthetic access$302(Lcom/stripe/android/view/CardNumberEditText;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/stripe/android/view/CardNumberEditText;->mIsCardNumberValid:Z

    return p1
.end method

.method static synthetic access$400(Lcom/stripe/android/view/CardNumberEditText;)Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardNumberCompleteListener:Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;

    return-object v0
.end method

.method private static getLengthForBrand(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 247
    const-string v0, "American Express"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Diners Club"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    :cond_0
    const/16 v0, 0x11

    .line 250
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x13

    goto :goto_0
.end method

.method private listenForTextChanges()V
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lcom/stripe/android/view/CardNumberEditText$1;

    invoke-direct {v0, p0}, Lcom/stripe/android/view/CardNumberEditText$1;-><init>(Lcom/stripe/android/view/CardNumberEditText;)V

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 220
    return-void
.end method

.method private updateCardBrand(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 223
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iput-object p1, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    .line 229
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrandChangeListener:Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;

    if-eqz v0, :cond_2

    .line 230
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrandChangeListener:Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;

    iget-object v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;->onCardBrandChanged(Ljava/lang/String;)V

    .line 233
    :cond_2
    iget v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    .line 234
    iget-object v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    invoke-static {v1}, Lcom/stripe/android/view/CardNumberEditText;->getLengthForBrand(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    .line 235
    iget v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    if-eq v0, v1, :cond_0

    .line 239
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    iget v3, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/stripe/android/view/CardNumberEditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0
.end method

.method private updateCardBrandFromNumber(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 243
    invoke-static {p1}, Lcom/stripe/android/util/CardUtils;->getPossibleCardType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/stripe/android/view/CardNumberEditText;->updateCardBrand(Ljava/lang/String;)V

    .line 244
    return-void
.end method


# virtual methods
.method public getCardBrand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    return-object v0
.end method

.method public getCardNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mIsCardNumberValid:Z

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/stripe/android/view/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stripe/android/util/StripeTextUtils;->removeSpacesAndHyphens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLengthMax()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mLengthMax:I

    return v0
.end method

.method public isCardNumberValid()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mIsCardNumberValid:Z

    return v0
.end method

.method setCardBrandChangeListener(Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;)V
    .locals 2

    .prologue
    .line 99
    iput-object p1, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrandChangeListener:Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;

    .line 102
    iget-object v0, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrandChangeListener:Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;

    iget-object v1, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/stripe/android/view/CardNumberEditText$CardBrandChangeListener;->onCardBrandChanged(Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method setCardNumberCompleteListener(Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardNumberCompleteListener:Lcom/stripe/android/view/CardNumberEditText$CardNumberCompleteListener;

    .line 96
    return-void
.end method

.method updateSelectionIndex(III)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 119
    .line 120
    const-string v0, "American Express"

    iget-object v2, p0, Lcom/stripe/android/view/CardNumberEditText;->mCardBrand:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/stripe/android/view/CardNumberEditText;->SPACE_SET_AMEX:Ljava/util/Set;

    .line 124
    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 125
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-gt p2, v4, :cond_0

    add-int v4, p2, p3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 126
    add-int/lit8 v2, v2, 0x1

    .line 131
    :cond_0
    if-nez p3, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ne p2, v0, :cond_5

    .line 132
    const/4 v1, 0x1

    move v0, v1

    :goto_2
    move v1, v0

    .line 134
    goto :goto_1

    .line 120
    :cond_1
    sget-object v0, Lcom/stripe/android/view/CardNumberEditText;->SPACE_SET_COMMON:Ljava/util/Set;

    goto :goto_0

    .line 136
    :cond_2
    add-int v0, p2, p3

    add-int/2addr v0, v2

    .line 137
    if-eqz v1, :cond_3

    if-lez v0, :cond_3

    .line 138
    add-int/lit8 v0, v0, -0x1

    .line 141
    :cond_3
    if-gt v0, p1, :cond_4

    move p1, v0

    :cond_4
    return p1

    :cond_5
    move v0, v1

    goto :goto_2
.end method
