.class Lcom/stripe/android/view/CardInputWidget$10;
.super Landroid/view/animation/Animation;
.source "CardInputWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/view/CardInputWidget;->scrollLeft()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stripe/android/view/CardInputWidget;

.field final synthetic val$dateDestination:I

.field final synthetic val$dateStartPosition:I


# direct methods
.method constructor <init>(Lcom/stripe/android/view/CardInputWidget;II)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/stripe/android/view/CardInputWidget$10;->this$0:Lcom/stripe/android/view/CardInputWidget;

    iput p2, p0, Lcom/stripe/android/view/CardInputWidget$10;->val$dateDestination:I

    iput p3, p0, Lcom/stripe/android/view/CardInputWidget$10;->val$dateStartPosition:I

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 602
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 603
    iget v0, p0, Lcom/stripe/android/view/CardInputWidget$10;->val$dateDestination:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    iget v2, p0, Lcom/stripe/android/view/CardInputWidget$10;->val$dateStartPosition:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v1, v0

    .line 606
    iget-object v0, p0, Lcom/stripe/android/view/CardInputWidget$10;->this$0:Lcom/stripe/android/view/CardInputWidget;

    .line 607
    invoke-static {v0}, Lcom/stripe/android/view/CardInputWidget;->access$1000(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/ExpiryDateEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stripe/android/view/ExpiryDateEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 608
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 609
    iget-object v1, p0, Lcom/stripe/android/view/CardInputWidget$10;->this$0:Lcom/stripe/android/view/CardInputWidget;

    invoke-static {v1}, Lcom/stripe/android/view/CardInputWidget;->access$1000(Lcom/stripe/android/view/CardInputWidget;)Lcom/stripe/android/view/ExpiryDateEditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/stripe/android/view/ExpiryDateEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 610
    return-void
.end method
