.class public Lcom/stripe/android/Stripe;
.super Ljava/lang/Object;
.source "Stripe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/Stripe$TokenCreator;,
        Lcom/stripe/android/Stripe$SourceCreator;,
        Lcom/stripe/android/Stripe$ResponseWrapper;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDefaultPublishableKey:Ljava/lang/String;

.field private mLoggingResponseListener:Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;

.field mSourceCreator:Lcom/stripe/android/Stripe$SourceCreator;

.field private mStripeAccount:Ljava/lang/String;

.field mTokenCreator:Lcom/stripe/android/Stripe$TokenCreator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/stripe/android/Stripe$1;

    invoke-direct {v0, p0}, Lcom/stripe/android/Stripe$1;-><init>(Lcom/stripe/android/Stripe;)V

    iput-object v0, p0, Lcom/stripe/android/Stripe;->mSourceCreator:Lcom/stripe/android/Stripe$SourceCreator;

    .line 80
    new-instance v0, Lcom/stripe/android/Stripe$2;

    invoke-direct {v0, p0}, Lcom/stripe/android/Stripe$2;-><init>(Lcom/stripe/android/Stripe;)V

    iput-object v0, p0, Lcom/stripe/android/Stripe;->mTokenCreator:Lcom/stripe/android/Stripe$TokenCreator;

    .line 133
    iput-object p1, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/stripe/android/Stripe$1;

    invoke-direct {v0, p0}, Lcom/stripe/android/Stripe$1;-><init>(Lcom/stripe/android/Stripe;)V

    iput-object v0, p0, Lcom/stripe/android/Stripe;->mSourceCreator:Lcom/stripe/android/Stripe$SourceCreator;

    .line 80
    new-instance v0, Lcom/stripe/android/Stripe$2;

    invoke-direct {v0, p0}, Lcom/stripe/android/Stripe$2;-><init>(Lcom/stripe/android/Stripe;)V

    iput-object v0, p0, Lcom/stripe/android/Stripe;->mTokenCreator:Lcom/stripe/android/Stripe$TokenCreator;

    .line 143
    iput-object p1, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    .line 144
    invoke-virtual {p0, p2}, Lcom/stripe/android/Stripe;->setDefaultPublishableKey(Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method static synthetic access$000(Lcom/stripe/android/Stripe;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/stripe/android/Stripe;Ljava/util/concurrent/Executor;Landroid/os/AsyncTask;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/stripe/android/Stripe;->executeTask(Ljava/util/concurrent/Executor;Landroid/os/AsyncTask;)V

    return-void
.end method

.method static synthetic access$400(Lcom/stripe/android/Stripe;)Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mLoggingResponseListener:Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/stripe/android/Stripe;Lcom/stripe/android/Stripe$ResponseWrapper;Lcom/stripe/android/TokenCallback;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/stripe/android/Stripe;->tokenTaskPostExecution(Lcom/stripe/android/Stripe$ResponseWrapper;Lcom/stripe/android/TokenCallback;)V

    return-void
.end method

.method private createTokenFromParams(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/stripe/android/TokenCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 709
    if-nez p5, :cond_0

    .line 710
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Required Parameter: \'callback\' is required to use the created token and handle errors"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 715
    :cond_0
    invoke-direct {p0, p2}, Lcom/stripe/android/Stripe;->validateKey(Ljava/lang/String;)V

    .line 716
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mTokenCreator:Lcom/stripe/android/Stripe$TokenCreator;

    iget-object v3, p0, Lcom/stripe/android/Stripe;->mStripeAccount:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/stripe/android/Stripe$TokenCreator;->create(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V

    .line 723
    return-void
.end method

.method private executeTask(Ljava/util/concurrent/Executor;Landroid/os/AsyncTask;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Lcom/stripe/android/Stripe$ResponseWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 754
    if-eqz p1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-le v0, v1, :cond_0

    .line 755
    new-array v0, v2, [Ljava/lang/Void;

    invoke-virtual {p2, p1, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 759
    :goto_0
    return-void

    .line 757
    :cond_0
    new-array v0, v2, [Ljava/lang/Void;

    invoke-virtual {p2, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private tokenTaskPostExecution(Lcom/stripe/android/Stripe$ResponseWrapper;Lcom/stripe/android/TokenCallback;)V
    .locals 2

    .prologue
    .line 741
    iget-object v0, p1, Lcom/stripe/android/Stripe$ResponseWrapper;->token:Lcom/stripe/android/model/Token;

    if-eqz v0, :cond_0

    .line 742
    iget-object v0, p1, Lcom/stripe/android/Stripe$ResponseWrapper;->token:Lcom/stripe/android/model/Token;

    invoke-interface {p2, v0}, Lcom/stripe/android/TokenCallback;->onSuccess(Lcom/stripe/android/model/Token;)V

    .line 751
    :goto_0
    return-void

    .line 744
    :cond_0
    iget-object v0, p1, Lcom/stripe/android/Stripe$ResponseWrapper;->error:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 745
    iget-object v0, p1, Lcom/stripe/android/Stripe$ResponseWrapper;->error:Ljava/lang/Exception;

    invoke-interface {p2, v0}, Lcom/stripe/android/TokenCallback;->onError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 748
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Somehow got neither a token response or an error response"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/stripe/android/TokenCallback;->onError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private validateKey(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 726
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 727
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Publishable Key: You must use a valid publishable key to create a token.  For more info, see https://stripe.com/docs/stripe.js."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 732
    :cond_1
    const-string v0, "sk_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 733
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Publishable Key: You are using a secret key to create a token, instead of the publishable one. For more info, see https://stripe.com/docs/stripe.js"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 738
    :cond_2
    return-void
.end method


# virtual methods
.method public createBankAccountToken(Lcom/stripe/android/model/BankAccount;Lcom/stripe/android/TokenCallback;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/stripe/android/Stripe;->createBankAccountToken(Lcom/stripe/android/model/BankAccount;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V

    .line 158
    return-void
.end method

.method public createBankAccountToken(Lcom/stripe/android/model/BankAccount;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V
    .locals 6

    .prologue
    .line 175
    if-nez p1, :cond_0

    .line 176
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Required parameter: \'bankAccount\' is requred to create a token"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    .line 181
    invoke-static {v0, p1}, Lcom/stripe/android/util/StripeNetworkUtils;->hashMapFromBankAccount(Landroid/content/Context;Lcom/stripe/android/model/BankAccount;)Ljava/util/Map;

    move-result-object v1

    const-string v3, "bank_account"

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 180
    invoke-direct/range {v0 .. v5}, Lcom/stripe/android/Stripe;->createTokenFromParams(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V

    .line 186
    return-void
.end method

.method public createBankAccountTokenSynchronous(Lcom/stripe/android/model/BankAccount;)Lcom/stripe/android/model/Token;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/stripe/android/Stripe;->createBankAccountTokenSynchronous(Lcom/stripe/android/model/BankAccount;Ljava/lang/String;)Lcom/stripe/android/model/Token;

    move-result-object v0

    return-object v0
.end method

.method public createBankAccountTokenSynchronous(Lcom/stripe/android/model/BankAccount;Ljava/lang/String;)Lcom/stripe/android/model/Token;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 273
    invoke-direct {p0, p2}, Lcom/stripe/android/Stripe;->validateKey(Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mStripeAccount:Ljava/lang/String;

    const-string v1, "source"

    invoke-static {p2, v0, v1}, Lcom/stripe/android/net/RequestOptions;->builder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;

    move-result-object v0

    .line 277
    invoke-virtual {v0}, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->build()Lcom/stripe/android/net/RequestOptions;

    move-result-object v0

    .line 278
    iget-object v1, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    .line 280
    invoke-static {v2, p1}, Lcom/stripe/android/util/StripeNetworkUtils;->hashMapFromBankAccount(Landroid/content/Context;Lcom/stripe/android/model/BankAccount;)Ljava/util/Map;

    move-result-object v2

    const-string v3, "bank_account"

    iget-object v4, p0, Lcom/stripe/android/Stripe;->mLoggingResponseListener:Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;

    .line 278
    invoke-static {v1, v2, v0, v3, v4}, Lcom/stripe/android/net/StripeApiHandler;->createTokenOnServer(Landroid/content/Context;Ljava/util/Map;Lcom/stripe/android/net/RequestOptions;Ljava/lang/String;Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;)Lcom/stripe/android/model/Token;

    move-result-object v0

    return-object v0
.end method

.method public createPiiToken(Ljava/lang/String;Lcom/stripe/android/TokenCallback;)V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/stripe/android/Stripe;->createPiiToken(Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V

    .line 199
    return-void
.end method

.method public createPiiToken(Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V
    .locals 6

    .prologue
    .line 216
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    .line 217
    invoke-static {v0, p1}, Lcom/stripe/android/util/StripeNetworkUtils;->hashMapFromPersonalId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    const-string v3, "pii"

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 216
    invoke-direct/range {v0 .. v5}, Lcom/stripe/android/Stripe;->createTokenFromParams(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V

    .line 222
    return-void
.end method

.method public createPiiTokenSynchronous(Ljava/lang/String;)Lcom/stripe/android/model/Token;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 514
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/stripe/android/Stripe;->createPiiTokenSynchronous(Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Token;

    move-result-object v0

    return-object v0
.end method

.method public createPiiTokenSynchronous(Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Token;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 537
    invoke-direct {p0, p2}, Lcom/stripe/android/Stripe;->validateKey(Ljava/lang/String;)V

    .line 538
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mStripeAccount:Ljava/lang/String;

    const-string v1, "source"

    invoke-static {p2, v0, v1}, Lcom/stripe/android/net/RequestOptions;->builder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;

    move-result-object v0

    .line 541
    invoke-virtual {v0}, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->build()Lcom/stripe/android/net/RequestOptions;

    move-result-object v0

    .line 542
    iget-object v1, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    .line 544
    invoke-static {v2, p1}, Lcom/stripe/android/util/StripeNetworkUtils;->hashMapFromPersonalId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    const-string v3, "pii"

    iget-object v4, p0, Lcom/stripe/android/Stripe;->mLoggingResponseListener:Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;

    .line 542
    invoke-static {v1, v2, v0, v3, v4}, Lcom/stripe/android/net/StripeApiHandler;->createTokenOnServer(Landroid/content/Context;Ljava/util/Map;Lcom/stripe/android/net/RequestOptions;Ljava/lang/String;Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;)Lcom/stripe/android/model/Token;

    move-result-object v0

    return-object v0
.end method

.method public createSource(Lcom/stripe/android/model/SourceParams;Lcom/stripe/android/SourceCallback;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 294
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/stripe/android/Stripe;->createSource(Lcom/stripe/android/model/SourceParams;Lcom/stripe/android/SourceCallback;Ljava/lang/String;Ljava/util/concurrent/Executor;)V

    .line 295
    return-void
.end method

.method public createSource(Lcom/stripe/android/model/SourceParams;Lcom/stripe/android/SourceCallback;Ljava/lang/String;Ljava/util/concurrent/Executor;)V
    .locals 6

    .prologue
    .line 310
    if-nez p3, :cond_0

    iget-object v2, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    .line 311
    :goto_0
    if-nez v2, :cond_1

    .line 315
    :goto_1
    return-void

    :cond_0
    move-object v2, p3

    .line 310
    goto :goto_0

    .line 314
    :cond_1
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mSourceCreator:Lcom/stripe/android/Stripe$SourceCreator;

    iget-object v3, p0, Lcom/stripe/android/Stripe;->mStripeAccount:Ljava/lang/String;

    move-object v1, p1

    move-object v4, p4

    move-object v5, p2

    invoke-interface/range {v0 .. v5}, Lcom/stripe/android/Stripe$SourceCreator;->create(Lcom/stripe/android/model/SourceParams;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/SourceCallback;)V

    goto :goto_1
.end method

.method public createSourceSynchronous(Lcom/stripe/android/model/SourceParams;)Lcom/stripe/android/model/Source;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 407
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/stripe/android/Stripe;->createSourceSynchronous(Lcom/stripe/android/model/SourceParams;Ljava/lang/String;)Lcom/stripe/android/model/Source;

    move-result-object v0

    return-object v0
.end method

.method public createSourceSynchronous(Lcom/stripe/android/model/SourceParams;Ljava/lang/String;)Lcom/stripe/android/model/Source;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 430
    if-nez p2, :cond_0

    iget-object v3, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    .line 431
    :goto_0
    if-nez v3, :cond_1

    .line 434
    :goto_1
    return-object v0

    :cond_0
    move-object v3, p2

    .line 430
    goto :goto_0

    .line 434
    :cond_1
    iget-object v1, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/stripe/android/Stripe;->mStripeAccount:Ljava/lang/String;

    iget-object v5, p0, Lcom/stripe/android/Stripe;->mLoggingResponseListener:Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/stripe/android/net/StripeApiHandler;->createSourceOnServer(Lcom/stripe/android/util/StripeNetworkUtils$UidProvider;Landroid/content/Context;Lcom/stripe/android/model/SourceParams;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;)Lcom/stripe/android/model/Source;

    move-result-object v0

    goto :goto_1
.end method

.method public createToken(Lcom/stripe/android/model/Card;Lcom/stripe/android/TokenCallback;)V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, p2}, Lcom/stripe/android/Stripe;->createToken(Lcom/stripe/android/model/Card;Ljava/lang/String;Lcom/stripe/android/TokenCallback;)V

    .line 327
    return-void
.end method

.method public createToken(Lcom/stripe/android/model/Card;Ljava/lang/String;Lcom/stripe/android/TokenCallback;)V
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/stripe/android/Stripe;->createToken(Lcom/stripe/android/model/Card;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V

    .line 341
    return-void
.end method

.method public createToken(Lcom/stripe/android/model/Card;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V
    .locals 6

    .prologue
    .line 371
    if-nez p1, :cond_0

    .line 372
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Required Parameter: \'card\' is required to create a token"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    .line 377
    invoke-static {v0, p1}, Lcom/stripe/android/util/StripeNetworkUtils;->hashMapFromCard(Landroid/content/Context;Lcom/stripe/android/model/Card;)Ljava/util/Map;

    move-result-object v1

    const-string v3, "card"

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 376
    invoke-direct/range {v0 .. v5}, Lcom/stripe/android/Stripe;->createTokenFromParams(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V

    .line 382
    return-void
.end method

.method public createToken(Lcom/stripe/android/model/Card;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/stripe/android/Stripe;->createToken(Lcom/stripe/android/model/Card;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/stripe/android/TokenCallback;)V

    .line 355
    return-void
.end method

.method public createTokenSynchronous(Lcom/stripe/android/model/Card;)Lcom/stripe/android/model/Token;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 458
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/stripe/android/Stripe;->createTokenSynchronous(Lcom/stripe/android/model/Card;Ljava/lang/String;)Lcom/stripe/android/model/Token;

    move-result-object v0

    return-object v0
.end method

.method public createTokenSynchronous(Lcom/stripe/android/model/Card;Ljava/lang/String;)Lcom/stripe/android/model/Token;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 481
    invoke-direct {p0, p2}, Lcom/stripe/android/Stripe;->validateKey(Ljava/lang/String;)V

    .line 483
    iget-object v0, p0, Lcom/stripe/android/Stripe;->mStripeAccount:Ljava/lang/String;

    const-string v1, "source"

    invoke-static {p2, v0, v1}, Lcom/stripe/android/net/RequestOptions;->builder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;

    move-result-object v0

    .line 486
    invoke-virtual {v0}, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->build()Lcom/stripe/android/net/RequestOptions;

    move-result-object v0

    .line 487
    iget-object v1, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/stripe/android/Stripe;->mContext:Landroid/content/Context;

    .line 489
    invoke-static {v2, p1}, Lcom/stripe/android/util/StripeNetworkUtils;->hashMapFromCard(Landroid/content/Context;Lcom/stripe/android/model/Card;)Ljava/util/Map;

    move-result-object v2

    const-string v3, "card"

    iget-object v4, p0, Lcom/stripe/android/Stripe;->mLoggingResponseListener:Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;

    .line 487
    invoke-static {v1, v2, v0, v3, v4}, Lcom/stripe/android/net/StripeApiHandler;->createTokenOnServer(Landroid/content/Context;Ljava/util/Map;Lcom/stripe/android/net/RequestOptions;Ljava/lang/String;Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;)Lcom/stripe/android/model/Token;

    move-result-object v0

    return-object v0
.end method

.method public pollSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/PollingResponseHandler;Ljava/lang/Integer;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 575
    if-nez p3, :cond_0

    iget-object p3, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    .line 576
    :cond_0
    if-nez p3, :cond_1

    .line 581
    :goto_0
    return-void

    .line 580
    :cond_1
    invoke-static {p1, p2, p3, p4, p5}, Lcom/stripe/android/net/StripeApiHandler;->pollSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/PollingResponseHandler;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public pollSourceSynchronous(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/stripe/android/net/PollingResponse;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 609
    if-nez p3, :cond_0

    iget-object p3, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    .line 610
    :cond_0
    if-nez p3, :cond_1

    .line 611
    const/4 v0, 0x0

    .line 614
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1, p2, p3, p4}, Lcom/stripe/android/net/StripeApiHandler;->pollSourceSynchronous(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/stripe/android/net/PollingResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public retrieveSourceSynchronous(Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 641
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/stripe/android/Stripe;->retrieveSourceSynchronous(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;

    move-result-object v0

    return-object v0
.end method

.method public retrieveSourceSynchronous(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/AuthenticationException;,
            Lcom/stripe/android/exception/InvalidRequestException;,
            Lcom/stripe/android/exception/APIConnectionException;,
            Lcom/stripe/android/exception/CardException;,
            Lcom/stripe/android/exception/APIException;
        }
    .end annotation

    .prologue
    .line 670
    if-nez p3, :cond_0

    iget-object p3, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    .line 671
    :cond_0
    if-nez p3, :cond_1

    .line 672
    const/4 v0, 0x0

    .line 674
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1, p2, p3}, Lcom/stripe/android/net/StripeApiHandler;->retrieveSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;

    move-result-object v0

    goto :goto_0
.end method

.method public setDefaultPublishableKey(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 683
    invoke-direct {p0, p1}, Lcom/stripe/android/Stripe;->validateKey(Ljava/lang/String;)V

    .line 684
    iput-object p1, p0, Lcom/stripe/android/Stripe;->mDefaultPublishableKey:Ljava/lang/String;

    .line 685
    return-void
.end method

.method setLoggingResponseListener(Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;)V
    .locals 0

    .prologue
    .line 700
    iput-object p1, p0, Lcom/stripe/android/Stripe;->mLoggingResponseListener:Lcom/stripe/android/net/StripeApiHandler$LoggingResponseListener;

    .line 701
    return-void
.end method

.method public setStripeAccount(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 695
    iput-object p1, p0, Lcom/stripe/android/Stripe;->mStripeAccount:Ljava/lang/String;

    .line 696
    return-void
.end method
