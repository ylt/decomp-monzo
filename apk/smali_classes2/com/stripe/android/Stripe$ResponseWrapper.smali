.class Lcom/stripe/android/Stripe$ResponseWrapper;
.super Ljava/lang/Object;
.source "Stripe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stripe/android/Stripe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResponseWrapper"
.end annotation


# instance fields
.field final error:Ljava/lang/Exception;

.field final source:Lcom/stripe/android/model/Source;

.field final synthetic this$0:Lcom/stripe/android/Stripe;

.field final token:Lcom/stripe/android/model/Token;


# direct methods
.method private constructor <init>(Lcom/stripe/android/Stripe;Lcom/stripe/android/model/Source;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 772
    iput-object p1, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->this$0:Lcom/stripe/android/Stripe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 773
    iput-object p2, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->source:Lcom/stripe/android/model/Source;

    .line 774
    iput-object v0, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->error:Ljava/lang/Exception;

    .line 775
    iput-object v0, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->token:Lcom/stripe/android/model/Token;

    .line 776
    return-void
.end method

.method synthetic constructor <init>(Lcom/stripe/android/Stripe;Lcom/stripe/android/model/Source;Lcom/stripe/android/Stripe$1;)V
    .locals 0

    .prologue
    .line 761
    invoke-direct {p0, p1, p2}, Lcom/stripe/android/Stripe$ResponseWrapper;-><init>(Lcom/stripe/android/Stripe;Lcom/stripe/android/model/Source;)V

    return-void
.end method

.method private constructor <init>(Lcom/stripe/android/Stripe;Lcom/stripe/android/model/Token;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 766
    iput-object p1, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->this$0:Lcom/stripe/android/Stripe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 767
    iput-object p2, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->token:Lcom/stripe/android/model/Token;

    .line 768
    iput-object v0, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->source:Lcom/stripe/android/model/Source;

    .line 769
    iput-object v0, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->error:Ljava/lang/Exception;

    .line 770
    return-void
.end method

.method synthetic constructor <init>(Lcom/stripe/android/Stripe;Lcom/stripe/android/model/Token;Lcom/stripe/android/Stripe$1;)V
    .locals 0

    .prologue
    .line 761
    invoke-direct {p0, p1, p2}, Lcom/stripe/android/Stripe$ResponseWrapper;-><init>(Lcom/stripe/android/Stripe;Lcom/stripe/android/model/Token;)V

    return-void
.end method

.method private constructor <init>(Lcom/stripe/android/Stripe;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 778
    iput-object p1, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->this$0:Lcom/stripe/android/Stripe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 779
    iput-object p2, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->error:Ljava/lang/Exception;

    .line 780
    iput-object v0, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->source:Lcom/stripe/android/model/Source;

    .line 781
    iput-object v0, p0, Lcom/stripe/android/Stripe$ResponseWrapper;->token:Lcom/stripe/android/model/Token;

    .line 782
    return-void
.end method

.method synthetic constructor <init>(Lcom/stripe/android/Stripe;Ljava/lang/Exception;Lcom/stripe/android/Stripe$1;)V
    .locals 0

    .prologue
    .line 761
    invoke-direct {p0, p1, p2}, Lcom/stripe/android/Stripe$ResponseWrapper;-><init>(Lcom/stripe/android/Stripe;Ljava/lang/Exception;)V

    return-void
.end method
