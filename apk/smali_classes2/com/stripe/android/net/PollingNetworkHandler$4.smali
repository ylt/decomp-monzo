.class Lcom/stripe/android/net/PollingNetworkHandler$4;
.super Landroid/os/Handler;
.source "PollingNetworkHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/net/PollingNetworkHandler;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/PollingResponseHandler;Ljava/lang/Integer;Lcom/stripe/android/net/SourceRetriever;Lcom/stripe/android/net/PollingParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field terminated:Z

.field final synthetic this$0:Lcom/stripe/android/net/PollingNetworkHandler;

.field final synthetic val$looper:Landroid/os/Looper;


# direct methods
.method constructor <init>(Lcom/stripe/android/net/PollingNetworkHandler;Landroid/os/Looper;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 183
    iput-object p1, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    iput-object p3, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->val$looper:Landroid/os/Looper;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->terminated:Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 188
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 189
    iget-boolean v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->terminated:Z

    if-eqz v0, :cond_0

    .line 205
    :goto_0
    return-void

    .line 193
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-ltz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$900(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/Runnable;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/stripe/android/net/PollingNetworkHandler$4;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 196
    :cond_1
    iput-boolean v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->terminated:Z

    .line 197
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$1000(Lcom/stripe/android/net/PollingNetworkHandler;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->val$looper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 201
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 202
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 203
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$4;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$900(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/stripe/android/net/PollingNetworkHandler$4;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
