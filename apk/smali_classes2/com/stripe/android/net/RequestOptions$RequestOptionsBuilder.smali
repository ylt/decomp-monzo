.class public final Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
.super Ljava/lang/Object;
.source "RequestOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stripe/android/net/RequestOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestOptionsBuilder"
.end annotation


# instance fields
.field private apiVersion:Ljava/lang/String;

.field private guid:Ljava/lang/String;

.field private idempotencyKey:Ljava/lang/String;

.field private publishableApiKey:Ljava/lang/String;

.field private requestType:Ljava/lang/String;

.field private stripeAccount:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->publishableApiKey:Ljava/lang/String;

    .line 143
    iput-object p2, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->requestType:Ljava/lang/String;

    .line 144
    return-void
.end method


# virtual methods
.method public build()Lcom/stripe/android/net/RequestOptions;
    .locals 8

    .prologue
    .line 210
    new-instance v0, Lcom/stripe/android/net/RequestOptions;

    iget-object v1, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->apiVersion:Ljava/lang/String;

    iget-object v2, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->guid:Ljava/lang/String;

    iget-object v3, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->idempotencyKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->publishableApiKey:Ljava/lang/String;

    iget-object v5, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->requestType:Ljava/lang/String;

    iget-object v6, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->stripeAccount:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/stripe/android/net/RequestOptions;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/RequestOptions$1;)V

    return-object v0
.end method

.method setApiVersion(Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
    .locals 1

    .prologue
    .line 192
    invoke-static {p1}, Lcom/stripe/android/util/StripeTextUtils;->isBlank(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    iput-object p1, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->apiVersion:Ljava/lang/String;

    .line 195
    return-object p0
.end method

.method setGuid(Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->guid:Ljava/lang/String;

    .line 180
    return-object p0
.end method

.method setIdempotencyKey(Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->idempotencyKey:Ljava/lang/String;

    .line 168
    return-object p0
.end method

.method setPublishableApiKey(Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->publishableApiKey:Ljava/lang/String;

    .line 155
    return-object p0
.end method

.method setStripeAccount(Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->stripeAccount:Ljava/lang/String;

    .line 201
    return-object p0
.end method
