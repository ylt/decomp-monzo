.class Lcom/stripe/android/net/PollingNetworkHandler$1;
.super Ljava/lang/Object;
.source "PollingNetworkHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stripe/android/net/PollingNetworkHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stripe/android/net/PollingNetworkHandler;


# direct methods
.method constructor <init>(Lcom/stripe/android/net/PollingNetworkHandler;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, -0x1

    const/4 v4, 0x3

    const/4 v2, 0x1

    .line 47
    const/4 v1, 0x0

    .line 49
    :try_start_0
    iget-object v5, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v5}, Lcom/stripe/android/net/PollingNetworkHandler;->access$300(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/net/SourceRetriever;

    move-result-object v5

    iget-object v6, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    .line 50
    invoke-static {v6}, Lcom/stripe/android/net/PollingNetworkHandler;->access$000(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    .line 51
    invoke-static {v7}, Lcom/stripe/android/net/PollingNetworkHandler;->access$100(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    .line 52
    invoke-static {v8}, Lcom/stripe/android/net/PollingNetworkHandler;->access$200(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/String;

    move-result-object v8

    .line 49
    invoke-interface {v5, v6, v7, v8}, Lcom/stripe/android/net/SourceRetriever;->retrieveSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;

    move-result-object v5

    .line 53
    iget-object v6, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v6, v5}, Lcom/stripe/android/net/PollingNetworkHandler;->access$402(Lcom/stripe/android/net/PollingNetworkHandler;Lcom/stripe/android/model/Source;)Lcom/stripe/android/model/Source;

    .line 54
    invoke-virtual {v5}, Lcom/stripe/android/model/Source;->getStatus()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 55
    invoke-virtual {v5}, Lcom/stripe/android/model/Source;->getStatus()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I
    :try_end_0
    .catch Lcom/stripe/android/exception/StripeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    :cond_1
    move-object v0, v1

    .line 76
    :goto_1
    if-eqz v0, :cond_2

    .line 77
    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v1}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 80
    :cond_2
    :goto_2
    return-void

    .line 55
    :sswitch_0
    :try_start_1
    const-string v2, "pending"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "chargeable"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v2, "consumed"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v3

    goto :goto_0

    :sswitch_3
    const-string v2, "canceled"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v4

    goto :goto_0

    :sswitch_4
    const-string v2, "failed"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 57
    :pswitch_0
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    goto :goto_1

    .line 60
    :pswitch_1
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    goto :goto_1

    .line 63
    :pswitch_2
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    goto :goto_1

    .line 66
    :pswitch_3
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    goto :goto_1

    .line 69
    :pswitch_4
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
    :try_end_1
    .catch Lcom/stripe/android/exception/StripeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 73
    :catch_0
    move-exception v0

    .line 74
    :try_start_2
    iget-object v2, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v2}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 76
    if-eqz v0, :cond_2

    .line 77
    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v1}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_2

    .line 76
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 77
    iget-object v2, p0, Lcom/stripe/android/net/PollingNetworkHandler$1;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v2}, Lcom/stripe/android/net/PollingNetworkHandler;->access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_3
    throw v0

    .line 55
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4c696bc3 -> :sswitch_4
        -0x28af7669 -> :sswitch_0
        -0x21d77c18 -> :sswitch_2
        -0x7577b67 -> :sswitch_3
        0x548c3e0e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
