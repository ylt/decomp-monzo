.class public Lcom/stripe/android/net/RequestOptions;
.super Ljava/lang/Object;
.source "RequestOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;,
        Lcom/stripe/android/net/RequestOptions$RequestType;
    }
.end annotation


# static fields
.field public static final TYPE_JSON:Ljava/lang/String; = "json_data"

.field public static final TYPE_QUERY:Ljava/lang/String; = "source"


# instance fields
.field private final mApiVersion:Ljava/lang/String;

.field private final mGuid:Ljava/lang/String;

.field private final mIdempotencyKey:Ljava/lang/String;

.field private final mPublishableApiKey:Ljava/lang/String;

.field private final mRequestType:Ljava/lang/String;

.field private final mStripeAccount:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/stripe/android/net/RequestOptions;->mApiVersion:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/stripe/android/net/RequestOptions;->mGuid:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/stripe/android/net/RequestOptions;->mIdempotencyKey:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/stripe/android/net/RequestOptions;->mPublishableApiKey:Ljava/lang/String;

    .line 41
    iput-object p5, p0, Lcom/stripe/android/net/RequestOptions;->mRequestType:Ljava/lang/String;

    .line 42
    iput-object p6, p0, Lcom/stripe/android/net/RequestOptions;->mStripeAccount:Ljava/lang/String;

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/RequestOptions$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct/range {p0 .. p6}, Lcom/stripe/android/net/RequestOptions;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static builder(Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
    .locals 1

    .prologue
    .line 96
    const-string v0, "source"

    invoke-static {p0, v0}, Lcom/stripe/android/net/RequestOptions;->builder(Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static builder(Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;

    invoke-direct {v0, p0, p1}, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static builder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;

    invoke-direct {v0, p0, p2}, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v0, p1}, Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;->setStripeAccount(Ljava/lang/String;)Lcom/stripe/android/net/RequestOptions$RequestOptionsBuilder;

    move-result-object v0

    .line 103
    return-object v0
.end method


# virtual methods
.method getApiVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/stripe/android/net/RequestOptions;->mApiVersion:Ljava/lang/String;

    return-object v0
.end method

.method getGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/stripe/android/net/RequestOptions;->mGuid:Ljava/lang/String;

    return-object v0
.end method

.method getIdempotencyKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/stripe/android/net/RequestOptions;->mIdempotencyKey:Ljava/lang/String;

    return-object v0
.end method

.method getPublishableApiKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/stripe/android/net/RequestOptions;->mPublishableApiKey:Ljava/lang/String;

    return-object v0
.end method

.method getRequestType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/stripe/android/net/RequestOptions;->mRequestType:Ljava/lang/String;

    return-object v0
.end method

.method getStripeAccount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/stripe/android/net/RequestOptions;->mStripeAccount:Ljava/lang/String;

    return-object v0
.end method
