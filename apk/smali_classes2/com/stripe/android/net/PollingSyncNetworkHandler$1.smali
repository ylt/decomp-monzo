.class final Lcom/stripe/android/net/PollingSyncNetworkHandler$1;
.super Ljava/lang/Object;
.source "PollingSyncNetworkHandler.java"

# interfaces
.implements Lcom/stripe/android/net/SourceRetriever;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/net/PollingSyncNetworkHandler;->generateSourceRetriever()Lcom/stripe/android/net/SourceRetriever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public retrieveSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/StripeException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {p1, p2, p3}, Lcom/stripe/android/net/StripeApiHandler;->retrieveSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;

    move-result-object v0

    return-object v0
.end method
