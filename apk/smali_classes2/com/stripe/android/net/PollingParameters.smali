.class Lcom/stripe/android/net/PollingParameters;
.super Ljava/lang/Object;
.source "PollingParameters.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final DEFAULT_TIMEOUT_MS:J = 0x2710L

.field private static final INITIAL_DELAY_MS:J = 0x3e8L

.field private static final MAX_DELAY_MS:J = 0x3a98L

.field private static final MAX_RETRY_COUNT:I = 0x5

.field private static final MAX_TIMEOUT_MS:J = 0x493e0L

.field private static final POLLING_MULTIPLIER:I = 0x2


# instance fields
.field private final mDefaultTimeoutMs:J

.field private final mInitialDelayMs:J

.field private final mMaxDelayMs:J

.field private final mMaxRetryCount:I

.field private final mMaxTimeoutMs:J

.field private final mPollingMultiplier:I


# direct methods
.method constructor <init>(JJJIJI)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-wide p1, p0, Lcom/stripe/android/net/PollingParameters;->mDefaultTimeoutMs:J

    .line 34
    iput-wide p3, p0, Lcom/stripe/android/net/PollingParameters;->mInitialDelayMs:J

    .line 35
    iput-wide p5, p0, Lcom/stripe/android/net/PollingParameters;->mMaxDelayMs:J

    .line 36
    iput p7, p0, Lcom/stripe/android/net/PollingParameters;->mMaxRetryCount:I

    .line 37
    iput-wide p8, p0, Lcom/stripe/android/net/PollingParameters;->mMaxTimeoutMs:J

    .line 38
    iput p10, p0, Lcom/stripe/android/net/PollingParameters;->mPollingMultiplier:I

    .line 39
    return-void
.end method

.method static generateDefaultParameters()Lcom/stripe/android/net/PollingParameters;
    .locals 12

    .prologue
    .line 70
    new-instance v1, Lcom/stripe/android/net/PollingParameters;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x3e8

    const-wide/16 v6, 0x3a98

    const/4 v8, 0x5

    const-wide/32 v9, 0x493e0

    const/4 v11, 0x2

    invoke-direct/range {v1 .. v11}, Lcom/stripe/android/net/PollingParameters;-><init>(JJJIJI)V

    return-object v1
.end method


# virtual methods
.method getDefaultTimeoutMs()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/stripe/android/net/PollingParameters;->mDefaultTimeoutMs:J

    return-wide v0
.end method

.method getInitialDelayMs()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/stripe/android/net/PollingParameters;->mInitialDelayMs:J

    return-wide v0
.end method

.method getInitialDelayMsInt()I
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/stripe/android/net/PollingParameters;->mInitialDelayMs:J

    long-to-int v0, v0

    return v0
.end method

.method getMaxDelayMs()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/stripe/android/net/PollingParameters;->mMaxDelayMs:J

    return-wide v0
.end method

.method getMaxRetryCount()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/stripe/android/net/PollingParameters;->mMaxRetryCount:I

    return v0
.end method

.method getMaxTimeoutMs()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/stripe/android/net/PollingParameters;->mMaxTimeoutMs:J

    return-wide v0
.end method

.method getPollingMultiplier()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/stripe/android/net/PollingParameters;->mPollingMultiplier:I

    return v0
.end method
