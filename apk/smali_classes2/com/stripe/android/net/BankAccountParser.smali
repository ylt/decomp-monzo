.class Lcom/stripe/android/net/BankAccountParser;
.super Ljava/lang/Object;
.source "BankAccountParser.java"


# static fields
.field private static final FIELD_ACCOUNT_HOLDER_NAME:Ljava/lang/String; = "account_holder_name"

.field private static final FIELD_ACCOUNT_HOLDER_TYPE:Ljava/lang/String; = "account_holder_type"

.field private static final FIELD_BANK_NAME:Ljava/lang/String; = "bank_name"

.field private static final FIELD_COUNTRY:Ljava/lang/String; = "country"

.field private static final FIELD_CURRENCY:Ljava/lang/String; = "currency"

.field private static final FIELD_FINGERPRINT:Ljava/lang/String; = "fingerprint"

.field private static final FIELD_LAST4:Ljava/lang/String; = "last4"

.field private static final FIELD_ROUTING_NUMBER:Ljava/lang/String; = "routing_number"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static parseBankAccount(Ljava/lang/String;)Lcom/stripe/android/model/BankAccount;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 26
    invoke-static {v0}, Lcom/stripe/android/net/BankAccountParser;->parseBankAccount(Lorg/json/JSONObject;)Lcom/stripe/android/model/BankAccount;

    move-result-object v0

    return-object v0
.end method

.method static parseBankAccount(Lorg/json/JSONObject;)Lcom/stripe/android/model/BankAccount;
    .locals 9

    .prologue
    .line 30
    new-instance v0, Lcom/stripe/android/model/BankAccount;

    const-string v1, "account_holder_name"

    .line 31
    invoke-static {p0, v1}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "account_holder_type"

    .line 33
    invoke-static {p0, v2}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 32
    invoke-static {v2}, Lcom/stripe/android/util/StripeTextUtils;->asBankAccountType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "bank_name"

    .line 34
    invoke-static {p0, v3}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "country"

    .line 35
    invoke-static {p0, v4}, Lcom/stripe/android/util/StripeJsonUtils;->optCountryCode(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "currency"

    .line 36
    invoke-static {p0, v5}, Lcom/stripe/android/util/StripeJsonUtils;->optCurrency(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "fingerprint"

    .line 37
    invoke-static {p0, v6}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "last4"

    .line 38
    invoke-static {p0, v7}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "routing_number"

    .line 39
    invoke-static {p0, v8}, Lcom/stripe/android/util/StripeJsonUtils;->optString(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/stripe/android/model/BankAccount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-object v0
.end method
