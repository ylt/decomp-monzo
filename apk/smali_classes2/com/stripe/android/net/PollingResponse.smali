.class public Lcom/stripe/android/net/PollingResponse;
.super Ljava/lang/Object;
.source "PollingResponse.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private mIsExpired:Z

.field private mIsSuccess:Z

.field private mSource:Lcom/stripe/android/model/Source;

.field private mStripeException:Lcom/stripe/android/exception/StripeException;


# direct methods
.method constructor <init>(Lcom/stripe/android/model/Source;Lcom/stripe/android/exception/StripeException;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/stripe/android/net/PollingResponse;->mSource:Lcom/stripe/android/model/Source;

    .line 30
    iput-object p2, p0, Lcom/stripe/android/net/PollingResponse;->mStripeException:Lcom/stripe/android/exception/StripeException;

    .line 31
    iput-boolean v0, p0, Lcom/stripe/android/net/PollingResponse;->mIsExpired:Z

    .line 32
    iput-boolean v0, p0, Lcom/stripe/android/net/PollingResponse;->mIsSuccess:Z

    .line 33
    return-void
.end method

.method constructor <init>(Lcom/stripe/android/model/Source;ZZ)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/stripe/android/net/PollingResponse;->mSource:Lcom/stripe/android/model/Source;

    .line 24
    iput-boolean p3, p0, Lcom/stripe/android/net/PollingResponse;->mIsExpired:Z

    .line 25
    iput-boolean p2, p0, Lcom/stripe/android/net/PollingResponse;->mIsSuccess:Z

    .line 26
    return-void
.end method


# virtual methods
.method public getSource()Lcom/stripe/android/model/Source;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/stripe/android/net/PollingResponse;->mSource:Lcom/stripe/android/model/Source;

    return-object v0
.end method

.method public getStripeException()Lcom/stripe/android/exception/StripeException;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/stripe/android/net/PollingResponse;->mStripeException:Lcom/stripe/android/exception/StripeException;

    return-object v0
.end method

.method public isExpired()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/stripe/android/net/PollingResponse;->mIsExpired:Z

    return v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/stripe/android/net/PollingResponse;->mIsSuccess:Z

    return v0
.end method
