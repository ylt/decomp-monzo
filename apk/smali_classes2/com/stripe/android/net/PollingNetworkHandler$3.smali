.class Lcom/stripe/android/net/PollingNetworkHandler$3;
.super Landroid/os/Handler;
.source "PollingNetworkHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/net/PollingNetworkHandler;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/PollingResponseHandler;Ljava/lang/Integer;Lcom/stripe/android/net/SourceRetriever;Lcom/stripe/android/net/PollingParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field delayMs:I

.field terminated:Z

.field final synthetic this$0:Lcom/stripe/android/net/PollingNetworkHandler;

.field final synthetic val$callback:Lcom/stripe/android/net/PollingResponseHandler;


# direct methods
.method constructor <init>(Lcom/stripe/android/net/PollingNetworkHandler;Landroid/os/Looper;Lcom/stripe/android/net/PollingResponseHandler;)V
    .locals 1

    .prologue
    .line 117
    iput-object p1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    iput-object p3, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->val$callback:Lcom/stripe/android/net/PollingResponseHandler;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 118
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$600(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/net/PollingParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stripe/android/net/PollingParameters;->getInitialDelayMsInt()I

    move-result v0

    iput v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->delayMs:I

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->terminated:Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 123
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 124
    iget-boolean v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->terminated:Z

    if-eqz v0, :cond_0

    .line 171
    :goto_0
    return-void

    .line 128
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 147
    :pswitch_1
    iput-boolean v3, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->terminated:Z

    .line 148
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->val$callback:Lcom/stripe/android/net/PollingResponseHandler;

    new-instance v1, Lcom/stripe/android/net/PollingResponse;

    iget-object v2, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    .line 149
    invoke-static {v2}, Lcom/stripe/android/net/PollingNetworkHandler;->access$400(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/model/Source;

    move-result-object v2

    invoke-direct {v1, v2, v4, v3}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;ZZ)V

    .line 148
    invoke-interface {v0, v1}, Lcom/stripe/android/net/PollingResponseHandler;->onPollingResponse(Lcom/stripe/android/net/PollingResponse;)V

    .line 150
    invoke-virtual {p0, v5}, Lcom/stripe/android/net/PollingNetworkHandler$3;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0

    .line 130
    :pswitch_2
    iput-boolean v3, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->terminated:Z

    .line 131
    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->val$callback:Lcom/stripe/android/net/PollingResponseHandler;

    new-instance v2, Lcom/stripe/android/net/PollingResponse;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/stripe/android/model/Source;

    invoke-direct {v2, v0, v3, v4}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;ZZ)V

    invoke-interface {v1, v2}, Lcom/stripe/android/net/PollingResponseHandler;->onPollingResponse(Lcom/stripe/android/net/PollingResponse;)V

    .line 133
    invoke-virtual {p0, v5}, Lcom/stripe/android/net/PollingNetworkHandler$3;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0

    .line 136
    :pswitch_3
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0, v4}, Lcom/stripe/android/net/PollingNetworkHandler;->access$702(Lcom/stripe/android/net/PollingNetworkHandler;I)I

    .line 137
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$600(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/net/PollingParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stripe/android/net/PollingParameters;->getInitialDelayMsInt()I

    move-result v0

    iput v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->delayMs:I

    .line 138
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$800(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    iget v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->delayMs:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 141
    :pswitch_4
    iput-boolean v3, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->terminated:Z

    .line 142
    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->val$callback:Lcom/stripe/android/net/PollingResponseHandler;

    new-instance v2, Lcom/stripe/android/net/PollingResponse;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/stripe/android/model/Source;

    invoke-direct {v2, v0, v4, v4}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;ZZ)V

    invoke-interface {v1, v2}, Lcom/stripe/android/net/PollingResponseHandler;->onPollingResponse(Lcom/stripe/android/net/PollingResponse;)V

    .line 144
    invoke-virtual {p0, v5}, Lcom/stripe/android/net/PollingNetworkHandler$3;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0

    .line 153
    :pswitch_5
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$708(Lcom/stripe/android/net/PollingNetworkHandler;)I

    .line 154
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$700(Lcom/stripe/android/net/PollingNetworkHandler;)I

    move-result v0

    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v1}, Lcom/stripe/android/net/PollingNetworkHandler;->access$600(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/net/PollingParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/stripe/android/net/PollingParameters;->getMaxRetryCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 155
    iput-boolean v3, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->terminated:Z

    .line 156
    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->val$callback:Lcom/stripe/android/net/PollingResponseHandler;

    new-instance v2, Lcom/stripe/android/net/PollingResponse;

    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    .line 157
    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$400(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/model/Source;

    move-result-object v3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/stripe/android/exception/StripeException;

    invoke-direct {v2, v3, v0}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;Lcom/stripe/android/exception/StripeException;)V

    .line 156
    invoke-interface {v1, v2}, Lcom/stripe/android/net/PollingResponseHandler;->onPollingResponse(Lcom/stripe/android/net/PollingResponse;)V

    .line 159
    invoke-virtual {p0, v5}, Lcom/stripe/android/net/PollingNetworkHandler$3;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 162
    :cond_1
    iget v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->delayMs:I

    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    .line 163
    invoke-static {v1}, Lcom/stripe/android/net/PollingNetworkHandler;->access$600(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/net/PollingParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/stripe/android/net/PollingParameters;->getPollingMultiplier()I

    move-result v1

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    .line 164
    invoke-static {v1}, Lcom/stripe/android/net/PollingNetworkHandler;->access$600(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/net/PollingParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/stripe/android/net/PollingParameters;->getMaxDelayMs()J

    move-result-wide v2

    long-to-int v1, v2

    .line 162
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->delayMs:I

    .line 165
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-static {v0}, Lcom/stripe/android/net/PollingNetworkHandler;->access$800(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;

    move-result-object v0

    iget v1, p0, Lcom/stripe/android/net/PollingNetworkHandler$3;->delayMs:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
