.class Lcom/stripe/android/net/PollingNetworkHandler$2;
.super Ljava/lang/Object;
.source "PollingNetworkHandler.java"

# interfaces
.implements Lcom/stripe/android/net/SourceRetriever;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stripe/android/net/PollingNetworkHandler;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/PollingResponseHandler;Ljava/lang/Integer;Lcom/stripe/android/net/SourceRetriever;Lcom/stripe/android/net/PollingParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/stripe/android/net/PollingNetworkHandler;


# direct methods
.method constructor <init>(Lcom/stripe/android/net/PollingNetworkHandler;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/stripe/android/net/PollingNetworkHandler$2;->this$0:Lcom/stripe/android/net/PollingNetworkHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public retrieveSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stripe/android/exception/StripeException;
        }
    .end annotation

    .prologue
    .line 103
    invoke-static {p1, p2, p3}, Lcom/stripe/android/net/StripeApiHandler;->retrieveSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;

    move-result-object v0

    return-object v0
.end method
