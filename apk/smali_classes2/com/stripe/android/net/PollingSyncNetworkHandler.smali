.class Lcom/stripe/android/net/PollingSyncNetworkHandler;
.super Ljava/lang/Object;
.source "PollingSyncNetworkHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final mClientSecret:Ljava/lang/String;

.field private final mPollingParameters:Lcom/stripe/android/net/PollingParameters;

.field private final mPublishableKey:Ljava/lang/String;

.field private final mSourceId:Ljava/lang/String;

.field private mSourceRetriever:Lcom/stripe/android/net/SourceRetriever;

.field private mTimeOutMs:J

.field private mTimeRetriever:Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/stripe/android/net/SourceRetriever;Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;Lcom/stripe/android/net/PollingParameters;)V
    .locals 4

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mSourceId:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mClientSecret:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPublishableKey:Ljava/lang/String;

    .line 41
    iput-object p7, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    .line 42
    if-nez p4, :cond_2

    iget-object v0, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    .line 43
    invoke-virtual {v0}, Lcom/stripe/android/net/PollingParameters;->getDefaultTimeoutMs()J

    move-result-wide v0

    .line 44
    :goto_0
    iput-wide v0, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mTimeOutMs:J

    .line 46
    if-nez p5, :cond_0

    invoke-static {}, Lcom/stripe/android/net/PollingSyncNetworkHandler;->generateSourceRetriever()Lcom/stripe/android/net/SourceRetriever;

    move-result-object p5

    :cond_0
    iput-object p5, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mSourceRetriever:Lcom/stripe/android/net/SourceRetriever;

    .line 47
    if-nez p6, :cond_1

    invoke-static {}, Lcom/stripe/android/net/PollingSyncNetworkHandler;->generateTimeRetriever()Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;

    move-result-object p6

    :cond_1
    iput-object p6, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mTimeRetriever:Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;

    .line 48
    return-void

    .line 44
    :cond_2
    invoke-virtual {p4}, Ljava/lang/Integer;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    invoke-virtual {v2}, Lcom/stripe/android/net/PollingParameters;->getMaxTimeoutMs()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method private static generateSourceRetriever()Lcom/stripe/android/net/SourceRetriever;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/stripe/android/net/PollingSyncNetworkHandler$1;

    invoke-direct {v0}, Lcom/stripe/android/net/PollingSyncNetworkHandler$1;-><init>()V

    return-object v0
.end method

.method private static generateTimeRetriever()Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/stripe/android/net/PollingSyncNetworkHandler$2;

    invoke-direct {v0}, Lcom/stripe/android/net/PollingSyncNetworkHandler$2;-><init>()V

    return-object v0
.end method


# virtual methods
.method getTimeOutMs()J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mTimeOutMs:J

    return-wide v0
.end method

.method pollForSourceUpdate()Lcom/stripe/android/net/PollingResponse;
    .locals 15

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 52
    const/4 v2, 0x0

    .line 54
    iget-object v0, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    invoke-virtual {v0}, Lcom/stripe/android/net/PollingParameters;->getInitialDelayMs()J

    move-result-wide v0

    .line 55
    iget-object v4, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mTimeRetriever:Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;

    invoke-interface {v4}, Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;->getCurrentTimeInMillis()J

    move-result-wide v8

    move-object v4, v2

    move v2, v3

    .line 57
    :cond_0
    iget-object v5, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mTimeRetriever:Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;

    invoke-interface {v5}, Lcom/stripe/android/net/PollingSyncNetworkHandler$TimeRetriever;->getCurrentTimeInMillis()J

    move-result-wide v10

    sub-long/2addr v10, v8

    .line 58
    iget-wide v12, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mTimeOutMs:J

    cmp-long v5, v10, v12

    if-lez v5, :cond_1

    .line 105
    :goto_0
    new-instance v0, Lcom/stripe/android/net/PollingResponse;

    invoke-direct {v0, v4, v3, v6}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;ZZ)V

    :goto_1
    return-object v0

    .line 63
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mSourceRetriever:Lcom/stripe/android/net/SourceRetriever;

    iget-object v7, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mSourceId:Ljava/lang/String;

    iget-object v10, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mClientSecret:Ljava/lang/String;

    iget-object v11, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPublishableKey:Ljava/lang/String;

    invoke-interface {v5, v7, v10, v11}, Lcom/stripe/android/net/SourceRetriever;->retrieveSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/stripe/android/model/Source;
    :try_end_0
    .catch Lcom/stripe/android/exception/StripeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v5

    .line 67
    :try_start_1
    iget-object v4, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    invoke-virtual {v4}, Lcom/stripe/android/net/PollingParameters;->getInitialDelayMs()J
    :try_end_1
    .catch Lcom/stripe/android/exception/StripeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v0

    move v2, v3

    move-object v4, v5

    .line 79
    :goto_2
    if-eqz v4, :cond_3

    .line 80
    invoke-virtual {v4}, Lcom/stripe/android/model/Source;->getStatus()Ljava/lang/String;

    move-result-object v7

    const/4 v5, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v5, :pswitch_data_0

    .line 96
    :cond_3
    :try_start_2
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 97
    :try_start_3
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 98
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :goto_4
    if-eqz v4, :cond_0

    const-string v5, "pending"

    invoke-virtual {v4}, Lcom/stripe/android/model/Source;->getStatus()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v4

    .line 70
    :goto_5
    add-int/lit8 v2, v2, 0x1

    iget-object v7, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    invoke-virtual {v7}, Lcom/stripe/android/net/PollingParameters;->getMaxRetryCount()I

    move-result v7

    if-lt v2, v7, :cond_4

    .line 71
    new-instance v0, Lcom/stripe/android/net/PollingResponse;

    invoke-direct {v0, v5, v4}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;Lcom/stripe/android/exception/StripeException;)V

    goto :goto_1

    .line 73
    :cond_4
    iget-object v4, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    .line 75
    invoke-virtual {v4}, Lcom/stripe/android/net/PollingParameters;->getPollingMultiplier()I

    move-result v4

    int-to-long v10, v4

    mul-long/2addr v0, v10

    iget-object v4, p0, Lcom/stripe/android/net/PollingSyncNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    .line 76
    invoke-virtual {v4}, Lcom/stripe/android/net/PollingParameters;->getMaxDelayMs()J

    move-result-wide v10

    .line 74
    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    move-object v4, v5

    goto :goto_2

    .line 80
    :sswitch_0
    const-string v10, "chargeable"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v5, v3

    goto :goto_3

    :sswitch_1
    const-string v10, "consumed"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v5, v6

    goto :goto_3

    :sswitch_2
    const-string v10, "canceled"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v5, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "failed"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v5, 0x3

    goto :goto_3

    .line 82
    :pswitch_0
    new-instance v0, Lcom/stripe/android/net/PollingResponse;

    invoke-direct {v0, v4, v6, v3}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;ZZ)V

    goto/16 :goto_1

    .line 84
    :pswitch_1
    new-instance v0, Lcom/stripe/android/net/PollingResponse;

    invoke-direct {v0, v4, v6, v3}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;ZZ)V

    goto/16 :goto_1

    .line 86
    :pswitch_2
    new-instance v0, Lcom/stripe/android/net/PollingResponse;

    invoke-direct {v0, v4, v3, v3}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;ZZ)V

    goto/16 :goto_1

    .line 88
    :pswitch_3
    new-instance v0, Lcom/stripe/android/net/PollingResponse;

    invoke-direct {v0, v4, v3, v3}, Lcom/stripe/android/net/PollingResponse;-><init>(Lcom/stripe/android/model/Source;ZZ)V

    goto/16 :goto_1

    .line 98
    :catchall_0
    move-exception v5

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v5
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1

    .line 99
    :catch_1
    move-exception v5

    goto :goto_4

    .line 69
    :catch_2
    move-exception v5

    move-object v14, v5

    move-object v5, v4

    move-object v4, v14

    goto :goto_5

    .line 80
    :sswitch_data_0
    .sparse-switch
        -0x4c696bc3 -> :sswitch_3
        -0x21d77c18 -> :sswitch_1
        -0x7577b67 -> :sswitch_2
        0x548c3e0e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
