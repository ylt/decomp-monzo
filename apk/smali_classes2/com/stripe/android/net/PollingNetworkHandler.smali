.class Lcom/stripe/android/net/PollingNetworkHandler;
.super Ljava/lang/Object;
.source "PollingNetworkHandler.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final ERROR:I = -0x1

.field private static final EXPIRED:I = -0x2

.field private static final FAILURE:I = 0x3

.field private static final PENDING:I = 0x2

.field private static final SUCCESS:I = 0x1


# instance fields
.field private final mClientSecret:Ljava/lang/String;

.field private final mIsInSingleThreadMode:Z

.field private mLatestRetrievedSource:Lcom/stripe/android/model/Source;

.field private mNetworkHandler:Landroid/os/Handler;

.field private final mPollingParameters:Lcom/stripe/android/net/PollingParameters;

.field private final mPublishableKey:Ljava/lang/String;

.field private mRetryCount:I

.field private final mSourceId:Ljava/lang/String;

.field private mSourceRetriever:Lcom/stripe/android/net/SourceRetriever;

.field private final mTimeoutMs:J

.field private mUiHandler:Landroid/os/Handler;

.field private final pollRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stripe/android/net/PollingResponseHandler;Ljava/lang/Integer;Lcom/stripe/android/net/SourceRetriever;Lcom/stripe/android/net/PollingParameters;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/stripe/android/net/PollingNetworkHandler$1;

    invoke-direct {v0, p0}, Lcom/stripe/android/net/PollingNetworkHandler$1;-><init>(Lcom/stripe/android/net/PollingNetworkHandler;)V

    iput-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->pollRunnable:Ljava/lang/Runnable;

    .line 91
    iput-object p1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mSourceId:Ljava/lang/String;

    .line 92
    iput-object p2, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mClientSecret:Ljava/lang/String;

    .line 93
    iput-object p3, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mPublishableKey:Ljava/lang/String;

    .line 94
    if-eqz p6, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mIsInSingleThreadMode:Z

    .line 95
    iput-object p7, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    .line 96
    if-nez p6, :cond_0

    new-instance p6, Lcom/stripe/android/net/PollingNetworkHandler$2;

    invoke-direct {p6, p0}, Lcom/stripe/android/net/PollingNetworkHandler$2;-><init>(Lcom/stripe/android/net/PollingNetworkHandler;)V

    :cond_0
    iput-object p6, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mSourceRetriever:Lcom/stripe/android/net/SourceRetriever;

    .line 111
    if-nez p5, :cond_3

    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    .line 112
    invoke-virtual {v0}, Lcom/stripe/android/net/PollingParameters;->getDefaultTimeoutMs()J

    move-result-wide v2

    .line 113
    :goto_1
    iput-wide v2, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mTimeoutMs:J

    .line 115
    iput v1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mRetryCount:I

    .line 117
    new-instance v0, Lcom/stripe/android/net/PollingNetworkHandler$3;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p4}, Lcom/stripe/android/net/PollingNetworkHandler$3;-><init>(Lcom/stripe/android/net/PollingNetworkHandler;Landroid/os/Looper;Lcom/stripe/android/net/PollingResponseHandler;)V

    iput-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mUiHandler:Landroid/os/Handler;

    .line 174
    const/4 v0, 0x0

    .line 175
    iget-boolean v1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mIsInSingleThreadMode:Z

    if-nez v1, :cond_1

    .line 176
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Network Handler Thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 180
    :cond_1
    iget-boolean v1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mIsInSingleThreadMode:Z

    if-eqz v1, :cond_4

    .line 181
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 183
    :goto_2
    new-instance v1, Lcom/stripe/android/net/PollingNetworkHandler$4;

    invoke-direct {v1, p0, v0, v0}, Lcom/stripe/android/net/PollingNetworkHandler$4;-><init>(Lcom/stripe/android/net/PollingNetworkHandler;Landroid/os/Looper;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mNetworkHandler:Landroid/os/Handler;

    .line 207
    return-void

    :cond_2
    move v0, v1

    .line 94
    goto :goto_0

    .line 113
    :cond_3
    invoke-virtual {p5}, Ljava/lang/Integer;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    invoke-virtual {v0}, Lcom/stripe/android/net/PollingParameters;->getMaxTimeoutMs()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    goto :goto_1

    .line 182
    :cond_4
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mSourceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mClientSecret:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/stripe/android/net/PollingNetworkHandler;)Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mIsInSingleThreadMode:Z

    return v0
.end method

.method static synthetic access$200(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mPublishableKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/net/SourceRetriever;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mSourceRetriever:Lcom/stripe/android/net/SourceRetriever;

    return-object v0
.end method

.method static synthetic access$400(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/model/Source;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mLatestRetrievedSource:Lcom/stripe/android/model/Source;

    return-object v0
.end method

.method static synthetic access$402(Lcom/stripe/android/net/PollingNetworkHandler;Lcom/stripe/android/model/Source;)Lcom/stripe/android/model/Source;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mLatestRetrievedSource:Lcom/stripe/android/model/Source;

    return-object p1
.end method

.method static synthetic access$500(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/stripe/android/net/PollingNetworkHandler;)Lcom/stripe/android/net/PollingParameters;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mPollingParameters:Lcom/stripe/android/net/PollingParameters;

    return-object v0
.end method

.method static synthetic access$700(Lcom/stripe/android/net/PollingNetworkHandler;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mRetryCount:I

    return v0
.end method

.method static synthetic access$702(Lcom/stripe/android/net/PollingNetworkHandler;I)I
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mRetryCount:I

    return p1
.end method

.method static synthetic access$708(Lcom/stripe/android/net/PollingNetworkHandler;)I
    .locals 2

    .prologue
    .line 21
    iget v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mRetryCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mRetryCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/stripe/android/net/PollingNetworkHandler;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mNetworkHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/stripe/android/net/PollingNetworkHandler;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->pollRunnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method getRetryCount()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mRetryCount:I

    return v0
.end method

.method getTimeoutMs()J
    .locals 2

    .prologue
    .line 211
    iget-wide v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mTimeoutMs:J

    return-wide v0
.end method

.method setSourceRetriever(Lcom/stripe/android/net/SourceRetriever;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mSourceRetriever:Lcom/stripe/android/net/SourceRetriever;

    .line 222
    return-void
.end method

.method start()V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 225
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mNetworkHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/stripe/android/net/PollingNetworkHandler;->pollRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 226
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mNetworkHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mTimeoutMs:J

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 227
    iget-object v0, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mUiHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/stripe/android/net/PollingNetworkHandler;->mTimeoutMs:J

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 228
    return-void
.end method
