.class public Lcom/stripe/android/util/CardUtils;
.super Ljava/lang/Object;
.source "CardUtils.java"


# static fields
.field public static final CVC_LENGTH_AMEX:I = 0x4

.field public static final CVC_LENGTH_COMMON:I = 0x3

.field public static final LENGTH_AMERICAN_EXPRESS:I = 0xf

.field public static final LENGTH_COMMON_CARD:I = 0x10

.field public static final LENGTH_DINERS_CLUB:I = 0xe


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPossibleCardType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/stripe/android/util/CardUtils;->getPossibleCardType(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPossibleCardType(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    invoke-static {p0}, Lcom/stripe/android/util/StripeTextUtils;->isBlank(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "Unknown"

    .line 145
    :goto_0
    return-object v0

    .line 128
    :cond_0
    if-eqz p1, :cond_1

    .line 129
    invoke-static {p0}, Lcom/stripe/android/util/StripeTextUtils;->removeSpacesAndHyphens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 132
    :cond_1
    sget-object v0, Lcom/stripe/android/model/Card;->PREFIXES_AMERICAN_EXPRESS:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeTextUtils;->hasAnyPrefix(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    const-string v0, "American Express"

    goto :goto_0

    .line 134
    :cond_2
    sget-object v0, Lcom/stripe/android/model/Card;->PREFIXES_DISCOVER:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeTextUtils;->hasAnyPrefix(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135
    const-string v0, "Discover"

    goto :goto_0

    .line 136
    :cond_3
    sget-object v0, Lcom/stripe/android/model/Card;->PREFIXES_JCB:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeTextUtils;->hasAnyPrefix(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 137
    const-string v0, "JCB"

    goto :goto_0

    .line 138
    :cond_4
    sget-object v0, Lcom/stripe/android/model/Card;->PREFIXES_DINERS_CLUB:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeTextUtils;->hasAnyPrefix(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 139
    const-string v0, "Diners Club"

    goto :goto_0

    .line 140
    :cond_5
    sget-object v0, Lcom/stripe/android/model/Card;->PREFIXES_VISA:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeTextUtils;->hasAnyPrefix(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 141
    const-string v0, "Visa"

    goto :goto_0

    .line 142
    :cond_6
    sget-object v0, Lcom/stripe/android/model/Card;->PREFIXES_MASTERCARD:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/stripe/android/util/StripeTextUtils;->hasAnyPrefix(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 143
    const-string v0, "MasterCard"

    goto :goto_0

    .line 145
    :cond_7
    const-string v0, "Unknown"

    goto :goto_0
.end method

.method public static isValidCardLength(Ljava/lang/String;)Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    if-nez p0, :cond_0

    .line 76
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, v0}, Lcom/stripe/android/util/CardUtils;->getPossibleCardType(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/stripe/android/util/CardUtils;->isValidCardLength(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isValidCardLength(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    if-eqz p0, :cond_0

    const-string v2, "Unknown"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 101
    :cond_1
    :goto_0
    return v0

    .line 94
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 95
    const/4 v2, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 101
    const/16 v2, 0x10

    if-eq v3, v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 95
    :sswitch_0
    const-string v4, "American Express"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v1

    goto :goto_1

    :sswitch_1
    const-string v4, "Diners Club"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v0

    goto :goto_1

    .line 97
    :pswitch_0
    const/16 v2, 0xf

    if-eq v3, v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 99
    :pswitch_1
    const/16 v2, 0xe

    if-eq v3, v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 95
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3b3bfd47 -> :sswitch_1
        -0x11ceb490 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isValidCardNumber(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 23
    invoke-static {p0}, Lcom/stripe/android/util/StripeTextUtils;->removeSpacesAndHyphens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-static {v0}, Lcom/stripe/android/util/CardUtils;->isValidLuhnNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/stripe/android/util/CardUtils;->isValidCardLength(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidLuhnNumber(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    if-nez p0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v2

    .line 41
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move v5, v2

    move v3, v1

    :goto_1
    if-ltz v4, :cond_5

    .line 42
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 43
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 47
    invoke-static {v0}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v0

    .line 48
    if-nez v3, :cond_4

    move v3, v1

    .line 50
    :goto_2
    if-eqz v3, :cond_2

    .line 51
    mul-int/lit8 v0, v0, 0x2

    .line 54
    :cond_2
    const/16 v6, 0x9

    if-le v0, v6, :cond_3

    .line 55
    add-int/lit8 v0, v0, -0x9

    .line 58
    :cond_3
    add-int/2addr v5, v0

    .line 41
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_1

    :cond_4
    move v3, v2

    .line 48
    goto :goto_2

    .line 61
    :cond_5
    rem-int/lit8 v0, v5, 0xa

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    move v2, v0

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_3
.end method

.method public static separateCardNumberGroups(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/16 v0, 0xa

    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 164
    const-string v1, "American Express"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 165
    new-array v3, v6, [Ljava/lang/String;

    .line 167
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 169
    if-le v5, v4, :cond_5

    .line 170
    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    move v1, v4

    .line 174
    :goto_0
    if-le v5, v0, :cond_4

    .line 175
    const/4 v1, 0x1

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 179
    :goto_1
    if-ge v2, v6, :cond_1

    .line 180
    aget-object v1, v3, v2

    if-eqz v1, :cond_0

    .line 179
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 183
    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    :cond_1
    move-object v0, v3

    .line 201
    :goto_2
    return-object v0

    .line 188
    :cond_2
    new-array v1, v4, [Ljava/lang/String;

    move v0, v2

    .line 191
    :goto_3
    add-int/lit8 v3, v2, 0x1

    mul-int/lit8 v3, v3, 0x4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 192
    add-int/lit8 v3, v2, 0x1

    mul-int/lit8 v3, v3, 0x4

    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 193
    aput-object v0, v1, v2

    .line 194
    add-int/lit8 v0, v2, 0x1

    mul-int/lit8 v0, v0, 0x4

    .line 195
    add-int/lit8 v2, v2, 0x1

    .line 196
    goto :goto_3

    .line 199
    :cond_3
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    move-object v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_0
.end method
