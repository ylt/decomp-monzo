.class public Lcom/stripe/android/util/LoggingUtils;
.super Ljava/lang/Object;
.source "LoggingUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stripe/android/util/LoggingUtils$LoggingEventName;,
        Lcom/stripe/android/util/LoggingUtils$LoggingToken;
    }
.end annotation


# static fields
.field private static final ANALYTICS_NAME:Ljava/lang/String; = "stripe_android"

.field private static final ANALYTICS_PREFIX:Ljava/lang/String; = "analytics"

.field private static final ANALYTICS_VERSION:Ljava/lang/String; = "1.0"

.field public static final ANDROID_PAY_TOKEN:Ljava/lang/String; = "AndroidPay"

.field public static final CARD_WIDGET_TOKEN:Ljava/lang/String; = "CardInputView"

.field public static final EVENT_SOURCE_CREATION:Ljava/lang/String; = "source_creation"

.field public static final EVENT_TOKEN_CREATION:Ljava/lang/String; = "token_creation"

.field static final FIELD_ANALYTICS_UA:Ljava/lang/String; = "analytics_ua"

.field static final FIELD_BINDINGS_VERSION:Ljava/lang/String; = "bindings_version"

.field static final FIELD_DEVICE_TYPE:Ljava/lang/String; = "device_type"

.field static final FIELD_EVENT:Ljava/lang/String; = "event"

.field static final FIELD_OS_NAME:Ljava/lang/String; = "os_name"

.field static final FIELD_OS_RELEASE:Ljava/lang/String; = "os_release"

.field static final FIELD_OS_VERSION:Ljava/lang/String; = "os_version"

.field public static final FIELD_PRODUCT_USAGE:Ljava/lang/String; = "product_usage"

.field static final FIELD_PUBLISHABLE_KEY:Ljava/lang/String; = "publishable_key"

.field static final FIELD_SOURCE_TYPE:Ljava/lang/String; = "source_type"

.field static final FIELD_TOKEN_TYPE:Ljava/lang/String; = "token_type"

.field public static final PII_TOKEN:Ljava/lang/String; = "PII"

.field public static final VALID_LOGGING_TOKENS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final VALID_PARAM_FIELDS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_LOGGING_TOKENS:Ljava/util/Set;

    .line 37
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_LOGGING_TOKENS:Ljava/util/Set;

    const-string v1, "AndroidPay"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_LOGGING_TOKENS:Ljava/util/Set;

    const-string v1, "CardInputView"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    .line 63
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "analytics_ua"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "bindings_version"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "device_type"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 66
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "event"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "os_version"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 68
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "os_name"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 69
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "os_release"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "product_usage"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 71
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "publishable_key"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 72
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "source_type"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/stripe/android/util/LoggingUtils;->VALID_PARAM_FIELDS:Ljava/util/Set;

    const-string v1, "token_type"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getAnalyticsUa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    const-string v0, "analytics.stripe_android-1.0"

    return-object v0
.end method

.method static getDeviceLoggingString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x5f

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 144
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 145
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getEventLoggingParams(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 113
    const-string v1, "analytics_ua"

    invoke-static {}, Lcom/stripe/android/util/LoggingUtils;->getAnalyticsUa()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    const-string v1, "event"

    invoke-static {p4}, Lcom/stripe/android/util/LoggingUtils;->getEventParamName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    const-string v1, "publishable_key"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const-string v1, "os_name"

    sget-object v2, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-string v1, "os_release"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    const-string v1, "os_version"

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    const-string v1, "device_type"

    invoke-static {}, Lcom/stripe/android/util/LoggingUtils;->getDeviceLoggingString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    const-string v1, "bindings_version"

    const-string v2, "4.1.3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    if-eqz p0, :cond_0

    .line 122
    const-string v1, "product_usage"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_0
    if-eqz p1, :cond_1

    .line 126
    const-string v1, "source_type"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    :cond_1
    if-eqz p2, :cond_3

    .line 130
    const-string v1, "token_type"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    :cond_2
    :goto_0
    return-object v0

    .line 131
    :cond_3
    if-nez p1, :cond_2

    .line 134
    const-string v1, "token_type"

    const-string v2, "unknown"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static getEventParamName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "stripe_android."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSourceCreationParams(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 97
    const-string v0, "source_creation"

    invoke-static {v1, p1, v1, p0, v0}, Lcom/stripe/android/util/LoggingUtils;->getEventLoggingParams(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static getTokenCreationParams(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    const/4 v0, 0x0

    const-string v1, "token_creation"

    invoke-static {p0, v0, p2, p1, v1}, Lcom/stripe/android/util/LoggingUtils;->getEventLoggingParams(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
