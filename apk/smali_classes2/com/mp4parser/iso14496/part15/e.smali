.class public Lcom/mp4parser/iso14496/part15/e;
.super Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;
.source "TemporalLayerSampleGroup.java"


# instance fields
.field a:I

.field b:I

.field c:Z

.field d:I

.field e:J

.field f:J

.field g:I

.field h:I

.field i:I

.field j:I

.field k:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string v0, "tscl"

    return-object v0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 122
    invoke-static {p1}, Lcom/coremedia/iso/e;->d(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lcom/mp4parser/iso14496/part15/e;->a:I

    .line 123
    invoke-static {p1}, Lcom/coremedia/iso/e;->d(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 124
    and-int/lit16 v0, v1, 0xc0

    shr-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/mp4parser/iso14496/part15/e;->b:I

    .line 125
    and-int/lit8 v0, v1, 0x20

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/mp4parser/iso14496/part15/e;->c:Z

    .line 126
    and-int/lit8 v0, v1, 0x1f

    iput v0, p0, Lcom/mp4parser/iso14496/part15/e;->d:I

    .line 127
    invoke-static {p1}, Lcom/coremedia/iso/e;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mp4parser/iso14496/part15/e;->e:J

    .line 128
    invoke-static {p1}, Lcom/coremedia/iso/e;->l(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mp4parser/iso14496/part15/e;->f:J

    .line 129
    invoke-static {p1}, Lcom/coremedia/iso/e;->d(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lcom/mp4parser/iso14496/part15/e;->g:I

    .line 130
    invoke-static {p1}, Lcom/coremedia/iso/e;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lcom/mp4parser/iso14496/part15/e;->h:I

    .line 131
    invoke-static {p1}, Lcom/coremedia/iso/e;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lcom/mp4parser/iso14496/part15/e;->i:I

    .line 132
    invoke-static {p1}, Lcom/coremedia/iso/e;->d(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lcom/mp4parser/iso14496/part15/e;->j:I

    .line 133
    invoke-static {p1}, Lcom/coremedia/iso/e;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lcom/mp4parser/iso14496/part15/e;->k:I

    .line 134
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/nio/ByteBuffer;
    .locals 4

    .prologue
    .line 138
    const/16 v0, 0x14

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 140
    iget v0, p0, Lcom/mp4parser/iso14496/part15/e;->a:I

    invoke-static {v1, v0}, Lcom/coremedia/iso/g;->c(Ljava/nio/ByteBuffer;I)V

    .line 141
    iget v0, p0, Lcom/mp4parser/iso14496/part15/e;->b:I

    shl-int/lit8 v2, v0, 0x6

    iget-boolean v0, p0, Lcom/mp4parser/iso14496/part15/e;->c:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    :goto_0
    add-int/2addr v0, v2

    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->d:I

    add-int/2addr v0, v2

    invoke-static {v1, v0}, Lcom/coremedia/iso/g;->c(Ljava/nio/ByteBuffer;I)V

    .line 143
    iget-wide v2, p0, Lcom/mp4parser/iso14496/part15/e;->e:J

    invoke-static {v1, v2, v3}, Lcom/coremedia/iso/g;->b(Ljava/nio/ByteBuffer;J)V

    .line 144
    iget-wide v2, p0, Lcom/mp4parser/iso14496/part15/e;->f:J

    invoke-static {v1, v2, v3}, Lcom/coremedia/iso/g;->c(Ljava/nio/ByteBuffer;J)V

    .line 145
    iget v0, p0, Lcom/mp4parser/iso14496/part15/e;->g:I

    invoke-static {v1, v0}, Lcom/coremedia/iso/g;->c(Ljava/nio/ByteBuffer;I)V

    .line 146
    iget v0, p0, Lcom/mp4parser/iso14496/part15/e;->h:I

    invoke-static {v1, v0}, Lcom/coremedia/iso/g;->b(Ljava/nio/ByteBuffer;I)V

    .line 147
    iget v0, p0, Lcom/mp4parser/iso14496/part15/e;->i:I

    invoke-static {v1, v0}, Lcom/coremedia/iso/g;->b(Ljava/nio/ByteBuffer;I)V

    .line 148
    iget v0, p0, Lcom/mp4parser/iso14496/part15/e;->j:I

    invoke-static {v1, v0}, Lcom/coremedia/iso/g;->c(Ljava/nio/ByteBuffer;I)V

    .line 149
    iget v0, p0, Lcom/mp4parser/iso14496/part15/e;->k:I

    invoke-static {v1, v0}, Lcom/coremedia/iso/g;->b(Ljava/nio/ByteBuffer;I)V

    .line 150
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    return-object v0

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 155
    const/16 v0, 0x14

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 161
    if-ne p0, p1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v0

    .line 162
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 164
    :cond_3
    check-cast p1, Lcom/mp4parser/iso14496/part15/e;

    .line 166
    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->a:I

    iget v3, p1, Lcom/mp4parser/iso14496/part15/e;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 167
    :cond_4
    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->i:I

    iget v3, p1, Lcom/mp4parser/iso14496/part15/e;->i:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 168
    :cond_5
    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->k:I

    iget v3, p1, Lcom/mp4parser/iso14496/part15/e;->k:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 169
    :cond_6
    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->j:I

    iget v3, p1, Lcom/mp4parser/iso14496/part15/e;->j:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    .line 170
    :cond_7
    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->h:I

    iget v3, p1, Lcom/mp4parser/iso14496/part15/e;->h:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    .line 171
    :cond_8
    iget-wide v2, p0, Lcom/mp4parser/iso14496/part15/e;->f:J

    iget-wide v4, p1, Lcom/mp4parser/iso14496/part15/e;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    .line 172
    :cond_9
    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->g:I

    iget v3, p1, Lcom/mp4parser/iso14496/part15/e;->g:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    .line 173
    :cond_a
    iget-wide v2, p0, Lcom/mp4parser/iso14496/part15/e;->e:J

    iget-wide v4, p1, Lcom/mp4parser/iso14496/part15/e;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    .line 174
    :cond_b
    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->d:I

    iget v3, p1, Lcom/mp4parser/iso14496/part15/e;->d:I

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    .line 175
    :cond_c
    iget v2, p0, Lcom/mp4parser/iso14496/part15/e;->b:I

    iget v3, p1, Lcom/mp4parser/iso14496/part15/e;->b:I

    if-eq v2, v3, :cond_d

    move v0, v1

    goto :goto_0

    .line 176
    :cond_d
    iget-boolean v2, p0, Lcom/mp4parser/iso14496/part15/e;->c:Z

    iget-boolean v3, p1, Lcom/mp4parser/iso14496/part15/e;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 183
    iget v0, p0, Lcom/mp4parser/iso14496/part15/e;->a:I

    .line 184
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->b:I

    add-int/2addr v0, v1

    .line 185
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/mp4parser/iso14496/part15/e;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 186
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->d:I

    add-int/2addr v0, v1

    .line 187
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/mp4parser/iso14496/part15/e;->e:J

    iget-wide v4, p0, Lcom/mp4parser/iso14496/part15/e;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 188
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/mp4parser/iso14496/part15/e;->f:J

    iget-wide v4, p0, Lcom/mp4parser/iso14496/part15/e;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 189
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->g:I

    add-int/2addr v0, v1

    .line 190
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->h:I

    add-int/2addr v0, v1

    .line 191
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->i:I

    add-int/2addr v0, v1

    .line 192
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->j:I

    add-int/2addr v0, v1

    .line 193
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->k:I

    add-int/2addr v0, v1

    .line 194
    return v0

    .line 185
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 199
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TemporalLayerSampleGroup{temporalLayerId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 200
    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 201
    const-string v1, ", tlprofile_space="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 202
    const-string v1, ", tltier_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mp4parser/iso14496/part15/e;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 203
    const-string v1, ", tlprofile_idc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 204
    const-string v1, ", tlprofile_compatibility_flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/mp4parser/iso14496/part15/e;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 205
    const-string v1, ", tlconstraint_indicator_flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/mp4parser/iso14496/part15/e;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 206
    const-string v1, ", tllevel_idc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 207
    const-string v1, ", tlMaxBitRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 208
    const-string v1, ", tlAvgBitRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 209
    const-string v1, ", tlConstantFrameRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 210
    const-string v1, ", tlAvgFrameRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mp4parser/iso14496/part15/e;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 211
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
