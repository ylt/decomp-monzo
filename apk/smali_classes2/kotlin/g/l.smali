.class public final Lkotlin/g/l;
.super Ljava/lang/Object;
.source "Sequences.kt"

# interfaces
.implements Lkotlin/g/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/g/g",
        "<TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010(\n\u0002\u0008\u0002\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0004\u0008\u0001\u0010\u00022\u0008\u0012\u0004\u0012\u0002H\u00020\u0003B\'\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0003\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J3\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\t0\u0003\"\u0004\u0008\u0002\u0010\t2\u0018\u0010\n\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\t0\u000b0\u0006H\u0000\u00a2\u0006\u0002\u0008\u000cJ\u000f\u0010\n\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u000bH\u0096\u0002R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lkotlin/sequences/TransformingSequence;",
        "T",
        "R",
        "Lkotlin/sequences/Sequence;",
        "sequence",
        "transformer",
        "Lkotlin/Function1;",
        "(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)V",
        "flatten",
        "E",
        "iterator",
        "",
        "flatten$kotlin_stdlib",
        "kotlin-stdlib"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lkotlin/g/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/g/g",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<TT;TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/g/g;Lkotlin/d/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/g/g",
            "<+TT;>;",
            "Lkotlin/d/a/b",
            "<-TT;+TR;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "sequence"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transformer"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/g/l;->a:Lkotlin/g/g;

    iput-object p2, p0, Lkotlin/g/l;->b:Lkotlin/d/a/b;

    return-void
.end method

.method public static final synthetic a(Lkotlin/g/l;)Lkotlin/d/a/b;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lkotlin/g/l;->b:Lkotlin/d/a/b;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/g/l;)Lkotlin/g/g;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lkotlin/g/l;->a:Lkotlin/g/g;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 145
    new-instance v0, Lkotlin/g/l$a;

    invoke-direct {v0, p0}, Lkotlin/g/l$a;-><init>(Lkotlin/g/l;)V

    check-cast v0, Ljava/util/Iterator;

    .line 154
    return-object v0
.end method

.method public final a(Lkotlin/d/a/b;)Lkotlin/g/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/d/a/b",
            "<-TR;+",
            "Ljava/util/Iterator",
            "<+TE;>;>;)",
            "Lkotlin/g/g",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const-string v0, "iterator"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    new-instance v0, Lkotlin/g/e;

    iget-object v1, p0, Lkotlin/g/l;->a:Lkotlin/g/g;

    iget-object v2, p0, Lkotlin/g/l;->b:Lkotlin/d/a/b;

    invoke-direct {v0, v1, v2, p1}, Lkotlin/g/e;-><init>(Lkotlin/g/g;Lkotlin/d/a/b;Lkotlin/d/a/b;)V

    check-cast v0, Lkotlin/g/g;

    return-object v0
.end method
