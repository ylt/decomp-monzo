.class final Lkotlin/g/c;
.super Ljava/lang/Object;
.source "Sequences.kt"

# interfaces
.implements Lkotlin/g/b;
.implements Lkotlin/g/g;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010(\n\u0002\u0008\u0002\u0008\u00c2\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00020\u0003B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u000f\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00020\tH\u0096\u0002J\u0010\u0010\n\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lkotlin/sequences/EmptySequence;",
        "Lkotlin/sequences/Sequence;",
        "",
        "Lkotlin/sequences/DropTakeSequence;",
        "()V",
        "drop",
        "n",
        "",
        "iterator",
        "",
        "take",
        "kotlin-stdlib"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lkotlin/g/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lkotlin/g/c;

    invoke-direct {v0}, Lkotlin/g/c;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/g/c;

    sput-object p0, Lkotlin/g/c;->a:Lkotlin/g/c;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lkotlin/a/t;->a:Lkotlin/a/t;

    check-cast v0, Ljava/util/Iterator;

    return-object v0
.end method

.method public synthetic a(I)Lkotlin/g/g;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lkotlin/g/c;->b(I)Lkotlin/g/c;

    move-result-object v0

    check-cast v0, Lkotlin/g/g;

    return-object v0
.end method

.method public b(I)Lkotlin/g/c;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lkotlin/g/c;->a:Lkotlin/g/c;

    return-object v0
.end method
