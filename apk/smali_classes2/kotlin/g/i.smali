.class Lkotlin/g/i;
.super Ljava/lang/Object;
.source "Sequences.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u001c\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u001a+\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u0014\u0008\u0004\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\u00050\u0004H\u0087\u0008\u001a\u0012\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\u001a&\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00082\u000e\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0004\u001a<\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00082\u000e\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u00042\u0014\u0010\t\u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u000b\u001a=\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00082\u0008\u0010\u000c\u001a\u0004\u0018\u0001H\u00022\u0014\u0010\t\u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u000bH\u0007\u00a2\u0006\u0002\u0010\r\u001a+\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u0012\u0010\u000f\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00020\u0010\"\u0002H\u0002\u00a2\u0006\u0002\u0010\u0011\u001a\u001f\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u0013H\u0087\u0008\u001a\u001c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u0005\u001a\u001c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\u001aC\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0016*\u0008\u0012\u0004\u0012\u0002H\u00020\u00012\u0018\u0010\u0003\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00160\u00050\u000bH\u0002\u00a2\u0006\u0002\u0008\u0017\u001a)\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\u00180\u0001H\u0007\u00a2\u0006\u0002\u0008\u0019\u001a\"\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\u00010\u0001\u001a@\u0010\u001a\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\u001c\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00160\u001c0\u001b\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0016*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00160\u001b0\u0001\u00a8\u0006\u001d"
    }
    d2 = {
        "Sequence",
        "Lkotlin/sequences/Sequence;",
        "T",
        "iterator",
        "Lkotlin/Function0;",
        "",
        "emptySequence",
        "generateSequence",
        "",
        "nextFunction",
        "seedFunction",
        "Lkotlin/Function1;",
        "seed",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;",
        "sequenceOf",
        "elements",
        "",
        "([Ljava/lang/Object;)Lkotlin/sequences/Sequence;",
        "asSequence",
        "Ljava/util/Enumeration;",
        "constrainOnce",
        "flatten",
        "R",
        "flatten$SequencesKt__SequencesKt",
        "",
        "flattenSequenceOfIterable",
        "unzip",
        "Lkotlin/Pair;",
        "",
        "kotlin-stdlib"
    }
    k = 0x5
    mv = {
        0x1,
        0x1,
        0x7
    }
    xi = 0x1
    xs = "kotlin/sequences/SequencesKt"
.end annotation


# direct methods
.method public static final a()Lkotlin/g/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lkotlin/g/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 42
    sget-object v0, Lkotlin/g/c;->a:Lkotlin/g/c;

    check-cast v0, Lkotlin/g/g;

    return-object v0
.end method

.method public static final a(Ljava/lang/Object;Lkotlin/d/a/b;)Lkotlin/g/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lkotlin/d/a/b",
            "<-TT;+TT;>;)",
            "Lkotlin/g/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "nextFunction"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 596
    if-nez p0, :cond_0

    .line 597
    sget-object v0, Lkotlin/g/c;->a:Lkotlin/g/c;

    check-cast v0, Lkotlin/g/g;

    .line 596
    :goto_0
    return-object v0

    .line 599
    :cond_0
    new-instance v1, Lkotlin/g/f;

    new-instance v0, Lkotlin/g/i$c;

    invoke-direct {v0, p0}, Lkotlin/g/i$c;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v1, v0, p1}, Lkotlin/g/f;-><init>(Lkotlin/d/a/a;Lkotlin/d/a/b;)V

    move-object v0, v1

    check-cast v0, Lkotlin/g/g;

    goto :goto_0
.end method

.method public static final a(Lkotlin/g/g;)Lkotlin/g/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/g/g",
            "<+",
            "Lkotlin/g/g",
            "<+TT;>;>;)",
            "Lkotlin/g/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    sget-object v0, Lkotlin/g/i$a;->a:Lkotlin/g/i$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p0, v0}, Lkotlin/g/i;->a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    return-object v0
.end method

.method private static final a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/g/g",
            "<+TT;>;",
            "Lkotlin/d/a/b",
            "<-TT;+",
            "Ljava/util/Iterator",
            "<+TR;>;>;)",
            "Lkotlin/g/g",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 66
    instance-of v0, p0, Lkotlin/g/l;

    if-eqz v0, :cond_0

    .line 67
    check-cast p0, Lkotlin/g/l;

    invoke-virtual {p0, p1}, Lkotlin/g/l;->a(Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    .line 69
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lkotlin/g/e;

    sget-object v0, Lkotlin/g/i$b;->a:Lkotlin/g/i$b;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v1, p0, v0, p1}, Lkotlin/g/e;-><init>(Lkotlin/g/g;Lkotlin/d/a/b;Lkotlin/d/a/b;)V

    move-object v0, v1

    check-cast v0, Lkotlin/g/g;

    goto :goto_0
.end method

.method public static final varargs a([Ljava/lang/Object;)Lkotlin/g/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Lkotlin/g/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "elements"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    array-length v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lkotlin/g/h;->a()Lkotlin/g/g;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lkotlin/a/g;->l([Ljava/lang/Object;)Lkotlin/g/g;

    move-result-object v0

    goto :goto_1
.end method
