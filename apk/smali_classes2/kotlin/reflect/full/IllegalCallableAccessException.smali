.class public final Lkotlin/reflect/full/IllegalCallableAccessException;
.super Lkotlin/reflect/IllegalCallableAccessException;
.source "exceptions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lkotlin/reflect/full/IllegalCallableAccessException;",
        "Lkotlin/reflect/IllegalCallableAccessException;",
        "cause",
        "Ljava/lang/IllegalAccessException;",
        "(Ljava/lang/IllegalAccessException;)V",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/IllegalAccessException;)V
    .locals 1

    .prologue
    const-string v0, "cause"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p1}, Lkotlin/reflect/IllegalCallableAccessException;-><init>(Ljava/lang/IllegalAccessException;)V

    return-void
.end method
