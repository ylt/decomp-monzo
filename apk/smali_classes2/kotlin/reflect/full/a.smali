.class public final Lkotlin/reflect/full/a;
.super Ljava/lang/Object;
.source "KClasses.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000Z\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0016\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\r\u001a+\u0010S\u001a\u0002H\u001d\"\u0008\u0008\u0000\u0010\u001d*\u00020\u0010*\u0008\u0012\u0004\u0012\u0002H\u001d0\u00022\u0008\u0010T\u001a\u0004\u0018\u00010\u0010H\u0007\u00a2\u0006\u0002\u0010U\u001a!\u0010V\u001a\u0002H\u001d\"\u0008\u0008\u0000\u0010\u001d*\u00020\u0010*\u0008\u0012\u0004\u0012\u0002H\u001d0\u0002H\u0007\u00a2\u0006\u0002\u0010\u0013\u001a\u001c\u0010W\u001a\u000203*\u0006\u0012\u0002\u0008\u00030\u00022\n\u0010X\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0007\u001a\u001c\u0010Y\u001a\u000203*\u0006\u0012\u0002\u0008\u00030\u00022\n\u0010Z\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0007\u001a-\u0010[\u001a\u0004\u0018\u0001H\u001d\"\u0008\u0008\u0000\u0010\u001d*\u00020\u0010*\u0008\u0012\u0004\u0012\u0002H\u001d0\u00022\u0008\u0010T\u001a\u0004\u0018\u00010\u0010H\u0007\u00a2\u0006\u0002\u0010U\",\u0010\u0000\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00020\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0003\u0010\u0004\u001a\u0004\u0008\u0005\u0010\u0006\"(\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\t\u0010\u0004\u001a\u0004\u0008\n\u0010\u0006\"(\u0010\u000b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0002*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u000c\u0010\u0004\u001a\u0004\u0008\r\u0010\u000e\"$\u0010\u000f\u001a\u0004\u0018\u00010\u0010*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0011\u0010\u0004\u001a\u0004\u0008\u0012\u0010\u0013\",\u0010\u0014\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00150\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0016\u0010\u0004\u001a\u0004\u0008\u0017\u0010\u0006\",\u0010\u0018\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00150\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0019\u0010\u0004\u001a\u0004\u0008\u001a\u0010\u0006\"B\u0010\u001b\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u0002H\u001d\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001c0\u0001\"\u0008\u0008\u0000\u0010\u001d*\u00020\u0010*\u0008\u0012\u0004\u0012\u0002H\u001d0\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u001e\u0010\u0004\u001a\u0004\u0008\u001f\u0010\u0006\",\u0010 \u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00150\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008!\u0010\u0004\u001a\u0004\u0008\"\u0010\u0006\">\u0010#\u001a\u0012\u0012\u000e\u0012\u000c\u0012\u0004\u0012\u0002H\u001d\u0012\u0002\u0008\u00030$0\u0001\"\u0008\u0008\u0000\u0010\u001d*\u00020\u0010*\u0008\u0012\u0004\u0012\u0002H\u001d0\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008%\u0010\u0004\u001a\u0004\u0008&\u0010\u0006\",\u0010\'\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030(0\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008)\u0010\u0004\u001a\u0004\u0008*\u0010\u0006\"\"\u0010+\u001a\u00020\u0008*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008,\u0010\u0004\u001a\u0004\u0008-\u0010.\",\u0010/\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00150\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u00080\u0010\u0004\u001a\u0004\u00081\u0010\u0006\"\u001c\u00102\u001a\u000203*\u0006\u0012\u0002\u0008\u0003048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00082\u00105\"\u001c\u00106\u001a\u000203*\u0006\u0012\u0002\u0008\u0003048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00086\u00105\",\u00107\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00150\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u00088\u0010\u0004\u001a\u0004\u00089\u0010\u0006\"B\u0010:\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u0002H\u001d\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001c0\u0001\"\u0008\u0008\u0000\u0010\u001d*\u00020\u0010*\u0008\u0012\u0004\u0012\u0002H\u001d0\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008;\u0010\u0004\u001a\u0004\u0008<\u0010\u0006\",\u0010=\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00150\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008>\u0010\u0004\u001a\u0004\u0008?\u0010\u0006\">\u0010@\u001a\u0012\u0012\u000e\u0012\u000c\u0012\u0004\u0012\u0002H\u001d\u0012\u0002\u0008\u00030$0\u0001\"\u0008\u0008\u0000\u0010\u001d*\u00020\u0010*\u0008\u0012\u0004\u0012\u0002H\u001d0\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008A\u0010\u0004\u001a\u0004\u0008B\u0010\u0006\"6\u0010C\u001a\n\u0012\u0004\u0012\u0002H\u001d\u0018\u00010\u0015\"\u0008\u0008\u0000\u0010\u001d*\u00020\u0010*\u0008\u0012\u0004\u0012\u0002H\u001d0\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008D\u0010\u0004\u001a\u0004\u0008E\u0010F\",\u0010G\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00150\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008H\u0010\u0004\u001a\u0004\u0008I\u0010\u0006\",\u0010J\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030K0\u0001*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008L\u0010\u0004\u001a\u0004\u0008M\u0010\u0006\",\u0010N\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00020O*\u0006\u0012\u0002\u0008\u00030\u00028FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008P\u0010\u0004\u001a\u0004\u0008Q\u0010R\u00a8\u0006\\"
    }
    d2 = {
        "allSuperclasses",
        "",
        "Lkotlin/reflect/KClass;",
        "allSuperclasses$annotations",
        "(Lkotlin/reflect/KClass;)V",
        "getAllSuperclasses",
        "(Lkotlin/reflect/KClass;)Ljava/util/Collection;",
        "allSupertypes",
        "Lkotlin/reflect/KType;",
        "allSupertypes$annotations",
        "getAllSupertypes",
        "companionObject",
        "companionObject$annotations",
        "getCompanionObject",
        "(Lkotlin/reflect/KClass;)Lkotlin/reflect/KClass;",
        "companionObjectInstance",
        "",
        "companionObjectInstance$annotations",
        "getCompanionObjectInstance",
        "(Lkotlin/reflect/KClass;)Ljava/lang/Object;",
        "declaredFunctions",
        "Lkotlin/reflect/KFunction;",
        "declaredFunctions$annotations",
        "getDeclaredFunctions",
        "declaredMemberExtensionFunctions",
        "declaredMemberExtensionFunctions$annotations",
        "getDeclaredMemberExtensionFunctions",
        "declaredMemberExtensionProperties",
        "Lkotlin/reflect/KProperty2;",
        "T",
        "declaredMemberExtensionProperties$annotations",
        "getDeclaredMemberExtensionProperties",
        "declaredMemberFunctions",
        "declaredMemberFunctions$annotations",
        "getDeclaredMemberFunctions",
        "declaredMemberProperties",
        "Lkotlin/reflect/KProperty1;",
        "declaredMemberProperties$annotations",
        "getDeclaredMemberProperties",
        "declaredMembers",
        "Lkotlin/reflect/KCallable;",
        "declaredMembers$annotations",
        "getDeclaredMembers",
        "defaultType",
        "defaultType$annotations",
        "getDefaultType",
        "(Lkotlin/reflect/KClass;)Lkotlin/reflect/KType;",
        "functions",
        "functions$annotations",
        "getFunctions",
        "isExtension",
        "",
        "Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "(Lkotlin/reflect/jvm/internal/KCallableImpl;)Z",
        "isNotExtension",
        "memberExtensionFunctions",
        "memberExtensionFunctions$annotations",
        "getMemberExtensionFunctions",
        "memberExtensionProperties",
        "memberExtensionProperties$annotations",
        "getMemberExtensionProperties",
        "memberFunctions",
        "memberFunctions$annotations",
        "getMemberFunctions",
        "memberProperties",
        "memberProperties$annotations",
        "getMemberProperties",
        "primaryConstructor",
        "primaryConstructor$annotations",
        "getPrimaryConstructor",
        "(Lkotlin/reflect/KClass;)Lkotlin/reflect/KFunction;",
        "staticFunctions",
        "staticFunctions$annotations",
        "getStaticFunctions",
        "staticProperties",
        "Lkotlin/reflect/KProperty0;",
        "staticProperties$annotations",
        "getStaticProperties",
        "superclasses",
        "",
        "superclasses$annotations",
        "getSuperclasses",
        "(Lkotlin/reflect/KClass;)Ljava/util/List;",
        "cast",
        "value",
        "(Lkotlin/reflect/KClass;Ljava/lang/Object;)Ljava/lang/Object;",
        "createInstance",
        "isSubclassOf",
        "base",
        "isSuperclassOf",
        "derived",
        "safeCast",
        "kotlin-reflection"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# direct methods
.method public static final a(Lkotlin/reflect/c;)Lkotlin/reflect/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/c",
            "<TT;>;)",
            "Lkotlin/reflect/f",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    if-nez p0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.reflect.jvm.internal.KClassImpl<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p0, Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->g()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 286
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/f;

    .line 41
    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.reflect.jvm.internal.KFunctionImpl"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ConstructorDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/l;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/l;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 287
    :goto_0
    check-cast v0, Lkotlin/reflect/f;

    .line 42
    return-object v0

    .line 287
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/j;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 174
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/j;->d()Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/c;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/c",
            "<TT;>;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/n",
            "<TT;*>;>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    if-nez p0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.reflect.jvm.internal.KClassImpl<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p0, Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->c()Lkotlin/reflect/jvm/internal/ae$b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/l$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l$a;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 332
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 333
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/j;

    .line 149
    invoke-static {v0}, Lkotlin/reflect/full/a;->b(Lkotlin/reflect/jvm/internal/j;)Z

    move-result v4

    if-eqz v4, :cond_2

    instance-of v0, v0, Lkotlin/reflect/n;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 334
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 149
    return-object v1
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/j;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 177
    invoke-static {p0}, Lkotlin/reflect/full/a;->a(Lkotlin/reflect/jvm/internal/j;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
