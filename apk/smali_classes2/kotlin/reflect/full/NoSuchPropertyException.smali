.class public final Lkotlin/reflect/full/NoSuchPropertyException;
.super Lkotlin/reflect/NoSuchPropertyException;
.source "exceptions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0012\u0010\u0008\u0002\u0010\u0002\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lkotlin/reflect/full/NoSuchPropertyException;",
        "Lkotlin/reflect/NoSuchPropertyException;",
        "cause",
        "Ljava/lang/Exception;",
        "Lkotlin/Exception;",
        "(Ljava/lang/Exception;)V",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, v1, v0, v1}, Lkotlin/reflect/full/NoSuchPropertyException;-><init>(Ljava/lang/Exception;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lkotlin/reflect/NoSuchPropertyException;-><init>(Ljava/lang/Exception;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Exception;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Exception;

    :goto_0
    invoke-direct {p0, v0}, Lkotlin/reflect/full/NoSuchPropertyException;-><init>(Ljava/lang/Exception;)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method
