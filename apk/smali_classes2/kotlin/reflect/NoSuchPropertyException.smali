.class public Lkotlin/reflect/NoSuchPropertyException;
.super Ljava/lang/Exception;
.source "exceptions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0017\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0017\u0012\u0010\u0008\u0002\u0010\u0003\u001a\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u0002\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lkotlin/reflect/NoSuchPropertyException;",
        "Ljava/lang/Exception;",
        "Lkotlin/Exception;",
        "cause",
        "(Ljava/lang/Exception;)V",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, v1, v0, v1}, Lkotlin/reflect/NoSuchPropertyException;-><init>(Ljava/lang/Exception;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 43
    if-eqz p1, :cond_1

    .line 44
    if-nez p0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.Throwable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p0, Ljava/lang/Throwable;

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Ljava/lang/Throwable;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 43
    :cond_1
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Exception;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Exception;

    :goto_0
    invoke-direct {p0, v0}, Lkotlin/reflect/NoSuchPropertyException;-><init>(Ljava/lang/Exception;)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method
