.class public final Lkotlin/reflect/jvm/internal/k;
.super Ljava/lang/Object;
.source "kClassCache.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a&\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\u0007\"\u0008\u0008\u0000\u0010\u0008*\u00020\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\nH\u0000\"/\u0010\u0000\u001a#\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00040\u00040\u0001\u00a2\u0006\u0002\u0008\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "K_CLASS_CACHE",
        "Lkotlin/reflect/jvm/internal/pcollections/HashPMap;",
        "",
        "kotlin.jvm.PlatformType",
        "",
        "Lorg/jetbrains/annotations/NotNull;",
        "getOrCreateKotlinClass",
        "Lkotlin/reflect/jvm/internal/KClassImpl;",
        "T",
        "jClass",
        "Ljava/lang/Class;",
        "kotlin-reflection"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field private static a:Lkotlin/reflect/jvm/internal/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/a/b",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lkotlin/reflect/jvm/internal/a/b;->a()Lkotlin/reflect/jvm/internal/a/b;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/k;->a:Lkotlin/reflect/jvm/internal/a/b;

    return-void
.end method

.method public static final a(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/l;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lkotlin/reflect/jvm/internal/l",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    const-string v0, "jClass"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/k;->a:Lkotlin/reflect/jvm/internal/a/b;

    invoke-virtual {v0, v7}, Lkotlin/reflect/jvm/internal/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 33
    instance-of v1, v0, Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    .line 35
    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/l;

    .line 36
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v1

    :goto_0
    invoke-static {v1, p0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 65
    :goto_1
    return-object v0

    :cond_0
    move-object v1, v3

    .line 36
    goto :goto_0

    .line 40
    :cond_1
    if-eqz v0, :cond_6

    .line 43
    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<java.lang.ref.WeakReference<kotlin.reflect.jvm.internal.KClassImpl<T>>>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v1, v0

    check-cast v1, [Ljava/lang/ref/WeakReference;

    move-object v1, v0

    .line 44
    check-cast v1, [Ljava/lang/ref/WeakReference;

    move v4, v5

    :goto_2
    array-length v2, v1

    if-ge v4, v2, :cond_5

    aget-object v2, v1, v4

    .line 45
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/l;

    .line 46
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v6

    :goto_3
    invoke-static {v6, p0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v0, v2

    .line 47
    goto :goto_1

    :cond_3
    move-object v6, v3

    .line 46
    goto :goto_3

    .line 44
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    :cond_5
    move-object v1, v0

    .line 53
    check-cast v1, [Ljava/lang/Object;

    array-length v1, v1

    .line 54
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [Ljava/lang/ref/WeakReference;

    .line 56
    invoke-static {v0, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57
    new-instance v0, Lkotlin/reflect/jvm/internal/l;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l;-><init>(Ljava/lang/Class;)V

    .line 58
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    aput-object v3, v2, v1

    .line 59
    sget-object v1, Lkotlin/reflect/jvm/internal/k;->a:Lkotlin/reflect/jvm/internal/a/b;

    invoke-virtual {v1, v7, v2}, Lkotlin/reflect/jvm/internal/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/a/b;

    move-result-object v1

    sput-object v1, Lkotlin/reflect/jvm/internal/k;->a:Lkotlin/reflect/jvm/internal/a/b;

    goto :goto_1

    .line 63
    :cond_6
    new-instance v0, Lkotlin/reflect/jvm/internal/l;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l;-><init>(Ljava/lang/Class;)V

    .line 64
    sget-object v1, Lkotlin/reflect/jvm/internal/k;->a:Lkotlin/reflect/jvm/internal/a/b;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v7, v2}, Lkotlin/reflect/jvm/internal/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/a/b;

    move-result-object v1

    sput-object v1, Lkotlin/reflect/jvm/internal/k;->a:Lkotlin/reflect/jvm/internal/a/b;

    goto :goto_1
.end method
