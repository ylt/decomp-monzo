.class public Lkotlin/reflect/jvm/internal/ae$b;
.super Lkotlin/reflect/jvm/internal/ae$c;
.source "ReflectProperties.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/reflect/jvm/internal/ae$c",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/d/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/a",
            "<TT;>;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lkotlin/d/a/a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "initializer"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/ReflectProperties$LazyVal"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/ae$c;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/ae$b;->b:Ljava/lang/Object;

    .line 53
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/ae$b;->a:Lkotlin/d/a/a;

    .line 54
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/ae$b;->b:Ljava/lang/Object;

    .line 59
    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/ae$b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    .line 63
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/ae$b;->a:Lkotlin/d/a/a;

    invoke-interface {v0}, Lkotlin/d/a/a;->v_()Ljava/lang/Object;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/ae$b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/ae$b;->b:Ljava/lang/Object;

    goto :goto_0
.end method
