.class final Lkotlin/reflect/jvm/internal/l$a$m;
.super Lkotlin/d/b/m;
.source "KClassImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/l$a;-><init>(Lkotlin/reflect/jvm/internal/l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\u0008\u0008\u0000\u0010\u0001*\u00020\u0002H\n\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "T",
        "",
        "invoke",
        "()Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/l$a;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/l$a;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/l$a$m;->a:Lkotlin/reflect/jvm/internal/l$a;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final v_()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$m;->a:Lkotlin/reflect/jvm/internal/l$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l$a;->a()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 102
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/f;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 110
    :goto_0
    return-object v0

    .line 104
    :cond_0
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/h;->a:Lkotlin/reflect/jvm/internal/impl/a/h;

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/a/h;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 105
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/l$a$m;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 110
    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type T"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$m;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "INSTANCE"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    goto :goto_1

    .line 110
    :cond_2
    check-cast v0, Ljava/lang/Object;

    goto :goto_0
.end method
