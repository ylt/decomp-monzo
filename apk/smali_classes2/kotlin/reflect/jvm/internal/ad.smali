.class public final Lkotlin/reflect/jvm/internal/ad;
.super Ljava/lang/Object;
.source "moduleByClassLoader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0005\u001a\u00020\u0004*\u0006\u0012\u0002\u0008\u00030\u0006H\u0000\" \u0010\u0000\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u00030\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "moduleByClassLoader",
        "Ljava/util/concurrent/ConcurrentMap;",
        "Lkotlin/reflect/jvm/internal/WeakClassLoaderBox;",
        "Ljava/lang/ref/WeakReference;",
        "Lorg/jetbrains/kotlin/load/kotlin/reflect/RuntimeModuleData;",
        "getOrCreateModule",
        "Ljava/lang/Class;",
        "kotlin-reflection"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lkotlin/reflect/jvm/internal/al;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/b/g;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    sput-object v0, Lkotlin/reflect/jvm/internal/ad;->a:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method public static final a(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/d/b/b/g;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lkotlin/reflect/jvm/internal/impl/d/b/b/g;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->a(Ljava/lang/Class;)Ljava/lang/ClassLoader;

    move-result-object v3

    .line 50
    new-instance v4, Lkotlin/reflect/jvm/internal/al;

    invoke-direct {v4, v3}, Lkotlin/reflect/jvm/internal/al;-><init>(Ljava/lang/ClassLoader;)V

    .line 52
    sget-object v0, Lkotlin/reflect/jvm/internal/ad;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v4}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 53
    if-eqz v0, :cond_1

    .line 54
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/b/b/g;

    if-eqz v1, :cond_0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/b/b/g;

    const-string v0, "it"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    :goto_0
    return-object v1

    .line 55
    :cond_0
    sget-object v1, Lkotlin/reflect/jvm/internal/ad;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v4, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 58
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/g;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/g$a;

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/b/g$a;->a(Ljava/lang/ClassLoader;)Lkotlin/reflect/jvm/internal/impl/d/b/b/g;

    move-result-object v3

    .line 59
    nop

    .line 61
    :goto_1
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/ad;->a:Ljava/util/concurrent/ConcurrentMap;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v4, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    if-nez v0, :cond_2

    move-object v0, v2

    .line 70
    check-cast v0, Ljava/lang/ClassLoader;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/al;->a(Ljava/lang/ClassLoader;)V

    move-object v1, v3

    goto :goto_0

    .line 64
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/b/b/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    if-eqz v1, :cond_3

    .line 70
    check-cast v2, Ljava/lang/ClassLoader;

    invoke-virtual {v4, v2}, Lkotlin/reflect/jvm/internal/al;->a(Ljava/lang/ClassLoader;)V

    goto :goto_0

    .line 66
    :cond_3
    :try_start_2
    sget-object v1, Lkotlin/reflect/jvm/internal/ad;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v4, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 70
    :catchall_0
    move-exception v0

    check-cast v2, Ljava/lang/ClassLoader;

    invoke-virtual {v4, v2}, Lkotlin/reflect/jvm/internal/al;->a(Ljava/lang/ClassLoader;)V

    throw v0
.end method
