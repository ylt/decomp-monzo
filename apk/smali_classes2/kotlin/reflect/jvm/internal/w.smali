.class public abstract Lkotlin/reflect/jvm/internal/w;
.super Lkotlin/reflect/jvm/internal/j;
.source "KPropertyImpl.kt"

# interfaces
.implements Lkotlin/reflect/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/w$a;,
        Lkotlin/reflect/jvm/internal/w$c;,
        Lkotlin/reflect/jvm/internal/w$d;,
        Lkotlin/reflect/jvm/internal/w$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/reflect/jvm/internal/j",
        "<TR;>;",
        "Lkotlin/reflect/l",
        "<TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008 \u0018\u0000 <*\u0006\u0008\u0000\u0010\u0001 \u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003:\u0004;<=>B)\u0008\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bB\u0017\u0008\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eB3\u0008\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0008\u0010\u000f\u001a\u0004\u0018\u00010\r\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u0010J\n\u00102\u001a\u0004\u0018\u00010+H\u0004J\u0013\u00103\u001a\u00020&2\u0008\u00104\u001a\u0004\u0018\u00010\nH\u0096\u0002J\u001e\u00105\u001a\u0004\u0018\u00010\n2\u0008\u00106\u001a\u0004\u0018\u00010+2\u0008\u00107\u001a\u0004\u0018\u00010\nH\u0004J\u0008\u00108\u001a\u000209H\u0016J\u0008\u0010:\u001a\u00020\u0007H\u0016R\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0018\u0010\u0013\u001a\u0006\u0012\u0002\u0008\u00030\u00148VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00148VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u0016R\u0014\u0010\u000c\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u001cR!\u0010\u001d\u001a\u0015\u0012\u000c\u0012\n \u001f*\u0004\u0018\u00010\r0\r0\u001e\u00a2\u0006\u0002\u0008 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010!\u001a\u0008\u0012\u0004\u0012\u00028\u00000\"X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010$R\u0014\u0010%\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\'R\u0014\u0010(\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008(\u0010\'R\u0014\u0010)\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010\'R\u0013\u0010*\u001a\u0004\u0018\u00010+8F\u00a2\u0006\u0006\u001a\u0004\u0008,\u0010-R\u001b\u0010.\u001a\u000f\u0012\u0006\u0012\u0004\u0018\u00010+0\u001e\u00a2\u0006\u0002\u0008 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u00100R\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u00100\u00a8\u0006?"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KPropertyImpl;",
        "R",
        "Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "Lkotlin/reflect/KProperty;",
        "container",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "name",
        "",
        "signature",
        "boundReceiver",
        "",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;)V",
        "descriptorInitialValue",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Ljava/lang/String;Ljava/lang/String;Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;Ljava/lang/Object;)V",
        "getBoundReceiver",
        "()Ljava/lang/Object;",
        "caller",
        "Lkotlin/reflect/jvm/internal/FunctionCaller;",
        "getCaller",
        "()Lkotlin/reflect/jvm/internal/FunctionCaller;",
        "getContainer",
        "()Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "defaultCaller",
        "getDefaultCaller",
        "getDescriptor",
        "()Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "descriptor_",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazySoftVal;",
        "kotlin.jvm.PlatformType",
        "Lorg/jetbrains/annotations/NotNull;",
        "getter",
        "Lkotlin/reflect/jvm/internal/KPropertyImpl$Getter;",
        "getGetter",
        "()Lkotlin/reflect/jvm/internal/KPropertyImpl$Getter;",
        "isBound",
        "",
        "()Z",
        "isConst",
        "isLateinit",
        "javaField",
        "Ljava/lang/reflect/Field;",
        "getJavaField",
        "()Ljava/lang/reflect/Field;",
        "javaField_",
        "getName",
        "()Ljava/lang/String;",
        "getSignature",
        "computeDelegateField",
        "equals",
        "other",
        "getDelegate",
        "field",
        "receiver",
        "hashCode",
        "",
        "toString",
        "Accessor",
        "Companion",
        "Getter",
        "Setter",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/w$b;

.field private static final h:Ljava/lang/Object;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/ae$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/ae$a",
            "<",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/ae$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/ae$a",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/n;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/w$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/w$b;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/w;->a:Lkotlin/reflect/jvm/internal/w$b;

    .line 175
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/w;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signature"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/w;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/Object;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 35
    .line 41
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/j;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/w;->d:Lkotlin/reflect/jvm/internal/n;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/w;->e:Ljava/lang/String;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/w;->f:Ljava/lang/String;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/w;->g:Ljava/lang/Object;

    .line 56
    new-instance v0, Lkotlin/reflect/jvm/internal/w$f;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/w$f;-><init>(Lkotlin/reflect/jvm/internal/w;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/w;->b:Lkotlin/reflect/jvm/internal/ae$a;

    .line 104
    new-instance v0, Lkotlin/reflect/jvm/internal/w$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/w$e;-><init>(Lkotlin/reflect/jvm/internal/w;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {p4, v0}, Lkotlin/reflect/jvm/internal/ae;->a(Ljava/lang/Object;Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/w;->c:Lkotlin/reflect/jvm/internal/ae$a;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)V
    .locals 6

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/ag;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    const-string v0, "descriptor.name.asString()"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/ai;->a:Lkotlin/reflect/jvm/internal/ai;

    invoke-virtual {v0, p2}, Lkotlin/reflect/jvm/internal/ai;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Lkotlin/reflect/jvm/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/i;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lkotlin/d/b/e;->c:Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/w;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/Object;)V

    return-void
.end method

.method public static final synthetic x()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lkotlin/reflect/jvm/internal/w;->h:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/lang/reflect/Field;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 89
    nop

    .line 90
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/w;->a:Lkotlin/reflect/jvm/internal/w$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$b;->a()Ljava/lang/Object;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->u()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' is not an extension property and thus getExtensionDelegate() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is not going to work, use getDelegate() instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 99
    new-instance v0, Lkotlin/reflect/full/IllegalPropertyDelegateAccessException;

    invoke-direct {v0, v1}, Lkotlin/reflect/full/IllegalPropertyDelegateAccessException;-><init>(Ljava/lang/IllegalAccessException;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 96
    :cond_0
    if-eqz p1, :cond_1

    :try_start_1
    invoke-virtual {p1, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/w;->e:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->u()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 119
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/ak;->b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/w;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 120
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/w;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/w;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/w;->f:Ljava/lang/String;

    iget-object v3, v1, Lkotlin/reflect/jvm/internal/w;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/w;->g:Ljava/lang/Object;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/w;->g:Ljava/lang/Object;

    invoke-static {v2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/f",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->r()Lkotlin/reflect/jvm/internal/w$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$c;->f()Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/f",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 112
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->r()Lkotlin/reflect/jvm/internal/w$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$c;->g()Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->o()Lkotlin/reflect/jvm/internal/n;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/n;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/w;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/n;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/w;->d:Lkotlin/reflect/jvm/internal/n;

    return-object v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/w;->g:Ljava/lang/Object;

    sget-object v1, Lkotlin/d/b/e;->c:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public abstract r()Lkotlin/reflect/jvm/internal/w$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/w$c",
            "<TR;>;"
        }
    .end annotation
.end method

.method public final s()Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/w;->b:Lkotlin/reflect/jvm/internal/ae$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Field;

    return-object v0
.end method

.method protected final t()Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->u()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->s()Ljava/lang/reflect/Field;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/w;->u()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/w;->c:Lkotlin/reflect/jvm/internal/ae$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$a;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "descriptor_()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/w;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/w;->g:Ljava/lang/Object;

    return-object v0
.end method
