.class public final Lkotlin/reflect/jvm/internal/ag;
.super Ljava/lang/Object;
.source "ReflectionObjectRenderer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u000c\u001a\u00020\nJ\u000e\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0011J\u000e\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u0017J\u001a\u0010\u0018\u001a\u00020\u0019*\u00060\u001aj\u0002`\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0002J\u0018\u0010\u001e\u001a\u00020\u0019*\u00060\u001aj\u0002`\u001b2\u0006\u0010\u001f\u001a\u00020\u0008H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/ReflectionObjectRenderer;",
        "",
        "()V",
        "renderer",
        "Lorg/jetbrains/kotlin/renderer/DescriptorRenderer;",
        "renderCallable",
        "",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/CallableDescriptor;",
        "renderFunction",
        "Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;",
        "renderLambda",
        "invoke",
        "renderParameter",
        "parameter",
        "Lkotlin/reflect/jvm/internal/KParameterImpl;",
        "renderProperty",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "renderType",
        "type",
        "Lorg/jetbrains/kotlin/types/KotlinType;",
        "renderTypeParameter",
        "typeParameter",
        "Lorg/jetbrains/kotlin/descriptors/TypeParameterDescriptor;",
        "appendReceiverType",
        "",
        "Ljava/lang/StringBuilder;",
        "Lkotlin/text/StringBuilder;",
        "receiver",
        "Lorg/jetbrains/kotlin/descriptors/ReceiverParameterDescriptor;",
        "appendReceivers",
        "callable",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/ag;

.field private static final b:Lkotlin/reflect/jvm/internal/impl/g/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lkotlin/reflect/jvm/internal/ag;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/ag;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/ag;

    sput-object p0, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    .line 26
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->e:Lkotlin/reflect/jvm/internal/impl/g/c;

    sput-object v0, Lkotlin/reflect/jvm/internal/ag;->b:Lkotlin/reflect/jvm/internal/impl/g/c;

    return-void
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a;)V
    .locals 3

    .prologue
    .line 36
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/a;->e()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    .line 37
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v1

    .line 39
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/ag;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/aj;)V

    .line 41
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 42
    :goto_0
    if-eqz v0, :cond_0

    const-string v2, "("

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    :cond_0
    invoke-direct {p0, p1, v1}, Lkotlin/reflect/jvm/internal/ag;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/aj;)V

    .line 44
    if-eqz v0, :cond_1

    const-string v0, ")"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    :cond_1
    return-void

    .line 41
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/aj;)V
    .locals 2

    .prologue
    .line 29
    if-eqz p2, :cond_0

    .line 30
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v1, "receiver.type"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-eqz v0, :cond_0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Ljava/lang/String;

    move-result-object v0

    .line 48
    :goto_0
    return-object v0

    .line 50
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal callable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Ljava/lang/String;
    .locals 5

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 58
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "var "

    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    sget-object v3, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    move-object v2, p1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {v3, v0, v2}, Lkotlin/reflect/jvm/internal/ag;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a;)V

    .line 60
    sget-object v2, Lkotlin/reflect/jvm/internal/ag;->b:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    const-string v4, "descriptor.name"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    sget-object v2, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    const-string v4, "descriptor.type"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    nop

    .line 57
    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 58
    :cond_0
    const-string v2, "val "

    goto :goto_0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/aq;)Ljava/lang/String;
    .locals 4

    .prologue
    const-string v0, "typeParameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 110
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/ah;->b:[I

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 116
    :goto_0
    :pswitch_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 117
    nop

    .line 109
    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 112
    :pswitch_1
    const-string v2, "in "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 113
    :pswitch_2
    const-string v2, "out "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v6, 0x0

    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object v10, v11

    check-cast v10, Ljava/lang/StringBuilder;

    .line 69
    const-string v0, "fun "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    sget-object v1, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {v1, v10, v0}, Lkotlin/reflect/jvm/internal/ag;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a;)V

    .line 71
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->b:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "descriptor.name"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    move-object v1, v10

    check-cast v1, Ljava/lang/Appendable;

    const-string v2, ", "

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "("

    check-cast v3, Ljava/lang/CharSequence;

    const-string v4, ")"

    check-cast v4, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    sget-object v7, Lkotlin/reflect/jvm/internal/ag$a;->a:Lkotlin/reflect/jvm/internal/ag$a;

    check-cast v7, Lkotlin/d/a/b;

    const/16 v8, 0x30

    move-object v9, v6

    invoke-static/range {v0 .. v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/Appendable;

    .line 77
    const-string v0, ": "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    const-string v2, "descriptor.returnType!!"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    nop

    .line 68
    check-cast v11, Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->b:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/s;)Ljava/lang/String;
    .locals 4

    .prologue
    const-string v0, "parameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 97
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/s;->d()Lkotlin/reflect/k$a;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/ah;->a:[I

    invoke-virtual {v2}, Lkotlin/reflect/k$a;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 103
    :goto_0
    const-string v2, " of "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    sget-object v3, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/s;->f()Lkotlin/reflect/jvm/internal/j;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/j;->d()Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-virtual {v3, v2}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    nop

    .line 96
    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 98
    :pswitch_0
    const-string v2, "extension receiver"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 99
    :pswitch_1
    const-string v2, "instance"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 100
    :pswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parameter #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/s;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/s;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v6, 0x0

    const-string v0, "invoke"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object v10, v11

    check-cast v10, Ljava/lang/StringBuilder;

    .line 84
    sget-object v1, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {v1, v10, v0}, Lkotlin/reflect/jvm/internal/ag;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a;)V

    .line 86
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    move-object v1, v10

    check-cast v1, Ljava/lang/Appendable;

    const-string v2, ", "

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "("

    check-cast v3, Ljava/lang/CharSequence;

    const-string v4, ")"

    check-cast v4, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    sget-object v7, Lkotlin/reflect/jvm/internal/ag$b;->a:Lkotlin/reflect/jvm/internal/ag$b;

    check-cast v7, Lkotlin/d/a/b;

    const/16 v8, 0x30

    move-object v9, v6

    invoke-static/range {v0 .. v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/Appendable;

    .line 90
    const-string v0, " -> "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    sget-object v0, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    const-string v2, "invoke.returnType!!"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    nop

    .line 83
    check-cast v11, Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
