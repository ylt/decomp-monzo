.class public final Lkotlin/reflect/jvm/internal/p$a;
.super Lkotlin/reflect/jvm/internal/w$d;
.source "KProperty0Impl.kt"

# interfaces
.implements Lkotlin/reflect/h$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/reflect/jvm/internal/w$d",
        "<TR;>;",
        "Lkotlin/reflect/h$a",
        "<TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u0000*\u0004\u0008\u0001\u0010\u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00028\u0001H\u0096\u0002\u00a2\u0006\u0002\u0010\u000cR\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KMutableProperty0Impl$Setter;",
        "R",
        "Lkotlin/reflect/jvm/internal/KPropertyImpl$Setter;",
        "Lkotlin/reflect/KMutableProperty0$Setter;",
        "property",
        "Lkotlin/reflect/jvm/internal/KMutableProperty0Impl;",
        "(Lkotlin/reflect/jvm/internal/KMutableProperty0Impl;)V",
        "getProperty",
        "()Lkotlin/reflect/jvm/internal/KMutableProperty0Impl;",
        "invoke",
        "",
        "value",
        "(Ljava/lang/Object;)V",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/p",
            "<TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/p;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/p",
            "<TR;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "property"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/w$d;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/p$a;->b:Lkotlin/reflect/jvm/internal/p;

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/p$a;->b(Ljava/lang/Object;)V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/p",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/p$a;->b:Lkotlin/reflect/jvm/internal/p;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/p$a;->a()Lkotlin/reflect/jvm/internal/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/p;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic c()Lkotlin/reflect/l;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/p$a;->a()Lkotlin/reflect/jvm/internal/p;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/w;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/p$a;->a()Lkotlin/reflect/jvm/internal/p;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/w;

    return-object v0
.end method
