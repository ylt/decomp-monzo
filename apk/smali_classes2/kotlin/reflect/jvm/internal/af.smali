.class public Lkotlin/reflect/jvm/internal/af;
.super Lkotlin/d/b/z;
.source "ReflectionFactoryImpl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lkotlin/d/b/z;-><init>()V

    return-void
.end method

.method private static a(Lkotlin/d/b/e;)Lkotlin/reflect/jvm/internal/n;
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0}, Lkotlin/d/b/e;->a()Lkotlin/reflect/e;

    move-result-object v0

    .line 106
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/n;

    if-eqz v1, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/n;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/e;->a:Lkotlin/reflect/jvm/internal/e;

    goto :goto_0
.end method


# virtual methods
.method public a(Lkotlin/d/b/m;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    invoke-static {p1}, Lkotlin/reflect/jvm/d;->a(Lkotlin/a;)Lkotlin/reflect/f;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ak;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/o;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    sget-object v1, Lkotlin/reflect/jvm/internal/ag;->a:Lkotlin/reflect/jvm/internal/ag;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/o;->c()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/ag;->b(Lkotlin/reflect/jvm/internal/impl/b/s;)Ljava/lang/String;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lkotlin/d/b/z;->a(Lkotlin/d/b/m;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;)Lkotlin/reflect/c;
    .locals 1

    .prologue
    .line 45
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/k;->a(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/l;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/d/b/k;)Lkotlin/reflect/f;
    .locals 5

    .prologue
    .line 69
    new-instance v0, Lkotlin/reflect/jvm/internal/o;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/af;->a(Lkotlin/d/b/e;)Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/d/b/k;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/d/b/k;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lkotlin/d/b/k;->c_()Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/o;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public a(Lkotlin/d/b/q;)Lkotlin/reflect/i;
    .locals 5

    .prologue
    .line 91
    new-instance v0, Lkotlin/reflect/jvm/internal/q;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/af;->a(Lkotlin/d/b/e;)Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/d/b/q;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/d/b/q;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lkotlin/d/b/q;->c_()Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/q;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public a(Lkotlin/d/b/t;)Lkotlin/reflect/m;
    .locals 5

    .prologue
    .line 76
    new-instance v0, Lkotlin/reflect/jvm/internal/t;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/af;->a(Lkotlin/d/b/e;)Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/d/b/t;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/d/b/t;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lkotlin/d/b/t;->c_()Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/t;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public a(Lkotlin/d/b/v;)Lkotlin/reflect/n;
    .locals 5

    .prologue
    .line 86
    new-instance v0, Lkotlin/reflect/jvm/internal/u;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/af;->a(Lkotlin/d/b/e;)Lkotlin/reflect/jvm/internal/n;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/d/b/v;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/d/b/v;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lkotlin/d/b/v;->c_()Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/u;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method
