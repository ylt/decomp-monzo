.class final Lkotlin/reflect/jvm/internal/l$a$n;
.super Lkotlin/d/b/m;
.source "KClassImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/l$a;-><init>(Lkotlin/reflect/jvm/internal/l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\t\u0018\u00010\u0001\u00a2\u0006\u0002\u0008\u0002\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lorg/jetbrains/annotations/NotNull;",
        "T",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/l$a;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/l$a;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/l$a$n;->a:Lkotlin/reflect/jvm/internal/l$a;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/l$a$n;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-object v0

    .line 66
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/l$a$n;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/l;->a(Lkotlin/reflect/jvm/internal/l;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 69
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l$a$n;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
