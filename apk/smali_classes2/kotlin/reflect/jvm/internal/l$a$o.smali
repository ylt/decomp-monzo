.class final Lkotlin/reflect/jvm/internal/l$a$o;
.super Lkotlin/d/b/m;
.source "KClassImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/l$a;-><init>(Lkotlin/reflect/jvm/internal/l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/l$a;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/l$a;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/l$a$o;->a:Lkotlin/reflect/jvm/internal/l$a;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$o;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    .line 56
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$o;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/l;->a(Lkotlin/reflect/jvm/internal/l;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l$a$o;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/l$a$o;->a:Lkotlin/reflect/jvm/internal/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/l$a;->b:Lkotlin/reflect/jvm/internal/l;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/l$a;->a(Lkotlin/reflect/jvm/internal/l$a;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->c()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "classId.shortClassName.asString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l$a$o;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
