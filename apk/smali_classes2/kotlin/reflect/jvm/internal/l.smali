.class public final Lkotlin/reflect/jvm/internal/l;
.super Lkotlin/reflect/jvm/internal/n;
.source "KClassImpl.kt"

# interfaces
.implements Lkotlin/reflect/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/l$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/reflect/jvm/internal/n;",
        "Lkotlin/reflect/c",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u00bc\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u001b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0001\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u00032\u0008\u0012\u0004\u0012\u0002H\u00010\u00042\u00020\u0005:\u0001]B\u0013\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\u0008J\u0013\u0010N\u001a\u00020&2\u0008\u0010O\u001a\u0004\u0018\u00010\u0002H\u0096\u0002J\u0016\u0010P\u001a\u0008\u0012\u0004\u0012\u00020Q0\u00132\u0006\u0010R\u001a\u00020SH\u0016J\u0016\u0010T\u001a\u0008\u0012\u0004\u0012\u00020U0\u00132\u0006\u0010R\u001a\u00020SH\u0016J\u0008\u0010V\u001a\u00020WH\u0016J\u0012\u0010X\u001a\u00020&2\u0008\u0010Y\u001a\u0004\u0018\u00010\u0002H\u0016J\u0008\u0010Z\u001a\u00020[H\u0002J\u0008\u0010\\\u001a\u00020=H\u0016R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R \u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00180\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u0016R8\u0010\u001a\u001a)\u0012 \u0012\u001e \u001d*\u000e\u0018\u00010\u001cR\u0008\u0012\u0004\u0012\u00028\u00000\u00000\u001cR\u0008\u0012\u0004\u0012\u00028\u00000\u00000\u001b\u00a2\u0006\u0002\u0008\u001e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0014\u0010!\u001a\u00020\"8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010$R\u0014\u0010%\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\'R\u0014\u0010(\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008(\u0010\'R\u0014\u0010)\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010\'R\u0014\u0010*\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008*\u0010\'R\u0014\u0010+\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008+\u0010\'R\u0014\u0010,\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008,\u0010\'R\u0014\u0010-\u001a\u00020&8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008-\u0010\'R\u001a\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010/R\u0014\u00100\u001a\u0002018@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u00082\u00103R\u001e\u00104\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u0003050\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u00086\u0010\u0016R\u001e\u00107\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00040\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u00088\u0010\u0016R\u0016\u00109\u001a\u0004\u0018\u00018\u00008VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008:\u0010;R\u0016\u0010<\u001a\u0004\u0018\u00010=8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008>\u0010?R\u0016\u0010@\u001a\u0004\u0018\u00010=8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008A\u0010?R\u0014\u0010B\u001a\u0002018@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008C\u00103R\u001a\u0010D\u001a\u0008\u0012\u0004\u0012\u00020E0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008F\u0010\rR\u001a\u0010G\u001a\u0008\u0012\u0004\u0012\u00020H0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008I\u0010\rR\u0016\u0010J\u001a\u0004\u0018\u00010K8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008L\u0010M\u00a8\u0006^"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KClassImpl;",
        "T",
        "",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "Lkotlin/reflect/KClass;",
        "Lkotlin/reflect/jvm/internal/KClassifierImpl;",
        "jClass",
        "Ljava/lang/Class;",
        "(Ljava/lang/Class;)V",
        "annotations",
        "",
        "",
        "getAnnotations",
        "()Ljava/util/List;",
        "classId",
        "Lorg/jetbrains/kotlin/name/ClassId;",
        "getClassId",
        "()Lorg/jetbrains/kotlin/name/ClassId;",
        "constructorDescriptors",
        "",
        "Lorg/jetbrains/kotlin/descriptors/ConstructorDescriptor;",
        "getConstructorDescriptors",
        "()Ljava/util/Collection;",
        "constructors",
        "Lkotlin/reflect/KFunction;",
        "getConstructors",
        "data",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazyVal;",
        "Lkotlin/reflect/jvm/internal/KClassImpl$Data;",
        "kotlin.jvm.PlatformType",
        "Lorg/jetbrains/annotations/NotNull;",
        "getData",
        "()Lkotlin/reflect/jvm/internal/ReflectProperties$LazyVal;",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;",
        "getDescriptor",
        "()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;",
        "isAbstract",
        "",
        "()Z",
        "isCompanion",
        "isData",
        "isFinal",
        "isInner",
        "isOpen",
        "isSealed",
        "getJClass",
        "()Ljava/lang/Class;",
        "memberScope",
        "Lorg/jetbrains/kotlin/resolve/scopes/MemberScope;",
        "getMemberScope$kotlin_reflection",
        "()Lorg/jetbrains/kotlin/resolve/scopes/MemberScope;",
        "members",
        "Lkotlin/reflect/KCallable;",
        "getMembers",
        "nestedClasses",
        "getNestedClasses",
        "objectInstance",
        "getObjectInstance",
        "()Ljava/lang/Object;",
        "qualifiedName",
        "",
        "getQualifiedName",
        "()Ljava/lang/String;",
        "simpleName",
        "getSimpleName",
        "staticScope",
        "getStaticScope$kotlin_reflection",
        "supertypes",
        "Lkotlin/reflect/KType;",
        "getSupertypes",
        "typeParameters",
        "Lkotlin/reflect/KTypeParameter;",
        "getTypeParameters",
        "visibility",
        "Lkotlin/reflect/KVisibility;",
        "getVisibility",
        "()Lkotlin/reflect/KVisibility;",
        "equals",
        "other",
        "getFunctions",
        "Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;",
        "name",
        "Lorg/jetbrains/kotlin/name/Name;",
        "getProperties",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "hashCode",
        "",
        "isInstance",
        "value",
        "reportUnresolvedClass",
        "",
        "toString",
        "Data",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/ae$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/ae$b",
            "<",
            "Lkotlin/reflect/jvm/internal/l",
            "<TT;>.a;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "jClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/n;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/l;->c:Ljava/lang/Class;

    .line 157
    new-instance v0, Lkotlin/reflect/jvm/internal/l$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/l$b;-><init>(Lkotlin/reflect/jvm/internal/l;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/l;->a:Lkotlin/reflect/jvm/internal/ae$b;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/l;)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/l;->k()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/l;)Ljava/lang/Void;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/l;->l()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method private final k()Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 2

    .prologue
    .line 163
    sget-object v0, Lkotlin/reflect/jvm/internal/ai;->a:Lkotlin/reflect/jvm/internal/ai;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/ai;->a(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    return-object v0
.end method

.method private final l()Ljava/lang/Void;
    .locals 4

    .prologue
    .line 250
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;->a(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/d/b/b/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    move-result-object v0

    move-object v1, v0

    .line 251
    :goto_0
    if-nez v1, :cond_1

    .line 271
    :pswitch_0
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unresolved class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 250
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 251
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/m;->a:[I

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 271
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 253
    :pswitch_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Packages and file facades are not yet supported in Kotlin reflection. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Meanwhile please use Java reflection to inspect this class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 259
    :pswitch_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This class is an internal synthetic class generated by the Kotlin compiler, such as an anonymous class for a lambda, a SAM wrapper, a callable reference, etc. It\'s not a Kotlin class or interface, so the reflection "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "library has no idea what declarations does it have. Please use Java reflection to inspect this class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 267
    :pswitch_3
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (kind = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l;->c:Ljava/lang/Class;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->e()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->h:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v1, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->f()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->h:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 182
    return-object v0
.end method

.method public b()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->d()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 174
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 177
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->k()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "descriptor.constructors"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->e()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->h:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v1, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->f()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->h:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 186
    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/ae$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/ae$b",
            "<",
            "Lkotlin/reflect/jvm/internal/l",
            "<TT;>.a;>;"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l;->a:Lkotlin/reflect/jvm/internal/ae$b;

    return-object v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l;->a:Lkotlin/reflect/jvm/internal/ae$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/l$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l$a;->a()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->d()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 235
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/l;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lkotlin/d/a;->c(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v0

    check-cast p1, Lkotlin/reflect/c;

    invoke-static {p1}, Lkotlin/d/a;->c(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 2

    .prologue
    .line 167
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/l;->d()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->d()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v1, "descriptor.staticScope"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public g()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/f",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l;->a:Lkotlin/reflect/jvm/internal/ae$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/l$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l$a;->d()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 238
    invoke-static {p0}, Lkotlin/d/a;->c(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->hashCode()I

    move-result v0

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l;->a:Lkotlin/reflect/jvm/internal/ae$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/l$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l$a;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/l;->k()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/a;

    .line 242
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    .line 243
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, ""

    move-object v6, v1

    .line 244
    :goto_0
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->b()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    const/16 v2, 0x24

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 241
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 243
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    goto :goto_0
.end method

.method public w_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/l;->a:Lkotlin/reflect/jvm/internal/ae$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/l$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/l$a;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
