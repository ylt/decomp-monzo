.class final Lkotlin/reflect/jvm/internal/x$d;
.super Lkotlin/d/b/m;
.source "KPropertyImpl.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/x;->b(Lkotlin/reflect/jvm/internal/w$a;Z)Lkotlin/reflect/jvm/internal/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Ljava/lang/reflect/Field;",
        "Lkotlin/reflect/jvm/internal/f",
        "<+",
        "Ljava/lang/reflect/Field;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "computeFieldCaller",
        "Lkotlin/reflect/jvm/internal/FunctionCaller;",
        "Ljava/lang/reflect/Field;",
        "field",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/w$a;

.field final synthetic b:Lkotlin/reflect/jvm/internal/x$a;

.field final synthetic c:Z

.field final synthetic d:Lkotlin/reflect/jvm/internal/x$c;

.field final synthetic e:Lkotlin/reflect/jvm/internal/x$b;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/w$a;Lkotlin/reflect/jvm/internal/x$a;ZLkotlin/reflect/jvm/internal/x$c;Lkotlin/reflect/jvm/internal/x$b;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/x$d;->b:Lkotlin/reflect/jvm/internal/x$a;

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/x$d;->c:Z

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/x$d;->d:Lkotlin/reflect/jvm/internal/x$c;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/x$d;->e:Lkotlin/reflect/jvm/internal/x$b;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/lang/reflect/Field;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/x$d;->a(Ljava/lang/reflect/Field;)Lkotlin/reflect/jvm/internal/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/reflect/Field;)Lkotlin/reflect/jvm/internal/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            ")",
            "Lkotlin/reflect/jvm/internal/f",
            "<",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "field"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->b:Lkotlin/reflect/jvm/internal/x$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/x$a;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 197
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$a;->l()Lkotlin/reflect/jvm/internal/impl/b/af;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/af;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ak;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 198
    :cond_1
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/x$d;->c:Z

    if-eqz v0, :cond_3

    .line 199
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lkotlin/reflect/jvm/internal/f$a;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$a;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    .line 222
    :goto_0
    return-object v0

    .line 200
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/f$k;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$k;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_0

    .line 202
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lkotlin/reflect/jvm/internal/f$b;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$b;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_0

    .line 203
    :cond_4
    new-instance v0, Lkotlin/reflect/jvm/internal/f$l;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$l;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_0

    .line 205
    :cond_5
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 206
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/x$d;->c:Z

    if-eqz v0, :cond_7

    .line 207
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lkotlin/reflect/jvm/internal/f$d;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/w$a;->e()Lkotlin/reflect/jvm/internal/w;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/w;->w()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$d;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$o;

    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_0

    .line 208
    :cond_6
    new-instance v0, Lkotlin/reflect/jvm/internal/f$q;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$q;-><init>(Ljava/lang/reflect/Field;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$o;

    goto :goto_1

    .line 210
    :cond_7
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lkotlin/reflect/jvm/internal/f$e;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/x$d;->d:Lkotlin/reflect/jvm/internal/x$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/x$c;->b()Z

    move-result v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/w$a;->e()Lkotlin/reflect/jvm/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/w;->w()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lkotlin/reflect/jvm/internal/f$e;-><init>(Ljava/lang/reflect/Field;ZLjava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$p;

    :goto_2
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto :goto_0

    .line 211
    :cond_8
    new-instance v0, Lkotlin/reflect/jvm/internal/f$r;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/x$d;->d:Lkotlin/reflect/jvm/internal/x$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/x$c;->b()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$r;-><init>(Ljava/lang/reflect/Field;Z)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$p;

    goto :goto_2

    .line 212
    :cond_9
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->e:Lkotlin/reflect/jvm/internal/x$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/x$b;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 213
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/x$d;->c:Z

    if-eqz v0, :cond_b

    .line 214
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lkotlin/reflect/jvm/internal/f$h;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$h;-><init>(Ljava/lang/reflect/Field;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$o;

    :goto_3
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto/16 :goto_0

    .line 215
    :cond_a
    new-instance v0, Lkotlin/reflect/jvm/internal/f$u;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$u;-><init>(Ljava/lang/reflect/Field;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$o;

    goto :goto_3

    .line 217
    :cond_b
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/x$d;->a:Lkotlin/reflect/jvm/internal/w$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/w$a;->p()Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Lkotlin/reflect/jvm/internal/f$i;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/x$d;->d:Lkotlin/reflect/jvm/internal/x$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/x$c;->b()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$i;-><init>(Ljava/lang/reflect/Field;Z)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$p;

    :goto_4
    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto/16 :goto_0

    .line 218
    :cond_c
    new-instance v0, Lkotlin/reflect/jvm/internal/f$v;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/x$d;->d:Lkotlin/reflect/jvm/internal/x$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/x$c;->b()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$v;-><init>(Ljava/lang/reflect/Field;Z)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f$p;

    goto :goto_4

    .line 220
    :cond_d
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/x$d;->c:Z

    if-eqz v0, :cond_e

    new-instance v0, Lkotlin/reflect/jvm/internal/f$x;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/f$x;-><init>(Ljava/lang/reflect/Field;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto/16 :goto_0

    .line 221
    :cond_e
    new-instance v0, Lkotlin/reflect/jvm/internal/f$y;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/x$d;->d:Lkotlin/reflect/jvm/internal/x$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/x$c;->b()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/f$y;-><init>(Ljava/lang/reflect/Field;Z)V

    check-cast v0, Lkotlin/reflect/jvm/internal/f;

    goto/16 :goto_0
.end method
