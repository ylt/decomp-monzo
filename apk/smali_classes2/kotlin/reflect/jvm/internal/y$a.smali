.class final Lkotlin/reflect/jvm/internal/y$a;
.super Lkotlin/d/b/m;
.source "KTypeImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/y;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/d/a/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/r;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lkotlin/reflect/KTypeProjection;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/y;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/y;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/y$a;->a:Lkotlin/reflect/jvm/internal/y;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/y$a;->a:Lkotlin/reflect/jvm/internal/y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/y;->d()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v1

    .line 74
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v2

    .line 110
    :goto_0
    return-object v2

    .line 76
    :cond_0
    sget-object v2, Lkotlin/f;->b:Lkotlin/f;

    new-instance v0, Lkotlin/reflect/jvm/internal/y$a$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/y$a$b;-><init>(Lkotlin/reflect/jvm/internal/y$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v2, v0}, Lkotlin/d;->a(Lkotlin/f;Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v7

    sget-object v0, Lkotlin/reflect/jvm/internal/aa;->a:Lkotlin/reflect/m;

    check-cast v0, Lkotlin/reflect/l;

    .line 78
    check-cast v1, Ljava/lang/Iterable;

    .line 126
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 127
    const/4 v3, 0x0

    .line 128
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v3

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 129
    add-int/lit8 v6, v5, 0x1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 79
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 80
    sget-object v1, Lkotlin/reflect/r;->a:Lkotlin/reflect/r$a;

    invoke-virtual {v1}, Lkotlin/reflect/r$a;->a()Lkotlin/reflect/r;

    move-result-object v1

    .line 109
    :goto_2
    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v5, v6

    .line 128
    goto :goto_1

    .line 83
    :cond_1
    new-instance v4, Lkotlin/reflect/jvm/internal/y;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v9

    const-string v3, "typeProjection.type"

    invoke-static {v9, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lkotlin/reflect/jvm/internal/y$a$a;

    invoke-direct {v3, v5, p0, v7, v0}, Lkotlin/reflect/jvm/internal/y$a$a;-><init>(ILkotlin/reflect/jvm/internal/y$a;Lkotlin/c;Lkotlin/reflect/l;)V

    check-cast v3, Lkotlin/d/a/a;

    invoke-direct {v4, v9, v3}, Lkotlin/reflect/jvm/internal/y;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/d/a/a;)V

    .line 104
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    sget-object v3, Lkotlin/reflect/jvm/internal/z;->a:[I

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    .line 107
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 105
    :pswitch_0
    sget-object v3, Lkotlin/reflect/r;->a:Lkotlin/reflect/r$a;

    move-object v1, v4

    check-cast v1, Lkotlin/reflect/p;

    invoke-virtual {v3, v1}, Lkotlin/reflect/r$a;->a(Lkotlin/reflect/p;)Lkotlin/reflect/r;

    move-result-object v1

    goto :goto_2

    .line 106
    :pswitch_1
    sget-object v1, Lkotlin/reflect/r;->a:Lkotlin/reflect/r$a;

    check-cast v4, Lkotlin/reflect/p;

    invoke-virtual {v1, v4}, Lkotlin/reflect/r$a;->b(Lkotlin/reflect/p;)Lkotlin/reflect/r;

    move-result-object v1

    goto :goto_2

    .line 107
    :pswitch_2
    sget-object v1, Lkotlin/reflect/r;->a:Lkotlin/reflect/r$a;

    check-cast v4, Lkotlin/reflect/p;

    invoke-virtual {v1, v4}, Lkotlin/reflect/r$a;->c(Lkotlin/reflect/p;)Lkotlin/reflect/r;

    move-result-object v1

    goto :goto_2

    .line 130
    :cond_2
    check-cast v2, Ljava/util/List;

    goto/16 :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/y$a;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
