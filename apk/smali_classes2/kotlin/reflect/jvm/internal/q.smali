.class public final Lkotlin/reflect/jvm/internal/q;
.super Lkotlin/reflect/jvm/internal/u;
.source "KProperty1Impl.kt"

# interfaces
.implements Lkotlin/reflect/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlin/reflect/jvm/internal/u",
        "<TT;TR;>;",
        "Lkotlin/reflect/i",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0004\u0008\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00032\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0004:\u0001\u001dB)\u0008\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000cB\u0017\u0008\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001d\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u001cR \u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00118VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R9\u0010\u0014\u001a-\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001 \u0016*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00110\u00110\u0015\u00a2\u0006\u0002\u0008\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/KMutableProperty1Impl;",
        "T",
        "R",
        "Lkotlin/reflect/jvm/internal/KProperty1Impl;",
        "Lkotlin/reflect/KMutableProperty1;",
        "container",
        "Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;",
        "name",
        "",
        "signature",
        "boundReceiver",
        "",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "(Lkotlin/reflect/jvm/internal/KDeclarationContainerImpl;Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;)V",
        "setter",
        "Lkotlin/reflect/jvm/internal/KMutableProperty1Impl$Setter;",
        "getSetter",
        "()Lkotlin/reflect/jvm/internal/KMutableProperty1Impl$Setter;",
        "setter_",
        "Lkotlin/reflect/jvm/internal/ReflectProperties$LazyVal;",
        "kotlin.jvm.PlatformType",
        "Lorg/jetbrains/annotations/NotNull;",
        "set",
        "",
        "receiver",
        "value",
        "(Ljava/lang/Object;Ljava/lang/Object;)V",
        "Setter",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/ae$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/ae$b",
            "<",
            "Lkotlin/reflect/jvm/internal/q$a",
            "<TT;TR;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signature"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/u;-><init>(Lkotlin/reflect/jvm/internal/n;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 51
    new-instance v0, Lkotlin/reflect/jvm/internal/q$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/q$b;-><init>(Lkotlin/reflect/jvm/internal/q;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/q;->b:Lkotlin/reflect/jvm/internal/ae$b;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)V
    .locals 1

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/u;-><init>(Lkotlin/reflect/jvm/internal/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)V

    .line 51
    new-instance v0, Lkotlin/reflect/jvm/internal/q$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/q$b;-><init>(Lkotlin/reflect/jvm/internal/q;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/ae;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/ae$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/q;->b:Lkotlin/reflect/jvm/internal/ae$b;

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/q$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/q$a",
            "<TT;TR;>;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/q;->b:Lkotlin/reflect/jvm/internal/ae$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/ae$b;->a()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "setter_()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/q$a;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TR;)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/q;->a()Lkotlin/reflect/jvm/internal/q$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/q$a;->a([Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public synthetic l()Lkotlin/reflect/i$a;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/q;->a()Lkotlin/reflect/jvm/internal/q$a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/i$a;

    return-object v0
.end method

.method public synthetic m()Lkotlin/reflect/g$a;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/q;->a()Lkotlin/reflect/jvm/internal/q$a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/g$a;

    return-object v0
.end method
