.class public final Lkotlin/reflect/jvm/internal/i$c;
.super Lkotlin/reflect/jvm/internal/i;
.source "RuntimeTypeMapper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\u0019\u001a\u00020\u0016H\u0016J\u0008\u0010\u001a\u001a\u00020\u0016H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006\u001b"
    }
    d2 = {
        "Lkotlin/reflect/jvm/internal/JvmPropertySignature$KotlinProperty;",
        "Lkotlin/reflect/jvm/internal/JvmPropertySignature;",
        "descriptor",
        "Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "proto",
        "Lorg/jetbrains/kotlin/serialization/ProtoBuf$Property;",
        "signature",
        "Lorg/jetbrains/kotlin/serialization/jvm/JvmProtoBuf$JvmPropertySignature;",
        "nameResolver",
        "Lorg/jetbrains/kotlin/serialization/deserialization/NameResolver;",
        "typeTable",
        "Lorg/jetbrains/kotlin/serialization/deserialization/TypeTable;",
        "(Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;Lorg/jetbrains/kotlin/serialization/ProtoBuf$Property;Lorg/jetbrains/kotlin/serialization/jvm/JvmProtoBuf$JvmPropertySignature;Lorg/jetbrains/kotlin/serialization/deserialization/NameResolver;Lorg/jetbrains/kotlin/serialization/deserialization/TypeTable;)V",
        "getDescriptor",
        "()Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;",
        "getNameResolver",
        "()Lorg/jetbrains/kotlin/serialization/deserialization/NameResolver;",
        "getProto",
        "()Lorg/jetbrains/kotlin/serialization/ProtoBuf$Property;",
        "getSignature",
        "()Lorg/jetbrains/kotlin/serialization/jvm/JvmProtoBuf$JvmPropertySignature;",
        "string",
        "",
        "getTypeTable",
        "()Lorg/jetbrains/kotlin/serialization/deserialization/TypeTable;",
        "asString",
        "getManglingSuffix",
        "kotlin-reflection"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lkotlin/reflect/jvm/internal/impl/b/ag;

.field private final c:Lkotlin/reflect/jvm/internal/impl/i/e$o;

.field private final d:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

.field private final e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

.field private final f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)V
    .locals 4

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signature"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    const/4 v0, 0x0

    .line 107
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/i;-><init>(Lkotlin/d/b/i;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/i$c;->b:Lkotlin/reflect/jvm/internal/impl/b/ag;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/i$c;->c:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/i$c;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/i$c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/i$c;->f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    .line 111
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/i$c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/i$c;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->q()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->m()I

    move-result v2

    invoke-interface {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/i$c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/i$c;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->q()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->o()I

    move-result v2

    invoke-interface {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->a:Ljava/lang/String;

    .line 119
    :goto_0
    return-void

    .line 116
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/i$c;->c:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/i$c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/i$c;->f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    invoke-virtual {v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/c/d$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 117
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d$a;->b()Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d$a;->c()Ljava/lang/String;

    move-result-object v0

    .line 118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/i$c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->a:Ljava/lang/String;

    goto :goto_0

    .line 117
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No field signature for property: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/i$c;->b:Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/ac;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final g()Ljava/lang/String;
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->b:Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/i$c;->b:Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    if-eqz v1, :cond_1

    .line 125
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    .line 127
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/c/c;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/i$c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c/c;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "classProto.getExtension(\u2026ProtoBuf.classModuleName)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 130
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "$"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "moduleName"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 139
    :goto_1
    return-object v0

    .line 129
    :cond_0
    const-string v0, "main"

    goto :goto_0

    .line 132
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/i$c;->b:Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_3

    .line 133
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->b:Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.serialization.deserialization.descriptors.DeserializedPropertyDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->I()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v1

    .line 134
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/d/b/p;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/p;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/p;->d()Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "$"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/b/p;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/p;->b()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 139
    :cond_3
    const-string v0, ""

    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->b:Lkotlin/reflect/jvm/internal/impl/b/ag;

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->c:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/i/b/x;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    return-object v0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/i/b/ah;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/i$c;->f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    return-object v0
.end method
