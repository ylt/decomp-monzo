.class public final Lkotlin/reflect/jvm/internal/ak;
.super Ljava/lang/Object;
.source "util.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000Z\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u001b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a&\u0010\u0004\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0000\u001a\"\u0010\u000b\u001a\u0002H\u000c\"\u0004\u0008\u0000\u0010\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u000eH\u0080\u0008\u00a2\u0006\u0002\u0010\u000f\u001a\u0014\u0010\u0010\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0011*\u0004\u0018\u00010\u0012H\u0000\u001a\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014*\u0004\u0018\u00010\u0012H\u0000\u001a\u0014\u0010\u0015\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0016*\u0004\u0018\u00010\u0012H\u0000\u001a\u0012\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018*\u00020\u001aH\u0000\u001a\u0012\u0010\u001b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0005*\u00020\u001cH\u0000\u001a\u000e\u0010\u001d\u001a\u0004\u0018\u00010\u001e*\u00020\u001fH\u0000\"\u0014\u0010\u0000\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\u00a8\u0006 "
    }
    d2 = {
        "JVM_STATIC",
        "Lorg/jetbrains/kotlin/name/FqName;",
        "getJVM_STATIC",
        "()Lorg/jetbrains/kotlin/name/FqName;",
        "loadClass",
        "Ljava/lang/Class;",
        "classLoader",
        "Ljava/lang/ClassLoader;",
        "packageName",
        "",
        "className",
        "reflectionCall",
        "R",
        "block",
        "Lkotlin/Function0;",
        "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;",
        "asKCallableImpl",
        "Lkotlin/reflect/jvm/internal/KCallableImpl;",
        "",
        "asKFunctionImpl",
        "Lkotlin/reflect/jvm/internal/KFunctionImpl;",
        "asKPropertyImpl",
        "Lkotlin/reflect/jvm/internal/KPropertyImpl;",
        "computeAnnotations",
        "",
        "",
        "Lorg/jetbrains/kotlin/descriptors/annotations/Annotated;",
        "toJavaClass",
        "Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;",
        "toKVisibility",
        "Lkotlin/reflect/KVisibility;",
        "Lorg/jetbrains/kotlin/descriptors/Visibility;",
        "kotlin-reflection"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x5
    }
.end annotation


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.jvm.JvmStatic"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/ak;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void
.end method

.method public static final a(Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ClassLoader;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    const-string v0, "classLoader"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "className"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    const-string v0, "kotlin"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v1, 0x2e

    const/16 v2, 0x24

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p2

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/d/b;->a(Ljava/lang/ClassLoader;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    :goto_0
    return-object v0

    .line 66
    :sswitch_0
    const-string v0, "IntArray"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const-class v0, [I

    goto :goto_0

    .line 66
    :sswitch_1
    const-string v0, "CharArray"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-class v0, [C

    goto :goto_0

    .line 66
    :sswitch_2
    const-string v0, "BooleanArray"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    const-class v0, [Z

    goto :goto_0

    .line 66
    :sswitch_3
    const-string v0, "LongArray"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const-class v0, [J

    goto :goto_0

    .line 66
    :sswitch_4
    const-string v0, "ShortArray"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-class v0, [S

    goto :goto_0

    .line 66
    :sswitch_5
    const-string v0, "ByteArray"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const-class v0, [B

    goto :goto_0

    .line 66
    :sswitch_6
    const-string v0, "Array"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-class v0, [Ljava/lang/Object;

    goto :goto_0

    .line 66
    :sswitch_7
    const-string v0, "DoubleArray"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const-class v0, [D

    goto :goto_0

    .line 66
    :sswitch_8
    const-string v0, "FloatArray"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-class v0, [F

    goto :goto_0

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        -0x35c13ccf -> :sswitch_2
        -0x2d7eb8a3 -> :sswitch_4
        -0x2d0e4b7d -> :sswitch_1
        -0x47759ef -> :sswitch_5
        0x15568e8 -> :sswitch_7
        0x3c98239 -> :sswitch_6
        0x23deebca -> :sswitch_0
        0x388e557d -> :sswitch_8
        0x7d6d891d -> :sswitch_3
    .end sparse-switch
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Class;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    .line 45
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/b/w;

    if-eqz v1, :cond_1

    .line 46
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/w;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/w;->b()Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.load.kotlin.reflect.ReflectKotlinClass"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->d()Ljava/lang/Class;

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    .line 48
    :cond_1
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;

    if-eqz v1, :cond_3

    .line 49
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.load.java.structure.reflect.ReflectJavaClass"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->u()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_3
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 55
    :cond_4
    :goto_1
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v1

    .line 56
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->b()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->a(Ljava/lang/Class;)Ljava/lang/ClassLoader;

    move-result-object v2

    const-string v3, "packageName"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "className"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v1, v0}, Lkotlin/reflect/jvm/internal/ak;->a(Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v0, p0

    .line 54
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/d/b/l;->a()V

    goto :goto_1
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a/a;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a/a;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 121
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 130
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 129
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 93
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->c()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    .line 95
    instance-of v4, v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;

    if-eqz v4, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;->b()Ljava/lang/annotation/Annotation;

    move-result-object v0

    .line 98
    :goto_1
    if-eqz v0, :cond_0

    .line 129
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_1
    instance-of v4, v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;

    if-eqz v4, :cond_4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;

    move-result-object v0

    instance-of v4, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    if-nez v4, :cond_2

    move-object v0, v2

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->e()Ljava/lang/annotation/Annotation;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 97
    goto :goto_1

    .line 132
    :cond_5
    check-cast v1, Ljava/util/List;

    .line 99
    return-object v1
.end method

.method public static final a()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lkotlin/reflect/jvm/internal/ak;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method

.method public static final a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/o;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 111
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/o;

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/o;

    if-eqz v0, :cond_0

    .line 112
    :goto_1
    return-object v0

    :cond_0
    instance-of v0, p0, Lkotlin/d/b/k;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_2
    check-cast v0, Lkotlin/d/b/k;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/d/b/k;->f()Lkotlin/reflect/b;

    move-result-object v0

    :goto_3
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/o;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/o;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_3

    :cond_3
    move-object v0, p0

    goto :goto_2

    :cond_4
    move-object v0, p0

    goto :goto_0
.end method

.method public static final b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lkotlin/reflect/jvm/internal/w",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 115
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/w;

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/w;

    if-eqz v0, :cond_0

    .line 116
    :goto_1
    return-object v0

    :cond_0
    instance-of v0, p0, Lkotlin/d/b/s;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_2
    check-cast v0, Lkotlin/d/b/s;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/d/b/s;->f()Lkotlin/reflect/b;

    move-result-object v0

    :goto_3
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/w;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/w;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_3

    :cond_3
    move-object v0, p0

    goto :goto_2

    :cond_4
    move-object v0, p0

    goto :goto_0
.end method

.method public static final c(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lkotlin/reflect/jvm/internal/j",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 119
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/j;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/j;

    if-eqz v0, :cond_0

    :goto_1
    if-eqz v0, :cond_1

    :goto_2
    return-object v0

    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/ak;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/j;

    goto :goto_1

    :cond_1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/ak;->b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/j;

    goto :goto_2

    :cond_2
    move-object v0, p0

    goto :goto_0
.end method
