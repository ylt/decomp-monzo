.class public final Lkotlin/reflect/jvm/internal/impl/c/a;
.super Ljava/lang/Object;
.source "utils.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 6

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scopeOwner"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a:Lkotlin/reflect/jvm/internal/impl/c/a/c$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/c/a/c$a;->a()Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-result-object v0

    if-ne p0, v0, :cond_1

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/c/a/b;->a()Lkotlin/reflect/jvm/internal/impl/c/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 33
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/c/a/a;->b()Lkotlin/reflect/jvm/internal/impl/c/a/e;

    move-result-object v2

    .line 34
    :goto_1
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/c/a/a;->a()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->a()Ljava/lang/String;

    move-result-object v3

    const-string v0, "DescriptorUtils.getFqName(scopeOwner).asString()"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/c/a/f;->b:Lkotlin/reflect/jvm/internal/impl/c/a/f;

    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v5

    const-string v0, "name.asString()"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    invoke-interface/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/c/a/e;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/c/a/f;Ljava/lang/String;)V

    goto :goto_0

    .line 33
    :cond_2
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/e;->a:Lkotlin/reflect/jvm/internal/impl/c/a/e$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/c/a/e$a;->a()Lkotlin/reflect/jvm/internal/impl/c/a/e;

    move-result-object v2

    goto :goto_1
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 6

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scopeOwner"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a:Lkotlin/reflect/jvm/internal/impl/c/a/c$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/c/a/c$a;->a()Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-result-object v0

    if-ne p0, v0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/c/a/b;->a()Lkotlin/reflect/jvm/internal/impl/c/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 40
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/c/a/a;->b()Lkotlin/reflect/jvm/internal/impl/c/a/e;

    move-result-object v2

    .line 41
    :goto_1
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/c/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v3

    const-string v0, "scopeOwner.fqName.asString()"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/c/a/f;->a:Lkotlin/reflect/jvm/internal/impl/c/a/f;

    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v5

    const-string v0, "name.asString()"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    invoke-interface/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/c/a/e;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/c/a/f;Ljava/lang/String;)V

    goto :goto_0

    .line 40
    :cond_2
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/e;->a:Lkotlin/reflect/jvm/internal/impl/c/a/e$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/c/a/e$a;->a()Lkotlin/reflect/jvm/internal/impl/c/a/e;

    move-result-object v2

    goto :goto_1
.end method
