.class public final Lkotlin/reflect/jvm/internal/impl/c/a/e;
.super Ljava/lang/Object;
.source "LookupLocation.kt"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/c/a/e$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/c/a/e$a;

.field private static final d:Lkotlin/reflect/jvm/internal/impl/c/a/e;


# instance fields
.field private final b:I

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/c/a/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/c/a/e$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->a:Lkotlin/reflect/jvm/internal/impl/c/a/e$a;

    .line 34
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/c/a/e;

    invoke-direct {v0, v2, v2}, Lkotlin/reflect/jvm/internal/impl/c/a/e;-><init>(II)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/c/a/e;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->b:I

    iput p2, p0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->c:I

    return-void
.end method

.method public static final synthetic a()Lkotlin/reflect/jvm/internal/impl/c/a/e;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/c/a/e;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lkotlin/reflect/jvm/internal/impl/c/a/e;

    if-eqz v2, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/c/a/e;

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->b:I

    iget v3, p1, Lkotlin/reflect/jvm/internal/impl/c/a/e;->b:I

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->c:I

    iget v3, p1, Lkotlin/reflect/jvm/internal/impl/c/a/e;->c:I

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->b:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->c:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Position(line="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", column="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/c/a/e;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
