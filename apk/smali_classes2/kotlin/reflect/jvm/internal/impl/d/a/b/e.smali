.class public Lkotlin/reflect/jvm/internal/impl/d/a/b/e;
.super Lkotlin/reflect/jvm/internal/impl/b/b/y;
.source "JavaPropertyDescriptor.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/b/a;


# instance fields
.field private final e:Z


# direct methods
.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)V
    .locals 18

    .prologue
    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "containingDeclaration"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "<init>"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "annotations"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "<init>"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    if-nez p3, :cond_2

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "modality"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "<init>"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    if-nez p4, :cond_3

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "visibility"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "<init>"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    if-nez p6, :cond_4

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "name"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "<init>"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    if-nez p7, :cond_5

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "source"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "<init>"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5
    if-nez p9, :cond_6

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "kind"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "<init>"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 48
    :cond_6
    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p8

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p9

    move-object/from16 v11, p7

    invoke-direct/range {v2 .. v17}, Lkotlin/reflect/jvm/internal/impl/b/b/y;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;ZZZZZZ)V

    .line 51
    move/from16 v0, p10

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->e:Z

    .line 52
    return-void
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Z)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;
    .locals 11

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "containingDeclaration"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "annotations"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "modality"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "visibility"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "source"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_5
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    const/4 v8, 0x0

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v10, p7

    invoke-direct/range {v0 .. v10}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)V

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 152
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->e:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/j;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->l(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H_()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/b/y;
    .locals 11

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newOwner"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newModality"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newVisibility"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p5, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kind"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_3
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->y()Z

    move-result v5

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v6

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    iget-boolean v10, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->e:Z

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v8, p4

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v10}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)V

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/b/a;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/d/a/b/a;"
        }
    .end annotation

    .prologue
    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "enhancedValueParametersTypes"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "enhance"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-nez p3, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "enhancedReturnType"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "enhance"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 97
    :cond_1
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v4

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v5

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->y()Z

    move-result v6

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v7

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v8

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v9

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v10

    iget-boolean v11, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->e:Z

    invoke-direct/range {v1 .. v11}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)V

    .line 110
    const/4 v2, 0x0

    .line 111
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->C()Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-result-object v11

    .line 112
    if-eqz v11, :cond_5

    .line 113
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v5

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v6

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->o()Z

    move-result v7

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->v()Z

    move-result v8

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a()Z

    move-result v9

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v10

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v12

    move-object v3, v1

    invoke-direct/range {v2 .. v12}, Lkotlin/reflect/jvm/internal/impl/b/b/z;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZZZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/ah;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 117
    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)V

    .line 118
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v13, v2

    .line 121
    :goto_0
    const/4 v2, 0x0

    .line 122
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->c()Lkotlin/reflect/jvm/internal/impl/b/ai;

    move-result-object v11

    .line 123
    if-eqz v11, :cond_2

    .line 124
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    invoke-interface {v11}, Lkotlin/reflect/jvm/internal/impl/b/ai;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-interface {v11}, Lkotlin/reflect/jvm/internal/impl/b/ai;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v5

    invoke-interface {v11}, Lkotlin/reflect/jvm/internal/impl/b/ai;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v6

    invoke-interface {v11}, Lkotlin/reflect/jvm/internal/impl/b/ai;->o()Z

    move-result v7

    invoke-interface {v11}, Lkotlin/reflect/jvm/internal/impl/b/ai;->v()Z

    move-result v8

    invoke-interface {v11}, Lkotlin/reflect/jvm/internal/impl/b/ai;->a()Z

    move-result v9

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v10

    invoke-interface {v11}, Lkotlin/reflect/jvm/internal/impl/b/ai;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v12

    move-object v3, v1

    invoke-direct/range {v2 .. v12}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZZZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/ai;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 128
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)V

    .line 129
    invoke-interface {v11}, Lkotlin/reflect/jvm/internal/impl/b/ai;->i()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/b/at;)V

    .line 132
    :cond_2
    invoke-virtual {v1, v13, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/b/b/z;Lkotlin/reflect/jvm/internal/impl/b/ai;)V

    .line 133
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->D()Z

    move-result v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Z)V

    .line 134
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a:Lkotlin/reflect/jvm/internal/impl/j/g;

    if-eqz v2, :cond_3

    .line 135
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a:Lkotlin/reflect/jvm/internal/impl/j/g;

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/j/g;)V

    .line 138
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->k()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Ljava/util/Collection;)V

    .line 140
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->e()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v1, v0, v2, v3, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 146
    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "@NotNull method %s.%s must not return null"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaPropertyDescriptor"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "enhance"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    return-object v1

    :cond_5
    move-object v13, v2

    goto/16 :goto_0
.end method
