.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;
.source "ReflectJavaPackage.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/t;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/b;)V
    .locals 1

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/g;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "nameFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method

.method public b()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 36
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
