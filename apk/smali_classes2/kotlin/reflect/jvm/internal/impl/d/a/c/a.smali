.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a;
.super Ljava/lang/Object;
.source "context.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;I)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingDeclaration"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameterOwner"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;I)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    return-object v0
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;IILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 86
    const/4 p3, 0x0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;I)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/b;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "components"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)V

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameterResolver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)V

    return-object v0
.end method
