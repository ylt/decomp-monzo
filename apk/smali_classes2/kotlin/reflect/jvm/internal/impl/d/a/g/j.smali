.class public final Lkotlin/reflect/jvm/internal/impl/d/a/g/j;
.super Ljava/lang/Object;
.source "predefinedEnhancementInfo.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x3

    invoke-direct {p0, v1, v1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;Ljava/util/List;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/q;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "parametersInfo"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;->b:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;Ljava/util/List;ILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    .line 29
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    :goto_0
    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_0

    .line 30
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object p2

    :cond_0
    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;Ljava/util/List;)V

    return-void

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/d/a/g/q;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;->b:Ljava/util/List;

    return-object v0
.end method
