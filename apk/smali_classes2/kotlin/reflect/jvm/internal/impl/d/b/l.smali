.class public Lkotlin/reflect/jvm/internal/impl/d/b/l;
.super Ljava/lang/Object;
.source "typeSignatureMapping.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/b/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/d/b/r",
            "<TT;>;"
        }
    .end annotation
.end field


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 258
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a:I

    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a:I

    .line 260
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    const-string v0, "objectType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->b(Ljava/lang/Object;)V

    .line 268
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "TT;)V"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    invoke-virtual {p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->b(Ljava/lang/Object;)V

    .line 278
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method protected final b(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 272
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->c:Lkotlin/reflect/jvm/internal/impl/d/b/r;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "["

    check-cast v0, Ljava/lang/CharSequence;

    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a:I

    invoke-static {v0, v3}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->c:Lkotlin/reflect/jvm/internal/impl/d/b/r;

    invoke-interface {v2, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/l;->b:Ljava/lang/Object;

    .line 274
    :cond_0
    return-void
.end method
