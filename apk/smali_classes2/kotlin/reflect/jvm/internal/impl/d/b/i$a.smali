.class public final Lkotlin/reflect/jvm/internal/impl/d/b/i$a;
.super Ljava/lang/Object;
.source "JvmBuiltInsSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 325
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/i$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->g()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/i$a;Lkotlin/reflect/jvm/internal/impl/e/c;)Z
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->b(Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    return v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/b/i$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->h()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/e/c;)Z
    .locals 1

    .prologue
    .line 341
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->h:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final g()Ljava/util/Set;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 383
    nop

    .line 495
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 384
    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/jvm/internal/impl/h/d/c;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v2, v1, v8

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/d/c;->b:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v2, v1, v9

    invoke-static {v1}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 496
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 497
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 385
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "it.wrapperFqName.shortName().asString()"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v5, v9, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Value()"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    invoke-virtual {v0, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 498
    invoke-static {v2, v1}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 500
    :cond_0
    check-cast v2, Ljava/util/LinkedHashSet;

    .line 495
    check-cast v2, Ljava/util/Set;

    .line 387
    return-object v2
.end method

.method private final h()Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 464
    nop

    .line 501
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 465
    const/16 v1, 0x8

    new-array v1, v1, [Lkotlin/reflect/jvm/internal/impl/h/d/c;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v2, v1, v6

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v2, v1, v7

    const/4 v2, 0x2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->h:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->f:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->g:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/d/c;->d:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    aput-object v3, v1, v2

    invoke-static {v1}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 467
    nop

    .line 502
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 503
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 469
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v4

    const-string v1, "it.wrapperFqName.shortName().asString()"

    invoke-static {v4, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v1, v7, [Ljava/lang/String;

    const-string v5, "Ljava/lang/String;"

    aput-object v5, v1, v6

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v5, v1

    invoke-static {v1, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 504
    invoke-static {v2, v1}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 506
    :cond_0
    check-cast v2, Ljava/util/LinkedHashSet;

    .line 501
    check-cast v2, Ljava/util/Set;

    .line 471
    return-object v2
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/c;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    const-string v1, "fqName"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 327
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->b(Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    const/4 v0, 0x1

    .line 337
    :cond_0
    :goto_0
    return v0

    .line 330
    :cond_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    invoke-virtual {v1, p1}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 331
    nop

    .line 332
    :try_start_0
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 337
    const-class v1, Ljava/io/Serializable;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    goto :goto_0

    .line 334
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->d()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->e()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 458
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->f()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
