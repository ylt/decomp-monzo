.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d$a;
.super Lkotlin/d/b/m;
.source "JvmPackageScope.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/t;Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 100
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 109
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 108
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u;

    .line 45
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->d()Lkotlin/reflect/jvm/internal/impl/d/b/e;

    move-result-object v4

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-virtual {v4, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 108
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 46
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d$a;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
