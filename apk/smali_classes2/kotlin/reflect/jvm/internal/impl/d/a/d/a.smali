.class public final Lkotlin/reflect/jvm/internal/impl/d/a/d/a;
.super Ljava/lang/Object;
.source "ReflectJavaClassFinder.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/e;


# instance fields
.field private final a:Ljava/lang/ClassLoader;


# direct methods
.method public constructor <init>(Ljava/lang/ClassLoader;)V
    .locals 1

    .prologue
    const-string v0, "classLoader"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/d/a;->a:Ljava/lang/ClassLoader;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/a/f/g;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v6

    .line 30
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->b()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    const/16 v2, 0x24

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/d/a;->a:Ljava/lang/ClassLoader;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/d/b;->a(Ljava/lang/ClassLoader;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 36
    if-eqz v1, :cond_1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;-><init>(Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    :goto_1
    return-object v0

    .line 33
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v5

    .line 36
    goto :goto_1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/t;
    .locals 1

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/u;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/t;

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "packageFqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x0

    return-object v0
.end method
