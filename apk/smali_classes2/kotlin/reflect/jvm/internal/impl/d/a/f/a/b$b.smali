.class final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b$b;
.super Lkotlin/d/b/m;
.source "reflectClassUtil.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->a(Ljava/lang/reflect/Type;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Ljava/lang/reflect/ParameterizedType;",
        "Lkotlin/g/g",
        "<+",
        "Ljava/lang/reflect/Type;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b$b;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b$b;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b$b;->a(Ljava/lang/reflect/ParameterizedType;)Lkotlin/g/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/reflect/ParameterizedType;)Lkotlin/g/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/ParameterizedType;",
            ")",
            "Lkotlin/g/g",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/a/g;->l([Ljava/lang/Object;)Lkotlin/g/g;

    move-result-object v0

    return-object v0
.end method
