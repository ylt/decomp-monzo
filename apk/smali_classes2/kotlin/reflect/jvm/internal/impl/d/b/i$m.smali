.class final Lkotlin/reflect/jvm/internal/impl/d/b/i$m;
.super Lkotlin/d/b/m;
.source "JvmBuiltInsSettings.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/b;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/i;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/i;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$m;->a:Lkotlin/reflect/jvm/internal/impl/d/b/i;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$m;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 2

    .prologue
    .line 223
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$m;->a:Lkotlin/reflect/jvm/internal/impl/d/b/i;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b(Lkotlin/reflect/jvm/internal/impl/d/b/i;)Lkotlin/reflect/jvm/internal/impl/f/a;

    move-result-object v1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
