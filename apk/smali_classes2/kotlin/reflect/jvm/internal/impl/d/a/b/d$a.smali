.class final enum Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;
.super Ljava/lang/Enum;
.source "JavaMethodDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/b/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

.field private static final synthetic g:[Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;


# instance fields
.field public final e:Z

.field public final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    const-string v1, "NON_STABLE_DECLARED"

    invoke-direct {v0, v1, v2, v2, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    .line 33
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    const-string v1, "STABLE_DECLARED"

    invoke-direct {v0, v1, v3, v3, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    .line 34
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    const-string v1, "NON_STABLE_SYNTHESIZED"

    invoke-direct {v0, v1, v4, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    const-string v1, "STABLE_SYNTHESIZED"

    invoke-direct {v0, v1, v5, v3, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->d:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->d:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->g:[Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->e:Z

    .line 43
    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->f:Z

    .line 44
    return-void
.end method

.method public static a(ZZ)Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;
    .locals 5

    .prologue
    .line 48
    if-eqz p0, :cond_1

    if-eqz p1, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->d:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaMethodDescriptor$ParameterNamesStatus"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "get"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    goto :goto_0

    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->g:[Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/b/d$a;

    return-object v0
.end method
