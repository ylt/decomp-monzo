.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;
.super Lkotlin/d/b/m;
.source "LazyJavaPackageScope.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/t;Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;",
        "Lkotlin/reflect/jvm/internal/impl/b/e;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 57
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->c()Lkotlin/reflect/jvm/internal/impl/d/b/t;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    move-object v1, v0

    .line 62
    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 65
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->f()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 93
    :cond_0
    :goto_2
    return-object v4

    .line 60
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->c()Lkotlin/reflect/jvm/internal/impl/d/b/t;

    move-result-object v0

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v4

    .line 62
    goto :goto_1

    .line 67
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;

    move-result-object v0

    .line 70
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$a;

    if-eqz v1, :cond_4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$a;->a()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v4

    goto :goto_2

    .line 71
    :cond_4
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$c;

    if-nez v1, :cond_0

    .line 72
    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$b;

    if-eqz v0, :cond_a

    .line 73
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 75
    :goto_3
    if-eqz v3, :cond_6

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    move-result-object v0

    :goto_4
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 76
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t find kotlin binary class for light class created by kotlin binary file\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "JavaClass: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ClassId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "findKotlinClass(JavaClass) = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->c()Lkotlin/reflect/jvm/internal/impl/d/b/t;

    move-result-object v5

    invoke-interface {v5, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findKotlinClass(ClassId) = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->c()Lkotlin/reflect/jvm/internal/impl/d/b/t;

    move-result-object v4

    invoke-interface {v4, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 73
    :cond_5
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->b()Lkotlin/reflect/jvm/internal/impl/d/a/e;

    move-result-object v0

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/e;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v3

    goto/16 :goto_3

    :cond_6
    move-object v0, v4

    .line 75
    goto/16 :goto_4

    .line 86
    :cond_7
    if-eqz v3, :cond_0

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v5

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_5
    sget-boolean v5, Lkotlin/p;->a:Z

    if-eqz v5, :cond_9

    if-nez v0, :cond_9

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Java class by request "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " should be contained in package "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", but it\'s fq-name: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 87
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_5

    .line 91
    :cond_9
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    const/16 v5, 0x8

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/b/e;ILkotlin/d/b/i;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v4, v0

    goto/16 :goto_2

    :cond_a
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
