.class public final Lkotlin/reflect/jvm/internal/impl/d/b/ad;
.super Ljava/lang/Object;
.source "methodSignatureMapping.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/ac;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/d/b/ac",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/b/q;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Lkotlin/reflect/jvm/internal/impl/d/b/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/ad;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ad;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/b/ad;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/b/ad;->b:Lkotlin/reflect/jvm/internal/impl/d/b/ad;

    return-void
.end method


# virtual methods
.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/ad;->c(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public a()Lkotlin/d/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/m",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/ac$b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/ac;)Lkotlin/d/a/m;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/r;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v0, "types"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    new-instance v9, Ljava/lang/AssertionError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There should be no intersection type in existing descriptors, but found: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    const/4 v4, 0x0

    const/16 v7, 0x3f

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    move-object v6, v1

    move-object v8, v1

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    move-object v0, v9

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/e;)V
    .locals 1

    .prologue
    const-string v0, "kotlinType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Void;
    .locals 1

    .prologue
    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x0

    return-object v0
.end method
