.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;
.super Lkotlin/reflect/jvm/internal/impl/d/a/c/a/m;
.source "LazyJavaPackageScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;,
        Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;
    }
.end annotation


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/g",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/j/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/d",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lkotlin/reflect/jvm/internal/impl/d/a/f/t;

.field private final f:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/t;Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V
    .locals 2

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jPackage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownerDescriptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/m;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->e:Lkotlin/reflect/jvm/internal/impl/d/a/f/t;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    .line 48
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$e;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/g;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->b:Lkotlin/reflect/jvm/internal/impl/j/g;

    .line 52
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->d:Lkotlin/reflect/jvm/internal/impl/j/d;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 134
    :goto_0
    return-object v0

    .line 129
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->b:Lkotlin/reflect/jvm/internal/impl/j/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/g;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 130
    if-nez p2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 131
    goto :goto_0

    .line 134
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->d:Lkotlin/reflect/jvm/internal/impl/j/d;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;

    invoke-direct {v1, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)V

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/j/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;
    .locals 2

    .prologue
    .line 104
    if-nez p1, :cond_0

    .line 105
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;

    .line 115
    :goto_0
    return-object v0

    .line 107
    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->d()Lkotlin/reflect/jvm/internal/impl/d/b/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    .line 109
    if-eqz v1, :cond_1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$a;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;

    goto :goto_0

    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;

    goto :goto_0

    .line 113
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b$c;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$b;

    goto :goto_0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->m:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-virtual {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    const-string v0, "javaClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->i()Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$d;

    invoke-direct {v1, p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    check-cast v1, Lkotlin/d/a/a;

    invoke-interface {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/m;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/d/a/b/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 166
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/f;

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 167
    :cond_0
    return-void
.end method

.method protected c(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b/g;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    .line 160
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->f:Lkotlin/reflect/jvm/internal/impl/h/e/d;

    invoke-virtual {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->d(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method protected c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    return-object v0
.end method

.method protected d(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v1

    .line 183
    :goto_0
    return-object v1

    .line 147
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->b:Lkotlin/reflect/jvm/internal/impl/j/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/g;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 148
    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 177
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 178
    check-cast v0, Ljava/lang/String;

    .line 148
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 179
    :cond_1
    check-cast v1, Ljava/util/Set;

    goto :goto_0

    .line 150
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->e:Lkotlin/reflect/jvm/internal/impl/d/a/f/t;

    if-eqz p2, :cond_4

    :goto_2
    invoke-interface {v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/t;->a(Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 181
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 180
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    .line 151
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v0, 0x0

    :goto_4
    if-eqz v0, :cond_3

    .line 180
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 150
    :cond_4
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/d;->b()Lkotlin/d/a/b;

    move-result-object p2

    goto :goto_2

    .line 151
    :cond_5
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    goto :goto_4

    .line 183
    :cond_6
    check-cast v1, Ljava/util/Set;

    goto :goto_0
.end method

.method public d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method protected d()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    return-object v0
.end method

.method protected e(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method
