.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f$a;
.super Ljava/lang/Object;
.source "JavaIncompatibilityRulesOverridabilityCondition.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;-><init>()V

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/at;)Lkotlin/reflect/jvm/internal/impl/d/b/q;
    .locals 1

    .prologue
    .line 144
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z

    move-result v0

    if-nez v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    :cond_0
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    .line 147
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 153
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 159
    :cond_0
    :goto_0
    return v4

    .line 155
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_2

    move-object v0, v2

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_0

    .line 157
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    instance-of v5, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v5, :cond_3

    move-object v1, v2

    :cond_3
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    .line 159
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v2

    if-eqz v2, :cond_4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    :goto_1
    move v4, v0

    goto :goto_0

    :cond_4
    move v0, v4

    goto :goto_1
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v0, "superDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-nez v0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v5

    :cond_1
    move-object v0, p2

    .line 127
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_2

    move v0, v4

    :goto_1
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 128
    const-string v1, "External overridability condition with CONFLICTS_ONLY should not be run with different value parameters size"

    .line 127
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    move v0, v5

    goto :goto_1

    :cond_3
    move-object v0, p2

    .line 131
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;->o()Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    move-object v1, p1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v1

    const-string v2, "superDescriptor.original.valueParameters"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lkotlin/h;

    invoke-virtual {v1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-virtual {v1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/at;

    move-object v2, p0

    .line 132
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;

    move-object v3, p2

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/s;

    const-string v7, "subParameter"

    invoke-static {v0, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/at;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    move-object v0, p0

    .line 133
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;

    move-object v2, p1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/s;

    const-string v7, "superParameter"

    invoke-static {v1, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v2, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/at;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    .line 135
    if-eq v3, v0, :cond_4

    move v5, v4

    .line 136
    goto/16 :goto_0
.end method
