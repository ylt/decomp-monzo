.class final Lkotlin/reflect/jvm/internal/impl/d/b/i$j;
.super Ljava/lang/Object;
.source "JvmBuiltInsSettings.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/utils/b$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/d/b/i$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<N:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/utils/b$b",
        "<TN;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/i;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/i;)V
    .locals 0

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$j;->a:Lkotlin/reflect/jvm/internal/impl/d/b/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 54
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$j;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 237
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 495
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 504
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 503
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 238
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/h;->f()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    :goto_1
    instance-of v4, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v4, :cond_1

    move-object v0, v2

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_3

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$j;->a:Lkotlin/reflect/jvm/internal/impl/d/b/i;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/d/b/i;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    .line 503
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 238
    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_2

    .line 506
    :cond_4
    check-cast v1, Ljava/util/List;

    .line 239
    return-object v1
.end method
