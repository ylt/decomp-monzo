.class public final Lkotlin/reflect/jvm/internal/impl/d/a/a/a;
.super Ljava/lang/Object;
.source "DescriptorResolverUtils.java"


# direct methods
.method public static a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<TD;>;",
            "Ljava/util/Collection",
            "<TD;>;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/r;",
            ")",
            "Ljava/util/Collection",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v6

    const-string v3, "resolveOverridesForNonStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "membersFromSupertypes"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v6

    const-string v3, "resolveOverridesForNonStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "membersFromCurrent"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v6

    const-string v3, "resolveOverridesForNonStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "classDescriptor"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v6

    const-string v3, "resolveOverridesForNonStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez p4, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "errorReporter"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v6

    const-string v3, "resolveOverridesForNonStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 45
    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;Z)Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverridesForNonStaticMembers"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;Z)Ljava/util/Collection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<TD;>;",
            "Ljava/util/Collection",
            "<TD;>;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/r;",
            "Z)",
            "Ljava/util/Collection",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "membersFromSupertypes"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "membersFromCurrent"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "classDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez p4, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "errorReporter"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverrides"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_4
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 67
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1;

    invoke-direct {v1, p4, v0, p5}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/r;Ljava/util/Set;Z)V

    invoke-static {p0, p1, p2, p3, v1}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/h/i;)V

    .line 101
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v4

    const-string v3, "resolveOverrides"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/at;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v4

    const-string v3, "getAnnotationParameterByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "annotationClass"

    aput-object v3, v2, v5

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v4

    const-string v3, "getAnnotationParameterByName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->k()Ljava/util/Collection;

    move-result-object v0

    .line 107
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    if-eq v2, v4, :cond_2

    move-object v0, v1

    .line 115
    :goto_0
    return-object v0

    .line 109
    :cond_2
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 110
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {v3, p0}, Lkotlin/reflect/jvm/internal/impl/e/f;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 115
    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/d/a/f/p;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "member"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "isObjectMethodInInterface"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 119
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/p;->e()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    instance-of v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    if-eqz v2, :cond_1

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "method"

    aput-object v4, v3, v0

    const/4 v0, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const-string v4, "isObjectMethod"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 123
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    .line 124
    const-string v2, "toString"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "hashCode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 125
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 130
    :cond_2
    :goto_0
    return v0

    .line 127
    :cond_3
    const-string v2, "equals"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 128
    const-string v0, "java.lang.Object"

    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "method"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v2, v4, v1

    const-string v1, "isMethodWithOneParameterWithFqName"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "fqName"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v2, v4, v1

    const-string v1, "isMethodWithOneParameterWithFqName"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->c()Ljava/util/List;

    move-result-object v0

    .line 135
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_3

    .line 136
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/y;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/y;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v0

    .line 137
    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;

    if-eqz v3, :cond_3

    .line 138
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/i;

    move-result-object v0

    .line 139
    instance-of v3, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    if-eqz v3, :cond_3

    .line 140
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 145
    :goto_0
    return v0

    :cond_2
    move v0, v2

    .line 141
    goto :goto_0

    :cond_3
    move v0, v2

    .line 145
    goto :goto_0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<TD;>;",
            "Ljava/util/Collection",
            "<TD;>;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/r;",
            ")",
            "Ljava/util/Collection",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverridesForStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "membersFromSupertypes"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverridesForStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "membersFromCurrent"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverridesForStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "classDescriptor"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverridesForStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez p4, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "errorReporter"

    aput-object v3, v2, v6

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v5

    const-string v3, "resolveOverridesForStaticMembers"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 53
    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;Z)Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils"

    aput-object v3, v2, v6

    const-string v3, "resolveOverridesForStaticMembers"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v0
.end method
