.class public final Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;
.super Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;
.source "AbstractBinaryClassAnnotationAndConstantLoader.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/a$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a$c;Lkotlin/reflect/jvm/internal/impl/d/b/x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/x;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "signature"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a$c;Lkotlin/reflect/jvm/internal/impl/d/b/x;)V

    return-void
.end method


# virtual methods
.method public a(ILkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 3

    .prologue
    const-string v0, "classId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;->b()Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/x;I)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v2

    .line 275
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 276
    if-nez v0, :cond_0

    .line 277
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 278
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->b:Ljava/util/HashMap;

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a;

    invoke-static {v1, p2, p3, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v0

    return-object v0
.end method
