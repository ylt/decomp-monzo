.class public final enum Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
.super Ljava/lang/Enum;
.source "TypeUsage.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/a/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field public static final enum g:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field private static final synthetic h:[Lkotlin/reflect/jvm/internal/impl/d/a/a/o;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const-string v1, "TYPE_ARGUMENT"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 26
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const-string v1, "UPPER_BOUND"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 27
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const-string v1, "MEMBER_SIGNATURE_COVARIANT"

    invoke-direct {v0, v1, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->c:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 28
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const-string v1, "MEMBER_SIGNATURE_CONTRAVARIANT"

    invoke-direct {v0, v1, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->d:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 29
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const-string v1, "MEMBER_SIGNATURE_INVARIANT"

    invoke-direct {v0, v1, v7}, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 30
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const-string v1, "SUPERTYPE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 31
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const-string v1, "SUPERTYPE_ARGUMENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->g:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 23
    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->c:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->d:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->g:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v2, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->h:[Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->h:[Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    return-object v0
.end method
