.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;
.super Ljava/lang/Object;
.source "LazyJavaStaticClassScope.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/utils/b$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Set;Lkotlin/d/a/b;)Ljava/util/Set;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<N:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/utils/b$b",
        "<TN;>;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 37
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->q(Ljava/lang/Iterable;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e$1;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e$1;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/g/h;->f(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    .line 115
    invoke-static {v0}, Lkotlin/g/h;->g(Lkotlin/g/g;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
