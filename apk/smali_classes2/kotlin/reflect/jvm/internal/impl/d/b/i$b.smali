.class final enum Lkotlin/reflect/jvm/internal/impl/d/b/i$b;
.super Ljava/lang/Enum;
.source "JvmBuiltInsSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/b/i$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

.field private static final synthetic e:[Lkotlin/reflect/jvm/internal/impl/d/b/i$b;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    const-string v2, "BLACK_LIST"

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    aput-object v1, v0, v3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    const-string v2, "WHITE_LIST"

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    const-string v2, "NOT_CONSIDERED"

    invoke-direct {v1, v2, v5}, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    const-string v2, "DROP"

    invoke-direct {v1, v2, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->d:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    aput-object v1, v0, v6

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->e:[Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 257
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/i$b;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/b/i$b;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->e:[Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    return-object v0
.end method
