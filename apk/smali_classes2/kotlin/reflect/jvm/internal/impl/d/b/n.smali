.class public final Lkotlin/reflect/jvm/internal/impl/d/b/n;
.super Ljava/lang/Object;
.source "JvmNameResolver.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/x;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/n$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/b/n$a;

.field private static final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

.field private final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/16 v3, 0xa

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/n$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/n$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->a:Lkotlin/reflect/jvm/internal/impl/d/b/n$a;

    .line 99
    const/16 v0, 0x2c

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "kotlin/Any"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "kotlin/Nothing"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "kotlin/Unit"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "kotlin/Throwable"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "kotlin/Number"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "kotlin/Byte"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "kotlin/Double"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "kotlin/Float"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "kotlin/Int"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "kotlin/Long"

    aput-object v2, v0, v1

    const-string v1, "kotlin/Short"

    aput-object v1, v0, v3

    const/16 v1, 0xb

    const-string v2, "kotlin/Boolean"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "kotlin/Char"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "kotlin/CharSequence"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "kotlin/String"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "kotlin/Comparable"

    aput-object v2, v0, v1

    const-string v1, "kotlin/Enum"

    aput-object v1, v0, v4

    const/16 v1, 0x11

    const-string v2, "kotlin/Array"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "kotlin/ByteArray"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "kotlin/DoubleArray"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "kotlin/FloatArray"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "kotlin/IntArray"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "kotlin/LongArray"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "kotlin/ShortArray"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "kotlin/BooleanArray"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "kotlin/CharArray"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "kotlin/Cloneable"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "kotlin/Annotation"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "kotlin/collections/Iterable"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "kotlin/collections/MutableIterable"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "kotlin/collections/Collection"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "kotlin/collections/MutableCollection"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "kotlin/collections/List"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "kotlin/collections/MutableList"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "kotlin/collections/Set"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "kotlin/collections/MutableSet"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "kotlin/collections/Map"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "kotlin/collections/MutableMap"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "kotlin/collections/Map.Entry"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "kotlin/collections/MutableMap.MutableEntry"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "kotlin/collections/Iterator"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "kotlin/collections/MutableIterator"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "kotlin/collections/ListIterator"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "kotlin/collections/MutableListIterator"

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->f:Ljava/util/List;

    .line 132
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->a:Lkotlin/reflect/jvm/internal/impl/d/b/n$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/n$a;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v1

    .line 138
    invoke-static {v1, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/a/ab;->a(I)I

    move-result v0

    invoke-static {v0, v4}, Lkotlin/f/d;->c(II)I

    move-result v2

    .line 139
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 140
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 141
    check-cast v1, Lkotlin/a/x;

    .line 132
    invoke-virtual {v1}, Lkotlin/a/x;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    check-cast v2, Lkotlin/a/x;

    invoke-virtual {v2}, Lkotlin/a/x;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 143
    :cond_0
    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->g:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const-string v0, "types"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strings"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->e:[Ljava/lang/String;

    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->m()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->b:Ljava/util/Set;

    .line 35
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/util/ArrayList;

    .line 36
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->h()Ljava/util/List;

    move-result-object v2

    .line 37
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 38
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    .line 39
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->m()I

    move-result v5

    const/4 v3, 0x0

    add-int/lit8 v5, v5, -0x1

    if-gt v3, v5, :cond_0

    .line 40
    :goto_1
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    nop

    .line 39
    if-eq v3, v5, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 32
    :cond_1
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 43
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 44
    nop

    .line 35
    check-cast v1, Ljava/util/List;

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->c:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a()Ljava/util/List;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->f:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    .line 50
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->q()Ljava/lang/String;

    move-result-object v2

    .line 56
    :goto_0
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->v()I

    move-result v0

    if-lt v0, v10, :cond_7

    .line 57
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 58
    const-string v7, "begin"

    invoke-static {v0, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v7

    invoke-static {v3, v7}, Lkotlin/d/b/l;->a(II)I

    move-result v7

    if-gtz v7, :cond_7

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v7

    const-string v8, "end"

    invoke-static {v1, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v8

    invoke-static {v7, v8}, Lkotlin/d/b/l;->a(II)I

    move-result v7

    if-gtz v7, :cond_7

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v7, v8}, Lkotlin/d/b/l;->a(II)I

    move-result v7

    if-gtz v7, :cond_7

    .line 59
    const-string v7, "begin"

    invoke-static {v0, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v7

    const-string v0, "end"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    if-nez v2, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->a:Lkotlin/reflect/jvm/internal/impl/d/b/n$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/n$a;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/util/Collection;)Lkotlin/f/c;

    move-result-object v0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->o()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/f/c;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->a:Lkotlin/reflect/jvm/internal/impl/d/b/n$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/n$a;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->o()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    goto/16 :goto_0

    .line 53
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->e:[Ljava/lang/String;

    aget-object v2, v0, p1

    goto/16 :goto_0

    :cond_2
    move-object v0, v2

    .line 59
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v0, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v2

    .line 63
    :goto_1
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->x()I

    move-result v1

    if-lt v1, v10, :cond_3

    .line 64
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->w()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 65
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    int-to-char v2, v2

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 68
    :cond_3
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->t()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    move-result-object v1

    if-eqz v1, :cond_4

    :goto_2
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/o;->a:[I

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 83
    :goto_3
    :pswitch_0
    const-string v1, "string"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 68
    :cond_4
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;->a:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    goto :goto_2

    .line 73
    :pswitch_1
    const/16 v1, 0x24

    const/16 v2, 0x2e

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 76
    :pswitch_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v10, :cond_6

    .line 77
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-nez v0, :cond_5

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    :cond_6
    const/16 v1, 0x24

    const/16 v2, 0x2e

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_7
    move-object v0, v2

    goto :goto_1

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/n;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/16 v1, 0x2f

    const/4 v2, 0x0

    .line 89
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/n;->a(I)Ljava/lang/String;

    move-result-object v12

    move-object v0, v12

    .line 90
    check-cast v0, Ljava/lang/CharSequence;

    const/4 v4, 0x6

    move v3, v2

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v4

    .line 92
    if-gez v4, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/e/b;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 94
    :goto_0
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    add-int/lit8 v2, v4, 0x1

    if-nez v12, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/e/b;

    if-nez v12, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v0, v12

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const-string v0, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v6, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v8, 0x2e

    const/4 v10, 0x4

    move v7, v1

    move v9, v2

    move-object v11, v5

    invoke-static/range {v6 .. v11}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    move-object v0, v3

    goto :goto_0

    .line 94
    :cond_2
    check-cast v12, Ljava/lang/String;

    invoke-virtual {v12, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    .line 95
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/a;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/n;->b:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-direct {v2, v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/b;Z)V

    return-object v2
.end method
