.class public final Lkotlin/reflect/jvm/internal/impl/d/a/a/l;
.super Ljava/lang/Object;
.source "RuntimeSourceElementFactory.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/e/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/a/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/l;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/l;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;
    .locals 2

    .prologue
    const-string v0, "javaElement"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;

    if-nez p1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.load.java.structure.reflect.ReflectJavaElement"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    return-object v0
.end method
