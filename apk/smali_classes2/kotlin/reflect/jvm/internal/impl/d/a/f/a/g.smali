.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/g;
.super Ljava/lang/Object;
.source "ReflectJavaAnnotationOwner.kt"


# direct methods
.method public static final a([Ljava/lang/annotation/Annotation;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    check-cast p0, [Ljava/lang/Object;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 45
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v1, p0

    if-ge v2, v1, :cond_0

    aget-object v1, p0, v2

    .line 46
    check-cast v1, Ljava/lang/annotation/Annotation;

    .line 37
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    invoke-direct {v3, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;-><init>(Ljava/lang/annotation/Annotation;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 47
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final a([Ljava/lang/annotation/Annotation;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    check-cast p0, [Ljava/lang/Object;

    .line 48
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    array-length v0, p0

    if-ge v2, v0, :cond_1

    aget-object v1, p0, v2

    move-object v0, v1

    check-cast v0, Ljava/lang/annotation/Annotation;

    .line 41
    invoke-static {v0}, Lkotlin/d/a;->a(Ljava/lang/annotation/Annotation;)Lkotlin/reflect/c;

    move-result-object v0

    invoke-static {v0}, Lkotlin/d/a;->a(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 49
    :goto_1
    check-cast v0, Ljava/lang/annotation/Annotation;

    if-eqz v0, :cond_2

    .line 41
    check-cast v0, Ljava/lang/annotation/Annotation;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;-><init>(Ljava/lang/annotation/Annotation;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    :goto_2
    return-object v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 49
    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 41
    goto :goto_2
.end method
