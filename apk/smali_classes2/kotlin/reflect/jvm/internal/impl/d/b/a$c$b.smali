.class public Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;
.super Ljava/lang/Object;
.source "AbstractBinaryClassAnnotationAndConstantLoader.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/a$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TA;>;"
        }
    .end annotation
.end field

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/b/x;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a$c;Lkotlin/reflect/jvm/internal/impl/d/b/x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/x;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "signature"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->b:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/x;

    .line 285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 2

    .prologue
    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->b:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

    iget-object v1, v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->a:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 292
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->a:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->b:Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->b:Ljava/util/HashMap;

    check-cast v0, Ljava/util/Map;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/x;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->a:Ljava/util/ArrayList;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    :cond_0
    return-void

    .line 292
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()Lkotlin/reflect/jvm/internal/impl/d/b/x;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/x;

    return-object v0
.end method
