.class public final Lkotlin/reflect/jvm/internal/impl/d/a/g/m;
.super Ljava/lang/Object;
.source "signatureEnhancement.kt"


# direct methods
.method public static final a(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(",
            "Ljava/util/Collection",
            "<+TD;>;)",
            "Ljava/util/Collection",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const-string v0, "platformSignatures"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p0, Ljava/lang/Iterable;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 111
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 112
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 29
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/m;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(TD;)TD;"
        }
    .end annotation

    .prologue
    const/16 v10, 0xa

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 38
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    if-nez v0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    move-object v0, p0

    .line 41
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;->l()Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-eq v0, v6, :cond_0

    :cond_2
    move-object v0, p0

    .line 44
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 45
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/m$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/m$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p0, v7, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/m;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/d/a/g/n;

    move-result-object v0

    invoke-static {v0, v5, v6, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/n;Lkotlin/reflect/jvm/internal/impl/d/a/g/q;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    move-result-object v0

    move-object v4, v0

    .line 52
    :goto_1
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    if-nez v0, :cond_13

    move-object v0, v5

    :goto_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    if-eqz v0, :cond_5

    .line 51
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    if-nez v1, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v4, v5

    .line 46
    goto :goto_1

    .line 51
    :cond_4
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {v0, v7, v6, v5}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "this.computeJvmDescriptor()"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 52
    check-cast v0, Ljava/lang/String;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/a/g/i;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;

    move-object v1, v0

    .line 55
    :goto_3
    if-eqz v1, :cond_8

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;

    .line 56
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move-object v2, p0

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;->i()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v3, v2, :cond_6

    move v2, v6

    :goto_4
    sget-boolean v3, Lkotlin/p;->a:Z

    if-eqz v3, :cond_7

    if-nez v2, :cond_7

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Predefined enhancement info for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", but "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " expected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 56
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_5
    move-object v1, v5

    .line 52
    goto :goto_3

    :cond_6
    move v2, v7

    .line 56
    goto :goto_4

    .line 59
    :cond_7
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    .line 55
    check-cast v0, Lkotlin/n;

    :cond_8
    move-object v0, p0

    .line 61
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 114
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v10}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 115
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 116
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 63
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/m$c;

    invoke-direct {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/m$c;-><init>(Lkotlin/reflect/jvm/internal/impl/b/at;)V

    check-cast v3, Lkotlin/d/a/b;

    invoke-static {p0, v7, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/m;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/d/a/g/n;

    move-result-object v3

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;->b()Ljava/util/List;

    move-result-object v9

    if-eqz v9, :cond_9

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->c()I

    move-result v0

    invoke-static {v9, v0}, Lkotlin/a/m;->c(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    .line 64
    :goto_6
    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;)Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    move-object v0, v5

    .line 63
    goto :goto_6

    .line 117
    :cond_a
    check-cast v2, Ljava/util/List;

    .line 67
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/m$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/m$b;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p0, v6, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/m;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/d/a/g/n;

    move-result-object v3

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;->a()Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    move-result-object v0

    :goto_7
    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;)Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    move-result-object v3

    .line 70
    if-eqz v4, :cond_f

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b()Z

    move-result v0

    .line 69
    :goto_8
    if-nez v0, :cond_c

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b()Z

    move-result v0

    if-nez v0, :cond_c

    move-object v0, v2

    .line 70
    check-cast v0, Ljava/lang/Iterable;

    .line 118
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    .line 70
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 119
    :goto_9
    if-eqz v6, :cond_0

    .line 72
    :cond_c
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    if-eqz v4, :cond_d

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    :cond_d
    check-cast v2, Ljava/lang/Iterable;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v2, v10}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 121
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 122
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    .line 72
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_e
    move-object v0, v5

    .line 67
    goto :goto_7

    :cond_f
    move v0, v7

    .line 69
    goto :goto_8

    :cond_10
    move v6, v7

    .line 119
    goto :goto_9

    .line 123
    :cond_11
    check-cast v0, Ljava/util/List;

    .line 72
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-interface {p0, v5, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    move-result-object v0

    if-nez v0, :cond_12

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type D"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    move-object p0, v0

    goto/16 :goto_0

    :cond_13
    move-object v0, p0

    goto/16 :goto_2
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/d/a/g/n;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(TD;Z",
            "Lkotlin/d/a/b",
            "<-TD;+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/n;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;

    invoke-interface {p2, p0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 124
    new-instance v2, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v1, v4}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 125
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 126
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 105
    if-nez v1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type D"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-interface {p2, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 101
    check-cast v2, Ljava/util/Collection;

    invoke-direct {v3, v0, v2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Collection;Z)V

    return-object v3
.end method
