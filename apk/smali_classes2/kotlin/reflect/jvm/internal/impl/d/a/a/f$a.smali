.class final Lkotlin/reflect/jvm/internal/impl/d/a/a/f$a;
.super Lkotlin/d/b/m;
.source "JavaAnnotationMapper.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/Map",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/at;",
        "+",
        "Lkotlin/reflect/jvm/internal/impl/h/b/f",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/a/f;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/f$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/f;->e()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 208
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 122
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 121
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 124
    if-eqz v0, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/b/g;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/f$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/b/g;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    const-string v2, "Deprecated in Java"

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/ab;->a(Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_2

    :goto_1
    return-object v0

    .line 209
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 124
    :cond_2
    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v0

    goto :goto_1
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/f$a;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
