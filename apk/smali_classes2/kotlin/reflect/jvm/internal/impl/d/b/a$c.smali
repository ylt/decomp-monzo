.class public final Lkotlin/reflect/jvm/internal/impl/d/b/a$c;
.super Ljava/lang/Object;
.source "AbstractBinaryClassAnnotationAndConstantLoader.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/b/a$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;,
        Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/a;

.field final synthetic b:Ljava/util/HashMap;

.field final synthetic c:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap;",
            "Ljava/util/HashMap;",
            ")V"
        }
    .end annotation

    .prologue
    .line 252
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->b:Ljava/util/HashMap;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->c:Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/b/u$c;
    .locals 3

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "desc"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 258
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "name.asString()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->b(Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v1

    .line 260
    if-eqz p3, :cond_0

    .line 261
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a;

    invoke-virtual {v0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 262
    if-eqz v2, :cond_0

    .line 263
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;->c:Ljava/util/HashMap;

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a$c;Lkotlin/reflect/jvm/internal/impl/d/b/x;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$c;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/u$e;
    .locals 4

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "desc"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "name.asString()"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a$c$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a$c;Lkotlin/reflect/jvm/internal/impl/d/b/x;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$e;

    return-object v0
.end method
