.class public final Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;
.super Ljava/lang/Object;
.source "BinaryClassAnnotationAndConstantLoaderImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

.field final synthetic c:Lkotlin/reflect/jvm/internal/impl/e/f;

.field final synthetic d:Ljava/util/ArrayList;

.field private final synthetic e:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$a;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/ArrayList;",
            ")V"
        }
    .end annotation

    .prologue
    .line 130
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->c:Lkotlin/reflect/jvm/internal/impl/e/f;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->d:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->e:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->e:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->e:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/b/u$b;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a()V

    .line 133
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->c:Lkotlin/reflect/jvm/internal/impl/e/f;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/b/a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->d:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    invoke-static {v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/h/b/f;)V

    .line 134
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->e:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V

    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enumClassId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enumEntryName"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;->e:Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    invoke-interface {v0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    return-void
.end method
