.class public final Lkotlin/reflect/jvm/internal/impl/d/b/b/c;
.super Ljava/lang/Object;
.source "ReflectKotlinClass.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;


# instance fields
.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/b/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->c:Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/a/a;Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;-><init>(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/a/a;)V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/d/b/u$c;)V
    .locals 2

    .prologue
    const-string v0, "visitor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/b;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    invoke-virtual {v0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$c;)V

    .line 66
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V
    .locals 2

    .prologue
    const-string v0, "visitor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/b;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    invoke-virtual {v0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V

    .line 70
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 7

    .prologue
    .line 59
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    const/16 v2, 0x2f

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".class"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->c:Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 72
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->b:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
