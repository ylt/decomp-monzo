.class Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;
.super Ljava/lang/Object;
.source "ReadKotlinClassHeaderAnnotationVisitor.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;


# direct methods
.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/a/b$1;)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;)V

    return-void
.end method

.method private b()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
    .locals 5

    .prologue
    .line 175
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b$1;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b$1;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "dataArrayVisitor"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method private c()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
    .locals 5

    .prologue
    .line 185
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b$2;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b$2;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "stringsArrayVisitor"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitAnnotation"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "classId"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitAnnotation"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "visitArray"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 162
    const-string v1, "d1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 163
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->b()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;

    move-result-object v0

    .line 169
    :goto_0
    return-object v0

    .line 165
    :cond_1
    const-string v1, "d2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->c()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;

    move-result-object v0

    goto :goto_0

    .line 169
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 205
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 128
    if-nez p1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 131
    const-string v1, "k"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 132
    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->a(I)Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;)Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    goto :goto_0

    .line 136
    :cond_2
    const-string v1, "mv"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 137
    instance-of v0, p2, [I

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/m;

    check-cast p2, [I

    check-cast p2, [I

    invoke-direct {v1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/m;-><init>([I)V

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/m;)Lkotlin/reflect/jvm/internal/impl/d/b/m;

    goto :goto_0

    .line 141
    :cond_3
    const-string v1, "bv"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 142
    instance-of v0, p2, [I

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/k;

    check-cast p2, [I

    check-cast p2, [I

    invoke-direct {v1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/k;-><init>([I)V

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/a/k;)Lkotlin/reflect/jvm/internal/impl/d/a/k;

    goto :goto_0

    .line 146
    :cond_4
    const-string v1, "xs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 147
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    check-cast p2, Ljava/lang/String;

    invoke-static {v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 151
    :cond_5
    const-string v1, "xi"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;I)I

    goto/16 :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitEnum"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "enumClassId"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitEnum"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "enumEntryName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitEnum"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_2
    return-void
.end method
