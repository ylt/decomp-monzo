.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d$a;
.super Ljava/lang/Object;
.source "ReflectJavaAnnotationArguments.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;
    .locals 2

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/o;

    if-nez p1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Enum<*>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/Enum;

    invoke-direct {v0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/o;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Enum;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;

    .line 27
    :goto_0
    return-object v0

    .line 29
    :cond_1
    instance-of v0, p1, Ljava/lang/annotation/Annotation;

    if-eqz v0, :cond_2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/e;

    check-cast p1, Ljava/lang/annotation/Annotation;

    invoke-direct {v0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/annotation/Annotation;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;

    goto :goto_0

    .line 30
    :cond_2
    instance-of v0, p1, [Ljava/lang/Object;

    if-eqz v0, :cond_3

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/h;

    check-cast p1, [Ljava/lang/Object;

    invoke-direct {v0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/h;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;

    goto :goto_0

    .line 31
    :cond_3
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_4

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/k;

    check-cast p1, Ljava/lang/Class;

    invoke-direct {v0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Class;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;

    goto :goto_0

    .line 32
    :cond_4
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/q;

    invoke-direct {v0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/q;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;

    goto :goto_0
.end method
