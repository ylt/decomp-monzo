.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;
.super Ljava/lang/Object;
.source "JavaTypeResolver.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/c/j;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)V
    .locals 1

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameterResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 4

    .prologue
    .line 147
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {p3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->p()Lkotlin/reflect/jvm/internal/impl/a/n;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/n;->a()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 151
    :cond_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    .line 154
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->c:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 165
    :goto_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v2, p3, v1}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 167
    invoke-virtual {v2, v1}, Lkotlin/reflect/jvm/internal/impl/f/a;->b(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 170
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->c:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "kotlinDescriptor"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 171
    :cond_1
    invoke-virtual {v2, v1}, Lkotlin/reflect/jvm/internal/impl/f/a;->d(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    goto :goto_0

    .line 155
    :cond_2
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->d:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    goto :goto_1

    .line 159
    :cond_3
    invoke-virtual {v2, p3}, Lkotlin/reflect/jvm/internal/impl/f/a;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    goto :goto_1

    .line 162
    :cond_4
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    goto :goto_1

    .line 165
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    move-object v0, v1

    .line 175
    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 3

    .prologue
    .line 142
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 143
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->d()Lkotlin/reflect/jvm/internal/impl/d/b/e;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->m()Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 252
    .line 253
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/z;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 254
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/z;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/z;->a()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v7

    .line 255
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/z;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/z;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-object v6, v0

    .line 256
    :goto_0
    if-eqz v7, :cond_0

    invoke-direct {p0, v6, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/b/aq;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    :cond_0
    invoke-static {p3, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    .line 252
    :goto_1
    return-object v0

    .line 255
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-object v6, v0

    goto :goto_0

    .line 259
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const/4 v4, 0x7

    move v2, v1

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    invoke-virtual {p0, v7, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0, v6, p3}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    goto :goto_1

    .line 266
    :cond_3
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_1
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;Lkotlin/reflect/jvm/internal/impl/d/a/f/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 64
    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Z)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 91
    new-instance v6, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$c;

    invoke-direct {v6, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;)V

    .line 93
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    move v4, v7

    .line 94
    :goto_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->e()Z

    move-result v5

    .line 95
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->e()Z

    move-result v0

    if-nez v0, :cond_2

    if-nez v4, :cond_2

    .line 96
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->b(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 104
    :goto_1
    return-object v0

    :cond_0
    move v4, v8

    .line 93
    goto :goto_0

    :cond_1
    move-object v0, v6

    .line 96
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$c;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "errorType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_1

    .line 99
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZZ)V

    move-object v1, v0

    .line 101
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$d;

    invoke-virtual {v1, v7}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$d;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 102
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$d;

    invoke-virtual {v0, v8}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$d;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 104
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 105
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_1

    .line 101
    :cond_3
    check-cast v6, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$c;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "errorType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_1

    .line 102
    :cond_4
    check-cast v6, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$c;->b()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "errorType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_1

    .line 108
    :cond_5
    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_1
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->b(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 185
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$a;

    .line 187
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->h(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 191
    :goto_0
    return v0

    .line 188
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    invoke-virtual {v0, p2}, Lkotlin/reflect/jvm/internal/impl/f/a;->d(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    .line 189
    invoke-static {v0}, Lkotlin/a/m;->h(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 191
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 189
    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/b/aq;)Z
    .locals 2

    .prologue
    .line 271
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 272
    :goto_0
    return v0

    :cond_0
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 113
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/b/a/k;

    const/4 v0, 0x2

    new-array v2, v0, [Lkotlin/reflect/jvm/internal/impl/b/a/h;

    const/4 v3, 0x0

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;

    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-direct {v1, v6, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->f()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Lkotlin/reflect/jvm/internal/impl/b/a/k;-><init>(Ljava/util/List;)V

    .line 114
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->c(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p0, p1, p2, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;

    move-result-object v2

    .line 116
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->d(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Z

    move-result v3

    move-object v0, v5

    .line 118
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    const/16 v5, 0x10

    move-object v6, v4

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v4

    :cond_0
    return-object v4
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 4

    .prologue
    .line 122
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/i;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 124
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 125
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 414
    if-eqz v0, :cond_1

    .line 125
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 127
    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 128
    :goto_0
    if-eqz v0, :cond_3

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 123
    :goto_1
    return-object v0

    .line 122
    :cond_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    goto :goto_1

    .line 414
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    .line 125
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Class type should have a FQ name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 127
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->k()Lkotlin/reflect/jvm/internal/impl/d/a/c/h;

    move-result-object v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    goto :goto_0

    .line 128
    :cond_3
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    goto :goto_1

    .line 130
    :cond_4
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    if-eqz v0, :cond_6

    .line 131
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/j;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/w;)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 133
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown classifier kind: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final d(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 282
    :cond_0
    :goto_0
    return v0

    .line 277
    :cond_1
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 282
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->c()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/i;

    move-result-object v2

    .line 283
    instance-of v3, v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    if-eqz v3, :cond_3

    .line 284
    const/4 v2, 0x4

    new-array v2, v2, [Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v3, v2, v1

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v3, v2, v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->g:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v3, v2, v4

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v3, v2, v5

    invoke-static {v2}, Lkotlin/a/ah;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 282
    :goto_1
    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 286
    :cond_3
    instance-of v3, v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    if-eqz v3, :cond_5

    .line 287
    :cond_4
    new-array v2, v5, [Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v3, v2, v1

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->g:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v3, v2, v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    aput-object v3, v2, v4

    invoke-static {v2}, Lkotlin/a/ah;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown classifier: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/j;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/16 v5, 0xa

    const/4 v10, 0x1

    const/4 v1, 0x0

    const-string v0, "javaType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attr"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "constructor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 195
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    .line 196
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->i()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    move v0, v10

    .line 204
    :goto_0
    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v6

    .line 205
    if-eqz v0, :cond_3

    move-object v0, v6

    .line 206
    check-cast v0, Ljava/lang/Iterable;

    .line 415
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 416
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 417
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 221
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v4

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$b;

    invoke-direct {v2, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/k/ad;)V

    check-cast v2, Lkotlin/d/a/a;

    invoke-static {v0, v4, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    .line 225
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;

    const-string v5, "parameter"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v0, p2, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 201
    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v10

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 418
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 226
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 244
    :goto_2
    return-object v0

    .line 229
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v0, v2, :cond_5

    .line 231
    check-cast v6, Ljava/lang/Iterable;

    .line 419
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v6, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 420
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 421
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 231
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v3, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 422
    :cond_4
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 231
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    .line 233
    :cond_5
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->g:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 234
    :goto_4
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->d()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v4

    .line 423
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v4, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object v7, v2

    check-cast v7, Ljava/util/Collection;

    .line 424
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 425
    check-cast v2, Lkotlin/a/x;

    .line 236
    invoke-virtual {v2}, Lkotlin/a/x;->c()I

    move-result v4

    invoke-virtual {v2}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    .line 238
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_7

    move v2, v10

    :goto_6
    sget-boolean v5, Lkotlin/p;->a:Z

    if-eqz v5, :cond_8

    if-nez v2, :cond_8

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Argument index should be less then type parameters count, but "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " > "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 233
    :cond_6
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    goto :goto_4

    :cond_7
    move v2, v1

    .line 238
    goto :goto_6

    .line 242
    :cond_8
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 243
    const/4 v4, 0x7

    move v2, v1

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v2

    const-string v4, "parameter"

    invoke-static {v9, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v8, v2, v9}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 426
    :cond_9
    check-cast v7, Ljava/util/List;

    check-cast v7, Ljava/util/Collection;

    .line 244
    invoke-static {v7}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Z)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    const-string v0, "arrayType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attr"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    .line 66
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v6

    .line 67
    instance-of v0, v6, Lkotlin/reflect/jvm/internal/impl/d/a/f/u;

    if-nez v0, :cond_8

    move-object v0, v3

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/u;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/u;->a()Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v0

    .line 68
    :goto_1
    if-eqz v0, :cond_3

    .line 69
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    .line 70
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    const-string v0, "jetType"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    .line 65
    :goto_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->f()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/ao;->b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0

    :cond_0
    move-object v0, v3

    .line 67
    goto :goto_1

    .line 72
    :cond_1
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->c()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v7

    :goto_3
    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    goto :goto_2

    :cond_2
    move v0, v8

    goto :goto_3

    .line 75
    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->e()Z

    move-result v1

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->g()Z

    move-result v2

    const/4 v4, 0x4

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 78
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v0, v2, v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v2, "c.module.builtIns.getArr\u2026INVARIANT, componentType)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v2, v3, v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1, v7}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    goto :goto_2

    .line 84
    :cond_4
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->d:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz p3, :cond_6

    :cond_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    .line 85
    :goto_4
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    .line 86
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->c()Z

    move-result v1

    if-nez v1, :cond_7

    :goto_5
    invoke-virtual {v0, v7}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    goto/16 :goto_2

    .line 84
    :cond_6
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_4

    :cond_7
    move v7, v8

    .line 86
    goto :goto_5

    :cond_8
    move-object v0, v6

    goto/16 :goto_0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 6

    .prologue
    const-string v0, "javaType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attr"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/u;

    if-eqz v0, :cond_2

    .line 52
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/u;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/u;->a()Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_1

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    :goto_0
    const-string v1, "if (primitiveType != nul\u2026.module.builtIns.unitType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 50
    :cond_0
    :goto_1
    return-object v0

    .line 54
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->I()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    goto :goto_0

    .line 56
    :cond_2
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;

    if-eqz v0, :cond_3

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_1

    .line 57
    :cond_3
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;

    if-eqz v0, :cond_4

    move-object v1, p1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;Lkotlin/reflect/jvm/internal/impl/d/a/f/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_1

    .line 59
    :cond_4
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/z;

    if-eqz v0, :cond_6

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/z;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/z;->a()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    invoke-virtual {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    if-nez v0, :cond_0

    :cond_5
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->z()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "c.module.builtIns.defaultBound"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_1

    .line 60
    :cond_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
