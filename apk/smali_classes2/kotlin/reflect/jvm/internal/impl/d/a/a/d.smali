.class public final Lkotlin/reflect/jvm/internal/impl/d/a/a/d;
.super Ljava/lang/Object;
.source "JavaAnnotationMapper.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

.field private static final b:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private static final c:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private static final d:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private static final e:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private static final f:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private static final g:Lkotlin/reflect/jvm/internal/impl/e/f;

.field private static final h:Lkotlin/reflect/jvm/internal/impl/e/f;

.field private static final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

    .line 44
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-class v1, Ljava/lang/annotation/Target;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 45
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-class v1, Ljava/lang/annotation/Retention;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 46
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-class v1, Ljava/lang/Deprecated;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 47
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-class v1, Ljava/lang/annotation/Documented;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 49
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "java.lang.annotation.Repeatable"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 51
    const-string v0, "message"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->g:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 52
    const-string v0, "allowedTargets"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->h:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 83
    new-array v0, v7, [Lkotlin/h;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/a/l$a;->C:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/a/l$a;->F:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/a/l$a;->G:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/a/l$a;->H:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->i:Ljava/util/Map;

    .line 89
    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/h;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->C:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->F:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->x:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->G:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->H:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v0}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->j:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 3

    .prologue
    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "c"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a;->b()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 56
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j;

    invoke-direct {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 62
    :goto_0
    return-object v0

    .line 57
    :cond_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/i;

    invoke-direct {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/i;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    goto :goto_0

    .line 58
    :cond_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->r()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    const-string v2, "c.module.builtIns.repeatableAnnotation"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p2, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    goto :goto_0

    .line 59
    :cond_2
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->s()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    const-string v2, "c.module.builtIns.mustBeDocumentedAnnotation"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p2, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    goto :goto_0

    .line 60
    :cond_3
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    .line 61
    :cond_4
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;

    invoke-direct {v0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/a;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    goto :goto_0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "kotlinName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationOwner"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "c"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->x:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    move-result-object v2

    .line 70
    if-nez v2, :cond_0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/f;

    invoke-direct {v0, v2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 74
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    if-eqz v0, :cond_3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 75
    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    .line 76
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

    invoke-virtual {v1, v0, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    .line 75
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 74
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 75
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 74
    goto :goto_0
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->g:Lkotlin/reflect/jvm/internal/impl/e/f;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->h:Lkotlin/reflect/jvm/internal/impl/e/f;

    return-object v0
.end method
