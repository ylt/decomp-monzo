.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;
.super Lkotlin/d/b/m;
.source "RawType.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->a(Lkotlin/reflect/jvm/internal/impl/g/c;Lkotlin/reflect/jvm/internal/impl/g/h;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/k/r;",
        "Ljava/util/List",
        "<+",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/g/c;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/g/c;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;->a:Lkotlin/reflect/jvm/internal/impl/g/c;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 171
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 172
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 173
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 57
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;->a:Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 174
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 57
    return-object v1
.end method
