.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;
.super Lkotlin/reflect/jvm/internal/impl/d/a/c/a/m;
.source "LazyJavaStaticClassScope.kt"


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

.field private final d:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)V
    .locals 1

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jClass"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownerDescriptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/m;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Set;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Ljava/util/Set",
            "<TR;>;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
            "+",
            "Ljava/util/Collection",
            "<+TR;>;>;)",
            "Ljava/util/Set",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 111
    invoke-static {p1}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$e;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/utils/b$b;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;

    invoke-direct {v2, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Set;Lkotlin/d/a/b;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/utils/b$c;

    invoke-static {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)Ljava/lang/Object;

    .line 133
    return-object p2
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/h;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->o:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-virtual {v1, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0

    .line 102
    :cond_0
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 3

    .prologue
    .line 138
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    :goto_0
    return-object p1

    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 174
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 175
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 176
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 140
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 177
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 140
    invoke-static {v1}, Lkotlin/a/m;->o(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-object p1, v0

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->i()Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$b;

    invoke-direct {v1, p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    check-cast v1, Lkotlin/d/a/a;

    invoke-interface {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/m;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/d/a/b/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/f;

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/util/Set;

    move-result-object v0

    .line 72
    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f()Lkotlin/reflect/jvm/internal/impl/i/b/r;

    move-result-object v2

    invoke-static {p2, v0, p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;)Ljava/util/Collection;

    move-result-object v0

    const-string v1, "resolveOverridesForStati\u2026components.errorReporter)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 74
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/b;->b(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    const-string v1, "createEnumValueOfMethod(ownerDescriptor)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_1
    :goto_0
    return-void

    .line 77
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    const-string v1, "createEnumValuesMethod(ownerDescriptor)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Set;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$c;

    invoke-direct {v2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$c;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;)V

    check-cast v2, Lkotlin/d/a/b;

    invoke-direct {p0, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Set;Lkotlin/d/a/b;)Ljava/util/Set;

    move-result-object v0

    .line 87
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    .line 88
    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f()Lkotlin/reflect/jvm/internal/impl/i/b/r;

    move-result-object v2

    invoke-static {p1, v0, p2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;)Ljava/util/Collection;

    move-result-object v0

    const-string v1, "resolveOverridesForStati\u2026rorReporter\n            )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 99
    :goto_1
    return-void

    .line 87
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 93
    :cond_1
    check-cast v0, Ljava/lang/Iterable;

    .line 148
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 149
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    .line 150
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 94
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v4

    .line 152
    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 153
    if-nez v0, :cond_2

    .line 154
    nop

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 155
    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 159
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 162
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 169
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 170
    check-cast v1, Ljava/util/Map$Entry;

    .line 96
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f()Lkotlin/reflect/jvm/internal/impl/i/b/r;

    move-result-object v4

    invoke-static {p1, v1, p2, v2, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;)Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 171
    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_3

    .line 173
    :cond_4
    check-cast v0, Ljava/util/List;

    .line 93
    check-cast v0, Ljava/util/Collection;

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method protected c(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->i()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->a()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->p(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Set;

    .line 47
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/h;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->A_()Ljava/util/Set;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    check-cast v2, Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 48
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    const/4 v2, 0x2

    new-array v2, v2, [Lkotlin/reflect/jvm/internal/impl/e/f;

    const/4 v3, 0x0

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/c;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/c;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    aput-object v4, v2, v3

    invoke-static {v2}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 51
    :cond_0
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->e()Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 144
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 145
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 146
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    .line 51
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 47
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v2

    goto :goto_1

    .line 147
    :cond_3
    check-cast v3, Ljava/util/List;

    .line 51
    check-cast v3, Ljava/util/Collection;

    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 52
    nop

    .line 46
    check-cast v1, Ljava/util/Set;

    .line 52
    return-object v1
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;
    .locals 3

    .prologue
    .line 43
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/d/a/b;)V

    return-object v1
.end method

.method protected d(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    return-object v0
.end method

.method protected e(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->i()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->b()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->p(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Set;

    .line 56
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$d;

    check-cast v3, Lkotlin/d/a/b;

    invoke-direct {p0, v2, v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Set;Lkotlin/d/a/b;)Ljava/util/Set;

    .line 57
    nop

    .line 55
    check-cast v1, Ljava/util/Set;

    .line 57
    return-object v1
.end method

.method protected e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method
