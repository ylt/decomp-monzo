.class public final enum Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;
.super Ljava/lang/Enum;
.source "KotlinClassHeader.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

.field public static final g:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a$a;

.field private static final synthetic h:[Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

.field private static final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    const-string v3, "UNKNOWN"

    .line 37
    invoke-direct {v1, v3, v2, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    const-string v3, "CLASS"

    .line 38
    invoke-direct {v1, v3, v4, v4}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    const-string v3, "FILE_FACADE"

    .line 39
    invoke-direct {v1, v3, v5, v5}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    const-string v3, "SYNTHETIC_CLASS"

    .line 40
    invoke-direct {v1, v3, v6, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    aput-object v1, v0, v6

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    const-string v3, "MULTIFILE_CLASS"

    .line 41
    invoke-direct {v1, v3, v7, v7}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->e:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    const-string v4, "MULTIFILE_CLASS_PART"

    const/4 v5, 0x5

    const/4 v6, 0x5

    .line 42
    invoke-direct {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    aput-object v3, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->h:[Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a$a;

    .line 45
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->values()[Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 80
    array-length v1, v0

    invoke-static {v1}, Lkotlin/a/ab;->a(I)I

    move-result v1

    const/16 v3, 0x10

    invoke-static {v1, v3}, Lkotlin/f/d;->c(II)I

    move-result v3

    .line 81
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    move v4, v2

    .line 82
    :goto_0
    array-length v2, v0

    if-ge v4, v2, :cond_0

    aget-object v3, v0, v4

    move-object v2, v3

    .line 83
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    .line 45
    iget v2, v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 85
    :cond_0
    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->j:Ljava/util/Map;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->i:I

    return-void
.end method

.method public static final synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->j:Ljava/util/Map;

    return-object v0
.end method

.method public static final a(I)Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a$a;

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a$a;->a(I)Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->h:[Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    return-object v0
.end method
