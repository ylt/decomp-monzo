.class public final Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;
.super Ljava/lang/Object;
.source "predefinedEnhancementInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/g/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/g/l;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/l;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "className"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lkotlin/d/a/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/l;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/l;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/l;)Ljava/util/Map;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->a()Lkotlin/h;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    return-void
.end method
