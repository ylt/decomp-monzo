.class final Lkotlin/reflect/jvm/internal/impl/d/b/s;
.super Ljava/lang/Object;
.source "methodSignatureMapping.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/r;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/d/b/r",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/b/q;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/b/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/s;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/s;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/b/s;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/b/s;->a:Lkotlin/reflect/jvm/internal/impl/d/b/s;

    return-void
.end method


# virtual methods
.method public synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->b()Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->a(Lkotlin/reflect/jvm/internal/impl/d/b/q;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/d/b/q;)Lkotlin/reflect/jvm/internal/impl/d/b/q;
    .locals 2

    .prologue
    const-string v0, "possiblyPrimitiveType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;->a()Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 115
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;->a()Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JvmClassName.byFqNameWit\u2026apperFqName).internalName"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    .line 119
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    .line 118
    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->b(Lkotlin/reflect/jvm/internal/impl/d/b/q;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/d/b/q;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/b/q$a;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/b/q$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/q$a;->a()Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v1

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->b(Lkotlin/reflect/jvm/internal/impl/d/b/q;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 147
    :cond_1
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    if-eqz v0, :cond_3

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;->a()Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :cond_2
    const-string v0, "V"

    goto :goto_0

    .line 148
    :cond_3
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "L"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/d/b/q;
    .locals 1

    .prologue
    .line 152
    const-string v0, "java/lang/Class"

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/q;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v0, "representation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v3

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    const-string v1, "empty string as JvmType"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    move v0, v4

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 125
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->values()[Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    move v5, v4

    .line 169
    :goto_1
    array-length v1, v0

    if-ge v5, v1, :cond_4

    aget-object v2, v0, v5

    move-object v1, v2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 125
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v7, :cond_2

    move v1, v3

    :goto_2
    if-eqz v1, :cond_3

    move-object v0, v2

    .line 170
    :goto_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    if-eqz v0, :cond_5

    .line 125
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;

    .line 126
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;-><init>(Lkotlin/reflect/jvm/internal/impl/h/d/c;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    .line 129
    :goto_4
    return-object v0

    :cond_2
    move v1, v4

    .line 125
    goto :goto_2

    :cond_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    :cond_4
    move-object v0, v6

    .line 170
    goto :goto_3

    .line 129
    :cond_5
    sparse-switch v7, :sswitch_data_0

    .line 133
    const/16 v0, 0x4c

    if-ne v7, v0, :cond_6

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    const/16 v1, 0x3b

    const/4 v2, 0x2

    invoke-static {v0, v1, v4, v2, v6}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;CZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v4, v3

    :cond_6
    sget-boolean v0, Lkotlin/p;->a:Z

    if-eqz v0, :cond_8

    if-nez v4, :cond_8

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Type that is not primitive nor array should be Object, but \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' was found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 133
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 130
    :sswitch_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    invoke-direct {v0, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;-><init>(Lkotlin/reflect/jvm/internal/impl/h/d/c;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    goto :goto_4

    .line 131
    :sswitch_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$a;

    if-nez p1, :cond_7

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/s;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/q$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/q;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    goto :goto_4

    .line 137
    :cond_8
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-nez p1, :cond_9

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    goto/16 :goto_4

    .line 129
    nop

    :sswitch_data_0
    .sparse-switch
        0x56 -> :sswitch_0
        0x5b -> :sswitch_1
    .end sparse-switch
.end method

.method public d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/q$b;
    .locals 1

    .prologue
    const-string v0, "internalName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
