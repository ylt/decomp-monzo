.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;
.super Lkotlin/reflect/jvm/internal/impl/k/b;
.source "LazyJavaClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 142
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V

    .line 143
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method private final k()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 191
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->l()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    move-object v3, v0

    .line 196
    :goto_0
    if-eqz v3, :cond_1

    move-object v2, v3

    .line 197
    :goto_1
    if-eqz v2, :cond_2

    .line 200
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v4

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->s:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-static {v4, v2, v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 202
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 203
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    .line 204
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 207
    if-ne v5, v2, :cond_5

    .line 208
    check-cast v0, Ljava/lang/Iterable;

    .line 258
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v7}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 259
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 260
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 210
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/k/aj;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v3, v5, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_0
    move-object v3, v1

    .line 191
    goto :goto_0

    .line 197
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/d;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 220
    :goto_3
    return-object v0

    :cond_3
    move-object v0, v1

    .line 200
    goto :goto_3

    .line 261
    :cond_4
    check-cast v1, Ljava/util/List;

    .line 220
    :goto_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-static {v0, v4, v1}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_3

    .line 212
    :cond_5
    if-ne v5, v6, :cond_7

    if-le v2, v6, :cond_7

    if-nez v3, :cond_7

    .line 214
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/k/aj;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v3, v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 215
    new-instance v0, Lkotlin/f/c;

    invoke-direct {v0, v6, v2}, Lkotlin/f/c;-><init>(II)V

    check-cast v0, Ljava/lang/Iterable;

    .line 262
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v7}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 263
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v2

    check-cast v0, Lkotlin/a/aa;

    invoke-virtual {v0}, Lkotlin/a/aa;->b()I

    .line 215
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 265
    :cond_6
    check-cast v1, Ljava/util/List;

    goto :goto_4

    :cond_7
    move-object v0, v1

    .line 217
    goto :goto_3
.end method

.method private final l()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/i;->h:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "JvmAnnotationNames.PURELY_IMPLEMENTS_ANNOTATION"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 228
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/b/s;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 229
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/d;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 231
    :cond_1
    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected F_()Lkotlin/reflect/jvm/internal/impl/b/ao;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->m()Lkotlin/reflect/jvm/internal/impl/b/ao;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/util/Collection;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 150
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->d()Ljava/util/Collection;

    move-result-object v0

    .line 151
    new-instance v8, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 152
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 154
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->k()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v11

    .line 156
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;

    .line 157
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v13

    move-object v7, v6

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const/4 v4, 0x7

    move v2, v1

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    invoke-virtual {v13, v7, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    .line 158
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$b;

    if-eqz v0, :cond_1

    .line 159
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    :cond_1
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v4

    if-eqz v11, :cond_2

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    :goto_1
    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->h(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v0, v3

    .line 162
    goto :goto_1

    :cond_3
    move-object v0, v8

    .line 173
    check-cast v0, Ljava/util/Collection;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->c(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    if-eqz v2, :cond_4

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 175
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/f/c;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/k/ae;

    move-result-object v3

    .line 176
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/k/ae;->f()Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v3

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v3, v2, v4}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    .line 173
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    :goto_2
    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;Ljava/lang/Object;)V

    move-object v0, v8

    .line 179
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0, v11}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;Ljava/lang/Object;)V

    move-object v0, v9

    .line 181
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v10

    :goto_3
    if-eqz v0, :cond_8

    .line 182
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f()Lkotlin/reflect/jvm/internal/impl/i/b/r;

    move-result-object v4

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->h()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    check-cast v9, Ljava/lang/Iterable;

    .line 254
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v9, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 255
    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 256
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    .line 183
    if-nez v3, :cond_6

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.load.java.structure.JavaClassifierType"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move-object v2, v3

    .line 173
    goto :goto_2

    :cond_5
    move v0, v1

    .line 181
    goto :goto_3

    .line 183
    :cond_6
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 257
    :cond_7
    check-cast v2, Ljava/util/List;

    .line 182
    invoke-interface {v4, v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/r;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;)V

    :cond_8
    move-object v0, v8

    .line 187
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    move v1, v10

    :cond_9
    if-eqz v1, :cond_a

    check-cast v8, Ljava/util/Collection;

    invoke-static {v8}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    :goto_5
    return-object v0

    :cond_a
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->x()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_5
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->h()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/v;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    return v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getName().asString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
