.class final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$g;
.super Lkotlin/d/b/m;
.source "ReflectJavaClass.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->y()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Ljava/lang/reflect/Method;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Ljava/lang/reflect/Method;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$g;->a(Ljava/lang/reflect/Method;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/reflect/Method;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->isSynthetic()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    const-string v3, "method"

    invoke-static {p1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;Ljava/lang/reflect/Method;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
