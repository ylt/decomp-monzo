.class public abstract Lkotlin/reflect/jvm/internal/impl/d/b/a;
.super Ljava/lang/Object;
.source "AbstractBinaryClassAnnotationAndConstantLoader.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/a$b;,
        Lkotlin/reflect/jvm/internal/impl/d/b/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "C:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/i/b/c",
        "<TA;TC;TT;>;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/b/a$a;

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a$b",
            "<TA;TC;>;>;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/b/t;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a$a;

    .line 356
    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/e/b;

    const/4 v1, 0x0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/i;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/i;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/i;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "java.lang.annotation.Target"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "java.lang.annotation.Retention"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "java.lang.annotation.Documented"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 363
    nop

    .line 375
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 376
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 377
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 363
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 378
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 363
    invoke-static {v1}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->d:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/d/b/t;)V
    .locals 1

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kotlinClassFinder"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    .line 42
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a$e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->b:Lkotlin/reflect/jvm/internal/impl/j/c;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    .line 161
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    if-eqz v0, :cond_2

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    move v1, v0

    .line 160
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 161
    goto :goto_0

    .line 162
    :cond_2
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    if-eqz v0, :cond_3

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    goto :goto_1

    .line 163
    :cond_3
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    if-eqz v0, :cond_6

    .line 164
    if-nez p1, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.serialization.deserialization.ProtoContainer.Class"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v1, 0x2

    goto :goto_1

    .line 165
    :cond_5
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->g()Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 166
    goto :goto_1

    .line 168
    :cond_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;ILjava/lang/Object;)Ljava/util/List;
    .locals 6

    .prologue
    const/4 v4, 0x0

    if-eqz p7, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: findClassAndLoadMemberAnnotations"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v0, p6, 0x4

    if-eqz v0, :cond_3

    move v3, v4

    .line 133
    :goto_0
    and-int/lit8 v0, p6, 0x8

    if-eqz v0, :cond_2

    :goto_1
    and-int/lit8 v0, p6, 0x10

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Boolean;

    move-object v5, v0

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v5, p5

    goto :goto_2

    :cond_2
    move v4, p4

    goto :goto_1

    :cond_3
    move v3, p3

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/x;",
            "ZZ",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0, p1, p3, p4, p5}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;ZZLjava/lang/Boolean;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->b:Lkotlin/reflect/jvm/internal/impl/j/c;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a$b;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    .line 137
    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_1
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static final synthetic a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->d:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/b/a$b;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/b/a$b;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/b/a$b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a$b",
            "<TA;TC;>;"
        }
    .end annotation

    .prologue
    .line 249
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 250
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 252
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;

    invoke-direct {v0, p0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a;Ljava/util/HashMap;Ljava/util/HashMap;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$d;

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V

    .line 299
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/b/a$b;

    move-object v0, v1

    check-cast v0, Ljava/util/Map;

    move-object v1, v2

    check-cast v1, Ljava/util/Map;

    invoke-direct {v3, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a$b;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    return-object v3
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->b(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/b/u;
    .locals 1

    .prologue
    .line 208
    if-eqz p2, :cond_0

    .line 207
    :goto_0
    return-object p2

    .line 209
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->b(Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object p2

    goto :goto_0

    .line 210
    :cond_1
    const/4 p2, 0x0

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;ZZLjava/lang/Boolean;)Lkotlin/reflect/jvm/internal/impl/d/b/u;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 219
    if-eqz p2, :cond_6

    .line 220
    if-nez p4, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isConst should not be null for property (container="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 221
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->e()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    const-string v2, "DefaultImpls"

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    const-string v2, "container.classId.create\u2026EFAULT_IMPLS_CLASS_NAME))"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v5

    .line 245
    :cond_1
    :goto_0
    return-object v5

    .line 226
    :cond_2
    if-nez p4, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$b;

    if-eqz v0, :cond_6

    .line 228
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->d()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/b/p;

    if-nez v1, :cond_4

    move-object v0, v5

    :cond_4
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/p;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/p;->d()Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v0

    .line 229
    :goto_1
    if-eqz v0, :cond_6

    .line 231
    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    new-instance v7, Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2f

    const/16 v2, 0x2e

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "ClassId.topLevel(FqName(\u2026lName.replace(\'/\', \'.\')))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v5

    goto :goto_0

    :cond_5
    move-object v0, v5

    .line 228
    goto :goto_1

    .line 235
    :cond_6
    if-eqz p3, :cond_8

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    if-eqz v0, :cond_8

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->g:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v0, p1

    .line 236
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->i()Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 239
    :cond_7
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->b(Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v5

    goto/16 :goto_0

    .line 242
    :cond_8
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$b;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->d()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/d/b/p;

    if-eqz v0, :cond_1

    .line 243
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->d()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    if-nez v0, :cond_9

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.load.kotlin.JvmPackagePartSource"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/p;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/p;->c()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v5

    goto/16 :goto_0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ZZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/b/x;
    .locals 6

    .prologue
    const/4 v5, 0x0

    if-eqz p7, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: getPropertySignature"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v0, p6, 0x8

    if-eqz v0, :cond_2

    move v4, v5

    .line 306
    :goto_0
    and-int/lit8 v0, p6, 0x10

    if-eqz v0, :cond_1

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 307
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ZZ)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    return-object v0

    :cond_1
    move v5, p5

    goto :goto_1

    :cond_2
    move v4, p4

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ZZ)Lkotlin/reflect/jvm/internal/impl/d/b/x;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 310
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    .line 313
    if-eqz p4, :cond_2

    .line 314
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    invoke-virtual {v0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/c/d$a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d$a;->c()Ljava/lang/String;

    move-result-object v0

    .line 315
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {v2, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->b(Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    .line 321
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 311
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 314
    goto :goto_0

    .line 317
    :cond_2
    if-eqz p5, :cond_3

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 318
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    const-string v2, "signature.syntheticMethod"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 321
    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/d/b/x;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 331
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    if-eqz v0, :cond_1

    .line 332
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    invoke-virtual {v2, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 335
    :cond_0
    return-object v1

    .line 334
    :cond_1
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    if-eqz v0, :cond_2

    .line 335
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    invoke-virtual {v2, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    goto :goto_0

    .line 337
    :cond_2
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c/c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 338
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c/c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    .line 339
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/b;->a:[I

    invoke-virtual {p4}, Lkotlin/reflect/jvm/internal/impl/i/b/b;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    .line 343
    goto :goto_0

    .line 340
    :pswitch_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->q()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    const-string v2, "signature.getter"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    goto :goto_0

    .line 341
    :pswitch_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->s()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    const-string v2, "signature.setter"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    move-object v1, p1

    .line 342
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ZZ)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 346
    goto :goto_0

    .line 339
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Lkotlin/reflect/jvm/internal/impl/b/al;",
            "Ljava/util/List",
            "<TA;>;)",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$a;"
        }
    .end annotation

    .prologue
    .line 62
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a$a;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v0

    goto :goto_0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->d()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/b/w;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/w;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/w;->b()Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v1

    :cond_1
    return-object v1
.end method


# virtual methods
.method protected abstract a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")TC;"
        }
    .end annotation
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")TC;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expectedType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p2

    .line 195
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->c()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/b/b;->b:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    invoke-direct {p0, v0, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 198
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->t:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p()I

    move-result v3

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v5, v5, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;ZZLjava/lang/Boolean;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    .line 199
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a;->b:Lkotlin/reflect/jvm/internal/impl/j/c;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a$b;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 196
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 199
    goto :goto_0
.end method

.method protected abstract a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")TA;"
        }
    .end annotation
.end method

.method protected abstract a(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+TA;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected abstract a(Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+TA;>;",
            "Ljava/util/List",
            "<+TA;>;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/e;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->b(Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 75
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a$d;

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a$d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a;Ljava/util/ArrayList;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$c;

    invoke-interface {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$c;)V

    move-object v0, v1

    .line 84
    check-cast v0, Ljava/util/List;

    return-object v0

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Class for loading annotations is not found: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/i/e$g;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->p()I

    move-result v2

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "container.nameResolver.getString(proto.name)"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.serialization.deserialization.ProtoContainer.Class"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->e()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/b;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->b(Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v2

    .line 123
    const/16 v6, 0x1c

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move-object v7, v5

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;->b:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    invoke-static {p3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 89
    if-nez p2, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.serialization.ProtoBuf.Property"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-object v1, p2

    .line 91
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->c()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ZZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v8

    move-object v1, p2

    .line 92
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->c()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ZZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v9

    .line 94
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->t:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v5

    .line 96
    if-eqz v8, :cond_1

    move-object v2, v8

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/b/x;

    .line 97
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 96
    check-cast v0, Ljava/util/List;

    .line 98
    :goto_0
    if-eqz v0, :cond_2

    move-object v6, v0

    .line 100
    :goto_1
    if-eqz v9, :cond_3

    move-object v2, v9

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/b/x;

    .line 101
    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;)Ljava/util/List;

    move-result-object v0

    .line 100
    check-cast v0, Ljava/util/List;

    .line 102
    :goto_2
    if-eqz v0, :cond_4

    move-object v2, v0

    .line 105
    :goto_3
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "$delegate"

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v0, v1, v3, v4, v5}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v0

    :goto_4
    if-eqz v0, :cond_6

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->i:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    :goto_5
    invoke-virtual {p0, v6, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/util/List;

    move-result-object v0

    .line 115
    :goto_6
    return-object v0

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_2
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    goto :goto_1

    .line 100
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 102
    :cond_4
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    goto :goto_3

    .line 105
    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->a:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    goto :goto_5

    .line 114
    :cond_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->c()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 115
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_6

    .line 114
    :cond_8
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_6
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;ILkotlin/reflect/jvm/internal/impl/i/e$ag;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            "I",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callableProto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->c()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_0

    .line 151
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v1, p4

    .line 152
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/x;I)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v2

    .line 153
    const/16 v6, 0x1c

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move-object v7, v5

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->f:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 371
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 372
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 373
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 191
    const-string v3, "it"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 374
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 367
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 368
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 369
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 187
    const-string v3, "it"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 370
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method protected abstract a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Lkotlin/reflect/jvm/internal/impl/b/al;",
            "Ljava/util/List",
            "<TA;>;)",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$a;"
        }
    .end annotation
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->c()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_0

    .line 179
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/x;->a:Lkotlin/reflect/jvm/internal/impl/d/b/x$a;

    invoke-virtual {v1, v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/x$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/x;I)Lkotlin/reflect/jvm/internal/impl/d/b/x;

    move-result-object v2

    .line 180
    const/16 v6, 0x1c

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move-object v7, v5

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/x;ZZLjava/lang/Boolean;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
