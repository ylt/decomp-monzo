.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/o;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;
.source "ReflectJavaAnnotationArguments.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/m;


# instance fields
.field private final b:Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Enum;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Enum",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/o;->b:Ljava/lang/Enum;

    return-void
.end method


# virtual methods
.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/d/a/f/n;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/o;->c()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/o;->b:Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    :goto_0
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/o;->b:Ljava/lang/Enum;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const-string v2, "enumClass.getDeclaredField(value.name)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;-><init>(Ljava/lang/reflect/Field;)V

    return-object v1

    .line 56
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method
