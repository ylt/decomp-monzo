.class Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1$1;
.super Ljava/lang/Object;
.source "DescriptorResolverUtils.java"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/b;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1$1;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1$1;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/n;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "descriptor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils$1$1"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "invoke"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1$1;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/a$1;->a:Lkotlin/reflect/jvm/internal/impl/i/b/r;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/r;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)V

    .line 77
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method
