.class public final Lkotlin/reflect/jvm/internal/impl/d/b/w;
.super Ljava/lang/Object;
.source "KotlinJvmBinarySourceElement.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/a/f;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/d/b/u;

.field private final c:Lkotlin/reflect/jvm/internal/impl/i/b/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/i/b/u",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/m;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/u;Lkotlin/reflect/jvm/internal/impl/i/b/u;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/u",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/m;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const-string v0, "binaryClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/w;->b:Lkotlin/reflect/jvm/internal/impl/d/b/u;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/w;->c:Lkotlin/reflect/jvm/internal/impl/i/b/u;

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/w;->d:Z

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/b/am;
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/am;->a:Lkotlin/reflect/jvm/internal/impl/b/am;

    const-string v1, "SourceFile.NO_SOURCE_FILE"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/d/b/u;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/w;->b:Lkotlin/reflect/jvm/internal/impl/d/b/u;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/w;->b:Lkotlin/reflect/jvm/internal/impl/d/b/u;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
