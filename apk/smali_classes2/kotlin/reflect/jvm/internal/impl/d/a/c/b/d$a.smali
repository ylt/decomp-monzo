.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;
.super Ljava/lang/Object;
.source "JavaTypeResolver.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZZZ)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Z

.field private final e:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

.field private final f:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

.field private final synthetic g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZZZ)V
    .locals 1

    .prologue
    .line 373
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    iput-boolean p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->b:Z

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->c:Z

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    .line 374
    if-nez p2, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    :goto_0
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->e:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    .line 375
    if-nez p4, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    :goto_1
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    return-void

    .line 374
    :cond_0
    if-eqz p3, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    goto :goto_0

    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    goto :goto_0

    .line 375
    :cond_2
    if-eqz p3, :cond_3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    goto :goto_1

    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    goto :goto_1
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->c()Z

    move-result v0

    return v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->e:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    return-object v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->e()Z

    move-result v0

    return v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->f()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->g()Z

    move-result v0

    return v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->g:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    return-object v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    return-object v0
.end method
