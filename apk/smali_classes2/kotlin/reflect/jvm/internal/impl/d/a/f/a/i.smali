.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;
.source "ReflectJavaArrayType.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/f;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

.field private final c:Ljava/lang/reflect/Type;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Type;)V
    .locals 3

    .prologue
    const-string v0, "reflectType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;->c:Ljava/lang/reflect/Type;

    .line 24
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;->K_()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    .line 26
    instance-of v1, v0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v1, :cond_0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;

    check-cast v0, Ljava/lang/reflect/GenericArrayType;

    invoke-interface {v0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v2, "genericComponentType"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;->a(Ljava/lang/reflect/Type;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    .line 24
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    return-void

    .line 27
    :cond_0
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getComponentType()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;->a(Ljava/lang/reflect/Type;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    goto :goto_0

    .line 28
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not an array type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;->K_()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;->K_()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method protected K_()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;->c:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/i;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    return-object v0
.end method
