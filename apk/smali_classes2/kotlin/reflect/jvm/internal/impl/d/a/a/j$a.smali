.class final Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;
.super Lkotlin/d/b/m;
.source "JavaAnnotationMapper.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/a/j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/Map",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/at;",
        "+",
        "Lkotlin/reflect/jvm/internal/impl/h/b/f",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/a/j;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/j;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/j;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/j;->f()Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

    move-result-object v0

    .line 137
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/e;

    if-eqz v1, :cond_0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/e;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/j;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/j;->f()Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/e;->b()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    .line 141
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/j;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/j;->e()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/ab;->a(Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    :goto_1
    return-object v0

    .line 138
    :cond_0
    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;

    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/e;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/j;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/j;->f()Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

    move-result-object v1

    invoke-static {v1}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_1
    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v0

    goto :goto_1
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/j$a;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
