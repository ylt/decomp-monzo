.class public final Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;
.super Ljava/lang/Object;
.source "predefinedEnhancementInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/h",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/q;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Lkotlin/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/q;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "functionName"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->d:Ljava/lang/String;

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->b:Ljava/util/List;

    .line 215
    const-string v0, "V"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->c:Lkotlin/h;

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/j;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v6, 0xa

    .line 234
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 235
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->d:Ljava/lang/String;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->b:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 259
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v1, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 260
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 261
    check-cast v1, Lkotlin/h;

    .line 235
    invoke-virtual {v1}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 262
    :cond_0
    check-cast v2, Ljava/util/List;

    .line 235
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->c:Lkotlin/h;

    invoke-virtual {v1}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v4, v2, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->c:Lkotlin/h;

    invoke-virtual {v0}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->b:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 263
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v1, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 264
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 265
    check-cast v1, Lkotlin/h;

    .line 236
    invoke-virtual {v1}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 266
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 235
    invoke-direct {v4, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;Ljava/util/List;)V

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    .line 234
    check-cast v0, Lkotlin/h;

    .line 237
    return-object v0
.end method

.method public final varargs a(Ljava/lang/String;[Lkotlin/reflect/jvm/internal/impl/d/a/g/d;)V
    .locals 7

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qualifiers"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->b:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    move-object v1, p2

    check-cast v1, [Ljava/lang/Object;

    array-length v1, v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-static {p1, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 224
    return-void

    .line 222
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    check-cast p2, [Ljava/lang/Object;

    invoke-static {p2}, Lkotlin/a/g;->k([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v2

    .line 247
    const/16 v1, 0xa

    invoke-static {v2, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/a/ab;->a(I)I

    move-result v1

    const/16 v3, 0x10

    invoke-static {v1, v3}, Lkotlin/f/d;->c(II)I

    move-result v3

    .line 248
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 249
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v2, v3

    .line 250
    check-cast v2, Lkotlin/a/x;

    .line 223
    invoke-virtual {v2}, Lkotlin/a/x;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    check-cast v3, Lkotlin/a/x;

    invoke-virtual {v3}, Lkotlin/a/x;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    invoke-interface {v1, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 222
    :cond_2
    invoke-direct {v4, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;-><init>(Ljava/util/Map;)V

    move-object v1, v4

    goto :goto_1
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/h/d/c;)V
    .locals 2

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->c:Lkotlin/h;

    .line 233
    return-void
.end method

.method public final varargs b(Ljava/lang/String;[Lkotlin/reflect/jvm/internal/impl/d/a/g/d;)V
    .locals 6

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qualifiers"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    check-cast p2, [Ljava/lang/Object;

    invoke-static {p2}, Lkotlin/a/g;->k([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v1

    .line 253
    const/16 v0, 0xa

    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/a/ab;->a(I)I

    move-result v0

    const/16 v2, 0x10

    invoke-static {v0, v2}, Lkotlin/f/d;->c(II)I

    move-result v2

    .line 254
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 255
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 256
    check-cast v1, Lkotlin/a/x;

    .line 229
    invoke-virtual {v1}, Lkotlin/a/x;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    check-cast v2, Lkotlin/a/x;

    invoke-virtual {v2}, Lkotlin/a/x;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-direct {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;-><init>(Ljava/util/Map;)V

    invoke-static {p1, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/l$a$a;->c:Lkotlin/h;

    .line 230
    return-void
.end method
