.class public final Lkotlin/reflect/jvm/internal/impl/d/a/j;
.super Ljava/lang/Object;
.source "JvmAnnotationNames.kt"


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    const/16 v0, 0xa

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/i;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "android.support.annotation.Nullable"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "com.android.annotations.Nullable"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "org.eclipse.jdt.annotation.Nullable"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "org.checkerframework.checker.nullness.qual.Nullable"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "javax.annotation.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "javax.annotation.CheckForNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "edu.umd.cs.findbugs.annotations.CheckForNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "edu.umd.cs.findbugs.annotations.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "edu.umd.cs.findbugs.annotations.PossiblyNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->a:Ljava/util/List;

    .line 34
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "javax.annotation.Nonnull"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 36
    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/e/b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/i;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "edu.umd.cs.findbugs.annotations.NonNull"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "android.support.annotation.NonNull"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "com.android.annotations.NonNull"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "org.eclipse.jdt.annotation.NonNull"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "org.checkerframework.checker.nullness.qual.NonNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "lombok.NonNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->c:Ljava/util/List;

    .line 46
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->g:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->d:Ljava/util/List;

    .line 50
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->e:Ljava/util/List;

    .line 56
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/util/List;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/j;->a:Ljava/util/List;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/j;->c:Ljava/util/List;

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/j;->d:Ljava/util/List;

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/j;->e:Ljava/util/List;

    aput-object v1, v0, v7

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/j;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 59
    nop

    .line 61
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 68
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 69
    check-cast v0, Ljava/util/List;

    .line 59
    check-cast v0, Ljava/lang/Iterable;

    .line 70
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 72
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 59
    invoke-static {v1}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->f:Ljava/util/Set;

    return-void
.end method

.method public static final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->a:Ljava/util/List;

    return-object v0
.end method

.method public static final b()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method

.method public static final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->c:Ljava/util/List;

    return-object v0
.end method

.method public static final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->d:Ljava/util/List;

    return-object v0
.end method

.method public static final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->e:Ljava/util/List;

    return-object v0
.end method

.method public static final f()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/j;->f:Ljava/util/Set;

    return-object v0
.end method
