.class final Lkotlin/reflect/jvm/internal/impl/d/b/b/b;
.super Ljava/lang/Object;
.source "ReflectKotlinClass.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/b/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/b;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Ljava/lang/annotation/Annotation;Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$a;",
            "Ljava/lang/annotation/Annotation;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 172
    invoke-virtual {p3}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-object v3, v2, v0

    .line 173
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    const-string v5, "Name.identifier(method.name)"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v3, p2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-direct {p0, p1, v4, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a()V

    .line 176
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 181
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/b/f;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 182
    invoke-interface {p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->b(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 186
    invoke-virtual {v1}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 187
    if-nez p3, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Enum<*>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_1

    .line 187
    :cond_3
    check-cast p3, Ljava/lang/Enum;

    invoke-virtual {p3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "Name.identifier((value as Enum<*>).name)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    goto :goto_0

    .line 189
    :cond_4
    const-class v2, Ljava/lang/annotation/Annotation;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 190
    invoke-virtual {v1}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/a/g;->d([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 191
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-interface {p1, p2, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 192
    if-nez p3, :cond_5

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Annotation"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast p3, Ljava/lang/annotation/Annotation;

    const-string v2, "annotationClass"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, p3, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Ljava/lang/annotation/Annotation;Ljava/lang/Class;)V

    goto :goto_0

    .line 194
    :cond_6
    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 195
    invoke-interface {p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/b/u$b;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 196
    invoke-virtual {v1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    .line 197
    invoke-virtual {v1}, Ljava/lang/Class;->isEnum()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 198
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v3

    .line 199
    if-nez p3, :cond_7

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<*>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    check-cast p3, [Ljava/lang/Object;

    move v1, v0

    :goto_2
    array-length v0, p3

    if-ge v1, v0, :cond_b

    aget-object v0, p3, v1

    .line 200
    if-nez v0, :cond_8

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Enum<*>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v4, "Name.identifier((element as Enum<*>).name)"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/u$b;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 199
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 204
    :cond_9
    if-nez p3, :cond_a

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<*>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    check-cast p3, [Ljava/lang/Object;

    :goto_3
    array-length v1, p3

    if-ge v0, v1, :cond_b

    aget-object v1, p3, v0

    .line 205
    invoke-interface {v2, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/u$b;->a(Ljava/lang/Object;)V

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 208
    :cond_b
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/u$b;->a()V

    goto/16 :goto_0

    .line 211
    :cond_c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported annotation argument value ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/b/u$c;Ljava/lang/annotation/Annotation;)V
    .locals 3

    .prologue
    .line 161
    invoke-static {p2}, Lkotlin/d/a;->a(Ljava/lang/annotation/Annotation;)Lkotlin/reflect/c;

    move-result-object v0

    invoke-static {v0}, Lkotlin/d/a;->a(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v1

    .line 162
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;

    invoke-direct {v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;-><init>(Ljava/lang/annotation/Annotation;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-interface {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/u$c;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    .line 163
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/b;

    invoke-direct {v2, v0, p2, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Ljava/lang/annotation/Annotation;Ljava/lang/Class;)V

    .line 164
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    .line 162
    check-cast v0, Lkotlin/n;

    .line 165
    :cond_0
    return-void
.end method

.method private final b(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$d;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 94
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v6

    move v3, v4

    :goto_0
    array-length v0, v6

    if-ge v3, v0, :cond_5

    aget-object v5, v6, v3

    .line 95
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "Name.identifier(method.name)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/b/i;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/i;

    const-string v2, "method"

    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Lkotlin/reflect/jvm/internal/impl/d/b/b/i;->a(Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/u$d;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/u$e;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 97
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v7

    move v2, v4

    :goto_1
    array-length v0, v7

    if-ge v2, v0, :cond_0

    aget-object v8, v7, v2

    move-object v0, v1

    .line 98
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$c;

    const-string v9, "annotation"

    invoke-static {v8, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v8}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$c;Ljava/lang/annotation/Annotation;)V

    .line 97
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 101
    :cond_0
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/a/g;->k([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/a/x;

    invoke-virtual {v0}, Lkotlin/a/x;->c()I

    move-result v8

    invoke-virtual {v0}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/annotation/Annotation;

    move v5, v4

    .line 102
    :goto_2
    array-length v2, v0

    if-ge v5, v2, :cond_1

    aget-object v9, v0, v5

    .line 103
    invoke-static {v9}, Lkotlin/d/a;->a(Ljava/lang/annotation/Annotation;)Lkotlin/reflect/c;

    move-result-object v2

    invoke-static {v2}, Lkotlin/d/a;->a(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v10

    .line 104
    invoke-static {v10}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v11

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;

    const-string v12, "annotation"

    invoke-static {v9, v12}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v9}, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;-><init>(Ljava/lang/annotation/Annotation;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-interface {v1, v8, v11, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/u$e;->a(ILkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    .line 105
    sget-object v11, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/b;

    const-string v12, "annotation"

    invoke-static {v9, v12}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v11, v2, v9, v10}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Ljava/lang/annotation/Annotation;Ljava/lang/Class;)V

    .line 106
    sget-object v2, Lkotlin/n;->a:Lkotlin/n;

    .line 104
    check-cast v2, Lkotlin/n;

    .line 102
    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 110
    :cond_3
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/u$e;->a()V

    .line 94
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 112
    :cond_5
    return-void
.end method

.method private final c(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$d;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v6

    const/4 v1, 0x0

    move v4, v1

    :goto_0
    array-length v1, v6

    if-ge v4, v1, :cond_7

    aget-object v5, v6, v4

    .line 116
    const-string v1, "<init>"

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "Name.special(\"<init>\")"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/b/i;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/i;

    const-string v3, "constructor"

    invoke-static {v5, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lkotlin/reflect/jvm/internal/impl/d/b/b/i;->a(Ljava/lang/reflect/Constructor;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-interface {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/u$d;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/u$e;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 118
    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v7

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    array-length v1, v7

    if-ge v3, v1, :cond_0

    aget-object v8, v7, v3

    move-object v1, v2

    .line 119
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/b/u$c;

    const-string v9, "annotation"

    invoke-static {v8, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v8}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$c;Ljava/lang/annotation/Annotation;)V

    .line 118
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 122
    :cond_0
    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v3

    move-object v1, v3

    .line 123
    check-cast v1, [Ljava/lang/Object;

    array-length v1, v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_5

    .line 130
    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    array-length v5, v1

    move-object v1, v3

    check-cast v1, [Ljava/lang/Object;

    array-length v1, v1

    sub-int v7, v5, v1

    .line 132
    check-cast v3, [Ljava/lang/Object;

    invoke-static {v3}, Lkotlin/a/g;->k([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/a/x;

    invoke-virtual {v1}, Lkotlin/a/x;->c()I

    move-result v9

    invoke-virtual {v1}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/annotation/Annotation;

    .line 133
    const/4 v3, 0x0

    move v5, v3

    :goto_4
    array-length v3, v1

    if-ge v5, v3, :cond_1

    aget-object v10, v1, v5

    .line 134
    invoke-static {v10}, Lkotlin/d/a;->a(Ljava/lang/annotation/Annotation;)Lkotlin/reflect/c;

    move-result-object v3

    invoke-static {v3}, Lkotlin/d/a;->a(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v11

    .line 135
    add-int v12, v9, v7

    invoke-static {v11}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v13

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;

    const-string v14, "annotation"

    invoke-static {v10, v14}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v10}, Lkotlin/reflect/jvm/internal/impl/d/b/b/a;-><init>(Ljava/lang/annotation/Annotation;)V

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-interface {v2, v12, v13, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/u$e;->a(ILkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 137
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    .line 138
    sget-object v12, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/b;

    const-string v13, "annotation"

    invoke-static {v10, v13}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v12, v3, v10, v11}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Ljava/lang/annotation/Annotation;Ljava/lang/Class;)V

    .line 139
    sget-object v3, Lkotlin/n;->a:Lkotlin/n;

    .line 137
    check-cast v3, Lkotlin/n;

    .line 133
    :cond_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_4

    .line 123
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    .line 144
    :cond_5
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/u$e;->a()V

    .line 115
    :cond_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_0

    .line 146
    :cond_7
    return-void
.end method

.method private final d(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$d;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    move v0, v1

    :goto_0
    array-length v2, v3

    if-ge v0, v2, :cond_2

    aget-object v2, v3, v0

    .line 150
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    const-string v5, "Name.identifier(field.name)"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/b/b/i;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/i;

    const-string v6, "field"

    invoke-static {v2, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/b/i;->a(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {p2, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/u$d;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/b/u$c;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 152
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v5

    move v2, v1

    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_0

    aget-object v6, v5, v2

    .line 153
    const-string v7, "annotation"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v4, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$c;Ljava/lang/annotation/Annotation;)V

    .line 152
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 156
    :cond_0
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/d/b/u$c;->a()V

    .line 149
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$c;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "klass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "visitor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    .line 82
    const-string v3, "annotation"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u$c;Ljava/lang/annotation/Annotation;)V

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_0
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/b/u$c;->a()V

    .line 85
    return-void
.end method

.method public final a(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$d;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "klass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "memberVisitor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->b(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V

    .line 89
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->c(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V

    .line 90
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/b/b;->d(Ljava/lang/Class;Lkotlin/reflect/jvm/internal/impl/d/b/u$d;)V

    .line 91
    return-void
.end method
