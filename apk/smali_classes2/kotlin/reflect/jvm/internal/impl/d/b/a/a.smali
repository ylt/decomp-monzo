.class public final Lkotlin/reflect/jvm/internal/impl/d/b/a/a;
.super Ljava/lang/Object;
.source "KotlinClassHeader.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/b/m;

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/a/k;

.field private final d:[Ljava/lang/String;

.field private final e:[Ljava/lang/String;

.field private final f:[Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:I


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;Lkotlin/reflect/jvm/internal/impl/d/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/k;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    const-string v0, "kind"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "metadataVersion"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bytecodeVersion"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->c:Lkotlin/reflect/jvm/internal/impl/d/a/k;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->d:[Ljava/lang/String;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->e:[Ljava/lang/String;

    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->f:[Ljava/lang/String;

    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->g:Ljava/lang/String;

    iput p8, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->h:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->g:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->h:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/d/b/m;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    return-object v0
.end method

.method public final e()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->d:[Ljava/lang/String;

    return-object v0
.end method

.method public final f()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->e:[Ljava/lang/String;

    return-object v0
.end method

.method public final g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
