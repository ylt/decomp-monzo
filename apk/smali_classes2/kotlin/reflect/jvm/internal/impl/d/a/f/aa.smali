.class public final enum Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;
.super Ljava/lang/Enum;
.source "javaElements.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

.field private static final synthetic c:[Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    const-string v2, "SOURCE"

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    aput-object v1, v0, v3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    const-string v2, "BINARY"

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    aput-object v1, v0, v4

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;->c:[Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;->c:[Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    return-object v0
.end method
