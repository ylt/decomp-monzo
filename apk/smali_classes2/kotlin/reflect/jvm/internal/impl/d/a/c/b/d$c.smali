.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;
.super Ljava/lang/Object;
.source "JavaTypeResolver.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lkotlin/reflect/jvm/internal/impl/b/aq;

.field private final e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field private final f:Z

.field private final g:Z

.field private final h:Lkotlin/reflect/jvm/internal/impl/b/a/h;

.field private final i:Z

.field private final j:Lkotlin/reflect/jvm/internal/impl/b/aq;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;)V
    .locals 1

    .prologue
    .line 359
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    iput-boolean p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->b:Z

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->c:Z

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->d:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 364
    iput-boolean p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->g:Z

    .line 366
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->h:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    .line 368
    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->i:Z

    .line 369
    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->j:Lkotlin/reflect/jvm/internal/impl/b/aq;

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
    .locals 1

    .prologue
    .line 362
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 363
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->f:Z

    return v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;
    .locals 1

    .prologue
    .line 359
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->g:Z

    return v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->h:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 368
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->i:Z

    return v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;->j:Lkotlin/reflect/jvm/internal/impl/b/aq;

    return-object v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;
    .locals 1

    .prologue
    .line 359
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a$a;->c(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    move-result-object v0

    return-object v0
.end method
