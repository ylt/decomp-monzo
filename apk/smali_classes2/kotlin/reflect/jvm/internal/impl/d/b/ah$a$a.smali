.class public final Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;
.super Ljava/lang/Object;
.source "UnsafeVarianceTypeSubstitution.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/ag;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->b()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/k/ag",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/k/ah;

.field final synthetic b:I

.field final synthetic c:Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/ah;ILkotlin/reflect/jvm/internal/impl/d/b/ah$a;)V
    .locals 0

    .prologue
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    iput p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->b:I

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;
    .locals 4

    .prologue
    .line 93
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->a:Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v0, "projection.type"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->d()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)V

    return-object v1
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/k/af;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;->c()Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/af;

    return-object v0
.end method
