.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/v;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;
.source "ReflectJavaPrimitiveType.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/u;


# instance fields
.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "reflectType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/v;->b:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public synthetic K_()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/v;->b()Ljava/lang/Class;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/a/m;
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/v;->b()Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 28
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/v;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a()Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v0

    goto :goto_0
.end method

.method protected b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/v;->b:Ljava/lang/Class;

    return-object v0
.end method
