.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;
.source "ReflectJavaAnnotation.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/a;


# instance fields
.field private final a:Ljava/lang/annotation/Annotation;


# direct methods
.method public constructor <init>(Ljava/lang/annotation/Annotation;)V
    .locals 1

    .prologue
    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 26
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    invoke-static {v0}, Lkotlin/d/a;->a(Ljava/lang/annotation/Annotation;)Lkotlin/reflect/c;

    move-result-object v0

    invoke-static {v0}, Lkotlin/d/a;->a(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 42
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    move v3, v4

    .line 43
    :goto_0
    array-length v2, v0

    if-ge v3, v2, :cond_0

    aget-object v2, v0, v3

    .line 44
    check-cast v2, Ljava/lang/reflect/Method;

    .line 27
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d$a;

    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    new-array v7, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    const-string v7, "method.invoke(annotation)"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d$a;->a(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 43
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 45
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 28
    return-object v1
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    invoke-static {v0}, Lkotlin/d/a;->a(Ljava/lang/annotation/Annotation;)Lkotlin/reflect/c;

    move-result-object v0

    invoke-static {v0}, Lkotlin/d/a;->a(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    return-object v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    invoke-static {v1}, Lkotlin/d/a;->a(Ljava/lang/annotation/Annotation;)Lkotlin/reflect/c;

    move-result-object v1

    invoke-static {v1}, Lkotlin/d/a;->a(Lkotlin/reflect/c;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public final e()Ljava/lang/annotation/Annotation;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 35
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;->a:Ljava/lang/annotation/Annotation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
