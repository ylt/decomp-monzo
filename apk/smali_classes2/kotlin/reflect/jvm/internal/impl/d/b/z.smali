.class public final Lkotlin/reflect/jvm/internal/impl/d/b/z;
.super Ljava/lang/Object;
.source "ModuleMapping.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/z$a;
    }
.end annotation


# static fields
# The value of this static final field might be set in the static constructor
.field public static final a:Ljava/lang/String; = "kotlin_module"

.field public static final b:Lkotlin/reflect/jvm/internal/impl/d/b/z;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/d/b/z$a;


# instance fields
.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/aa;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/z$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/z$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/z;->c:Lkotlin/reflect/jvm/internal/impl/d/b/z$a;

    .line 33
    const-string v0, "kotlin_module"

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/z;->a:Ljava/lang/String;

    .line 36
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/z;

    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v1

    const-string v2, "EMPTY"

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/z;-><init>(Ljava/util/Map;Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/z;->b:Lkotlin/reflect/jvm/internal/impl/d/b/z;

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/aa;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/z;->d:Ljava/util/Map;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/z;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/aa;
    .locals 1

    .prologue
    const-string v0, "packageFqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/z;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/aa;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/z;->e:Ljava/lang/String;

    return-object v0
.end method
