.class public final Lkotlin/reflect/jvm/internal/impl/d/b/y;
.super Ljava/lang/Object;
.source "methodSignatureMapping.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    nop

    .line 171
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    move-object v1, p0

    .line 70
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-object v3

    .line 72
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    instance-of v2, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_2

    move-object v1, v3

    :cond_2
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    .line 73
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->h()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v2

    instance-of v4, v2, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-nez v4, :cond_3

    move-object v2, v3

    :cond_3
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-eqz v2, :cond_0

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/s;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v2, v4, v5, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "(original as? SimpleFunc\u2026l).computeJvmDescriptor()"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    check-cast v0, Ljava/lang/String;

    move-object v3, v0

    .line 79
    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->b()Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/a;

    .line 84
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JvmClassName.byClassId(it).internalName"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    invoke-static {p0, v2, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/b/ac;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/s;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 34
    instance-of v2, p0, Lkotlin/reflect/jvm/internal/impl/b/l;

    if-eqz v2, :cond_0

    const-string v2, "<init>"

    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 169
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 38
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v4, "it.type"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_1

    .line 34
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "name.asString()"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_1
    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    if-eqz p1, :cond_2

    move-object v2, p0

    .line 44
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 45
    const-string v2, "V"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    :cond_2
    :goto_2
    nop

    .line 33
    check-cast v1, Ljava/lang/StringBuilder;

    .line 51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 48
    :cond_3
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    if-nez v2, :cond_4

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_4
    const-string v3, "returnType!!"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_2
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 32
    const/4 p1, 0x1

    :cond_0
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/b/q;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/s;->a:Lkotlin/reflect/jvm/internal/impl/d/b/s;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/b/r;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->b:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/b/ad;->b:Lkotlin/reflect/jvm/internal/impl/d/b/ad;

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/d/b/ac;

    const/16 v6, 0x20

    move-object v0, p0

    move-object v5, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ac;Lkotlin/reflect/jvm/internal/impl/d/b/l;Lkotlin/d/a/q;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q;

    return-object v0
.end method

.method private static final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 1

    .prologue
    .line 96
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 97
    return-void
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "f"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->f(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v4, "remove"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v3

    .line 57
    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    instance-of v4, v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    if-nez v4, :cond_2

    move-object v0, v1

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/q$c;->a()Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v1

    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 60
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/b/q;

    move-result-object v0

    .line 65
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    .line 64
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v4, v4, Lkotlin/reflect/jvm/internal/impl/a/l$a;->U:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/e/b;->b()Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v4

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 65
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    if-eqz v1, :cond_4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/q$b;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "java/lang/Object"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    move v3, v0

    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_1
.end method
