.class public abstract Lkotlin/reflect/jvm/internal/impl/d/a/c/a/m;
.super Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;
.source "LazyJavaStaticScope.kt"


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
    .locals 1

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    return-void
.end method


# virtual methods
.method protected a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/q;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;"
        }
    .end annotation

    .prologue
    const-string v0, "method"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "methodTypeParameters"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "valueParameters"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v6

    move-object v1, p3

    move-object v3, p4

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;ZLjava/util/List;)V

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/m;->g()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aj;

    return-object v0
.end method

.method protected g()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return-object v0
.end method
