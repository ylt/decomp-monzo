.class public final enum Lkotlin/reflect/jvm/internal/impl/d/a/g/g;
.super Ljava/lang/Enum;
.source "typeQualifiers.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/g/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

.field private static final synthetic c:[Lkotlin/reflect/jvm/internal/impl/d/a/g/g;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    const-string v2, "NULLABLE"

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    aput-object v1, v0, v3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    const-string v2, "NOT_NULL"

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    aput-object v1, v0, v4

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->c:[Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/g/g;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/a/g/g;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->c:[Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    return-object v0
.end method
