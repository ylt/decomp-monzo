.class public Lkotlin/reflect/jvm/internal/impl/d/b/a/b;
.super Ljava/lang/Object;
.source "ReadKotlinClassHeaderAnnotationVisitor.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/a/b$1;,
        Lkotlin/reflect/jvm/internal/impl/d/b/a/b$a;,
        Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;,
        Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;
    }
.end annotation


# static fields
.field private static final a:Z

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Lkotlin/reflect/jvm/internal/impl/d/b/m;

.field private d:Lkotlin/reflect/jvm/internal/impl/d/a/k;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:[Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:[Ljava/lang/String;

.field private j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const-string v0, "true"

    const-string v1, "kotlin.ignore.old.metadata"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a:Z

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->b:Ljava/util/Map;

    .line 44
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->b:Ljava/util/Map;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "kotlin.jvm.internal.KotlinClass"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->b:Ljava/util/Map;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "kotlin.jvm.internal.KotlinFileFacade"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->b:Ljava/util/Map;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "kotlin.jvm.internal.KotlinMultifileClass"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->e:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->b:Ljava/util/Map;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "kotlin.jvm.internal.KotlinMultifileClassPart"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->b:Ljava/util/Map;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "kotlin.jvm.internal.KotlinSyntheticClass"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    .line 52
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->d:Lkotlin/reflect/jvm/internal/impl/d/a/k;

    .line 53
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->e:Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->f:I

    .line 55
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->g:[Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->h:[Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->i:[Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    .line 279
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->f:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;)Lkotlin/reflect/jvm/internal/impl/d/a/k;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->d:Lkotlin/reflect/jvm/internal/impl/d/a/k;

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/a/k;)Lkotlin/reflect/jvm/internal/impl/d/a/k;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->d:Lkotlin/reflect/jvm/internal/impl/d/a/k;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;)Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/m;)Lkotlin/reflect/jvm/internal/impl/d/b/m;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->g:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->h:[Ljava/lang/String;

    return-object p1
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "classId"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitAnnotation"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "source"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitAnnotation"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 101
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/i;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/a/b$1;)V

    .line 118
    :goto_0
    return-object v0

    .line 105
    :cond_2
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a:Z

    if-eqz v0, :cond_3

    move-object v0, v1

    goto :goto_0

    .line 107
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 109
    goto :goto_0

    .line 112
    :cond_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    .line 113
    if-eqz v0, :cond_5

    .line 114
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    .line 115
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/a/b$1;)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 118
    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 62
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    if-nez v1, :cond_0

    .line 79
    :goto_0
    return-object v0

    .line 66
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/m;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 67
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->g:[Ljava/lang/String;

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->i:[Ljava/lang/String;

    .line 70
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/m;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 71
    :cond_2
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->g:[Ljava/lang/String;

    .line 79
    :cond_3
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->j:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    :goto_1
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->d:Lkotlin/reflect/jvm/internal/impl/d/a/k;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->d:Lkotlin/reflect/jvm/internal/impl/d/a/k;

    :goto_2
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->g:[Ljava/lang/String;

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->i:[Ljava/lang/String;

    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->h:[Ljava/lang/String;

    iget-object v7, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->e:Ljava/lang/String;

    iget v8, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->f:I

    invoke-direct/range {v0 .. v8}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;Lkotlin/reflect/jvm/internal/impl/d/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/k;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 73
    :cond_4
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->g:[Ljava/lang/String;

    if-nez v1, :cond_3

    goto :goto_0

    .line 79
    :cond_5
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/m;->b:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    goto :goto_1

    :cond_6
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/k;->b:Lkotlin/reflect/jvm/internal/impl/d/a/k;

    goto :goto_2
.end method
