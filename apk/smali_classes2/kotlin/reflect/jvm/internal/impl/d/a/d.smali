.class public final Lkotlin/reflect/jvm/internal/impl/d/a/d;
.super Ljava/lang/Object;
.source "FakePureImplementationsProvider.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/d;

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/d;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/d;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/d;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/d;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/d;->b:Ljava/util/HashMap;

    .line 31
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->V:Lkotlin/reflect/jvm/internal/impl/e/b;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "java.util.ArrayList"

    aput-object v2, v1, v4

    const-string v2, "java.util.LinkedList"

    aput-object v2, v1, v5

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/util/List;)V

    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->X:Lkotlin/reflect/jvm/internal/impl/e/b;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "java.util.HashSet"

    aput-object v2, v1, v4

    const-string v2, "java.util.TreeSet"

    aput-object v2, v1, v5

    const-string v2, "java.util.LinkedHashSet"

    aput-object v2, v1, v3

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/util/List;)V

    .line 33
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->Y:Lkotlin/reflect/jvm/internal/impl/e/b;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "java.util.HashMap"

    aput-object v2, v1, v4

    const-string v2, "java.util.TreeMap"

    aput-object v2, v1, v5

    const-string v2, "java.util.LinkedHashMap"

    aput-object v2, v1, v3

    const-string v2, "java.util.concurrent.ConcurrentHashMap"

    aput-object v2, v1, v6

    const/4 v2, 0x4

    const-string v3, "java.util.concurrent.ConcurrentSkipListMap"

    aput-object v3, v1, v2

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/util/List;)V

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "java.util.function.Function"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "java.util.function.UnaryOperator"

    aput-object v2, v1, v4

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/util/List;)V

    .line 36
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "java.util.function.BiFunction"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "java.util.function.BinaryOperator"

    aput-object v2, v1, v4

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/util/List;)V

    return-void
.end method

.method private final varargs a([Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    check-cast p1, [Ljava/lang/Object;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 48
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v1, p1

    if-ge v2, v1, :cond_0

    aget-object v1, p1, v2

    .line 49
    check-cast v1, Ljava/lang/String;

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 39
    invoke-direct {v3, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 48
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 50
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 39
    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    check-cast p2, Ljava/lang/Iterable;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/d;->b:Ljava/util/HashMap;

    check-cast v0, Ljava/util/Map;

    .line 43
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 44
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 27
    invoke-static {v1, p1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    check-cast v1, Lkotlin/h;

    invoke-virtual {v1}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 28
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    const-string v0, "classFqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/d;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method
