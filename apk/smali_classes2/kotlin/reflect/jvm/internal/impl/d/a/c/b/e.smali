.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;
.super Ljava/lang/Object;
.source "JavaTypeResolver.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/a/l;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

.field private final c:Z

.field private final d:Z


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZ)V
    .locals 2

    .prologue
    const-string v0, "howThisTypeIsUsed"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->c:Z

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->d:Z

    .line 339
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/a/l;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/b/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/d/a/b;)V

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->a:Lkotlin/reflect/jvm/internal/impl/b/a/l;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_0

    .line 336
    const/4 p3, 0x1

    :cond_0
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_1

    .line 337
    const/4 p4, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZ)V

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/b;)Z
    .locals 1

    .prologue
    .line 349
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->j()Lkotlin/reflect/jvm/internal/impl/b/a/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/b/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;
    .locals 2

    .prologue
    .line 342
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->g:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "JETBRAINS_READONLY_ANNOTATION"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "JETBRAINS_MUTABLE_ANNOTATION"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->d:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    .line 345
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->c:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 347
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->j()Lkotlin/reflect/jvm/internal/impl/b/a/l;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Z

    move-result v0

    return v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;
    .locals 1

    .prologue
    .line 333
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 336
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->c:Z

    return v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->j()Lkotlin/reflect/jvm/internal/impl/b/a/l;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->d:Z

    return v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 1

    .prologue
    .line 333
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a$a;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    return-object v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;
    .locals 1

    .prologue
    .line 333
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a$a;->c(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/b/a/l;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;->a:Lkotlin/reflect/jvm/internal/impl/b/a/l;

    return-object v0
.end method
