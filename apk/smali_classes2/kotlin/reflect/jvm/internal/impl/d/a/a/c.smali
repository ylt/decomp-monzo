.class public Lkotlin/reflect/jvm/internal/impl/d/a/a/c;
.super Ljava/lang/Object;
.source "JavaAnnotationMapper.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/a/c;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/al;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

.field private final c:Lkotlin/reflect/jvm/internal/impl/b/e;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/b/e;)V
    .locals 3

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kotlinAnnotationClassDescriptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;->c:Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 101
    if-eqz p2, :cond_0

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object v1, p0

    :goto_0
    iput-object v0, v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    .line 110
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a;->a()Ljava/util/Collection;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

    :goto_1
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

    return-void

    :cond_0
    move-object v0, p0

    .line 101
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object v2, v1

    move-object v1, v0

    move-object v0, v2

    goto :goto_0

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;->d()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 112
    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    return-object v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;->c:Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method protected final e()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;->c:Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->f(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v0

    const-string v1, "kotlinAnnotationClassDes\u2026.single().valueParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final f()Lkotlin/reflect/jvm/internal/impl/d/a/f/b;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

    return-object v0
.end method
