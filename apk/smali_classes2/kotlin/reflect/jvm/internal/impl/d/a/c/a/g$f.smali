.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;
.super Lkotlin/d/b/m;
.source "LazyJavaClassMemberScope.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/b/d;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->m()Ljava/util/Collection;

    move-result-object v0

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 83
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/k;

    .line 84
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/f/k;)Lkotlin/reflect/jvm/internal/impl/d/a/b/b;

    move-result-object v2

    .line 85
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 86
    check-cast v0, Ljava/util/Collection;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->i()Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

    move-result-object v4

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v4, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/m;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :cond_0
    check-cast v1, Ljava/util/Collection;

    .line 682
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 89
    :goto_1
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/m;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 91
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    .line 90
    goto :goto_1
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
