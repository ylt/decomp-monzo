.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c;
.super Ljava/lang/Object;
.source "specialBuiltinMembers.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/c;

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/c;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c;

    .line 53
    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/h;

    const/4 v1, 0x0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->r:Lkotlin/reflect/jvm/internal/impl/e/c;

    const-string v3, "name"

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/e/c;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "name"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->r:Lkotlin/reflect/jvm/internal/impl/e/c;

    const-string v3, "ordinal"

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/e/c;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "ordinal"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->M:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "size"

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "size"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->Q:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "size"

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "size"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->f:Lkotlin/reflect/jvm/internal/impl/e/c;

    const-string v3, "length"

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/e/c;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "length"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->Q:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "keys"

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "keySet"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->Q:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "values"

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "values"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->Q:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "entries"

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "entrySet"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->b:Ljava/util/Map;

    .line 65
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 66
    nop

    .line 355
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 356
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 357
    check-cast v0, Ljava/util/Map$Entry;

    .line 66
    new-instance v4, Lkotlin/h;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v4, v2, v0}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 358
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 67
    nop

    .line 359
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 360
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 361
    check-cast v1, Lkotlin/h;

    .line 67
    invoke-virtual {v1}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 363
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 364
    if-nez v3, :cond_1

    .line 365
    nop

    .line 362
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 366
    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v3

    .line 362
    :goto_2
    check-cast v1, Ljava/util/List;

    .line 370
    check-cast v2, Lkotlin/h;

    .line 67
    invoke-virtual {v2}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v1, v3

    .line 369
    goto :goto_2

    .line 372
    :cond_2
    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->c:Ljava/util/Map;

    .line 69
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->d:Ljava/util/Set;

    .line 70
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->d:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 373
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 374
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 375
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 70
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 376
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 70
    invoke-static {v1}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->e:Ljava/util/Set;

    return-void
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 79
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->d:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    move-object v1, p1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    :goto_0
    return v2

    :cond_0
    move-object v0, p1

    .line 80
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_1

    move v2, v3

    goto :goto_0

    .line 82
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 353
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 82
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c;

    const-string v5, "it"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    move v2, v0

    .line 354
    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name1"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->e:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 2

    .prologue
    const-string v0, "callableMemberDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->e:Ljava/util/Set;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 75
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c;->c(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/b/b;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 89
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "This method is defined only for builtin members, but "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 91
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c$a;

    check-cast v0, Lkotlin/d/a/b;

    const/4 v4, 0x1

    invoke-static {v2, v3, v0, v4, v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 92
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/c;->b:Ljava/util/Map;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    .line 91
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 92
    goto :goto_0
.end method
