.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;
.super Lkotlin/reflect/jvm/internal/impl/k/ak;
.source "RawType.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;

.field private static final b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

.field private static final c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    const/4 v4, 0x7

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/ak;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;

    .line 88
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move v2, v1

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    .line 89
    invoke-static {v0, v1, v6, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZZZ)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    .line 90
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move v2, v1

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    .line 91
    invoke-static {v0, v1, v6, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZZZ)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/h;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;",
            ")",
            "Lkotlin/h",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 116
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    .line 118
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 120
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v3, "componentTypeProjection.type"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    invoke-static {v1}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 123
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v3

    const/16 v5, 0x10

    move-object v6, v4

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 125
    invoke-static {v0, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    goto :goto_0

    .line 128
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Raw error type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    goto :goto_0

    .line 130
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v8

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v9

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 171
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    move-object v7, v1

    check-cast v7, Ljava/util/Collection;

    .line 172
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 173
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 134
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;

    const-string v3, "parameter"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x4

    move-object v3, p3

    move-object v6, v4

    invoke-static/range {v1 .. v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 174
    :cond_3
    check-cast v7, Ljava/util/List;

    .line 130
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c()Z

    move-result v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v2, "declaration.getMemberScope(RawSubstitution)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v8, v9, v7, v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 137
    invoke-static {v0, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 2

    .prologue
    const/4 v1, 0x0

    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 143
    const/4 v0, 0x3

    invoke-static {p1, v1, v1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/a;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object p3

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 3

    .prologue
    const-string v0, "parameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attr"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "erasedUpperBound"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->i()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/i;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/g;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 156
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 149
    :pswitch_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-direct {v0, v1, p3}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 166
    :goto_0
    return-object v0

    .line 156
    :pswitch_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ap;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0

    .line 159
    :cond_0
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 161
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-direct {v0, v1, p3}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0

    .line 159
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 164
    :cond_2
    invoke-static {p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/aj;
    .locals 2

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/aj;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    return-object v0
.end method

.method public final c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 96
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v1, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    const/4 v1, 0x3

    invoke-static {v0, v2, v2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/a;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    .line 97
    :cond_0
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_3

    .line 98
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/o;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    move-object v1, v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-direct {p0, v2, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/h;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-virtual {v2}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 99
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/o;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-direct {p0, v2, v0, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/h;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-virtual {v2}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 101
    if-nez v3, :cond_1

    if-eqz v2, :cond_2

    .line 102
    :cond_1
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    invoke-direct {v2, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    .line 101
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_0

    .line 105
    :cond_2
    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    goto :goto_1

    .line 108
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected declaration kind: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
