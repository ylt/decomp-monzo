.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;
.source "ReflectJavaClass.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f;
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/g;


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "klass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    return-void
.end method

.method private final a(Ljava/lang/reflect/Method;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    move v0, v2

    :goto_1
    return v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v3, "values"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_1

    .line 72
    :sswitch_1
    const-string v3, "valueOf"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    new-array v1, v1, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v2

    .line 120
    check-cast v1, [Ljava/lang/Object;

    .line 74
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 73
    goto :goto_1

    .line 72
    :sswitch_data_0
    .sparse-switch
        -0x311a62de -> :sswitch_0
        0xdce0328 -> :sswitch_1
    .end sparse-switch
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;Ljava/lang/reflect/Method;)Z
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a(Ljava/lang/reflect/Method;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public A()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 88
    invoke-static {v0}, Lkotlin/a/g;->l([Ljava/lang/Object;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$a;

    check-cast v0, Lkotlin/d/a/b;

    .line 89
    invoke-static {v1, v0}, Lkotlin/g/h;->a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$b;

    check-cast v0, Lkotlin/d/a/b;

    .line 90
    invoke-static {v1, v0}, Lkotlin/g/h;->e(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    .line 91
    invoke-static {v0}, Lkotlin/g/h;->d(Lkotlin/g/g;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public B()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->B()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;
    .locals 1

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 27
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f$a;->b(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f;)Z

    move-result v0

    return v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->e(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "klass.classId.asSingleFqName()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    const-class v1, Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 119
    :goto_0
    return-object v0

    .line 55
    :cond_0
    new-instance v1, Lkotlin/d/b/aa;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Lkotlin/d/b/aa;-><init>(I)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lkotlin/d/b/aa;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v2, "klass.genericInterfaces"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lkotlin/d/b/aa;->a(Ljava/lang/Object;)V

    invoke-virtual {v1}, Lkotlin/d/b/aa;->a()I

    move-result v0

    new-array v0, v0, [Ljava/lang/reflect/Type;

    invoke-virtual {v1, v0}, Lkotlin/d/b/aa;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Type;

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 116
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 117
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 118
    check-cast v0, Ljava/lang/reflect/Type;

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/l;

    .line 55
    invoke-direct {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/l;-><init>(Ljava/lang/reflect/Type;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    const-class v0, Ljava/lang/Object;

    check-cast v0, Ljava/lang/reflect/Type;

    goto :goto_1

    .line 119
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    move-object v0, v1

    goto :goto_0
.end method

.method public synthetic e()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->w()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 109
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->x()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isAnnotation()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    return v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->y()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public synthetic l()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->z()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public synthetic m()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->A()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 27
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z

    move-result v0

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 27
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;->b(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z

    move-result v0

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 27
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;->c(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z

    move-result v0

    return v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 1

    .prologue
    .line 27
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;->d(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    return-object v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "Name.identifier(klass.simpleName)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public s()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 121
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 122
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v2, v0

    if-ge v3, v2, :cond_0

    aget-object v2, v0, v3

    .line 123
    check-cast v2, Ljava/lang/reflect/TypeVariable;

    .line 100
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/x;

    const-string v5, "it"

    invoke-static {v2, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/x;-><init>(Ljava/lang/reflect/TypeVariable;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 122
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 124
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 100
    return-object v1
.end method

.method public synthetic t()Ljava/lang/reflect/AnnotatedElement;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->u()Ljava/lang/Class;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/AnnotatedElement;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public v()I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    return v0
.end method

.method public w()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredClasses()[Ljava/lang/Class;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 36
    invoke-static {v0}, Lkotlin/a/g;->l([Ljava/lang/Object;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$e;

    check-cast v0, Lkotlin/d/a/b;

    .line 37
    invoke-static {v1, v0}, Lkotlin/g/h;->b(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$f;

    check-cast v0, Lkotlin/d/a/b;

    .line 43
    invoke-static {v1, v0}, Lkotlin/g/h;->e(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    .line 44
    invoke-static {v0}, Lkotlin/g/h;->d(Lkotlin/g/g;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Class;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;-><init>(Ljava/lang/Class;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 60
    invoke-static {v0}, Lkotlin/a/g;->l([Ljava/lang/Object;)Lkotlin/g/g;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$g;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;)V

    check-cast v0, Lkotlin/d/a/b;

    .line 61
    invoke-static {v1, v0}, Lkotlin/g/h;->a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$h;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$h;

    check-cast v0, Lkotlin/d/a/b;

    .line 68
    invoke-static {v1, v0}, Lkotlin/g/h;->e(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    .line 69
    invoke-static {v0}, Lkotlin/g/h;->d(Lkotlin/g/g;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public z()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 81
    invoke-static {v0}, Lkotlin/a/g;->l([Ljava/lang/Object;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$c;

    check-cast v0, Lkotlin/d/a/b;

    .line 82
    invoke-static {v1, v0}, Lkotlin/g/h;->a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j$d;

    check-cast v0, Lkotlin/d/a/b;

    .line 83
    invoke-static {v1, v0}, Lkotlin/g/h;->e(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    .line 84
    invoke-static {v0}, Lkotlin/g/h;->d(Lkotlin/g/g;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
