.class public abstract Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;
.source "ReflectJavaMember.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f;
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract L_()Ljava/lang/reflect/Member;
.end method

.method public synthetic a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->j()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method protected final a([Ljava/lang/reflect/Type;[[Ljava/lang/annotation/Annotation;Z)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/reflect/Type;",
            "[[",
            "Ljava/lang/annotation/Annotation;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/y;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const-string v0, "parameterTypes"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parameterAnnotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    new-instance v2, Ljava/util/ArrayList;

    move-object v0, p1

    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 48
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;->b(Ljava/lang/reflect/Member;)Ljava/util/List;

    move-result-object v1

    .line 49
    array-length v0, p1

    add-int/lit8 v6, v0, -0x1

    if-gt v4, v6, :cond_2

    move v3, v4

    .line 50
    :goto_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;

    aget-object v5, p1, v3

    invoke-virtual {v0, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;->a(Ljava/lang/reflect/Type;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v7

    .line 51
    if-eqz v1, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    .line 52
    :goto_1
    if-eqz p3, :cond_1

    move-object v0, p1

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/a/g;->g([Ljava/lang/Object;)I

    move-result v0

    if-ne v3, v0, :cond_1

    const/4 v0, 0x1

    .line 53
    :goto_2
    new-instance v8, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/y;

    aget-object v9, p2, v3

    invoke-direct {v8, v7, v9, v5, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/y;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;[Ljava/lang/annotation/Annotation;Ljava/lang/String;Z)V

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    if-eq v3, v6, :cond_2

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 51
    :cond_0
    const/4 v0, 0x0

    move-object v5, v0

    goto :goto_1

    :cond_1
    move v0, v4

    .line 52
    goto :goto_2

    :cond_2
    move-object v0, v2

    .line 55
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;
    .locals 1

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f$a;->b(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f;)Z

    move-result v0

    return v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->i()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 58
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/reflect/Member;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "member.declaringClass"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/j;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/f;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z

    move-result v0

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;->b(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z

    move-result v0

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;->c(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z

    move-result v0

    return v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;->d(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    return-object v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/reflect/Member;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/e/h;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v1, "SpecialNames.NO_NAME_PROVIDED"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public t()Ljava/lang/reflect/AnnotatedElement;
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.reflect.AnnotatedElement"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/reflect/AnnotatedElement;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v()I
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;->L_()Ljava/lang/reflect/Member;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/reflect/Member;->getModifiers()I

    move-result v0

    return v0
.end method
