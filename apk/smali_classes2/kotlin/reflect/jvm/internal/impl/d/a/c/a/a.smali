.class public Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;
.super Ljava/lang/Object;
.source "DeclaredMemberIndex.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;


# instance fields
.field private final a:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/q;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/q;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/n;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

.field private final e:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/p;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/d/a/b;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/g;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/p;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "jClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "memberFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->e:Lkotlin/d/a/b;

    .line 43
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;)V

    check-cast v0, Lkotlin/d/a/b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->a:Lkotlin/d/a/b;

    .line 48
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->q(Ljava/lang/Iterable;)Lkotlin/g/g;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->a:Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/g/h;->a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    .line 65
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 66
    invoke-interface {v1}, Lkotlin/g/g;->a()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 67
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    .line 48
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    .line 69
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 70
    if-nez v1, :cond_0

    .line 71
    nop

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 72
    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 76
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    :cond_1
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->b:Ljava/util/Map;

    .line 49
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->l()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->q(Ljava/lang/Iterable;)Lkotlin/g/g;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->e:Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/g/h;->a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    .line 79
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 80
    invoke-interface {v1}, Lkotlin/g/g;->a()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 81
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    .line 49
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 83
    :cond_2
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/q;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/util/Collection;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_0
.end method

.method public a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->q(Ljava/lang/Iterable;)Lkotlin/g/g;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->a:Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/g/h;->a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    .line 59
    invoke-interface {v1}, Lkotlin/g/g;->a()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 60
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    .line 52
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 61
    :cond_0
    check-cast v0, Ljava/util/Set;

    .line 52
    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->l()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->q(Ljava/lang/Iterable;)Lkotlin/g/g;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->e:Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/g/h;->a(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    .line 62
    invoke-interface {v1}, Lkotlin/g/g;->a()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 63
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    .line 55
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 64
    :cond_0
    check-cast v0, Ljava/util/Set;

    .line 55
    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/n;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    return-object v0
.end method

.method public final c()Lkotlin/d/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/p;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;->e:Lkotlin/d/a/b;

    return-object v0
.end method
