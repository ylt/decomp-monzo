.class public abstract Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;
.super Lkotlin/reflect/jvm/internal/impl/h/e/i;
.source "LazyJavaScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;,
        Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final g:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final h:Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "functionNamesLazy"

    const-string v5, "getFunctionNamesLazy()Ljava/util/Set;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "propertyNamesLazy"

    const-string v5, "getPropertyNamesLazy()Ljava/util/Set;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
    .locals 3

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/h/e/i;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    .line 57
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 66
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$d;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 75
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->e:Lkotlin/reflect/jvm/internal/impl/j/c;

    .line 217
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->f:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 218
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$h;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$h;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->g:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 236
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->h:Lkotlin/reflect/jvm/internal/impl/j/c;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 254
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->b(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Lkotlin/reflect/jvm/internal/impl/b/b/y;

    move-result-object v1

    .line 255
    invoke-virtual {v1, v0, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/b/z;Lkotlin/reflect/jvm/internal/impl/b/ai;)V

    .line 257
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    const-string v3, "propertyDescriptor.annotations"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    .line 259
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->f()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1, v2, v3, v4, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    .line 261
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/av;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/av;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$i;

    invoke-direct {v0, p0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$i;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;Lkotlin/reflect/jvm/internal/impl/d/a/f/n;Lkotlin/reflect/jvm/internal/impl/b/b/y;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v2, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/g;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/j/g;)V

    .line 268
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->g()Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;Lkotlin/reflect/jvm/internal/impl/b/ag;)V

    .line 270
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ag;

    return-object v1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 289
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->c(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->h()Lkotlin/reflect/jvm/internal/impl/d/a/a/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v3, 0x1

    .line 290
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v7

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v8

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZILkotlin/d/b/i;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-virtual {v7, v8, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 294
    if-nez v3, :cond_1

    .line 295
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v1, "TypeUtils.makeNotNullable(propertyType)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    :cond_1
    return-object v0

    :cond_2
    move v3, v4

    .line 289
    goto :goto_0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Lkotlin/reflect/jvm/internal/impl/b/b/y;
    .locals 8

    .prologue
    .line 274
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->p()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v4, 0x1

    .line 275
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    .line 277
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->h()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->q()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v3

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v6

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v7

    move-object v6, p1

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v7, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v6

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->c(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Z

    move-result v7

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Z)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    move-result-object v0

    const-string v1, "JavaPropertyDescriptor.c\u2026d.isFinalStatic\n        )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/y;

    return-object v0

    .line 274
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private final c()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->f:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Z
    .locals 1

    .prologue
    .line 284
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final e()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->g:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public A_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public B_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->e()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->B_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 303
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->h:Lkotlin/reflect/jvm/internal/impl/j/c;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method protected final a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 313
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 315
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->h()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->d(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 317
    invoke-interface {p2, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 319
    check-cast v1, Ljava/util/Collection;

    invoke-virtual {p0, v0, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;Ljava/lang/Object;)V

    goto :goto_0

    .line 324
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/c$a;->a:Lkotlin/reflect/jvm/internal/impl/h/e/c$a;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 325
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->c(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 326
    invoke-interface {p2, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 327
    invoke-virtual {p0, v0, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 332
    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/c$a;->a:Lkotlin/reflect/jvm/internal/impl/h/e/c$a;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 333
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->e(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 334
    invoke-interface {p2, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 335
    invoke-virtual {p0, v0, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 340
    :cond_5
    check-cast v2, Ljava/util/Collection;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;)Lkotlin/reflect/jvm/internal/impl/d/a/b/d;
    .locals 12

    .prologue
    const-string v0, "method"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v6

    .line 115
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->h()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-static {v1, v6, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    move-result-object v8

    .line 119
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    const-string v1, "functionDescriptorImpl"

    invoke-static {v8, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v8

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    move-object v2, p1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/x;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;IILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v9

    .line 121
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->s()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 358
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 359
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 360
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    .line 121
    invoke-virtual {v9}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    move-result-object v3

    invoke-interface {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/j;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/w;)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 361
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 122
    const-string v0, "functionDescriptorImpl"

    invoke-static {v8, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v8

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->c()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v9, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;

    move-result-object v10

    .line 124
    invoke-virtual {p0, p1, v6, v9}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 126
    invoke-virtual {v10}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;

    move-result-object v11

    .line 128
    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;->b()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->f()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v2

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;->d()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;->c()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/u;->e:Lkotlin/reflect/jvm/internal/impl/b/u$a;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->n()Z

    move-result v7

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->p()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v6, v7, v0}, Lkotlin/reflect/jvm/internal/impl/b/u$a;->a(ZZ)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v6

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->q()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v7

    move-object v0, v8

    invoke-virtual/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    .line 138
    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;->e()Z

    move-result v0

    invoke-virtual {v10}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;->b()Z

    move-result v1

    invoke-virtual {v8, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/b/d;->a(ZZ)V

    .line 140
    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;->f()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_2

    .line 141
    invoke-virtual {v9}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->e()Lkotlin/reflect/jvm/internal/impl/d/a/a/n;

    move-result-object v1

    move-object v0, v8

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-virtual {v11}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/n;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/util/List;)V

    .line 144
    :cond_2
    const-string v0, "functionDescriptorImpl"

    invoke-static {v8, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v8

    .line 128
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 140
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected abstract a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/q;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;"
        }
    .end annotation
.end method

.method protected final a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/f;",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/y;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;"
        }
    .end annotation

    .prologue
    const-string v1, "c"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "function"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "jValueParameters"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    new-instance v17, Lkotlin/d/b/x$a;

    invoke-direct/range {v17 .. v17}, Lkotlin/d/b/x$a;-><init>()V

    const/4 v1, 0x0

    move-object/from16 v0, v17

    iput-boolean v1, v0, Lkotlin/d/b/x$a;->a:Z

    move-object/from16 v1, p3

    .line 168
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v2

    .line 362
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v16, v1

    check-cast v16, Ljava/util/Collection;

    .line 363
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 364
    check-cast v1, Lkotlin/a/x;

    .line 169
    invoke-virtual {v1}, Lkotlin/a/x;->c()I

    move-result v19

    invoke-virtual {v1}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/d/a/f/y;

    move-object v1, v8

    .line 171
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    .line 172
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->d:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZILkotlin/d/b/i;)V

    .line 174
    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/d/a/f/y;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 175
    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/d/a/f/y;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v2

    instance-of v4, v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;

    if-nez v4, :cond_0

    const/4 v2, 0x0

    :cond_0
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;

    if-eqz v2, :cond_2

    .line 177
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v4

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v1, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Z)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 178
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    invoke-virtual {v2, v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    .line 174
    :goto_1
    invoke-virtual {v1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v10

    .line 173
    check-cast v10, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 184
    invoke-interface/range {p2 .. p2}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "equals"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 186
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-static {v1, v10}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 191
    const-string v1, "other"

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v9

    .line 200
    :cond_1
    :goto_2
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    move-object/from16 v5, p2

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/b/a;

    const/4 v6, 0x0

    const-string v1, "name"

    invoke-static {v9, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v1

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v1, v8}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v15

    check-cast v15, Lkotlin/reflect/jvm/internal/impl/b/al;

    move/from16 v7, v19

    move-object v8, v3

    invoke-direct/range {v4 .. v15}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 212
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 176
    :cond_2
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Vararg parameter should be an array: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 181
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v2

    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/d/a/f/y;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v4

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-virtual {v2, v4, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    goto/16 :goto_1

    .line 195
    :cond_4
    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/d/a/f/y;->c()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v9

    .line 196
    if-nez v9, :cond_5

    const/4 v1, 0x1

    move-object/from16 v0, v17

    iput-boolean v1, v0, Lkotlin/d/b/x$a;->a:Z

    .line 197
    :cond_5
    if-nez v9, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "p"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v9

    goto/16 :goto_2

    .line 365
    :cond_6
    check-cast v16, Ljava/util/List;

    check-cast v16, Ljava/lang/Iterable;

    .line 213
    invoke-static/range {v16 .. v16}, Lkotlin/a/m;->k(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 214
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;

    move-object/from16 v0, v17

    iget-boolean v3, v0, Lkotlin/d/b/x$a;->a:Z

    invoke-direct {v2, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;-><init>(Ljava/util/List;Z)V

    return-object v2
.end method

.method protected final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 4

    .prologue
    const-string v0, "method"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "c"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->e()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->h()Z

    move-result v2

    .line 149
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->c:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-direct {v0, v3, p2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZ)V

    .line 154
    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    invoke-virtual {v1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 156
    if-eqz v2, :cond_0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v1, "TypeUtils.makeNotNullable(it)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0

    .line 149
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected abstract a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")V"
        }
    .end annotation
.end method

.method protected abstract a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;)V"
        }
    .end annotation
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/d/a/b/d;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->A_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 225
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->e:Lkotlin/reflect/jvm/internal/impl/j/c;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_0
.end method

.method protected c(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected abstract d(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract d()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;
.end method

.method protected e(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected abstract f()Lkotlin/reflect/jvm/internal/impl/b/aj;
.end method

.method protected abstract h()Lkotlin/reflect/jvm/internal/impl/b/m;
.end method

.method protected final i()Lkotlin/reflect/jvm/internal/impl/j/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-object v0
.end method

.method protected final j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Lazy scope for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->h()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
