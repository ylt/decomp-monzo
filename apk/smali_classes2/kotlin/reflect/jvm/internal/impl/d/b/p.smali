.class public final Lkotlin/reflect/jvm/internal/impl/d/b/p;
.super Ljava/lang/Object;
.source "JvmPackagePartSource.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/a/f;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/h/d/b;

.field private final c:Lkotlin/reflect/jvm/internal/impl/h/d/b;

.field private final d:Lkotlin/reflect/jvm/internal/impl/i/b/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/i/b/u",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/m;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/u;Lkotlin/reflect/jvm/internal/impl/i/b/u;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/u",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/m;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "kotlinClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v3

    const-string v0, "JvmClassName.byClassId(kotlinClass.classId)"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 40
    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v0

    .line 33
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-object v1, v3

    .line 37
    :goto_2
    invoke-direct {p0, v1, v0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/p;-><init>(Lkotlin/reflect/jvm/internal/impl/h/d/b;Lkotlin/reflect/jvm/internal/impl/h/d/b;Lkotlin/reflect/jvm/internal/impl/i/b/u;Z)V

    return-void

    .line 40
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, v2

    goto :goto_1

    :cond_2
    move-object v0, v2

    move-object v1, v3

    .line 33
    goto :goto_2
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/h/d/b;Lkotlin/reflect/jvm/internal/impl/h/d/b;Lkotlin/reflect/jvm/internal/impl/i/b/u;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/d/b;",
            "Lkotlin/reflect/jvm/internal/impl/h/d/b;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/u",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/m;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const-string v0, "className"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/p;->b:Lkotlin/reflect/jvm/internal/impl/h/d/b;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/p;->c:Lkotlin/reflect/jvm/internal/impl/h/d/b;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/p;->d:Lkotlin/reflect/jvm/internal/impl/i/b/u;

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/d/b/p;->e:Z

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/b/am;
    .locals 2

    .prologue
    .line 55
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/am;->a:Lkotlin/reflect/jvm/internal/impl/b/am;

    const-string v1, "SourceFile.NO_SOURCE_FILE"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 49
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/p;->b:Lkotlin/reflect/jvm/internal/impl/h/d/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2f

    const/4 v2, 0x2

    invoke-static {v0, v1, v3, v2, v3}, Lkotlin/h/j;->c(Ljava/lang/String;CLjava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "Name.identifier(classNam\u2026.substringAfterLast(\'/\'))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/p;->b:Lkotlin/reflect/jvm/internal/impl/h/d/b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/p;->b()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/h/d/b;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/p;->c:Lkotlin/reflect/jvm/internal/impl/h/d/b;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/p;->b:Lkotlin/reflect/jvm/internal/impl/h/d/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
