.class Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b$1;
.super Lkotlin/reflect/jvm/internal/impl/d/b/a/b$a;
.source "ReadKotlinClassHeaderAnnotationVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->b()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b$1;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a([Ljava/lang/String;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "result"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor$1"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "visitEnd"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b$1;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;[Ljava/lang/String;)[Ljava/lang/String;

    .line 179
    return-void
.end method
