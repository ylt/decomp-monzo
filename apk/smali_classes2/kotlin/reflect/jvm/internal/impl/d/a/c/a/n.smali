.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;
.super Lkotlin/reflect/jvm/internal/impl/b/b/b;
.source "LazyJavaTypeParameterDescriptor.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/d/a/c/c;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/a/f/w;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/w;ILkotlin/reflect/jvm/internal/impl/b/m;)V
    .locals 9

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "javaTypeParameter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingDeclaration"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    const/4 v5, 0x0

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->m()Lkotlin/reflect/jvm/internal/impl/b/ao;

    move-result-object v8

    move-object v0, p0

    move-object v2, p4

    move v6, p3

    .line 36
    invoke-direct/range {v0 .. v8}, Lkotlin/reflect/jvm/internal/impl/b/b/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/ap;ZILkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ao;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->c:Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    .line 45
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->c:Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)V

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/c;

    return-void
.end method


# virtual methods
.method protected a(Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 1

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method protected m()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->c:Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;->c()Ljava/util/Collection;

    move-result-object v0

    .line 49
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->x()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "c.module.builtIns.anyType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    const-string v2, "c.module.builtIns.nullableAnyType"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 68
    :goto_0
    return-object v6

    .line 55
    :cond_0
    check-cast v0, Ljava/lang/Iterable;

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    move-object v6, v2

    check-cast v6, Ljava/util/Collection;

    .line 66
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 67
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/j;

    .line 56
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v9

    move-object v7, v0

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->b:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-object v3, p0

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/aq;

    const/4 v4, 0x3

    const/4 v5, 0x0

    move v2, v1

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    invoke-virtual {v9, v7, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 68
    :cond_1
    check-cast v6, Ljava/util/List;

    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/d/a/c/c;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/c;

    return-object v0
.end method

.method public synthetic w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;->n()Lkotlin/reflect/jvm/internal/impl/d/a/c/c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method
