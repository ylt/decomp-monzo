.class public Lkotlin/reflect/jvm/internal/impl/d/a/g;
.super Ljava/lang/Object;
.source "JavaVisibilities.java"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/b/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 30
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g$1;

    const-string v1, "package"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g$1;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 68
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g$2;

    const-string v1, "protected_static"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g$2;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 93
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g$3;

    const-string v1, "protected_and_package"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g$3;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 25
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g;->b(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 1

    .prologue
    .line 25
    invoke-static {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/g;->b(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    return v0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "first"

    aput-object v5, v4, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities"

    aput-object v3, v4, v2

    const-string v2, "areInSamePackage"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "second"

    aput-object v5, v4, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities"

    aput-object v3, v4, v2

    const-string v2, "areInSamePackage"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_1
    const-class v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-static {p0, v0, v3}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;Z)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    .line 139
    const-class v1, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-static {p1, v1, v3}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;Z)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/y;

    .line 140
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    return v0

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v0, 0x1

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "what"

    aput-object v4, v3, v5

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities"

    aput-object v4, v3, v0

    const-string v0, "isVisibleForProtectedAndPackage"

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "from"

    aput-object v4, v3, v5

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities"

    aput-object v4, v3, v0

    const-string v0, "isVisibleForProtectedAndPackage"

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 130
    :cond_1
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/q;)Lkotlin/reflect/jvm/internal/impl/b/q;

    move-result-object v1

    invoke-static {v1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/g;->b(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 134
    :goto_0
    return v0

    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-virtual {v0, p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/ay;->a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    goto :goto_0
.end method
