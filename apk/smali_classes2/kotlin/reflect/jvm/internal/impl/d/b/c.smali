.class public final Lkotlin/reflect/jvm/internal/impl/d/b/c;
.super Lkotlin/reflect/jvm/internal/impl/d/b/a;
.source "BinaryClassAnnotationAndConstantLoaderImpl.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/d/b/a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
        "Lkotlin/reflect/jvm/internal/impl/h/b/f",
        "<*>;",
        "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/i/b/e;

.field private final c:Lkotlin/reflect/jvm/internal/impl/h/b/g;

.field private final d:Lkotlin/reflect/jvm/internal/impl/b/w;

.field private final e:Lkotlin/reflect/jvm/internal/impl/i/b/aa;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/d/b/t;)V
    .locals 3

    .prologue
    const-string v0, "module"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notFoundClasses"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storageManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kotlinClassFinder"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0, p3, p4}, Lkotlin/reflect/jvm/internal/impl/d/b/a;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/d/b/t;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->d:Lkotlin/reflect/jvm/internal/impl/b/w;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    .line 48
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/e;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->d:Lkotlin/reflect/jvm/internal/impl/b/w;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/aa;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->b:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    .line 49
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/g;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->d:Lkotlin/reflect/jvm/internal/impl/b/w;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->c:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/c;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->d:Lkotlin/reflect/jvm/internal/impl/b/w;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->e:Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    invoke-static {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/s;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b/aa;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/c;)Lkotlin/reflect/jvm/internal/impl/h/b/g;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->c:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    return-object v0
.end method


# virtual methods
.method public synthetic a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->b(Ljava/lang/String;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->b(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "annotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    check-cast p1, Ljava/lang/Iterable;

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 182
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 183
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 82
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lkotlin/reflect/jvm/internal/impl/b/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method protected a(Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/e;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0xa

    const-string v0, "propertyAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fieldAnnotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fieldUseSiteTarget"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    check-cast p1, Ljava/lang/Iterable;

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 174
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 175
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 77
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lkotlin/reflect/jvm/internal/impl/b/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    check-cast p2, Ljava/lang/Iterable;

    .line 177
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p2, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 178
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 179
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 78
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    invoke-direct {v4, v2, p3}, Lkotlin/reflect/jvm/internal/impl/b/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 180
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 77
    invoke-static {v0, v1}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Lkotlin/reflect/jvm/internal/impl/b/al;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u$a;"
        }
    .end annotation

    .prologue
    const-string v0, "annotationClassId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    .line 92
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    invoke-direct {v0, p0, v1, p3, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/c;Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    return-object v0
.end method

.method protected b(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 1

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->b:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    invoke-virtual {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    return-object v0
.end method

.method protected b(Ljava/lang/String;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "desc"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initializer"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    const-string v0, "ZBCS"

    check-cast v0, Ljava/lang/CharSequence;

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 56
    if-nez p2, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Int"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 57
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 62
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 57
    :sswitch_0
    const-string v1, "S"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    int-to-short v0, v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    .line 69
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c;->c:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    return-object v0

    .line 57
    :sswitch_1
    const-string v1, "B"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    int-to-byte v0, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0

    .line 57
    :sswitch_2
    const-string v1, "C"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60
    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    goto :goto_0

    .line 57
    :sswitch_3
    const-string v1, "Z"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move-object v0, p2

    .line 66
    goto :goto_0

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x42 -> :sswitch_1
        0x43 -> :sswitch_2
        0x53 -> :sswitch_0
        0x5a -> :sswitch_3
    .end sparse-switch
.end method
