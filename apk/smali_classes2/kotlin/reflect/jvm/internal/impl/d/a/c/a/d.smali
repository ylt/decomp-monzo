.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;
.super Ljava/lang/Object;
.source "JvmPackageScope.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/h/e/h;


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

.field private final d:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final e:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final f:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kotlinScopes"

    const-string v5, "getKotlinScopes()Ljava/util/List;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/t;Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V
    .locals 3

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jPackage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageFragment"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->e:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    .line 41
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->e:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    invoke-direct {v0, v1, p2, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/t;Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->e:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->e:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->n()Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-result-object v1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-static {v1, p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/c/a;->a(Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 97
    return-void
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->e:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    return-object v0
.end method

.method private final d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public A_()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->d()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Set;

    check-cast v1, Ljava/util/Collection;

    .line 129
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 130
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 73
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->A_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 131
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 73
    check-cast v0, Ljava/util/Set;

    .line 74
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->A_()Ljava/util/Set;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 75
    nop

    .line 73
    check-cast v1, Ljava/util/Set;

    .line 75
    return-object v1
.end method

.method public B_()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->d()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Set;

    check-cast v1, Ljava/util/Collection;

    .line 134
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 135
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 76
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->B_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 136
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 76
    check-cast v0, Ljava/util/Set;

    .line 77
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->B_()Ljava/util/Set;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 78
    nop

    .line 76
    check-cast v1, Ljava/util/Set;

    .line 78
    return-object v1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->a(Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 60
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->d()Ljava/util/List;

    move-result-object v1

    .line 114
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 60
    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 115
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 116
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 60
    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/l/b/a;->a(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    move-object v1, v0

    .line 115
    goto :goto_0

    .line 118
    :cond_0
    if-eqz v1, :cond_1

    :goto_1
    return-object v1

    :cond_1
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    move-object v1, v0

    goto :goto_1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->d()Ljava/util/List;

    move-result-object v1

    .line 124
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 71
    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 125
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 126
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 71
    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/l/b/a;->a(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    move-object v1, v0

    .line 125
    goto :goto_0

    .line 128
    :cond_0
    if-eqz v1, :cond_1

    .line 71
    :goto_1
    return-object v1

    .line 128
    :cond_1
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    move-object v1, v0

    goto :goto_1
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->a(Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->d()Ljava/util/List;

    move-result-object v1

    .line 119
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 65
    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 120
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 121
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 65
    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/l/b/a;->a(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    move-object v1, v0

    .line 120
    goto :goto_0

    .line 123
    :cond_0
    if-eqz v1, :cond_1

    :goto_1
    return-object v1

    :cond_1
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    move-object v1, v0

    goto :goto_1
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 4

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->a(Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 52
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    invoke-virtual {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    .line 113
    :cond_0
    :goto_0
    return-object v0

    .line 55
    :cond_1
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->d()Ljava/util/List;

    move-result-object v1

    .line 100
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    .line 101
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 102
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 55
    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    .line 103
    if-eqz v0, :cond_3

    .line 104
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/i;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    if-nez v2, :cond_3

    :goto_2
    move-object v2, v0

    .line 101
    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 113
    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    return-object v0
.end method
