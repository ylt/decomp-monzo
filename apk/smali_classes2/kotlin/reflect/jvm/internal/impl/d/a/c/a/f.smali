.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;
.super Lkotlin/reflect/jvm/internal/impl/b/b/g;
.source "LazyJavaClassDescriptor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/b/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;
    }
.end annotation


# static fields
.field static final synthetic c:[Lkotlin/reflect/l;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final e:Lkotlin/reflect/jvm/internal/impl/b/f;

.field private final f:Lkotlin/reflect/jvm/internal/impl/b/u;

.field private final g:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field private final h:Z

.field private final i:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

.field private final k:Lkotlin/reflect/jvm/internal/impl/h/e/f;

.field private final l:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

.field private final m:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final n:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;>;"
        }
    .end annotation
.end field

.field private final o:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

.field private final q:Lkotlin/reflect/jvm/internal/impl/b/e;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "annotations"

    const-string v5, "getAnnotations()Lorg/jetbrains/kotlin/descriptors/annotations/Annotations;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->c:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/b/e;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    const-string v0, "outerContext"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingDeclaration"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jClass"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v2

    move-object v0, p3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v4

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object v0, p0

    move-object v2, p2

    .line 60
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/b/g;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Z)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->o:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->q:Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 64
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->o:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-object v3, p0

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/m;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/d/a/f/x;

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;IILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    .line 67
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->g()Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/f/aa;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Creating LazyJavaClassDescriptor for light class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 69
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    move v0, v5

    goto :goto_0

    .line 75
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 74
    :goto_1
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 81
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 82
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 81
    :goto_2
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->f:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 85
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->q()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->g:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 86
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->f()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->o()Z

    move-result v0

    if-nez v0, :cond_9

    :goto_3
    iput-boolean v8, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->h:Z

    .line 106
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$d;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->i:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 109
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-direct {v1, v2, v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)V

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->j:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    .line 112
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/e/f;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/f;-><init>(Lkotlin/reflect/jvm/internal/impl/h/e/h;)V

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->k:Lkotlin/reflect/jvm/internal/impl/h/e/f;

    .line 115
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-direct {v0, v1, v2, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->l:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

    .line 124
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 126
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->n:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void

    .line 76
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto/16 :goto_1

    .line 77
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto/16 :goto_1

    .line 78
    :cond_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto/16 :goto_1

    .line 83
    :cond_5
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/u;->e:Lkotlin/reflect/jvm/internal/impl/b/u$a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->n()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v8

    :goto_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->p()Z

    move-result v1

    if-nez v1, :cond_8

    move v1, v8

    :goto_5
    invoke-virtual {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/u$a;->a(ZZ)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    goto/16 :goto_2

    :cond_7
    move v0, v5

    goto :goto_4

    :cond_8
    move v1, v5

    goto :goto_5

    :cond_9
    move v8, v5

    .line 86
    goto/16 :goto_3
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/b/e;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    return-void

    :cond_0
    move-object v0, p4

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/g;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    return-object v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->q:Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method


# virtual methods
.method public B()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->k:Lkotlin/reflect/jvm/internal/impl/h/e/f;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/a/h;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;
    .locals 4

    .prologue
    const-string v0, "javaResolverCache"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->d:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-virtual {v2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/b;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    const-string v3, "containingDeclaration"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-direct {v0, v1, v2, v3, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    .line 251
    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->j:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->j:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->e()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->l:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->i:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    return-object v0
.end method

.method public synthetic g()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public l()Lkotlin/reflect/jvm/internal/impl/b/f;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    return-object v0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->f:Lkotlin/reflect/jvm/internal/impl/b/u;

    return-object v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/b/d;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->g:Lkotlin/reflect/jvm/internal/impl/b/ay;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->p:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->f()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->g:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->h:Z

    return v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Lazy Java class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->m:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->c:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method

.method public y()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->n:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
