.class Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;
.super Ljava/lang/Object;
.source "ReadKotlinClassHeaderAnnotationVisitor.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;


# direct methods
.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/a/b$1;)V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;)V

    return-void
.end method

.method private b()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
    .locals 5

    .prologue
    .line 246
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c$1;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c$1;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "dataArrayVisitor"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method private c()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
    .locals 5

    .prologue
    .line 256
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c$2;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c$2;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "stringsArrayVisitor"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitAnnotation"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "classId"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitAnnotation"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "visitArray"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 233
    const-string v1, "data"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "filePartClassNames"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 234
    :cond_1
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;->b()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 236
    :cond_2
    const-string v1, "strings"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 237
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;->c()Lkotlin/reflect/jvm/internal/impl/d/b/u$b;

    move-result-object v0

    goto :goto_0

    .line 240
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 211
    if-nez p1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 214
    const-string v1, "version"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 215
    instance-of v0, p2, [I

    if-eqz v0, :cond_0

    .line 216
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-object v0, p2

    check-cast v0, [I

    check-cast v0, [I

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/m;-><init>([I)V

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/b/m;)Lkotlin/reflect/jvm/internal/impl/d/b/m;

    .line 219
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;)Lkotlin/reflect/jvm/internal/impl/d/a/k;

    move-result-object v0

    if-nez v0, :cond_0

    .line 220
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/k;

    check-cast p2, [I

    check-cast p2, [I

    invoke-direct {v1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/k;-><init>([I)V

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Lkotlin/reflect/jvm/internal/impl/d/a/k;)Lkotlin/reflect/jvm/internal/impl/d/a/k;

    goto :goto_0

    .line 224
    :cond_2
    const-string v1, "multifileClassName"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/a/b$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/a/b;

    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast p2, Ljava/lang/String;

    :goto_1
    invoke-static {v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/b;->a(Lkotlin/reflect/jvm/internal/impl/d/b/a/b;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    goto :goto_1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitEnum"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "enumClassId"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitEnum"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "enumEntryName"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor"

    aput-object v3, v2, v5

    const-string v3, "visitEnum"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :cond_2
    return-void
.end method
