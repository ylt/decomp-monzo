.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;
.super Lkotlin/reflect/jvm/internal/impl/utils/b$a;
.source "LazyJavaStaticClassScope.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Set;Lkotlin/d/a/b;)Ljava/util/Set;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/utils/b$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/e;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/b/e;

.field final synthetic b:Ljava/util/Set;

.field final synthetic c:Lkotlin/d/a/b;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/Set;Lkotlin/d/a/b;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;->a:Lkotlin/reflect/jvm/internal/impl/b/e;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;->b:Ljava/util/Set;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;->c:Lkotlin/d/a/b;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/utils/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 117
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    return v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const-string v1, "current"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;->a:Lkotlin/reflect/jvm/internal/impl/b/e;

    if-ne p1, v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 120
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->d()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    .line 122
    instance-of v2, v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/m;

    if-eqz v2, :cond_0

    .line 123
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;->b:Ljava/util/Set;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;->c:Lkotlin/d/a/b;

    const-string v3, "staticScope"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 124
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l$f;->a()V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method
