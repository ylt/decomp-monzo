.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$c;
.super Lkotlin/d/b/m;
.source "LazyJavaClassDescriptor.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/b/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/b/aq;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->s()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 254
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 255
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 256
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    .line 129
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    move-result-object v3

    invoke-interface {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/j;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/w;)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 130
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Parameter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " surely belongs to class "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", so it must be resolved"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    move-object v0, v1

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 257
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 131
    return-object v1
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f$c;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
