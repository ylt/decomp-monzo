.class public final Lkotlin/reflect/jvm/internal/impl/d/b/ah;
.super Lkotlin/reflect/jvm/internal/impl/k/ak;
.source "UnsafeVarianceTypeSubstitution.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/a/i;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V
    .locals 2

    .prologue
    const-string v0, "kotlinBuiltIns"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/ak;-><init>()V

    .line 27
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/i;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v1

    invoke-static {v1}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/i;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a:Lkotlin/reflect/jvm/internal/impl/b/a/i;

    return-void
.end method

.method private final a(Ljava/util/Collection;I)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;I)",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 70
    check-cast p1, Ljava/lang/Iterable;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 106
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    check-cast v1, Ljava/util/List;

    .line 70
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    if-ne v1, p2, :cond_1

    move v1, v3

    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v1, v4

    goto :goto_1

    .line 107
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 70
    nop

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 109
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 110
    check-cast v0, Ljava/util/List;

    .line 70
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v0, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 111
    :cond_3
    check-cast v1, Ljava/util/List;

    .line 70
    return-object v1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/ao;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ao;",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;)",
            "Lkotlin/reflect/jvm/internal/impl/k/ao;"
        }
    .end annotation

    .prologue
    .line 43
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    :goto_0
    return-object p1

    .line 45
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 46
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Ljava/util/Collection;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {p0, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Ljava/util/Collection;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {p0, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    :goto_1
    move-object p1, v0

    .line 44
    goto :goto_0

    .line 49
    :cond_1
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v0, :cond_2

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    goto :goto_1

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/v;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;>;)",
            "Lkotlin/reflect/jvm/internal/impl/k/v;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 54
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    :goto_0
    return-object p1

    .line 57
    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a:Lkotlin/reflect/jvm/internal/impl/b/a/i;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/b/a/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object p1

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v1

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 102
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 103
    check-cast v1, Lkotlin/a/x;

    .line 62
    invoke-virtual {v1}, Lkotlin/a/x;->c()I

    move-result v4

    invoke-virtual {v1}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 63
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    :goto_2
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 64
    :cond_2
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v5

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v6

    invoke-direct {p0, p2, v4}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Ljava/util/Collection;I)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {p0, v6, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v2, v5, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v1, v2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_2

    .line 104
    :cond_3
    check-cast v0, Ljava/util/List;

    .line 61
    const/4 v1, 0x2

    invoke-static {p1, v0, v7, v1, v7}, Lkotlin/reflect/jvm/internal/impl/k/al;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Void;
    .locals 1

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-string v0, "topLevelType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "position"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 33
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v3, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;ILkotlin/d/b/i;)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/af;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/ah$b;

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$b;-><init>(Ljava/util/List;)V

    check-cast v2, Lkotlin/d/a/q;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/b/ah$c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ah$c;

    check-cast v3, Lkotlin/d/a/b;

    invoke-static {v1, p2, v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/ar;->a(Lkotlin/reflect/jvm/internal/impl/k/af;Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/d/a/q;Lkotlin/d/a/b;)Z

    .line 40
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v1

    check-cast v0, Ljava/util/Collection;

    invoke-direct {p0, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    return-object v0
.end method
