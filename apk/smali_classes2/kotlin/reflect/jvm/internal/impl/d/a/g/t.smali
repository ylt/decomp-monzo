.class public final Lkotlin/reflect/jvm/internal/impl/d/a/g/t;
.super Ljava/lang/Object;
.source "typeQualifiers.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Collection;Z)Lkotlin/d/a/b;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;Z)",
            "Lkotlin/d/a/b",
            "<",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/d;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fromSupertypes"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/t$a;

    move-object v0, p1

    .line 118
    check-cast v0, Ljava/lang/Iterable;

    .line 184
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 185
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 186
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 118
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/t$a;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 119
    invoke-virtual {v2, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v10

    .line 126
    if-eqz p2, :cond_5

    check-cast p1, Ljava/lang/Iterable;

    .line 188
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 126
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c;

    invoke-interface {v3, v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v5

    :goto_1
    if-eqz v0, :cond_1

    move v0, v5

    .line 189
    :goto_2
    if-eqz v0, :cond_5

    move v9, v5

    .line 128
    :goto_3
    if-eqz v9, :cond_6

    move v0, v5

    .line 129
    :goto_4
    nop

    .line 190
    new-array v4, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    .line 191
    add-int/lit8 v11, v0, -0x1

    if-gt v6, v11, :cond_d

    move v8, v6

    .line 131
    :goto_5
    if-nez v8, :cond_7

    move v7, v5

    .line 132
    :goto_6
    if-nez v7, :cond_2

    if-nez v9, :cond_8

    :cond_2
    move v0, v5

    :goto_7
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_9

    if-nez v0, :cond_9

    const-string v1, "Only head type constructors should be computed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    move v0, v6

    .line 126
    goto :goto_1

    :cond_4
    move v0, v6

    .line 189
    goto :goto_2

    :cond_5
    move v9, v6

    goto :goto_3

    .line 128
    :cond_6
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_4

    :cond_7
    move v7, v6

    .line 131
    goto :goto_6

    :cond_8
    move v0, v6

    .line 132
    goto :goto_7

    .line 134
    :cond_9
    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v2, v1

    .line 135
    check-cast v2, Ljava/lang/Iterable;

    .line 193
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 202
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_a
    :goto_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 201
    check-cast v2, Ljava/util/List;

    .line 135
    invoke-static {v2, v8}, Lkotlin/a/m;->c(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    if-eqz v2, :cond_a

    .line 201
    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 204
    :cond_b
    check-cast v3, Ljava/util/List;

    .line 138
    check-cast v3, Ljava/util/Collection;

    if-eqz p2, :cond_c

    if-eqz v7, :cond_c

    move v2, v5

    :goto_9
    invoke-static {v0, v3, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Collection;Z)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    move-result-object v0

    aput-object v0, v4, v8

    .line 191
    if-eq v8, v11, :cond_d

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_5

    :cond_c
    move v2, v6

    .line 138
    goto :goto_9

    :cond_d
    move-object v0, v4

    .line 205
    check-cast v0, [Ljava/lang/Object;

    .line 129
    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    .line 141
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$b;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$b;-><init>([Lkotlin/reflect/jvm/internal/impl/d/a/g/d;)V

    move-object v0, v1

    check-cast v0, Lkotlin/d/a/b;

    return-object v0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 54
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/o;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    new-instance v1, Lkotlin/h;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    check-cast v0, Lkotlin/h;

    move-object v1, v0

    .line 54
    :goto_0
    invoke-virtual {v1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    .line 53
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 58
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    .line 59
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    :goto_1
    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/f/a;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;

    invoke-direct {v5, v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/g;Lkotlin/reflect/jvm/internal/impl/d/a/g/e;Z)V

    return-object v5

    .line 56
    :cond_1
    new-instance v0, Lkotlin/h;

    invoke-direct {v0, p0, p0}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v1, v0

    goto :goto_0

    .line 59
    :cond_2
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    goto :goto_1

    :cond_3
    move-object v2, v3

    goto :goto_1

    :cond_4
    invoke-virtual {v4, v1}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    goto :goto_2
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;
    .locals 7

    .prologue
    .line 66
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$f;

    invoke-direct {v2, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$f;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 69
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/t$g;

    .line 70
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$h;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/t$h;

    .line 77
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$i;

    invoke-direct {v3, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$i;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 86
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$h;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/a/j;->a()Ljava/util/List;

    move-result-object v4

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    invoke-virtual {v2, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$f;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/a/j;->c()Ljava/util/List;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    invoke-virtual {v2, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$f;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/a/j;->b()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v6

    invoke-virtual {v3, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$i;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    move-result-object v3

    invoke-virtual {v0, v4, v5, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$h;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    .line 91
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$g;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/a/j;->d()Ljava/util/List;

    move-result-object v4

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    invoke-virtual {v2, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$f;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/a/j;->e()Ljava/util/List;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    invoke-virtual {v2, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$f;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-direct {v3, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/g;Lkotlin/reflect/jvm/internal/impl/d/a/g/e;Z)V

    return-object v3

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Collection;Z)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;Z)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/d;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 145
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 206
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 215
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 214
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 145
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->a()Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 214
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 145
    invoke-static {v1}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v4

    move-object v0, p1

    .line 146
    check-cast v0, Ljava/lang/Iterable;

    .line 218
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 227
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 226
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 146
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->b()Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 226
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 229
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 146
    invoke-static {v1}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v5

    .line 148
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    move-result-object v6

    .line 150
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->c()Z

    move-result v0

    if-nez v0, :cond_5

    check-cast p1, Ljava/lang/Iterable;

    .line 230
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 150
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    .line 231
    :goto_2
    if-eqz v0, :cond_7

    :cond_5
    move v1, v3

    .line 152
    :goto_3
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$c;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$c;-><init>(Z)V

    .line 159
    if-eqz p2, :cond_8

    .line 160
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/t$d;

    .line 164
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->a()Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    move-result-object v7

    invoke-virtual {v2, v4, v1, v3, v7}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$d;->a(Ljava/util/Set;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->b()Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    move-result-object v6

    invoke-virtual {v2, v5, v3, v4, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$d;->a(Ljava/util/Set;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/g;Lkotlin/reflect/jvm/internal/impl/d/a/g/e;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    move-result-object v0

    .line 177
    :goto_4
    return-object v0

    :cond_6
    move v0, v2

    .line 231
    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3

    .line 170
    :cond_8
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/t$e;

    .line 177
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->a()Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$e;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->b()Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    move-result-object v3

    invoke-virtual {v2, v5, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$e;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/g;Lkotlin/reflect/jvm/internal/impl/d/a/g/e;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    move-result-object v0

    goto :goto_4
.end method
