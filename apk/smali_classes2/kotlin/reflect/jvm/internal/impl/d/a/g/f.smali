.class public final Lkotlin/reflect/jvm/internal/impl/d/a/g/f;
.super Lkotlin/reflect/jvm/internal/impl/k/g;
.source "typeEnhancement.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/e;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/v;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/v;)V
    .locals 1

    .prologue
    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/k/g;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 239
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    .line 240
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 242
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    goto :goto_0
.end method


# virtual methods
.method public M_()Z
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/g/f;
    .locals 2

    .prologue
    const-string v0, "newAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1, p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;)V

    return-object v0
.end method

.method public a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 247
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/v;

    goto :goto_0
.end method

.method public a_(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 3

    .prologue
    const-string v0, "replacement"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v1

    move-object v0, v1

    .line 225
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/an;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 227
    :goto_0
    return-object v1

    .line 228
    :cond_0
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v0, :cond_1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    :goto_1
    move-object v1, v0

    .line 227
    goto :goto_0

    .line 229
    :cond_1
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_1

    .line 231
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incorrect type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/g/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic b(Z)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/g/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return v0
.end method

.method protected e()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method
