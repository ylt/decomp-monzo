.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/c;
.super Ljava/lang/Object;
.source "LazyJavaAnnotations.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/a/h;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/d",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final d:Lkotlin/reflect/jvm/internal/impl/d/a/f/d;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)V
    .locals 2

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationOwner"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->a()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/c$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/c;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->b:Lkotlin/reflect/jvm/internal/impl/j/d;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/c;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 3

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->b:Lkotlin/reflect/jvm/internal/impl/j/d;

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    if-eqz v0, :cond_0

    .line 37
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0, p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/b;)Z
    .locals 1

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$b;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    return v0
.end method

.method public c()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    check-cast p0, Ljava/lang/Iterable;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 57
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 58
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 44
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lkotlin/reflect/jvm/internal/impl/b/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 44
    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;->a()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->q(Ljava/lang/Iterable;)Lkotlin/g/g;

    move-result-object v1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->b:Lkotlin/reflect/jvm/internal/impl/j/d;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/g/h;->e(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/a/l$a;->x:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "KotlinBuiltIns.FQ_NAMES.deprecated"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->d:Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/c;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v1

    .line 48
    invoke-static {v0, v1}, Lkotlin/g/h;->a(Lkotlin/g/g;Ljava/lang/Object;)Lkotlin/g/g;

    move-result-object v0

    invoke-static {v0}, Lkotlin/g/h;->c(Lkotlin/g/g;)Lkotlin/g/g;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/g/g;->a()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
