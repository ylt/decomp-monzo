.class public final Lkotlin/reflect/jvm/internal/impl/d/b/d;
.super Ljava/lang/Object;
.source "DeserializationComponentsForJava.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/i/b/m;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/n;Lkotlin/reflect/jvm/internal/impl/d/b/g;Lkotlin/reflect/jvm/internal/impl/d/b/c;Lkotlin/reflect/jvm/internal/impl/d/a/c/e;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/i/b/r;Lkotlin/reflect/jvm/internal/impl/c/a/c;)V
    .locals 16

    .prologue
    const-string v1, "storageManager"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "moduleDescriptor"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "configuration"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "classDataFinder"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annotationAndConstantLoader"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "packageFragmentProvider"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "notFoundClasses"

    move-object/from16 v0, p7

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "errorReporter"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "lookupTracker"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-interface/range {p2 .. p2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    instance-of v2, v1, Lkotlin/reflect/jvm/internal/impl/f/b;

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    move-object v2, v1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/f/b;

    .line 44
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-object/from16 v5, p4

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/i/b/h;

    move-object/from16 v6, p5

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/i/b/c;

    move-object/from16 v7, p6

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/b/z;

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/i/b/v$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/v$a;

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/i/b/v;

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/d/b/h;->a:Lkotlin/reflect/jvm/internal/impl/d/b/h;

    check-cast v11, Lkotlin/reflect/jvm/internal/impl/i/b/t;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v12

    check-cast v12, Ljava/lang/Iterable;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/f/b;->a()Lkotlin/reflect/jvm/internal/impl/d/b/i;

    move-result-object v3

    if-eqz v3, :cond_1

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/b/a;

    move-object v14, v3

    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/f/b;->a()Lkotlin/reflect/jvm/internal/impl/d/b/i;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/b/ac;

    move-object v15, v2

    :goto_1
    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v13, p7

    invoke-direct/range {v1 .. v15}, Lkotlin/reflect/jvm/internal/impl/i/b/m;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/n;Lkotlin/reflect/jvm/internal/impl/i/b/h;Lkotlin/reflect/jvm/internal/impl/i/b/c;Lkotlin/reflect/jvm/internal/impl/b/z;Lkotlin/reflect/jvm/internal/impl/i/b/v;Lkotlin/reflect/jvm/internal/impl/i/b/r;Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/i/b/t;Ljava/lang/Iterable;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/i/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ac;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lkotlin/reflect/jvm/internal/impl/d/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    return-void

    :cond_1
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/b/a$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a$a;

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/b/a;

    move-object v14, v3

    goto :goto_0

    :cond_2
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/b/ac;

    move-object v15, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/i/b/m;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    return-object v0
.end method
