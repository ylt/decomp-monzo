.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;
.super Ljava/lang/Object;
.source "LazyJavaPackageScope.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/e/f;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)V
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 119
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->hashCode()I

    move-result v0

    return v0
.end method
