.class public final Lkotlin/reflect/jvm/internal/impl/d/b/m;
.super Lkotlin/reflect/jvm/internal/impl/i/b/g;
.source "JvmMetadataVersion.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/m$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/b/m;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/d/b/m;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/d/b/m$a;

.field private static e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/m$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/m$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/m;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m$a;

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/m;

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/m;-><init>([I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/m;->a:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    .line 38
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/m;

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/m;-><init>([I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/m;->b:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    return-void

    .line 35
    :array_0
    .array-data 4
        0x1
        0x1
        0x5
    .end array-data
.end method

.method public varargs constructor <init>([I)V
    .locals 1

    .prologue
    const-string v0, "numbers"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/g;-><init>([I)V

    return-void
.end method

.method public static final synthetic b()Z
    .locals 1

    .prologue
    .line 25
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/d/b/m;->e:Z

    return v0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 28
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/m;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/m$a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/m;->c()I

    move-result v1

    if-ne v1, v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/m;->d()I

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
