.class public final Lkotlin/reflect/jvm/internal/impl/d/a/i;
.super Ljava/lang/Object;
.source "JvmAnnotationNames.java"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final b:Ljava/lang/String;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/e/f;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final e:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final f:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final g:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final h:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final i:Lkotlin/reflect/jvm/internal/impl/e/b;

.field public static final j:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.Metadata"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "L"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/i;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->b:Ljava/lang/String;

    .line 41
    const-string v0, "value"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->c:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 43
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "org.jetbrains.annotations.NotNull"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 44
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "org.jetbrains.annotations.Nullable"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 45
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "org.jetbrains.annotations.Mutable"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 46
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "org.jetbrains.annotations.ReadOnly"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->g:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 48
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.jvm.PurelyImplements"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->h:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 51
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.jvm.internal.EnhancedNullability"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->i:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 52
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.jvm.internal.EnhancedMutability"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->j:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void
.end method
