.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;
.super Lkotlin/d/b/m;
.source "resolvers.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/f/w;",
        "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/g;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/g;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/w;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/w;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;
    .locals 4

    .prologue
    const-string v0, "typeParameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/g;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 45
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/g;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/g;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/g;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->c(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/g;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->d(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v3

    invoke-direct {v1, v0, p1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/w;ILkotlin/reflect/jvm/internal/impl/b/m;)V

    move-object v0, v1

    .line 44
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;

    .line 46
    :goto_0
    return-object v0

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
