.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f;
.super Ljava/lang/Object;
.source "JavaIncompatibilityRulesOverridabilityCondition.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/h/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/f$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/f$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f$a;

    return-void
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-eqz v0, :cond_0

    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 65
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v3

    .line 110
    :goto_0
    return v0

    .line 69
    :cond_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b;

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a;

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    .line 70
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 73
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->c(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v1

    move-object v0, p2

    .line 77
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->y()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-nez v0, :cond_b

    move-object v0, v2

    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->y()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    :cond_3
    invoke-static {v5, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 79
    if-eqz v0, :cond_5

    if-eqz v1, :cond_4

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->y()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v4

    .line 80
    goto :goto_0

    .line 84
    :cond_5
    instance-of v0, p3, Lkotlin/reflect/jvm/internal/impl/d/a/b/c;

    if-eqz v0, :cond_6

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v3

    .line 85
    goto :goto_0

    .line 90
    :cond_7
    if-eqz v1, :cond_8

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {p3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    move v0, v3

    goto :goto_0

    .line 102
    :cond_9
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_a

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_a

    .line 103
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 104
    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {p2, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Z)Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    invoke-static {v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v3

    .line 105
    goto/16 :goto_0

    :cond_a
    move v0, v4

    .line 110
    goto/16 :goto_0

    :cond_b
    move-object v0, p1

    goto :goto_1
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/h/d$a;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$a;->a:Lkotlin/reflect/jvm/internal/impl/h/d$a;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/d$b;
    .locals 1

    .prologue
    const-string v0, "superDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/f;->b(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->c:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    .line 51
    :goto_0
    return-object v0

    .line 47
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f$a;

    invoke-virtual {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->c:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    goto :goto_0

    .line 51
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/d$b;->d:Lkotlin/reflect/jvm/internal/impl/h/d$b;

    goto :goto_0
.end method
