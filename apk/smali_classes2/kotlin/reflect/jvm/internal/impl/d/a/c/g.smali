.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/g;
.super Ljava/lang/Object;
.source "resolvers.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/c/j;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/w;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/reflect/jvm/internal/impl/j/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/d",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/w;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final d:Lkotlin/reflect/jvm/internal/impl/b/m;

.field private final e:I


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;I)V
    .locals 2

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingDeclaration"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameterOwner"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->d:Lkotlin/reflect/jvm/internal/impl/b/m;

    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->e:I

    .line 40
    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/d/a/f/x;->s()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->a:Ljava/util/Map;

    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/g$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->b:Lkotlin/reflect/jvm/internal/impl/j/d;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->a:Ljava/util/Map;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    return-object v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->e:I

    return v0
.end method

.method public static final synthetic d(Lkotlin/reflect/jvm/internal/impl/d/a/c/g;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->d:Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/d/a/f/w;)Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 1

    .prologue
    const-string v0, "javaTypeParameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->b:Lkotlin/reflect/jvm/internal/impl/j/d;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/j/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/n;

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/g;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/j;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/w;)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    goto :goto_0
.end method
