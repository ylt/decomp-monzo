.class final Lkotlin/reflect/jvm/internal/impl/d/a/g/n;
.super Ljava/lang/Object;
.source "signatureEnhancement.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/r;

.field private final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Collection;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const-string v0, "fromOverride"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fromOverridden"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->b:Ljava/util/Collection;

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->c:Z

    return-void
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/g/n;Lkotlin/reflect/jvm/internal/impl/d/a/g/q;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/h;
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    :goto_0
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;)Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;)Lkotlin/reflect/jvm/internal/impl/d/a/g/h;
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->b:Ljava/util/Collection;

    iget-boolean v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->c:Z

    invoke-static {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Collection;Z)Lkotlin/d/a/b;

    move-result-object v1

    .line 86
    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/q;

    .line 87
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n$a;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/n$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/q;Lkotlin/d/a/b;)V

    check-cast v0, Lkotlin/d/a/b;

    .line 86
    check-cast v0, Lkotlin/d/a/b;

    .line 92
    :goto_0
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 93
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Z)V

    move-object v0, v1

    .line 92
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    .line 94
    :goto_2
    return-object v0

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 92
    goto :goto_1

    .line 94
    :cond_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/n;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Z)V

    goto :goto_2
.end method
