.class public final Lkotlin/reflect/jvm/internal/impl/d/a/b;
.super Ljava/lang/Object;
.source "specialBuiltinMembers.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/b$b;,
        Lkotlin/reflect/jvm/internal/impl/d/a/b$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/b;

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/l;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/l;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/b$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/b$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/16 v8, 0xa

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/b;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b;

    .line 97
    new-array v0, v11, [Ljava/lang/String;

    const-string v1, "containsAll"

    aput-object v1, v0, v7

    const-string v1, "removeAll"

    aput-object v1, v0, v9

    const-string v1, "retainAll"

    aput-object v1, v0, v10

    invoke-static {v0}, Lkotlin/a/ah;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 99
    nop

    .line 353
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 354
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 355
    check-cast v0, Ljava/lang/String;

    .line 99
    const-string v3, "java/util/Collection"

    const-string v4, "Ljava/util/Collection;"

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "JvmPrimitiveType.BOOLEAN.desc"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v0, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 356
    :cond_0
    check-cast v1, Ljava/util/List;

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b;->b:Ljava/util/List;

    .line 101
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->b:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 357
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 358
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 359
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/l;

    .line 101
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/l;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 360
    :cond_1
    check-cast v1, Ljava/util/List;

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b;->c:Ljava/util/List;

    .line 114
    nop

    .line 361
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 115
    new-array v1, v8, [Lkotlin/h;

    const-string v2, "Collection"

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "contains"

    const-string v4, "Ljava/lang/Object;"

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "JvmPrimitiveType.BOOLEAN.desc"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v3, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "Collection"

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "remove"

    const-string v4, "Ljava/lang/Object;"

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "JvmPrimitiveType.BOOLEAN.desc"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v3, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v1, v9

    const-string v2, "Map"

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "containsKey"

    const-string v4, "Ljava/lang/Object;"

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "JvmPrimitiveType.BOOLEAN.desc"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v3, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v1, v10

    const-string v2, "Map"

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "containsValue"

    const-string v4, "Ljava/lang/Object;"

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "JvmPrimitiveType.BOOLEAN.desc"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v3, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v2, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v2

    aput-object v2, v1, v11

    const/4 v2, 0x4

    const-string v3, "Map"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "remove"

    const-string v5, "Ljava/lang/Object;Ljava/lang/Object;"

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "JvmPrimitiveType.BOOLEAN.desc"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "Map"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "getOrDefault"

    const-string v5, "Ljava/lang/Object;Ljava/lang/Object;"

    const-string v6, "Ljava/lang/Object;"

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->d:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "Map"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "get"

    const-string v5, "Ljava/lang/Object;"

    const-string v6, "Ljava/lang/Object;"

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "Map"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "remove"

    const-string v5, "Ljava/lang/Object;"

    const-string v6, "Ljava/lang/Object;"

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "List"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "indexOf"

    const-string v5, "Ljava/lang/Object;"

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "JvmPrimitiveType.INT.desc"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "List"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "lastIndexOf"

    const-string v4, "Ljava/lang/Object;"

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "JvmPrimitiveType.INT.desc"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v3, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v0, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    .line 361
    check-cast v0, Ljava/util/Map;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->d:Ljava/util/Map;

    .line 145
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b;->d:Ljava/util/Map;

    .line 362
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lkotlin/a/ab;->a(I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 363
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 364
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 365
    check-cast v1, Ljava/util/Map$Entry;

    .line 145
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/l;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/l;->b()Ljava/lang/String;

    move-result-object v1

    check-cast v2, Ljava/util/Map$Entry;

    .line 363
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 367
    :cond_2
    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->e:Ljava/util/Map;

    .line 150
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->b:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    move-object v0, v1

    .line 151
    check-cast v0, Ljava/lang/Iterable;

    .line 368
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 369
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 370
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/l;

    .line 151
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/l;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 371
    :cond_3
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 151
    invoke-static {v2}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->f:Ljava/util/Set;

    .line 152
    check-cast v1, Ljava/lang/Iterable;

    .line 372
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v1, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 373
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 374
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/l;

    .line 152
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/l;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 375
    :cond_4
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 152
    invoke-static {v0}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->g:Ljava/util/Set;

    return-void
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-string v0, "functionDescriptor"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 163
    :goto_0
    return-object v0

    :cond_0
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/b;

    const/4 v2, 0x0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b$c;

    check-cast v0, Lkotlin/d/a/b;

    const/4 v3, 0x1

    invoke-static {p0, v2, v0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/d/a/b$a;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->f:Ljava/util/Set;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 199
    :goto_0
    return-object v0

    .line 192
    :cond_0
    const/4 v2, 0x0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b$d;

    check-cast v0, Lkotlin/d/a/b;

    const/4 v3, 0x1

    invoke-static {p0, v2, v0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 195
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 193
    goto :goto_0

    .line 197
    :cond_2
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    .line 199
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 201
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    goto :goto_0

    .line 203
    :cond_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/b;Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    return v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->g:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
