.class public final Lkotlin/reflect/jvm/internal/impl/d/b/e;
.super Ljava/lang/Object;
.source "DeserializedDescriptorResolver.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/e$a;
    }
.end annotation


# static fields
.field public static final b:Lkotlin/reflect/jvm/internal/impl/d/b/e$a;

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lkotlin/reflect/jvm/internal/impl/d/b/m;

.field private static f:Z


# instance fields
.field public a:Lkotlin/reflect/jvm/internal/impl/i/b/m;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->b:Lkotlin/reflect/jvm/internal/impl/d/b/e$a;

    .line 105
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    invoke-static {v0}, Lkotlin/a/ah;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->c:Ljava/util/Set;

    .line 108
    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    const/4 v1, 0x0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->c:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/ah;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->d:Ljava/util/Set;

    .line 110
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/m;

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/m;-><init>([I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->e:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->c:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic c()Ljava/util/Set;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->d:Ljava/util/Set;

    return-object v0
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/i/b/u;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/i/b/u",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->d()Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/b/u;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->d()Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/m;->a:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/b/g;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v4

    invoke-direct {v2, v0, v1, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/u;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/g;Lkotlin/reflect/jvm/internal/impl/i/b/g;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/a;)V

    move-object v0, v2

    goto :goto_0
.end method

.method public static final synthetic d()Lkotlin/reflect/jvm/internal/impl/d/b/m;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->e:Lkotlin/reflect/jvm/internal/impl/d/b/m;

    return-object v0
.end method

.method private final d(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Z
    .locals 2

    .prologue
    .line 79
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/m;->c:Lkotlin/reflect/jvm/internal/impl/d/b/m$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/m$a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->b:Lkotlin/reflect/jvm/internal/impl/d/b/e$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->d()Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/e;->b:Lkotlin/reflect/jvm/internal/impl/d/b/e$a;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;->b(Lkotlin/reflect/jvm/internal/impl/d/b/e$a;)Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final synthetic e()Z
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->f:Z

    return v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const-string v0, "kotlinClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->b(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/i/b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 44
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    if-nez v1, :cond_0

    const-string v2, "components"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->a()Lkotlin/reflect/jvm/internal/impl/i/b/j;

    move-result-object v1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/j;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    :goto_0
    return-object v0

    .line 43
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const-string v1, "descriptor"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "kotlinClass"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/e;->b:Lkotlin/reflect/jvm/internal/impl/d/b/e$a;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/e$a;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u;Ljava/util/Set;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 59
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->g()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 137
    nop

    .line 138
    nop

    .line 139
    nop

    .line 61
    :try_start_0
    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->b([Ljava/lang/String;[Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/d;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 150
    :goto_0
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/d;

    if-eqz v1, :cond_1

    .line 62
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/d;->a()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    .line 60
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/d;->b()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v3

    .line 63
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/d/b/p;

    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->c(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/i/b/u;

    move-result-object v1

    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->d(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Z

    move-result v2

    invoke-direct {v5, p2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/p;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/u;Lkotlin/reflect/jvm/internal/impl/i/b/u;Z)V

    .line 64
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    if-nez v6, :cond_0

    const-string v2, "components"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    sget-object v7, Lkotlin/reflect/jvm/internal/impl/d/b/e$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/e$b;

    check-cast v7, Lkotlin/d/a/a;

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;-><init>(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/i/e$m;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/i/b/m;Lkotlin/d/a/a;)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-object v3, v1

    :cond_1
    return-object v3

    .line 141
    :catch_0
    move-exception v1

    .line 142
    :try_start_1
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not read data from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    check-cast v1, Ljava/lang/Throwable;

    invoke-direct {v2, v4, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    check-cast v0, Ljava/lang/Throwable;

    move-object v1, v0

    throw v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 145
    :catch_1
    move-exception v1

    .line 146
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->d()Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/m;->a()Z

    move-result v2

    if-nez v2, :cond_2

    move-object v1, v3

    .line 148
    goto :goto_0

    .line 150
    :cond_2
    throw v1
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/i/b/m;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    if-nez v0, :cond_0

    const-string v1, "components"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/b/d;)V
    .locals 1

    .prologue
    const-string v0, "components"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/d;->a()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    .line 40
    return-void
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/b/u;Ljava/util/Set;)[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
            "Ljava/util/Set",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const-string v0, "kotlinClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expectedKinds"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v2

    .line 83
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->e()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/e$c;

    invoke-direct {v0, p2, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/e$c;-><init>(Ljava/util/Set;Lkotlin/reflect/jvm/internal/impl/d/b/a/a;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->f()[Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/i/b;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-string v1, "kotlinClass"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/e;->b:Lkotlin/reflect/jvm/internal/impl/d/b/e$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;->a()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a(Lkotlin/reflect/jvm/internal/impl/d/b/u;Ljava/util/Set;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 49
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->g()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 123
    nop

    .line 124
    nop

    .line 125
    nop

    .line 51
    :try_start_0
    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a([Ljava/lang/String;[Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/a;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 136
    :goto_0
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/a;

    if-eqz v1, :cond_0

    .line 53
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/w;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->c(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/i/b/u;

    move-result-object v3

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->d(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Z

    move-result v4

    invoke-direct {v2, p1, v3, v4}, Lkotlin/reflect/jvm/internal/impl/d/b/w;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/u;Lkotlin/reflect/jvm/internal/impl/i/b/u;Z)V

    .line 54
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/i/b;

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v3, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b;-><init>(Lkotlin/reflect/jvm/internal/impl/i/a;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    :cond_0
    return-object v3

    .line 127
    :catch_0
    move-exception v1

    .line 128
    :try_start_1
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not read data from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    check-cast v1, Ljava/lang/Throwable;

    invoke-direct {v2, v4, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    check-cast v0, Ljava/lang/Throwable;

    move-object v1, v0

    throw v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 131
    :catch_1
    move-exception v1

    .line 132
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->c()Lkotlin/reflect/jvm/internal/impl/d/b/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/a/a;->d()Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/m;->a()Z

    move-result v2

    if-nez v2, :cond_1

    move-object v1, v3

    .line 134
    goto :goto_0

    .line 136
    :cond_1
    throw v1
.end method
