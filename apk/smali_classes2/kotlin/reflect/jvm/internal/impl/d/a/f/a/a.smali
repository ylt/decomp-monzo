.class final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;
.super Ljava/lang/Object;
.source "ReflectJavaMember.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;

.field private static b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/reflect/Member;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v0, "member"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 74
    nop

    .line 75
    :try_start_0
    const-string v1, "getParameters"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 81
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/b;->a(Ljava/lang/Class;)Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v2, "java.lang.reflect.Parameter"

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 83
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;

    const-string v3, "getName"

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;-><init>(Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V

    :goto_0
    return-object v0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;

    invoke-direct {v0, v3, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;-><init>(Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/reflect/Member;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Member;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const-string v0, "member"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;

    .line 88
    if-nez v0, :cond_0

    .line 89
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;->a(Ljava/lang/reflect/Member;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;

    move-result-object v0

    .line 90
    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;

    .line 93
    :cond_0
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;->a()Ljava/lang/reflect/Method;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 94
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/a$a;->b()Ljava/lang/reflect/Method;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 96
    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<*>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, [Ljava/lang/Object;

    .line 102
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    move v3, v4

    .line 103
    :goto_0
    array-length v2, v0

    if-ge v3, v2, :cond_3

    aget-object v2, v0, v3

    .line 97
    new-array v6, v4, [Ljava/lang/Object;

    invoke-virtual {v5, v2, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 105
    :cond_3
    check-cast v1, Ljava/util/List;

    :cond_4
    return-object v1
.end method
