.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f;
.super Ljava/lang/Object;
.source "notFoundClasses.kt"


# direct methods
.method public static final a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const-string v0, "fqName"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 29
    new-instance v9, Lkotlin/reflect/jvm/internal/impl/e/b;

    check-cast v0, Ljava/lang/Iterable;

    const-string v1, "."

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f$a;

    check-cast v6, Lkotlin/d/a/b;

    const/16 v7, 0x1e

    move-object v3, v2

    move-object v5, v2

    move-object v8, v2

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    .line 30
    invoke-static {v9}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "ClassId.topLevel(resultingClassFqName)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private static final b(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 35
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f$b;

    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p0

    .line 41
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->h(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/a/x;

    invoke-virtual {v0}, Lkotlin/a/x;->c()I

    move-result v6

    invoke-virtual {v0}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    move v0, v2

    move v2, v3

    :goto_1
    move v3, v2

    move v2, v0

    .line 41
    goto :goto_0

    .line 43
    :sswitch_0
    if-nez v3, :cond_1

    .line 44
    if-nez p0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v2, v6, 0x1

    :cond_1
    move v0, v2

    move v2, v3

    goto :goto_1

    .line 47
    :sswitch_1
    add-int/lit8 v3, v3, 0x1

    move v0, v2

    move v2, v3

    goto :goto_1

    .line 48
    :sswitch_2
    add-int/lit8 v3, v3, -0x1

    move v0, v2

    move v2, v3

    goto :goto_1

    .line 51
    :cond_2
    if-nez p0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast p0, Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/f$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 52
    check-cast v0, Ljava/util/List;

    return-object v0

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x2e -> :sswitch_0
        0x3c -> :sswitch_1
        0x3e -> :sswitch_2
    .end sparse-switch
.end method
