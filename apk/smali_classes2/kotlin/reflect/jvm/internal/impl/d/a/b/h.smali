.class public final Lkotlin/reflect/jvm/internal/impl/d/a/b/h;
.super Ljava/lang/Object;
.source "util.kt"


# direct methods
.method public static final a(Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v1, "newValueParametersTypes"

    invoke-static {p0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "oldValueParameters"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newOwner"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Different value parameters sizes: Enhanced = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Old = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 36
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 40
    :cond_1
    check-cast p0, Ljava/lang/Iterable;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static/range {p0 .. p1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 76
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    move-object v13, v2

    check-cast v13, Ljava/util/Collection;

    .line 77
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 78
    check-cast v1, Lkotlin/h;

    .line 42
    invoke-virtual {v1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 43
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->c()I

    move-result v4

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v6

    const-string v2, "oldParameter.name"

    invoke-static {v6, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->l()Z

    move-result v8

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->o()Z

    move-result v9

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->q()Z

    move-result v10

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->m()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object/from16 v2, p2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->c(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    invoke-virtual {v2, v7}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v11

    :goto_2
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v12

    const-string v2, "oldParameter.source"

    invoke-static {v12, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v12}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 55
    invoke-interface {v13, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 43
    :cond_2
    const/4 v11, 0x0

    goto :goto_2

    .line 79
    :cond_3
    check-cast v13, Ljava/util/List;

    return-object v13
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;
    .locals 3

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 62
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->d()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    .line 64
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/b/h;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 66
    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/l;

    goto :goto_0
.end method
