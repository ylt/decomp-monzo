.class public final enum Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;
.super Ljava/lang/Enum;
.source "JavaTypeResolver.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

.field private static final synthetic d:[Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    const-string v2, "INFLEXIBLE"

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    aput-object v1, v0, v3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    const-string v2, "FLEXIBLE_UPPER_BOUND"

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    const-string v2, "FLEXIBLE_LOWER_BOUND"

    invoke-direct {v1, v2, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->d:[Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 327
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->d:[Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/c/b/b;

    return-object v0
.end method
