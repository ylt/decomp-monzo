.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t$a;
.super Ljava/lang/Object;
.source "ReflectJavaModifierListOwner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z
    .locals 1

    .prologue
    .line 29
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isAbstract(I)Z

    move-result v0

    return v0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z
    .locals 1

    .prologue
    .line 32
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    return v0
.end method

.method public static c(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Z
    .locals 1

    .prologue
    .line 35
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;->v()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v0

    return v0
.end method

.method public static d(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;)Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 2

    .prologue
    .line 38
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/t;->v()I

    move-result v0

    .line 40
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    const-string v1, "Visibilities.PUBLIC"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 47
    return-object v0

    .line 41
    :cond_0
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    const-string v1, "Visibilities.PRIVATE"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_1
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isProtected(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 43
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/ay;

    :goto_1
    const-string v1, "if (Modifier.isStatic(mo\u2026ies.PROTECTED_AND_PACKAGE"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_1

    .line 45
    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    const-string v1, "JavaVisibilities.PACKAGE_VISIBILITY"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
