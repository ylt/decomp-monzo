.class public final Lkotlin/reflect/jvm/internal/impl/d/b/b/g$a;
.super Ljava/lang/Object;
.source "RuntimeModuleData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/b/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/g$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/ClassLoader;)Lkotlin/reflect/jvm/internal/impl/d/b/b/g;
    .locals 27

    .prologue
    const-string v2, "classLoader"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v21, Lkotlin/reflect/jvm/internal/impl/j/b;

    invoke-direct/range {v21 .. v21}, Lkotlin/reflect/jvm/internal/impl/j/b;-><init>()V

    .line 47
    new-instance v22, Lkotlin/reflect/jvm/internal/impl/f/b;

    move-object/from16 v2, v21

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/j/i;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-direct {v0, v2, v3, v4, v5}, Lkotlin/reflect/jvm/internal/impl/f/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;ZILkotlin/d/b/i;)V

    .line 48
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<runtime module for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    const-string v4, "Name.special(\"<runtime module for $classLoader>\")"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v4, v21

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/j/i;

    move-object/from16 v5, v22

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/a/l;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x38

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lkotlin/reflect/jvm/internal/impl/b/b/u;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/h/f;Lkotlin/reflect/jvm/internal/impl/b/an;Ljava/util/Map;ILkotlin/d/b/i;)V

    .line 50
    new-instance v23, Lkotlin/reflect/jvm/internal/impl/d/b/b/d;

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/b/d;-><init>(Ljava/lang/ClassLoader;)V

    .line 51
    new-instance v7, Lkotlin/reflect/jvm/internal/impl/d/b/e;

    invoke-direct {v7}, Lkotlin/reflect/jvm/internal/impl/d/b/e;-><init>()V

    .line 52
    new-instance v24, Lkotlin/reflect/jvm/internal/impl/d/a/c/i;

    invoke-direct/range {v24 .. v24}, Lkotlin/reflect/jvm/internal/impl/d/a/c/i;-><init>()V

    .line 53
    new-instance v25, Lkotlin/reflect/jvm/internal/impl/d/b/b/h;

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/b/h;-><init>(Ljava/lang/ClassLoader;)V

    .line 54
    sget-object v11, Lkotlin/reflect/jvm/internal/impl/d/a/a/h;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    .line 55
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-object/from16 v4, v21

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/j/i;

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/d/a/d/a;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/d/a;-><init>(Ljava/lang/ClassLoader;)V

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/d/a/e;

    move-object/from16 v6, v23

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/d/b/t;

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/d/a/a/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/b;

    const-string v9, "ExternalAnnotationResolver.EMPTY"

    invoke-static {v8, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/d/a/a/n;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/n;

    const-string v10, "SignaturePropagator.DO_NOTHING"

    invoke-static {v9, v10}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/d/a/a/k;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/k;

    check-cast v10, Lkotlin/reflect/jvm/internal/impl/i/b/r;

    const-string v12, "javaResolverCache"

    invoke-static {v11, v12}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v12, Lkotlin/reflect/jvm/internal/impl/d/a/a/g$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/g$a;

    check-cast v12, Lkotlin/reflect/jvm/internal/impl/d/a/a/g;

    sget-object v13, Lkotlin/reflect/jvm/internal/impl/d/a/a/m;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/m$a;

    check-cast v13, Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

    sget-object v14, Lkotlin/reflect/jvm/internal/impl/d/a/a/l;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/l;

    check-cast v14, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-object/from16 v15, v24

    check-cast v15, Lkotlin/reflect/jvm/internal/impl/d/a/c/h;

    move-object/from16 v16, v25

    check-cast v16, Lkotlin/reflect/jvm/internal/impl/b/ab;

    sget-object v17, Lkotlin/reflect/jvm/internal/impl/b/ao$a;->a:Lkotlin/reflect/jvm/internal/impl/b/ao$a;

    check-cast v17, Lkotlin/reflect/jvm/internal/impl/b/ao;

    sget-object v18, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a:Lkotlin/reflect/jvm/internal/impl/c/a/c$a;

    invoke-virtual/range {v18 .. v18}, Lkotlin/reflect/jvm/internal/impl/c/a/c$a;->a()Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-result-object v18

    move-object/from16 v19, v2

    check-cast v19, Lkotlin/reflect/jvm/internal/impl/b/w;

    new-instance v20, Lkotlin/reflect/jvm/internal/impl/a/n;

    move-object/from16 v26, v2

    check-cast v26, Lkotlin/reflect/jvm/internal/impl/b/w;

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/n;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;)V

    invoke-direct/range {v3 .. v20}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/d/a/e;Lkotlin/reflect/jvm/internal/impl/d/b/t;Lkotlin/reflect/jvm/internal/impl/d/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/a/b;Lkotlin/reflect/jvm/internal/impl/d/a/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/r;Lkotlin/reflect/jvm/internal/impl/d/a/a/h;Lkotlin/reflect/jvm/internal/impl/d/a/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/a/m;Lkotlin/reflect/jvm/internal/impl/d/a/e/b;Lkotlin/reflect/jvm/internal/impl/d/a/c/h;Lkotlin/reflect/jvm/internal/impl/b/ab;Lkotlin/reflect/jvm/internal/impl/b/ao;Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/a/n;)V

    .line 62
    new-instance v14, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;

    invoke-direct {v14, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b;)V

    move-object v3, v2

    .line 64
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/w;

    const/4 v4, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lkotlin/reflect/jvm/internal/impl/f/b;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Z)V

    .line 66
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/h/d/a;

    const-string v3, "javaResolverCache"

    invoke-static {v11, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v14, v11}, Lkotlin/reflect/jvm/internal/impl/h/d/a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/e;Lkotlin/reflect/jvm/internal/impl/d/a/a/h;)V

    .line 67
    new-instance v12, Lkotlin/reflect/jvm/internal/impl/d/b/g;

    move-object/from16 v3, v23

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/d/b/t;

    invoke-direct {v12, v3, v7}, Lkotlin/reflect/jvm/internal/impl/d/b/g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/t;Lkotlin/reflect/jvm/internal/impl/d/b/e;)V

    .line 68
    new-instance v15, Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    move-object/from16 v3, v21

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/j/i;

    move-object v4, v2

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/w;

    invoke-direct {v15, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;)V

    .line 69
    new-instance v13, Lkotlin/reflect/jvm/internal/impl/d/b/c;

    move-object v3, v2

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/w;

    move-object/from16 v4, v21

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/j/i;

    check-cast v23, Lkotlin/reflect/jvm/internal/impl/d/b/t;

    move-object/from16 v0, v23

    invoke-direct {v13, v3, v15, v4, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/d/b/t;)V

    .line 72
    new-instance v8, Lkotlin/reflect/jvm/internal/impl/d/b/d;

    move-object/from16 v9, v21

    check-cast v9, Lkotlin/reflect/jvm/internal/impl/j/i;

    move-object v10, v2

    check-cast v10, Lkotlin/reflect/jvm/internal/impl/b/w;

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/i/b/n$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/n$a;

    check-cast v11, Lkotlin/reflect/jvm/internal/impl/i/b/n;

    sget-object v16, Lkotlin/reflect/jvm/internal/impl/d/a/a/k;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/k;

    check-cast v16, Lkotlin/reflect/jvm/internal/impl/i/b/r;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/c/a/c;->a:Lkotlin/reflect/jvm/internal/impl/c/a/c$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/c/a/c$a;->a()Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-result-object v17

    invoke-direct/range {v8 .. v17}, Lkotlin/reflect/jvm/internal/impl/d/b/d;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/n;Lkotlin/reflect/jvm/internal/impl/d/b/g;Lkotlin/reflect/jvm/internal/impl/d/b/c;Lkotlin/reflect/jvm/internal/impl/d/a/c/e;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/i/b/r;Lkotlin/reflect/jvm/internal/impl/c/a/c;)V

    .line 78
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/i;->a(Lkotlin/reflect/jvm/internal/impl/h/d/a;)V

    .line 79
    invoke-virtual {v7, v8}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a(Lkotlin/reflect/jvm/internal/impl/d/b/d;)V

    .line 81
    const/4 v3, 0x2

    new-array v3, v3, [Lkotlin/reflect/jvm/internal/impl/b/b/u;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v4, 0x1

    invoke-virtual/range {v22 .. v22}, Lkotlin/reflect/jvm/internal/impl/f/b;->h()Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v6

    const-string v7, "builtIns.builtInsModule"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    aput-object v6, v3, v4

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a([Lkotlin/reflect/jvm/internal/impl/b/b/u;)V

    .line 82
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/h/d/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/e;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/z;

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a(Lkotlin/reflect/jvm/internal/impl/b/z;)V

    .line 84
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/b/g;

    invoke-virtual {v8}, Lkotlin/reflect/jvm/internal/impl/d/b/d;->a()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-direct {v2, v3, v0, v4}, Lkotlin/reflect/jvm/internal/impl/d/b/b/g;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/m;Lkotlin/reflect/jvm/internal/impl/d/b/b/h;Lkotlin/d/b/i;)V

    return-object v2
.end method
