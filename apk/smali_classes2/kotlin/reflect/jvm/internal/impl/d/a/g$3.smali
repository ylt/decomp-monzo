.class final Lkotlin/reflect/jvm/internal/impl/d/a/g$3;
.super Lkotlin/reflect/jvm/internal/impl/b/ay;
.source "JavaVisibilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/ay;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "visibility"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities$3"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "compareTo"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    if-ne p0, p1, :cond_1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    .line 107
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    .line 108
    :cond_2
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 109
    :cond_3
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 115
    const-string v0, "protected/*protected and package*/"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities$3"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getDisplayName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "what"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities$3"

    aput-object v3, v2, v5

    const-string v3, "isVisible"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "from"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities$3"

    aput-object v3, v2, v5

    const-string v3, "isVisible"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_1
    invoke-static {p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/g;->a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    return v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 5

    .prologue
    .line 121
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/load/java/JavaVisibilities$3"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "normalize"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
