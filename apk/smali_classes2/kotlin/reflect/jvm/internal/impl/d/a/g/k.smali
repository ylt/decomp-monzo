.class Lkotlin/reflect/jvm/internal/impl/d/a/g/k;
.super Ljava/lang/Object;
.source "typeEnhancement.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/r;

.field private final b:I

.field private final c:Z


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/r;IZ)V
    .locals 1

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    iput p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->b:I

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->c:Z

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->b()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/k$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/k;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->b:I

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->c:Z

    return v0
.end method
