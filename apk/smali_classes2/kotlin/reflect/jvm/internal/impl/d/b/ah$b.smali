.class final Lkotlin/reflect/jvm/internal/impl/d/b/ah$b;
.super Lkotlin/d/b/m;
.source "UnsafeVarianceTypeSubstitution.kt"

# interfaces
.implements Lkotlin/d/a/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/ah;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/q",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/aq;",
        "Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;",
        "Lkotlin/reflect/jvm/internal/impl/k/ap;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$b;->a:Ljava/util/List;

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    check-cast p3, Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$b;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;Lkotlin/reflect/jvm/internal/impl/k/ap;)V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;Lkotlin/reflect/jvm/internal/impl/k/ap;)V
    .locals 2

    .prologue
    const-string v0, "typeParameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "indexedTypeHolder"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorPosition"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$b;->a:Ljava/util/List;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method
