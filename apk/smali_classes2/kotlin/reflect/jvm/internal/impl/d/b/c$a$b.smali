.class public final Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;
.super Ljava/lang/Object;
.source "BinaryClassAnnotationAndConstantLoaderImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/e/f;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 118
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v2

    .line 119
    if-eqz v2, :cond_0

    .line 120
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 121
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;)Ljava/util/HashMap;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c;)Lkotlin/reflect/jvm/internal/impl/h/b/g;

    move-result-object v3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->c:Ljava/util/ArrayList;

    check-cast v1, Ljava/util/List;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v4

    const-string v5, "parameter.type"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1, v4}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/h/b/b;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {v1, v2, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 2

    .prologue
    const-string v0, "enumClassId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enumEntryName"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c$a;

    invoke-static {v1, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    return-void
.end method
