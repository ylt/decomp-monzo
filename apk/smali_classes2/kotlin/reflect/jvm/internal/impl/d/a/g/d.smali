.class public final Lkotlin/reflect/jvm/internal/impl/d/a/g/d;
.super Ljava/lang/Object;
.source "typeQualifiers.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/g/d$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/g/d$a;

.field private static final e:Lkotlin/reflect/jvm/internal/impl/d/a/g/d;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d$a;

    invoke-direct {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/d$a;

    .line 48
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/g;Lkotlin/reflect/jvm/internal/impl/d/a/g/e;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->e:Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/g/g;Lkotlin/reflect/jvm/internal/impl/d/a/g/e;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->c:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->d:Z

    return-void
.end method

.method public static final synthetic d()Lkotlin/reflect/jvm/internal/impl/d/a/g/d;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->e:Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    return-object v0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/d/a/g/g;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/d/a/g/e;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->c:Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->d:Z

    return v0
.end method
