.class public final Lkotlin/reflect/jvm/internal/impl/d/b/g;
.super Ljava/lang/Object;
.source "JavaClassDataFinder.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/h;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/d/b/t;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/b/e;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/t;Lkotlin/reflect/jvm/internal/impl/d/b/e;)V
    .locals 1

    .prologue
    const-string v0, "kotlinClassFinder"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deserializedDescriptorResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/g;->a:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/g;->b:Lkotlin/reflect/jvm/internal/impl/d/b/e;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/i/b;
    .locals 3

    .prologue
    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/g;->a:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 29
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Class with incorrect id found: expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", actual "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/u;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 29
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 28
    :cond_0
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/g;->b:Lkotlin/reflect/jvm/internal/impl/d/b/e;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->b(Lkotlin/reflect/jvm/internal/impl/d/b/u;)Lkotlin/reflect/jvm/internal/impl/i/b;

    move-result-object v0

    goto :goto_0
.end method
