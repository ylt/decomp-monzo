.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;
.source "ReflectJavaMethod.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/q;


# instance fields
.field private final a:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Method;)V
    .locals 1

    .prologue
    const-string v0, "member"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->a:Ljava/lang/reflect/Method;

    return-void
.end method


# virtual methods
.method public synthetic L_()Ljava/lang/reflect/Member;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Member;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getGenericParameterTypes()[Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "member.genericParameterTypes"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v1

    const-string v2, "member.parameterAnnotations"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->isVarArgs()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->a([Ljava/lang/reflect/Type;[[Ljava/lang/annotation/Annotation;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->g()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getDefaultValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v1

    const-string v2, "member.genericReturnType"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;->a(Ljava/lang/reflect/Type;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->a:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public s()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/s;->h()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 37
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 38
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v2, v0

    if-ge v3, v2, :cond_0

    aget-object v2, v0, v3

    .line 39
    check-cast v2, Ljava/lang/reflect/TypeVariable;

    .line 34
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/x;

    const-string v5, "it"

    invoke-static {v2, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/x;-><init>(Ljava/lang/reflect/TypeVariable;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 38
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 40
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 34
    return-object v1
.end method
