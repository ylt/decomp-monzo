.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;
.super Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;
.source "LazyJavaClassMemberScope.kt"


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/n;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/reflect/jvm/internal/impl/j/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/d",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/b/b/g;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lkotlin/reflect/jvm/internal/impl/b/e;

.field private final h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)V
    .locals 2

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownerDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jClass"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g:Lkotlin/reflect/jvm/internal/impl/b/e;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    .line 80
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 617
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$k;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$k;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 621
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$g;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->e:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 625
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->f:Lkotlin/reflect/jvm/internal/impl/j/d;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->i()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 705
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 706
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 707
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    .line 141
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;)Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 708
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 141
    return-object v1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/b/f;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/b/f;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 562
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->k()Ljava/util/Collection;

    move-result-object v6

    .line 563
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 565
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const/4 v4, 0x4

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v13

    move-object v0, v6

    .line 567
    check-cast v0, Ljava/lang/Iterable;

    .line 568
    nop

    .line 772
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 773
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 774
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    .line 775
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    .line 568
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/d/a/i;->c:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {v0, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 776
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 778
    :cond_0
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 781
    :cond_1
    new-instance v4, Lkotlin/h;

    invoke-direct {v4, v5, v6}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    .line 567
    check-cast v0, Ljava/util/List;

    invoke-virtual {v4}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Ljava/util/List;

    .line 570
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-gt v4, v2, :cond_2

    move v4, v2

    :goto_1
    sget-boolean v5, Lkotlin/p;->a:Z

    if-eqz v5, :cond_3

    if-nez v4, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "There can\'t be more than one method named \'value\' in annotation class: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    move v4, v1

    goto :goto_1

    .line 571
    :cond_3
    invoke-static {v0}, Lkotlin/a/m;->f(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    .line 572
    if-eqz v8, :cond_4

    .line 573
    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v4

    .line 575
    instance-of v0, v4, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;

    if-eqz v0, :cond_5

    .line 576
    new-instance v5, Lkotlin/h;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v6

    move-object v0, v4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;

    invoke-virtual {v6, v0, v13, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;Z)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v6

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v6

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/f/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v4

    invoke-virtual {v6, v4, v13}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v4

    invoke-direct {v5, v0, v4}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v5

    .line 575
    :goto_2
    invoke-virtual {v0}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v9

    .line 574
    check-cast v9, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v5, v12

    .line 581
    check-cast v5, Ljava/util/List;

    move-object v6, p1

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/l;

    move-object v4, p0

    move v7, v1

    invoke-direct/range {v4 .. v10}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/l;ILkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 584
    :cond_4
    if-eqz v8, :cond_6

    .line 585
    :goto_3
    check-cast v11, Ljava/lang/Iterable;

    invoke-static {v11}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/a/x;

    invoke-virtual {v0}, Lkotlin/a/x;->c()I

    move-result v4

    invoke-virtual {v0}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    .line 586
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v0

    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v5

    invoke-virtual {v0, v5, v13}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v9

    move-object v5, v12

    .line 587
    check-cast v5, Ljava/util/List;

    move-object v6, p1

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/l;

    add-int v7, v4, v2

    move-object v4, p0

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/l;ILkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_4

    .line 579
    :cond_5
    new-instance v0, Lkotlin/h;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v5

    invoke-virtual {v5, v4, v13}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v4

    invoke-direct {v0, v4, v3}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    move v2, v1

    .line 584
    goto :goto_3

    .line 590
    :cond_7
    check-cast v12, Ljava/util/List;

    return-object v12
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/String;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            "Ljava/lang/String;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 208
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "Name.identifier(getterName)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 719
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 720
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 210
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_1

    move-object v0, v2

    .line 721
    :goto_0
    if-eqz v0, :cond_0

    .line 723
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    return-object v0

    .line 212
    :cond_1
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$h;

    invoke-direct {v1, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$h;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/ag;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 723
    goto :goto_1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 192
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->a()Lkotlin/reflect/jvm/internal/impl/b/ah;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ah;

    move-object v1, v0

    .line 193
    :goto_0
    if-eqz v1, :cond_2

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c;

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Ljava/lang/String;

    move-result-object v0

    .line 195
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    const-string v3, "overriddenBuiltinProperty!!"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 198
    invoke-direct {p0, p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/String;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    .line 201
    :goto_2
    return-object v0

    :cond_1
    move-object v1, v2

    .line 192
    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 193
    goto :goto_1

    .line 201
    :cond_3
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "JvmAbi.getterName(name.asString())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/String;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    goto :goto_2
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            "Lkotlin/reflect/jvm/internal/impl/b/a;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 349
    check-cast p3, Ljava/lang/Iterable;

    .line 739
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 349
    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v4

    if-nez v4, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    move v0, v1

    .line 740
    :goto_1
    if-eqz v0, :cond_3

    .line 352
    :goto_2
    return-object p1

    :cond_1
    move v0, v1

    .line 349
    goto :goto_0

    :cond_2
    move v0, v2

    .line 740
    goto :goto_1

    .line 352
    :cond_3
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->d()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->f()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_4
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-object p1, v0

    goto :goto_2
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 2

    .prologue
    .line 164
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 165
    invoke-interface {v0, p2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 166
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 167
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->b()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 168
    nop

    .line 164
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 168
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->f()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 358
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "overridden.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 741
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 359
    invoke-direct {p0, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/s;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 742
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-eqz v0, :cond_3

    .line 360
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 362
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v2

    move-object v1, v2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 363
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 743
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 744
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 745
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 363
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 742
    goto :goto_0

    .line 746
    :cond_2
    check-cast v4, Ljava/util/List;

    .line 363
    check-cast v4, Ljava/util/Collection;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->i()Ljava/util/List;

    move-result-object v0

    const-string v3, "override.valueParameters"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {v4, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/b/h;->a(Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 364
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 365
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->b()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 366
    nop

    .line 362
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 366
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->f()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 360
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    :goto_2
    return-object v0

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 2

    .prologue
    .line 554
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    .line 555
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    const-string v1, "JavaVisibilities.PROTECTED_AND_PACKAGE"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 558
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "visibility"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/f/k;)Lkotlin/reflect/jvm/internal/impl/d/a/b/b;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/k;)Lkotlin/reflect/jvm/internal/impl/d/a/b/b;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/k;)Lkotlin/reflect/jvm/internal/impl/d/a/b/b;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 509
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v4

    .line 511
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v2

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-static {v4, v1, v9, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->b(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/a/b/b;

    move-result-object v1

    .line 516
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v3

    const-string v0, "constructorDescriptor"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    move-object v2, p1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/x;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/e;->y()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v3, v0, v2, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;I)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v5

    .line 517
    const-string v0, "constructorDescriptor"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/k;->c()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v5, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;

    move-result-object v6

    .line 519
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/e;->y()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/k;->s()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 768
    new-instance v3, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v2, v7}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 769
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 770
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/w;

    .line 520
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->e()Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    move-result-object v8

    invoke-interface {v8, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/j;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/w;)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 771
    :cond_1
    check-cast v3, Ljava/util/List;

    .line 519
    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v0, v3}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 522
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/k;->q()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/ay;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/b/f;

    .line 523
    invoke-virtual {v1, v9}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->j(Z)V

    .line 524
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$b;->b()Z

    move-result v0

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->k(Z)V

    .line 526
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 528
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->g()Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    move-result-object v2

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/l;

    invoke-interface {v2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;Lkotlin/reflect/jvm/internal/impl/b/l;)V

    .line 530
    const-string v0, "constructorDescriptor"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_0

    .line 416
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 418
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    .line 420
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->q()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v3

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v6

    move-object v2, p1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v6, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v6

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object v2, p3

    move v7, v4

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Z)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    move-result-object v2

    move-object v0, v2

    .line 426
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/reflect/jvm/internal/impl/h/b;->b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-result-object v0

    .line 427
    invoke-virtual {v2, v0, v10}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/b/b/z;Lkotlin/reflect/jvm/internal/impl/b/ai;)V

    .line 429
    if-eqz p2, :cond_0

    .line 430
    :goto_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->f()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v3

    check-cast v10, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v2, p2, v1, v3, v10}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 431
    invoke-virtual {v0, p2}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 433
    const-string v0, "propertyDescriptor"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v2

    .line 429
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v5

    const-string v3, "propertyDescriptor"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v2

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/m;

    move-object v7, p1

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/d/a/f/x;

    const/4 v9, 0x4

    move v8, v4

    invoke-static/range {v5 .. v10}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/x;IILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v3

    invoke-virtual {p0, p1, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object p2

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)Lkotlin/reflect/jvm/internal/impl/d/a/f/g;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    return-object v0
.end method

.method private final a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 283
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f()Lkotlin/reflect/jvm/internal/impl/i/b/r;

    move-result-object v1

    invoke-static {p2, p3, p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;)Ljava/util/Collection;

    move-result-object v1

    .line 287
    if-nez p4, :cond_0

    .line 288
    const-string v0, "additionalOverrides"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 301
    :goto_0
    return-void

    .line 291
    :cond_0
    const-string v0, "additionalOverrides"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {p1, v0}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v4

    .line 292
    check-cast v1, Ljava/lang/Iterable;

    .line 735
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 736
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 737
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-object v2, v1

    .line 295
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->c(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-eqz v2, :cond_1

    .line 298
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a;

    move-object v3, v4

    check-cast v3, Ljava/util/Collection;

    invoke-direct {p0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v1

    :cond_1
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 738
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 292
    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private final a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/l;ILkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/l;",
            "I",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/q;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")V"
        }
    .end annotation

    .prologue
    .line 600
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/a;

    const/4 v3, 0x0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    invoke-interface/range {p4 .. p4}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v6

    invoke-static/range {p5 .. p5}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v7

    const-string v1, "TypeUtils.makeNotNullable(returnType)"

    invoke-static {v7, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface/range {p4 .. p4}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->f()Z

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    if-eqz p6, :cond_0

    check-cast p6, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 612
    invoke-static/range {p6 .. p6}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 600
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v11, v1

    move/from16 v4, p3

    move-object v13, v2

    move-object v1, v2

    move-object/from16 v2, p2

    :goto_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v12

    invoke-virtual {v12}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v12

    invoke-virtual {v12}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v12

    check-cast p4, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    move-object/from16 v0, p4

    invoke-interface {v12, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v12

    check-cast v12, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct/range {v1 .. v12}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    invoke-interface {p1, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 615
    return-void

    .line 600
    :cond_0
    const/4 v11, 0x0

    move/from16 v4, p3

    move-object v1, v2

    move-object v13, v2

    move-object/from16 v2, p2

    goto :goto_0
.end method

.method private final a(Ljava/util/Set;Ljava/util/Collection;Lkotlin/d/a/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 401
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 402
    invoke-direct {p0, v0, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    move-result-object v0

    .line 403
    if-eqz v0, :cond_0

    .line 404
    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_1
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/d/a/b;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 310
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 311
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-eqz v0, :cond_0

    move-object v1, v0

    .line 313
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->d(Lkotlin/reflect/jvm/internal/impl/b/b;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 314
    :cond_1
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "Name.identifier(nameInJava)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p5, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 315
    invoke-direct {p0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v2

    move-object v1, v2

    .line 317
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/s;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 318
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {p0, v2, v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 325
    :cond_3
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 327
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 330
    invoke-direct {p0, v1, p5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 332
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 333
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {p0, v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 335
    :cond_5
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    .line 330
    check-cast v0, Lkotlin/n;

    goto :goto_1

    .line 337
    :cond_6
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 182
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/j;->a:Lkotlin/reflect/jvm/internal/impl/h/j;

    invoke-virtual {v1, p2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;Z)Lkotlin/reflect/jvm/internal/impl/h/j$a;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/j$a;->b()Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    move-result-object v1

    .line 185
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/j$a$a;->a:Lkotlin/reflect/jvm/internal/impl/h/j$a$a;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f$a;

    invoke-virtual {v1, p2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f$a;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 100
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "function.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->c(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 687
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 102
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 688
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 113
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$j;

    invoke-direct {v1, p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/b/ak;)V

    check-cast v1, Lkotlin/d/a/b;

    .line 104
    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 113
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/h;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    move v0, v2

    .line 114
    :goto_1
    if-eqz v0, :cond_0

    move v0, v2

    .line 690
    :goto_2
    if-eqz v0, :cond_6

    .line 117
    :goto_3
    return v3

    :cond_3
    move v0, v3

    .line 113
    goto :goto_0

    :cond_4
    move v0, v3

    .line 689
    goto :goto_1

    :cond_5
    move v0, v3

    .line 690
    goto :goto_2

    .line 117
    :cond_6
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    :goto_4
    move v3, v0

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_4
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 1

    .prologue
    .line 175
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/a;->c(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/s;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    .line 177
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {p0, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v0

    return v0

    :cond_0
    move-object v0, p2

    .line 175
    goto :goto_0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 709
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 710
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-object v3, v0

    .line 145
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 146
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 711
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 147
    return-object v1
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 219
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/h;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "Name.identifier(JvmAbi.s\u2026terName(name.asString()))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 724
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 725
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 221
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x1

    if-eq v1, v4, :cond_1

    move-object v0, v2

    .line 726
    :goto_0
    if-eqz v0, :cond_0

    .line 728
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    return-object v0

    .line 223
    :cond_1
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->k(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v0, v2

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_0

    .line 224
    :cond_3
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$i;

    invoke-direct {v1, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$i;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/ag;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    goto :goto_0

    :cond_4
    move-object v0, v2

    .line 728
    goto :goto_1
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)Lkotlin/reflect/jvm/internal/impl/b/d;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->k()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v0

    return-object v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 411
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->i()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    if-eqz v1, :cond_0

    .line 412
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    const/4 v4, 0x2

    move-object v0, p0

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 413
    :cond_0
    return-void
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 128
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 704
    :goto_0
    return v2

    .line 130
    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 691
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 700
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 699
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 131
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 699
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 702
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 134
    check-cast v1, Ljava/lang/Iterable;

    .line 703
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 136
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/s;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v2, v0

    .line 704
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/s;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 505
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 503
    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Z)Ljava/lang/String;

    move-result-object v0

    .line 504
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/s;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 505
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/a;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 747
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 748
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 372
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->o:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v3, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 749
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 751
    :cond_0
    check-cast v1, Ljava/util/Set;

    return-object v1
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)Lkotlin/reflect/jvm/internal/impl/j/f;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-object v0
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 232
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/c;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v1

    .line 233
    :cond_1
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v2

    .line 234
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v3

    .line 236
    if-eqz v2, :cond_0

    .line 237
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v4

    if-nez v4, :cond_2

    move v1, v0

    goto :goto_0

    .line 239
    :cond_2
    if-eqz v3, :cond_3

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/ak;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v3

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/ak;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v2

    invoke-static {v3, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 150
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "name"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 712
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 154
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 713
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 714
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v1, v3

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 154
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 715
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 155
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v5

    .line 159
    :goto_1
    if-eqz v0, :cond_0

    move v0, v4

    .line 718
    :goto_2
    return v0

    .line 157
    :cond_3
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v1

    .line 159
    check-cast v2, Ljava/lang/Iterable;

    .line 716
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-object v0, v1

    .line 159
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-direct {p0, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/s;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    goto :goto_1

    :cond_5
    move v0, v5

    .line 717
    goto :goto_1

    :cond_6
    move v0, v5

    .line 718
    goto :goto_2
.end method

.method private final d(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 752
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 759
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 760
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 483
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->o:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 761
    new-instance v2, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 762
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 763
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 483
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 764
    :cond_0
    check-cast v2, Ljava/util/List;

    .line 483
    check-cast v2, Ljava/lang/Iterable;

    .line 765
    invoke-static {v1, v2}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 767
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 484
    invoke-static {v1}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private final d(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/b/e;"
        }
    .end annotation

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v7, 0x0

    .line 478
    :goto_0
    return-object v7

    .line 442
    :cond_0
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v9

    if-nez v9, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 444
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->y()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 445
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v8

    if-nez v8, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 449
    :cond_2
    :goto_1
    if-eqz v8, :cond_4

    move-object v0, v8

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    invoke-interface {v9}, Lkotlin/reflect/jvm/internal/impl/b/ak;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    :goto_2
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_6

    if-nez v0, :cond_6

    .line 450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Different accessors modalities when creating overrides for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "for getter is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v9}, Lkotlin/reflect/jvm/internal/impl/b/ak;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", but for setter is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v8, :cond_5

    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/b/ak;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 449
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 447
    :cond_3
    const/4 v8, 0x0

    goto :goto_1

    .line 449
    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    .line 451
    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    .line 454
    :cond_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-interface {v9}, Lkotlin/reflect/jvm/internal/impl/b/ak;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v2

    invoke-interface {v9}, Lkotlin/reflect/jvm/internal/impl/b/ak;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v3

    if-eqz v8, :cond_8

    const/4 v4, 0x1

    :goto_4
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    invoke-interface {v9}, Lkotlin/reflect/jvm/internal/impl/b/ak;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Z)Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    move-result-object v7

    .line 460
    invoke-interface {v9}, Lkotlin/reflect/jvm/internal/impl/b/ak;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    if-nez v1, :cond_7

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_7
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->f()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v3

    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v7, v1, v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v7

    .line 462
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v9}, Lkotlin/reflect/jvm/internal/impl/b/ak;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v9}, Lkotlin/reflect/jvm/internal/impl/b/ak;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/h/b;->b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZLkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-result-object v1

    move-object v0, v1

    .line 465
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-object v2, v9

    .line 466
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)V

    .line 467
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 468
    nop

    move-object v9, v1

    .line 462
    check-cast v9, Lkotlin/reflect/jvm/internal/impl/b/b/z;

    .line 470
    if-eqz v8, :cond_9

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-object v0, v7

    .line 471
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/b/ak;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/b/ak;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v5

    invoke-interface {v8}, Lkotlin/reflect/jvm/internal/impl/b/ak;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZZZLkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    move-result-object v1

    move-object v0, v1

    .line 473
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    .line 474
    check-cast v8, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-virtual {v0, v8}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)V

    .line 475
    nop

    .line 473
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    .line 470
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    :goto_5
    move-object v0, v7

    .line 478
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-virtual {v0, v9, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;->a(Lkotlin/reflect/jvm/internal/impl/b/b/z;Lkotlin/reflect/jvm/internal/impl/b/ai;)V

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/d/a/b/e;

    goto/16 :goto_0

    .line 454
    :cond_8
    const/4 v4, 0x0

    goto :goto_4

    .line 470
    :cond_9
    const/4 v1, 0x0

    goto :goto_5
.end method

.method public static final synthetic d(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)Lkotlin/reflect/jvm/internal/impl/j/f;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->e:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-object v0
.end method

.method private final d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)V
    .locals 2

    .prologue
    .line 676
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->n()Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    invoke-static {v0, p2, v1, p1}, Lkotlin/reflect/jvm/internal/impl/c/a;->a(Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 677
    return-void
.end method

.method private final k()Lkotlin/reflect/jvm/internal/impl/b/d;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 534
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->h()Z

    move-result v2

    .line 535
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez v2, :cond_0

    .line 536
    const/4 v1, 0x0

    .line 550
    :goto_0
    return-object v1

    .line 538
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v3

    .line 539
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v4

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v4, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-static {v3, v1, v5, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->b(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/d/a/b/b;

    move-result-object v1

    .line 542
    if-eqz v2, :cond_1

    const-string v0, "constructorDescriptor"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/f;

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/b/f;)Ljava/util/List;

    move-result-object v0

    .line 544
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->k(Z)V

    .line 546
    invoke-direct {p0, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/f;

    .line 547
    invoke-virtual {v1, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->j(Z)V

    .line 548
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 549
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->g()Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    move-result-object v3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    move-object v2, v1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/l;

    invoke-interface {v3, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;Lkotlin/reflect/jvm/internal/impl/b/l;)V

    .line 550
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/d;

    goto :goto_0

    .line 543
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 660
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)V

    .line 661
    invoke-super {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/q;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;"
        }
    .end annotation

    .prologue
    const-string v0, "method"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "methodTypeParameters"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "valueParameters"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 491
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->e()Lkotlin/reflect/jvm/internal/impl/d/a/a/n;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    const/4 v4, 0x0

    move-object v1, p1

    move-object v3, p3

    move-object v5, p4

    move-object v6, p2

    invoke-interface/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/d/a/a/n;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/a/a/n$a;

    move-result-object v6

    .line 494
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/a/n$a;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    const-string v2, "propagated.returnType"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/a/n$a;->b()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/a/n$a;->c()Ljava/util/List;

    move-result-object v3

    const-string v4, "propagated.valueParameters"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/a/n$a;->d()Ljava/util/List;

    move-result-object v4

    const-string v5, "propagated.typeParameters"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/a/n$a;->e()Z

    move-result v5

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/d/a/a/n$a;->f()Ljava/util/List;

    move-result-object v6

    const-string v7, "propagated.errors"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;ZLjava/util/List;)V

    return-object v0
.end method

.method protected a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Set;

    move-result-object v6

    .line 245
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a;

    invoke-virtual {v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b;

    invoke-virtual {v0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v6

    .line 247
    check-cast v0, Ljava/lang/Iterable;

    .line 729
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 730
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 249
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 731
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 247
    check-cast v1, Ljava/util/Collection;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Z)V

    .line 274
    :goto_1
    return-void

    .line 254
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a()Lkotlin/reflect/jvm/internal/impl/utils/f;

    move-result-object v7

    move-object v0, v6

    .line 257
    check-cast v0, Ljava/util/Collection;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/b/r;->b:Lkotlin/reflect/jvm/internal/impl/i/b/r;

    invoke-static {p2, v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;)Ljava/util/Collection;

    move-result-object v3

    .line 261
    const-string v0, "mergedFunctionFromSuperTypes"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$b;

    invoke-direct {v5, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)V

    check-cast v5, Lkotlin/d/a/b;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/d/a/b;)V

    .line 266
    const-string v0, "mergedFunctionFromSuperTypes"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v7

    check-cast v4, Ljava/util/Collection;

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$c;

    invoke-direct {v5, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)V

    check-cast v5, Lkotlin/d/a/b;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/d/a/b;)V

    .line 271
    check-cast v6, Ljava/lang/Iterable;

    .line 732
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 733
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 271
    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 734
    :cond_4
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    check-cast v7, Ljava/lang/Iterable;

    .line 271
    invoke-static {v0, v7}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 273
    check-cast v0, Ljava/util/Collection;

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Z)V

    goto :goto_1
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V

    .line 381
    :cond_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Set;

    move-result-object v3

    .line 382
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394
    :goto_0
    return-void

    .line 384
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a()Lkotlin/reflect/jvm/internal/impl/utils/f;

    move-result-object v1

    .line 386
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$d;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {p0, v3, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Ljava/util/Set;Ljava/util/Collection;Lkotlin/d/a/b;)V

    move-object v0, v1

    .line 388
    check-cast v0, Ljava/util/Collection;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$e;

    invoke-direct {v2, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)V

    check-cast v2, Lkotlin/d/a/b;

    invoke-direct {p0, v3, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Ljava/util/Set;Ljava/util/Collection;Lkotlin/d/a/b;)V

    .line 392
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v3, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f()Lkotlin/reflect/jvm/internal/impl/i/b/r;

    move-result-object v2

    invoke-static {p1, v0, p2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/i/b/r;)Ljava/util/Collection;

    move-result-object v0

    const-string v1, "resolveOverridesForNonSt\u2026components.errorReporter)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/d/a/b/d;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    :cond_0
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/ak;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 655
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)V

    .line 656
    invoke-super {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/HashSet",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 682
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 683
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 74
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->A_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 684
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 75
    check-cast v0, Ljava/util/HashSet;

    .line 76
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->i()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->a()Ljava/util/Set;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 77
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 78
    nop

    .line 75
    check-cast v1, Ljava/util/HashSet;

    .line 78
    return-object v1
.end method

.method public synthetic c(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/HashSet;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 650
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)V

    .line 651
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->f:Lkotlin/reflect/jvm/internal/impl/j/d;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/j/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method protected c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;
    .locals 3

    .prologue
    .line 70
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/d/a/b;)V

    return-object v1
.end method

.method protected d(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 665
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->e:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    return-object v0
.end method

.method protected e(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 668
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->A_()Ljava/util/Set;

    move-result-object v2

    .line 786
    :goto_0
    return-object v2

    .line 669
    :cond_0
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->i()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->b()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v2, v0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 670
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 782
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 783
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 671
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->B_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    move-object v1, v2

    .line 784
    check-cast v1, Ljava/util/Collection;

    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 786
    :cond_1
    check-cast v2, Ljava/util/Collection;

    check-cast v2, Ljava/util/Set;

    goto :goto_0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/j/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-object v0
.end method

.method protected f()Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 1

    .prologue
    .line 647
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g:Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 679
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Lazy Java member scope for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->h:Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
