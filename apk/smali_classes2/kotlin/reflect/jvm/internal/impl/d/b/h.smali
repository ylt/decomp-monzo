.class public final Lkotlin/reflect/jvm/internal/impl/d/b/h;
.super Ljava/lang/Object;
.source "JavaFlexibleTypeDeserializer.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/t;


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/b/h; = null

.field private static final b:Ljava/lang/String; = "kotlin.jvm.PlatformType"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/h;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/h;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/b/h;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/b/h;->a:Lkotlin/reflect/jvm/internal/impl/d/b/h;

    .line 29
    const-string v0, "kotlin.jvm.PlatformType"

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/h;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flexibleId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lowerBound"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "upperBound"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/h;->b:Ljava/lang/String;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error java flexible type with id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorTy\u2026owerBound..$upperBound)\")"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 36
    :goto_0
    return-object v0

    .line 33
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    invoke-direct {v0, p3, p4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_0

    .line 36
    :cond_1
    invoke-static {p3, p4}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_0
.end method
