.class public final Lkotlin/reflect/jvm/internal/impl/d/a/g/r;
.super Ljava/lang/Object;
.source "typeEnhancement.kt"


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/impl/d/a/g/b;

.field private static final b:Lkotlin/reflect/jvm/internal/impl/d/a/g/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 188
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/i;->i:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "JvmAnnotationNames.ENHANCED_NULLABILITY_ANNOTATION"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/b;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/b;

    .line 189
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/i;->j:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "JvmAnnotationNames.ENHANCED_MUTABILITY_ANNOTATION"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/b;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/b;

    return-void
.end method

.method private static final a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;"
        }
    .end annotation

    .prologue
    .line 143
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 146
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/k;

    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/k;-><init>(Ljava/util/List;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    .line 147
    :goto_0
    return-object v0

    .line 144
    :pswitch_0
    const-string v1, "At least one Annotations object expected"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 145
    :pswitch_1
    invoke-static {p0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    goto :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;-><init>(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/b/a/h;)V

    return-object v0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/d/a/g/d;Lkotlin/reflect/jvm/internal/impl/d/a/g/p;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/h;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/d;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/p;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/p;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 158
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v0, :cond_1

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    .line 162
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->b()Lkotlin/reflect/jvm/internal/impl/d/a/g/e;

    move-result-object v0

    if-nez v0, :cond_3

    .line 175
    :cond_2
    :goto_1
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    goto :goto_0

    .line 162
    :cond_3
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/s;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/e;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 164
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/f/a;->c(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->c(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    goto :goto_0

    .line 169
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/f/a;->b(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/f/a;->d(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->c(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    goto :goto_0

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/a/g/d;Lkotlin/reflect/jvm/internal/impl/d/a/g/p;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/d;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/p;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/p;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    .line 181
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->a()Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    move-result-object v0

    if-nez v0, :cond_1

    .line 184
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    goto :goto_0

    .line 181
    :cond_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/s;->b:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 182
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    goto :goto_0

    .line 183
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v0

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/d/a/b;I)Lkotlin/reflect/jvm/internal/impl/d/a/g/k;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ao;",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/d;",
            ">;I)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/k;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/ao;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v0, p0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;IZ)V

    .line 62
    :goto_0
    return-object v0

    .line 63
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v0, :cond_7

    move-object v0, p0

    .line 64
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    invoke-static {v0, p1, p2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;ILkotlin/reflect/jvm/internal/impl/d/a/g/p;)Lkotlin/reflect/jvm/internal/impl/d/a/g/o;

    move-result-object v3

    move-object v0, p0

    .line 65
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    invoke-static {v0, p1, p2, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;ILkotlin/reflect/jvm/internal/impl/d/a/g/p;)Lkotlin/reflect/jvm/internal/impl/d/a/g/o;

    move-result-object v4

    .line 66
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->c()I

    move-result v0

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->c()I

    move-result v5

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_1
    sget-boolean v5, Lkotlin/p;->a:Z

    if-eqz v5, :cond_2

    if-nez v0, :cond_2

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Different tree sizes of bounds: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lower = ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/m;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "), "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "upper = ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/m;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/m;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 66
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    .line 72
    :cond_2
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->d()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v2, v1

    .line 73
    :cond_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;

    if-eqz v2, :cond_6

    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    if-eqz v0, :cond_5

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v5

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v4

    invoke-direct {v0, v5, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    :goto_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->c()I

    move-result v3

    invoke-direct {v1, v0, v3, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;IZ)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;->e()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v4

    invoke-static {v0, v4}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    goto :goto_2

    :cond_6
    move-object v0, p0

    goto :goto_2

    .line 88
    :cond_7
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v0, :cond_8

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/k/v;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->c:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    invoke-static {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;ILkotlin/reflect/jvm/internal/impl/d/a/g/p;)Lkotlin/reflect/jvm/internal/impl/d/a/g/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;

    goto/16 :goto_0

    :cond_8
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/d/a/b;ILkotlin/reflect/jvm/internal/impl/d/a/g/p;)Lkotlin/reflect/jvm/internal/impl/d/a/g/o;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/d;",
            ">;I",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/p;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/o;"
        }
    .end annotation

    .prologue
    .line 93
    invoke-static/range {p3 .. p3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/d/a/g/p;)Z

    move-result v1

    .line 94
    if-nez v1, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;IZ)V

    .line 140
    :goto_0
    return-object v1

    .line 96
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 99
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;

    .line 100
    move-object/from16 v0, p3

    invoke-static {v2, v8, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/d/a/g/d;Lkotlin/reflect/jvm/internal/impl/d/a/g/p;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/h;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;->b()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v9

    .line 102
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/h;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v2

    .line 104
    new-instance v10, Lkotlin/d/b/x$b;

    invoke-direct {v10}, Lkotlin/d/b/x$b;-><init>()V

    add-int/lit8 v3, p2, 0x1

    iput v3, v10, Lkotlin/d/b/x$b;->a:I

    .line 105
    new-instance v11, Lkotlin/d/b/x$a;

    invoke-direct {v11}, Lkotlin/d/b/x$a;-><init>()V

    if-eqz v9, :cond_2

    const/4 v3, 0x1

    :goto_1
    iput-boolean v3, v11, Lkotlin/d/b/x$a;->a:Z

    .line 106
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 253
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 254
    const/4 v5, 0x0

    .line 255
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 256
    add-int/lit8 v6, v5, 0x1

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 108
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 109
    iget v3, v10, Lkotlin/d/b/x$b;->a:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v10, Lkotlin/d/b/x$b;->a:I

    .line 110
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/h;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v3

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/k/an;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v3

    .line 117
    :goto_3
    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v5, v6

    .line 255
    goto :goto_2

    .line 97
    :cond_1
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;IZ)V

    goto/16 :goto_0

    .line 105
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 113
    :cond_3
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v7

    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v7

    iget v13, v10, Lkotlin/d/b/x$b;->a:I

    move-object/from16 v0, p1

    invoke-static {v7, v0, v13}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/d/a/b;I)Lkotlin/reflect/jvm/internal/impl/d/a/g/k;

    move-result-object v13

    .line 114
    iget-boolean v7, v11, Lkotlin/d/b/x$a;->a:Z

    if-nez v7, :cond_4

    invoke-virtual {v13}, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->d()Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_4
    const/4 v7, 0x1

    :goto_4
    iput-boolean v7, v11, Lkotlin/d/b/x$a;->a:Z

    .line 115
    iget v7, v10, Lkotlin/d/b/x$b;->a:I

    invoke-virtual {v13}, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->c()I

    move-result v14

    add-int/2addr v7, v14

    iput v7, v10, Lkotlin/d/b/x$b;->a:I

    .line 116
    invoke-virtual {v13}, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->b()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v7

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v13

    const-string v3, "arg.projectionKind"

    invoke-static {v13, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-static {v7, v13, v3}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v3

    goto :goto_3

    .line 114
    :cond_5
    const/4 v7, 0x0

    goto :goto_4

    :cond_6
    move-object v3, v4

    .line 257
    check-cast v3, Ljava/util/List;

    move-object v1, p0

    .line 120
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object/from16 v0, p3

    invoke-static {v1, v8, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/a/g/d;Lkotlin/reflect/jvm/internal/impl/d/a/g/p;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;->b()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    .line 121
    iget-boolean v1, v11, Lkotlin/d/b/x$a;->a:Z

    if-nez v1, :cond_7

    if-eqz v5, :cond_8

    :cond_7
    const/4 v1, 0x1

    :goto_5
    iput-boolean v1, v11, Lkotlin/d/b/x$a;->a:Z

    .line 123
    iget v1, v10, Lkotlin/d/b/x$b;->a:I

    sub-int v10, v1, p2

    .line 124
    iget-boolean v1, v11, Lkotlin/d/b/x$a;->a:Z

    if-nez v1, :cond_9

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v10, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;IZ)V

    goto/16 :goto_0

    .line 121
    :cond_8
    const/4 v1, 0x0

    goto :goto_5

    .line 126
    :cond_9
    const/4 v1, 0x3

    new-array v1, v1, [Lkotlin/reflect/jvm/internal/impl/b/a/h;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/v;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v7

    aput-object v7, v1, v6

    const/4 v6, 0x1

    aput-object v9, v1, v6

    const/4 v6, 0x2

    aput-object v5, v1, v6

    invoke-static {v1}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 130
    invoke-static {v1}, Lkotlin/a/m;->h(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    .line 132
    const-string v5, "typeConstructor"

    invoke-static {v2, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    .line 139
    invoke-virtual {v8}, Lkotlin/reflect/jvm/internal/impl/d/a/g/d;->c()Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/g/f;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 140
    :goto_6
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v10, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/o;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;IZ)V

    move-object v1, v2

    goto/16 :goto_0

    :cond_a
    move-object v1, v2

    .line 139
    goto :goto_6
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/d/a/b",
            "<-",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/d;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/r;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qualifiers"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/ao;Lkotlin/d/a/b;I)Lkotlin/reflect/jvm/internal/impl/d/a/g/k;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/k;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/d/a/g/p;)Z
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->c:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    invoke-static {p0, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 3

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/i;->i:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "JvmAnnotationNames.ENHANCED_NULLABILITY_ANNOTATION"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final b(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 153
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-direct {v1, p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;-><init>(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/b/a/h;)V

    return-object v1
.end method

.method private static final c(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/g/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lkotlin/reflect/jvm/internal/impl/d/a/g/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 154
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-direct {v1, p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/c;-><init>(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/b/a/h;)V

    return-object v1
.end method
