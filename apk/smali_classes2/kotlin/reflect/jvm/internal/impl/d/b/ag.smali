.class public final Lkotlin/reflect/jvm/internal/impl/d/b/ag;
.super Ljava/lang/Object;
.source "typeSignatureMapping.kt"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/c;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->b()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a:Ljava/lang/String;

    return-void
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/d/b/r;Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/d/b/r",
            "<TT;>;TT;Z)TT;"
        }
    .end annotation

    .prologue
    .line 49
    if-eqz p2, :cond_0

    invoke-interface {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ac;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/r",
            "<TT;>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/ac",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 185
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_1

    .line 187
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/a/p;->a()Lkotlin/reflect/jvm/internal/impl/b/b/v;

    move-result-object v2

    if-ne v0, v2, :cond_2

    .line 189
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a:Ljava/lang/String;

    const-string v1, "CONTINUATION_INTERNAL_NAME"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 211
    :cond_1
    :goto_0
    return-object v1

    .line 192
    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    .line 194
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v2

    .line 195
    if-eqz v2, :cond_5

    .line 196
    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JvmPrimitiveType.get(primitiveType).desc"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 197
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/an;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/r;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 198
    :goto_1
    invoke-static {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/d/b/r;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 197
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 201
    :cond_5
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v2

    .line 202
    if-eqz v2, :cond_6

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->a(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/h/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 206
    :cond_6
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 207
    if-eqz v0, :cond_1

    .line 208
    invoke-static {v0, p2}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/d/b/ac;)Lkotlin/reflect/jvm/internal/impl/h/d/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/d/b;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JvmClassName.byClassId(c\u2026nfiguration).internalName"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ac;Lkotlin/reflect/jvm/internal/impl/d/b/l;Lkotlin/d/a/q;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/r",
            "<TT;>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/ac",
            "<+TT;>;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/l",
            "<TT;>;",
            "Lkotlin/d/a/q",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "-TT;-",
            "Lkotlin/reflect/jvm/internal/impl/d/b/ae;",
            "Lkotlin/n;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    const-string v0, "kotlinType"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mode"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeMappingConfiguration"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "writeGenericType"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/p;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ac;Lkotlin/reflect/jvm/internal/impl/d/b/l;Lkotlin/d/a/q;)Ljava/lang/Object;

    move-result-object v0

    .line 166
    :cond_0
    :goto_0
    return-object v0

    .line 86
    :cond_1
    invoke-static {p0, p1, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ac;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 87
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->a()Z

    move-result v1

    invoke-static {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/d/b/r;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    .line 88
    invoke-interface {p5, p0, v0, p2}, Lkotlin/d/a/q;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 92
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    .line 93
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/q;

    if-eqz v1, :cond_3

    .line 94
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/q;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/q;->G_()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "constructor.supertypes"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ac;->a(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 101
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ac;Lkotlin/reflect/jvm/internal/impl/d/b/l;Lkotlin/d/a/q;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 107
    :cond_3
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v6

    if-eqz v6, :cond_4

    move-object v0, v6

    .line 111
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 112
    const-string v0, "error/NonExistentClass"

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 113
    if-nez v6, :cond_5

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no descriptor for type constructor of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 113
    :cond_5
    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {p3, p0, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/ac;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    .line 114
    if-eqz p4, :cond_0

    invoke-virtual {p4, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    :cond_6
    instance-of v0, v6, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_c

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 119
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    .line 120
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "arrays must have one type argument"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 122
    :cond_7
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 123
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 126
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    if-ne v2, v3, :cond_a

    .line 127
    const-string v0, "java/lang/Object"

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 128
    if-eqz p4, :cond_8

    move-object v0, p4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/l;

    .line 129
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a()V

    .line 130
    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a(Ljava/lang/Object;)V

    .line 131
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->b()V

    .line 132
    nop

    .line 128
    check-cast p4, Lkotlin/reflect/jvm/internal/impl/d/b/l;

    :cond_8
    move-object v0, v1

    .line 146
    :cond_9
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 135
    :cond_a
    if-eqz p4, :cond_b

    invoke-virtual {p4}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a()V

    .line 138
    :cond_b
    const-string v2, "memberType"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    const-string v2, "memberProjection.projectionKind"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    move-result-object v2

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ac;Lkotlin/reflect/jvm/internal/impl/d/b/l;Lkotlin/d/a/q;)Ljava/lang/Object;

    move-result-object v0

    .line 143
    if-eqz p4, :cond_9

    invoke-virtual {p4}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->b()V

    goto :goto_1

    .line 149
    :cond_c
    instance-of v0, v6, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_f

    .line 151
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    move-object v0, v6

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->d(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 152
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->a()Ljava/lang/Object;

    move-result-object v0

    .line 157
    :cond_d
    :goto_2
    invoke-interface {p5, p0, v0, p2}, Lkotlin/d/a/q;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_e
    move-object v0, v6

    .line 154
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->D()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    const-string v1, "descriptor.original"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ac;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_d

    .line 155
    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v6}, Lkotlin/reflect/jvm/internal/impl/b/e;->D()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    const-string v1, "descriptor.original"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/b/ac;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/r;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 162
    :cond_f
    instance-of v0, v6, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v0, :cond_10

    .line 163
    const-string v0, "descriptor"

    invoke-static {v6, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v6

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/d;->d()Lkotlin/d/a/q;

    move-result-object v5

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ac;Lkotlin/reflect/jvm/internal/impl/d/b/l;Lkotlin/d/a/q;)Ljava/lang/Object;

    move-result-object v0

    .line 165
    if-eqz p4, :cond_0

    invoke-interface {v6}, Lkotlin/reflect/jvm/internal/impl/b/h;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "descriptor.getName()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 169
    :cond_10
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public static synthetic a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ac;Lkotlin/reflect/jvm/internal/impl/d/b/l;Lkotlin/d/a/q;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    and-int/lit8 v0, p6, 0x20

    if-eqz v0, :cond_0

    .line 76
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/d;->d()Lkotlin/d/a/q;

    move-result-object v5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/d/b/r;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ac;Lkotlin/reflect/jvm/internal/impl/d/b/l;Lkotlin/d/a/q;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v5, p5

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/b/ac;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/ac",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/16 v2, 0x2f

    const-string v0, "klass"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeMappingConfiguration"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 220
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->b()Ljava/lang/String;

    move-result-object v6

    .line 221
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v1, :cond_2

    .line 222
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 223
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "name"

    invoke-static {v6, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v6

    .line 232
    :cond_0
    :goto_0
    return-object v0

    .line 223
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_2
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_5

    move-object v1, v5

    :goto_1
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_3

    .line 230
    invoke-interface {p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ac;->b(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 232
    :goto_2
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->d:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/ac;->a()Lkotlin/d/a/m;

    move-result-object v1

    const-string v2, "name"

    invoke-static {v6, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0, v6}, Lkotlin/d/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 227
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected container: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 231
    :cond_4
    invoke-static {v1, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/b/ac;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/b/ac;ILjava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ad;->b:Lkotlin/reflect/jvm/internal/impl/d/b/ad;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ac;

    :goto_0
    invoke-static {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/b/ac;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 237
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v1

    .line 238
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    :goto_0
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Upper bounds should not be empty: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    move v0, v4

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 240
    check-cast v0, Ljava/lang/Iterable;

    .line 281
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 241
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v7, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v7, :cond_3

    move-object v0, v5

    :cond_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_4

    .line 242
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v7

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v7, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_5

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v0, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    move v0, v3

    :goto_1
    if-eqz v0, :cond_2

    move-object v0, v2

    .line 282
    :goto_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    if-eqz v0, :cond_7

    .line 243
    :goto_3
    return-object v0

    :cond_4
    move v0, v4

    .line 241
    goto :goto_1

    :cond_5
    move v0, v4

    .line 242
    goto :goto_1

    :cond_6
    move-object v0, v5

    .line 282
    goto :goto_2

    .line 243
    :cond_7
    invoke-static {v1}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "upperBounds.first()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_3
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    const-string v1, "descriptor"

    invoke-static {p0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    instance-of v1, p0, Lkotlin/reflect/jvm/internal/impl/b/l;

    if-eqz v1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 176
    :cond_2
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->k(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/an;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 177
    instance-of v1, p0, Lkotlin/reflect/jvm/internal/impl/b/ah;

    if-eqz v1, :cond_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
