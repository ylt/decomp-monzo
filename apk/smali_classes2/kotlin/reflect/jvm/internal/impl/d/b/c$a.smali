.class public final Lkotlin/reflect/jvm/internal/impl/d/b/c$a;
.super Ljava/lang/Object;
.source "BinaryClassAnnotationAndConstantLoaderImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/u$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/b/e;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Lkotlin/reflect/jvm/internal/impl/b/al;

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/b/c;Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/al;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            "Ljava/util/List;",
            "Lkotlin/reflect/jvm/internal/impl/b/al;",
            ")V"
        }
    .end annotation

    .prologue
    .line 92
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->c:Ljava/util/List;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->d:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->e:Ljava/util/HashMap;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->s:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 143
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c;)Lkotlin/reflect/jvm/internal/impl/h/b/g;

    move-result-object v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/b/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    .line 147
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c;)Lkotlin/reflect/jvm/internal/impl/h/b/g;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unresolved enum entry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/h/b/f;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/h/b/f;)V

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/h/b/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v1

    .line 161
    if-eqz v1, :cond_0

    .line 162
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->e:Ljava/util/HashMap;

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    :cond_0
    return-void
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Object;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c;)Lkotlin/reflect/jvm/internal/impl/h/b/g;

    move-result-object v0

    invoke-virtual {v0, p2}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/b/c;)Lkotlin/reflect/jvm/internal/impl/h/b/g;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported annotation argument: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;
    .locals 4

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 129
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a:Lkotlin/reflect/jvm/internal/impl/d/b/c;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const-string v0, "SourceElement.NO_SOURCE"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Ljava/util/List;

    invoke-virtual {v2, p2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/b/al;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 130
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;

    invoke-direct {v0, p0, v2, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/d/b/u$a;Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/ArrayList;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$a;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/b/u$b;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/c$a;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u$b;

    return-object v0
.end method

.method public a()V
    .locals 5

    .prologue
    .line 151
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->c:Ljava/util/List;

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/a/d;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->b:Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->e:Ljava/util/HashMap;

    check-cast v1, Ljava/util/Map;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->d:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v3, v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 96
    if-eqz p1, :cond_0

    .line 97
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/h/b/f;)V

    .line 99
    :cond_0
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enumClassId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enumEntryName"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/c$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/h/b/f;)V

    .line 103
    return-void
.end method
