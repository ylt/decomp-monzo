.class public final Lkotlin/reflect/jvm/internal/impl/d/a/a;
.super Ljava/lang/Object;
.source "specialBuiltinMembers.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/a;

.field private static final b:Lkotlin/reflect/jvm/internal/impl/d/a/l;

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/l;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 9

    .prologue
    const/16 v8, 0xa

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/a;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a;

    .line 212
    const-string v0, "java/util/List"

    const-string v1, "removeAt"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "JvmPrimitiveType.INT.desc"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "Ljava/lang/Object;"

    invoke-static {v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/l;

    .line 214
    nop

    .line 353
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 215
    const/16 v1, 0x8

    new-array v1, v1, [Lkotlin/h;

    const/4 v2, 0x0

    const-string v3, "Number"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "toByte"

    const-string v5, ""

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "JvmPrimitiveType.BYTE.desc"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    const-string v4, "byteValue"

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "Number"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "toShort"

    const-string v5, ""

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/d/c;->d:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "JvmPrimitiveType.SHORT.desc"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    const-string v4, "shortValue"

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "Number"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "toInt"

    const-string v5, ""

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "JvmPrimitiveType.INT.desc"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    const-string v4, "intValue"

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "Number"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "toLong"

    const-string v5, ""

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/d/c;->g:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "JvmPrimitiveType.LONG.desc"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    const-string v4, "longValue"

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "Number"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "toFloat"

    const-string v5, ""

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/d/c;->f:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "JvmPrimitiveType.FLOAT.desc"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    const-string v4, "floatValue"

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "Number"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "toDouble"

    const-string v5, ""

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/h/d/c;->h:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "JvmPrimitiveType.DOUBLE.desc"

    invoke-static {v6, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v3

    const-string v4, "doubleValue"

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/l;

    const-string v4, "remove"

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "CharSequence"

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "get"

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/d/c;->e:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "JvmPrimitiveType.INT.desc"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/h/d/c;->b:Lkotlin/reflect/jvm/internal/impl/h/d/c;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/h/d/c;->c()Ljava/lang/String;

    move-result-object v5

    const-string v6, "JvmPrimitiveType.CHAR.desc"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v3, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v0

    const-string v3, "charAt"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    .line 353
    check-cast v0, Ljava/util/Map;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->c:Ljava/util/Map;

    .line 229
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a;->c:Ljava/util/Map;

    .line 354
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lkotlin/a/ab;->a(I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 355
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 356
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 357
    check-cast v1, Ljava/util/Map$Entry;

    .line 229
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/l;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/l;->b()Ljava/lang/String;

    move-result-object v1

    check-cast v2, Ljava/util/Map$Entry;

    .line 355
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 359
    :cond_0
    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->d:Ljava/util/Map;

    .line 231
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 360
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 361
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 362
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/l;

    .line 231
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/l;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 363
    :cond_1
    check-cast v1, Ljava/util/List;

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a;->e:Ljava/util/List;

    .line 234
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 235
    nop

    .line 364
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 365
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 366
    check-cast v0, Ljava/util/Map$Entry;

    .line 235
    new-instance v4, Lkotlin/h;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/l;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/l;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v4, v2, v0}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 367
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 236
    nop

    .line 368
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 369
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 370
    check-cast v1, Lkotlin/h;

    .line 236
    invoke-virtual {v1}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 372
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 373
    if-nez v3, :cond_3

    .line 374
    nop

    .line 371
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 375
    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v3

    .line 371
    :goto_4
    check-cast v1, Ljava/util/List;

    .line 379
    check-cast v2, Lkotlin/h;

    .line 236
    invoke-virtual {v2}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    move-object v1, v3

    .line 378
    goto :goto_4

    .line 381
    :cond_4
    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->f:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 207
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->d:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->e:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/ak;)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 2

    .prologue
    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->d:Ljava/util/Map;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "functionDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 246
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/a$a;

    invoke-direct {v1, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ak;)V

    check-cast v1, Lkotlin/d/a/b;

    const/4 v4, 0x0

    invoke-static {v0, v3, v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    move v0, v3

    goto :goto_0
.end method

.method public final c(Lkotlin/reflect/jvm/internal/impl/b/ak;)Z
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "removeAt"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/a;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/l;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/l;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
