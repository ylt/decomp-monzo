.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/k;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;
.source "ReflectJavaAnnotationArguments.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/h;


# instance fields
.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "klass"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/k;->b:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public b()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;
    .locals 2

    .prologue
    .line 65
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/k;->b:Ljava/lang/Class;

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;->a(Ljava/lang/reflect/Type;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    return-object v0
.end method
