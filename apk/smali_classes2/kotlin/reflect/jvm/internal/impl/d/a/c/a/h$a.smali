.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;
.super Lkotlin/d/b/m;
.source "LazyJavaPackageFragment.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/t;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "+",
        "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->l()Lkotlin/reflect/jvm/internal/impl/b/ab;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fqName.asString()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/ab;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 91
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 90
    check-cast v0, Ljava/lang/String;

    .line 38
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/a;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v4

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 39
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->c()Lkotlin/reflect/jvm/internal/impl/d/b/t;

    move-result-object v4

    invoke-interface {v4, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/t;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v2

    if-eqz v2, :cond_1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/b/u;

    invoke-static {v0, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    check-cast v0, Lkotlin/h;

    :goto_1
    if-eqz v0, :cond_0

    .line 90
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 93
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 40
    invoke-static {v1}, Lkotlin/a/ab;->a(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
