.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
.super Ljava/lang/Object;
.source "context.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/a/c/j;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)V
    .locals 2

    .prologue
    const-string v0, "components"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameterResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    .line 67
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/j/i;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->a()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->o()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/d/a/c/j;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    return-object v0
.end method
