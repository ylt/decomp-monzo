.class final Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;
.super Ljava/lang/Object;
.source "UnsafeVarianceTypeSubstitution.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/af;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/ah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/k/af",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/r;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "argumentIndices"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->b:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 74
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/h;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v2, Lkotlin/h;

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->b:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v4}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)V

    new-instance v4, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/o;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->b:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1, v5}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;)V

    invoke-direct {v2, v3, v4}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ag",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v1

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 102
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 103
    check-cast v1, Lkotlin/a/x;

    .line 86
    invoke-virtual {v1}, Lkotlin/a/x;->c()I

    move-result v3

    invoke-virtual {v1}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 87
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;

    invoke-direct {v4, v1, v3, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ah;ILkotlin/reflect/jvm/internal/impl/d/b/ah$a;)V

    .line 95
    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 96
    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ah$a;->b:Ljava/util/List;

    return-object v0
.end method
