.class public final Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;
.super Ljava/lang/Object;
.source "RuntimeSourceElementFactory.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/e/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/a/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;)V
    .locals 1

    .prologue
    const-string v0, "javaElement"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/b/am;
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/am;->a:Lkotlin/reflect/jvm/internal/impl/b/am;

    const-string v1, "SourceFile.NO_SOURCE_FILE"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;

    return-object v0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/d/a/f/l;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/l$a;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/n;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
