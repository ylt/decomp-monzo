.class public final Lkotlin/reflect/jvm/internal/impl/d/b/ae;
.super Ljava/lang/Object;
.source "TypeMappingMode.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/ae$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

.field public static final e:Lkotlin/reflect/jvm/internal/impl/d/b/ae$a;


# instance fields
.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

.field private final k:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

.field private final l:Lkotlin/reflect/jvm/internal/impl/d/b/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ae$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->e:Lkotlin/reflect/jvm/internal/impl/d/b/ae$a;

    .line 37
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;-><init>(ZZZZLkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    .line 43
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x6e

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;-><init>(ZZZZLkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->b:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    .line 50
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x6b

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;-><init>(ZZZZLkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->c:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    .line 57
    new-instance v11, Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x6d

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;-><init>(ZZZZLkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;ILkotlin/d/b/i;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x6c

    const/4 v10, 0x0

    move-object v1, v11

    move v2, v13

    move v3, v12

    move v4, v14

    move v5, v15

    move-object v6, v0

    invoke-direct/range {v1 .. v10}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;-><init>(ZZZZLkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;ILkotlin/d/b/i;)V

    sput-object v11, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->d:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    return-void
.end method

.method private constructor <init>(ZZZZLkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->f:Z

    iput-boolean p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->g:Z

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->h:Z

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->i:Z

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->j:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->k:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->l:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    return-void
.end method

.method synthetic constructor <init>(ZZZZLkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;ILkotlin/d/b/i;)V
    .locals 8

    .prologue
    and-int/lit8 v0, p8, 0x1

    if-eqz v0, :cond_6

    .line 23
    const/4 v1, 0x1

    :goto_0
    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_5

    .line 24
    const/4 v2, 0x0

    :goto_1
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_4

    .line 26
    const/4 v3, 0x0

    :goto_2
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_3

    .line 27
    const/4 v4, 0x0

    :goto_3
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_2

    .line 28
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    move-object v5, v0

    :goto_4
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_1

    move-object v6, v5

    .line 29
    :goto_5
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    move-object v7, v5

    :goto_6
    move-object v0, p0

    .line 30
    invoke-direct/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/d/b/ae;-><init>(ZZZZLkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;Lkotlin/reflect/jvm/internal/impl/d/b/ae;)V

    return-void

    :cond_0
    move-object v7, p7

    goto :goto_6

    :cond_1
    move-object v6, p6

    goto :goto_5

    :cond_2
    move-object v5, p5

    goto :goto_4

    :cond_3
    move v4, p4

    goto :goto_3

    :cond_4
    move v3, p3

    goto :goto_2

    :cond_5
    move v2, p2

    goto :goto_1

    :cond_6
    move v1, p1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/d/b/ae;
    .locals 2

    .prologue
    const-string v0, "effectiveVariance"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/af;->a:[I

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 124
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->j:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    if-eqz v0, :cond_0

    move-object p0, v0

    .line 125
    :cond_0
    :goto_0
    return-object p0

    .line 122
    :pswitch_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->k:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    if-eqz v0, :cond_0

    move-object p0, v0

    goto :goto_0

    .line 123
    :pswitch_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->l:Lkotlin/reflect/jvm/internal/impl/d/b/ae;

    if-eqz v0, :cond_0

    move-object p0, v0

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->f:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/ae;->g:Z

    return v0
.end method
