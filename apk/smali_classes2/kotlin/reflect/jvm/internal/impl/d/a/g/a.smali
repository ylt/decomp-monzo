.class final Lkotlin/reflect/jvm/internal/impl/d/a/g/a;
.super Ljava/lang/Object;
.source "typeEnhancement.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/a/c;


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/g/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/a;

    return-void
.end method

.method private final g()Ljava/lang/Void;
    .locals 2

    .prologue
    .line 211
    const-string v1, "No methods should be called on this descriptor. Only its presence matters"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;->d()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public synthetic b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;->e()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;->f()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    return-object v0
.end method

.method public d()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;->g()Ljava/lang/Void;

    const/4 v0, 0x0

    throw v0
.end method

.method public e()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 213
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;->g()Ljava/lang/Void;

    const/4 v0, 0x0

    throw v0
.end method

.method public f()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 214
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/g/a;->g()Ljava/lang/Void;

    const/4 v0, 0x0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    const-string v0, "[EnhancedType]"

    return-object v0
.end method
