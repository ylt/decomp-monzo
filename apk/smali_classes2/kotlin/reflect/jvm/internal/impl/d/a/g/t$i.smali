.class final Lkotlin/reflect/jvm/internal/impl/d/a/g/t$i;
.super Lkotlin/d/b/m;
.source "typeQualifiers.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/g/t;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/d/a/g/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/e/b;",
        "Lkotlin/reflect/jvm/internal/impl/d/a/g/g;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/k/r;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$i;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$i;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/g/g;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/t$i;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 79
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/b/f;->c()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 81
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_1

    move-object v0, v1

    .line 79
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    if-eqz v0, :cond_3

    .line 83
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 78
    goto :goto_1

    .line 82
    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ALWAYS"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    goto :goto_0

    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    goto :goto_0

    .line 83
    :cond_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/g;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/g;

    goto :goto_1
.end method
