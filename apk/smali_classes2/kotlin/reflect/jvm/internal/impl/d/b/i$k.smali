.class public final Lkotlin/reflect/jvm/internal/impl/d/b/i$k;
.super Lkotlin/reflect/jvm/internal/impl/utils/b$a;
.source "JvmBuiltInsSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/d/b/i$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/utils/b$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/e;",
        "Lkotlin/reflect/jvm/internal/impl/d/b/i$b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lkotlin/d/b/x$c;


# direct methods
.method constructor <init>(Ljava/lang/String;Lkotlin/d/b/x$c;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->a:Ljava/lang/String;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->b:Lkotlin/d/b/x$c;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/utils/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/d/b/i$b;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->b:Lkotlin/d/b/x$c;

    iget-object v0, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 241
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    return v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 3

    .prologue
    const-string v0, "javaClassDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->a:Ljava/lang/String;

    const-string v2, "jvmDescriptor"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 245
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->b:Lkotlin/d/b/x$c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->a:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    iput-object v1, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    .line 250
    :cond_0
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->b:Lkotlin/d/b/x$c;

    iget-object v0, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 246
    :cond_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->b:Lkotlin/d/b/x$c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    iput-object v1, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    goto :goto_0

    .line 247
    :cond_2
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->b:Lkotlin/d/b/x$c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->d:Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    iput-object v1, v0, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    goto :goto_0

    .line 250
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;->a()Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    move-result-object v0

    return-object v0
.end method
