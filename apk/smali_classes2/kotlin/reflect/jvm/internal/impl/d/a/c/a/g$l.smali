.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;
.super Lkotlin/d/b/m;
.source "LazyJavaClassMemberScope.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/e/f;",
        "Lkotlin/reflect/jvm/internal/impl/b/b/g;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/b/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/b/g;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->c(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    .line 628
    if-nez v3, :cond_1

    .line 629
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->d(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    .line 630
    if-eqz v2, :cond_0

    .line 631
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v3

    .line 634
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-object v4, v2

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/d/a/f/d;

    invoke-static {v5, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/c/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/d;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v5

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v5, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v5

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/b/n;->a(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/n;

    move-result-object v0

    .line 630
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/g;

    .line 643
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v4

    .line 639
    goto :goto_0

    .line 642
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->b:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$l;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    const/16 v5, 0x8

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/d/a/f/g;Lkotlin/reflect/jvm/internal/impl/b/e;ILkotlin/d/b/i;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/g;

    goto :goto_1
.end method
