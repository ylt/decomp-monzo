.class public final Lkotlin/reflect/jvm/internal/impl/d/a/a/i;
.super Lkotlin/reflect/jvm/internal/impl/d/a/a/c;
.source "JavaAnnotationMapper.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
    .locals 2

    .prologue
    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "c"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->q()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    const-string v1, "c.module.builtIns.retentionAnnotation"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-direct {p0, p2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/a;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    .line 152
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/i$a;

    invoke-direct {v0, p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/i$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/i;Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/i;->a:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method


# virtual methods
.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/i;->a:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method
