.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;
.source "ReflectJavaField.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/n;


# instance fields
.field private final a:Ljava/lang/reflect/Field;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Field;)V
    .locals 1

    .prologue
    const-string v0, "member"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/r;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;->a:Ljava/lang/reflect/Field;

    return-void
.end method


# virtual methods
.method public synthetic L_()Ljava/lang/reflect/Member;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;->h()Ljava/lang/reflect/Field;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Member;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;->h()Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->isEnumConstant()Z

    move-result v0

    return v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;->g()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;->h()Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v1

    const-string v2, "member.genericType"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;->a(Ljava/lang/reflect/Type;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/p;->a:Ljava/lang/reflect/Field;

    return-object v0
.end method
