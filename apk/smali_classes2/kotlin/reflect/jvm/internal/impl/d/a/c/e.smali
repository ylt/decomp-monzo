.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/e;
.super Ljava/lang/Object;
.source "LazyJavaPackageFragmentProvider.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/z;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final b:Lkotlin/reflect/jvm/internal/impl/j/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/d",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b;)V
    .locals 2

    .prologue
    const-string v0, "components"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/j$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/j;

    invoke-direct {v1, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b;Lkotlin/reflect/jvm/internal/impl/d/a/c/j;)V

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/e$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/e$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/e;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->b:Lkotlin/reflect/jvm/internal/impl/j/d;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    return-object v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->b:Lkotlin/reflect/jvm/internal/impl/j/d;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/j/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    return-object v0
.end method


# virtual methods
.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->b(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/d/a/b;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/d/a/b;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/e;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->g()Ljava/util/List;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method
