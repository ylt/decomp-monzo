.class public final Lkotlin/reflect/jvm/internal/impl/d/b/e$a;
.super Ljava/lang/Object;
.source "DeserializedDescriptorResolver.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/b/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/e$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/b/e$a;)Lkotlin/reflect/jvm/internal/impl/d/b/m;
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/e$a;->d()Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-result-object v0

    return-object v0
.end method

.method private final c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private final d()Lkotlin/reflect/jvm/internal/impl/d/b/m;
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->d()Lkotlin/reflect/jvm/internal/impl/d/b/m;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/b/a/a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 114
    const-string v0, "kotlin.test.is.pre.release"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->e()Z

    move-result v0

    goto :goto_0
.end method
