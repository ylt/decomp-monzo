.class public final Lkotlin/reflect/jvm/internal/impl/d/a/a/e;
.super Ljava/lang/Object;
.source "JavaAnnotationMapper.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/d/a/a/e;

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/n;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/e;

    .line 164
    const/16 v0, 0xa

    new-array v3, v0, [Lkotlin/h;

    const-string v0, "PACKAGE"

    const-class v1, Lkotlin/reflect/jvm/internal/impl/b/a/n;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v6

    const-string v2, "TYPE"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->a:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/n;->n:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v1, Ljava/lang/Enum;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v7

    const-string v1, "ANNOTATION_TYPE"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->b:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v8

    const-string v1, "TYPE_PARAMETER"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->c:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v9

    const/4 v1, 0x4

    const-string v2, "FIELD"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->e:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v1

    const/4 v1, 0x5

    const-string v2, "LOCAL_VARIABLE"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->f:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v1

    const/4 v1, 0x6

    const-string v2, "PARAMETER"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->g:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v1

    const/4 v1, 0x7

    const-string v2, "CONSTRUCTOR"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->h:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v1

    const/16 v4, 0x8

    const-string v5, "METHOD"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->i:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/n;->j:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v1, Ljava/lang/Enum;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/a/n;->k:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v2, Ljava/lang/Enum;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v5, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v4

    const/16 v1, 0x9

    const-string v2, "TYPE_USE"

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/n;->l:Lkotlin/reflect/jvm/internal/impl/b/a/n;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v3}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->b:Ljava/util/Map;

    .line 192
    new-array v0, v9, [Lkotlin/h;

    const-string v1, "RUNTIME"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/a/m;->a:Lkotlin/reflect/jvm/internal/impl/b/a/m;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "CLASS"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/a/m;->b:Lkotlin/reflect/jvm/internal/impl/b/a/m;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v7

    const-string v1, "SOURCE"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/a/m;->c:Lkotlin/reflect/jvm/internal/impl/b/a/m;

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-static {v0}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.Map<K, V>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumSet;

    if-eqz v0, :cond_1

    check-cast v0, Ljava/util/Set;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/b;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/a/l;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const-string v0, "arguments"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builtIns"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    check-cast p1, Ljava/lang/Iterable;

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 215
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;

    if-eqz v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 216
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 183
    nop

    .line 217
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 224
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 225
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;

    .line 183
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 226
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 183
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 228
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 184
    nop

    .line 229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 238
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 237
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a/n;

    .line 184
    invoke-virtual {p2, v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/a/n;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 237
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 240
    :cond_5
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 185
    nop

    .line 241
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 242
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 243
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 185
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/h/b/i;

    const-string v4, "it"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/i;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 244
    :cond_6
    check-cast v1, Ljava/util/List;

    .line 186
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a/d;->b()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/a/l;->p()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v0

    .line 189
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/h/b/b;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_7

    :goto_5
    invoke-direct {v2, v1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/h/b/b;-><init>(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/a/l;)V

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    return-object v0

    :cond_7
    const-string v0, "Error: AnnotationTarget[]"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v3, "ErrorUtils.createErrorTy\u2026ror: AnnotationTarget[]\")"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_5
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/b;Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/b;",
            "Lkotlin/reflect/jvm/internal/impl/a/l;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "element"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builtIns"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;

    if-nez v0, :cond_6

    move-object v0, v2

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;

    if-eqz v0, :cond_5

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;

    .line 201
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/e;->c:Ljava/util/Map;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    :goto_1
    if-nez v1, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.Map<K, V>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v3, v2

    goto :goto_1

    :cond_1
    move-object v0, v1

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/m;

    if-eqz v0, :cond_4

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/m;

    .line 202
    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/a/m;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_2

    move-object v0, v2

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/b/i;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/i;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/i;

    .line 201
    :goto_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/i;

    .line 200
    :goto_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/i;

    :goto_4
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    return-object v0

    :cond_3
    move-object v0, v2

    .line 202
    goto :goto_2

    :cond_4
    move-object v0, v2

    .line 201
    goto :goto_3

    :cond_5
    move-object v0, v2

    .line 200
    goto :goto_4

    :cond_6
    move-object v0, p1

    goto :goto_0
.end method
