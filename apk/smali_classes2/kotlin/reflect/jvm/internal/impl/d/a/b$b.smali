.class public enum Lkotlin/reflect/jvm/internal/impl/d/a/b$b;
.super Ljava/lang/Enum;
.source "specialBuiltinMembers.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/a/b$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/b$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

.field private static final synthetic e:[Lkotlin/reflect/jvm/internal/impl/d/a/b$b;


# instance fields
.field private final f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    const-string v2, "NULL"

    const/4 v3, 0x0

    .line 104
    invoke-direct {v1, v2, v4, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    const-string v2, "INDEX"

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v5, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    const-string v2, "FALSE"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v2, v6, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    aput-object v1, v0, v6

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b$a;

    const-string v2, "MAP_GET_OR_DEFAULT"

    invoke-direct {v1, v2, v7}, Lkotlin/reflect/jvm/internal/impl/d/a/b$b$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->d:Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    aput-object v1, v0, v7

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->e:[Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->f:Ljava/lang/Object;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/b$b;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/a/b$b;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->e:[Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/a/b$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/b$b;

    return-object v0
.end method
