.class public final enum Lkotlin/reflect/jvm/internal/impl/d/a/b$a;
.super Ljava/lang/Enum;
.source "specialBuiltinMembers.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

.field private static final synthetic d:[Lkotlin/reflect/jvm/internal/impl/d/a/b$a;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    const-string v2, "ONE_COLLECTION_PARAMETER"

    const-string v3, "Ljava/util/Collection<+Ljava/lang/Object;>;"

    .line 178
    invoke-direct {v1, v2, v5, v3, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    const-string v2, "OBJECT_PARAMETER_NON_GENERIC"

    const/4 v3, 0x0

    .line 179
    invoke-direct {v1, v2, v4, v3, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->b:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    const-string v2, "OBJECT_PARAMETER_GENERIC"

    const-string v3, "Ljava/lang/Object;"

    .line 180
    invoke-direct {v1, v2, v6, v3, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->c:Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    aput-object v1, v0, v6

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->d:[Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->e:Ljava/lang/String;

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->f:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/b$a;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/a/b$a;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->d:[Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/a/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/b$a;

    return-object v0
.end method
