.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;
.super Ljava/lang/Object;
.source "LazyJavaAnnotationDescriptor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/a/c;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/j/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

.field private final d:Lkotlin/reflect/jvm/internal/impl/h/b/g;

.field private final e:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;>;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final g:Lkotlin/reflect/jvm/internal/impl/d/a/f/a;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/a;)V
    .locals 2

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "javaAnnotation"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->g:Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/g;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a:Lkotlin/reflect/jvm/internal/impl/j/g;

    .line 51
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e$d;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e$d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 59
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-result-object v1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->g:Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/l;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/e/b;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/l;)Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->c:Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    .line 61
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/g;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->e:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    const-string v2, "ClassId.topLevel(fqName)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->d()Lkotlin/reflect/jvm/internal/impl/d/b/e;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/b/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->m()Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/s;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b/aa;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 154
    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;Lkotlin/reflect/jvm/internal/impl/d/a/f/b;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/b;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-direct {v0, v2, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/a;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Lkotlin/reflect/jvm/internal/impl/b/a/c;)Lkotlin/reflect/jvm/internal/impl/h/b/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/b;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/b;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 92
    .line 93
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/o;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/o;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 94
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/m;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_1
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/e;

    if-eqz v0, :cond_3

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/b;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/e;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/e;->b()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->c:Lkotlin/reflect/jvm/internal/impl/e/f;

    const-string v1, "DEFAULT_ANNOTATION_MEMBER_NAME"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 96
    :cond_3
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/c;

    if-eqz v0, :cond_4

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/c;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/c;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/a;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_4
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/h;

    if-eqz v0, :cond_5

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/f/h;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/h;->b()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    goto :goto_0

    .line 98
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/n;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 118
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 127
    :goto_0
    return-object v0

    .line 120
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->e()Lkotlin/reflect/jvm/internal/impl/d/a/f/g;

    move-result-object v0

    .line 122
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->k()Lkotlin/reflect/jvm/internal/impl/d/a/c/h;

    move-result-object v2

    invoke-interface {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 124
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->r()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->s:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 125
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 122
    goto :goto_0

    .line 127
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/b/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/v;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 132
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;

    move-result-object v6

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    const/4 v4, 0x6

    move v2, v1

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    invoke-virtual {v6, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/c;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/v;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/an;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 137
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    new-instance v4, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v0, "java.lang.Class"

    invoke-direct {v4, v0}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->p:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-static {v2, v4, v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-direct {v2, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    invoke-static {v2}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 141
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    .line 143
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/h/b/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v3

    .line 137
    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/b;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-object v0

    .line 109
    :cond_1
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    invoke-static {p1, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 111
    check-cast p2, Ljava/lang/Iterable;

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 164
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 165
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

    .line 112
    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/b;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v1

    if-eqz v1, :cond_2

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a()Lkotlin/reflect/jvm/internal/impl/h/b/q;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_2

    .line 166
    :cond_3
    check-cast v0, Ljava/util/List;

    .line 114
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->d:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v3, "valueParameter.type"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/h/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;)Lkotlin/reflect/jvm/internal/impl/j/g;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a:Lkotlin/reflect/jvm/internal/impl/j/g;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    return-object v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->f()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private final f()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->g()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->k()Ljava/util/Collection;

    move-result-object v3

    .line 75
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->g:Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a;->a()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 157
    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/a/ab;->a(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/f/d;->c(II)I

    move-result v2

    .line 158
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 159
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    .line 160
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/b;

    .line 77
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/b;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    move-object v0, v3

    .line 79
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e$b;

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;Ljava/util/Map;)V

    move-object v1, v2

    check-cast v1, Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/lang/Iterable;Lkotlin/d/a/b;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method private final g()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "type()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->e:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->d()Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    return-object v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/d/a/e/a;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->c:Lkotlin/reflect/jvm/internal/impl/d/a/e/a;

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/d/a/f/a;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/e;->g:Lkotlin/reflect/jvm/internal/impl/d/a/f/a;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 147
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->e:Lkotlin/reflect/jvm/internal/impl/g/c;

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/g/c;Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
