.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/h;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;
.source "ReflectJavaAnnotationArguments.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/e;


# instance fields
.field private final b:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    const-string v0, "values"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/h;->b:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public b()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/h;->b:[Ljava/lang/Object;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 76
    const/4 v1, 0x0

    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    aget-object v3, v2, v1

    .line 47
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d$a;

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d$a;->a(Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/d;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 78
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 47
    return-object v0
.end method
