.class public final Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;
.super Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;
.source "ReflectJavaWildcardType.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/a/f/z;


# instance fields
.field private final b:Ljava/lang/reflect/WildcardType;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/WildcardType;)V
    .locals 1

    .prologue
    const-string v0, "reflectType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;->b:Ljava/lang/reflect/WildcardType;

    return-void
.end method


# virtual methods
.method public synthetic K_()Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;->e()Ljava/lang/reflect/WildcardType;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/d/a/f/v;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;->d()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/v;

    return-object v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;->e()Ljava/lang/reflect/WildcardType;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/a/g;->b([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    const-class v1, Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;->e()Ljava/lang/reflect/WildcardType;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 26
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;->e()Ljava/lang/reflect/WildcardType;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v2

    move-object v0, v1

    .line 27
    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    if-gt v0, v4, :cond_0

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    if-le v0, v4, :cond_1

    .line 28
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wildcard types with many bounds are not yet supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;->e()Ljava/lang/reflect/WildcardType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    move-object v0, v2

    .line 31
    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    if-ne v0, v4, :cond_2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;

    check-cast v2, [Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/a/g;->d([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "lowerBounds.single()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;->a(Ljava/lang/reflect/Type;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    .line 30
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    .line 32
    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    if-ne v0, v4, :cond_4

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/a/g;->d([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    const-class v1, Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;->a:Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;

    const-string v2, "ub"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w$a;->a(Ljava/lang/reflect/Type;)Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    move-result-object v0

    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/w;

    goto :goto_0

    :cond_3
    move-object v0, v3

    goto :goto_1

    :cond_4
    move-object v0, v3

    .line 33
    goto :goto_0
.end method

.method protected e()Ljava/lang/reflect/WildcardType;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/f/a/z;->b:Ljava/lang/reflect/WildcardType;

    return-object v0
.end method
