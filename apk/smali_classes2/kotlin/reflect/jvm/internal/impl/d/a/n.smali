.class public final Lkotlin/reflect/jvm/internal/impl/d/a/n;
.super Ljava/lang/Object;
.source "specialBuiltinMembers.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 262
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v2

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/b;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 264
    :goto_0
    return-object v1

    .line 265
    :cond_0
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-eqz v0, :cond_2

    .line 266
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/n$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/n$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p0, v3, v0, v4, v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    :goto_1
    move-object v1, v0

    .line 264
    goto :goto_0

    .line 265
    :cond_2
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/af;

    if-nez v0, :cond_1

    .line 267
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-eqz v0, :cond_3

    .line 268
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/n$b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/n$b;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p0, v3, v0, v4, v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 271
    goto :goto_1
.end method

.method public static final synthetic a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->b(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/e/c;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->b(Lkotlin/reflect/jvm/internal/impl/e/c;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/a;)Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "specialCallableDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    .line 325
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v3

    .line 327
    :goto_0
    if-eqz v3, :cond_4

    .line 328
    instance-of v0, v3, Lkotlin/reflect/jvm/internal/impl/d/a/b/c;

    if-nez v0, :cond_3

    .line 332
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v1, v2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/p;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v4

    .line 334
    :goto_1
    if-eqz v0, :cond_3

    move-object v0, v3

    .line 335
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v4

    .line 342
    :goto_2
    return v0

    :cond_1
    move v0, v5

    .line 332
    goto :goto_1

    :cond_2
    move v0, v5

    .line 335
    goto :goto_2

    .line 339
    :cond_3
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v3

    goto :goto_0

    :cond_4
    move v0, v5

    .line 342
    goto :goto_2
.end method

.method private static final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/l;
    .locals 5

    .prologue
    .line 48
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/l;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "Name.identifier(name)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;)V

    .line 50
    return-object v0
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 2

    .prologue
    .line 42
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "child(Name.identifier(name))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/e/c;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 2

    .prologue
    .line 43
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "child(Name.identifier(name)).toSafe()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 283
    :goto_0
    return-object v0

    .line 281
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a:Lkotlin/reflect/jvm/internal/impl/d/a/b;

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    .line 283
    :cond_1
    const/4 v2, 0x0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/n$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/n$c;

    check-cast v0, Lkotlin/d/a/b;

    const/4 v3, 0x1

    invoke-static {p0, v2, v0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;ZLkotlin/d/a/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    goto :goto_0
.end method

.method public static final d(Lkotlin/reflect/jvm/internal/impl/b/b;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "callableMemberDescriptor"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 305
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->g(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 308
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-eqz v2, :cond_1

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Ljava/lang/String;

    move-result-object v0

    .line 307
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 306
    goto :goto_0

    .line 309
    :cond_1
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-eqz v2, :cond_3

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/a;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 310
    goto :goto_0
.end method

.method public static final e(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 347
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 348
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/b/a;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    :cond_1
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/d/a/b/c;

    .line 349
    return v0
.end method

.method public static final f(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 351
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->e(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-nez v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final g(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    .line 317
    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/n;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    .line 318
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
