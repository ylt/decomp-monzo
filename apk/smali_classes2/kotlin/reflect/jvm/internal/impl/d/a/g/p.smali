.class final enum Lkotlin/reflect/jvm/internal/impl/d/a/g/p;
.super Ljava/lang/Enum;
.source "typeEnhancement.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/d/a/g/p;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

.field private static final synthetic d:[Lkotlin/reflect/jvm/internal/impl/d/a/g/p;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    const-string v2, "FLEXIBLE_LOWER"

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->a:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    aput-object v1, v0, v3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    const-string v2, "FLEXIBLE_UPPER"

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->b:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    const-string v2, "INFLEXIBLE"

    invoke-direct {v1, v2, v5}, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->c:Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->d:[Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/a/g/p;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/d/a/g/p;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->d:[Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/d/a/g/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/d/a/g/p;

    return-object v0
.end method
