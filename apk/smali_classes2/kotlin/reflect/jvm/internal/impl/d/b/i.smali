.class public Lkotlin/reflect/jvm/internal/impl/d/b/i;
.super Ljava/lang/Object;
.source "JvmBuiltInsSettings.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/a;
.implements Lkotlin/reflect/jvm/internal/impl/i/b/ac;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/d/b/i$b;,
        Lkotlin/reflect/jvm/internal/impl/d/b/i$a;
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

.field private static final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lkotlin/reflect/jvm/internal/impl/f/a;

.field private final d:Lkotlin/c;

.field private final e:Lkotlin/c;

.field private final f:Lkotlin/reflect/jvm/internal/impl/k/r;

.field private final g:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final h:Lkotlin/reflect/jvm/internal/impl/j/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/a",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final j:Lkotlin/reflect/jvm/internal/impl/b/w;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    .line 345
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    const-string v1, "Collection"

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "toArray()[Ljava/lang/Object;"

    aput-object v3, v2, v6

    const-string v3, "toArray([Ljava/lang/Object;)[Ljava/lang/Object;"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    const-string v1, "java/lang/annotation/Annotation.annotationType()Ljava/lang/Class;"

    .line 347
    invoke-static {v0, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->k:Ljava/util/Set;

    .line 352
    nop

    .line 529
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 353
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/i$a;)Ljava/util/Set;

    move-result-object v2

    const-string v1, "List"

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "sort(Ljava/util/Comparator;)V"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    const-string v1, "String"

    const/16 v3, 0x2d

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "codePointAt(I)I"

    aput-object v4, v3, v6

    const-string v4, "codePointBefore(I)I"

    aput-object v4, v3, v7

    const-string v4, "codePointCount(II)I"

    aput-object v4, v3, v8

    const-string v4, "compareToIgnoreCase(Ljava/lang/String;)I"

    aput-object v4, v3, v9

    const-string v4, "concat(Ljava/lang/String;)Ljava/lang/String;"

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string v5, "contains(Ljava/lang/CharSequence;)Z"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "contentEquals(Ljava/lang/CharSequence;)Z"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "contentEquals(Ljava/lang/StringBuffer;)Z"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "endsWith(Ljava/lang/String;)Z"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "equalsIgnoreCase(Ljava/lang/String;)Z"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "getBytes()[B"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "getBytes(II[BI)V"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "getBytes(Ljava/lang/String;)[B"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    const-string v5, "getBytes(Ljava/nio/charset/Charset;)[B"

    aput-object v5, v3, v4

    const/16 v4, 0xe

    const-string v5, "getChars(II[CI)V"

    aput-object v5, v3, v4

    const/16 v4, 0xf

    const-string v5, "indexOf(I)I"

    aput-object v5, v3, v4

    const/16 v4, 0x10

    const-string v5, "indexOf(II)I"

    aput-object v5, v3, v4

    const/16 v4, 0x11

    const-string v5, "indexOf(Ljava/lang/String;)I"

    aput-object v5, v3, v4

    const/16 v4, 0x12

    const-string v5, "indexOf(Ljava/lang/String;I)I"

    aput-object v5, v3, v4

    const/16 v4, 0x13

    const-string v5, "intern()Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x14

    const-string v5, "isEmpty()Z"

    aput-object v5, v3, v4

    const/16 v4, 0x15

    const-string v5, "lastIndexOf(I)I"

    aput-object v5, v3, v4

    const/16 v4, 0x16

    const-string v5, "lastIndexOf(II)I"

    aput-object v5, v3, v4

    const/16 v4, 0x17

    const-string v5, "lastIndexOf(Ljava/lang/String;)I"

    aput-object v5, v3, v4

    const/16 v4, 0x18

    const-string v5, "lastIndexOf(Ljava/lang/String;I)I"

    aput-object v5, v3, v4

    const/16 v4, 0x19

    const-string v5, "matches(Ljava/lang/String;)Z"

    aput-object v5, v3, v4

    const/16 v4, 0x1a

    const-string v5, "offsetByCodePoints(II)I"

    aput-object v5, v3, v4

    const/16 v4, 0x1b

    const-string v5, "regionMatches(ILjava/lang/String;II)Z"

    aput-object v5, v3, v4

    const/16 v4, 0x1c

    const-string v5, "regionMatches(ZILjava/lang/String;II)Z"

    aput-object v5, v3, v4

    const/16 v4, 0x1d

    const-string v5, "replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x1e

    const-string v5, "replace(CC)Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x1f

    const-string v5, "replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x20

    const-string v5, "replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x21

    const-string v5, "split(Ljava/lang/String;I)[Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x22

    const-string v5, "split(Ljava/lang/String;)[Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x23

    const-string v5, "startsWith(Ljava/lang/String;I)Z"

    aput-object v5, v3, v4

    const/16 v4, 0x24

    const-string v5, "startsWith(Ljava/lang/String;)Z"

    aput-object v5, v3, v4

    const/16 v4, 0x25

    const-string v5, "substring(II)Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x26

    const-string v5, "substring(I)Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x27

    const-string v5, "toCharArray()[C"

    aput-object v5, v3, v4

    const/16 v4, 0x28

    const-string v5, "toLowerCase()Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x29

    const-string v5, "toLowerCase(Ljava/util/Locale;)Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x2a

    const-string v5, "toUpperCase()Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x2b

    const-string v5, "toUpperCase(Ljava/util/Locale;)Ljava/lang/String;"

    aput-object v5, v3, v4

    const/16 v4, 0x2c

    const-string v5, "trim()Ljava/lang/String;"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 355
    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    const-string v1, "Double"

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "isInfinite()Z"

    aput-object v4, v3, v6

    const-string v4, "isNaN()Z"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 374
    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    const-string v1, "Float"

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "isInfinite()Z"

    aput-object v4, v3, v6

    const-string v4, "isNaN()Z"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 376
    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    const-string v2, "Enum"

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "getDeclaringClass()Ljava/lang/Class;"

    aput-object v4, v3, v6

    const-string v4, "finalize()V"

    aput-object v4, v3, v7

    invoke-virtual {v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 377
    invoke-static {v1, v0}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 529
    check-cast v0, Ljava/util/Set;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->l:Ljava/util/Set;

    .line 390
    nop

    .line 530
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 391
    const-string v1, "CharSequence"

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "codePoints()Ljava/util/stream/IntStream;"

    aput-object v3, v2, v6

    const-string v3, "chars()Ljava/util/stream/IntStream;"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    const-string v2, "Iterator"

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "forEachRemaining(Ljava/util/function/Consumer;)V"

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 392
    invoke-static {v1, v2}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    const-string v1, "Iterable"

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "forEach(Ljava/util/function/Consumer;)V"

    aput-object v4, v3, v6

    const-string v4, "spliterator()Ljava/util/Spliterator;"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 395
    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    const-string v1, "Throwable"

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "setStackTrace([Ljava/lang/StackTraceElement;)V"

    aput-object v4, v3, v6

    const-string v4, "fillInStackTrace()Ljava/lang/Throwable;"

    aput-object v4, v3, v7

    const-string v4, "getLocalizedMessage()Ljava/lang/String;"

    aput-object v4, v3, v8

    const-string v4, "printStackTrace()V"

    aput-object v4, v3, v9

    const-string v4, "printStackTrace(Ljava/io/PrintStream;)V"

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string v5, "printStackTrace(Ljava/io/PrintWriter;)V"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "getStackTrace()[Ljava/lang/StackTraceElement;"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "getSuppressed()[Ljava/lang/Throwable;"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "addSuppressed(Ljava/lang/Throwable;)V"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 398
    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    const-string v1, "Collection"

    new-array v3, v10, [Ljava/lang/String;

    const-string v4, "spliterator()Ljava/util/Spliterator;"

    aput-object v4, v3, v6

    const-string v4, "parallelStream()Ljava/util/stream/Stream;"

    aput-object v4, v3, v7

    const-string v4, "stream()Ljava/util/stream/Stream;"

    aput-object v4, v3, v8

    const-string v4, "removeIf(Ljava/util/function/Predicate;)Z"

    aput-object v4, v3, v9

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 405
    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    const-string v1, "List"

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "replaceAll(Ljava/util/function/UnaryOperator;)V"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 409
    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    const-string v2, "Map"

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"

    aput-object v4, v3, v6

    const-string v4, "forEach(Ljava/util/function/BiConsumer;)V"

    aput-object v4, v3, v7

    const-string v4, "replaceAll(Ljava/util/function/BiFunction;)V"

    aput-object v4, v3, v8

    const-string v4, "merge(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;"

    aput-object v4, v3, v9

    const-string v4, "computeIfPresent(Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;"

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string v5, "putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "computeIfAbsent(Ljava/lang/Object;Ljava/util/function/Function;)Ljava/lang/Object;"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "compute(Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 412
    invoke-static {v1, v0}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 530
    check-cast v0, Ljava/util/Set;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->m:Ljava/util/Set;

    .line 427
    nop

    .line 531
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 428
    const-string v1, "Collection"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "removeIf(Ljava/util/function/Predicate;)Z"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    const-string v2, "List"

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "replaceAll(Ljava/util/function/UnaryOperator;)V"

    aput-object v4, v3, v6

    const-string v4, "sort(Ljava/util/Comparator;)V"

    aput-object v4, v3, v7

    invoke-virtual {v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v1, v2}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    const-string v2, "Map"

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "computeIfAbsent(Ljava/lang/Object;Ljava/util/function/Function;)Ljava/lang/Object;"

    aput-object v4, v3, v6

    const-string v4, "computeIfPresent(Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;"

    aput-object v4, v3, v7

    const-string v4, "compute(Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;"

    aput-object v4, v3, v8

    const-string v4, "merge(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;"

    aput-object v4, v3, v9

    const-string v4, "putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string v5, "remove(Ljava/lang/Object;Ljava/lang/Object;)Z"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "replaceAll(Ljava/util/function/BiFunction;)V"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->b(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 430
    invoke-static {v1, v0}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 531
    check-cast v0, Ljava/util/Set;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->n:Ljava/util/Set;

    .line 444
    nop

    .line 532
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 445
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->b(Lkotlin/reflect/jvm/internal/impl/d/b/i$a;)Ljava/util/Set;

    move-result-object v2

    const-string v3, "Float"

    new-array v1, v7, [Ljava/lang/String;

    const-string v4, "D"

    aput-object v4, v1, v6

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    invoke-static {v1, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v2, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    const-string v3, "String"

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/String;

    const-string v4, "[C"

    aput-object v4, v1, v6

    const-string v4, "[CII"

    aput-object v4, v1, v7

    const-string v4, "[III"

    aput-object v4, v1, v8

    const-string v4, "[BIILjava/lang/String;"

    aput-object v4, v1, v9

    const-string v4, "[BIILjava/nio/charset/Charset;"

    aput-object v4, v1, v10

    const/4 v4, 0x5

    const-string v5, "[BLjava/lang/String;"

    aput-object v5, v1, v4

    const/4 v4, 0x6

    const-string v5, "[BLjava/nio/charset/Charset;"

    aput-object v5, v1, v4

    const/4 v4, 0x7

    const-string v5, "[BII"

    aput-object v5, v1, v4

    const/16 v4, 0x8

    const-string v5, "[B"

    aput-object v5, v1, v4

    const/16 v4, 0x9

    const-string v5, "Ljava/lang/StringBuffer;"

    aput-object v5, v1, v4

    const/16 v4, 0xa

    const-string v5, "Ljava/lang/StringBuilder;"

    aput-object v5, v1, v4

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    invoke-static {v1, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 446
    invoke-static {v2, v0}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 532
    check-cast v0, Ljava/util/Set;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->o:Ljava/util/Set;

    .line 459
    nop

    .line 533
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    .line 460
    const-string v2, "Throwable"

    new-array v1, v7, [Ljava/lang/String;

    const-string v3, "Ljava/lang/String;Ljava/lang/Throwable;ZZ"

    aput-object v3, v1, v6

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v3, v1

    invoke-static {v1, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 533
    check-cast v0, Ljava/util/Set;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->p:Ljava/util/Set;

    new-array v1, v10, [Lkotlin/reflect/l;

    new-instance v0, Lkotlin/d/b/w;

    const-class v2, Lkotlin/reflect/jvm/internal/impl/d/b/i;

    invoke-static {v2}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v2

    const-string v3, "ownerModuleDescriptor"

    const-string v4, "getOwnerModuleDescriptor()Lorg/jetbrains/kotlin/descriptors/ModuleDescriptor;"

    invoke-direct {v0, v2, v3, v4}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v6

    new-instance v0, Lkotlin/d/b/w;

    const-class v2, Lkotlin/reflect/jvm/internal/impl/d/b/i;

    invoke-static {v2}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v2

    const-string v3, "isAdditionalBuiltInsFeatureSupported"

    const-string v4, "isAdditionalBuiltInsFeatureSupported()Z"

    invoke-direct {v0, v2, v3, v4}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v7

    new-instance v0, Lkotlin/d/b/w;

    const-class v2, Lkotlin/reflect/jvm/internal/impl/d/b/i;

    invoke-static {v2}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v2

    const-string v3, "cloneableType"

    const-string v4, "getCloneableType()Lorg/jetbrains/kotlin/types/SimpleType;"

    invoke-direct {v0, v2, v3, v4}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v8

    new-instance v0, Lkotlin/d/b/w;

    const-class v2, Lkotlin/reflect/jvm/internal/impl/d/b/i;

    invoke-static {v2}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v2

    const-string v3, "notConsideredDeprecation"

    const-string v4, "getNotConsideredDeprecation()Lorg/jetbrains/kotlin/descriptors/annotations/AnnotationsImpl;"

    invoke-direct {v0, v2, v3, v4}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v9

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;Lkotlin/d/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            "Lkotlin/reflect/jvm/internal/impl/j/i;",
            "Lkotlin/d/a/a",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            ">;",
            "Lkotlin/d/a/a",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "moduleDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storageManager"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deferredOwnerModuleDescriptor"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isAdditionalBuiltInsFeatureSupported"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->j:Lkotlin/reflect/jvm/internal/impl/b/w;

    .line 60
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/f/a;->a:Lkotlin/reflect/jvm/internal/impl/f/a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->c:Lkotlin/reflect/jvm/internal/impl/f/a;

    .line 62
    invoke-static {p3}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->d:Lkotlin/c;

    .line 63
    invoke-static {p4}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->e:Lkotlin/c;

    .line 65
    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/j/i;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->f:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 66
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$c;

    invoke-direct {v0, p0, p2}, Lkotlin/reflect/jvm/internal/impl/d/b/i$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/i;Lkotlin/reflect/jvm/internal/impl/j/i;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->g:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 73
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/j/i;->b()Lkotlin/reflect/jvm/internal/impl/j/a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->h:Lkotlin/reflect/jvm/internal/impl/j/a;

    .line 76
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$n;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$n;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/i;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->i:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a/e;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
            "+",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;>;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 171
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 173
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->c:Lkotlin/reflect/jvm/internal/impl/f/a;

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/b/f;->a:Lkotlin/reflect/jvm/internal/impl/d/b/f$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/b/f$a;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lkotlin/reflect/jvm/internal/impl/f/a;->b(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/a/l;)Ljava/util/Collection;

    move-result-object v2

    move-object v0, v2

    .line 174
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->e(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_1

    .line 175
    sget-object v6, Lkotlin/reflect/jvm/internal/impl/utils/f;->a:Lkotlin/reflect/jvm/internal/impl/utils/f$b;

    check-cast v2, Ljava/lang/Iterable;

    .line 509
    new-instance v3, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v2, v7}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 510
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 511
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 175
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 171
    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 517
    :goto_1
    return-object v0

    .line 174
    :cond_1
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_1

    .line 512
    :cond_2
    check-cast v3, Ljava/util/List;

    .line 175
    check-cast v3, Ljava/util/Collection;

    invoke-virtual {v6, v3}, Lkotlin/reflect/jvm/internal/impl/utils/f$b;->a(Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/utils/f;

    move-result-object v6

    .line 177
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->c:Lkotlin/reflect/jvm/internal/impl/f/a;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v2, p1}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v7

    .line 179
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->h:Lkotlin/reflect/jvm/internal/impl/j/a;

    move-object v2, v1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v8

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/i$f;

    invoke-direct {v2, v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$f;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    move-object v0, v2

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v3, v8, v0}, Lkotlin/reflect/jvm/internal/impl/j/a;->a(Ljava/lang/Object;Lkotlin/d/a/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 185
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->g()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    .line 187
    const-string v1, "scope"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 188
    nop

    .line 513
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 514
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 189
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v3

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-static {v3, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_4

    move v0, v4

    .line 197
    :goto_3
    if-eqz v0, :cond_3

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 190
    :cond_4
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/ay;->c()Z

    move-result v3

    if-nez v3, :cond_5

    move v0, v4

    goto :goto_3

    :cond_5
    move-object v3, v0

    .line 191
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v0, v4

    goto :goto_3

    .line 193
    :cond_6
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->k()Ljava/util/Collection;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 515
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 194
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/s;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v3

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v3

    invoke-virtual {v6, v3}, Lkotlin/reflect/jvm/internal/impl/utils/f;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v5

    .line 516
    :goto_4
    if-eqz v3, :cond_9

    move v0, v4

    .line 195
    goto :goto_3

    :cond_8
    move v3, v4

    .line 516
    goto :goto_4

    .line 197
    :cond_9
    invoke-direct {p0, v0, v7}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/ak;Z)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v5

    goto :goto_3

    :cond_a
    move v0, v4

    goto :goto_3

    .line 517
    :cond_b
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    move-object v0, v1

    goto/16 :goto_1
.end method

.method public static final synthetic a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->k:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/i;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->j:Lkotlin/reflect/jvm/internal/impl/b/w;

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 264
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/k;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-object v1

    .line 266
    :cond_1
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v2

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$i;->a:Lkotlin/reflect/jvm/internal/impl/d/b/i$i;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->c:Lkotlin/reflect/jvm/internal/impl/f/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->b()Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 269
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->g()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v3

    const-string v0, "javaAnalogueFqName"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->d:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-static {v3, v2, v0}, Lkotlin/reflect/jvm/internal/impl/b/r;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    if-nez v2, :cond_2

    move-object v0, v1

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-object v1, v0

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/b/i;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/d/b/i$b;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 228
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 229
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {p1, v2, v3, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 230
    new-instance v4, Lkotlin/d/b/x$c;

    invoke-direct {v4}, Lkotlin/d/b/x$c;-><init>()V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    iput-object v1, v4, Lkotlin/d/b/x$c;->a:Ljava/lang/Object;

    .line 231
    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$j;

    invoke-direct {v1, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$j;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/i;)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/utils/b$b;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;

    invoke-direct {v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/d/b/i$k;-><init>(Ljava/lang/String;Lkotlin/d/b/x$c;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/utils/b$c;

    invoke-static {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/reflect/jvm/internal/impl/utils/b$c;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "DFS.dfs<ClassDescriptor,\u2026IDERED\n                })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/j/i;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 8

    .prologue
    .line 83
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$d;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->j:Lkotlin/reflect/jvm/internal/impl/b/w;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "java.io"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, p0, v0, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/i$d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    .line 88
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/k/t;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$e;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/i;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/k/t;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    invoke-static {v2}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 90
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/h;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    const-string v2, "Serializable"

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    check-cast v5, Ljava/util/Collection;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/h;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/f;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/al;Z)V

    .line 95
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/h$b;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h$b;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/h;->a(Lkotlin/reflect/jvm/internal/impl/h/e/h;Ljava/util/Set;Lkotlin/reflect/jvm/internal/impl/b/d;)V

    .line 96
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/h;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "mockSerializableClass.defaultType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ak;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 212
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ak;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v1, p1

    .line 213
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/s;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v3, v2, v4}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 215
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->d()Ljava/util/Set;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    const-string v5, "jvmDescriptor"

    invoke-static {v1, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, p2

    if-eqz v0, :cond_1

    move v0, v2

    .line 217
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i$l;->a:Lkotlin/reflect/jvm/internal/impl/d/b/i$l;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/utils/b$b;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/i$m;

    invoke-direct {v2, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$m;-><init>(Lkotlin/reflect/jvm/internal/impl/d/b/i;)V

    check-cast v2, Lkotlin/d/a/b;

    invoke-static {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/utils/b;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/utils/b$b;Lkotlin/d/a/b;)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "DFS.ifAny<CallableMember\u2026lassDescriptor)\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 323
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/l;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/l;->i()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    :goto_0
    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static final synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->l:Ljava/util/Set;

    return-object v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;Lkotlin/reflect/jvm/internal/impl/b/ak;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 3

    .prologue
    .line 204
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-object v2, p1

    .line 205
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 206
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 207
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 208
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->C()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v2

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/aj;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 209
    nop

    .line 204
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 209
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->f()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/b/i;)Lkotlin/reflect/jvm/internal/impl/f/a;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->c:Lkotlin/reflect/jvm/internal/impl/f/a;

    return-object v0
.end method

.method public static final synthetic c()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->m:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/d/b/i;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->g()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic d()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->n:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic e()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->o:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic f()Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->p:Ljava/util/Set;

    return-object v0
.end method

.method private final g()Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->d:Lkotlin/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/w;

    return-object v0
.end method

.method private final h()Z
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->e:Lkotlin/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private final i()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->g:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    return-object v0
.end method

.method private final j()Lkotlin/reflect/jvm/internal/impl/b/a/i;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->i:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/i;

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a/e;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/g;->a:Lkotlin/reflect/jvm/internal/impl/a/g$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/g$a;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->C()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 495
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    .line 112
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->t()I

    move-result v0

    invoke-interface {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/g;->a:Lkotlin/reflect/jvm/internal/impl/a/g$a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/a/g$a;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 496
    :goto_0
    if-eqz v0, :cond_2

    .line 114
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 508
    :goto_1
    return-object v0

    .line 496
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 116
    :cond_2
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->i()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->d:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v1, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->f(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;Lkotlin/reflect/jvm/internal/impl/b/ak;)Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_1

    .line 121
    :cond_3
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->h()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_1

    .line 123
    :cond_4
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/i$h;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i$h;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 125
    nop

    .line 497
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 506
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 505
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 128
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ak;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    if-nez v2, :cond_6

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v3, p2

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/f/c;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/k/ae;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/ae;->f()Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v2

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/ak;->d(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v2

    if-nez v2, :cond_7

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.SimpleFunctionDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 133
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v3

    move-object v2, v3

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-object v4, p2

    .line 134
    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-interface {v2, v4}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 135
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->C()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v4

    invoke-interface {v2, v4}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/aj;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 136
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->b()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 137
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/d/b/ah;

    iget-object v7, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->j:Lkotlin/reflect/jvm/internal/impl/b/w;

    invoke-interface {v7}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v7

    invoke-direct {v4, v7}, Lkotlin/reflect/jvm/internal/impl/d/b/ah;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-interface {v2, v4}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 139
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/d/b/i$b;

    move-result-object v0

    .line 140
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/b/j;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$b;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 156
    :goto_3
    nop

    .line 133
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 156
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->f()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-nez v0, :cond_8

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_8
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    :goto_4
    if-eqz v0, :cond_5

    .line 505
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :pswitch_0
    move-object v0, p2

    .line 143
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/v;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v0, v5

    goto :goto_4

    .line 144
    :cond_9
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->e()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    const-string v2, "setHiddenForResolutionEverywhereBesideSupercalls()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 148
    :pswitch_1
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->j()Lkotlin/reflect/jvm/internal/impl/b/a/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-interface {v2, v0}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    const-string v2, "setAdditionalAnnotations(notConsideredDeprecation)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_2
    move-object v0, v5

    .line 151
    goto :goto_4

    .line 153
    :pswitch_3
    nop

    goto :goto_3

    .line 508
    :cond_a
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    move-object v0, v1

    goto/16 :goto_1

    .line 140
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a/e;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    .line 102
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->a(Lkotlin/reflect/jvm/internal/impl/d/b/i$a;Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/jvm/internal/impl/k/r;

    const/4 v2, 0x0

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->i()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v3, "cloneableType"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    aput-object v0, v1, v2

    const/4 v0, 0x1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->f:Lkotlin/reflect/jvm/internal/impl/k/r;

    aput-object v2, v1, v0

    invoke-static {v1}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 101
    :goto_0
    return-object v0

    .line 103
    :cond_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->f:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_0

    .line 104
    :cond_1
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;Lkotlin/reflect/jvm/internal/impl/b/ak;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "functionDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/ak;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/b/ad;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v3

    invoke-interface {v0, v3}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 528
    :cond_0
    :goto_0
    return v1

    .line 310
    :cond_1
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->h()Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    goto :goto_0

    .line 312
    :cond_2
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v3

    if-eqz v3, :cond_0

    move-object v0, p2

    .line 314
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {v0, v2, v1, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 315
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    move-result-object v3

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/ak;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    const-string v0, "functionDescriptor.name"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->d:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    .line 317
    invoke-virtual {v3, v5, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 318
    nop

    .line 527
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    .line 318
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {v0, v2, v1, v6}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    move v1, v0

    .line 528
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a/e;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->h()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    .line 164
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->a()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->A_()Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_0

    :cond_2
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic c(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public d(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a/e;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->h()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 526
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p1

    .line 275
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 278
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/b/i;->c:Lkotlin/reflect/jvm/internal/impl/f/a;

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/d/b/f;->a:Lkotlin/reflect/jvm/internal/impl/d/b/f$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/d/b/f$a;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lkotlin/reflect/jvm/internal/impl/f/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 280
    const-string v0, "defaultKotlinVersion"

    invoke-static {v6, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v6, v0}, Lkotlin/reflect/jvm/internal/impl/f/c;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/k/ae;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ae;->f()Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v7

    .line 282
    new-instance v8, Lkotlin/reflect/jvm/internal/impl/d/b/i$g;

    invoke-direct {v8, v7}, Lkotlin/reflect/jvm/internal/impl/d/b/i$g;-><init>(Lkotlin/reflect/jvm/internal/impl/k/am;)V

    .line 286
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/f;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 518
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 519
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    .line 292
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/d;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/ay;->c()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 289
    invoke-interface {v6}, Lkotlin/reflect/jvm/internal/impl/b/e;->k()Ljava/util/Collection;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 520
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/d;

    .line 289
    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/l;

    const-string v5, "javaConstructor"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v0

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/b/l;

    invoke-virtual {v8, v4, v5}, Lkotlin/reflect/jvm/internal/impl/d/b/i$g;->a(Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/b/l;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    .line 521
    :goto_2
    if-eqz v4, :cond_7

    move-object v4, v0

    .line 290
    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/l;

    invoke-direct {p0, v4, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->a(Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Z

    move-result v4

    if-nez v4, :cond_7

    move-object v4, v0

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 291
    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 292
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->e()Ljava/util/Set;

    move-result-object v5

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    move-object v4, v1

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/e;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-static {v0, v11, v12, v13}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v11, "javaConstructor.computeJvmDescriptor()"

    invoke-static {v0, v11}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v10, v4, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_2

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 275
    :cond_4
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto/16 :goto_0

    .line 278
    :cond_5
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto/16 :goto_0

    .line 521
    :cond_6
    const/4 v4, 0x1

    goto :goto_2

    .line 292
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 522
    :cond_8
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 293
    nop

    .line 523
    new-instance v0, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 524
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 525
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/d;

    .line 295
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/d;->D()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v4

    move-object v3, v4

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-object v5, p1

    .line 296
    check-cast v5, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-interface {v3, v5}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 297
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v5

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v3, v5}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 298
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->b()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 299
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/k/am;->b()Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v5

    invoke-interface {v3, v5}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 300
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/b/i;->b:Lkotlin/reflect/jvm/internal/impl/d/b/i$a;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/d/b/i$a;->f()Ljava/util/Set;

    move-result-object v8

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a:Lkotlin/reflect/jvm/internal/impl/d/b/ab;

    move-object v5, v1

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/b/e;

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/s;

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v2, v10, v11, v12}, Lkotlin/reflect/jvm/internal/impl/d/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/s;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v10, "javaConstructor.computeJvmDescriptor()"

    invoke-static {v2, v10}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9, v5, v2}, Lkotlin/reflect/jvm/internal/impl/d/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_9

    .line 301
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/d/b/i;->j()Lkotlin/reflect/jvm/internal/impl/b/a/i;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-interface {v3, v2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 304
    :cond_9
    nop

    .line 295
    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/s$a;

    .line 304
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->f()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v2

    if-nez v2, :cond_a

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassConstructorDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/d;

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 526
    :cond_b
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    goto/16 :goto_0
.end method
