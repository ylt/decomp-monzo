.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$c;
.super Lkotlin/d/b/k;
.source "LazyJavaClassMemberScope.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/k;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/e/f;",
        "Ljava/util/Collection",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/b/ak;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/d/b/k;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g$c;->b:Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    .line 268
    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;->b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lkotlin/reflect/e;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/g;

    invoke-static {v0}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "searchMethodsInSupertypesWithoutBuiltinMagic"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "searchMethodsInSupertypesWithoutBuiltinMagic(Lorg/jetbrains/kotlin/name/Name;)Ljava/util/Collection;"

    return-object v0
.end method
