.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;
.super Ljava/lang/Object;
.source "JavaTypeResolver.kt"


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "java.lang.Class"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 359
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;

    invoke-direct {v0, p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    .line 370
    return-object v0
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_0

    .line 356
    const/4 p1, 0x1

    :cond_0
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 357
    const/4 p2, 0x0

    :cond_1
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_2

    .line 358
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    :goto_0
    invoke-static {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/d/a/a/o;ZZLkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v0, p3

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZZZ)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;

    invoke-direct {v0, p0, p1, p3, p2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;ZZZ)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;

    .line 376
    return-object v0
.end method

.method public static final synthetic a()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 2

    .prologue
    const-string v0, "typeParameter"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attr"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/a;->a()Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/a/o;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/o;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/z;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 297
    :goto_0
    return-object v0

    .line 300
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/y;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/k/y;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            "Lkotlin/d/a/a",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/r;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 390
    if-ne p0, p1, :cond_0

    invoke-interface {p2}, Lkotlin/d/a/a;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 410
    :goto_0
    return-object v0

    .line 392
    :cond_0
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 394
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    instance-of v1, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_1

    .line 395
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_0

    .line 398
    :cond_1
    if-eqz p1, :cond_2

    .line 399
    :goto_1
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.TypeParameterDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object p1, p0

    .line 398
    goto :goto_1

    .line 399
    :cond_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 401
    :goto_2
    invoke-static {v0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_6

    .line 402
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 403
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    instance-of v1, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_4

    .line 404
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_0

    .line 407
    :cond_4
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.TypeParameterDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    goto :goto_2

    .line 410
    :cond_6
    invoke-interface {p2}, Lkotlin/d/a/a;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_0
.end method

.method public static synthetic a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/a;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 387
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-object p1, v0

    :cond_0
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_1

    .line 388
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d$b;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;)V

    check-cast v0, Lkotlin/d/a/a;

    :goto_0
    invoke-static {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/d;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Z
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 352
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/i;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "JETBRAINS_NOT_NULL_ANNOTATION"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
