.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;
.super Lkotlin/d/b/m;
.source "LazyJavaScope.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/e/f;",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/b/ag;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 240
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/d/a/f/n;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/n;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 242
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;Lkotlin/reflect/jvm/internal/impl/d/a/f/n;)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    :cond_0
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V

    .line 247
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$g;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->h()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->n(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 250
    :goto_0
    return-object v0

    :cond_1
    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/g/m;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
