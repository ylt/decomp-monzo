.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;
.super Lkotlin/reflect/jvm/internal/impl/k/m;
.source "RawType.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/u;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V
    .locals 3

    .prologue
    const-string v0, "lowerBound"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "upperBound"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/k/m;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    .line 34
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a:Lkotlin/reflect/jvm/internal/impl/k/a/c;

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v1, p2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/a/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Lower bound "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of a flexible type must be a subtype of the upper bound "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 34
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public J_()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/g/c;Lkotlin/reflect/jvm/internal/impl/g/h;)Ljava/lang/String;
    .locals 15

    .prologue
    const-string v1, "renderer"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "options"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$a;

    .line 57
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;-><init>(Lkotlin/reflect/jvm/internal/impl/g/c;)V

    .line 59
    sget-object v13, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$c;

    .line 64
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v14

    .line 65
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v12

    .line 67
    invoke-interface/range {p2 .. p2}, Lkotlin/reflect/jvm/internal/impl/g/h;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "raw ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 81
    :goto_0
    return-object v1

    .line 70
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v12, v1}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/a/l;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 72
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v1, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v10

    .line 73
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v2, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$b;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v11

    move-object v1, v10

    .line 74
    check-cast v1, Ljava/lang/Iterable;

    .line 171
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 172
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 173
    check-cast v1, Ljava/lang/String;

    .line 74
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "(raw) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 174
    :cond_2
    check-cast v2, Ljava/util/List;

    move-object v1, v2

    check-cast v1, Ljava/lang/Iterable;

    const-string v2, ", "

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    .line 74
    invoke-static/range {v1 .. v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 76
    check-cast v10, Ljava/lang/Iterable;

    move-object v1, v11

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v10, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 175
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lkotlin/h;

    .line 76
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$a;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$a;

    invoke-virtual {v2}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x0

    .line 176
    :goto_2
    if-eqz v1, :cond_5

    .line 77
    invoke-virtual {v13, v12, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$c;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    :goto_3
    invoke-virtual {v13, v14, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j$c;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-static {v2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object v1, v2

    goto/16 :goto_0

    .line 176
    :cond_4
    const/4 v1, 0x1

    goto :goto_2

    :cond_5
    move-object v1, v12

    .line 78
    goto :goto_3

    .line 81
    :cond_6
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1, v3}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/a/l;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;
    .locals 3

    .prologue
    const-string v0, "newAnnotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1, p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-virtual {v2, p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->c(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    return-object v0
.end method

.method public a(Z)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1, p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->h()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-virtual {v2, p1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_1

    .line 45
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/b/h;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const-string v1, "classDescriptor.getMemberScope(RawSubstitution)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 44
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Incorrect classifier: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method

.method public synthetic b(Z)Lkotlin/reflect/jvm/internal/impl/k/ao;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;->a(Z)Lkotlin/reflect/jvm/internal/impl/d/a/c/b/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ao;

    return-object v0
.end method
