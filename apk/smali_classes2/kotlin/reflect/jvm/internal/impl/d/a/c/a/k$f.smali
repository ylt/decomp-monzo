.class final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;
.super Lkotlin/d/b/m;
.source "LazyJavaScope.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/e/f;",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/b/ak;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 79
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->i()Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;

    .line 80
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;)Lkotlin/reflect/jvm/internal/impl/d/a/b/d;

    move-result-object v2

    .line 81
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Lkotlin/reflect/jvm/internal/impl/d/a/b/d;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->g()Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    move-result-object v5

    move-object v1, v2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ak;

    invoke-interface {v5, v0, v1}, Lkotlin/reflect/jvm/internal/impl/d/a/a/h;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/q;Lkotlin/reflect/jvm/internal/impl/b/ak;)V

    .line 84
    invoke-virtual {v3, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 85
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/f/q;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v3

    .line 86
    check-cast v0, Ljava/util/Collection;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->j()Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->d()Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->i()Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

    move-result-object v1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v1, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/a/m;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k$f;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;

    move-object v0, v3

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v1, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/k;->a(Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 92
    check-cast v3, Ljava/util/Collection;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/d/a/g/m;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
