.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;
.super Lkotlin/reflect/jvm/internal/impl/b/b/w;
.source "LazyJavaPackageFragment.kt"


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

.field private final d:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

.field private final g:Lkotlin/reflect/jvm/internal/impl/d/a/f/t;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "binaryClasses"

    const-string v5, "getBinaryClasses$kotlin_core()Ljava/util/Map;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "partToFacade"

    const-string v5, "getPartToFacade()Ljava/util/HashMap;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/t;)V
    .locals 3

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jPackage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/d/a/f/t;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    .line 35
    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/w;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->g:Lkotlin/reflect/jvm/internal/impl/d/a/f/t;

    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$a;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 43
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->g:Lkotlin/reflect/jvm/internal/impl/d/a/f/t;

    invoke-direct {v0, v1, v2, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/f;Lkotlin/reflect/jvm/internal/impl/d/a/f/t;Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;Ljava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 55
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/f;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h$b;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->e:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/c/f;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f:Lkotlin/reflect/jvm/internal/impl/d/a/c/f;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/f/t;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->g:Lkotlin/reflect/jvm/internal/impl/d/a/f/t;

    return-object v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    const-string v0, "jClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;->c()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/j;->a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkotlin/reflect/jvm/internal/impl/d/b/u;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->c:Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Lazy Java package fragment: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/d/b/v;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/b/v;-><init>(Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    return-object v0
.end method

.method public synthetic x_()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c/a/h;->h()Lkotlin/reflect/jvm/internal/impl/d/a/c/a/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method
