.class public final Lkotlin/reflect/jvm/internal/impl/d/a/c/b;
.super Ljava/lang/Object;
.source "context.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/j/i;

.field private final b:Lkotlin/reflect/jvm/internal/impl/d/a/e;

.field private final c:Lkotlin/reflect/jvm/internal/impl/d/b/t;

.field private final d:Lkotlin/reflect/jvm/internal/impl/d/b/e;

.field private final e:Lkotlin/reflect/jvm/internal/impl/d/a/a/b;

.field private final f:Lkotlin/reflect/jvm/internal/impl/d/a/a/n;

.field private final g:Lkotlin/reflect/jvm/internal/impl/i/b/r;

.field private final h:Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

.field private final i:Lkotlin/reflect/jvm/internal/impl/d/a/a/g;

.field private final j:Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

.field private final k:Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

.field private final l:Lkotlin/reflect/jvm/internal/impl/d/a/c/h;

.field private final m:Lkotlin/reflect/jvm/internal/impl/b/ab;

.field private final n:Lkotlin/reflect/jvm/internal/impl/b/ao;

.field private final o:Lkotlin/reflect/jvm/internal/impl/c/a/c;

.field private final p:Lkotlin/reflect/jvm/internal/impl/b/w;

.field private final q:Lkotlin/reflect/jvm/internal/impl/a/n;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/d/a/e;Lkotlin/reflect/jvm/internal/impl/d/b/t;Lkotlin/reflect/jvm/internal/impl/d/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/a/b;Lkotlin/reflect/jvm/internal/impl/d/a/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/r;Lkotlin/reflect/jvm/internal/impl/d/a/a/h;Lkotlin/reflect/jvm/internal/impl/d/a/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/a/m;Lkotlin/reflect/jvm/internal/impl/d/a/e/b;Lkotlin/reflect/jvm/internal/impl/d/a/c/h;Lkotlin/reflect/jvm/internal/impl/b/ab;Lkotlin/reflect/jvm/internal/impl/b/ao;Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/a/n;)V
    .locals 2

    .prologue
    const-string v1, "storageManager"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "finder"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "kotlinClassFinder"

    invoke-static {p3, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "deserializedDescriptorResolver"

    invoke-static {p4, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "externalAnnotationResolver"

    invoke-static {p5, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "signaturePropagator"

    invoke-static {p6, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "errorReporter"

    invoke-static {p7, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "javaResolverCache"

    invoke-static {p8, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "javaPropertyInitializerEvaluator"

    invoke-static {p9, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "samConversionResolver"

    invoke-static {p10, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "sourceElementFactory"

    invoke-static {p11, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "moduleClassResolver"

    invoke-static {p12, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "packageMapper"

    invoke-static {p13, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "supertypeLoopChecker"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "lookupTracker"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "module"

    move-object/from16 v0, p16

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "reflectionTypes"

    move-object/from16 v0, p17

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->a:Lkotlin/reflect/jvm/internal/impl/j/i;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/e;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->d:Lkotlin/reflect/jvm/internal/impl/d/b/e;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/b;

    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/n;

    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->g:Lkotlin/reflect/jvm/internal/impl/i/b/r;

    iput-object p8, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->h:Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    iput-object p9, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->i:Lkotlin/reflect/jvm/internal/impl/d/a/a/g;

    iput-object p10, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j:Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

    iput-object p11, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->k:Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    iput-object p12, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->l:Lkotlin/reflect/jvm/internal/impl/d/a/c/h;

    iput-object p13, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->m:Lkotlin/reflect/jvm/internal/impl/b/ab;

    move-object/from16 v0, p14

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->n:Lkotlin/reflect/jvm/internal/impl/b/ao;

    move-object/from16 v0, p15

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->o:Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-object/from16 v0, p16

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->p:Lkotlin/reflect/jvm/internal/impl/b/w;

    move-object/from16 v0, p17

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->q:Lkotlin/reflect/jvm/internal/impl/a/n;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/d/a/a/h;)Lkotlin/reflect/jvm/internal/impl/d/a/c/b;
    .locals 19

    .prologue
    const-string v1, "javaResolverCache"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->a:Lkotlin/reflect/jvm/internal/impl/j/i;

    move-object/from16 v0, p0

    iget-object v3, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/e;

    move-object/from16 v0, p0

    iget-object v4, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    move-object/from16 v0, p0

    iget-object v5, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->d:Lkotlin/reflect/jvm/internal/impl/d/b/e;

    move-object/from16 v0, p0

    iget-object v6, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->e:Lkotlin/reflect/jvm/internal/impl/d/a/a/b;

    move-object/from16 v0, p0

    iget-object v7, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/n;

    move-object/from16 v0, p0

    iget-object v8, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->g:Lkotlin/reflect/jvm/internal/impl/i/b/r;

    move-object/from16 v0, p0

    iget-object v10, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->i:Lkotlin/reflect/jvm/internal/impl/d/a/a/g;

    move-object/from16 v0, p0

    iget-object v11, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j:Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

    move-object/from16 v0, p0

    iget-object v12, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->k:Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    move-object/from16 v0, p0

    iget-object v13, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->l:Lkotlin/reflect/jvm/internal/impl/d/a/c/h;

    move-object/from16 v0, p0

    iget-object v14, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->m:Lkotlin/reflect/jvm/internal/impl/b/ab;

    move-object/from16 v0, p0

    iget-object v15, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->n:Lkotlin/reflect/jvm/internal/impl/b/ao;

    move-object/from16 v0, p0

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->o:Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->p:Lkotlin/reflect/jvm/internal/impl/b/w;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->q:Lkotlin/reflect/jvm/internal/impl/a/n;

    move-object/from16 v18, v0

    move-object/from16 v9, p1

    invoke-direct/range {v1 .. v18}, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/d/a/e;Lkotlin/reflect/jvm/internal/impl/d/b/t;Lkotlin/reflect/jvm/internal/impl/d/b/e;Lkotlin/reflect/jvm/internal/impl/d/a/a/b;Lkotlin/reflect/jvm/internal/impl/d/a/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/r;Lkotlin/reflect/jvm/internal/impl/d/a/a/h;Lkotlin/reflect/jvm/internal/impl/d/a/a/g;Lkotlin/reflect/jvm/internal/impl/d/a/a/m;Lkotlin/reflect/jvm/internal/impl/d/a/e/b;Lkotlin/reflect/jvm/internal/impl/d/a/c/h;Lkotlin/reflect/jvm/internal/impl/b/ab;Lkotlin/reflect/jvm/internal/impl/b/ao;Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/a/n;)V

    .line 60
    return-object v1
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/j/i;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->a:Lkotlin/reflect/jvm/internal/impl/j/i;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/d/a/e;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->b:Lkotlin/reflect/jvm/internal/impl/d/a/e;

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/d/b/t;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->c:Lkotlin/reflect/jvm/internal/impl/d/b/t;

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/d/b/e;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->d:Lkotlin/reflect/jvm/internal/impl/d/b/e;

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/d/a/a/n;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->f:Lkotlin/reflect/jvm/internal/impl/d/a/a/n;

    return-object v0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/i/b/r;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->g:Lkotlin/reflect/jvm/internal/impl/i/b/r;

    return-object v0
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/d/a/a/h;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->h:Lkotlin/reflect/jvm/internal/impl/d/a/a/h;

    return-object v0
.end method

.method public final h()Lkotlin/reflect/jvm/internal/impl/d/a/a/g;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->i:Lkotlin/reflect/jvm/internal/impl/d/a/a/g;

    return-object v0
.end method

.method public final i()Lkotlin/reflect/jvm/internal/impl/d/a/a/m;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->j:Lkotlin/reflect/jvm/internal/impl/d/a/a/m;

    return-object v0
.end method

.method public final j()Lkotlin/reflect/jvm/internal/impl/d/a/e/b;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->k:Lkotlin/reflect/jvm/internal/impl/d/a/e/b;

    return-object v0
.end method

.method public final k()Lkotlin/reflect/jvm/internal/impl/d/a/c/h;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->l:Lkotlin/reflect/jvm/internal/impl/d/a/c/h;

    return-object v0
.end method

.method public final l()Lkotlin/reflect/jvm/internal/impl/b/ab;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->m:Lkotlin/reflect/jvm/internal/impl/b/ab;

    return-object v0
.end method

.method public final m()Lkotlin/reflect/jvm/internal/impl/b/ao;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->n:Lkotlin/reflect/jvm/internal/impl/b/ao;

    return-object v0
.end method

.method public final n()Lkotlin/reflect/jvm/internal/impl/c/a/c;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->o:Lkotlin/reflect/jvm/internal/impl/c/a/c;

    return-object v0
.end method

.method public final o()Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->p:Lkotlin/reflect/jvm/internal/impl/b/w;

    return-object v0
.end method

.method public final p()Lkotlin/reflect/jvm/internal/impl/a/n;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/c/b;->q:Lkotlin/reflect/jvm/internal/impl/a/n;

    return-object v0
.end method
