.class public final Lkotlin/reflect/jvm/internal/impl/d/b/b/d;
.super Ljava/lang/Object;
.source "ReflectKotlinClassFinder.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/d/b/t;


# instance fields
.field private final a:Ljava/lang/ClassLoader;


# direct methods
.method public constructor <init>(Ljava/lang/ClassLoader;)V
    .locals 1

    .prologue
    const-string v0, "classLoader"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/d;->a:Ljava/lang/ClassLoader;

    return-void
.end method

.method private final a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/u;
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/b/b/d;->a:Ljava/lang/ClassLoader;

    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/a/d/b;->a(Ljava/lang/ClassLoader;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Class;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;->a:Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/c$a;->a(Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/d/b/b/c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/b/c;

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/d/b/u;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/d/a/f/g;)Lkotlin/reflect/jvm/internal/impl/d/b/u;
    .locals 1

    .prologue
    const-string v0, "javaClass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/d/a/f/g;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/d;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/d/b/u;
    .locals 1

    .prologue
    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/d/b/b/e;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/d/b/b/d;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/d/b/u;

    move-result-object v0

    return-object v0
.end method
