.class public final Lkotlin/reflect/jvm/internal/impl/d/a/m;
.super Ljava/lang/Object;
.source "propertiesConventionUtil.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v0, "methodName"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    const-string v1, "get"

    const/16 v4, 0xc

    move-object v0, p0

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "is"

    const/16 v4, 0x8

    move-object v0, p0

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    goto :goto_0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;ZLjava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 35
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/f;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object p0, v0

    .line 49
    :cond_0
    :goto_0
    return-object p0

    .line 36
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/f;->b()Ljava/lang/String;

    move-result-object v3

    .line 37
    const/4 v4, 0x2

    invoke-static {v3, p1, v1, v4, v0}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object p0, v0

    goto :goto_0

    .line 38
    :cond_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v4, v5, :cond_3

    move-object p0, v0

    goto :goto_0

    .line 39
    :cond_3
    const/16 v4, 0x61

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-gt v4, v5, :cond_4

    const/16 v4, 0x7a

    if-gt v5, v4, :cond_4

    move v1, v2

    :cond_4
    if-eqz v1, :cond_5

    move-object p0, v0

    goto :goto_0

    .line 41
    :cond_5
    if-eqz p3, :cond_7

    .line 42
    sget-boolean v0, Lkotlin/p;->a:Z

    if-eqz v0, :cond_6

    if-nez p2, :cond_6

    const-string v1, "Assertion failed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 43
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v3, p1}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object p0

    goto :goto_0

    .line 46
    :cond_7
    if-eqz p2, :cond_0

    .line 47
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v3, p1}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/l/a/a;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 48
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    move-object p0, v0

    goto :goto_0

    .line 49
    :cond_8
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object p0

    goto :goto_0
.end method

.method static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 34
    const/4 p2, 0x1

    :cond_0
    and-int/lit8 v0, p4, 0x8

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-static {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;ZLjava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, p3

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/e/f;Z)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const-string v0, "methodName"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    const-string v1, "set"

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const-string v3, "is"

    :goto_0
    const/4 v4, 0x4

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v3, v5

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "methodName"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {p0, v2}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Z)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p0, v3}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Z)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->h(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/h;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 59
    :cond_0
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/d/a/h;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/d/a/m;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 63
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a:Lkotlin/reflect/jvm/internal/impl/d/a/c;

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/d/a/c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
