.class final Lkotlin/reflect/jvm/internal/impl/d/a/g/h;
.super Ljava/lang/Object;
.source "signatureEnhancement.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/k/r;

.field private final b:Z


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/k/r;Z)V
    .locals 1

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    iput-boolean p2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    if-eqz v2, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    iget-object v3, p1, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b:Z

    iget-boolean v3, p1, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartEnhancementResult(type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->a:Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", wereChanges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/d/a/g/h;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
