.class public final Lkotlin/reflect/jvm/internal/impl/g/b$c;
.super Ljava/lang/Object;
.source "ClassifierNamePolicy.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/g/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/g/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/g/b$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/b$c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/g/b$c;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/g/b$c;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/g/b$c;->a:Lkotlin/reflect/jvm/internal/impl/g/b$c;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/m;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    .line 67
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/h;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/g/b$c;->a(Lkotlin/reflect/jvm/internal/impl/b/h;)Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    .line 68
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->b()Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/g/o;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/h;)Ljava/lang/String;
    .locals 3

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/h;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/g/o;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v0

    .line 59
    instance-of v1, p1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-object v0

    .line 62
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/h;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    const-string v2, "descriptor.containingDeclaration"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/b$c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Ljava/lang/String;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/g/c;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "classifier"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderer"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/g/b$c;->a(Lkotlin/reflect/jvm/internal/impl/b/h;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
