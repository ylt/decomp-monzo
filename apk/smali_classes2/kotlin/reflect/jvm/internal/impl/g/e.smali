.class public final Lkotlin/reflect/jvm/internal/impl/g/e;
.super Lkotlin/reflect/jvm/internal/impl/g/c;
.source "DescriptorRendererImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/g/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/g/e$a;
    }
.end annotation


# static fields
.field static final synthetic j:[Lkotlin/reflect/l;


# instance fields
.field private final k:Lkotlin/c;

.field private final l:Lkotlin/c;

.field private final m:Lkotlin/reflect/jvm/internal/impl/g/i;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/g/e;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "functionTypeAnnotationsRenderer"

    const-string v5, "getFunctionTypeAnnotationsRenderer()Lorg/jetbrains/kotlin/renderer/DescriptorRendererImpl;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/g/e;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "functionTypeParameterTypesRenderer"

    const-string v5, "getFunctionTypeParameterTypesRenderer()Lorg/jetbrains/kotlin/renderer/DescriptorRenderer;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/e;->j:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/g/i;)V
    .locals 2

    .prologue
    const-string v0, "options"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/c;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    .line 46
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->a()Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    const-string v1, "Assertion failed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 49
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/e$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/g/e$b;-><init>(Lkotlin/reflect/jvm/internal/impl/g/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->k:Lkotlin/c;

    .line 55
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/e$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/g/e$c;-><init>(Lkotlin/reflect/jvm/internal/impl/g/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->l:Lkotlin/c;

    return-void
.end method

.method private final M()Lkotlin/reflect/jvm/internal/impl/g/e;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->k:Lkotlin/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/e;->j:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/e;

    return-object v0
.end method

.method private final N()Lkotlin/reflect/jvm/internal/impl/g/c;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->l:Lkotlin/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/e;->j:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/c;

    return-object v0
.end method

.method private final O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string v0, "<"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final P()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const-string v0, ">"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final Q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->C()Lkotlin/reflect/jvm/internal/impl/g/n;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/f;->c:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/n;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 82
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 81
    :pswitch_0
    const-string v0, "->"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 82
    :pswitch_1
    const-string v0, "&rarr;"

    goto :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1096
    invoke-static {p1, p2, v2, v3, v1}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p3, p4, v2, v3, v1}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1097
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez p1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1098
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez p3, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast p3, Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1099
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1101
    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1107
    :goto_0
    return-object v0

    .line 1103
    :cond_2
    invoke-direct {p0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1104
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 1107
    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/h/b/f;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 449
    .line 450
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/h/b/b;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/h/b/b;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/b/b;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1136
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1137
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1138
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    .line 450
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/h/b/f;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1139
    :cond_0
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    const-string v1, ", "

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "{"

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "}"

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/16 v7, 0x38

    move-object v6, v5

    move-object v8, v5

    .line 450
    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 449
    :goto_1
    return-object v0

    .line 451
    :cond_1
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/h/b/a;

    if-eqz v0, :cond_2

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/h/b/a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/b/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    const/4 v1, 0x2

    invoke-static {p0, v0, v5, v1, v5}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/g/c;Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "@"

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 452
    :cond_2
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/h/b/o;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/h/b/o;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/b/o;->b()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::class"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 453
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/b/f;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/a/c;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v6, 0xa

    .line 431
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->b()Ljava/util/Map;

    move-result-object v4

    .line 432
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/an;->h(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 433
    :goto_0
    if-eqz v0, :cond_4

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->o()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Ljava/lang/Iterable;

    .line 1122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 1123
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 434
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 432
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1124
    :cond_2
    check-cast v1, Ljava/util/List;

    :goto_2
    move-object v0, v1

    .line 436
    check-cast v0, Ljava/lang/Iterable;

    .line 1125
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 1126
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 436
    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 435
    :cond_4
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v1

    goto :goto_2

    .line 436
    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    .line 1127
    :cond_6
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 436
    nop

    .line 1128
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v2, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1129
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1130
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 437
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " = ..."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1131
    :cond_7
    check-cast v0, Ljava/util/List;

    .line 439
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 440
    nop

    .line 1132
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v2, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 1133
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1134
    check-cast v2, Ljava/util/Map$Entry;

    .line 441
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v4

    .line 442
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    const-string v6, "entry.value"

    invoke-static {v2, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    invoke-direct {p0, v2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/h/b/f;)Ljava/lang/String;

    move-result-object v2

    .line 443
    :goto_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 442
    :cond_8
    const-string v2, "..."

    goto :goto_7

    .line 1135
    :cond_9
    check-cast v3, Ljava/util/List;

    .line 445
    check-cast v0, Ljava/util/Collection;

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v0, v3}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->j(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    const/16 v1, 0x20

    .line 1089
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 1090
    if-eqz v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    if-eq v0, v1, :cond_1

    .line 1091
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1093
    :cond_1
    return-void
.end method

.method private final a(Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 294
    check-cast p2, Ljava/lang/Iterable;

    .line 1118
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1119
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1120
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 295
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 296
    const-string v1, "*"

    .line 301
    :goto_1
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 299
    :cond_0
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v5, "it.type"

    invoke-static {v2, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v2

    .line 300
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v1, v2

    goto :goto_1

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ah;->b()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1121
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    move-object v1, p1

    check-cast v1, Ljava/lang/Appendable;

    const-string v2, ", "

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const/16 v8, 0x7c

    move-object v4, v3

    move-object v6, v3

    move-object v7, v3

    move-object v9, v3

    .line 302
    invoke-static/range {v0 .. v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/Appendable;

    .line 303
    return-void
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V
    .locals 5

    .prologue
    .line 390
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->d:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 406
    :cond_0
    return-void

    .line 392
    :cond_1
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/k/r;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->h()Ljava/util/Set;

    move-result-object v0

    move-object v1, v0

    .line 398
    :goto_0
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/a/a;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->c()Ljava/util/List;

    move-result-object v0

    .line 399
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/g;->c()Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v3

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/g;->d()Lkotlin/reflect/jvm/internal/impl/b/a/e;

    move-result-object v4

    .line 400
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->g()Ljava/util/Set;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 400
    :cond_4
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 402
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->e(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 403
    invoke-virtual {p0, v3, v4}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/ae;)V
    .locals 2

    .prologue
    .line 271
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/b/ae;->c()Lkotlin/reflect/jvm/internal/impl/b/ae;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ae;

    .line 272
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/ae;)V

    .line 273
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 274
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/b/ae;->a()Lkotlin/reflect/jvm/internal/impl/b/i;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/i;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "possiblyInnerType.classifierDescriptor.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 271
    check-cast v0, Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    .line 277
    :goto_0
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/b/ae;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    return-void

    .line 275
    :cond_0
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/b/ae;->a()Lkotlin/reflect/jvm/internal/impl/b/i;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/i;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    const-string v1, "possiblyInnerType.classi\u2026escriptor.typeConstructor"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/m;)V
    .locals 3

    .prologue
    .line 373
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-nez v0, :cond_0

    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/ac;

    if-eqz v0, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/w;

    if-eqz v0, :cond_2

    .line 377
    const-string v0, " is a module"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 381
    :cond_2
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 382
    if-eqz v0, :cond_0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/w;

    if-nez v1, :cond_0

    .line 383
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "defined in"

    invoke-virtual {p0, v2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    .line 385
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "root package"

    :goto_1
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    const-string v1, "fqName"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/a;)V
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->C()Lkotlin/reflect/jvm/internal/impl/g/n;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/n;->b:Lkotlin/reflect/jvm/internal/impl/g/n;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "<font color=\"808080\"><i>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :cond_0
    const-string v0, " /* = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/a;->f()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 156
    const-string v0, " */"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->C()Lkotlin/reflect/jvm/internal/impl/g/n;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/n;->b:Lkotlin/reflect/jvm/internal/impl/g/n;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    const-string v0, "</i></font>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    :cond_1
    return-void
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/a;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/a;

    .line 138
    if-eqz v0, :cond_2

    .line 140
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/a;->g()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 141
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->x()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/a;)V

    .line 148
    :cond_1
    :goto_0
    return-void

    .line 147
    :cond_2
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_0
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ad;)V
    .locals 1

    .prologue
    .line 260
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/b/ar;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/ae;

    move-result-object v0

    .line 261
    if-nez v0, :cond_0

    .line 262
    invoke-virtual {p0, p3}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :goto_0
    return-void

    .line 267
    :cond_0
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/ae;)V

    goto :goto_0
.end method

.method private final a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/v;)V
    .locals 2

    .prologue
    .line 175
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/an;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/an;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    :cond_0
    const-string v0, "???"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    :goto_0
    return-void

    :cond_1
    move-object v0, p2

    .line 179
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 180
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->E()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 181
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.types.ErrorUtils.UninferredParameterTypeConstructor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/l$e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/l$e;->a()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "(type.constructor as Uni\u2026escriptor.name.toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 184
    :cond_3
    const-string v0, "???"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 189
    :cond_4
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/v;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 190
    check-cast p2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_0

    :cond_5
    move-object v0, p2

    .line 193
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 194
    check-cast p2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_0

    .line 197
    :cond_6
    check-cast p2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    goto :goto_0
.end method

.method private final a(Ljava/util/Collection;ZLjava/lang/StringBuilder;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;Z",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 769
    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->i(Z)Z

    move-result v1

    .line 770
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    .line 771
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->G()Lkotlin/reflect/jvm/internal/impl/g/c$j;

    move-result-object v0

    invoke-interface {v0, v2, p3}, Lkotlin/reflect/jvm/internal/impl/g/c$j;->a(ILjava/lang/StringBuilder;)V

    .line 772
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/a/x;

    invoke-virtual {v0}, Lkotlin/a/x;->c()I

    move-result v4

    invoke-virtual {v0}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 773
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->G()Lkotlin/reflect/jvm/internal/impl/g/c$j;

    move-result-object v5

    invoke-interface {v5, v0, v4, v2, p3}, Lkotlin/reflect/jvm/internal/impl/g/c$j;->a(Lkotlin/reflect/jvm/internal/impl/b/at;IILjava/lang/StringBuilder;)V

    .line 774
    const/4 v5, 0x0

    invoke-direct {p0, v0, v1, p3, v5}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/at;ZLjava/lang/StringBuilder;Z)V

    .line 775
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->G()Lkotlin/reflect/jvm/internal/impl/g/c$j;

    move-result-object v5

    invoke-interface {v5, v0, v4, v2, p3}, Lkotlin/reflect/jvm/internal/impl/g/c$j;->b(Lkotlin/reflect/jvm/internal/impl/b/at;IILjava/lang/StringBuilder;)V

    goto :goto_0

    .line 777
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->G()Lkotlin/reflect/jvm/internal/impl/g/c$j;

    move-result-object v0

    invoke-interface {v0, v2, p3}, Lkotlin/reflect/jvm/internal/impl/g/c$j;->b(ILjava/lang/StringBuilder;)V

    .line 778
    return-void
.end method

.method private final a(Ljava/util/List;Ljava/lang/StringBuilder;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 748
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    :cond_0
    :goto_0
    return-void

    .line 750
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 752
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 753
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    const/4 v4, 0x1

    .line 754
    invoke-static {v1, v4}, Lkotlin/a/m;->b(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 755
    nop

    .line 1144
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v1, v2

    .line 1145
    check-cast v1, Ljava/util/Collection;

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 755
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v9

    const-string v10, "typeParameter.name"

    invoke-static {v9, v10}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "it"

    invoke-static {v4, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    move-object v0, v2

    .line 1146
    check-cast v0, Ljava/util/Collection;

    goto :goto_1

    .line 758
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 759
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "where"

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 760
    check-cast v0, Ljava/lang/Iterable;

    move-object v1, p2

    check-cast v1, Ljava/lang/Appendable;

    const-string v2, ", "

    check-cast v2, Ljava/lang/CharSequence;

    const/16 v8, 0x7c

    move-object v4, v3

    move-object v6, v3

    move-object v7, v3

    move-object v9, v3

    invoke-static/range {v0 .. v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/Appendable;

    goto/16 :goto_0
.end method

.method private final a(Ljava/util/List;Ljava/lang/StringBuilder;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 634
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 637
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->O()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    invoke-direct {p0, p2, p1}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 639
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->P()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 640
    if-eqz p3, :cond_0

    .line 641
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 703
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 705
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    .line 706
    if-eqz v0, :cond_0

    .line 707
    const-string v1, " on "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v2, "receiver.type"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ac;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 982
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ac;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "package"

    invoke-direct {p0, v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 983
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 984
    const-string v0, " in context of "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 985
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ac;->e()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 987
    :cond_0
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/af;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 1007
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    .line 1008
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 545
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    const-string v0, "lateinit "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    :cond_0
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ap;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 889
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 890
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ap;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    const-string v1, "typeAlias.visibility"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Ljava/lang/StringBuilder;)V

    .line 891
    const-string v0, "typealias"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, p1

    .line 892
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 894
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ap;->y()Ljava/util/List;

    move-result-object v0

    const-string v1, "typeAlias.declaredTypeParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;Z)V

    move-object v0, p1

    .line 895
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/i;Ljava/lang/StringBuilder;)V

    .line 897
    const-string v0, " = "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ap;->a()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 898
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/aq;Ljava/lang/StringBuilder;Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 582
    if-eqz p3, :cond_0

    .line 583
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->O()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 587
    const-string v0, "/*"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*/ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 590
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 591
    const-string v0, "reified"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/ap;->b()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 594
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_8

    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    .line 595
    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    move-object v0, p1

    .line 598
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    move-object v0, p1

    .line 600
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 601
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 602
    if-le v0, v2, :cond_4

    if-eqz p3, :cond_5

    :cond_4
    if-ne v0, v2, :cond_9

    .line 603
    :cond_5
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 604
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->j(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 605
    const-string v1, " : "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "upperBound"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 628
    :cond_6
    if-eqz p3, :cond_7

    .line 629
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->P()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 631
    :cond_7
    return-void

    :cond_8
    move v0, v3

    .line 594
    goto :goto_0

    .line 608
    :cond_9
    if-eqz p3, :cond_6

    .line 610
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 611
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->j(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_a

    move v0, v1

    :goto_2
    move v1, v0

    .line 610
    goto :goto_1

    .line 614
    :cond_a
    if-eqz v1, :cond_b

    .line 615
    const-string v1, " : "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    :goto_3
    const-string v1, "upperBound"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v3

    .line 621
    goto :goto_2

    .line 618
    :cond_b
    const-string v1, " & "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/at;ZLjava/lang/StringBuilder;Z)V
    .locals 2

    .prologue
    .line 790
    if-eqz p4, :cond_0

    .line 791
    const-string v0, "value-parameter"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 795
    const-string v0, "/*"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/at;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*/ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    move-object v0, p1

    .line 798
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p3, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 800
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/at;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 801
    const-string v0, "crossinline "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 804
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/at;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 805
    const-string v0, "noinline "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    move-object v0, p1

    .line 808
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/av;

    invoke-direct {p0, v0, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/av;ZLjava/lang/StringBuilder;Z)V

    .line 810
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->w()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/at;->l()Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_6

    const/4 v0, 0x1

    .line 811
    :goto_1
    if-eqz v0, :cond_4

    .line 812
    const-string v0, " = ..."

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 814
    :cond_4
    return-void

    .line 810
    :cond_5
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/at;)Z

    move-result v0

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/av;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 817
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-nez v0, :cond_0

    .line 818
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/av;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "var"

    :goto_0
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    :cond_0
    return-void

    .line 818
    :cond_1
    const-string v0, "val"

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/av;ZLjava/lang/StringBuilder;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 823
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/av;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    .line 825
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-nez v0, :cond_6

    move-object v0, v1

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->m()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    .line 826
    :goto_1
    if-eqz v2, :cond_5

    move-object v1, v2

    .line 828
    :goto_2
    if-eqz v2, :cond_0

    .line 829
    const-string v0, "vararg"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    :cond_0
    if-eqz p4, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->B()Z

    move-result v0

    if-nez v0, :cond_1

    .line 832
    invoke-direct {p0, p1, p3}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/av;Ljava/lang/StringBuilder;)V

    .line 835
    :cond_1
    if-eqz p2, :cond_2

    move-object v0, p1

    .line 836
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p3}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 837
    const-string v0, ": "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 840
    :cond_2
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 842
    invoke-direct {p0, p1, p3}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/av;Ljava/lang/StringBuilder;)V

    .line 844
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    .line 845
    const-string v0, " /*"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "realType"

    invoke-static {v3, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    :cond_3
    return-void

    :cond_4
    move-object v2, v1

    .line 825
    goto :goto_1

    .line 826
    :cond_5
    const-string v0, "realType"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v3

    goto :goto_2

    :cond_6
    move-object v0, p1

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/ay;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 459
    .line 460
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->a:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 462
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/b/ay;->b()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object p1

    .line 464
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->z()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->k:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 465
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/b/ay;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 501
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->f(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 502
    :cond_0
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->o()Lkotlin/reflect/jvm/internal/impl/g/l;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/l;->a:Lkotlin/reflect/jvm/internal/impl/g/l;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 507
    :cond_1
    :goto_0
    return-void

    .line 505
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    const-string v1, "callable.modality"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/u;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    .line 913
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->d:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 915
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->B()Z

    move-result v0

    if-nez v0, :cond_4

    move-object v0, p1

    .line 916
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 917
    if-nez v1, :cond_0

    .line 918
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    const-string v2, "klass.visibility"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Ljava/lang/StringBuilder;)V

    :cond_0
    move-object v0, p1

    .line 920
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/t;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/t;Ljava/lang/StringBuilder;)V

    .line 922
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/f;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 923
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    const-string v2, "klass.modality"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/u;Ljava/lang/StringBuilder;)V

    .line 925
    :cond_3
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->t()Z

    move-result v0

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(ZLjava/lang/StringBuilder;)V

    .line 926
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->u()Z

    move-result v0

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(ZLjava/lang/StringBuilder;)V

    .line 927
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->r()Z

    move-result v0

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(ZLjava/lang/StringBuilder;)V

    .line 928
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->s()Z

    move-result v0

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(ZLjava/lang/StringBuilder;)V

    .line 929
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/StringBuilder;)V

    :cond_4
    move-object v0, p1

    .line 932
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->i(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 933
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->B()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;)V

    :cond_5
    move-object v0, p1

    .line 934
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 940
    :goto_0
    if-eqz v1, :cond_7

    .line 959
    :goto_1
    return-void

    :cond_6
    move-object v0, p1

    .line 937
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    goto :goto_0

    .line 942
    :cond_7
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->y()Ljava/util/List;

    move-result-object v2

    .line 943
    const-string v0, "typeParameters"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v2, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;Z)V

    move-object v0, p1

    .line 944
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/i;Ljava/lang/StringBuilder;)V

    .line 946
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/f;->a()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 947
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->o()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v1

    .line 948
    if-eqz v1, :cond_8

    .line 949
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v1

    .line 950
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 951
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/d;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    const-string v3, "primaryConstructor.visibility"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Ljava/lang/StringBuilder;)V

    .line 952
    const-string v0, "constructor"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 953
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v0

    const-string v3, "primaryConstructor.valueParameters"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/d;->H_()Z

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/Collection;ZLjava/lang/StringBuilder;)V

    .line 957
    :cond_8
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/StringBuilder;)V

    .line 958
    const-string v0, "typeParameters"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;)V

    goto :goto_1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/i;Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    .line 901
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/i;->y()Ljava/util/List;

    move-result-object v0

    .line 902
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/i;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v1

    .line 904
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/i;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 905
    const-string v2, " /*captured type parameters: "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 906
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 907
    const-string v0, "*/"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 909
    :cond_0
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/l;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 724
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 725
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/l;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    const-string v1, "constructor.visibility"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Ljava/lang/StringBuilder;)V

    move-object v0, p1

    .line 726
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    .line 728
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 729
    const-string v0, "constructor"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 731
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 732
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/l;->o()Lkotlin/reflect/jvm/internal/impl/b/i;

    move-result-object v0

    .line 733
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 734
    const-string v1, " "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 736
    :cond_1
    const-string v1, "classDescriptor"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 737
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/l;->f()Ljava/util/List;

    move-result-object v0

    const-string v1, "constructor.typeParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;Z)V

    .line 740
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/l;->i()Ljava/util/List;

    move-result-object v0

    const-string v1, "constructor.valueParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/l;->H_()Z

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/Collection;ZLjava/lang/StringBuilder;)V

    .line 742
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->y()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 743
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/l;->f()Ljava/util/List;

    move-result-object v0

    const-string v1, "constructor.typeParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;)V

    .line 745
    :cond_3
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 99
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "descriptor.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 551
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1140
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 551
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1141
    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 552
    :cond_1
    const-string v0, "operator "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->A()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1142
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 554
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->A()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1143
    :goto_1
    if-nez v0, :cond_4

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 555
    :cond_4
    const-string v0, "infix "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    move-object v0, p1

    .line 557
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    .line 558
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 559
    const-string v0, "inline "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    :cond_6
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 562
    const-string v0, "tailrec "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    :cond_7
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->C()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 565
    const-string v0, "suspend "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    :cond_8
    return-void

    :cond_9
    move v0, v2

    .line 1141
    goto :goto_0

    :cond_a
    move v0, v2

    .line 1143
    goto :goto_1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/t;Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 539
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/t;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    const-string v0, "external "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 542
    :cond_0
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/u;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 469
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->b:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 472
    :goto_0
    return-void

    .line 470
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/b/u;->name()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toLowerCase()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 471
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/y;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 990
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "package-fragment"

    invoke-direct {p0, v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 991
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 992
    const-string v0, " in "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 993
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/y;->e()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 995
    :cond_0
    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 998
    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 999
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->b()Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    const-string v1, "fqName.toUnsafe()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1000
    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 1001
    const-string v0, " "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1002
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1004
    :cond_0
    return-void

    .line 1000
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ad;ILjava/lang/Object;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ad;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/ac;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ac;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/af;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/af;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/ap;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ap;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/aq;Ljava/lang/StringBuilder;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Ljava/lang/StringBuilder;Z)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/at;ZLjava/lang/StringBuilder;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/at;ZLjava/lang/StringBuilder;Z)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/l;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/l;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/g/e;Lkotlin/reflect/jvm/internal/impl/b/y;Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/y;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method private final a(ZLjava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 475
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->h:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    if-eqz p1, :cond_0

    .line 477
    const-string v0, "header"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 1111
    const-string v1, "?"

    const-string v2, ""

    const/4 v4, 0x4

    move-object v0, p2

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "?"

    const/4 v1, 0x2

    invoke-static {p2, v0, v3, v1, v5}, Lkotlin/h/j;->b(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v3, 0x1

    :cond_2
    return v3
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 1

    .prologue
    .line 1113
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->C()Lkotlin/reflect/jvm/internal/impl/g/n;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/f;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/n;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 63
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :pswitch_1
    return-object p1

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final b(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/g/o;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final b(Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 647
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 648
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 649
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 650
    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, v2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Ljava/lang/StringBuilder;Z)V

    .line 651
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 652
    const-string v0, ", "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 655
    :cond_1
    return-void
.end method

.method private final b(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 2

    .prologue
    .line 163
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/k/at;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/at;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/at;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 164
    const-string v0, "<Not computed yet>"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->j()Lkotlin/reflect/jvm/internal/impl/k/ao;

    move-result-object v0

    .line 169
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    if-eqz v1, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/m;

    move-object v1, p0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/g/c;

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/g/h;

    invoke-virtual {v0, v1, p0}, Lkotlin/reflect/jvm/internal/impl/k/m;->a(Lkotlin/reflect/jvm/internal/impl/g/c;Lkotlin/reflect/jvm/internal/impl/g/h;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 170
    :cond_2
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v1, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    goto :goto_0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 712
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->d()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    .line 713
    if-eqz v0, :cond_1

    .line 714
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 715
    const-string v0, "type"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    .line 716
    const-string v2, "type"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/an;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 717
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 719
    :cond_0
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 721
    :cond_1
    return-void
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 850
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->B()Z

    move-result v0

    if-nez v0, :cond_2

    .line 851
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->A()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, p1

    .line 852
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 853
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    const-string v1, "property.visibility"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Ljava/lang/StringBuilder;)V

    .line 855
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    const-string v0, "const "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    move-object v0, p1

    .line 859
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    move-object v0, p1

    .line 860
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    move-object v0, p1

    .line 861
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    .line 862
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/StringBuilder;)V

    move-object v0, p1

    .line 863
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    :cond_1
    move-object v0, p1

    .line 865
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/av;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/av;Ljava/lang/StringBuilder;)V

    .line 866
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->f()Ljava/util/List;

    move-result-object v0

    const-string v1, "property.typeParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;Z)V

    move-object v0, p1

    .line 867
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/lang/StringBuilder;)V

    :cond_2
    move-object v0, p1

    .line 870
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 871
    const-string v0, ": "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    const-string v2, "property.type"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, p1

    .line 873
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/lang/StringBuilder;)V

    move-object v0, p1

    .line 875
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/av;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/av;Ljava/lang/StringBuilder;)V

    .line 877
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/ag;->f()Ljava/util/List;

    move-result-object v0

    const-string v1, "property.typeParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;)V

    .line 878
    return-void
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/av;Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 881
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 882
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/av;->z()Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    .line 883
    const-string v1, " = "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "constant"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/h/b/f;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 882
    check-cast v0, Ljava/lang/StringBuilder;

    .line 886
    :cond_0
    return-void
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 510
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->c:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 511
    :cond_1
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->o()Lkotlin/reflect/jvm/internal/impl/g/l;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/l;->b:Lkotlin/reflect/jvm/internal/impl/g/l;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 513
    const-string v0, "override "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    const-string v0, "/*"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->k()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*/ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/StringBuilder;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 962
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 973
    :cond_0
    :goto_0
    return-void

    .line 964
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 966
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v1

    .line 967
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->h(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 969
    :cond_2
    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;)V

    .line 970
    const-string v0, ": "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v1

    .line 971
    check-cast v0, Ljava/lang/Iterable;

    .line 1147
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1148
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1149
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 971
    const-string v4, "it"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1150
    :cond_3
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    move-object v1, p2

    check-cast v1, Ljava/lang/Appendable;

    const-string v2, ", "

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const/16 v8, 0x7c

    move-object v4, v3

    move-object v6, v3

    move-object v7, v3

    move-object v9, v3

    .line 972
    invoke-static/range {v0 .. v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/Appendable;

    goto :goto_0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "companion object"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    :cond_0
    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;)V

    .line 108
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 109
    if-eqz v0, :cond_1

    .line 110
    const-string v1, "of "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "containingDeclaration.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/e/h;->c:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 115
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->B()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;)V

    .line 116
    :cond_3
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "descriptor.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    :cond_4
    return-void
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 659
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->B()Z

    move-result v0

    if-nez v0, :cond_3

    .line 660
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->A()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, p1

    .line 661
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 662
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    const-string v1, "function.visibility"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Ljava/lang/StringBuilder;)V

    move-object v0, p1

    .line 663
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    .line 665
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/StringBuilder;)V

    :cond_0
    move-object v0, p1

    .line 669
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    move-object v0, p1

    .line 670
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V

    .line 672
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 673
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 674
    const-string v0, "/*isHiddenToOvercomeSignatureClash*/ "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 677
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 678
    const-string v0, "/*isHiddenForResolutionEverywhereBesideSupercalls*/ "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 683
    :cond_2
    const-string v0, "fun"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->f()Ljava/util/List;

    move-result-object v0

    const-string v1, "function.typeParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;Z)V

    move-object v0, p1

    .line 685
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/lang/StringBuilder;)V

    :cond_3
    move-object v0, p1

    .line 688
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/StringBuilder;)V

    .line 690
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->i()Ljava/util/List;

    move-result-object v0

    const-string v1, "function.valueParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->H_()Z

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/Collection;ZLjava/lang/StringBuilder;)V

    move-object v0, p1

    .line 692
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Ljava/lang/StringBuilder;)V

    .line 694
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 695
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->J()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->F()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v0, :cond_4

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->k(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 696
    :cond_4
    const-string v1, ": "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v0, :cond_6

    const-string v0, "[NULL]"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    :cond_5
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/s;->f()Ljava/util/List;

    move-result-object v0

    const-string v1, "function.typeParameters"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;Ljava/lang/StringBuilder;)V

    .line 700
    return-void

    .line 696
    :cond_6
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private final b(ZLjava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 482
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->i:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    if-eqz p1, :cond_0

    .line 484
    const-string v0, "impl"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 202
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/j;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1116
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 202
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 1117
    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->C()Lkotlin/reflect/jvm/internal/impl/g/n;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/f;->b:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/n;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 70
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<font color=red><b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</b></font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :pswitch_1
    return-object p1

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final c(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 241
    move-object v0, p2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 243
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :goto_0
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    :cond_0
    return-void

    .line 248
    :cond_1
    const/4 v4, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/g/e;Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ad;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 522
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->f:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 523
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 524
    const-string v0, "/*"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b$a;->name()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "(this as java.lang.String).toLowerCase()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*/ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 976
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/reflect/jvm/internal/impl/b/i;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 977
    return-void
.end method

.method private final c(ZLjava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 489
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->e:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    if-eqz p1, :cond_0

    .line 491
    const-string v0, "inner"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    .line 369
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/j;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->C()Lkotlin/reflect/jvm/internal/impl/g/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final d(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 306
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    .line 308
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->M()Lkotlin/reflect/jvm/internal/impl/g/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/e;

    move-object v1, p2

    .line 309
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a/a;

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/a/a;)V

    .line 310
    nop

    .line 308
    nop

    .line 311
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eq v0, v4, :cond_1

    move v0, v2

    .line 313
    :goto_0
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v5

    .line 314
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/a/j;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 316
    if-nez v5, :cond_0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    :cond_0
    move v4, v2

    .line 318
    :goto_1
    if-eqz v4, :cond_5

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 319
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->g(Ljava/lang/CharSequence;)C

    move-result v0

    const/16 v6, 0x20

    if-ne v0, v6, :cond_3

    move v0, v2

    :goto_2
    sget-boolean v6, Lkotlin/p;->a:Z

    if-eqz v6, :cond_4

    if-nez v0, :cond_4

    const-string v1, "Assertion failed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    move v0, v3

    .line 311
    goto :goto_0

    :cond_2
    move v4, v3

    .line 316
    goto :goto_1

    :cond_3
    move v0, v3

    .line 319
    goto :goto_2

    :cond_4
    move-object v0, p1

    .line 320
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->e(Ljava/lang/CharSequence;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v6, 0x29

    if-eq v0, v6, :cond_5

    move-object v0, p1

    .line 322
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->e(Ljava/lang/CharSequence;)I

    move-result v0

    const-string v6, "()"

    invoke-virtual {p1, v0, v6}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    :cond_5
    if-eqz v4, :cond_6

    const-string v0, "("

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    :cond_6
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/a/j;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 329
    const-string v0, "suspend "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_7
    if-eqz v1, :cond_d

    .line 333
    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 334
    :cond_8
    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    move v3, v2

    .line 335
    :cond_a
    if-eqz v3, :cond_b

    .line 336
    const-string v0, "("

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    :cond_b
    invoke-direct {p0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 339
    if-eqz v3, :cond_c

    .line 340
    const-string v0, ")"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    :cond_c
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    :cond_d
    const-string v0, "("

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/a/j;->g(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v0

    .line 348
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/a/x;

    invoke-virtual {v0}, Lkotlin/a/x;->c()I

    move-result v1

    invoke-virtual {v0}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 349
    if-lez v1, :cond_e

    const-string v1, ", "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    :cond_e
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->q()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/a/j;->h(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    .line 352
    :goto_4
    if-eqz v1, :cond_f

    .line 353
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    const-string v1, ": "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    :cond_f
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->N()Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 351
    :cond_10
    const/4 v1, 0x0

    goto :goto_4

    .line 360
    :cond_11
    const-string v0, ") "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/a/j;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 363
    if-eqz v4, :cond_12

    const-string v0, ")"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    :cond_12
    if-eqz v5, :cond_13

    const-string v0, "?"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    :cond_13
    return-void
.end method

.method private final d(Lkotlin/reflect/jvm/internal/impl/b/b;Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 529
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/t;

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/t;Ljava/lang/StringBuilder;)V

    .line 530
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    const-string v0, "header "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 533
    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    const-string v0, "impl "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    :cond_1
    return-void
.end method

.method private final d(ZLjava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 496
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->m()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->g:Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    const-string v0, "data"

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private final i(Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 781
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->p()Lkotlin/reflect/jvm/internal/impl/g/m;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/g/f;->e:[I

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/g/m;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 784
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 783
    :pswitch_0
    if-nez p1, :cond_0

    .line 784
    :goto_0
    :pswitch_1
    return v0

    :cond_0
    move v0, v1

    .line 783
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 784
    goto :goto_0

    .line 781
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public A()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->k()Z

    move-result v0

    return v0
.end method

.method public B()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->j()Z

    move-result v0

    return v0
.end method

.method public C()Lkotlin/reflect/jvm/internal/impl/g/n;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->A()Lkotlin/reflect/jvm/internal/impl/g/n;

    move-result-object v0

    return-object v0
.end method

.method public D()Lkotlin/d/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->v()Lkotlin/d/a/b;

    move-result-object v0

    return-object v0
.end method

.method public E()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->r()Z

    move-result v0

    return v0
.end method

.method public F()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->n()Z

    move-result v0

    return v0
.end method

.method public G()Lkotlin/reflect/jvm/internal/impl/g/c$j;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->z()Lkotlin/reflect/jvm/internal/impl/g/c$j;

    move-result-object v0

    return-object v0
.end method

.method public H()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->m()Z

    move-result v0

    return v0
.end method

.method public I()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->g()Z

    move-result v0

    return v0
.end method

.method public J()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->o()Z

    move-result v0

    return v0
.end method

.method public K()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->u()Z

    move-result v0

    return v0
.end method

.method public L()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->t()Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->C()Lkotlin/reflect/jvm/internal/impl/g/n;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/f;->d:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/n;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 89
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<i>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</i>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 87
    :pswitch_1
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/a/l;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const-string v0, "lowerRendered"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "upperRendered"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builtIns"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    const-string v0, "("

    const/4 v1, 0x0

    invoke-static {p2, v0, v1, v8, v7}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    :cond_0
    :goto_0
    return-object v0

    .line 211
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->e()Lkotlin/reflect/jvm/internal/impl/g/b;

    move-result-object v2

    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/a/l;->u()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    const-string v1, "builtIns.collection"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    move-object v1, p0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/b;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/g/c;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Collection"

    invoke-static {v0, v1, v7, v8, v7}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 215
    const-string v0, "Mutable"

    .line 217
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 218
    if-nez v0, :cond_0

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MutableMap.MutableEntry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Map.Entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(Mutable)Map.(Mutable)Entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v6

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 221
    if-nez v0, :cond_0

    .line 223
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->e()Lkotlin/reflect/jvm/internal/impl/g/b;

    move-result-object v2

    invoke-virtual {p3}, Lkotlin/reflect/jvm/internal/impl/a/l;->l()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    const-string v1, "builtIns.array"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    move-object v1, p0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/b;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/g/c;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Array"

    invoke-static {v0, v1, v7, v8, v7}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 225
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Array<"

    invoke-direct {p0, v2}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "Array<out "

    invoke-direct {p0, v3}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Array<(out) "

    invoke-direct {p0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 226
    if-nez v0, :cond_0

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const-string v0, "typeArguments"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    .line 233
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 234
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->O()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    invoke-direct {p0, v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 236
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->P()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    nop

    .line 233
    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 409
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object v10, v11

    check-cast v10, Ljava/lang/StringBuilder;

    .line 410
    const/16 v0, 0x40

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 411
    if-eqz p2, :cond_0

    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/b/a/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v12

    .line 415
    const-string v0, "annotationType"

    invoke-static {v12, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v12}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 418
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/a/c;)Ljava/util/List;

    move-result-object v1

    .line 419
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->k()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 420
    check-cast v0, Ljava/lang/Iterable;

    move-object v1, v10

    check-cast v1, Ljava/lang/Appendable;

    const-string v2, ", "

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "("

    check-cast v3, Ljava/lang/CharSequence;

    const-string v4, ")"

    check-cast v4, Ljava/lang/CharSequence;

    const/16 v8, 0x70

    move-object v7, v6

    move-object v9, v6

    invoke-static/range {v0 .. v9}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/Appendable;

    .line 424
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->H()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v12}, Lkotlin/reflect/jvm/internal/impl/k/r;->d()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v12}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$b;

    if-eqz v0, :cond_4

    .line 425
    :cond_3
    const-string v0, " /* annotation class not found */"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    :cond_4
    nop

    .line 409
    check-cast v11, Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_5
    move v0, v5

    .line 419
    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/h;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "klass"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 125
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/h;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->e()Lkotlin/reflect/jvm/internal/impl/g/b;

    move-result-object v0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/g/c;

    invoke-interface {v0, p1, p0}, Lkotlin/reflect/jvm/internal/impl/g/b;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/g/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/m;)Ljava/lang/String;
    .locals 3

    .prologue
    const-string v0, "declarationDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 570
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 571
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/g/e$a;

    invoke-direct {v2, p0}, Lkotlin/reflect/jvm/internal/impl/g/e$a;-><init>(Lkotlin/reflect/jvm/internal/impl/g/e;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/o;

    invoke-interface {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/b/m;->a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->I()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 574
    invoke-direct {p0, v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/b/m;)V

    .line 576
    :cond_0
    nop

    .line 570
    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/c;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/c;->h()Ljava/util/List;

    move-result-object v0

    const-string v1, "fqName.pathSegments()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/g/o;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/ad;)Ljava/lang/String;
    .locals 3

    .prologue
    const-string v0, "typeConstructor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 283
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Lkotlin/reflect/jvm/internal/impl/b/h;)Ljava/lang/String;

    move-result-object v0

    .line 282
    :goto_0
    return-object v0

    .line 283
    :cond_1
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-nez v1, :cond_0

    .line 284
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 285
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected classifier: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Ljava/lang/String;
    .locals 3

    .prologue
    const-string v0, "typeProjection"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 289
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 290
    invoke-static {p1}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 291
    nop

    .line 289
    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;
    .locals 3

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->D()Lkotlin/d/a/b;

    move-result-object v2

    invoke-interface {v2, p1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {p0, v0, v2}, Lkotlin/reflect/jvm/internal/impl/g/e;->a(Ljava/lang/StringBuilder;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 134
    nop

    .line 132
    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    return-object v0
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/g/i;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    return-object v0
.end method

.method public a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->a(Ljava/util/Set;)V

    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/g/a;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->a(Lkotlin/reflect/jvm/internal/impl/g/a;)V

    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/g/b;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->a(Lkotlin/reflect/jvm/internal/impl/g/b;)V

    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/g/m;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->a(Lkotlin/reflect/jvm/internal/impl/g/m;)V

    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/g/n;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->a(Lkotlin/reflect/jvm/internal/impl/g/n;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->a(Z)V

    return-void
.end method

.method public b(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/g/g;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->b(Ljava/util/Set;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->b(Z)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->H()Z

    move-result v0

    return v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/g/a;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->c()Lkotlin/reflect/jvm/internal/impl/g/a;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->c(Z)V

    return-void
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->d(Z)V

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->l()Z

    move-result v0

    return v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/g/b;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->e()Lkotlin/reflect/jvm/internal/impl/g/b;

    move-result-object v0

    return-object v0
.end method

.method public e(Z)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->e(Z)V

    return-void
.end method

.method public f(Z)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->f(Z)V

    return-void
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->f()Z

    move-result v0

    return v0
.end method

.method public g()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->G()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public g(Z)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->g(Z)V

    return-void
.end method

.method public h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->h()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public h(Z)V
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/g/i;->h(Z)V

    return-void
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->K()Z

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->M()Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->N()Z

    move-result v0

    return v0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->s()Z

    move-result v0

    return v0
.end method

.method public m()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/g/g;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->i()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->p()Z

    move-result v0

    return v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/g/l;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->y()Lkotlin/reflect/jvm/internal/impl/g/l;

    move-result-object v0

    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/g/m;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->B()Lkotlin/reflect/jvm/internal/impl/g/m;

    move-result-object v0

    return-object v0
.end method

.method public q()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->L()Z

    move-result v0

    return v0
.end method

.method public r()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->C()Z

    move-result v0

    return v0
.end method

.method public s()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->E()Z

    move-result v0

    return v0
.end method

.method public t()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->D()Z

    move-result v0

    return v0
.end method

.method public u()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->I()Z

    move-result v0

    return v0
.end method

.method public v()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->F()Z

    move-result v0

    return v0
.end method

.method public w()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->w()Z

    move-result v0

    return v0
.end method

.method public x()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->J()Z

    move-result v0

    return v0
.end method

.method public y()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->x()Z

    move-result v0

    return v0
.end method

.method public z()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/g/e;->m:Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->q()Z

    move-result v0

    return v0
.end method
