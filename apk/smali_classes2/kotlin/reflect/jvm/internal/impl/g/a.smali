.class public final enum Lkotlin/reflect/jvm/internal/impl/g/a;
.super Ljava/lang/Enum;
.source "DescriptorRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/g/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/g/a;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/g/a;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/g/a;

.field private static final synthetic d:[Lkotlin/reflect/jvm/internal/impl/g/a;


# instance fields
.field private final e:Z

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v12, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    new-array v14, v5, [Lkotlin/reflect/jvm/internal/impl/g/a;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/a;

    const-string v1, "NO_ARGUMENTS"

    move v3, v2

    move v4, v2

    .line 173
    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/g/a;-><init>(Ljava/lang/String;IZZILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/a;->a:Lkotlin/reflect/jvm/internal/impl/g/a;

    aput-object v0, v14, v2

    new-instance v7, Lkotlin/reflect/jvm/internal/impl/g/a;

    const-string v8, "UNLESS_EMPTY"

    move v10, v9

    move v11, v2

    move-object v13, v6

    .line 174
    invoke-direct/range {v7 .. v13}, Lkotlin/reflect/jvm/internal/impl/g/a;-><init>(Ljava/lang/String;IZZILkotlin/d/b/i;)V

    sput-object v7, Lkotlin/reflect/jvm/internal/impl/g/a;->b:Lkotlin/reflect/jvm/internal/impl/g/a;

    aput-object v7, v14, v9

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/a;

    const-string v1, "ALWAYS_PARENTHESIZED"

    .line 175
    invoke-direct {v0, v1, v12, v9, v9}, Lkotlin/reflect/jvm/internal/impl/g/a;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/a;->c:Lkotlin/reflect/jvm/internal/impl/g/a;

    aput-object v0, v14, v12

    sput-object v14, Lkotlin/reflect/jvm/internal/impl/g/a;->d:[Lkotlin/reflect/jvm/internal/impl/g/a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/g/a;->e:Z

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/g/a;->f:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IZZILkotlin/d/b/i;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    and-int/lit8 v1, p5, 0x1

    if-eqz v1, :cond_0

    move p3, v0

    .line 170
    :cond_0
    and-int/lit8 v1, p5, 0x2

    if-eqz v1, :cond_1

    move p4, v0

    .line 171
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/g/a;-><init>(Ljava/lang/String;IZZ)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/g/a;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/g/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/a;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/g/a;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/a;->d:[Lkotlin/reflect/jvm/internal/impl/g/a;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/g/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/g/a;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/g/a;->e:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/g/a;->f:Z

    return v0
.end method
