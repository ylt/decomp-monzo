.class public final enum Lkotlin/reflect/jvm/internal/impl/g/m;
.super Ljava/lang/Enum;
.source "DescriptorRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/g/m;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/g/m;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/g/m;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/g/m;

.field private static final synthetic d:[Lkotlin/reflect/jvm/internal/impl/g/m;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/g/m;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/m;

    const-string v2, "ALL"

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/g/m;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/m;->a:Lkotlin/reflect/jvm/internal/impl/g/m;

    aput-object v1, v0, v3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/m;

    const-string v2, "ONLY_NON_SYNTHESIZED"

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/g/m;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/m;->b:Lkotlin/reflect/jvm/internal/impl/g/m;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/m;

    const-string v2, "NONE"

    invoke-direct {v1, v2, v5}, Lkotlin/reflect/jvm/internal/impl/g/m;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/m;->c:Lkotlin/reflect/jvm/internal/impl/g/m;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/m;->d:[Lkotlin/reflect/jvm/internal/impl/g/m;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 267
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/g/m;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/g/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/m;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/g/m;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/m;->d:[Lkotlin/reflect/jvm/internal/impl/g/m;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/g/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/g/m;

    return-object v0
.end method
