.class public final Lkotlin/reflect/jvm/internal/impl/g/c$j$a;
.super Ljava/lang/Object;
.source "DescriptorRenderer.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/g/c$j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/g/c$j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/g/c$j$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/c$j$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/g/c$j$a;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/g/c$j$a;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/g/c$j$a;->a:Lkotlin/reflect/jvm/internal/impl/g/c$j$a;

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/StringBuilder;)V
    .locals 1

    .prologue
    const-string v0, "builder"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    const-string v0, "("

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/at;IILjava/lang/StringBuilder;)V
    .locals 1

    .prologue
    const-string v0, "parameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builder"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public b(ILjava/lang/StringBuilder;)V
    .locals 1

    .prologue
    const-string v0, "builder"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    return-void
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/at;IILjava/lang/StringBuilder;)V
    .locals 1

    .prologue
    const-string v0, "parameter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builder"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    add-int/lit8 v0, p3, -0x1

    if-eq p2, v0, :cond_0

    .line 86
    const-string v0, ", "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :cond_0
    return-void
.end method
