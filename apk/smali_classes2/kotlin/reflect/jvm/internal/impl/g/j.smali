.class public final Lkotlin/reflect/jvm/internal/impl/g/j;
.super Ljava/lang/Object;
.source "DescriptorRenderer.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/g/j;

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 220
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/j;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/g/j;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/g/j;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/g/j;->a:Lkotlin/reflect/jvm/internal/impl/g/j;

    .line 221
    const/16 v0, 0x14

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/e/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "org.jetbrains.annotations.ReadOnly"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "org.jetbrains.annotations.Mutable"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "org.jetbrains.annotations.NotNull"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "org.jetbrains.annotations.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "android.support.annotation.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "android.support.annotation.NonNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "com.android.annotations.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "com.android.annotations.NonNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "org.eclipse.jdt.annotation.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "org.eclipse.jdt.annotation.NonNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "org.checkerframework.checker.nullness.qual.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "org.checkerframework.checker.nullness.qual.NonNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "javax.annotation.Nonnull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "javax.annotation.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "javax.annotation.CheckForNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "edu.umd.cs.findbugs.annotations.NonNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "edu.umd.cs.findbugs.annotations.CheckForNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "edu.umd.cs.findbugs.annotations.Nullable"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "edu.umd.cs.findbugs.annotations.PossiblyNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "lombok.NonNull"

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/ah;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/j;->b:Ljava/util/Set;

    .line 244
    new-array v0, v6, [Lkotlin/reflect/jvm/internal/impl/e/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "kotlin.internal.NoInfer"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "kotlin.internal.Exact"

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    invoke-static {v0}, Lkotlin/a/ah;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/j;->c:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/j;->b:Ljava/util/Set;

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/j;->c:Ljava/util/Set;

    return-object v0
.end method
