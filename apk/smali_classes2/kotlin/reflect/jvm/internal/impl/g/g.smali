.class public final enum Lkotlin/reflect/jvm/internal/impl/g/g;
.super Ljava/lang/Enum;
.source "DescriptorRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/g/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/g/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final enum g:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final enum h:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final enum i:Lkotlin/reflect/jvm/internal/impl/g/g;

.field public static final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/g/g;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/g/g;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lkotlin/reflect/jvm/internal/impl/g/g$a;

.field private static final synthetic m:[Lkotlin/reflect/jvm/internal/impl/g/g;


# instance fields
.field private final n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/16 v0, 0x9

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/g/g;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v3, "VISIBILITY"

    .line 274
    invoke-direct {v1, v3, v2, v6}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->a:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v3, "MODALITY"

    .line 275
    invoke-direct {v1, v3, v6, v6}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->b:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v1, v0, v6

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v3, "OVERRIDE"

    .line 276
    invoke-direct {v1, v3, v4, v6}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->c:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v3, "ANNOTATIONS"

    .line 277
    invoke-direct {v1, v3, v5, v2}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->d:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v3, "INNER"

    .line 278
    invoke-direct {v1, v3, v7, v6}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/g/g;->e:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v4, "MEMBER_KIND"

    const/4 v5, 0x5

    .line 279
    invoke-direct {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/g/g;->f:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v4, "DATA"

    const/4 v5, 0x6

    .line 280
    invoke-direct {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/g/g;->g:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v4, "HEADER"

    const/4 v5, 0x7

    .line 281
    invoke-direct {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/g/g;->h:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/g/g;

    const-string v4, "IMPL"

    const/16 v5, 0x8

    .line 282
    invoke-direct {v3, v4, v5, v6}, Lkotlin/reflect/jvm/internal/impl/g/g;-><init>(Ljava/lang/String;IZ)V

    sput-object v3, Lkotlin/reflect/jvm/internal/impl/g/g;->i:Lkotlin/reflect/jvm/internal/impl/g/g;

    aput-object v3, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/g;->m:[Lkotlin/reflect/jvm/internal/impl/g/g;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/g$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/g;->l:Lkotlin/reflect/jvm/internal/impl/g/g$a;

    .line 287
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/g/g;->values()[Lkotlin/reflect/jvm/internal/impl/g/g;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 294
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    move v4, v2

    .line 295
    :goto_0
    array-length v2, v0

    if-ge v4, v2, :cond_1

    aget-object v3, v0, v4

    move-object v2, v3

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/g/g;

    .line 287
    iget-boolean v2, v2, Lkotlin/reflect/jvm/internal/impl/g/g;->n:Z

    if-eqz v2, :cond_0

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 296
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 287
    invoke-static {v1}, Lkotlin/a/m;->m(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/g;->j:Ljava/util/Set;

    .line 290
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/g/g;->values()[Lkotlin/reflect/jvm/internal/impl/g/g;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/a/g;->j([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/g;->k:Ljava/util/Set;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 273
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lkotlin/reflect/jvm/internal/impl/g/g;->n:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/g/g;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/g;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/g/g;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/g;->m:[Lkotlin/reflect/jvm/internal/impl/g/g;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/g/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/g/g;

    return-object v0
.end method
