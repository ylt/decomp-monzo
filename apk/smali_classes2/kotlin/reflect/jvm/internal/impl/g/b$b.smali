.class public final Lkotlin/reflect/jvm/internal/impl/g/b$b;
.super Ljava/lang/Object;
.source "ClassifierNamePolicy.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/g/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/g/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/g/b$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/b$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/g/b$b;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/g/b$b;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/g/b$b;->a:Lkotlin/reflect/jvm/internal/impl/g/b$b;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/g/c;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "classifier"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderer"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v0, :cond_0

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    const-string v1, "classifier.name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    .line 29
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 32
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 34
    :cond_1
    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_2
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object p1

    .line 37
    instance-of v1, p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_1

    .line 39
    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lkotlin/a/m;->d(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/g/o;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
