.class public final Lkotlin/reflect/jvm/internal/impl/g/c$a;
.super Ljava/lang/Object;
.source "DescriptorRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/g/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/i;)Ljava/lang/String;
    .locals 3

    .prologue
    const-string v0, "classifier"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "typealias"

    .line 165
    :goto_0
    return-object v0

    .line 151
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 152
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    const-string v0, "companion object"

    goto :goto_0

    .line 155
    :cond_1
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/d;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/f;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 161
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 156
    :pswitch_0
    const-string v0, "class"

    goto :goto_0

    .line 157
    :pswitch_1
    const-string v0, "interface"

    goto :goto_0

    .line 158
    :pswitch_2
    const-string v0, "enum class"

    goto :goto_0

    .line 159
    :pswitch_3
    const-string v0, "object"

    goto :goto_0

    .line 160
    :pswitch_4
    const-string v0, "annotation class"

    goto :goto_0

    .line 161
    :pswitch_5
    const-string v0, "enum entry"

    goto :goto_0

    .line 164
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected classifier: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 155
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/g/h;",
            "Lkotlin/n;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/g/c;"
        }
    .end annotation

    .prologue
    const-string v0, "changeOptions"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/g/i;

    invoke-direct {v1}, Lkotlin/reflect/jvm/internal/impl/g/i;-><init>()V

    .line 95
    invoke-interface {p1, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/g/i;->b()V

    .line 97
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/e;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;-><init>(Lkotlin/reflect/jvm/internal/impl/g/i;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/c;

    return-object v0
.end method
