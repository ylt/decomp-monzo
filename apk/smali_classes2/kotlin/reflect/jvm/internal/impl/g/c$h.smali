.class final Lkotlin/reflect/jvm/internal/impl/g/c$h;
.super Lkotlin/d/b/m;
.source "DescriptorRenderer.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/g/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/g/h;",
        "Lkotlin/n;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/g/c$h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/c$h;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/g/c$h;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$h;->a:Lkotlin/reflect/jvm/internal/impl/g/c$h;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/g/h;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/g/c$h;->a(Lkotlin/reflect/jvm/internal/impl/g/h;)V

    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/g/h;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/h;->f(Z)V

    .line 117
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/h;->b(Ljava/util/Set;)V

    .line 118
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/b$b;->a:Lkotlin/reflect/jvm/internal/impl/g/b$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/b;

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/h;->a(Lkotlin/reflect/jvm/internal/impl/g/b;)V

    .line 119
    invoke-interface {p1, v1}, Lkotlin/reflect/jvm/internal/impl/g/h;->h(Z)V

    .line 120
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/m;->c:Lkotlin/reflect/jvm/internal/impl/g/m;

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/h;->a(Lkotlin/reflect/jvm/internal/impl/g/m;)V

    .line 121
    invoke-interface {p1, v1}, Lkotlin/reflect/jvm/internal/impl/g/h;->b(Z)V

    .line 122
    invoke-interface {p1, v1}, Lkotlin/reflect/jvm/internal/impl/g/h;->c(Z)V

    .line 123
    invoke-interface {p1, v1}, Lkotlin/reflect/jvm/internal/impl/g/h;->g(Z)V

    .line 124
    invoke-interface {p1, v1}, Lkotlin/reflect/jvm/internal/impl/g/h;->d(Z)V

    .line 125
    return-void
.end method
