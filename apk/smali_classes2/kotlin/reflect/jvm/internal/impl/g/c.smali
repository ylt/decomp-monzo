.class public abstract Lkotlin/reflect/jvm/internal/impl/g/c;
.super Ljava/lang/Object;
.source "DescriptorRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/g/c$j;,
        Lkotlin/reflect/jvm/internal/impl/g/c$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/g/c;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/g/c;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/g/c;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/g/c;

.field public static final e:Lkotlin/reflect/jvm/internal/impl/g/c;

.field public static final f:Lkotlin/reflect/jvm/internal/impl/g/c;

.field public static final g:Lkotlin/reflect/jvm/internal/impl/g/c;

.field public static final h:Lkotlin/reflect/jvm/internal/impl/g/c;

.field public static final i:Lkotlin/reflect/jvm/internal/impl/g/c$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/c$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    .line 100
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$c;->a:Lkotlin/reflect/jvm/internal/impl/g/c$c;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->a:Lkotlin/reflect/jvm/internal/impl/g/c;

    .line 104
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$b;->a:Lkotlin/reflect/jvm/internal/impl/g/c$b;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->b:Lkotlin/reflect/jvm/internal/impl/g/c;

    .line 109
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$d;->a:Lkotlin/reflect/jvm/internal/impl/g/c$d;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->c:Lkotlin/reflect/jvm/internal/impl/g/c;

    .line 115
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$h;->a:Lkotlin/reflect/jvm/internal/impl/g/c$h;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->d:Lkotlin/reflect/jvm/internal/impl/g/c;

    .line 127
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$f;->a:Lkotlin/reflect/jvm/internal/impl/g/c$f;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->e:Lkotlin/reflect/jvm/internal/impl/g/c;

    .line 131
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$i;->a:Lkotlin/reflect/jvm/internal/impl/g/c$i;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->f:Lkotlin/reflect/jvm/internal/impl/g/c;

    .line 136
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$e;->a:Lkotlin/reflect/jvm/internal/impl/g/c$e;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->g:Lkotlin/reflect/jvm/internal/impl/g/c;

    .line 142
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/g/c;->i:Lkotlin/reflect/jvm/internal/impl/g/c$a;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/g/c$g;->a:Lkotlin/reflect/jvm/internal/impl/g/c$g;

    check-cast v0, Lkotlin/d/a/b;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c$a;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/g/c;->h:Lkotlin/reflect/jvm/internal/impl/g/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lkotlin/reflect/jvm/internal/impl/g/c;Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    if-eqz p4, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: renderAnnotation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_1

    .line 52
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    :goto_0
    invoke-virtual {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/g/c;->a(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/a/l;)Ljava/lang/String;
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/lang/String;
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/b/m;)Ljava/lang/String;
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/e/c;)Ljava/lang/String;
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/e/f;)Ljava/lang/String;
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/k/ah;)Ljava/lang/String;
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/String;
.end method

.method public final a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/g/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/g/h;",
            "Lkotlin/n;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/g/c;"
        }
    .end annotation

    .prologue
    const-string v0, "changeOptions"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    if-nez p0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.renderer.DescriptorRendererImpl"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/g/e;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/g/e;->a()Lkotlin/reflect/jvm/internal/impl/g/i;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/g/i;->d()Lkotlin/reflect/jvm/internal/impl/g/i;

    move-result-object v1

    .line 33
    invoke-interface {p1, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/g/i;->b()V

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/g/e;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/g/e;-><init>(Lkotlin/reflect/jvm/internal/impl/g/i;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/g/c;

    return-object v0
.end method
