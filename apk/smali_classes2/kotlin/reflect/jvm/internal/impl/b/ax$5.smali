.class final Lkotlin/reflect/jvm/internal/impl/b/ax$5;
.super Lkotlin/reflect/jvm/internal/impl/b/ay;
.source "Visibilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/b/ax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/ay;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 140
    const-string v0, "private/*private to this*/"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$2"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getDisplayName"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "what"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$2"

    aput-object v1, v4, v0

    const-string v0, "isVisible"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p3, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "from"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$2"

    aput-object v1, v4, v0

    const-string v0, "isVisible"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 118
    :cond_1
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-virtual {v2, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/b/ay;->a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 120
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->l:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    if-ne p1, v2, :cond_2

    .line 129
    :goto_0
    return v0

    .line 121
    :cond_2
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a()Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    move-result-object v0

    if-ne p1, v0, :cond_3

    move v0, v1

    goto :goto_0

    .line 123
    :cond_3
    const-class v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {p2, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_4

    instance-of v2, p1, Lkotlin/reflect/jvm/internal/impl/h/e/a/g;

    if-eqz v2, :cond_4

    .line 126
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/h/e/a/g;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/h/e/a/g;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->D()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/m;->z_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 129
    goto :goto_0
.end method
