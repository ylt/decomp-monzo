.class public final Lkotlin/reflect/jvm/internal/impl/b/a/h$a;
.super Ljava/lang/Object;
.source "Annotations.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/b/a/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/impl/b/a/h;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/h$a$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a$a;-><init>()V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;-><init>()V

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/e;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 152
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    check-cast v1, Ljava/util/ArrayList;

    .line 69
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/g;->b()Lkotlin/reflect/jvm/internal/impl/b/a/e;

    move-result-object v3

    invoke-static {p2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 70
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/g;->a()Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 153
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/a/e;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 3

    .prologue
    const-string v0, "annotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "target"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fqName"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/a/e;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 149
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 64
    invoke-static {v0, p3}, Lkotlin/reflect/jvm/internal/impl/b/a/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 150
    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method
