.class public Lkotlin/reflect/jvm/internal/impl/b/b/y;
.super Lkotlin/reflect/jvm/internal/impl/b/b/ai;
.source "PropertyDescriptorImpl.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/ag;


# instance fields
.field private final e:Lkotlin/reflect/jvm/internal/impl/b/u;

.field private f:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field private g:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lkotlin/reflect/jvm/internal/impl/b/ag;

.field private final i:Lkotlin/reflect/jvm/internal/impl/b/b$a;

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private p:Lkotlin/reflect/jvm/internal/impl/b/aj;

.field private q:Lkotlin/reflect/jvm/internal/impl/b/aj;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

.field private t:Lkotlin/reflect/jvm/internal/impl/b/ai;

.field private u:Z


# direct methods
.method protected constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;ZZZZZZ)V
    .locals 8

    .prologue
    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "containingDeclaration"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "<init>"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-nez p3, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "annotations"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "<init>"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    if-nez p4, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "modality"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "<init>"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    if-nez p5, :cond_3

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "visibility"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "<init>"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    if-nez p7, :cond_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "name"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "<init>"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    if-nez p8, :cond_5

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "kind"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "<init>"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    if-nez p9, :cond_6

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "source"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "<init>"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_6
    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p7

    move v6, p6

    move-object/from16 v7, p9

    invoke-direct/range {v1 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/ai;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZLkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 42
    const/4 v1, 0x0

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->g:Ljava/util/Collection;

    .line 77
    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->e:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 78
    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 79
    if-nez p2, :cond_7

    move-object p2, p0

    :cond_7
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->h:Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 80
    move-object/from16 v0, p8

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->i:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 81
    move/from16 v0, p10

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->j:Z

    .line 82
    move/from16 v0, p11

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->k:Z

    .line 83
    move/from16 v0, p12

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->l:Z

    .line 84
    move/from16 v0, p13

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->m:Z

    .line 85
    move/from16 v0, p14

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->n:Z

    .line 86
    move/from16 v0, p15

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->o:Z

    .line 87
    return-void
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 1

    .prologue
    .line 340
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/ay;->b()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    sget-object p0, Lkotlin/reflect/jvm/internal/impl/b/ax;->h:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 343
    :cond_0
    return-object p0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;ZZZZZZ)Lkotlin/reflect/jvm/internal/impl/b/b/y;
    .locals 16

    .prologue
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "containingDeclaration"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "annotations"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "modality"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "visibility"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kind"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "source"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_6
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/y;

    const/4 v2, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v14, p12

    move/from16 v15, p13

    invoke-direct/range {v0 .. v15}, Lkotlin/reflect/jvm/internal/impl/b/b/y;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;ZZZZZZ)V

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    return-object v0
.end method

.method private static a(Lkotlin/reflect/jvm/internal/impl/k/am;Lkotlin/reflect/jvm/internal/impl/b/af;)Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "substitutor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "getSubstitutedInitialSignatureDescriptor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "accessorDescriptor"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "getSubstitutedInitialSignatureDescriptor"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/af;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/af;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-interface {v0, p0}, Lkotlin/reflect/jvm/internal/impl/b/s;->d(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->k:Z

    return v0
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->o:Z

    return v0
.end method

.method public C()Lkotlin/reflect/jvm/internal/impl/b/b/z;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    return-object v0
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->u:Z

    return v0
.end method

.method public synthetic E()Lkotlin/reflect/jvm/internal/impl/b/av;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/o",
            "<TR;TD;>;TD;)TR;"
        }
    .end annotation

    .prologue
    .line 371
    invoke-interface {p1, p0, p2}, Lkotlin/reflect/jvm/internal/impl/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 8

    .prologue
    const/4 v6, 0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "originalSubstitutor"

    aput-object v4, v2, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v3, v2, v6

    const/4 v3, 0x2

    const-string v4, "substitute"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/am;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->e:Lkotlin/reflect/jvm/internal/impl/b/u;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v5

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/am;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ag;ZLkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object p0

    goto :goto_0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/k/am;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ag;ZLkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 14

    .prologue
    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "originalSubstitutor"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "doSubstitute"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "newOwner"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "doSubstitute"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "newModality"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "doSubstitute"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    if-nez p4, :cond_3

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "newVisibility"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "doSubstitute"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    if-nez p7, :cond_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "kind"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "doSubstitute"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move-object v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p7

    .line 252
    invoke-virtual/range {v1 .. v6}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/b/y;

    move-result-object v2

    .line 254
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->f()Ljava/util/List;

    move-result-object v1

    .line 255
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 256
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/am;->b()Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v3

    invoke-static {v1, v3, v2, v4}, Lkotlin/reflect/jvm/internal/impl/k/h;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v13

    .line 260
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 261
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v13, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    .line 262
    if-nez v5, :cond_6

    .line 263
    const/4 v2, 0x0

    .line 336
    :cond_5
    :goto_0
    return-object v2

    .line 268
    :cond_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->e()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v1

    .line 269
    if-eqz v1, :cond_7

    .line 270
    invoke-interface {v1, v13}, Lkotlin/reflect/jvm/internal/impl/b/aj;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v1

    .line 271
    if-nez v1, :cond_8

    const/4 v2, 0x0

    goto :goto_0

    .line 274
    :cond_7
    const/4 v1, 0x0

    .line 278
    :cond_8
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q:Lkotlin/reflect/jvm/internal/impl/b/aj;

    if-eqz v3, :cond_9

    .line 279
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q:Lkotlin/reflect/jvm/internal/impl/b/aj;

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v13, v3, v6}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    .line 280
    if-nez v3, :cond_a

    const/4 v2, 0x0

    goto :goto_0

    .line 283
    :cond_9
    const/4 v3, 0x0

    .line 286
    :cond_a
    invoke-virtual {v2, v5, v4, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 288
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    if-nez v1, :cond_d

    const/4 v1, 0x0

    move-object v12, v1

    .line 293
    :goto_1
    if-eqz v12, :cond_b

    .line 294
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 295
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-static {v13, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/am;Lkotlin/reflect/jvm/internal/impl/b/af;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v3

    invoke-virtual {v12, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)V

    .line 296
    if-eqz v1, :cond_f

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v13, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    :goto_2
    invoke-virtual {v12, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 298
    :cond_b
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    if-nez v1, :cond_10

    const/4 v1, 0x0

    move-object v3, v1

    .line 303
    :goto_3
    if-eqz v3, :cond_13

    .line 304
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ai;->i()Ljava/util/List;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v1, v13, v4, v5}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/am;ZZ)Ljava/util/List;

    move-result-object v1

    .line 308
    if-nez v1, :cond_c

    .line 314
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Z)V

    .line 315
    invoke-static/range {p2 .. p2}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->v()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/b/ai;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 319
    :cond_c
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_12

    .line 320
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 288
    :cond_d
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/b/z;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v5

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->o()Z

    move-result v6

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->v()Z

    move-result v7

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a()Z

    move-result v8

    if-nez p5, :cond_e

    const/4 v10, 0x0

    :goto_4
    sget-object v11, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object/from16 v4, p3

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v11}, Lkotlin/reflect/jvm/internal/impl/b/b/z;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZZZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/ah;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v12, v1

    goto/16 :goto_1

    :cond_e
    invoke-interface/range {p5 .. p5}, Lkotlin/reflect/jvm/internal/impl/b/ag;->a()Lkotlin/reflect/jvm/internal/impl/b/ah;

    move-result-object v10

    goto :goto_4

    .line 296
    :cond_f
    const/4 v1, 0x0

    goto :goto_2

    .line 298
    :cond_10
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/ai;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/ai;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v5

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/ai;->o()Z

    move-result v6

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/ai;->v()Z

    move-result v7

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/ai;->a()Z

    move-result v8

    if-nez p5, :cond_11

    const/4 v10, 0x0

    :goto_5
    sget-object v11, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object/from16 v4, p3

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v11}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZZZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/ai;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v3, v1

    goto/16 :goto_3

    :cond_11
    invoke-interface/range {p5 .. p5}, Lkotlin/reflect/jvm/internal/impl/b/ag;->c()Lkotlin/reflect/jvm/internal/impl/b/ai;

    move-result-object v10

    goto :goto_5

    .line 322
    :cond_12
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-static {v13, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/am;Lkotlin/reflect/jvm/internal/impl/b/af;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)V

    .line 323
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-virtual {v3, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/b/at;)V

    .line 326
    :cond_13
    invoke-virtual {v2, v12, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/b/b/z;Lkotlin/reflect/jvm/internal/impl/b/ai;)V

    .line 328
    if-eqz p6, :cond_5

    .line 329
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/utils/f;->c()Lkotlin/reflect/jvm/internal/impl/utils/f;

    move-result-object v3

    .line 330
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->k()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ag;

    .line 331
    invoke-interface {v1, v13}, Lkotlin/reflect/jvm/internal/impl/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 333
    :cond_14
    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Ljava/util/Collection;)V

    goto/16 :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/b/ah;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->C()Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/b/y;
    .locals 16

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newOwner"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newModality"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newVisibility"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p5, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kind"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 363
    :cond_3
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/y;

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->y()Z

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v7

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->r()Z

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->A()Z

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t()Z

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->u()Z

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->v()Z

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->B()Z

    move-result v15

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v8, p5

    invoke-direct/range {v0 .. v15}, Lkotlin/reflect/jvm/internal/impl/b/b/y;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;ZZZZZZ)V

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "createSubstitutedCopy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    .line 39
    invoke-virtual/range {p0 .. p5}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->b(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "overriddenDescriptors"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "setOverriddenDescriptors"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 399
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->g:Ljava/util/Collection;

    .line 400
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/ay;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "visibility"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "setVisibility"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 146
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/b/z;Lkotlin/reflect/jvm/internal/impl/b/ai;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    .line 137
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    .line 138
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/b/aj;)V
    .locals 7
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/ReadOnly;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/aj;",
            "Lkotlin/reflect/jvm/internal/impl/b/aj;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "outType"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "setType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeParameters"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "setType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_1
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->r:Ljava/util/List;

    .line 131
    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q:Lkotlin/reflect/jvm/internal/impl/b/aj;

    .line 132
    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->p:Lkotlin/reflect/jvm/internal/impl/b/aj;

    .line 133
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 7
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/ReadOnly;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/aj;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "outType"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "setType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "typeParameters"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "setType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_1
    invoke-static {p0, p4}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    .line 118
    invoke-virtual {p0, p1, p2, p3, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/b/aj;)V

    .line 119
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 141
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->u:Z

    .line 142
    return-void
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 8

    .prologue
    .line 411
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/am;->a:Lkotlin/reflect/jvm/internal/impl/k/am;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, p5

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/am;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ag;ZLkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "copy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/b/ai;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    return-object v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q:Lkotlin/reflect/jvm/internal/impl/b/aj;

    return-object v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->p:Lkotlin/reflect/jvm/internal/impl/b/aj;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->r:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getTypeParameters"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 5

    .prologue
    .line 169
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getReturnType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/b/a;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/b/p;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 405
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->g:Ljava/util/Collection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->g:Ljava/util/Collection;

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getOverriddenDescriptors"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public synthetic l()Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    return-object v0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 5

    .prologue
    .line 175
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->e:Lkotlin/reflect/jvm/internal/impl/b/u;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getModality"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/b/b$a;
    .locals 5

    .prologue
    .line 383
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->i:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getKind"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public o()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/af;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 225
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    if-eqz v1, :cond_0

    .line 226
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->s:Lkotlin/reflect/jvm/internal/impl/b/b/z;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    if-eqz v1, :cond_1

    .line 229
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->t:Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getAccessors"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 5

    .prologue
    .line 181
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getVisibility"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 5

    .prologue
    .line 377
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->h:Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-ne v0, p0, :cond_0

    :goto_0
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/PropertyDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getOriginal"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->h:Lkotlin/reflect/jvm/internal/impl/b/ag;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ag;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object p0

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->j:Z

    return v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 388
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->l:Z

    return v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->m:Z

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/y;->n:Z

    return v0
.end method

.method public synthetic z_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/y;->q()Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-result-object v0

    return-object v0
.end method
