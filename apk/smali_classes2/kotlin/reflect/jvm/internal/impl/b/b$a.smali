.class public final enum Lkotlin/reflect/jvm/internal/impl/b/b$a;
.super Ljava/lang/Enum;
.source "CallableMemberDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/b/b$a;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/b/b$a;

.field private static final synthetic e:[Lkotlin/reflect/jvm/internal/impl/b/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;

    const-string v1, "DECLARATION"

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/b/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 36
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;

    const-string v1, "FAKE_OVERRIDE"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 37
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;

    const-string v1, "DELEGATION"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/b/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->c:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 38
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;

    const-string v1, "SYNTHESIZED"

    invoke-direct {v0, v1, v5}, Lkotlin/reflect/jvm/internal/impl/b/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->d:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 34
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/b/b$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b$a;->c:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b$a;->d:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->e:[Lkotlin/reflect/jvm/internal/impl/b/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/b$a;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/b/b$a;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->e:[Lkotlin/reflect/jvm/internal/impl/b/b$a;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/b/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/b/b$a;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
