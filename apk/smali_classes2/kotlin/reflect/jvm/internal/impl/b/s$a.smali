.class public interface abstract Lkotlin/reflect/jvm/internal/impl/b/s$a;
.super Ljava/lang/Object;
.source "FunctionDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/b/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Lkotlin/reflect/jvm/internal/impl/b/s;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/b/aj;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/aj;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/ay;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/b$a;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/b/u;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/u;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ak;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract a(Z)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract b()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract c()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract d()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract e()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<TD;>;"
        }
    .end annotation
.end method

.method public abstract f()Lkotlin/reflect/jvm/internal/impl/b/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation
.end method
