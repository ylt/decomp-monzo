.class public final Lkotlin/reflect/jvm/internal/impl/b/r;
.super Ljava/lang/Object;
.source "descriptorUtil.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lookupLocation"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 32
    :goto_0
    return-object v0

    .line 27
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v2, "fqName.parent()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->c()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    const-string v3, "fqName.shortName()"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-interface {v0, v2, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    goto :goto_0

    .line 32
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v2, "fqName.parent()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/b/r;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    const-string v3, "fqName.shortName()"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-interface {v0, v2, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    :goto_1
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_3

    move-object v0, v1

    :cond_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
