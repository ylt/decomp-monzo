.class public Lkotlin/reflect/jvm/internal/impl/b/b/ad;
.super Lkotlin/reflect/jvm/internal/impl/h/e/i;
.source "SubpackagesScope.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/w;

.field private final b:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;)V
    .locals 1

    .prologue
    const-string v0, "moduleDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fqName"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/h/e/i;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ad;->a:Lkotlin/reflect/jvm/internal/impl/b/w;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ad;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/d;->k:Lkotlin/reflect/jvm/internal/impl/h/e/d$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d$a;->d()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 57
    :goto_0
    return-object v0

    .line 47
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ad;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/h/e/d;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/c$b;->a:Lkotlin/reflect/jvm/internal/impl/h/e/c$b;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ad;->a:Lkotlin/reflect/jvm/internal/impl/b/w;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ad;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-interface {v0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 51
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 52
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    .line 53
    const-string v0, "shortName"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v3}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 54
    check-cast v0, Ljava/util/Collection;

    const-string v4, "shortName"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/ad;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;Ljava/lang/Object;)V

    goto :goto_1

    .line 57
    :cond_3
    check-cast v1, Ljava/util/Collection;

    move-object v0, v1

    goto :goto_0
.end method

.method protected final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/ac;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const-string v1, "name"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return-object v0

    .line 37
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ad;->a:Lkotlin/reflect/jvm/internal/impl/b/w;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ad;->b:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v2, p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "fqName.child(name)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v1

    .line 38
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ac;->g()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 41
    goto :goto_0
.end method
