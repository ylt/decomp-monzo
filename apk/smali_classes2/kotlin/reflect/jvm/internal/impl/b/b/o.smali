.class public abstract Lkotlin/reflect/jvm/internal/impl/b/b/o;
.super Lkotlin/reflect/jvm/internal/impl/b/b/k;
.source "FunctionDescriptorImpl.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation
.end field

.field protected c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lkotlin/reflect/jvm/internal/impl/k/r;

.field private e:Lkotlin/reflect/jvm/internal/impl/b/aj;

.field private f:Lkotlin/reflect/jvm/internal/impl/b/aj;

.field private g:Lkotlin/reflect/jvm/internal/impl/b/u;

.field private h:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            ">;"
        }
    .end annotation
.end field

.field private volatile v:Lkotlin/d/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/a",
            "<",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private final w:Lkotlin/reflect/jvm/internal/impl/b/s;

.field private final x:Lkotlin/reflect/jvm/internal/impl/b/b$a;

.field private y:Lkotlin/reflect/jvm/internal/impl/b/s;


# direct methods
.method protected constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "containingDeclaration"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "annotations"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p5, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kind"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez p6, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "source"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_4
    invoke-direct {p0, p1, p3, p4, p6}, Lkotlin/reflect/jvm/internal/impl/b/b/k;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 40
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->i:Lkotlin/reflect/jvm/internal/impl/b/ay;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->h:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 41
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->i:Z

    .line 42
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->j:Z

    .line 43
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->k:Z

    .line 44
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->l:Z

    .line 45
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->m:Z

    .line 46
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->n:Z

    .line 47
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->o:Z

    .line 51
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->p:Z

    .line 52
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->q:Z

    .line 53
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r:Z

    .line 54
    iput-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->s:Z

    .line 55
    iput-boolean v4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->t:Z

    .line 56
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->u:Ljava/util/Collection;

    .line 57
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->v:Lkotlin/d/a/a;

    .line 60
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->y:Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 63
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->c:Ljava/util/Map;

    .line 74
    if-nez p2, :cond_5

    move-object p2, p0

    :cond_5
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->w:Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 75
    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->x:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 76
    return-void
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/am;ZZ)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/am;",
            "ZZ)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation

    .prologue
    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "unsubstitutedValueParameters"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "getSubstitutedValueParameters"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "substitutor"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "getSubstitutedValueParameters"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 772
    :cond_1
    new-instance v13, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v13, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 773
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 775
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v7

    .line 776
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->m()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 777
    if-nez v1, :cond_2

    const/4 v11, 0x0

    .line 779
    :goto_1
    if-nez v7, :cond_3

    const/4 v1, 0x0

    .line 796
    :goto_2
    return-object v1

    .line 777
    :cond_2
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v11

    goto :goto_1

    .line 780
    :cond_3
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    if-eqz p3, :cond_4

    const/4 v3, 0x0

    :goto_3
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->c()I

    move-result v4

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v6

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->l()Z

    move-result v8

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->o()Z

    move-result v9

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->q()Z

    move-result v10

    if-eqz p4, :cond_5

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/at;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v12

    :goto_4
    move-object v2, p0

    invoke-direct/range {v1 .. v12}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    invoke-interface {v13, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    move-object v3, v2

    goto :goto_3

    :cond_5
    sget-object v12, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    goto :goto_4

    :cond_6
    move-object v1, v13

    .line 796
    goto :goto_2
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/b/b/o;)Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->f:Lkotlin/reflect/jvm/internal/impl/b/aj;

    return-object v0
.end method

.method private a(ZLkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 753
    if-eqz p3, :cond_0

    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v4

    const-string v3, "getSourceToUseForCopy"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 754
    :cond_0
    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/s;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object p3

    :goto_0
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v4

    const-string v3, "getSourceToUseForCopy"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object p3

    goto :goto_0

    :cond_2
    sget-object p3, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    goto :goto_0

    :cond_3
    return-object p3
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/b/s;)V
    .locals 0

    .prologue
    .line 806
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->y:Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 807
    return-void
.end method

.method private l(Z)V
    .locals 0

    .prologue
    .line 152
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->q:Z

    .line 153
    return-void
.end method

.method private o()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->v:Lkotlin/d/a/a;

    .line 196
    if-eqz v0, :cond_0

    .line 197
    invoke-interface {v0}, Lkotlin/d/a/a;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->u:Ljava/util/Collection;

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->v:Lkotlin/d/a/a;

    .line 204
    :cond_0
    return-void
.end method


# virtual methods
.method public A()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 231
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->j:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 237
    :goto_0
    return v0

    .line 233
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 234
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    .line 237
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 349
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->q:Z

    return v0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r:Z

    return v0
.end method

.method public D()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/b/s$a",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 570
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/am;->a:Lkotlin/reflect/jvm/internal/impl/k/am;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->e(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "newCopyBuilder"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public G()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->s:Z

    return v0
.end method

.method protected H()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->e:Lkotlin/reflect/jvm/internal/impl/b/aj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 344
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->e:Lkotlin/reflect/jvm/internal/impl/b/aj;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aj;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_0
.end method

.method public H_()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->t:Z

    return v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/o",
            "<TR;TD;>;TD;)TR;"
        }
    .end annotation

    .prologue
    .line 761
    invoke-interface {p1, p0, p2}, Lkotlin/reflect/jvm/internal/impl/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/o;
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/o;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/b/aj;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/b/u;",
            "Lkotlin/reflect/jvm/internal/impl/b/ay;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/b/o;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "typeParameters"

    aput-object v4, v3, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v2, v3, v5

    const-string v2, "initialize"

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "unsubstitutedValueParameters"

    aput-object v4, v3, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v2, v3, v5

    const-string v2, "initialize"

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p7, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "visibility"

    aput-object v4, v3, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v2, v3, v5

    const-string v2, "initialize"

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_2
    invoke-static {p3}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a:Ljava/util/List;

    .line 89
    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->b:Ljava/util/List;

    .line 90
    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->d:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 91
    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->g:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 92
    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->h:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 93
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->e:Lkotlin/reflect/jvm/internal/impl/b/aj;

    .line 94
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->f:Lkotlin/reflect/jvm/internal/impl/b/aj;

    move v1, v2

    .line 96
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 97
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 98
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->g()I

    move-result v3

    if-eq v3, v1, :cond_3

    .line 99
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " index is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->g()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " but position is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 96
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    move v1, v2

    .line 103
    :goto_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 106
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 107
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->c()I

    move-result v3

    add-int v4, v1, v2

    if-eq v3, v4, :cond_5

    .line 108
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "index is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->c()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " but position is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 103
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 112
    :cond_6
    if-nez p0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v3, v2

    const-string v2, "initialize"

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    return-object p0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    .line 33
    invoke-virtual/range {p0 .. p5}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->c(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "configuration"

    aput-object v4, v2, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v9

    const/4 v3, 0x2

    const-string v4, "doSubstitute"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->a(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->a(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/j;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    .line 588
    :goto_0
    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b:Lkotlin/reflect/jvm/internal/impl/b/m;

    iget-object v2, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->e:Lkotlin/reflect/jvm/internal/impl/b/s;

    iget-object v3, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->f:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    iget-object v4, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->k:Lkotlin/reflect/jvm/internal/impl/e/f;

    iget-boolean v0, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->n:Z

    iget-object v6, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->e:Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v8

    invoke-direct {p0, v0, v6, v8}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(ZLkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/o;

    move-result-object v0

    .line 592
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->c(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->f()Ljava/util/List;

    move-result-object v1

    .line 595
    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 597
    iget-object v2, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-static {v1, v2, v0, v3}, Lkotlin/reflect/jvm/internal/impl/k/h;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/am;

    move-result-object v8

    .line 602
    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->h:Lkotlin/reflect/jvm/internal/impl/k/r;

    if-eqz v1, :cond_4

    .line 603
    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->h:Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v8, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 604
    if-nez v1, :cond_5

    .line 716
    :cond_1
    :goto_2
    return-object v7

    .line 583
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    goto :goto_0

    .line 592
    :cond_3
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->c(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    :cond_4
    move-object v1, v7

    .line 610
    :cond_5
    iget-object v2, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->i:Lkotlin/reflect/jvm/internal/impl/b/aj;

    if-eqz v2, :cond_12

    .line 621
    iget-object v2, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->i:Lkotlin/reflect/jvm/internal/impl/b/aj;

    invoke-interface {v2, v8}, Lkotlin/reflect/jvm/internal/impl/b/aj;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v2

    .line 622
    if-eqz v2, :cond_1

    .line 627
    :goto_3
    iget-object v4, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->g:Ljava/util/List;

    iget-boolean v5, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->o:Z

    iget-boolean v6, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->n:Z

    invoke-static {v0, v4, v8, v5, v6}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/s;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/am;ZZ)Ljava/util/List;

    move-result-object v4

    .line 631
    if-eqz v4, :cond_1

    .line 635
    iget-object v5, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->j:Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {v8, v5, v6}, Lkotlin/reflect/jvm/internal/impl/k/am;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    .line 636
    if-eqz v5, :cond_1

    .line 640
    iget-object v6, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    iget-object v7, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-virtual/range {v0 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/o;

    .line 649
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->i:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Z)V

    .line 650
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->j:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->b(Z)V

    .line 651
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->k:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->c(Z)V

    .line 652
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->l:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->d(Z)V

    .line 653
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->m:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->e(Z)V

    .line 654
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->i(Z)V

    .line 655
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->n:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->f(Z)V

    .line 656
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->o:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->g(Z)V

    .line 657
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->s:Z

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->j(Z)V

    .line 658
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->d(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->h(Z)V

    .line 659
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->e(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Z

    move-result v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->l(Z)V

    .line 661
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->f(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->f(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_4
    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->k(Z)V

    .line 665
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->g(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->c:Ljava/util/Map;

    if-eqz v1, :cond_a

    .line 666
    :cond_6
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->g(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/util/Map;

    move-result-object v2

    .line 668
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->c:Ljava/util/Map;

    if-eqz v1, :cond_9

    .line 669
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 670
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 671
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 661
    :cond_8
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->t:Z

    goto :goto_4

    .line 676
    :cond_9
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v1, v9, :cond_e

    .line 677
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->c:Ljava/util/Map;

    .line 686
    :cond_a
    :goto_6
    iget-boolean v1, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->m:Z

    if-nez v1, :cond_b

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 687
    :cond_b
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    if-eqz v1, :cond_f

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->s()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    .line 688
    :goto_7
    invoke-interface {v1, v8}, Lkotlin/reflect/jvm/internal/impl/b/s;->d(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    .line 689
    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)V

    .line 692
    :cond_c
    iget-boolean v1, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->l:Z

    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/s;->k()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    .line 693
    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/ak;->a()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 694
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->v:Lkotlin/d/a/a;

    .line 695
    if-eqz v1, :cond_10

    .line 696
    iput-object v1, v0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->v:Lkotlin/d/a/a;

    :cond_d
    :goto_8
    move-object v7, v0

    .line 716
    goto/16 :goto_2

    .line 682
    :cond_e
    iput-object v2, v0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->c:Ljava/util/Map;

    goto :goto_6

    :cond_f
    move-object v1, p0

    .line 687
    goto :goto_7

    .line 699
    :cond_10
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->k()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Ljava/util/Collection;)V

    goto :goto_8

    .line 703
    :cond_11
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/b/o$1;

    invoke-direct {v1, p0, v8}, Lkotlin/reflect/jvm/internal/impl/b/b/o$1;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/o;Lkotlin/reflect/jvm/internal/impl/k/am;)V

    iput-object v1, v0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->v:Lkotlin/d/a/a;

    goto :goto_8

    :cond_12
    move-object v2, v7

    goto/16 :goto_3
.end method

.method public a(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "overriddenDescriptors"

    aput-object v4, v2, v3

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "setOverriddenDescriptors"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->u:Ljava/util/Collection;

    .line 286
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->u:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 287
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    iput-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->q:Z

    .line 292
    :cond_2
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/ay;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "visibility"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "setVisibility"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->h:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 117
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "unsubstitutedReturnType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "setReturnType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->d:Lkotlin/reflect/jvm/internal/impl/k/r;

    if-eqz v0, :cond_1

    .line 164
    :cond_1
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->d:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 165
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 120
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->i:Z

    .line 121
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->l:Z

    return v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->d(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 124
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->j:Z

    .line 125
    return-void
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 5

    .prologue
    .line 728
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->D()Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    invoke-interface {v0, p2}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/u;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    invoke-interface {v0, p3}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    invoke-interface {v0, p4}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    invoke-interface {v0, p5}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->a(Z)Lkotlin/reflect/jvm/internal/impl/b/s$a;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s$a;->f()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "copy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 128
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->k:Z

    .line 129
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->m:Z

    return v0
.end method

.method public d()Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->e:Lkotlin/reflect/jvm/internal/impl/b/aj;

    return-object v0
.end method

.method public d(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 5

    .prologue
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "originalSubstitutor"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "substitute"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/am;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->e(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->f()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object p0

    goto :goto_0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 132
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->l:Z

    .line 133
    return-void
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->f:Lkotlin/reflect/jvm/internal/impl/b/aj;

    return-object v0
.end method

.method protected e(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "substitutor"

    aput-object v3, v2, v11

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v12

    const-string v3, "newCopyBuilder"

    aput-object v3, v2, v13

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 575
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/am;->b()Lkotlin/reflect/jvm/internal/impl/k/ak;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->m()Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v4

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v5

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v6

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->i()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->H()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v8

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v9

    const/4 v10, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/o;Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v13, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v3, v2, v11

    const-string v3, "newCopyBuilder"

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 136
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->m:Z

    .line 137
    return-void
.end method

.method public f()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getTypeParameters"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 140
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->n:Z

    .line 141
    return-void
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->d:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public g(Z)V
    .locals 0

    .prologue
    .line 144
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->o:Z

    .line 145
    return-void
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/b/a;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    return-object v0
.end method

.method public h(Z)V
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->p:Z

    .line 149
    return-void
.end method

.method public i()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->b:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getValueParameters"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public i(Z)V
    .locals 0

    .prologue
    .line 156
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r:Z

    .line 157
    return-void
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/b/p;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    return-object v0
.end method

.method public j(Z)V
    .locals 0

    .prologue
    .line 168
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->s:Z

    .line 169
    return-void
.end method

.method public k()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->o()V

    .line 191
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->u:Ljava/util/Collection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->u:Ljava/util/Collection;

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getOverriddenDescriptors"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public k(Z)V
    .locals 0

    .prologue
    .line 172
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->t:Z

    .line 173
    return-void
.end method

.method public synthetic l()Lkotlin/reflect/jvm/internal/impl/b/b;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    return-object v0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 5

    .prologue
    .line 209
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->g:Lkotlin/reflect/jvm/internal/impl/b/u;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getModality"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/b/b$a;
    .locals 5

    .prologue
    .line 330
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->x:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getKind"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 5

    .prologue
    .line 215
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->h:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getVisibility"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 5

    .prologue
    .line 324
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->w:Lkotlin/reflect/jvm/internal/impl/b/s;

    if-ne v0, p0, :cond_0

    :goto_0
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getOriginal"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->w:Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object p0

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->y:Lkotlin/reflect/jvm/internal/impl/b/s;

    return-object v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->n:Z

    return v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->o:Z

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 242
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->k:Z

    return v0
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->p:Z

    return v0
.end method

.method public z()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 220
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o;->i:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 226
    :goto_0
    return v0

    .line 222
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 223
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    .line 226
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic z_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->r()Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    return-object v0
.end method
