.class public abstract Lkotlin/reflect/jvm/internal/impl/b/ay;
.super Ljava/lang/Object;
.source "Visibility.kt"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/ay;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lkotlin/reflect/jvm/internal/impl/b/ay;->b:Z

    return-void
.end method


# virtual methods
.method protected a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;
    .locals 1

    .prologue
    const-string v0, "visibility"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ay;->a:Ljava/lang/String;

    return-object v0
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 0

    .prologue
    .line 64
    return-object p0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ay;->b:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/ay;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
