.class public abstract Lkotlin/reflect/jvm/internal/impl/b/b/d;
.super Lkotlin/reflect/jvm/internal/impl/b/b/k;
.source "AbstractTypeAliasDescriptor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/ap;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/reflect/jvm/internal/impl/b/b/d$b;

.field private final c:Lkotlin/reflect/jvm/internal/impl/b/ay;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ay;)V
    .locals 1

    .prologue
    const-string v0, "containingDeclaration"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sourceElement"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "visibilityImpl"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/b/b/k;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 86
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/d;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d;->b:Lkotlin/reflect/jvm/internal/impl/b/b/d$b;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/o",
            "<TR;TD;>;TD;)TR;"
        }
    .end annotation

    .prologue
    const-string v0, "visitor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    invoke-interface {p1, p0, p2}, Lkotlin/reflect/jvm/internal/impl/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/ap;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "declaredTypeParameters"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d;->a:Ljava/util/List;

    .line 44
    return-void
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d;->b:Lkotlin/reflect/jvm/internal/impl/b/b/d$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d;->g()Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/b/ap;
    .locals 2

    .prologue
    .line 79
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/k;->j()Lkotlin/reflect/jvm/internal/impl/b/p;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.TypeAliasDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    return-object v0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/b/p;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d;->g()Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/p;

    return-object v0
.end method

.method protected abstract k()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation
.end method

.method protected final l()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 84
    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->g()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/an;->a(Lkotlin/reflect/jvm/internal/impl/b/h;Lkotlin/reflect/jvm/internal/impl/h/e/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "TypeUtils.makeUnsubstitu\u2026ope ?: MemberScope.Empty)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/e/h$b;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h$b;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    return-object v0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d;->a()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/b/d$a;

    invoke-direct {v1, p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d$a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/d;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/an;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/d/a/b;)Z

    move-result v0

    .line 59
    return v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "typealias "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public y()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d;->a:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "declaredTypeParametersImpl"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public synthetic z_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d;->g()Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method
