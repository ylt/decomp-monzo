.class public Lkotlin/reflect/jvm/internal/impl/b/ax;
.super Ljava/lang/Object;
.source "Visibilities.java"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final c:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final d:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final e:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final f:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final g:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final h:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final i:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ay;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field public static final l:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

.field public static final m:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ay;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final o:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

.field private static final p:Lkotlin/reflect/jvm/internal/impl/l/g;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$1;

    const-string v1, "private"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/ax$1;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 115
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$5;

    const-string v1, "private_to_this"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/ax$5;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->b:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 145
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$6;

    const-string v1, "protected"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/b/ax$6;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 211
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$7;

    const-string v1, "internal"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/ax$7;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 227
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$8;

    const-string v1, "public"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/b/ax$8;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 240
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$9;

    const-string v1, "local"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/ax$9;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 253
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$10;

    const-string v1, "inherited"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/ax$10;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->g:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 267
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$11;

    const-string v1, "invisible_fake"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/ax$11;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->h:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 282
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$12;

    const-string v1, "unknown"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/ax$12;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->i:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 296
    new-array v0, v6, [Lkotlin/reflect/jvm/internal/impl/b/ay;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->b:Lkotlin/reflect/jvm/internal/impl/b/ay;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/ah;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->j:Ljava/util/Set;

    .line 347
    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(I)Ljava/util/HashMap;

    move-result-object v0

    .line 348
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->b:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->n:Ljava/util/Map;

    .line 381
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->k:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 387
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$2;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/b/ax$2;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->o:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    .line 399
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$3;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/b/ax$3;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->l:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    .line 409
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ax$4;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/b/ax$4;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->m:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    .line 425
    const-class v0, Lkotlin/reflect/jvm/internal/impl/l/g;

    const-class v1, Lkotlin/reflect/jvm/internal/impl/l/g;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/ServiceLoader;->load(Ljava/lang/Class;Ljava/lang/ClassLoader;)Ljava/util/ServiceLoader;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ServiceLoader;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 426
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/l/g;

    :goto_0
    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->p:Lkotlin/reflect/jvm/internal/impl/l/g;

    .line 427
    return-void

    .line 426
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/l/g$a;->a:Lkotlin/reflect/jvm/internal/impl/l/g$a;

    goto :goto_0
.end method

.method static a(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "first"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v3, v2, v5

    const-string v3, "compareLocal"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "second"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v3, v2, v5

    const-string v3, "compareLocal"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_1
    if-ne p0, p1, :cond_2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 365
    :goto_0
    return-object v0

    .line 360
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->n:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 361
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->n:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 362
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 363
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 365
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/q;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "what"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v3, v2, v5

    const-string v3, "findInvisibleMember"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "from"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v3, v2, v5

    const-string v3, "findInvisibleMember"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/q;->z_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/q;

    .line 328
    :goto_0
    if-eqz v0, :cond_4

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/q;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-eq v1, v2, :cond_4

    .line 329
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/q;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/b/ay;->a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 341
    :cond_2
    :goto_1
    return-object v0

    .line 332
    :cond_3
    const-class v1, Lkotlin/reflect/jvm/internal/impl/b/q;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/q;

    goto :goto_0

    .line 335
    :cond_4
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/b/ae;

    if-eqz v0, :cond_5

    .line 336
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/b/ae;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/b/ae;->E()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/q;

    move-result-object v0

    .line 338
    if-nez v0, :cond_2

    .line 341
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a()Lkotlin/reflect/jvm/internal/impl/h/e/a/e;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->o:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "visibility"

    aput-object v5, v4, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v0, v4, v1

    const/4 v0, 0x2

    const-string v1, "isPrivate"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 418
    :cond_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-eq p0, v2, :cond_1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->b:Lkotlin/reflect/jvm/internal/impl/b/ay;

    if-ne p0, v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "what"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v1, v4, v0

    const-string v0, "isVisibleIgnoringReceiver"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "from"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v1, v4, v0

    const-string v0, "isVisibleIgnoringReceiver"

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 310
    :cond_1
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/ax;->l:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    invoke-static {v2, p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/q;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static b(Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "first"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v3, v2, v5

    const-string v3, "compare"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "second"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities"

    aput-object v3, v2, v5

    const-string v3, "compare"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :cond_1
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/ay;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;

    move-result-object v0

    .line 371
    if-eqz v0, :cond_2

    .line 378
    :goto_0
    return-object v0

    .line 374
    :cond_2
    invoke-virtual {p1, p0}, Lkotlin/reflect/jvm/internal/impl/b/ay;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Ljava/lang/Integer;

    move-result-object v0

    .line 375
    if-eqz v0, :cond_3

    .line 376
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    neg-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 378
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Lkotlin/reflect/jvm/internal/impl/l/g;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->p:Lkotlin/reflect/jvm/internal/impl/l/g;

    return-object v0
.end method
