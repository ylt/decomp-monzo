.class public Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
.super Ljava/lang/Object;
.source "FunctionDescriptorImpl.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/s$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/b/b/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/b/s$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/s;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Lkotlin/reflect/jvm/internal/impl/k/ak;

.field protected b:Lkotlin/reflect/jvm/internal/impl/b/m;

.field protected c:Lkotlin/reflect/jvm/internal/impl/b/u;

.field protected d:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field protected e:Lkotlin/reflect/jvm/internal/impl/b/s;

.field protected f:Lkotlin/reflect/jvm/internal/impl/b/b$a;

.field protected g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation
.end field

.field protected h:Lkotlin/reflect/jvm/internal/impl/k/r;

.field protected i:Lkotlin/reflect/jvm/internal/impl/b/aj;

.field protected j:Lkotlin/reflect/jvm/internal/impl/k/r;

.field protected k:Lkotlin/reflect/jvm/internal/impl/e/f;

.field protected l:Z

.field protected m:Z

.field protected n:Z

.field protected o:Z

.field final synthetic p:Lkotlin/reflect/jvm/internal/impl/b/b/o;

.field private q:Z

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lkotlin/reflect/jvm/internal/impl/b/a/h;

.field private t:Z

.field private u:Lkotlin/reflect/jvm/internal/impl/b/al;

.field private v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/b/o;Lkotlin/reflect/jvm/internal/impl/k/ak;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/b$a;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/e/f;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/ak;",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Lkotlin/reflect/jvm/internal/impl/b/u;",
            "Lkotlin/reflect/jvm/internal/impl/b/ay;",
            "Lkotlin/reflect/jvm/internal/impl/b/b$a;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")V"
        }
    .end annotation

    .prologue
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "substitution"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newOwner"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newModality"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p5, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newVisibility"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-nez p6, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kind"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    if-nez p7, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newValueParameterDescriptors"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    if-nez p9, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "newReturnType"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "<init>"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 386
    :cond_6
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->p:Lkotlin/reflect/jvm/internal/impl/b/b/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->e:Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 361
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->p:Lkotlin/reflect/jvm/internal/impl/b/b/o;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/b/o;)Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->i:Lkotlin/reflect/jvm/internal/impl/b/aj;

    .line 364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->l:Z

    .line 365
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->m:Z

    .line 366
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->n:Z

    .line 367
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->o:Z

    .line 368
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->p:Lkotlin/reflect/jvm/internal/impl/b/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->y()Z

    move-result v0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->q:Z

    .line 369
    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->r:Ljava/util/List;

    .line 370
    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->s:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    .line 371
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->p:Lkotlin/reflect/jvm/internal/impl/b/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->B()Z

    move-result v0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->t:Z

    .line 373
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->v:Ljava/util/Map;

    .line 374
    const/4 v0, 0x0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->w:Ljava/lang/Boolean;

    .line 387
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    .line 388
    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b:Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 389
    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 390
    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 391
    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->f:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 392
    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->g:Ljava/util/List;

    .line 393
    iput-object p8, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->h:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 394
    iput-object p9, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->j:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 395
    iput-object p10, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->k:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 396
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->s:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->u:Lkotlin/reflect/jvm/internal/impl/b/al;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->r:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Z
    .locals 1

    .prologue
    .line 352
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->q:Z

    return v0
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Z
    .locals 1

    .prologue
    .line 352
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->t:Z

    return v0
.end method

.method static synthetic f(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->w:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic g(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->v:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 5

    .prologue
    .line 478
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->e:Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 479
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "setOriginal"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->g()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/aj;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/b/aj;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/u;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/b/u;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Z)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Z)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/b/o$a;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "parameters"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setValueParameters"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 443
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->g:Ljava/util/List;

    .line 444
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setValueParameters"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "additionalAnnotations"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setAdditionalAnnotations"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 527
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->s:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    .line 528
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setAdditionalAnnotations"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/aj;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 5

    .prologue
    .line 471
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->i:Lkotlin/reflect/jvm/internal/impl/b/aj;

    .line 472
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "setDispatchReceiverParameter"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "visibility"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setVisibility"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 416
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setVisibility"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "kind"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setKind"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->f:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 423
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setKind"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "owner"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setOwner"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 401
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b:Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 402
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setOwner"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/b/u;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "modality"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setModality"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 409
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setModality"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "name"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setName"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->k:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 437
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setName"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/ak;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "substitution"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setSubstitution"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 539
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->a:Lkotlin/reflect/jvm/internal/impl/k/ak;

    .line 540
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setSubstitution"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public b(Z)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 5

    .prologue
    .line 429
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->l:Z

    .line 430
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "setCopyOverrides"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->h()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "type"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v5

    const-string v3, "setReturnType"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 457
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->j:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 458
    if-nez p0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v3, v2, v4

    const-string v3, "setReturnType"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object p0
.end method

.method public c(Z)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 1

    .prologue
    .line 532
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->w:Ljava/lang/Boolean;

    .line 533
    return-object p0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->i()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public d(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 5

    .prologue
    .line 464
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->h:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 465
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "setExtensionReceiverType"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->j()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/b/s$a;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->k()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->p:Lkotlin/reflect/jvm/internal/impl/b/b/o;

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/b/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 485
    iput-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->m:Z

    .line 486
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const-string v3, "setSignatureChange"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 492
    iput-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->n:Z

    .line 493
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const-string v3, "setPreserveSourceElement"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 506
    iput-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->o:Z

    .line 507
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const-string v3, "setDropOriginalInContainingParts"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 513
    iput-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->q:Z

    .line 514
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const-string v3, "setHiddenToOvercomeSignatureClash"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/b/b/o$a;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 520
    iput-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->t:Z

    .line 521
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/impl/FunctionDescriptorImpl$CopyConfiguration"

    aput-object v4, v2, v3

    const-string v3, "setHiddenForResolutionEverywhereBesideSupercalls"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object p0
.end method
