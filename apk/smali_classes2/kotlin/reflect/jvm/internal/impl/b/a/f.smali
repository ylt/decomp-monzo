.class public final Lkotlin/reflect/jvm/internal/impl/b/a/f;
.super Ljava/lang/Object;
.source "annotationUtil.kt"


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v1, "kotlin.internal.InlineOnly"

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 4

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->I:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 63
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/a/d;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v1, v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/a/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 18

    .prologue
    const-string v2, "$receiver"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "message"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "replaceWith"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "level"

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->o()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v2

    .line 36
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/e;->o()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v4

    .line 38
    const-string v3, "ReplaceWith"

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v5

    .line 40
    invoke-interface {v5}, Lkotlin/reflect/jvm/internal/impl/b/e;->o()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v6

    .line 41
    new-instance v8, Lkotlin/reflect/jvm/internal/impl/b/a/d;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    const/4 v3, 0x3

    new-array v9, v3, [Lkotlin/h;

    const/4 v7, 0x0

    move-object v3, v4

    check-cast v3, Ljava/util/Collection;

    const-string v10, "message"

    invoke-static {v3, v10}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Ljava/util/Collection;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v3

    new-instance v10, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v10, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/s;-><init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/a/l;)V

    invoke-static {v3, v10}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v9, v7

    const/4 v10, 0x1

    move-object v3, v4

    check-cast v3, Ljava/util/Collection;

    const-string v7, "replaceWith"

    invoke-static {v3, v7}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Ljava/util/Collection;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v11

    new-instance v12, Lkotlin/reflect/jvm/internal/impl/h/b/a;

    new-instance v7, Lkotlin/reflect/jvm/internal/impl/b/a/d;

    invoke-interface {v5}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/k/r;

    const/4 v5, 0x2

    new-array v13, v5, [Lkotlin/h;

    const/4 v14, 0x0

    move-object v5, v6

    check-cast v5, Ljava/util/Collection;

    const-string v15, "expression"

    invoke-static {v5, v15}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Ljava/util/Collection;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v5

    new-instance v15, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-direct {v15, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/s;-><init>(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/a/l;)V

    invoke-static {v5, v15}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v5

    aput-object v5, v13, v14

    const/4 v14, 0x1

    check-cast v6, Ljava/util/Collection;

    const-string v5, "imports"

    invoke-static {v6, v5}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Ljava/util/Collection;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v6

    new-instance v15, Lkotlin/reflect/jvm/internal/impl/h/b/b;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v16

    sget-object v17, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/a/l;->J()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v5

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v5

    const-string v17, "getArrayType(Variance.INVARIANT, stringType)"

    move-object/from16 v0, v17

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v15, v0, v5, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/b;-><init>(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/a/l;)V

    invoke-static {v6, v15}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v5

    aput-object v5, v13, v14

    invoke-static {v13}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v7, v3, v5, v6}, Lkotlin/reflect/jvm/internal/impl/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v3, v7

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    invoke-direct {v12, v3}, Lkotlin/reflect/jvm/internal/impl/h/b/a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;)V

    invoke-static {v11, v12}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v3

    aput-object v3, v9, v10

    const/4 v3, 0x2

    check-cast v4, Ljava/util/Collection;

    const-string v5, "level"

    invoke-static {v4, v5}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Ljava/util/Collection;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v4

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/h/b/i;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-direct {v5, v6}, Lkotlin/reflect/jvm/internal/impl/h/b/i;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;)V

    invoke-static {v4, v5}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v4

    aput-object v4, v9, v3

    invoke-static {v9}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v8, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v2, v8

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    return-object v2

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deprecation level "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/a/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_0

    .line 32
    const-string p2, ""

    :cond_0
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_1

    .line 33
    const-string p3, "WARNING"

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    return-object v0
.end method

.method private static final a(Ljava/util/Collection;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/at;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/b/at;"
        }
    .end annotation

    .prologue
    .line 69
    check-cast p0, Ljava/lang/Iterable;

    .line 87
    const/4 v2, 0x0

    .line 88
    const/4 v3, 0x0

    .line 89
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    .line 90
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 69
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    if-eqz v3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Collection contains more than one matching element."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 93
    :cond_0
    const/4 v0, 0x1

    :goto_1
    move v3, v0

    move-object v2, v1

    .line 89
    goto :goto_0

    .line 96
    :cond_1
    if-nez v3, :cond_2

    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Collection contains no element matching the predicate."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    move-object v0, v2

    .line 97
    check-cast v0, Ljava/lang/Object;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 69
    return-object v0

    :cond_3
    move v0, v3

    move-object v1, v2

    goto :goto_1
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 2

    .prologue
    .line 83
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->f()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 98
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 83
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    .line 99
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/t;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->b(Lkotlin/reflect/jvm/internal/impl/b/t;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/b/b;)Z
    .locals 2

    .prologue
    .line 85
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/b;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/f;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    return v0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/b/t;)Z
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/s;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    .line 78
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Lkotlin/reflect/jvm/internal/impl/b/b;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/f;->b(Lkotlin/reflect/jvm/internal/impl/b/b;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    :cond_1
    move-object v0, p0

    .line 79
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/s;->a()Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Function is not inline: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 80
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
