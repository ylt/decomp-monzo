.class final Lkotlin/reflect/jvm/internal/impl/b/ar$b;
.super Lkotlin/d/b/m;
.source "typeParameterUtils.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/b/ar;->a(Lkotlin/reflect/jvm/internal/impl/b/i;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/m;",
        "Lkotlin/g/g",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/b/aq;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/b/ar$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ar$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/b/ar$b;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/ar$b;->a:Lkotlin/reflect/jvm/internal/impl/b/ar$b;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/ar$b;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/g/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/g/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ")",
            "Lkotlin/g/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    if-nez p1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.CallableDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/a;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a;->f()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->q(Ljava/lang/Iterable;)Lkotlin/g/g;

    move-result-object v0

    return-object v0
.end method
