.class public final Lkotlin/reflect/jvm/internal/impl/b/a/g;
.super Ljava/lang/Object;
.source "AnnotationWithTarget.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/a/c;

.field private final b:Lkotlin/reflect/jvm/internal/impl/b/a/e;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)V
    .locals 1

    .prologue
    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/a/c;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/a/c;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/b/a/e;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/a/c;

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/b/a/e;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/a/c;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/b/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/a/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/b/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/a/c;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnnotationWithTarget(annotation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->a:Lkotlin/reflect/jvm/internal/impl/b/a/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/a/g;->b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
