.class final Lkotlin/reflect/jvm/internal/impl/b/ax$6;
.super Lkotlin/reflect/jvm/internal/impl/b/ay;
.source "Visibilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/b/ax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/ay;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "whatDeclaration"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$3"

    aput-object v1, v4, v2

    const-string v1, "doesReceiverFitForProtectedVisibility"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "fromClass"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$3"

    aput-object v1, v4, v2

    const-string v1, "doesReceiverFitForProtectedVisibility"

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->m:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    if-ne p1, v0, :cond_3

    move v2, v1

    .line 206
    :cond_2
    :goto_0
    return v2

    .line 194
    :cond_3
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/b;

    if-eqz v0, :cond_2

    .line 196
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/l;

    if-nez v0, :cond_2

    .line 199
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->l:Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    if-eq p1, v0, :cond_2

    .line 200
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/b/ax;->a()Lkotlin/reflect/jvm/internal/impl/h/e/a/e;

    move-result-object v0

    if-eq p1, v0, :cond_4

    if-nez p1, :cond_5

    :cond_4
    move v2, v1

    goto :goto_0

    .line 202
    :cond_5
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/h/e/a/f;

    if-eqz v0, :cond_7

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/h/e/a/f;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/h/e/a/f;->b()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 206
    :goto_1
    invoke-static {v0, p3}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/k;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    move v0, v2

    :goto_2
    move v2, v0

    goto :goto_0

    .line 202
    :cond_7
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/h/e/a/e;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    goto :goto_1

    :cond_8
    move v0, v1

    .line 206
    goto :goto_2
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "what"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$3"

    aput-object v2, v4, v3

    const-string v2, "isVisible"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "from"

    aput-object v5, v4, v2

    const-string v2, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$3"

    aput-object v2, v4, v3

    const-string v2, "isVisible"

    aput-object v2, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_1
    const-class v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {p2, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 158
    const-class v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {p3, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;Z)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 159
    if-nez v1, :cond_2

    move v0, v2

    .line 182
    :goto_0
    return v0

    .line 161
    :cond_2
    if-eqz v0, :cond_3

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->i(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 165
    const-class v4, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v0, v4}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 166
    if-eqz v0, :cond_3

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    goto :goto_0

    .line 172
    :cond_3
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/q;)Lkotlin/reflect/jvm/internal/impl/b/q;

    move-result-object v4

    .line 174
    const-class v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 175
    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    .line 177
    :cond_4
    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p1, v4, v1}, Lkotlin/reflect/jvm/internal/impl/b/ax$6;->a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v3

    .line 179
    goto :goto_0

    .line 182
    :cond_5
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/b/ax$6;->a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    goto :goto_0
.end method
