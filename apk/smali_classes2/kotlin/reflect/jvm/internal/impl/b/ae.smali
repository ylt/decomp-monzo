.class public final Lkotlin/reflect/jvm/internal/impl/b/ae;
.super Ljava/lang/Object;
.source "typeParameterUtils.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/i;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/b/ae;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/i;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/ae;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/i;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/ae;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "classifierDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "arguments"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/ae;->a:Lkotlin/reflect/jvm/internal/impl/b/i;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/ae;->b:Ljava/util/List;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/b/ae;->c:Lkotlin/reflect/jvm/internal/impl/b/ae;

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/b/i;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ae;->a:Lkotlin/reflect/jvm/internal/impl/b/i;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ae;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/b/ae;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/ae;->c:Lkotlin/reflect/jvm/internal/impl/b/ae;

    return-object v0
.end method
