.class public final Lkotlin/reflect/jvm/internal/impl/b/b/q;
.super Lkotlin/reflect/jvm/internal/impl/b/b/j;
.source "LazyPackageViewDescriptorImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/ac;


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final c:Lkotlin/reflect/jvm/internal/impl/h/e/h;

.field private final d:Lkotlin/reflect/jvm/internal/impl/b/b/u;

.field private final e:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/b/b/q;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "fragments"

    const-string v5, "getFragments()Ljava/util/List;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/b/q;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/b/u;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/j/i;)V
    .locals 2

    .prologue
    const-string v0, "module"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fqName"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storageManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/e/b;->f()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    .line 36
    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q;->d:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 38
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/q$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q$a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/q;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {p3, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 42
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/e/g;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/q;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {p3, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/g;-><init>(Lkotlin/reflect/jvm/internal/impl/j/f;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q;->c:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/o",
            "<TR;TD;>;TD;)TR;"
        }
    .end annotation

    .prologue
    const-string v0, "visitor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ac;

    invoke-interface {p1, p0, p2}, Lkotlin/reflect/jvm/internal/impl/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/ac;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    const-string v0, "substitutor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object p0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q;->c:Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->j()Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/w;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/ac;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ac;

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->j()Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->e()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 58
    goto :goto_1

    :cond_1
    move v0, v1

    .line 59
    goto :goto_1

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b/q;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/b/ac$a;->a(Lkotlin/reflect/jvm/internal/impl/b/ac;)Z

    move-result v0

    return v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/b/ac;
    .locals 3

    .prologue
    .line 54
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->j()Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    const-string v2, "fqName.parent()"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->j()Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->hashCode()I

    move-result v0

    .line 64
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    return v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/b/b/u;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q;->d:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    return-object v0
.end method

.method public synthetic y_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->h()Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method
