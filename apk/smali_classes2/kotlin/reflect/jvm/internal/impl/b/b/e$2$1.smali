.class Lkotlin/reflect/jvm/internal/impl/b/b/e$2$1;
.super Ljava/lang/Object;
.source "AbstractTypeParameterDescriptor.java"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/b/b/e$2;->b()Lkotlin/reflect/jvm/internal/impl/k/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/d/a/a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/b/b/e$2;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/b/e$2;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/e$2$1;->a:Lkotlin/reflect/jvm/internal/impl/b/b/e$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Scope for type parameter "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/e$2$1;->a:Lkotlin/reflect/jvm/internal/impl/b/b/e$2;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/b/b/e$2;->b:Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/e$2$1;->a:Lkotlin/reflect/jvm/internal/impl/b/b/e$2;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/b/b/e$2;->c:Lkotlin/reflect/jvm/internal/impl/b/b/e;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/b/e;->I_()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/m;->a(Ljava/lang/String;Ljava/util/Collection;)Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/e$2$1;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    return-object v0
.end method
