.class public final enum Lkotlin/reflect/jvm/internal/impl/b/f;
.super Ljava/lang/Enum;
.source "ClassKind.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/b/f;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/b/f;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/b/f;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/b/f;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/b/f;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/b/f;

.field private static final synthetic g:[Lkotlin/reflect/jvm/internal/impl/b/f;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/f;

    const-string v1, "CLASS"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 21
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/f;

    const-string v1, "INTERFACE"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 22
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/f;

    const-string v1, "ENUM_CLASS"

    invoke-direct {v0, v1, v5}, Lkotlin/reflect/jvm/internal/impl/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 23
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/f;

    const-string v1, "ENUM_ENTRY"

    invoke-direct {v0, v1, v6}, Lkotlin/reflect/jvm/internal/impl/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->d:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 24
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/f;

    const-string v1, "ANNOTATION_CLASS"

    invoke-direct {v0, v1, v7}, Lkotlin/reflect/jvm/internal/impl/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 25
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/f;

    const-string v1, "OBJECT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 19
    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/b/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->d:Lkotlin/reflect/jvm/internal/impl/b/f;

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    aput-object v2, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->g:[Lkotlin/reflect/jvm/internal/impl/b/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/f;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/f;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/b/f;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->g:[Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/b/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/b/f;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    if-eq p0, v0, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->d:Lkotlin/reflect/jvm/internal/impl/b/f;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
