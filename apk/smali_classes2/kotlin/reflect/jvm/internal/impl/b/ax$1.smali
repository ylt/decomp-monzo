.class final Lkotlin/reflect/jvm/internal/impl/b/ax$1;
.super Lkotlin/reflect/jvm/internal/impl/b/ay;
.source "Visibilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/b/ax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/ay;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "descriptor"

    aput-object v5, v4, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$1"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "hasContainingSourceFile"

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 53
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->r(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/am;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/am;->a:Lkotlin/reflect/jvm/internal/impl/b/am;

    if-eq v2, v3, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "what"

    aput-object v4, v3, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$1"

    aput-object v0, v3, v5

    const-string v0, "inSameFile"

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "from"

    aput-object v4, v3, v0

    const-string v0, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$1"

    aput-object v0, v3, v5

    const-string v0, "inSameFile"

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 45
    :cond_1
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/c;->r(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/am;

    move-result-object v1

    .line 46
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/am;->a:Lkotlin/reflect/jvm/internal/impl/b/am;

    if-eq v1, v2, :cond_2

    .line 47
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c;->r(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/am;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 49
    :cond_2
    return v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "what"

    aput-object v5, v2, v4

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$1"

    aput-object v4, v2, v3

    const-string v3, "isVisible"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v5, "from"

    aput-object v5, v2, v4

    const-string v4, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$1"

    aput-object v4, v2, v3

    const-string v3, "isVisible"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/c;->f(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p3}, Lkotlin/reflect/jvm/internal/impl/b/ax$1;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 59
    invoke-direct {p0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/b/ax$1;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v3

    .line 96
    :cond_2
    :goto_0
    return v3

    .line 62
    :cond_3
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/b/l;

    if-eqz v0, :cond_4

    move-object v0, p2

    .line 63
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/l;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/l;->o()Lkotlin/reflect/jvm/internal/impl/b/i;

    move-result-object v0

    .line 64
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->j(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->f(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_4

    instance-of v0, p3, Lkotlin/reflect/jvm/internal/impl/b/l;

    if-eqz v0, :cond_4

    invoke-interface {p3}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->f(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/b/ax$1;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_4
    move-object v1, p2

    .line 74
    :cond_5
    if-eqz v1, :cond_7

    .line 75
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    .line 76
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_6

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/h/c;->i(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_5

    .line 81
    :cond_7
    if-nez v1, :cond_8

    move v3, v4

    .line 82
    goto :goto_0

    :cond_8
    move-object v2, p3

    .line 85
    :goto_1
    if-eqz v2, :cond_b

    .line 86
    if-eq v1, v2, :cond_2

    .line 89
    instance-of v0, v2, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_a

    .line 90
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_9

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v5

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v5, v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/h/c;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v3

    :goto_2
    move v3, v0

    goto :goto_0

    :cond_9
    move v0, v4

    goto :goto_2

    .line 94
    :cond_a
    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/m;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    goto :goto_1

    :cond_b
    move v3, v4

    .line 96
    goto :goto_0
.end method
