.class final Lkotlin/reflect/jvm/internal/impl/b/b/q$b;
.super Lkotlin/d/b/m;
.source "LazyPackageViewDescriptorImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/b/b/q;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/u;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/j/i;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/b/b/q;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/b/q;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/q;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/q;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/h$b;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    .line 50
    :goto_0
    return-object v0

    .line 48
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/q;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->f()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 74
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 75
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    .line 48
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/y;->x_()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 76
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/b/ad;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/q;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->j()Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/w;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/q;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lkotlin/reflect/jvm/internal/impl/b/b/ad;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    .line 48
    invoke-static {v1, v2}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 49
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "package view scope for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/q;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/q;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/b/q;->j()Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/b;-><init>(Ljava/lang/String;Ljava/util/List;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    goto :goto_0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/q$b;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    return-object v0
.end method
