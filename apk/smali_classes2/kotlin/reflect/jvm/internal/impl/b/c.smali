.class final Lkotlin/reflect/jvm/internal/impl/b/c;
.super Ljava/lang/Object;
.source "typeParameterUtils.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/aq;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/aq;

.field private final b:Lkotlin/reflect/jvm/internal/impl/b/m;

.field private final c:I


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/m;I)V
    .locals 1

    .prologue
    const-string v0, "originalDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "declarationDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->b:Lkotlin/reflect/jvm/internal/impl/b/m;

    iput p3, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->c:I

    return-void
.end method


# virtual methods
.method public D_()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    return-object v0
.end method

.method public E_()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public I_()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->I_()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/o",
            "<TR;TD;>;TD;)TR;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/aq;->a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 1

    const-string v0, "substitutor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/c;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/b/aq;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->c()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    return-object v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/c;->c()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method public g()I
    .locals 2

    .prologue
    .line 58
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->c:I

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/aq;->g()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->k()Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v0

    return-object v0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->l()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[inner-copy]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->a:Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->x()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v0

    return-object v0
.end method

.method public y_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/c;->b:Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public synthetic z_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/c;->c()Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method
