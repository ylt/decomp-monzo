.class public final Lkotlin/reflect/jvm/internal/impl/b/b/d$b;
.super Ljava/lang/Object;
.source "AbstractTypeAliasDescriptor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/k/ad;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/b/b/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ay;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/b/b/d;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/b/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 86
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public G_()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;->a()Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ap;->a()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "declarationDescriptor.un\u2026pe.constructor.supertypes"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/b/ap;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;->a:Lkotlin/reflect/jvm/internal/impl/b/b/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/d;->k()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;->a()Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;->a()Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ap;->a()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->e()Z

    move-result v0

    return v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/a/l;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;->a()Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[typealias "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/d$b;->a()Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/ap;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
