.class public final enum Lkotlin/reflect/jvm/internal/impl/b/a/e;
.super Ljava/lang/Enum;
.source "AnnotationUseSiteTarget.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/b/a/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/a/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final enum g:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final enum h:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final enum i:Lkotlin/reflect/jvm/internal/impl/b/a/e;

.field public static final j:Lkotlin/reflect/jvm/internal/impl/b/a/e$a;

.field private static final synthetic k:[Lkotlin/reflect/jvm/internal/impl/b/a/e;


# instance fields
.field private final l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x9

    new-array v6, v0, [Lkotlin/reflect/jvm/internal/impl/b/a/e;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v1, "FIELD"

    move-object v5, v3

    .line 22
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->a:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v0, v6, v2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v1, "FILE"

    move v2, v4

    move-object v5, v3

    .line 23
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->b:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v0, v6, v4

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v1, "PROPERTY"

    move v2, v7

    move-object v5, v3

    .line 24
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->c:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v0, v6, v7

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v1, "PROPERTY_GETTER"

    const-string v2, "get"

    .line 25
    invoke-direct {v0, v1, v8, v2}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->d:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v0, v6, v8

    const/4 v0, 0x4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v2, "PROPERTY_SETTER"

    const/4 v5, 0x4

    const-string v7, "set"

    .line 26
    invoke-direct {v1, v2, v5, v7}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/e;->e:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v1, v6, v0

    const/4 v7, 0x5

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v1, "RECEIVER"

    const/4 v2, 0x5

    move-object v5, v3

    .line 27
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;ILkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->f:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v0, v6, v7

    const/4 v0, 0x6

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v2, "CONSTRUCTOR_PARAMETER"

    const/4 v4, 0x6

    const-string v5, "param"

    .line 28
    invoke-direct {v1, v2, v4, v5}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v1, v6, v0

    const/4 v0, 0x7

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v2, "SETTER_PARAMETER"

    const/4 v4, 0x7

    const-string v5, "setparam"

    .line 29
    invoke-direct {v1, v2, v4, v5}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/e;->h:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v1, v6, v0

    const/16 v0, 0x8

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    const-string v2, "PROPERTY_DELEGATE_FIELD"

    const/16 v4, 0x8

    const-string v5, "delegate"

    .line 30
    invoke-direct {v1, v2, v4, v5}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/e;->i:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    aput-object v1, v6, v0

    sput-object v6, Lkotlin/reflect/jvm/internal/impl/b/a/e;->k:[Lkotlin/reflect/jvm/internal/impl/b/a/e;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/e$a;

    invoke-direct {v0, v3}, Lkotlin/reflect/jvm/internal/impl/b/a/e$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->j:Lkotlin/reflect/jvm/internal/impl/b/a/e$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    if-eqz p3, :cond_0

    :goto_0
    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->l:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/a/e;->name()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p3

    const-string v0, "(this as java.lang.String).toLowerCase()"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/b/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void

    :cond_0
    move-object v0, p3

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/a/e;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/b/a/e;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->k:[Lkotlin/reflect/jvm/internal/impl/b/a/e;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/b/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/b/a/e;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/a/e;->l:Ljava/lang/String;

    return-object v0
.end method
