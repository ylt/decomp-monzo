.class public Lkotlin/reflect/jvm/internal/impl/b/b/ag;
.super Lkotlin/reflect/jvm/internal/impl/b/b/ah;
.source "ValueParameterDescriptorImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/at;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/b/b/ag$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/b/b/ag$a;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/b/at;

.field private final e:I

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Lkotlin/reflect/jvm/internal/impl/k/r;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/ag$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/ag$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->a:Lkotlin/reflect/jvm/internal/impl/b/b/ag$a;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V
    .locals 7

    .prologue
    const-string v1, "containingDeclaration"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annotations"

    invoke-static {p4, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "name"

    invoke-static {p5, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "outType"

    invoke-static {p6, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "source"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    .line 25
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    move-object v1, p0

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p11

    .line 37
    invoke-direct/range {v1 .. v6}, Lkotlin/reflect/jvm/internal/impl/b/b/ah;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    iput p3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->e:I

    iput-boolean p7, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->f:Z

    iput-boolean p8, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->g:Z

    move/from16 v0, p9

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->h:Z

    move-object/from16 v0, p10

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->i:Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 86
    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/b/at;

    return-void

    :cond_0
    move-object v1, p0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/at;

    move-object p2, v1

    goto :goto_0
.end method


# virtual methods
.method public synthetic E()Lkotlin/reflect/jvm/internal/impl/b/av;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->n()Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/av;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/o",
            "<TR;TD;>;TD;)TR;"
        }
    .end annotation

    .prologue
    const-string v0, "visitor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {p1, p0, p2}, Lkotlin/reflect/jvm/internal/impl/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/at;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/b/a;
    .locals 2

    .prologue
    .line 88
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ah;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.CallableDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/e/f;I)Lkotlin/reflect/jvm/internal/impl/b/at;
    .locals 12

    .prologue
    const-string v0, "newOwner"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newName"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    const-string v1, "annotations"

    invoke-static {v4, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v6

    const-string v1, "type"

    invoke-static {v6, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->l()Z

    move-result v7

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->o()Z

    move-result v8

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->q()Z

    move-result v9

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->m()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v10

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const-string v1, "SourceElement.NO_SOURCE"

    invoke-static {v11, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    move v3, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v11}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/at;
    .locals 1

    .prologue
    const-string v0, "substitutor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/am;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/at;

    return-object p0

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->e:I

    return v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/b/a;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->n()Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    return-object v0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/b/p;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->n()Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/p;

    return-object v0
.end method

.method public k()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->a()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 125
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 126
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 119
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a;->i()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->c()I

    move-result v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    return-object v1
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 91
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->f:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->a()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.CallableMemberDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->i:Lkotlin/reflect/jvm/internal/impl/k/r;

    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/b/at;
    .locals 2

    .prologue
    .line 94
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/b/at;

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    if-ne v1, v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/at;

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->n()Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object p0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->g:Z

    return v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    return-object v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->h:Z

    return v0
.end method

.method public r()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic y_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->a()Lkotlin/reflect/jvm/internal/impl/b/a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public synthetic z()Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->r()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    return-object v0
.end method

.method public synthetic z_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;->n()Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method
