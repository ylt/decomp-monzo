.class public final Lkotlin/reflect/jvm/internal/impl/b/b/u;
.super Lkotlin/reflect/jvm/internal/impl/b/b/j;
.source "ModuleDescriptorImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/b/w;


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/w$a",
            "<+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lkotlin/reflect/jvm/internal/impl/b/b/s;

.field private d:Lkotlin/reflect/jvm/internal/impl/b/z;

.field private final e:Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/b/ac;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lkotlin/c;

.field private final h:Lkotlin/reflect/jvm/internal/impl/j/i;

.field private final i:Lkotlin/reflect/jvm/internal/impl/a/l;

.field private final j:Lkotlin/reflect/jvm/internal/impl/b/an;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "packageFragmentProviderForWholeModuleWithDependencies"

    const-string v5, "getPackageFragmentProviderForWholeModuleWithDependencies()Lorg/jetbrains/kotlin/descriptors/impl/CompositePackageFragmentProvider;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/h/f;)V
    .locals 9

    const/4 v5, 0x0

    const/16 v7, 0x30

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v0 .. v8}, Lkotlin/reflect/jvm/internal/impl/b/b/u;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/h/f;Lkotlin/reflect/jvm/internal/impl/b/an;Ljava/util/Map;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/h/f;Lkotlin/reflect/jvm/internal/impl/b/an;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/j/i;",
            "Lkotlin/reflect/jvm/internal/impl/a/l;",
            "Lkotlin/reflect/jvm/internal/impl/h/f;",
            "Lkotlin/reflect/jvm/internal/impl/b/an;",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/w$a",
            "<*>;+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "moduleName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storageManager"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builtIns"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sourceKind"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capabilities"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    .line 38
    invoke-direct {p0, v0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->h:Lkotlin/reflect/jvm/internal/impl/j/i;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->i:Lkotlin/reflect/jvm/internal/impl/a/l;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->j:Lkotlin/reflect/jvm/internal/impl/b/an;

    .line 40
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Module name must be special: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 45
    :cond_0
    if-eqz p4, :cond_1

    check-cast p4, Lkotlin/reflect/jvm/internal/impl/h/f;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/f;->a:Lkotlin/reflect/jvm/internal/impl/b/w$a;

    invoke-static {v0, p4}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/ab;->a(Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_1

    move-object v1, p0

    :goto_0
    invoke-static {p6, v0}, Lkotlin/a/ab;->a(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, v1, Lkotlin/reflect/jvm/internal/impl/b/b/u;->b:Ljava/util/Map;

    .line 50
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->h:Lkotlin/reflect/jvm/internal/impl/j/i;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/u$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u$b;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/u;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->e:Lkotlin/reflect/jvm/internal/impl/j/c;

    .line 60
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->f:Ljava/util/Set;

    .line 68
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/b/u;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->g:Lkotlin/c;

    return-void

    :cond_1
    move-object v0, p0

    .line 45
    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_0
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/h/f;Lkotlin/reflect/jvm/internal/impl/b/an;Ljava/util/Map;ILkotlin/d/b/i;)V
    .locals 7

    .prologue
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/f;

    move-object v4, v0

    :goto_0
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    .line 36
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/b/an;->a:Lkotlin/reflect/jvm/internal/impl/b/an;

    :goto_1
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_0

    .line 37
    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v6

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/b/b/u;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/h/f;Lkotlin/reflect/jvm/internal/impl/b/an;Ljava/util/Map;)V

    return-void

    :cond_0
    move-object v6, p6

    goto :goto_2

    :cond_1
    move-object v5, p5

    goto :goto_1

    :cond_2
    move-object v4, p4

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Lkotlin/reflect/jvm/internal/impl/j/i;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->h:Lkotlin/reflect/jvm/internal/impl/j/i;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Lkotlin/reflect/jvm/internal/impl/b/b/s;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/b/s;

    return-object v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic d(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Z
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->j()Z

    move-result v0

    return v0
.end method

.method public static final synthetic e(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Lkotlin/reflect/jvm/internal/impl/b/z;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/z;

    return-object v0
.end method

.method private final h()Lkotlin/reflect/jvm/internal/impl/b/b/i;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->g:Lkotlin/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/i;

    return-object v0
.end method

.method private final j()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/z;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "name.toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/o",
            "<TR;TD;>;TD;)TR;"
        }
    .end annotation

    .prologue
    const-string v0, "visitor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/w$b;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/b/o;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/w$a;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/b/w$a",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const-string v0, "capability"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Object;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Ljava/lang/Object;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->g()Lkotlin/reflect/jvm/internal/impl/b/z;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/z;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/d/a/b;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/a/l;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->i:Lkotlin/reflect/jvm/internal/impl/a/l;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;
    .locals 1

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->e:Lkotlin/reflect/jvm/internal/impl/j/c;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ac;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    const-string v0, "substitutor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/w$b;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/b/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "descriptors"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/t;

    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/t;-><init>(Ljava/util/List;Ljava/util/Set;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/s;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a(Lkotlin/reflect/jvm/internal/impl/b/b/s;)V

    .line 103
    return-void
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/b/s;)V
    .locals 4

    .prologue
    const-string v0, "dependencies"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/b/s;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Dependencies of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " were already set"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_1
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/b/s;

    .line 89
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/g;->a(Lkotlin/reflect/jvm/internal/impl/b/w;)Lkotlin/reflect/jvm/internal/impl/h/f;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/h/f$a;->c:Lkotlin/reflect/jvm/internal/impl/h/f$a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    :cond_2
    return-void

    .line 90
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/w;

    .line 91
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/g;->a(Lkotlin/reflect/jvm/internal/impl/b/w;)Lkotlin/reflect/jvm/internal/impl/h/f;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/h/f$a;->c:Lkotlin/reflect/jvm/internal/impl/h/f$a;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_4

    .line 92
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->c()Lkotlin/reflect/jvm/internal/impl/b/an;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c()Lkotlin/reflect/jvm/internal/impl/b/an;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_4

    .line 93
    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    if-nez v2, :cond_5

    const/4 v0, 0x0

    :cond_5
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->f()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/z;)V
    .locals 2

    .prologue
    const-string v0, "providerForModuleContent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->j()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Attempt to initialize module "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " twice"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 117
    :cond_1
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/z;

    .line 118
    return-void
.end method

.method public final varargs a([Lkotlin/reflect/jvm/internal/impl/b/b/u;)V
    .locals 1

    .prologue
    const-string v0, "descriptors"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    check-cast p1, [Ljava/lang/Object;

    invoke-static {p1}, Lkotlin/a/g;->h([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a(Ljava/util/List;)V

    .line 99
    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/b/w;)Z
    .locals 1

    .prologue
    const-string v0, "targetModule"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 106
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-static {v0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/b/s;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/s;->b()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/b/an;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->j:Lkotlin/reflect/jvm/internal/impl/b/an;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/b/s;

    .line 149
    if-eqz v0, :cond_1

    .line 58
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/s;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/s;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 150
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 151
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-object v3, p0

    .line 58
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dependencies of module "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " were not set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 152
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 58
    return-object v1
.end method

.method public f()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u;->f:Ljava/util/Set;

    return-object v0
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/b/z;
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->h()Lkotlin/reflect/jvm/internal/impl/b/b/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/z;

    return-object v0
.end method

.method public y_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 30
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/b/w$b;->a(Lkotlin/reflect/jvm/internal/impl/b/w;)Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    return-object v0
.end method
