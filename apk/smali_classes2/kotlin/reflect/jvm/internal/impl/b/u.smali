.class public final enum Lkotlin/reflect/jvm/internal/impl/b/u;
.super Ljava/lang/Enum;
.source "Modality.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/b/u$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/u;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/b/u;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/b/u;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/b/u;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/b/u;

.field public static final e:Lkotlin/reflect/jvm/internal/impl/b/u$a;

.field private static final synthetic f:[Lkotlin/reflect/jvm/internal/impl/b/u;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/b/u;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/u;

    const-string v2, "FINAL"

    invoke-direct {v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/b/u;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    aput-object v1, v0, v3

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/u;

    const-string v2, "SEALED"

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/b/u;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->b:Lkotlin/reflect/jvm/internal/impl/b/u;

    aput-object v1, v0, v4

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/u;

    const-string v2, "OPEN"

    invoke-direct {v1, v2, v5}, Lkotlin/reflect/jvm/internal/impl/b/u;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    aput-object v1, v0, v5

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/u;

    const-string v2, "ABSTRACT"

    invoke-direct {v1, v2, v6}, Lkotlin/reflect/jvm/internal/impl/b/u;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    aput-object v1, v0, v6

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->f:[Lkotlin/reflect/jvm/internal/impl/b/u;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/u$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/u$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->e:Lkotlin/reflect/jvm/internal/impl/b/u$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 1

    const-class v0, Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/u;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->f:[Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/b/u;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/b/u;

    return-object v0
.end method
