.class public final Lkotlin/reflect/jvm/internal/impl/b/a/j;
.super Ljava/lang/Object;
.source "Annotations.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 3

    .prologue
    const-string v0, "first"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "second"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    :goto_0
    return-object p1

    .line 143
    :cond_0
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p1, p0

    goto :goto_0

    .line 144
    :cond_1
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/k;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/jvm/internal/impl/b/a/h;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/k;-><init>([Lkotlin/reflect/jvm/internal/impl/b/a/h;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-object p1, v0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/e/b;)Z
    .locals 2

    .prologue
    const-string v0, "annotation"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 80
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->b()Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
