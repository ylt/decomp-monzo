.class final Lkotlin/reflect/jvm/internal/impl/b/b/u$a;
.super Lkotlin/d/b/m;
.source "ModuleDescriptorImpl.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/b/b/u;-><init>(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/h/f;Lkotlin/reflect/jvm/internal/impl/b/an;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/b/i;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/b/b/u;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/b/u;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Lkotlin/reflect/jvm/internal/impl/b/b/i;
    .locals 5

    .prologue
    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->b(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Lkotlin/reflect/jvm/internal/impl/b/b/s;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 69
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/s;

    .line 70
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/s;->a()Ljava/util/List;

    move-result-object v1

    .line 71
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Module "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not contained in his own dependencies, this is probably a misconfiguration"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 149
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dependencies of module "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " were not set before querying module content"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    move-object v0, v1

    .line 72
    check-cast v0, Ljava/lang/Iterable;

    .line 150
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    .line 74
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->d(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Z

    move-result v3

    sget-boolean v4, Lkotlin/p;->a:Z

    if-eqz v4, :cond_2

    if-nez v3, :cond_2

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dependency module "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " was not initialized by the time contents of dependent module "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b/u;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->c(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " were queried"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 74
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 78
    :cond_3
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/b/i;

    check-cast v1, Ljava/lang/Iterable;

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 153
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 154
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b/u;

    .line 79
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->e(Lkotlin/reflect/jvm/internal/impl/b/b/u;)Lkotlin/reflect/jvm/internal/impl/b/z;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_4
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 155
    :cond_5
    check-cast v0, Ljava/util/List;

    .line 78
    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/i;-><init>(Ljava/util/List;)V

    .line 80
    return-object v2
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/b/b/u$a;->b()Lkotlin/reflect/jvm/internal/impl/b/b/i;

    move-result-object v0

    return-object v0
.end method
