.class public final Lkotlin/reflect/jvm/internal/impl/b/ar;
.super Ljava/lang/Object;
.source "typeParameterUtils.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/i;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/i;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/i;->y()Ljava/util/List;

    move-result-object v3

    .line 29
    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/i;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/i;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    instance-of v0, v0, Lkotlin/reflect/jvm/internal/impl/b/a;

    if-nez v0, :cond_0

    const-string v0, "declaredParameters"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    :goto_0
    return-object v3

    :cond_0
    move-object v0, p0

    .line 32
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->f(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ar$a;->a:Lkotlin/reflect/jvm/internal/impl/b/ar$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/g/h;->c(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ar$b;->a:Lkotlin/reflect/jvm/internal/impl/b/ar$b;

    check-cast v0, Lkotlin/d/a/b;

    .line 33
    invoke-static {v1, v0}, Lkotlin/g/h;->d(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    invoke-static {v0}, Lkotlin/g/h;->d(Lkotlin/g/g;)Ljava/util/List;

    move-result-object v1

    move-object v0, p0

    .line 35
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->f(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/g/g;

    move-result-object v0

    .line 95
    invoke-interface {v0}, Lkotlin/g/g;->a()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v5, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v5, :cond_1

    .line 96
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v2

    .line 35
    :cond_2
    if-eqz v2, :cond_4

    .line 36
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p0}, Lkotlin/reflect/jvm/internal/impl/b/i;->y()Ljava/util/List;

    move-result-object v3

    const-string v0, "declaredTypeParameters"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 96
    goto :goto_1

    .line 35
    :cond_4
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v2

    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 39
    check-cast v0, Ljava/util/Collection;

    move-object v1, v2

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 40
    nop

    .line 97
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 98
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 99
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-object v2, p0

    .line 40
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v0, v2, v5}, Lkotlin/reflect/jvm/internal/impl/b/ar;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/m;I)Lkotlin/reflect/jvm/internal/impl/b/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 100
    :cond_6
    check-cast v1, Ljava/util/List;

    move-object v0, v3

    .line 42
    check-cast v0, Ljava/util/Collection;

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/b/ae;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/ar;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/i;I)Lkotlin/reflect/jvm/internal/impl/b/ae;

    move-result-object v0

    return-object v0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/i;I)Lkotlin/reflect/jvm/internal/impl/b/ae;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 78
    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-object v1

    .line 80
    :cond_1
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/i;->y()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int v3, v0, p2

    .line 81
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/i;->r()Z

    move-result v0

    if-nez v0, :cond_5

    .line 82
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v3, v0, :cond_2

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " trailing arguments were found in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 82
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 86
    :cond_4
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/ae;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v2, p2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, p1, v2, v1}, Lkotlin/reflect/jvm/internal/impl/b/ae;-><init>(Lkotlin/reflect/jvm/internal/impl/b/i;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/ae;)V

    move-object v1, v0

    goto :goto_0

    .line 89
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    .line 90
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/ae;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/i;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    instance-of v5, v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    if-nez v5, :cond_6

    move-object v0, v1

    :cond_6
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-static {p0, v0, v3}, Lkotlin/reflect/jvm/internal/impl/b/ar;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/i;I)Lkotlin/reflect/jvm/internal/impl/b/ae;

    move-result-object v0

    invoke-direct {v2, p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/b/ae;-><init>(Lkotlin/reflect/jvm/internal/impl/b/i;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/ae;)V

    move-object v1, v2

    goto/16 :goto_0
.end method

.method private static final a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/m;I)Lkotlin/reflect/jvm/internal/impl/b/c;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/c;

    invoke-direct {v0, p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/c;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/b/m;I)V

    return-object v0
.end method
