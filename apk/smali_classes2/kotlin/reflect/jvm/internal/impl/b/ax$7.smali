.class final Lkotlin/reflect/jvm/internal/impl/b/ax$7;
.super Lkotlin/reflect/jvm/internal/impl/b/ay;
.source "Visibilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/b/ax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/b/ay;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/a/e;Lkotlin/reflect/jvm/internal/impl/b/q;Lkotlin/reflect/jvm/internal/impl/b/m;)Z
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "what"

    aput-object v4, v3, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$4"

    aput-object v1, v3, v5

    const-string v1, "isVisible"

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "from"

    aput-object v4, v3, v1

    const-string v1, "kotlin/reflect/jvm/internal/impl/descriptors/Visibilities$4"

    aput-object v1, v3, v5

    const-string v1, "isVisible"

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_1
    instance-of v0, p3, Lkotlin/reflect/jvm/internal/impl/b/ac;

    if-eqz v0, :cond_2

    move-object v0, p3

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ac;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->e()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    .line 220
    :goto_0
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->g(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/h/c;->g(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/b/w;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 222
    :goto_1
    return v0

    :cond_2
    move-object v0, p3

    .line 219
    goto :goto_0

    .line 222
    :cond_3
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/b/ax;->b()Lkotlin/reflect/jvm/internal/impl/l/g;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/l/g;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    goto :goto_1
.end method
