.class final Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;
.super Ljava/lang/Object;
.source "GeneratedMessageLite.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/g$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<*>;"
        }
    .end annotation
.end field

.field final b:I

.field final c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field final d:Z

.field final e:Z


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;ILkotlin/reflect/jvm/internal/impl/protobuf/w$a;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<*>;I",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 680
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    .line 681
    iput p2, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b:I

    .line 682
    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 683
    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->d:Z

    .line 684
    iput-boolean p5, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->e:Z

    .line 685
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 694
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b:I

    return v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;)I
    .locals 2

    .prologue
    .line 725
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b:I

    iget v1, p1, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 720
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/protobuf/h;

    invoke-virtual {p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    move-result-object v0

    return-object v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 671
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;)I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 706
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->d:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 710
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->e:Z

    return v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 714
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    return-object v0
.end method
