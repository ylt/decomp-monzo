.class public final Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;
.super Ljava/io/OutputStream;
.source "ByteString.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/protobuf/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field private static final a:[B


# instance fields
.field private final b:I

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:[B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 765
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a:[B

    return-void
.end method

.method constructor <init>(I)V
    .locals 2

    .prologue
    .line 783
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 784
    if-gez p1, :cond_0

    .line 785
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Buffer size < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 787
    :cond_0
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->b:I

    .line 788
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->c:Ljava/util/ArrayList;

    .line 789
    new-array v0, p1, [B

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    .line 790
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 898
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->c:Ljava/util/ArrayList;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/n;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 899
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->d:I

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->d:I

    .line 903
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->b:I

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->d:I

    ushr-int/lit8 v1, v1, 0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 905
    new-array v0, v0, [B

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    .line 906
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    .line 907
    return-void
.end method

.method private a([BI)[B
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 835
    new-array v0, p2, [B

    .line 836
    array-length v1, p1

    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 837
    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 914
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 915
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    if-lez v0, :cond_0

    .line 916
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a([BI)[B

    move-result-object v0

    .line 917
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->c:Ljava/util/ArrayList;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/n;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 930
    :cond_0
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->d:I

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->d:I

    .line 931
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    .line 932
    return-void

    .line 922
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->c:Ljava/util/ArrayList;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    invoke-direct {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/n;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 928
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a:[B

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 827
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->c()V

    .line 828
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Ljava/lang/Iterable;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 827
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()I
    .locals 2

    .prologue
    .line 872
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->d:I

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 888
    const-string v0, "<ByteString.Output@%s size=%d>"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized write(I)V
    .locals 3

    .prologue
    .line 794
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 795
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a(I)V

    .line 797
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798
    monitor-exit p0

    return-void

    .line 794
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([BII)V
    .locals 4

    .prologue
    .line 802
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    array-length v0, v0

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    sub-int/2addr v0, v1

    if-gt p3, v0, :cond_0

    .line 804
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 805
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    add-int/2addr v0, p3

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 818
    :goto_0
    monitor-exit p0

    return-void

    .line 808
    :cond_0
    :try_start_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    array-length v0, v0

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    sub-int/2addr v0, v1

    .line 809
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 810
    add-int v1, p2, v0

    .line 811
    sub-int v0, p3, v0

    .line 814
    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a(I)V

    .line 815
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->e:[B

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 816
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 802
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
