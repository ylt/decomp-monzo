.class public abstract Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "GeneratedMessageLite.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/h$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<TMessageType;>;>",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$d",
        "<TMessageType;>;"
    }
.end annotation


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 172
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    .line 173
    return-void
.end method

.method protected constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<TMessageType;*>;)V"
        }
    .end annotation

    .prologue
    .line 175
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 176
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    .line 177
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)Lkotlin/reflect/jvm/internal/impl/protobuf/g;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    return-object v0
.end method

.method private c(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<TMessageType;*>;)V"
        }
    .end annotation

    .prologue
    .line 181
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 184
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This extension is for a different message type.  Please make sure that you are not suppressing any generics type warnings."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/g;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    return v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Type:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<TMessageType;TType;>;)Z"
        }
    .end annotation

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)V

    .line 195
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/g$a;)Z

    move-result v0

    return v0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Type:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<TMessageType;TType;>;)TType;"
        }
    .end annotation

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)V

    .line 212
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/g$a;)Ljava/lang/Object;

    move-result-object v0

    .line 213
    if-nez v0, :cond_0

    .line 214
    iget-object v0, p1, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->b:Ljava/lang/Object;

    .line 216
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->c()V

    .line 262
    return-void
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->f()Z

    move-result v0

    return v0
.end method

.method protected g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
            "<TMessageType;>.a;"
        }
    .end annotation

    .prologue
    .line 308
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;ZLkotlin/reflect/jvm/internal/impl/protobuf/h$1;)V

    return-object v0
.end method

.method protected h()I
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->g()I

    move-result v0

    return v0
.end method
