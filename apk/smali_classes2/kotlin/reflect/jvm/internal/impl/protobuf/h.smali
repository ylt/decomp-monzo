.class public abstract Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/a;
.source "GeneratedMessageLite.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$1;,
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$g;,
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;,
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;,
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;,
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;,
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$d;,
        Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/a;-><init>()V

    .line 54
    return-void
.end method

.method protected constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/a;-><init>()V

    .line 57
    return-void
.end method

.method static varargs a(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 746
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 747
    :catch_0
    move-exception v0

    .line 748
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t use Java reflection to implement protocol message reflection."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 751
    :catch_1
    move-exception v0

    .line 752
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 753
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_0

    .line 754
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 755
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 756
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 758
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected exception thrown by generated accessor method."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static varargs a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 7

    .prologue
    .line 735
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 736
    :catch_0
    move-exception v0

    .line 737
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2d

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Generated message class \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\" missing method \""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\"."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;ILkotlin/reflect/jvm/internal/impl/protobuf/w$a;Ljava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ContainingType::",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Type:",
            "Ljava/lang/Object;",
            ">(TContainingType;TType;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<*>;I",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;",
            "Ljava/lang/Class;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<TContainingType;TType;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 639
    new-instance v7, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    move-object v1, p3

    move v2, p4

    move-object v3, p5

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;ILkotlin/reflect/jvm/internal/impl/protobuf/w$a;ZZ)V

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v0

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;Ljava/lang/Class;)V

    return-object v7
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;ILkotlin/reflect/jvm/internal/impl/protobuf/w$a;ZLjava/lang/Class;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ContainingType::",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Type:",
            "Ljava/lang/Object;",
            ">(TContainingType;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<*>;I",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;",
            "Z",
            "Ljava/lang/Class;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<TContainingType;TType;>;"
        }
    .end annotation

    .prologue
    .line 661
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 662
    new-instance v7, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    const/4 v4, 0x1

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;ILkotlin/reflect/jvm/internal/impl/protobuf/w$a;ZZ)V

    move-object v1, v7

    move-object v2, p0

    move-object v3, v6

    move-object v4, p1

    move-object v5, v0

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Ljava/lang/Object;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;Ljava/lang/Class;)V

    return-object v7
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/g;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-static/range {p0 .. p5}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/g;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    return v0
.end method

.method private static b(Lkotlin/reflect/jvm/internal/impl/protobuf/g;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MessageType::",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;",
            ">;TMessageType;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/e;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/f;",
            "I)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 514
    invoke-static {p5}, Lkotlin/reflect/jvm/internal/impl/protobuf/w;->a(I)I

    move-result v0

    .line 515
    invoke-static {p5}, Lkotlin/reflect/jvm/internal/impl/protobuf/w;->b(I)I

    move-result v3

    .line 517
    invoke-virtual {p4, p1, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/f;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;I)Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v4

    .line 523
    if-nez v4, :cond_0

    move v0, v2

    move v3, v1

    .line 539
    :goto_0
    if-eqz v3, :cond_3

    .line 540
    invoke-virtual {p2, p5, p3}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)Z

    move-result v0

    .line 623
    :goto_1
    return v0

    .line 525
    :cond_0
    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;Z)I

    move-result v3

    if-ne v0, v3, :cond_1

    move v0, v2

    move v3, v2

    .line 528
    goto :goto_0

    .line 529
    :cond_1
    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    iget-boolean v3, v3, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->d:Z

    if-eqz v3, :cond_2

    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    iget-object v3, v3, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;Z)I

    move-result v3

    if-ne v0, v3, :cond_2

    move v0, v1

    move v3, v2

    .line 534
    goto :goto_0

    :cond_2
    move v0, v2

    move v3, v1

    .line 536
    goto :goto_0

    .line 543
    :cond_3
    if-eqz v0, :cond_7

    .line 544
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->s()I

    move-result v0

    .line 545
    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->c(I)I

    move-result v0

    .line 546
    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    move-result-object v3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->n:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    if-ne v3, v5, :cond_5

    .line 547
    :goto_2
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v2

    if-lez v2, :cond_6

    .line 548
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->n()I

    move-result v2

    .line 549
    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->f()Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    move-result-object v3

    invoke-interface {v3, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;->a(I)Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;

    move-result-object v2

    .line 551
    if-nez v2, :cond_4

    move v0, v1

    .line 554
    goto :goto_1

    .line 556
    :cond_4
    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v4, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v3, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/g$a;Ljava/lang/Object;)V

    goto :goto_2

    .line 560
    :cond_5
    :goto_3
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v3

    if-lez v3, :cond_6

    .line 561
    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    move-result-object v3

    invoke-static {p2, v3, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;Z)Ljava/lang/Object;

    move-result-object v3

    .line 565
    iget-object v5, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {p0, v5, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/g$a;Ljava/lang/Object;)V

    goto :goto_3

    .line 568
    :cond_6
    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->d(I)V

    :goto_4
    move v0, v1

    .line 623
    goto/16 :goto_1

    .line 571
    :cond_7
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$1;->a:[I

    iget-object v3, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->c()Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 608
    iget-object v0, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    move-result-object v0

    invoke-static {p2, v0, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;Z)Ljava/lang/Object;

    move-result-object v0

    .line 614
    :cond_8
    :goto_5
    iget-object v2, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->d()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 615
    iget-object v2, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/g$a;Ljava/lang/Object;)V

    goto :goto_4

    .line 573
    :pswitch_0
    const/4 v2, 0x0

    .line 574
    iget-object v0, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->d()Z

    move-result v0

    if-nez v0, :cond_c

    .line 575
    iget-object v0, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/g$a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    .line 577
    if-eqz v0, :cond_c

    .line 578
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/o;->k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;

    move-result-object v0

    .line 581
    :goto_6
    if-nez v0, :cond_9

    .line 582
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->c()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/o;->j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;

    move-result-object v0

    .line 585
    :cond_9
    iget-object v2, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->j:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    if-ne v2, v3, :cond_a

    .line 587
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->b()I

    move-result v2

    invoke-virtual {p2, v2, v0, p4}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/o$a;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    .line 592
    :goto_7
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;->h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    goto :goto_5

    .line 590
    :cond_a
    invoke-virtual {p2, v0, p4}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    goto :goto_7

    .line 596
    :pswitch_1
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->n()I

    move-result v2

    .line 597
    iget-object v0, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;->f()Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    move-result-object v0

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;->a(I)Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;

    move-result-object v0

    .line 601
    if-nez v0, :cond_8

    .line 602
    invoke-virtual {p3, p5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 603
    invoke-virtual {p3, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(I)V

    move v0, v1

    .line 604
    goto/16 :goto_1

    .line 618
    :cond_b
    iget-object v2, v4, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/g$a;Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_c
    move-object v0, v2

    goto :goto_6

    .line 571
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p1, p4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)Z

    move-result v0

    return v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 947
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$g;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$g;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    return-object v0
.end method
