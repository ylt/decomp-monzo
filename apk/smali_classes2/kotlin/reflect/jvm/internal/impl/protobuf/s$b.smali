.class Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;
.super Ljava/lang/Object;
.source "RopeByteString.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/protobuf/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/s;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lkotlin/reflect/jvm/internal/impl/protobuf/n;


# direct methods
.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V
    .locals 1

    .prologue
    .line 722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 718
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a:Ljava/util/Stack;

    .line 723
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    .line 724
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/d;Lkotlin/reflect/jvm/internal/impl/protobuf/s$1;)V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    return-void
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/n;
    .locals 2

    .prologue
    .line 727
    move-object v0, p1

    .line 728
    :goto_0
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/protobuf/s;

    if-eqz v1, :cond_0

    .line 729
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/s;

    .line 730
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 731
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/s;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    goto :goto_0

    .line 733
    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    return-object v0
.end method

.method private b()Lkotlin/reflect/jvm/internal/impl/protobuf/n;
    .locals 2

    .prologue
    .line 740
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 741
    const/4 v0, 0x0

    .line 745
    :goto_0
    return-object v0

    .line 743
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/s;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/s;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    move-result-object v0

    .line 744
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/n;->d()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/protobuf/n;
    .locals 2

    .prologue
    .line 761
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    if-nez v0, :cond_0

    .line 762
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 764
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    .line 765
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    .line 766
    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 716
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 770
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
