.class Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;
.super Ljava/lang/Object;
.source "RopeByteString.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/protobuf/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field a:I

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/protobuf/s;

.field private final c:Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;

.field private d:Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;


# direct methods
.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/s;)V
    .locals 2

    .prologue
    .line 788
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 789
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/d;Lkotlin/reflect/jvm/internal/impl/protobuf/s$1;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;

    .line 790
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/n;->c()Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;

    .line 791
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/s;->a()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->a:I

    .line 792
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/s;Lkotlin/reflect/jvm/internal/impl/protobuf/s$1;)V
    .locals 0

    .prologue
    .line 782
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/s;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Byte;
    .locals 1

    .prologue
    .line 799
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->b()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public b()B
    .locals 1

    .prologue
    .line 803
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 804
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/n;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/n;->c()Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;

    .line 806
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->a:I

    .line 807
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$a;->b()B

    move-result v0

    return v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 795
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 782
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/s$c;->a()Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 811
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
