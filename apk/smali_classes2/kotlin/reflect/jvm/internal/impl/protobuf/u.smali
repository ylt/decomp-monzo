.class public Lkotlin/reflect/jvm/internal/impl/protobuf/u;
.super Ljava/util/AbstractList;
.source "UnmodifiableLazyStringList.java"

# interfaces
.implements Ljava/util/RandomAccess;
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/RandomAccess;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/m;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/m;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 53
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/u;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 54
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/u;)Lkotlin/reflect/jvm/internal/impl/protobuf/m;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/u;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/u;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/u;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lkotlin/reflect/jvm/internal/impl/protobuf/m;
    .locals 0

    .prologue
    .line 203
    return-object p0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/u;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->c(I)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/u;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/u$2;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/u$2;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/u;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/u$1;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/u$1;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/u;I)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/u;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->size()I

    move-result v0

    return v0
.end method
