.class public abstract Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "GeneratedMessageLite.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/h$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<TMessageType;>;BuilderType:",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<TMessageType;TBuilderType;>;>",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<TMessageType;TBuilderType;>;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$d",
        "<TMessageType;>;"
    }
.end annotation


# instance fields
.field private a:Lkotlin/reflect/jvm/internal/impl/protobuf/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 332
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 334
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    .line 332
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)Lkotlin/reflect/jvm/internal/impl/protobuf/g;
    .locals 1

    .prologue
    .line 327
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->k()Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    move-result-object v0

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 350
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->b:Z

    if-nez v0, :cond_0

    .line 351
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->b:Z

    .line 354
    :cond_0
    return-void
.end method

.method private k()Lkotlin/reflect/jvm/internal/impl/protobuf/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->c()V

    .line 362
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->b:Z

    .line 363
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    return-object v0
.end method


# virtual methods
.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)V"
        }
    .end annotation

    .prologue
    .line 495
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->j()V

    .line 496
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/g;)V

    .line 497
    return-void
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation

    .prologue
    .line 423
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected g()Z
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/g;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/g;->f()Z

    move-result v0

    return v0
.end method
