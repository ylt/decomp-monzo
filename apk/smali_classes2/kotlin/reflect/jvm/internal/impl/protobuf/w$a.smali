.class public enum Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;
.super Ljava/lang/Enum;
.source "WireFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/protobuf/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4009
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum g:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum h:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum i:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum j:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum k:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum l:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum m:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum n:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum o:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum p:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum q:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field public static final enum r:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

.field private static final synthetic u:[Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;


# instance fields
.field private final s:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

.field private final t:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 108
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "DOUBLE"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v4, v2, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 109
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "FLOAT"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v5, v2, v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 110
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "INT64"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v6, v2, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 111
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "UINT64"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v8, v2, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 112
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "INT32"

    const/4 v2, 0x4

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 113
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "FIXED64"

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v7, v2, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->f:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 114
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "FIXED32"

    const/4 v2, 0x6

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 115
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "BOOL"

    const/4 v2, 0x7

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->h:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 116
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a$1;

    const-string v1, "STRING"

    const/16 v2, 0x8

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->f:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a$1;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->i:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 119
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a$2;

    const-string v1, "GROUP"

    const/16 v2, 0x9

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->i:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v8}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a$2;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->j:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 122
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a$3;

    const-string v1, "MESSAGE"

    const/16 v2, 0xa

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->i:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a$3;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->k:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 125
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a$4;

    const-string v1, "BYTES"

    const/16 v2, 0xb

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a$4;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->l:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 128
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "UINT32"

    const/16 v2, 0xc

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->m:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 129
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "ENUM"

    const/16 v2, 0xd

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->h:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->n:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 130
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "SFIXED32"

    const/16 v2, 0xe

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->o:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 131
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "SFIXED64"

    const/16 v2, 0xf

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->p:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 132
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "SINT32"

    const/16 v2, 0x10

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->q:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 133
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    const-string v1, "SINT64"

    const/16 v2, 0x11

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->r:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    .line 107
    const/16 v0, 0x12

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->f:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v1, v0, v7

    const/4 v1, 0x6

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->h:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->i:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->j:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->k:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->l:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->m:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->n:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->o:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->p:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->q:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->r:Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    aput-object v2, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->u:[Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 136
    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->s:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    .line 137
    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->t:I

    .line 138
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;ILkotlin/reflect/jvm/internal/impl/protobuf/w$1;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;-><init>(Ljava/lang/String;ILkotlin/reflect/jvm/internal/impl/protobuf/w$b;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;
    .locals 1

    .prologue
    .line 107
    const-class v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->u:[Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;

    return-object v0
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->s:Lkotlin/reflect/jvm/internal/impl/protobuf/w$b;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/protobuf/w$a;->t:I

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    return v0
.end method
