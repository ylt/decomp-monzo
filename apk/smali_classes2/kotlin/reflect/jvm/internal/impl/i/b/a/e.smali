.class public final Lkotlin/reflect/jvm/internal/impl/i/b/a/e;
.super Lkotlin/reflect/jvm/internal/impl/b/b/a;
.source "DeserializedClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;,
        Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;,
        Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;
    }
.end annotation


# instance fields
.field private final c:Lkotlin/reflect/jvm/internal/impl/e/a;

.field private final d:Lkotlin/reflect/jvm/internal/impl/b/u;

.field private final e:Lkotlin/reflect/jvm/internal/impl/b/ay;

.field private final f:Lkotlin/reflect/jvm/internal/impl/b/f;

.field private final g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

.field private final h:Lkotlin/reflect/jvm/internal/impl/h/e/i;

.field private final i:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;

.field private final j:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;

.field private final k:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

.field private final l:Lkotlin/reflect/jvm/internal/impl/b/m;

.field private final m:Lkotlin/reflect/jvm/internal/impl/j/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private final o:Lkotlin/reflect/jvm/internal/impl/j/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/g",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private final q:Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

.field private final r:Lkotlin/reflect/jvm/internal/impl/b/a/h;

.field private final s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

.field private final t:Lkotlin/reflect/jvm/internal/impl/b/al;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/i/e$c;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/b/al;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const-string v0, "outerContext"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classProto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sourceElement"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->r()I

    move-result v1

    invoke-interface {p3, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->c()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    .line 50
    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/a;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->t:Lkotlin/reflect/jvm/internal/impl/b/al;

    .line 54
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->r()I

    move-result v0

    invoke-interface {p3, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->c:Lkotlin/reflect/jvm/internal/impl/e/a;

    .line 56
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->d:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$l;)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 57
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->c:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ai;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 58
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->e:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c$b;)Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    move-object v0, p0

    .line 60
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->u()Ljava/util/List;

    move-result-object v1

    const-string v2, "classProto.typeParameterList"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->M()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v3

    const-string v4, "classProto.typeTable"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;-><init>(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)V

    invoke-virtual {p1, v0, v1, p3, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    .line 62
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/h/e/k;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v2

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/k;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/i;

    :goto_0
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->h:Lkotlin/reflect/jvm/internal/impl/h/e/i;

    .line 63
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->i:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;

    .line 64
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->j:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;

    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V

    :goto_1
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->k:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    .line 67
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->l:Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 68
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$g;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$g;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/g;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->m:Lkotlin/reflect/jvm/internal/impl/j/g;

    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$f;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$f;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->n:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 70
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$e;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/g;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->o:Lkotlin/reflect/jvm/internal/impl/j/g;

    .line 71
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$h;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$h;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->p:Lkotlin/reflect/jvm/internal/impl/j/f;

    .line 73
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v3

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->t:Lkotlin/reflect/jvm/internal/impl/b/al;

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->l:Lkotlin/reflect/jvm/internal/impl/b/m;

    instance-of v7, v5, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    if-nez v7, :cond_0

    move-object v5, v6

    :cond_0
    check-cast v5, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    if-eqz v5, :cond_3

    iget-object v5, v5, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->q:Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    :goto_2
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/e$c;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->q:Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    .line 82
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->b:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 83
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    .line 82
    :goto_3
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->r:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-void

    .line 62
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/h$b;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/i;

    goto/16 :goto_0

    :cond_2
    move-object v0, v6

    .line 65
    goto/16 :goto_1

    :cond_3
    move-object v5, v6

    .line 73
    goto :goto_2

    .line 85
    :cond_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$d;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$d;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    goto :goto_3
.end method

.method private final F()Lkotlin/reflect/jvm/internal/impl/b/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 116
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 117
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/f;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/f;

    .line 118
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/b/f;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 119
    nop

    .line 117
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/d;

    .line 122
    :goto_0
    return-object v1

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->A()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 347
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    .line 122
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/i/c;->i:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->p()I

    move-result v0

    invoke-virtual {v5, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_1

    move-object v0, v1

    .line 348
    :goto_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    if-eqz v0, :cond_4

    .line 122
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    .line 123
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->b()Lkotlin/reflect/jvm/internal/impl/i/b/w;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;Z)Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v0

    .line 122
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    :goto_3
    move-object v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 348
    goto :goto_2

    :cond_4
    move-object v0, v2

    .line 122
    goto :goto_3
.end method

.method private final G()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->H()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->o()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/utils/a;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->n()Lkotlin/reflect/jvm/internal/impl/i/b/a;

    move-result-object v1

    invoke-interface {v1, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a;->d(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 131
    return-object v0
.end method

.method private final H()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->A()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 349
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 350
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    .line 134
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/c;->i:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->p()I

    move-result v0

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    const-string v4, "Flags.IS_SECONDARY.get(it.flags)"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 351
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 134
    nop

    .line 352
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 353
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 354
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    .line 135
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->b()Lkotlin/reflect/jvm/internal/impl/i/b/w;

    move-result-object v3

    const-string v4, "it"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;Z)Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 355
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 136
    return-object v0
.end method

.method private final I()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-object v1

    .line 143
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->t()I

    move-result v2

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    .line 144
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->j:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;

    const-string v0, "companionObjectName"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->r:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-virtual {v3, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v1, v0

    goto :goto_0
.end method

.method private final J()Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->b:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 367
    :goto_0
    return-object v0

    .line 155
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->K()Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 156
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 157
    check-cast v1, Ljava/lang/Iterable;

    .line 356
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 365
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 364
    check-cast v1, Ljava/lang/Integer;

    .line 158
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v3

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    const-string v5, "index"

    invoke-static {v1, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-interface {v4, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    const-string v4, "c.nameResolver.getClassId(index)"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 364
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 156
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 367
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    goto :goto_0

    .line 163
    :cond_4
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->c(Lkotlin/reflect/jvm/internal/impl/b/e;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->i:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->k:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    return-object v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->c:Lkotlin/reflect/jvm/internal/impl/e/a;

    return-object v0
.end method

.method public static final synthetic d(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/b/d;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->F()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic e(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->G()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic f(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->I()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic g(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->J()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()Lkotlin/reflect/jvm/internal/impl/h/e/i;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->h:Lkotlin/reflect/jvm/internal/impl/h/e/i;

    return-object v0
.end method

.method public final E()Lkotlin/reflect/jvm/internal/impl/i/e$c;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    return-object v0
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/i/b/o;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->j:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->q:Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->A()Lkotlin/reflect/jvm/internal/impl/h/e/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->i:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->j:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public i()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->o:Lkotlin/reflect/jvm/internal/impl/j/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/g;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method public j()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 99
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->f:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->n:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public l()Lkotlin/reflect/jvm/internal/impl/b/f;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    return-object v0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    return-object v0
.end method

.method public n()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 101
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->g:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/b/d;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->m:Lkotlin/reflect/jvm/internal/impl/j/g;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/g;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    return-object v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 113
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->e:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->g:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic r()Z
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->j()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public synthetic s()Z
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->n()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deserialized class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic v()Z
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->z()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->r:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->t:Lkotlin/reflect/jvm/internal/impl/b/al;

    return-object v0
.end method

.method public y()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->g:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public y_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->l:Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public z()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->h:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->s:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
