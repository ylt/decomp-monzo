.class public abstract Lkotlin/reflect/jvm/internal/impl/i/b/q;
.super Lkotlin/reflect/jvm/internal/impl/b/b/w;
.source "DeserializedPackageFragment.kt"


# instance fields
.field public a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/h/e/h;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/j/i;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;)V
    .locals 2

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storageManager"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "module"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p3, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/w;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/q;->c:Lkotlin/reflect/jvm/internal/impl/j/i;

    .line 37
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/q;->c:Lkotlin/reflect/jvm/internal/impl/j/i;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/q$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/q$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/q;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/q;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/i/b/m;)V
    .locals 1

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/q;->a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    return-void
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z
    .locals 2

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/q;->x_()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    .line 47
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;

    if-eqz v1, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()Lkotlin/reflect/jvm/internal/impl/i/b/h;
.end method

.method protected abstract d()Lkotlin/reflect/jvm/internal/impl/h/e/h;
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/i/b/m;
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/q;->a:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    if-nez v0, :cond_0

    const-string v1, "components"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public x_()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/q;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method
