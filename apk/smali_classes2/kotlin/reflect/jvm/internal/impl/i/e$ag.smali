.class public final Lkotlin/reflect/jvm/internal/impl/i/e$ag;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$ah;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ag"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ah;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$ag;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private i:I

.field private j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private k:I

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17239
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 17961
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 17962
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->D()V

    .line 17963
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 17152
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 17367
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    .line 17425
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m:I

    .line 17153
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->D()V

    .line 17155
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v5

    .line 17157
    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v6

    .line 17161
    const/4 v2, 0x0

    .line 17162
    :goto_0
    if-nez v2, :cond_3

    .line 17163
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 17164
    sparse-switch v0, :sswitch_data_0

    .line 17169
    invoke-virtual {p0, p1, v6, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    move v2, v0

    .line 17222
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 17167
    goto :goto_1

    .line 17176
    :sswitch_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    .line 17177
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->f:I

    move v0, v2

    .line 17178
    goto :goto_1

    .line 17181
    :sswitch_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    .line 17182
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->g:I

    move v0, v2

    .line 17183
    goto :goto_1

    .line 17187
    :sswitch_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_5

    .line 17188
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v3, v0

    .line 17190
    :goto_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17191
    if-eqz v3, :cond_0

    .line 17192
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 17193
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17195
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    move v0, v2

    .line 17196
    goto :goto_1

    .line 17200
    :sswitch_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 17201
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v3, v0

    .line 17203
    :goto_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17204
    if-eqz v3, :cond_1

    .line 17205
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 17206
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17208
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    move v0, v2

    .line 17209
    goto :goto_1

    .line 17212
    :sswitch_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    .line 17213
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->i:I

    move v0, v2

    .line 17214
    goto/16 :goto_1

    .line 17217
    :sswitch_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    .line 17218
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->k:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_2
    move v0, v2

    goto/16 :goto_1

    .line 17230
    :cond_3
    :try_start_1
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17234
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 17236
    :goto_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d()V

    .line 17238
    return-void

    .line 17231
    :catch_0
    move-exception v0

    .line 17234
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_4

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 17223
    :catch_1
    move-exception v0

    .line 17224
    :try_start_2
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 17229
    :catchall_1
    move-exception v0

    .line 17230
    :try_start_3
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 17234
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 17236
    :goto_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d()V

    throw v0

    .line 17225
    :catch_2
    move-exception v0

    .line 17226
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 17231
    :catch_3
    move-exception v1

    .line 17234
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_5

    :catchall_2
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    :cond_4
    move-object v3, v4

    goto/16 :goto_3

    :cond_5
    move-object v3, v4

    goto/16 :goto_2

    .line 17164
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17127
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 17134
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 17367
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    .line 17425
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m:I

    .line 17135
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 17136
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 17127
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17137
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 17367
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    .line 17425
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m:I

    .line 17137
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method public static A()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17521
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->t()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method private D()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17360
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->f:I

    .line 17361
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->g:I

    .line 17362
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17363
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->i:I

    .line 17364
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17365
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->k:I

    .line 17366
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I
    .locals 0

    .prologue
    .line 17127
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->f:I

    return p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17524
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->A()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 17127
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I
    .locals 0

    .prologue
    .line 17127
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 17127
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 17127
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I
    .locals 0

    .prologue
    .line 17127
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->i:I

    return p1
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I
    .locals 0

    .prologue
    .line 17127
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->k:I

    return p1
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I
    .locals 0

    .prologue
    .line 17127
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    return p1
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 17141
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object v0
.end method


# virtual methods
.method public B()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17522
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->A()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public C()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17526
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 17399
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->i()I

    .line 17401
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v0

    .line 17403
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 17404
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->f:I

    invoke-virtual {p1, v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 17406
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 17407
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->g:I

    invoke-virtual {p1, v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 17409
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_2

    .line 17410
    const/4 v1, 0x3

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 17412
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_3

    .line 17413
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v4, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 17415
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 17416
    const/4 v1, 0x5

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->i:I

    invoke-virtual {p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 17418
    :cond_4
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 17419
    const/4 v1, 0x6

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->k:I

    invoke-virtual {p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 17421
    :cond_5
    const/16 v1, 0xc8

    invoke-virtual {v0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 17422
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 17423
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17251
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 17127
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 17427
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m:I

    .line 17428
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 17458
    :goto_0
    return v0

    .line 17430
    :cond_0
    const/4 v0, 0x0

    .line 17431
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 17432
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17435
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 17436
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->g:I

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17439
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 17440
    const/4 v1, 0x3

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17443
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 17444
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v4, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17447
    :cond_4
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 17448
    const/4 v1, 0x5

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->i:I

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17451
    :cond_5
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 17452
    const/4 v1, 0x6

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->k:I

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17455
    :cond_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h()I

    move-result v1

    add-int/2addr v0, v1

    .line 17456
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 17457
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 17127
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->B()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 17127
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->C()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 17369
    iget-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    .line 17370
    if-ne v2, v0, :cond_0

    .line 17394
    :goto_0
    return v0

    .line 17371
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 17373
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->q()Z

    move-result v2

    if-nez v2, :cond_2

    .line 17374
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    move v0, v1

    .line 17375
    goto :goto_0

    .line 17377
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->s()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 17378
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->t()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v2

    if-nez v2, :cond_3

    .line 17379
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    move v0, v1

    .line 17380
    goto :goto_0

    .line 17383
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->w()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 17384
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->x()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v2

    if-nez v2, :cond_4

    .line 17385
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    move v0, v1

    .line 17386
    goto :goto_0

    .line 17389
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->f()Z

    move-result v2

    if-nez v2, :cond_5

    .line 17390
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    move v0, v1

    .line 17391
    goto :goto_0

    .line 17393
    :cond_5
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l:B

    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 17145
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 17268
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 17281
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->f:I

    return v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 17290
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 17296
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->g:I

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 17305
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 17311
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 17320
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()I
    .locals 1

    .prologue
    .line 17326
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->i:I

    return v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 17335
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 17465
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 17341
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->j:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public y()Z
    .locals 2

    .prologue
    .line 17350
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()I
    .locals 1

    .prologue
    .line 17356
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->k:I

    return v0
.end method
