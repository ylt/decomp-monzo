.class public final Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$n;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$m;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$m$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$n;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

.field private f:Lkotlin/reflect/jvm/internal/impl/i/e$u;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 11557
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 11724
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    .line 11849
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    .line 11974
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    .line 12099
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 12159
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 11558
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->u()V

    .line 11559
    return-void
.end method

.method static synthetic t()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 1

    .prologue
    .line 11551
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->v()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method private u()V
    .locals 0

    .prologue
    .line 11562
    return-void
.end method

.method private static v()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 1

    .prologue
    .line 11564
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;-><init>()V

    return-object v0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 11727
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 11728
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    .line 11729
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11731
    :cond_0
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 11852
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 11853
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    .line 11854
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11856
    :cond_0
    return-void
.end method

.method private y()V
    .locals 2

    .prologue
    .line 11977
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 11978
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    .line 11979
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11981
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 1

    .prologue
    .line 11749
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 2

    .prologue
    .line 12138
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 12140
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 12146
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 12147
    return-object p0

    .line 12143
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 2

    .prologue
    .line 11630
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->m()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 11670
    :goto_0
    return-object p0

    .line 11631
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 11632
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 11633
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    .line 11634
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11641
    :cond_1
    :goto_1
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 11642
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 11643
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    .line 11644
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11651
    :cond_2
    :goto_2
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 11652
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 11653
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    .line 11654
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11661
    :cond_3
    :goto_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 11662
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->v()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    .line 11664
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->w()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 11665
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->x()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    .line 11667
    :cond_5
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 11668
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 11636
    :cond_6
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->w()V

    .line 11637
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 11646
    :cond_7
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->x()V

    .line 11647
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 11656
    :cond_8
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->y()V

    .line 11657
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 2

    .prologue
    .line 12198
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 12200
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 12206
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 12207
    return-object p0

    .line 12203
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 11551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11551
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 11551
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 1

    .prologue
    .line 11874
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 11551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11551
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11709
    const/4 v2, 0x0

    .line 11711
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 11716
    if-eqz v0, :cond_0

    .line 11717
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    .line 11720
    :cond_0
    return-object p0

    .line 11712
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 11713
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 11714
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 11716
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 11717
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    :cond_1
    throw v0

    .line 11716
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 1

    .prologue
    .line 11999
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;

    return-object v0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 11551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 11551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 11551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 11551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 11551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 2

    .prologue
    .line 11583
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->v()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 1

    .prologue
    .line 11587
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->m()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 11674
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->o()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 11675
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l()Z

    move-result v2

    if-nez v2, :cond_1

    .line 11702
    :cond_0
    :goto_1
    return v1

    .line 11674
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 11680
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->p()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 11681
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 11680
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    .line 11686
    :goto_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->q()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 11687
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 11686
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 11692
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->r()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 11693
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->s()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11698
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11702
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 2

    .prologue
    .line 11591
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    .line 11592
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 11593
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 11595
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 11599
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$m;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 11600
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11601
    const/4 v1, 0x0

    .line 11602
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 11603
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    .line 11604
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11606
    :cond_0
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    invoke-static {v2, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;Ljava/util/List;)Ljava/util/List;

    .line 11607
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 11608
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    .line 11609
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11611
    :cond_1
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    invoke-static {v2, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b(Lkotlin/reflect/jvm/internal/impl/i/e$m;Ljava/util/List;)Ljava/util/List;

    .line 11612
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    .line 11613
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    .line 11614
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    .line 11616
    :cond_2
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    invoke-static {v2, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c(Lkotlin/reflect/jvm/internal/impl/i/e$m;Ljava/util/List;)Ljava/util/List;

    .line 11617
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_4

    .line 11620
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 11621
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 11622
    or-int/lit8 v0, v0, 0x2

    .line 11624
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 11625
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;I)I

    .line 11626
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 11743
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 11868
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 11993
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 12104
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 1

    .prologue
    .line 12110
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object v0
.end method
