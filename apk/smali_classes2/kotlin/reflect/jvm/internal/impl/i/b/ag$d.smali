.class final Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;
.super Lkotlin/d/b/m;
.source "TypeDeserializer.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field final synthetic c:Lkotlin/reflect/jvm/internal/impl/b/a/h;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;->b:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;->c:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->f()Lkotlin/reflect/jvm/internal/impl/i/b/c;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;->b:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    .line 92
    invoke-interface {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 93
    nop

    .line 211
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 212
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 213
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 93
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lkotlin/reflect/jvm/internal/impl/b/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;->c:Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 93
    invoke-static {v1, v0}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
