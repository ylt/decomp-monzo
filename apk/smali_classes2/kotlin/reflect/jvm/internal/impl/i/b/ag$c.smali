.class final Lkotlin/reflect/jvm/internal/impl/i/b/ag$c;
.super Lkotlin/d/b/m;
.source "TypeDeserializer.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/ag;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y$a;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-static {p1, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Ljava/util/List;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v1

    goto :goto_1
.end method
