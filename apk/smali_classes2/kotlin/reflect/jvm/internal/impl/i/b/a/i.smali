.class public Lkotlin/reflect/jvm/internal/impl/i/b/a/i;
.super Lkotlin/reflect/jvm/internal/impl/i/b/a/h;
.source "DeserializedPackageMemberScope.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/e/b;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/i/e$m;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/i/b/m;Lkotlin/d/a/a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$m;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a/f;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/m;",
            "Lkotlin/d/a/a",
            "<+",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "packageDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "components"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classNames"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->v()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    const-string v1, "proto.typeTable"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;-><init>(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/n;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/n$a;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->x()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v1

    const-string v2, "proto.sinceKotlinInfoTable"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/n$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v4

    move-object v0, p5

    move-object v1, p1

    move-object v2, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->a(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v1

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->o()Ljava/util/List;

    move-result-object v2

    const-string v0, "proto.functionList"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->q()Ljava/util/List;

    move-result-object v3

    const-string v0, "proto.propertyList"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/util/Collection;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->s()Ljava/util/List;

    move-result-object v4

    const-string v0, "proto.typeAliasList"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/util/Collection;

    move-object v0, p0

    move-object v5, p6

    .line 37
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/d/a/a;)V

    .line 42
    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-void
.end method


# virtual methods
.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;->b(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 2

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/e/a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-direct {v0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    return-object v0
.end method

.method protected a(Ljava/util/Collection;Lkotlin/d/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->m:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-virtual {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;->a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;->f()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->l()Ljava/lang/Iterable;

    move-result-object v1

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 68
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 69
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/b/i;

    .line 46
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-interface {v1, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/i;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 70
    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 72
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 45
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v2, v0}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method protected b(Lkotlin/reflect/jvm/internal/impl/e/f;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-super {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;->f()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->l()Ljava/lang/Iterable;

    move-result-object v0

    .line 73
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/i;

    .line 49
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/i;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-interface {v0, v4, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/i;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 74
    :goto_0
    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    .line 49
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 74
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method protected c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
