.class public final Lkotlin/reflect/jvm/internal/impl/i/e$e;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$f;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$e;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12991
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$e$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 13584
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->c:Lkotlin/reflect/jvm/internal/impl/i/e$e;

    .line 13585
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->c:Lkotlin/reflect/jvm/internal/impl/i/e$e;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->x()V

    .line 13586
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v7, 0x2

    .line 12929
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 13097
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->i:B

    .line 13136
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->j:I

    .line 12930
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->x()V

    .line 12932
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v3

    .line 12934
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v4

    move v2, v0

    .line 12939
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 12940
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v5

    .line 12941
    sparse-switch v5, :sswitch_data_0

    .line 12946
    invoke-virtual {p0, p1, v4, p2, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 12948
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 12944
    goto :goto_0

    .line 12953
    :sswitch_1
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    .line 12954
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v5

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->f:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 12972
    :catch_0
    move-exception v0

    .line 12973
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12978
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v2, 0x2

    if-ne v1, v7, :cond_1

    .line 12979
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    .line 12982
    :cond_1
    :try_start_2
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 12986
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 12988
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d()V

    throw v0

    .line 12958
    :sswitch_2
    and-int/lit8 v5, v2, 0x2

    if-eq v5, v7, :cond_2

    .line 12959
    :try_start_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    .line 12960
    or-int/lit8 v2, v2, 0x2

    .line 12962
    :cond_2
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v6, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 12974
    :catch_1
    move-exception v0

    .line 12975
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 12966
    :sswitch_3
    :try_start_5
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    .line 12967
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v5

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->h:I
    :try_end_5
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 12978
    :cond_3
    and-int/lit8 v0, v2, 0x2

    if-ne v0, v7, :cond_4

    .line 12979
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    .line 12982
    :cond_4
    :try_start_6
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 12986
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 12988
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d()V

    .line 12990
    return-void

    .line 12983
    :catch_2
    move-exception v0

    .line 12986
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 12983
    :catch_3
    move-exception v1

    .line 12986
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 12941
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0xf8 -> :sswitch_3
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12904
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$e;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 12911
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 13097
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->i:B

    .line 13136
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->j:I

    .line 12912
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 12913
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 12904
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12914
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 13097
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->i:B

    .line 13136
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->j:I

    .line 12914
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$e;I)I
    .locals 0

    .prologue
    .line 12904
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->f:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$e;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 12904
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 1

    .prologue
    .line 13223
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->u()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$e;I)I
    .locals 0

    .prologue
    .line 12904
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->h:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Ljava/util/List;
    .locals 1

    .prologue
    .line 12904
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$e;I)I
    .locals 0

    .prologue
    .line 12904
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    return p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 12904
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$e;
    .locals 1

    .prologue
    .line 12918
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->c:Lkotlin/reflect/jvm/internal/impl/i/e$e;

    return-object v0
.end method

.method public static u()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 1

    .prologue
    .line 13220
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method private x()V
    .locals 1

    .prologue
    .line 13093
    const/4 v0, 0x6

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->f:I

    .line 13094
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    .line 13095
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->h:I

    .line 13096
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 13059
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 13119
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->i()I

    .line 13121
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v2

    .line 13123
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 13124
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 13126
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 13127
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 13126
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 13129
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 13130
    const/16 v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->h:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 13132
    :cond_2
    const/16 v0, 0xc8

    invoke-virtual {v2, v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 13133
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 13134
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13003
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 12904
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->n()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 13138
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->j:I

    .line 13139
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 13157
    :goto_0
    return v0

    .line 13142
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 13143
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->f:I

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v0

    .line 13146
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 13147
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v0, v2

    .line 13146
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 13150
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 13151
    const/16 v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->h:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 13154
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->h()I

    move-result v0

    add-int/2addr v0, v2

    .line 13155
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 13156
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->j:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 12904
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->v()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 12904
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->w()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 13099
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->i:B

    .line 13100
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 13114
    :cond_0
    :goto_0
    return v1

    .line 13101
    :cond_1
    if-eqz v0, :cond_0

    move v0, v1

    .line 13103
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->r()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 13104
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l()Z

    move-result v3

    if-nez v3, :cond_2

    .line 13105
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->i:B

    goto :goto_0

    .line 13103
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 13109
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->f()Z

    move-result v0

    if-nez v0, :cond_4

    .line 13110
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->i:B

    goto :goto_0

    .line 13113
    :cond_4
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->i:B

    move v1, v2

    .line 13114
    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$e;
    .locals 1

    .prologue
    .line 12922
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->c:Lkotlin/reflect/jvm/internal/impl/i/e$e;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 13019
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 13031
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->f:I

    return v0
.end method

.method public q()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13040
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    return-object v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 13053
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 13079
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 13089
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->h:I

    return v0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 1

    .prologue
    .line 13221
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->u()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 1

    .prologue
    .line 13225
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 13164
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
