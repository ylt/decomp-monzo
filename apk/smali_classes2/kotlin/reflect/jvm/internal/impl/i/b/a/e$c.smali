.class final Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;
.super Ljava/lang/Object;
.source "DeserializedClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/j/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/d",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 304
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->I()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 365
    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/a/ab;->a(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/f/d;->c(II)I

    move-result v2

    .line 366
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 367
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    .line 368
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;

    .line 305
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->p()I

    move-result v0

    invoke-interface {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 370
    :cond_0
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->b:Ljava/util/Map;

    .line 307
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->c:Lkotlin/reflect/jvm/internal/impl/j/d;

    .line 321
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$b;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->b:Ljava/util/Map;

    return-object v0
.end method

.method private final b()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 328
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 330
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 331
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    const/4 v3, 0x3

    invoke-static {v0, v5, v5, v3, v5}, Lkotlin/reflect/jvm/internal/impl/h/e/j$a;->a(Lkotlin/reflect/jvm/internal/impl/h/e/j;Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    .line 332
    instance-of v4, v0, Lkotlin/reflect/jvm/internal/impl/b/ak;

    if-nez v4, :cond_2

    instance-of v4, v0, Lkotlin/reflect/jvm/internal/impl/b/ag;

    if-eqz v4, :cond_1

    .line 333
    :cond_2
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 338
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->C()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 347
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v1

    .line 348
    check-cast v0, Ljava/util/Collection;

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    .line 338
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->t()I

    move-result v2

    invoke-interface {v4, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    const-string v4, "c.nameResolver.getName(it.name)"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 349
    check-cast v0, Ljava/util/Collection;

    check-cast v0, Ljava/util/Set;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->E()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 350
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v2, v1

    .line 351
    check-cast v2, Ljava/util/Collection;

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    .line 339
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v5

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->t()I

    move-result v3

    invoke-interface {v5, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    const-string v5, "c.nameResolver.getName(it.name)"

    invoke-static {v3, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 352
    :cond_5
    check-cast v1, Ljava/util/Collection;

    check-cast v1, Ljava/lang/Iterable;

    .line 338
    invoke-static {v0, v1}, Lkotlin/a/ah;->a(Ljava/util/Set;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;)Lkotlin/reflect/jvm/internal/impl/j/f;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-object v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 304
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 353
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 362
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 361
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 343
    const-string v3, "name"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 361
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 364
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 343
    return-object v1
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->c:Lkotlin/reflect/jvm/internal/impl/j/d;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/j/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method
