.class public final Lkotlin/reflect/jvm/internal/impl/i/b/aa;
.super Ljava/lang/Object;
.source "NotFoundClasses.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;,
        Lkotlin/reflect/jvm/internal/impl/i/b/aa$b;,
        Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/j/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;",
            "Lkotlin/reflect/jvm/internal/impl/b/ap;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/j/i;

.field private final e:Lkotlin/reflect/jvm/internal/impl/b/w;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;)V
    .locals 2

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "module"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->d:Lkotlin/reflect/jvm/internal/impl/j/i;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->e:Lkotlin/reflect/jvm/internal/impl/b/w;

    .line 43
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->d:Lkotlin/reflect/jvm/internal/impl/j/i;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$e;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/aa;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->a:Lkotlin/reflect/jvm/internal/impl/j/c;

    .line 47
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->d:Lkotlin/reflect/jvm/internal/impl/j/i;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$d;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$d;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/aa;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->b:Lkotlin/reflect/jvm/internal/impl/j/c;

    .line 54
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->d:Lkotlin/reflect/jvm/internal/impl/j/i;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$f;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$f;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/aa;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->c:Lkotlin/reflect/jvm/internal/impl/j/c;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;Lkotlin/d/a/r;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;",
            "Lkotlin/d/a/r",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "-",
            "Ljava/lang/Boolean;",
            "-",
            "Ljava/lang/Integer;",
            "+TD;>;)TD;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v3

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;->b()Ljava/util/List;

    move-result-object v1

    .line 69
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unresolved local class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 74
    :cond_0
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->e()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v2

    const-string v0, "classId.outerClassId"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    const/4 v4, 0x1

    invoke-static {v0, v4}, Lkotlin/a/m;->b(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->b(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/g;

    move-object v2, v0

    .line 78
    :goto_0
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->f()Z

    move-result v0

    .line 80
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->c()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    const-string v4, "classId.shortClassName"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v1}, Lkotlin/a/m;->f(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v2, v3, v4, v0}, Lkotlin/d/a/r;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 75
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->a:Lkotlin/reflect/jvm/internal/impl/j/c;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v4, "classId.packageFqName"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/g;

    move-object v2, v0

    goto :goto_0

    .line 80
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;Lkotlin/d/a/r;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;Lkotlin/d/a/r;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/aa;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->e:Lkotlin/reflect/jvm/internal/impl/b/w;

    return-object v0
.end method

.method private final b(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/e;"
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->b:Lkotlin/reflect/jvm/internal/impl/j/c;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;

    invoke-direct {v1, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)V

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/i/b/aa;)Lkotlin/reflect/jvm/internal/impl/j/i;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->d:Lkotlin/reflect/jvm/internal/impl/j/i;

    return-object v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;"
        }
    .end annotation

    .prologue
    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParametersCount"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->b(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    const-string v1, "getOrCreateClass(classId\u2026ersCount).typeConstructor"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 2

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->z()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 161
    const-string v1, "classId"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "classId"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->b(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    const-string v1, "getOrCreateClass(classId\u2026peTable)).typeConstructor"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 4

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->F()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->c:Lkotlin/reflect/jvm/internal/impl/j/c;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;

    const-string v3, "classId"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "classId"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)V

    invoke-interface {v1, v2}, Lkotlin/reflect/jvm/internal/impl/j/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ap;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    const-string v1, "typeAliases(ClassRequest\u2026eTable))).typeConstructor"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
