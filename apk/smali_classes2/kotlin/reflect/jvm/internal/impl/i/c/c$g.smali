.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;,
        Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;,
        Lkotlin/reflect/jvm/internal/impl/i/c/c$g$c;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$g;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 165
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 2087
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    .line 2088
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->q()V

    .line 2089
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v8, 0x2

    const/4 v1, 0x1

    .line 89
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 1625
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->g:I

    .line 1631
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->h:B

    .line 1657
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->i:I

    .line 90
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->q()V

    .line 92
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v3

    .line 94
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v4

    move v2, v0

    .line 99
    :cond_0
    :goto_0
    if-nez v0, :cond_7

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v5

    .line 101
    sparse-switch v5, :sswitch_data_0

    .line 106
    invoke-virtual {p0, p1, v4, p2, v5}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 108
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 104
    goto :goto_0

    .line 113
    :sswitch_1
    and-int/lit8 v5, v2, 0x1

    if-eq v5, v1, :cond_1

    .line 114
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    .line 115
    or-int/lit8 v2, v2, 0x1

    .line 117
    :cond_1
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v6, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v0

    .line 144
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    :catchall_0
    move-exception v0

    and-int/lit8 v5, v2, 0x1

    if-ne v5, v1, :cond_2

    .line 150
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    .line 152
    :cond_2
    and-int/lit8 v1, v2, 0x2

    if-ne v1, v8, :cond_3

    .line 153
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    .line 156
    :cond_3
    :try_start_2
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 160
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 162
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d()V

    throw v0

    .line 121
    :sswitch_2
    and-int/lit8 v5, v2, 0x2

    if-eq v5, v8, :cond_4

    .line 122
    :try_start_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    .line 123
    or-int/lit8 v2, v2, 0x2

    .line 125
    :cond_4
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 145
    :catch_1
    move-exception v0

    .line 146
    :try_start_4
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 129
    :sswitch_3
    :try_start_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->s()I

    move-result v5

    .line 130
    invoke-virtual {p1, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->c(I)I

    move-result v5

    .line 131
    and-int/lit8 v6, v2, 0x2

    if-eq v6, v8, :cond_5

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v6

    if-lez v6, :cond_5

    .line 132
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    .line 133
    or-int/lit8 v2, v2, 0x2

    .line 135
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v6

    if-lez v6, :cond_6

    .line 136
    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 138
    :cond_6
    invoke-virtual {p1, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->d(I)V
    :try_end_5
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 149
    :cond_7
    and-int/lit8 v0, v2, 0x1

    if-ne v0, v1, :cond_8

    .line 150
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    .line 152
    :cond_8
    and-int/lit8 v0, v2, 0x2

    if-ne v0, v8, :cond_9

    .line 153
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    .line 156
    :cond_9
    :try_start_6
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 160
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 162
    :goto_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d()V

    .line 164
    return-void

    .line 157
    :catch_2
    move-exception v0

    .line 160
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_3

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 157
    :catch_3
    move-exception v1

    .line 160
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto/16 :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 101
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x28 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 71
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 1625
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->g:I

    .line 1631
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->h:B

    .line 1657
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->i:I

    .line 72
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 73
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 74
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 1625
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->g:I

    .line 1631
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->h:B

    .line 1657
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->i:I

    .line 74
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 1

    .prologue
    .line 1749
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->n()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1732
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->e(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Ljava/util/List;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Ljava/util/List;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    return-object v0
.end method

.method public static n()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 1

    .prologue
    .line 1746
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->m()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 1628
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    .line 1629
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    .line 1630
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1643
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->i()I

    move v1, v2

    .line 1644
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1645
    const/4 v3, 0x1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 1644
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1647
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1648
    const/16 v0, 0x2a

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 1649
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->g:I

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 1651
    :cond_1
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1652
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(I)V

    .line 1651
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1654
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 1655
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1563
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    return-object v0
.end method

.method public i()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1659
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->i:I

    .line 1660
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1683
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1663
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1664
    const/4 v4, 0x1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 1663
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 1669
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1670
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->g(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 1669
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_2

    .line 1673
    :cond_2
    add-int v0, v3, v1

    .line 1674
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->m()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1675
    add-int/lit8 v0, v0, 0x1

    .line 1676
    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->g(I)I

    move-result v2

    add-int/2addr v0, v2

    .line 1679
    :cond_3
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->g:I

    .line 1681
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 1682
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->i:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->p()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1633
    iget-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->h:B

    .line 1634
    if-ne v1, v0, :cond_0

    .line 1638
    :goto_0
    return v0

    .line 1635
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1637
    :cond_1
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->h:B

    goto :goto_0
.end method

.method public m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1603
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f:Ljava/util/List;

    return-object v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 1

    .prologue
    .line 1747
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->n()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 1

    .prologue
    .line 1751
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1690
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
