.class public abstract Lkotlin/reflect/jvm/internal/impl/i/b/g;
.super Ljava/lang/Object;
.source "BinaryVersion.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/b/g$a;
    }
.end annotation


# static fields
.field public static final d:Lkotlin/reflect/jvm/internal/impl/i/b/g$a;

# The value of this static final field might be set in the static constructor
.field private static final g:I = -0x1


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final f:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/g$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->d:Lkotlin/reflect/jvm/internal/impl/i/b/g$a;

    .line 67
    const/4 v0, -0x1

    sput v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->g:I

    return-void
.end method

.method public varargs constructor <init>([I)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const-string v0, "numbers"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->f:[I

    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->f:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lkotlin/a/g;->b([II)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_0
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    .line 30
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->f:[I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkotlin/a/g;->b([II)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_1
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->f:[I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lkotlin/a/g;->b([II)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    :goto_2
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->c:I

    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->f:[I

    array-length v0, v0

    if-le v0, v2, :cond_3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->f:[I

    invoke-static {v0}, Lkotlin/a/g;->c([I)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->f:[I

    array-length v1, v1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->k(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->e:Ljava/util/List;

    return-void

    .line 29
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->d:Lkotlin/reflect/jvm/internal/impl/i/b/g$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/g$a;)I

    move-result v0

    goto :goto_0

    .line 30
    :cond_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->d:Lkotlin/reflect/jvm/internal/impl/i/b/g$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/g$a;)I

    move-result v0

    goto :goto_1

    .line 31
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->d:Lkotlin/reflect/jvm/internal/impl/i/b/g$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/g$a;)I

    move-result v0

    goto :goto_2

    .line 32
    :cond_3
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_3
.end method

.method public static final synthetic f()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->g:I

    return v0
.end method


# virtual methods
.method protected final a(Lkotlin/reflect/jvm/internal/impl/i/b/g;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "ourVersion"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    if-nez v2, :cond_2

    iget v2, p1, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    if-nez v2, :cond_1

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    iget v3, p1, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 46
    :cond_2
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    iget v3, p1, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    iget v3, p1, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    if-le v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    return v0
.end method

.method public final e()[I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->f:[I

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    if-nez p1, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.serialization.deserialization.BinaryVersion"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;

    iget v0, v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    if-ne v1, v0, :cond_2

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;

    iget v0, v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    if-ne v1, v0, :cond_2

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->c:I

    move-object v0, p1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;

    iget v0, v0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->c:I

    if-ne v1, v0, :cond_2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->e:Ljava/util/List;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/b/g;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/i/b/g;->e:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 59
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->a:I

    .line 60
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->b:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 61
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->c:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 62
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/g;->e:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 63
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 50
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/g;->e()[I

    move-result-object v5

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move v1, v4

    .line 72
    :goto_0
    array-length v3, v5

    if-ge v1, v3, :cond_0

    aget v6, v5, v1

    .line 50
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/b/g;->d:Lkotlin/reflect/jvm/internal/impl/i/b/g$a;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/g$a;)I

    move-result v3

    if-eq v6, v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    if-nez v3, :cond_2

    .line 77
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 51
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "unknown"

    :goto_2
    return-object v0

    :cond_1
    move v3, v4

    .line 50
    goto :goto_1

    .line 75
    :cond_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    :cond_3
    check-cast v0, Ljava/lang/Iterable;

    const-string v1, "."

    check-cast v1, Ljava/lang/CharSequence;

    const/16 v7, 0x3e

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
