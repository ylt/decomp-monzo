.class public final enum Lkotlin/reflect/jvm/internal/impl/i/b/b;
.super Ljava/lang/Enum;
.source "AnnotatedCallableKind.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/i/b/b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/i/b/b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/i/b/b;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/i/b/b;

.field private static final synthetic e:[Lkotlin/reflect/jvm/internal/impl/i/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;

    const-string v1, "FUNCTION"

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    .line 21
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;

    const-string v1, "PROPERTY"

    invoke-direct {v0, v1, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;->b:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    .line 22
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;

    const-string v1, "PROPERTY_GETTER"

    invoke-direct {v0, v1, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;->c:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    .line 23
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;

    const-string v1, "PROPERTY_SETTER"

    invoke-direct {v0, v1, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;->d:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    .line 19
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/i/b/b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/b/b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/b/b;->b:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/b/b;->c:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/b/b;->d:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;->e:[Lkotlin/reflect/jvm/internal/impl/i/b/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/b/b;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/i/b/b;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/b;->e:[Lkotlin/reflect/jvm/internal/impl/i/b/b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/i/b/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/i/b/b;

    return-object v0
.end method
