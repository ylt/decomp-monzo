.class public abstract Lkotlin/reflect/jvm/internal/impl/i/c$c;
.super Ljava/lang/Object;
.source "Flags.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final a:I

.field protected final b:I


# direct methods
.method private constructor <init>(II)V
    .locals 0

    .prologue
    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c$c;->a:I

    .line 309
    iput p2, p0, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b:I

    .line 310
    return-void
.end method

.method synthetic constructor <init>(IILkotlin/reflect/jvm/internal/impl/i/c$1;)V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c$c;-><init>(II)V

    return-void
.end method

.method public static a()Lkotlin/reflect/jvm/internal/impl/i/c$a;
    .locals 2

    .prologue
    .line 296
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$a;-><init>(I)V

    return-object v0
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/c$c",
            "<*>;)",
            "Lkotlin/reflect/jvm/internal/impl/i/c$a;"
        }
    .end annotation

    .prologue
    .line 300
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c$c;->a:I

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b:I

    add-int/2addr v0, v1

    .line 301
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;-><init>(I)V

    return-object v1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/c$c;[Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;)Lkotlin/reflect/jvm/internal/impl/i/c$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/i/c$c",
            "<*>;[TE;)",
            "Lkotlin/reflect/jvm/internal/impl/i/c$c",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 287
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c$c;->a:I

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b:I

    add-int/2addr v0, v1

    .line 288
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/c$b;

    invoke-direct {v1, v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c$b;-><init>(I[Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;)V

    return-object v1
.end method


# virtual methods
.method public abstract b(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation
.end method
