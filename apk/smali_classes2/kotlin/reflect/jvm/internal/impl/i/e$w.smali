.class public final Lkotlin/reflect/jvm/internal/impl/i/e$w;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$x;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "w"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$w;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$w;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 378
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$w$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 722
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->c:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    .line 723
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->c:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->p()V

    .line 724
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 325
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 425
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f:B

    .line 444
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->g:I

    .line 326
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->p()V

    .line 328
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v3

    .line 330
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v4

    move v2, v0

    .line 335
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 336
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v5

    .line 337
    sparse-switch v5, :sswitch_data_0

    .line 342
    invoke-virtual {p0, p1, v4, p2, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 344
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 340
    goto :goto_0

    .line 349
    :sswitch_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->l()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v5

    .line 350
    and-int/lit8 v6, v2, 0x1

    if-eq v6, v1, :cond_1

    .line 351
    new-instance v6, Lkotlin/reflect/jvm/internal/impl/protobuf/l;

    invoke-direct {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/l;-><init>()V

    iput-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 352
    or-int/lit8 v2, v2, 0x1

    .line 354
    :cond_1
    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v6, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 359
    :catch_0
    move-exception v0

    .line 360
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_2

    .line 366
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 369
    :cond_2
    :try_start_2
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 373
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 375
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d()V

    throw v0

    .line 365
    :cond_3
    and-int/lit8 v0, v2, 0x1

    if-ne v0, v1, :cond_4

    .line 366
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 369
    :cond_4
    :try_start_3
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 373
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 375
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d()V

    .line 377
    return-void

    .line 370
    :catch_1
    move-exception v0

    .line 373
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 361
    :catch_2
    move-exception v0

    .line 362
    :try_start_4
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 370
    :catch_3
    move-exception v1

    .line 373
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 337
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 301
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$w;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 307
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 425
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f:B

    .line 444
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->g:I

    .line 308
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 309
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 310
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 425
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f:B

    .line 444
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->g:I

    .line 310
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 1

    .prologue
    .line 527
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->m()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$w;Lkotlin/reflect/jvm/internal/impl/protobuf/m;)Lkotlin/reflect/jvm/internal/impl/protobuf/m;
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/protobuf/m;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$w;
    .locals 1

    .prologue
    .line 314
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->c:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    return-object v0
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 1

    .prologue
    .line 524
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 423
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/l;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 424
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 437
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->i()I

    .line 438
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 439
    const/4 v1, 0x1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->c(I)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 438
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 441
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 442
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 390
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->g()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$w;
    .locals 1

    .prologue
    .line 318
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->c:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    return-object v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/protobuf/r;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    return-object v0
.end method

.method public i()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 446
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->g:I

    .line 447
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 461
    :goto_0
    return v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 452
    :goto_1
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 453
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->c(I)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v3

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)I

    move-result v3

    add-int/2addr v2, v3

    .line 452
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 456
    :cond_1
    add-int v0, v1, v2

    .line 457
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->h()Lkotlin/reflect/jvm/internal/impl/protobuf/r;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/r;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 459
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 460
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->g:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->n()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->o()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 427
    iget-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f:B

    .line 428
    if-ne v1, v0, :cond_0

    .line 432
    :goto_0
    return v0

    .line 429
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 431
    :cond_1
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f:B

    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 1

    .prologue
    .line 525
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->m()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 1

    .prologue
    .line 529
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 468
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
