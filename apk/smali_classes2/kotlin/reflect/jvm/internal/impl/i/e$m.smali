.class public final Lkotlin/reflect/jvm/internal/impl/i/e$m;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$n;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "m"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$m;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$n;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$m;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$m;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

.field private j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11228
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$m$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 12223
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    .line 12224
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->B()V

    .line 12225
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v1, 0x1

    .line 11128
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 11386
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    .line 11449
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->l:I

    .line 11129
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->B()V

    .line 11131
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v5

    .line 11133
    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v6

    move v3, v2

    .line 11138
    :goto_0
    if-nez v2, :cond_2

    .line 11139
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 11140
    sparse-switch v0, :sswitch_data_0

    .line 11145
    invoke-virtual {p0, p1, v6, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v1

    move v2, v3

    :goto_1
    move v3, v2

    move v2, v0

    .line 11202
    goto :goto_0

    :sswitch_0
    move v0, v1

    move v2, v3

    .line 11143
    goto :goto_1

    .line 11152
    :sswitch_1
    and-int/lit8 v0, v3, 0x1

    if-eq v0, v1, :cond_d

    .line 11153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 11154
    or-int/lit8 v0, v3, 0x1

    .line 11156
    :goto_2
    :try_start_1
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move v9, v2

    move v2, v0

    move v0, v9

    .line 11157
    goto :goto_1

    .line 11160
    :sswitch_2
    and-int/lit8 v0, v3, 0x2

    if-eq v0, v7, :cond_c

    .line 11161
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;
    :try_end_2
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 11162
    or-int/lit8 v0, v3, 0x2

    .line 11164
    :goto_3
    :try_start_3
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move v9, v2

    move v2, v0

    move v0, v9

    .line 11165
    goto :goto_1

    .line 11168
    :sswitch_3
    and-int/lit8 v0, v3, 0x4

    if-eq v0, v8, :cond_b

    .line 11169
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;
    :try_end_4
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 11170
    or-int/lit8 v0, v3, 0x4

    .line 11172
    :goto_4
    :try_start_5
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move v9, v2

    move v2, v0

    move v0, v9

    .line 11173
    goto :goto_1

    .line 11176
    :sswitch_4
    const/4 v0, 0x0

    .line 11177
    :try_start_6
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v1, :cond_a

    .line 11178
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->r()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    move-object v4, v0

    .line 11180
    :goto_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 11181
    if-eqz v4, :cond_0

    .line 11182
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    .line 11183
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 11185
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    move v0, v2

    move v2, v3

    .line 11186
    goto/16 :goto_1

    .line 11189
    :sswitch_5
    const/4 v0, 0x0

    .line 11190
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    and-int/lit8 v4, v4, 0x2

    if-ne v4, v7, :cond_9

    .line 11191
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->p()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    move-object v4, v0

    .line 11193
    :goto_6
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$u;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$u;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 11194
    if-eqz v4, :cond_1

    .line 11195
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    .line 11196
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 11198
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I
    :try_end_6
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move v0, v2

    move v2, v3

    .line 11199
    goto/16 :goto_1

    .line 11209
    :cond_2
    and-int/lit8 v0, v3, 0x1

    if-ne v0, v1, :cond_3

    .line 11210
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    .line 11212
    :cond_3
    and-int/lit8 v0, v3, 0x2

    if-ne v0, v7, :cond_4

    .line 11213
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    .line 11215
    :cond_4
    and-int/lit8 v0, v3, 0x4

    if-ne v0, v8, :cond_5

    .line 11216
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    .line 11219
    :cond_5
    :try_start_7
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 11223
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 11225
    :goto_7
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d()V

    .line 11227
    return-void

    .line 11220
    :catch_0
    move-exception v0

    .line 11223
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_7

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 11203
    :catch_1
    move-exception v0

    .line 11204
    :goto_8
    :try_start_8
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 11209
    :catchall_1
    move-exception v0

    :goto_9
    and-int/lit8 v2, v3, 0x1

    if-ne v2, v1, :cond_6

    .line 11210
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    .line 11212
    :cond_6
    and-int/lit8 v1, v3, 0x2

    if-ne v1, v7, :cond_7

    .line 11213
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    .line 11215
    :cond_7
    and-int/lit8 v1, v3, 0x4

    if-ne v1, v8, :cond_8

    .line 11216
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    .line 11219
    :cond_8
    :try_start_9
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 11223
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 11225
    :goto_a
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d()V

    throw v0

    .line 11205
    :catch_2
    move-exception v0

    .line 11206
    :goto_b
    :try_start_a
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 11220
    :catch_3
    move-exception v1

    .line 11223
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_a

    :catchall_2
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 11209
    :catchall_3
    move-exception v2

    move v3, v0

    move-object v0, v2

    goto :goto_9

    .line 11205
    :catch_4
    move-exception v2

    move v3, v0

    move-object v0, v2

    goto :goto_b

    .line 11203
    :catch_5
    move-exception v2

    move v3, v0

    move-object v0, v2

    goto :goto_8

    :cond_9
    move-object v4, v0

    goto/16 :goto_6

    :cond_a
    move-object v4, v0

    goto/16 :goto_5

    :cond_b
    move v0, v3

    goto/16 :goto_4

    :cond_c
    move v0, v3

    goto/16 :goto_3

    :cond_d
    move v0, v3

    goto/16 :goto_2

    :cond_e
    move v0, v2

    move v2, v3

    goto/16 :goto_1

    .line 11140
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0xf2 -> :sswitch_4
        0x102 -> :sswitch_5
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 11103
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$m;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$m;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 11110
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 11386
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    .line 11449
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->l:I

    .line 11111
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 11112
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 11103
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 11113
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 11386
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    .line 11449
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->l:I

    .line 11113
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method private B()V
    .locals 1

    .prologue
    .line 11380
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    .line 11381
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    .line 11382
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    .line 11383
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 11384
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 11385
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$m;I)I
    .locals 0

    .prologue
    .line 11103
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$m;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 11103
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$m;Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 0

    .prologue
    .line 11103
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 1

    .prologue
    .line 11544
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->y()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11517
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->f(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$m;Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u;
    .locals 0

    .prologue
    .line 11103
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;
    .locals 1

    .prologue
    .line 11103
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$m;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 11103
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;
    .locals 1

    .prologue
    .line 11103
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$m;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 11103
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    return-object p1
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Ljava/util/List;
    .locals 1

    .prologue
    .line 11103
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 11103
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 1

    .prologue
    .line 11117
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    return-object v0
.end method

.method public static y()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 1

    .prologue
    .line 11541
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->t()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 1

    .prologue
    .line 11546
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 1

    .prologue
    .line 11269
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 11426
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i()I

    .line 11428
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v3

    move v1, v2

    .line 11430
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 11431
    const/4 v4, 0x3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 11430
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 11433
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 11434
    const/4 v4, 0x4

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 11433
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 11436
    :cond_1
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 11437
    const/4 v1, 0x5

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 11436
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 11439
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 11440
    const/16 v0, 0x1e

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 11442
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 11443
    const/16 v0, 0x20

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 11445
    :cond_4
    const/16 v0, 0xc8

    invoke-virtual {v3, v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 11446
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 11447
    return-void
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 1

    .prologue
    .line 11304
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 1

    .prologue
    .line 11339
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11240
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 11103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->n()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 11451
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->l:I

    .line 11452
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 11478
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 11455
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 11456
    const/4 v4, 0x3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 11455
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 11459
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 11460
    const/4 v4, 0x4

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 11459
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 11463
    :cond_2
    :goto_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 11464
    const/4 v1, 0x5

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 11463
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 11467
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 11468
    const/16 v0, 0x1e

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 11471
    :cond_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 11472
    const/16 v0, 0x20

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 11475
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h()I

    move-result v0

    add-int/2addr v0, v3

    .line 11476
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 11477
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->l:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 11103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->z()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 11103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->A()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 11388
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    .line 11389
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 11421
    :cond_0
    :goto_0
    return v1

    .line 11390
    :cond_1
    if-eqz v0, :cond_0

    move v0, v1

    .line 11392
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->p()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 11393
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l()Z

    move-result v3

    if-nez v3, :cond_2

    .line 11394
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    goto :goto_0

    .line 11392
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 11398
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->r()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 11399
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l()Z

    move-result v3

    if-nez v3, :cond_4

    .line 11400
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    goto :goto_0

    .line 11398
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 11404
    :goto_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->t()I

    move-result v3

    if-ge v0, v3, :cond_7

    .line 11405
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l()Z

    move-result v3

    if-nez v3, :cond_6

    .line 11406
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    goto :goto_0

    .line 11404
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 11410
    :cond_7
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->u()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 11411
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->v()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->l()Z

    move-result v0

    if-nez v0, :cond_8

    .line 11412
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    goto :goto_0

    .line 11416
    :cond_8
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f()Z

    move-result v0

    if-nez v0, :cond_9

    .line 11417
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    goto :goto_0

    .line 11420
    :cond_9
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->k:B

    move v1, v2

    .line 11421
    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 1

    .prologue
    .line 11121
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->c:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    return-object v0
.end method

.method public o()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11250
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    return-object v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 11263
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public q()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11285
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    return-object v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 11298
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public s()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11320
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    return-object v0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 11333
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public u()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 11355
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 1

    .prologue
    .line 11361
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 11370
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 11485
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/i/e$u;
    .locals 1

    .prologue
    .line 11376
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->j:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    return-object v0
.end method

.method public z()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;
    .locals 1

    .prologue
    .line 11542
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->y()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    return-object v0
.end method
