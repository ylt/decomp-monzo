.class public final Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "BuiltInsProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/a/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

.field private g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

.field private h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
            ">;"
        }
    .end annotation
.end field

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 184
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 876
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    .line 877
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->w()V

    .line 878
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v5, 0x0

    const/16 v8, 0x8

    const/4 v1, 0x1

    .line 93
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 286
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->j:B

    .line 332
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->k:I

    .line 94
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->w()V

    .line 96
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v6

    .line 98
    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v7

    move v3, v2

    .line 103
    :goto_0
    if-nez v2, :cond_3

    .line 104
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 105
    sparse-switch v0, :sswitch_data_0

    .line 110
    invoke-virtual {p0, p1, v7, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    move v2, v3

    :goto_1
    move v3, v2

    move v2, v0

    .line 164
    goto :goto_0

    :sswitch_0
    move v0, v1

    move v2, v3

    .line 108
    goto :goto_1

    .line 118
    :sswitch_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_9

    .line 119
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->o()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    move-object v4, v0

    .line 121
    :goto_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    .line 122
    if-eqz v4, :cond_0

    .line 123
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    .line 124
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    .line 126
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    move v0, v2

    move v2, v3

    .line 127
    goto :goto_1

    .line 131
    :sswitch_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_8

    .line 132
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->o()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    move-object v4, v0

    .line 134
    :goto_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    .line 135
    if-eqz v4, :cond_1

    .line 136
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    .line 137
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    .line 139
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    move v0, v2

    move v2, v3

    .line 140
    goto :goto_1

    .line 144
    :sswitch_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_7

    .line 145
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->A()Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    move-object v4, v0

    .line 147
    :goto_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$m;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    .line 148
    if-eqz v4, :cond_2

    .line 149
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    .line 150
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    .line 152
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    move v0, v2

    move v2, v3

    .line 153
    goto/16 :goto_1

    .line 156
    :sswitch_4
    and-int/lit8 v0, v3, 0x8

    if-eq v0, v8, :cond_6

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 158
    or-int/lit8 v0, v3, 0x8

    .line 160
    :goto_5
    :try_start_1
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move v9, v2

    move v2, v0

    move v0, v9

    goto/16 :goto_1

    .line 171
    :cond_3
    and-int/lit8 v0, v3, 0x8

    if-ne v0, v8, :cond_4

    .line 172
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    .line 175
    :cond_4
    :try_start_2
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 179
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 181
    :goto_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d()V

    .line 183
    return-void

    .line 176
    :catch_0
    move-exception v0

    .line 179
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_6

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 165
    :catch_1
    move-exception v0

    .line 166
    :goto_7
    :try_start_3
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 171
    :catchall_1
    move-exception v0

    :goto_8
    and-int/lit8 v1, v3, 0x8

    if-ne v1, v8, :cond_5

    .line 172
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    .line 175
    :cond_5
    :try_start_4
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 179
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 181
    :goto_9
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d()V

    throw v0

    .line 167
    :catch_2
    move-exception v0

    .line 168
    :goto_a
    :try_start_5
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 176
    :catch_3
    move-exception v1

    .line 179
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_9

    :catchall_2
    move-exception v0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 171
    :catchall_3
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_8

    .line 167
    :catch_4
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_a

    .line 165
    :catch_5
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_7

    :cond_6
    move v0, v3

    goto/16 :goto_5

    :cond_7
    move-object v4, v5

    goto/16 :goto_4

    :cond_8
    move-object v4, v5

    goto/16 :goto_3

    :cond_9
    move-object v4, v5

    goto/16 :goto_2

    :cond_a
    move v0, v2

    move v2, v3

    goto/16 :goto_1

    .line 105
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/a/a$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 75
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 286
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->j:B

    .line 332
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->k:I

    .line 76
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 77
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/a/a$1;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 78
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 286
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->j:B

    .line 332
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->k:I

    .line 78
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;I)I
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 1

    .prologue
    .line 422
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->t()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 395
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->f(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    return-object v0
.end method

.method public static t()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 1

    .prologue
    .line 419
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->r()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 281
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    .line 282
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    .line 283
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->m()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    .line 284
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    .line 285
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$c;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 316
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i()I

    .line 317
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 318
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 320
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 321
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 323
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 324
    const/4 v0, 0x3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 326
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 327
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 326
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 329
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 330
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/a/a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 206
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 334
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->k:I

    .line 335
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 356
    :goto_0
    return v0

    .line 338
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 339
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v0, v1

    .line 342
    :goto_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 343
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-static {v4, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v2

    add-int/2addr v0, v2

    .line 346
    :cond_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 347
    const/4 v2, 0x3

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    move v2, v0

    .line 350
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 351
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v0, v2

    .line 350
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 354
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v0

    add-int/2addr v0, v2

    .line 355
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->k:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->u()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->v()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 288
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->j:B

    .line 289
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 311
    :cond_0
    :goto_0
    return v1

    .line 290
    :cond_1
    if-eqz v0, :cond_0

    .line 292
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->l()Z

    move-result v0

    if-nez v0, :cond_2

    .line 294
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->j:B

    goto :goto_0

    .line 298
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 299
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->l()Z

    move-result v0

    if-nez v0, :cond_3

    .line 300
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->j:B

    goto :goto_0

    :cond_3
    move v0, v1

    .line 304
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->s()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 305
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->l()Z

    move-result v3

    if-nez v3, :cond_4

    .line 306
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->j:B

    goto :goto_0

    .line 304
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 310
    :cond_5
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->j:B

    move v1, v2

    .line 311
    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$w;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    return-object v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 221
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/e$q;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    return-object v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 236
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    return-object v0
.end method

.method public r()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    return-object v0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public u()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 1

    .prologue
    .line 420
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->t()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 1

    .prologue
    .line 424
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 363
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
