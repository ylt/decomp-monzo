.class public final Lkotlin/reflect/jvm/internal/impl/i/e$y;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$ab;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "y"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$y$c;,
        Lkotlin/reflect/jvm/internal/impl/i/e$y$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$y$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ab;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$y;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:I

.field private i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private p:I

.field private q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private r:I

.field private s:I

.field private t:B

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 4679
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 6848
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 6849
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->T()V

    .line 6850
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 4538
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 5665
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    .line 5755
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->u:I

    .line 4539
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->T()V

    .line 4541
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v6

    .line 4543
    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v7

    move v3, v2

    .line 4548
    :goto_0
    if-nez v2, :cond_4

    .line 4549
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 4550
    sparse-switch v0, :sswitch_data_0

    .line 4555
    invoke-virtual {p0, p1, v7, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    move v2, v3

    :goto_1
    move v3, v2

    move v2, v0

    .line 4659
    goto :goto_0

    :sswitch_0
    move v0, v1

    move v2, v3

    .line 4553
    goto :goto_1

    .line 4562
    :sswitch_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4563
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->s:I

    move v0, v2

    move v2, v3

    .line 4564
    goto :goto_1

    .line 4567
    :sswitch_2
    and-int/lit8 v0, v3, 0x1

    if-eq v0, v1, :cond_a

    .line 4568
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4569
    or-int/lit8 v0, v3, 0x1

    .line 4571
    :goto_2
    :try_start_1
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move v8, v2

    move v2, v0

    move v0, v8

    .line 4572
    goto :goto_1

    .line 4575
    :sswitch_3
    :try_start_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4576
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->i()Z

    move-result v0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->g:Z

    move v0, v2

    move v2, v3

    .line 4577
    goto :goto_1

    .line 4580
    :sswitch_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4581
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->h:I

    move v0, v2

    move v2, v3

    .line 4582
    goto :goto_1

    .line 4586
    :sswitch_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_9

    .line 4587
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v4, v0

    .line 4589
    :goto_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4590
    if-eqz v4, :cond_0

    .line 4591
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 4592
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4594
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    move v0, v2

    move v2, v3

    .line 4595
    goto :goto_1

    .line 4598
    :sswitch_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4599
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->k:I

    move v0, v2

    move v2, v3

    .line 4600
    goto/16 :goto_1

    .line 4603
    :sswitch_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4604
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l:I

    move v0, v2

    move v2, v3

    .line 4605
    goto/16 :goto_1

    .line 4608
    :sswitch_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4609
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->j:I

    move v0, v2

    move v2, v3

    .line 4610
    goto/16 :goto_1

    .line 4613
    :sswitch_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4614
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m:I

    move v0, v2

    move v2, v3

    .line 4615
    goto/16 :goto_1

    .line 4619
    :sswitch_a
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_8

    .line 4620
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v4, v0

    .line 4622
    :goto_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4623
    if-eqz v4, :cond_1

    .line 4624
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 4625
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4627
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    move v0, v2

    move v2, v3

    .line 4628
    goto/16 :goto_1

    .line 4631
    :sswitch_b
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4632
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->p:I

    move v0, v2

    move v2, v3

    .line 4633
    goto/16 :goto_1

    .line 4636
    :sswitch_c
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4637
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->n:I

    move v0, v2

    move v2, v3

    .line 4638
    goto/16 :goto_1

    .line 4642
    :sswitch_d
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_7

    .line 4643
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v4, v0

    .line 4645
    :goto_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4646
    if-eqz v4, :cond_2

    .line 4647
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 4648
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4650
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    move v0, v2

    move v2, v3

    .line 4651
    goto/16 :goto_1

    .line 4654
    :sswitch_e
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    .line 4655
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r:I
    :try_end_2
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    move v0, v2

    move v2, v3

    goto/16 :goto_1

    .line 4666
    :cond_4
    and-int/lit8 v0, v3, 0x1

    if-ne v0, v1, :cond_5

    .line 4667
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    .line 4670
    :cond_5
    :try_start_3
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4674
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 4676
    :goto_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d()V

    .line 4678
    return-void

    .line 4671
    :catch_0
    move-exception v0

    .line 4674
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_6

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 4660
    :catch_1
    move-exception v0

    .line 4661
    :goto_7
    :try_start_4
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 4666
    :catchall_1
    move-exception v0

    :goto_8
    and-int/lit8 v2, v3, 0x1

    if-ne v2, v1, :cond_6

    .line 4667
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    .line 4670
    :cond_6
    :try_start_5
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 4674
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 4676
    :goto_9
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d()V

    throw v0

    .line 4662
    :catch_2
    move-exception v0

    .line 4663
    :goto_a
    :try_start_6
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 4671
    :catch_3
    move-exception v1

    .line 4674
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_9

    :catchall_2
    move-exception v0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 4666
    :catchall_3
    move-exception v2

    move v3, v0

    move-object v0, v2

    goto :goto_8

    .line 4662
    :catch_4
    move-exception v2

    move v3, v0

    move-object v0, v2

    goto :goto_a

    .line 4660
    :catch_5
    move-exception v2

    move v3, v0

    move-object v0, v2

    goto :goto_7

    :cond_7
    move-object v4, v5

    goto/16 :goto_5

    :cond_8
    move-object v4, v5

    goto/16 :goto_4

    :cond_9
    move-object v4, v5

    goto/16 :goto_3

    :cond_a
    move v0, v3

    goto/16 :goto_2

    .line 4550
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 4513
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$y;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 4520
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 5665
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    .line 5755
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->u:I

    .line 4521
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 4522
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 4513
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4523
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 5665
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    .line 5755
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->u:I

    .line 4523
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method public static Q()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 5883
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method private T()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5650
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    .line 5651
    iput-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->g:Z

    .line 5652
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->h:I

    .line 5653
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 5654
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->j:I

    .line 5655
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->k:I

    .line 5656
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l:I

    .line 5657
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m:I

    .line 5658
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->n:I

    .line 5659
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 5660
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->p:I

    .line 5661
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 5662
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r:I

    .line 5663
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->s:I

    .line 5664
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->h:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 4513
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 5886
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->Q()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 4513
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Z)Z
    .locals 0

    .prologue
    .line 4513
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->g:Z

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->j:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Ljava/util/List;
    .locals 1

    .prologue
    .line 4513
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 4513
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->k:I

    return p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 4513
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 4513
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l:I

    return p1
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m:I

    return p1
.end method

.method static synthetic f(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->n:I

    return p1
.end method

.method static synthetic g(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->p:I

    return p1
.end method

.method static synthetic h(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r:I

    return p1
.end method

.method static synthetic i(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->s:I

    return p1
.end method

.method static synthetic j(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I
    .locals 0

    .prologue
    .line 4513
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    return p1
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 4527
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 5505
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()I
    .locals 1

    .prologue
    .line 5515
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l:I

    return v0
.end method

.method public C()Z
    .locals 2

    .prologue
    .line 5528
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()I
    .locals 1

    .prologue
    .line 5538
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m:I

    return v0
.end method

.method public E()Z
    .locals 2

    .prologue
    .line 5552
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()I
    .locals 1

    .prologue
    .line 5563
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->n:I

    return v0
.end method

.method public G()Z
    .locals 2

    .prologue
    .line 5572
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 5578
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public I()Z
    .locals 2

    .prologue
    .line 5587
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()I
    .locals 1

    .prologue
    .line 5593
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->p:I

    return v0
.end method

.method public K()Z
    .locals 2

    .prologue
    .line 5602
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 5608
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public M()Z
    .locals 2

    .prologue
    .line 5617
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()I
    .locals 1

    .prologue
    .line 5623
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r:I

    return v0
.end method

.method public O()Z
    .locals 2

    .prologue
    .line 5636
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public P()I
    .locals 1

    .prologue
    .line 5646
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->s:I

    return v0
.end method

.method public R()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 5884
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->Q()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 5888
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    .locals 1

    .prologue
    .line 5400
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 5705
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i()I

    .line 5707
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v2

    .line 5709
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    .line 5710
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->s:I

    invoke-virtual {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5712
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 5713
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 5712
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 5715
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 5716
    const/4 v0, 0x3

    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->g:Z

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(IZ)V

    .line 5718
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 5719
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->h:I

    invoke-virtual {p1, v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5721
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 5722
    const/4 v0, 0x5

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 5724
    :cond_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 5725
    const/4 v0, 0x6

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->k:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5727
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 5728
    const/4 v0, 0x7

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5730
    :cond_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_7

    .line 5731
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->j:I

    invoke-virtual {p1, v6, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5733
    :cond_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 5734
    const/16 v0, 0x9

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5736
    :cond_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 5737
    const/16 v0, 0xa

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 5739
    :cond_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 5740
    const/16 v0, 0xb

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->p:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5742
    :cond_a
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_b

    .line 5743
    const/16 v0, 0xc

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->n:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5745
    :cond_b
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 5746
    const/16 v0, 0xd

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 5748
    :cond_c
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 5749
    const/16 v0, 0xe

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5751
    :cond_d
    const/16 v0, 0xc8

    invoke-virtual {v2, v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 5752
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 5753
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4691
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 4513
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 5757
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->u:I

    .line 5758
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5820
    :goto_0
    return v0

    .line 5761
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_e

    .line 5762
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->s:I

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v0

    .line 5765
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 5766
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v0, v2

    .line 5765
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 5769
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 5770
    const/4 v0, 0x3

    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->g:Z

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 5773
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 5774
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->h:I

    invoke-static {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 5777
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 5778
    const/4 v0, 0x5

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v2, v0

    .line 5781
    :cond_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 5782
    const/4 v0, 0x6

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->k:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 5785
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 5786
    const/4 v0, 0x7

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 5789
    :cond_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_7

    .line 5790
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->j:I

    invoke-static {v6, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 5793
    :cond_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 5794
    const/16 v0, 0x9

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 5797
    :cond_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 5798
    const/16 v0, 0xa

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->o:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v2, v0

    .line 5801
    :cond_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 5802
    const/16 v0, 0xb

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->p:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 5805
    :cond_a
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_b

    .line 5806
    const/16 v0, 0xc

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->n:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 5809
    :cond_b
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 5810
    const/16 v0, 0xd

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v2, v0

    .line 5813
    :cond_c
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 5814
    const/16 v0, 0xe

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 5817
    :cond_d
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->h()I

    move-result v0

    add-int/2addr v0, v2

    .line 5818
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 5819
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->u:I

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 4513
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->R()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 4513
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5667
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    .line 5668
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 5700
    :cond_0
    :goto_0
    return v1

    .line 5669
    :cond_1
    if-eqz v0, :cond_0

    move v0, v1

    .line 5671
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->p()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 5672
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->l()Z

    move-result v3

    if-nez v3, :cond_2

    .line 5673
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    goto :goto_0

    .line 5671
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5677
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5678
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_4

    .line 5679
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    goto :goto_0

    .line 5683
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->G()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5684
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->H()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5685
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    goto :goto_0

    .line 5689
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->K()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5690
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->L()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_6

    .line 5691
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    goto :goto_0

    .line 5695
    :cond_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f()Z

    move-result v0

    if-nez v0, :cond_7

    .line 5696
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    goto :goto_0

    .line 5699
    :cond_7
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t:B

    move v1, v2

    .line 5700
    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 4531
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public o()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5381
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    return-object v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 5394
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public q()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5416
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 5422
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->g:Z

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 5436
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 5447
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->h:I

    return v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 5456
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 5462
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 5471
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 5827
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 5477
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->j:I

    return v0
.end method

.method public y()Z
    .locals 2

    .prologue
    .line 5486
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()I
    .locals 1

    .prologue
    .line 5492
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->k:I

    return v0
.end method
