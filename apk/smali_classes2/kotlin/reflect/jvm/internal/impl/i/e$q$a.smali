.class public final Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$r;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$q;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$q$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$r;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$q$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1619
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 1709
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    .line 1620
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->o()V

    .line 1621
    return-void
.end method

.method static synthetic n()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 1

    .prologue
    .line 1612
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 0

    .prologue
    .line 1624
    return-void
.end method

.method private static p()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 1

    .prologue
    .line 1626
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;-><init>()V

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1712
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1713
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    .line 1714
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    .line 1716
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 2

    .prologue
    .line 1664
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1677
    :goto_0
    return-object p0

    .line 1665
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->b(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1666
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1667
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->b(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    .line 1668
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    .line 1675
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->c(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0

    .line 1670
    :cond_2
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->q()V

    .line 1671
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->b(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    .locals 1

    .prologue
    .line 1734
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    return-object v0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 1612
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1612
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 1612
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 1612
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1612
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1694
    const/4 v2, 0x0

    .line 1696
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1701
    if-eqz v0, :cond_0

    .line 1702
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    .line 1705
    :cond_0
    return-object p0

    .line 1697
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1698
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1699
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1701
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 1702
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    :cond_1
    throw v0

    .line 1701
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 1612
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1612
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1612
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 2

    .prologue
    .line 1637
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$q;
    .locals 1

    .prologue
    .line 1641
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1612
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1612
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$q;
    .locals 2

    .prologue
    .line 1645
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    .line 1646
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1647
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1649
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$q;
    .locals 3

    .prologue
    .line 1653
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 1654
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    .line 1655
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1656
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    .line 1657
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a:I

    .line 1659
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;Ljava/util/List;)Ljava/util/List;

    .line 1660
    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1681
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->m()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1682
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->l()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1687
    :goto_1
    return v1

    .line 1681
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1687
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public m()I
    .locals 1

    .prologue
    .line 1728
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
