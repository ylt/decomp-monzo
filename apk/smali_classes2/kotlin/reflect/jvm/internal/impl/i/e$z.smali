.class public final Lkotlin/reflect/jvm/internal/impl/i/e$z;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$aa;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "z"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$aa;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$z;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private j:I

.field private k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private l:I

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private n:I

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18224
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$z$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 19416
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->c:Lkotlin/reflect/jvm/internal/impl/i/e$z;

    .line 19417
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->c:Lkotlin/reflect/jvm/internal/impl/i/e$z;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->J()V

    .line 19418
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    const/16 v9, 0x80

    const/4 v8, 0x4

    .line 18110
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 18444
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    .line 18523
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->p:I

    .line 18111
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->J()V

    .line 18113
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v5

    .line 18115
    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v6

    move v3, v2

    .line 18120
    :goto_0
    if-nez v2, :cond_3

    .line 18121
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 18122
    sparse-switch v0, :sswitch_data_0

    .line 18127
    invoke-virtual {p0, p1, v6, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    move v2, v3

    :goto_1
    move v3, v2

    move v2, v0

    .line 18201
    goto :goto_0

    :sswitch_0
    move v0, v1

    move v2, v3

    .line 18125
    goto :goto_1

    .line 18134
    :sswitch_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    .line 18135
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->f:I

    move v0, v2

    move v2, v3

    .line 18136
    goto :goto_1

    .line 18139
    :sswitch_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    .line 18140
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->g:I

    move v0, v2

    move v2, v3

    .line 18141
    goto :goto_1

    .line 18144
    :sswitch_3
    and-int/lit8 v0, v3, 0x4

    if-eq v0, v8, :cond_b

    .line 18145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 18146
    or-int/lit8 v0, v3, 0x4

    .line 18148
    :goto_2
    :try_start_1
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move v10, v2

    move v2, v0

    move v0, v10

    .line 18149
    goto :goto_1

    .line 18152
    :sswitch_4
    const/4 v0, 0x0

    .line 18153
    :try_start_2
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v8, :cond_a

    .line 18154
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v4, v0

    .line 18156
    :goto_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 18157
    if-eqz v4, :cond_0

    .line 18158
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 18159
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 18161
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    move v0, v2

    move v2, v3

    .line 18162
    goto :goto_1

    .line 18165
    :sswitch_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    .line 18166
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->j:I

    move v0, v2

    move v2, v3

    .line 18167
    goto :goto_1

    .line 18170
    :sswitch_6
    const/4 v0, 0x0

    .line 18171
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v4, v4, 0x10

    const/16 v7, 0x10

    if-ne v4, v7, :cond_9

    .line 18172
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v4, v0

    .line 18174
    :goto_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 18175
    if-eqz v4, :cond_1

    .line 18176
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 18177
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 18179
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    move v0, v2

    move v2, v3

    .line 18180
    goto/16 :goto_1

    .line 18183
    :sswitch_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    .line 18184
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l:I

    move v0, v2

    move v2, v3

    .line 18185
    goto/16 :goto_1

    .line 18188
    :sswitch_8
    and-int/lit16 v0, v3, 0x80

    if-eq v0, v9, :cond_8

    .line 18189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;
    :try_end_2
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 18190
    or-int/lit16 v0, v3, 0x80

    .line 18192
    :goto_5
    :try_start_3
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move v10, v2

    move v2, v0

    move v0, v10

    .line 18193
    goto/16 :goto_1

    .line 18196
    :sswitch_9
    :try_start_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    .line 18197
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->n:I
    :try_end_4
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_2
    move v0, v2

    move v2, v3

    goto/16 :goto_1

    .line 18208
    :cond_3
    and-int/lit8 v0, v3, 0x4

    if-ne v0, v8, :cond_4

    .line 18209
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    .line 18211
    :cond_4
    and-int/lit16 v0, v3, 0x80

    if-ne v0, v9, :cond_5

    .line 18212
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    .line 18215
    :cond_5
    :try_start_5
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 18219
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 18221
    :goto_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d()V

    .line 18223
    return-void

    .line 18216
    :catch_0
    move-exception v0

    .line 18219
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_6

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 18202
    :catch_1
    move-exception v0

    .line 18203
    :goto_7
    :try_start_6
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 18208
    :catchall_1
    move-exception v0

    :goto_8
    and-int/lit8 v1, v3, 0x4

    if-ne v1, v8, :cond_6

    .line 18209
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    .line 18211
    :cond_6
    and-int/lit16 v1, v3, 0x80

    if-ne v1, v9, :cond_7

    .line 18212
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    .line 18215
    :cond_7
    :try_start_7
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 18219
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 18221
    :goto_9
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d()V

    throw v0

    .line 18204
    :catch_2
    move-exception v0

    .line 18205
    :goto_a
    :try_start_8
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 18216
    :catch_3
    move-exception v1

    .line 18219
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_9

    :catchall_2
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 18208
    :catchall_3
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_8

    .line 18204
    :catch_4
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_a

    .line 18202
    :catch_5
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_7

    :cond_8
    move v0, v3

    goto/16 :goto_5

    :cond_9
    move-object v4, v0

    goto/16 :goto_4

    :cond_a
    move-object v4, v0

    goto/16 :goto_3

    :cond_b
    move v0, v3

    goto/16 :goto_2

    .line 18122
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0xf8 -> :sswitch_9
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 18085
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$z;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 18092
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 18444
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    .line 18523
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->p:I

    .line 18093
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 18094
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 18085
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18095
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 18444
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    .line 18523
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->p:I

    .line 18095
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method public static G()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 18631
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->v()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method private J()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18434
    const/4 v0, 0x6

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->f:I

    .line 18435
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->g:I

    .line 18436
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    .line 18437
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 18438
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->j:I

    .line 18439
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 18440
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l:I

    .line 18441
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    .line 18442
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->n:I

    .line 18443
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I
    .locals 0

    .prologue
    .line 18085
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->f:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$z;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 18085
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 18085
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 18634
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->G()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I
    .locals 0

    .prologue
    .line 18085
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Ljava/util/List;
    .locals 1

    .prologue
    .line 18085
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$z;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 18085
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 18085
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I
    .locals 0

    .prologue
    .line 18085
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->j:I

    return p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Ljava/util/List;
    .locals 1

    .prologue
    .line 18085
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I
    .locals 0

    .prologue
    .line 18085
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l:I

    return p1
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 18085
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I
    .locals 0

    .prologue
    .line 18085
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->n:I

    return p1
.end method

.method static synthetic f(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I
    .locals 0

    .prologue
    .line 18085
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    return p1
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 1

    .prologue
    .line 18099
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->c:Lkotlin/reflect/jvm/internal/impl/i/e$z;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 18366
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()I
    .locals 1

    .prologue
    .line 18372
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l:I

    return v0
.end method

.method public C()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18381
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    return-object v0
.end method

.method public D()I
    .locals 1

    .prologue
    .line 18394
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public E()Z
    .locals 2

    .prologue
    .line 18420
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()I
    .locals 1

    .prologue
    .line 18430
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->n:I

    return v0
.end method

.method public H()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 18632
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->G()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public I()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 18636
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 18305
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 18488
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i()I

    .line 18490
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v3

    .line 18492
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 18493
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 18495
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 18496
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->g:I

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    :cond_1
    move v1, v2

    .line 18498
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 18499
    const/4 v4, 0x3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 18498
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 18501
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 18502
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 18504
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_4

    .line 18505
    const/4 v0, 0x5

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->j:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 18507
    :cond_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 18508
    const/4 v0, 0x6

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 18510
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 18511
    const/4 v0, 0x7

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 18513
    :cond_6
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 18514
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v6, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 18513
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 18516
    :cond_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 18517
    const/16 v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->n:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 18519
    :cond_8
    const/16 v0, 0xc8

    invoke-virtual {v3, v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 18520
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 18521
    return-void
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$a;
    .locals 1

    .prologue
    .line 18400
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18236
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 18085
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->n()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 18525
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->p:I

    .line 18526
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 18568
    :goto_0
    return v0

    .line 18529
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 18530
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->f:I

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 18533
    :goto_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 18534
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->g:I

    invoke-static {v4, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v0

    .line 18537
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 18538
    const/4 v4, 0x3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 18537
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 18541
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 18542
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 18545
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_4

    .line 18546
    const/4 v0, 0x5

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->j:I

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v3, v0

    .line 18549
    :cond_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_5

    .line 18550
    const/4 v0, 0x6

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 18553
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_6

    .line 18554
    const/4 v0, 0x7

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l:I

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v3, v0

    .line 18557
    :cond_6
    :goto_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 18558
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v6, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 18557
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 18561
    :cond_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 18562
    const/16 v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->n:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v3, v0

    .line 18565
    :cond_8
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h()I

    move-result v0

    add-int/2addr v0, v3

    .line 18566
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 18567
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->p:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 18085
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->H()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 18085
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->I()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 18446
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    .line 18447
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 18483
    :cond_0
    :goto_0
    return v1

    .line 18448
    :cond_1
    if-eqz v0, :cond_0

    .line 18450
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->q()Z

    move-result v0

    if-nez v0, :cond_2

    .line 18451
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    goto :goto_0

    :cond_2
    move v0, v1

    .line 18454
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->t()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 18455
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l()Z

    move-result v3

    if-nez v3, :cond_3

    .line 18456
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    goto :goto_0

    .line 18454
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 18460
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 18461
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_5

    .line 18462
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    goto :goto_0

    .line 18466
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 18467
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->z()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_6

    .line 18468
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    goto :goto_0

    :cond_6
    move v0, v1

    .line 18472
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->D()I

    move-result v3

    if-ge v0, v3, :cond_8

    .line 18473
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->l()Z

    move-result v3

    if-nez v3, :cond_7

    .line 18474
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    goto :goto_0

    .line 18472
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 18478
    :cond_8
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->f()Z

    move-result v0

    if-nez v0, :cond_9

    .line 18479
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    goto :goto_0

    .line 18482
    :cond_9
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o:B

    move v1, v2

    .line 18483
    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 1

    .prologue
    .line 18103
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->c:Lkotlin/reflect/jvm/internal/impl/i/e$z;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 18251
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 18262
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->f:I

    return v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 18271
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 18277
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->g:I

    return v0
.end method

.method public s()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18286
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    return-object v0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 18299
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 18321
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 18327
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 18336
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 18575
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 18342
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->j:I

    return v0
.end method

.method public y()Z
    .locals 2

    .prologue
    .line 18351
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 18357
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method
