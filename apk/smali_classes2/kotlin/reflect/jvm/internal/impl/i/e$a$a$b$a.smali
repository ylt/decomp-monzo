.class public final Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$a$a$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$c;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field private c:J

.field private d:F

.field private e:D

.field private f:I

.field private g:I

.field private h:I

.field private i:Lkotlin/reflect/jvm/internal/impl/i/e$a;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2874
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 3044
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 3311
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 3371
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    .line 2875
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->q()V

    .line 2876
    return-void
.end method

.method static synthetic p()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 2867
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 0

    .prologue
    .line 2879
    return-void
.end method

.method private static r()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 2881
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;-><init>()V

    return-object v0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 3374
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_0

    .line 3375
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    .line 3376
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3378
    :cond_0
    return-void
.end method


# virtual methods
.method public a(D)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 3184
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3185
    iput-wide p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->e:D

    .line 3187
    return-object p0
.end method

.method public a(F)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 3152
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3153
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->d:F

    .line 3155
    return-object p0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 3216
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3217
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->f:I

    .line 3219
    return-object p0
.end method

.method public a(J)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 3120
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3121
    iput-wide p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->c:J

    .line 3123
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 3079
    if-nez p1, :cond_0

    .line 3080
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3082
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3083
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 3085
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 2

    .prologue
    .line 2969
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 3006
    :goto_0
    return-object p0

    .line 2970
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2971
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2973
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2974
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(J)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2976
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2977
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->q()F

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(F)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2979
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2980
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->s()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(D)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2982
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2983
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->u()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2985
    :cond_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->v()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2986
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->w()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2988
    :cond_6
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->x()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2989
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->y()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2991
    :cond_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->z()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2992
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->A()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2994
    :cond_8
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2995
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2996
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    .line 2997
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3004
    :cond_9
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->c(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 2999
    :cond_a
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->s()V

    .line 3000
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 2

    .prologue
    .line 3350
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 3352
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$c;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 3358
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3359
    return-object p0

    .line 3355
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 2867
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2867
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 2867
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 3260
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3261
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->g:I

    .line 3263
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 2867
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2867
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 3296
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 3297
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->h:I

    .line 3299
    return-object p0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3029
    const/4 v2, 0x0

    .line 3031
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3036
    if-eqz v0, :cond_0

    .line 3037
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 3040
    :cond_0
    return-object p0

    .line 3032
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 3033
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3034
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3036
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 3037
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    :cond_1
    throw v0

    .line 3036
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 2867
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2867
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 1

    .prologue
    .line 3396
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2867
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 2

    .prologue
    .line 2908
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 1

    .prologue
    .line 2912
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2867
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2867
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 2

    .prologue
    .line 2916
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    .line 2917
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2918
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2920
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2924
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 2925
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 2926
    const/4 v1, 0x0

    .line 2927
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    .line 2930
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2931
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2932
    or-int/lit8 v0, v0, 0x2

    .line 2934
    :cond_0
    iget-wide v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->c:J

    invoke-static {v2, v4, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;J)J

    .line 2935
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2936
    or-int/lit8 v0, v0, 0x4

    .line 2938
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->d:F

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;F)F

    .line 2939
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2940
    or-int/lit8 v0, v0, 0x8

    .line 2942
    :cond_2
    iget-wide v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->e:D

    invoke-static {v2, v4, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;D)D

    .line 2943
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2944
    or-int/lit8 v0, v0, 0x10

    .line 2946
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;I)I

    .line 2947
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 2948
    or-int/lit8 v0, v0, 0x20

    .line 2950
    :cond_4
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->g:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;I)I

    .line 2951
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 2952
    or-int/lit8 v0, v0, 0x40

    .line 2954
    :cond_5
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->h:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->c(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;I)I

    .line 2955
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 2956
    or-int/lit16 v0, v0, 0x80

    .line 2958
    :cond_6
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 2959
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    .line 2960
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    .line 2961
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    .line 2963
    :cond_7
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Ljava/util/List;)Ljava/util/List;

    .line 2964
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;I)I

    .line 2965
    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3010
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3011
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3022
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 3016
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->o()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 3017
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->d(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3016
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3022
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 3316
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$a;
    .locals 1

    .prologue
    .line 3322
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 3390
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
