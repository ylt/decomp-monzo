.class public final Lkotlin/reflect/jvm/internal/impl/i/b/s;
.super Ljava/lang/Object;
.source "findClassInModule.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "classId.packageFqName"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v0

    .line 27
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->b()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->g()Ljava/util/List;

    move-result-object v3

    .line 28
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->c()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v4

    invoke-static {v3}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "segments.first()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/d;->r:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v4, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_3

    .line 30
    const/4 v1, 0x1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 31
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v4

    const-string v1, "name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/d;->r:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v4, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_1

    move-object v0, v2

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_3

    move-object v1, v0

    .line 30
    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 33
    :cond_3
    return-object v2
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b/aa;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notFoundClasses"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/s;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_0

    .line 45
    :goto_0
    return-object v0

    .line 43
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/s$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/s$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p1, v0}, Lkotlin/g/h;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/s$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/s$b;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/g/h;->e(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    invoke-static {v0}, Lkotlin/g/h;->d(Lkotlin/g/g;)Ljava/util/List;

    move-result-object v0

    .line 45
    invoke-virtual {p2, p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/ap;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "classId.packageFqName"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v0

    .line 53
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->b()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->g()Ljava/util/List;

    move-result-object v3

    .line 54
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v4, v1, -0x1

    .line 55
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->c()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v5

    invoke-static {v3}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "segments.first()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/d;->r:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v5, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 56
    if-nez v4, :cond_1

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    .line 63
    :goto_0
    return-object v0

    .line 58
    :cond_1
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_2

    move-object v0, v2

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_4

    .line 59
    const/4 v1, 0x1

    invoke-interface {v3, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 60
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v6

    const-string v1, "name"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/d;->r:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v6, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_3

    move-object v0, v2

    :cond_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_5

    move-object v1, v0

    .line 59
    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 58
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 60
    goto :goto_0

    .line 62
    :cond_6
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 63
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->g()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v3

    const-string v1, "lastName"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/c/a/d;->r:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v3, v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    if-nez v1, :cond_7

    move-object v0, v2

    :cond_7
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    goto :goto_0
.end method
