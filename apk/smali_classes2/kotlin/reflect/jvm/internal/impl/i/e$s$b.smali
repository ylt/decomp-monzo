.class public final enum Lkotlin/reflect/jvm/internal/impl/i/e$s$b;
.super Ljava/lang/Enum;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$s$b;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

.field private static d:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$s$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic f:[Lkotlin/reflect/jvm/internal/impl/i/e$s$b;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20008
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    const-string v1, "WARNING"

    invoke-direct {v0, v1, v2, v2, v2}, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    .line 20012
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3, v3, v3}, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    .line 20016
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v4, v4, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    .line 20003
    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    aput-object v1, v0, v4

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->f:[Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    .line 20049
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$b$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 20058
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20059
    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->e:I

    .line 20060
    return-void
.end method

.method public static a(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$b;
    .locals 1

    .prologue
    .line 20036
    packed-switch p0, :pswitch_data_0

    .line 20040
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 20037
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    goto :goto_0

    .line 20038
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    goto :goto_0

    .line 20039
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    goto :goto_0

    .line 20036
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/e$s$b;
    .locals 1

    .prologue
    .line 20003
    const-class v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/i/e$s$b;
    .locals 1

    .prologue
    .line 20003
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->f:[Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 20033
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->e:I

    return v0
.end method
