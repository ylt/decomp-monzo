.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$g$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$c;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 949
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 1090
    const/4 v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->b:I

    .line 1186
    const-string v0, ""

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->d:Ljava/lang/Object;

    .line 1286
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;->a:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    .line 1337
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    .line 1445
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    .line 950
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->n()V

    .line 951
    return-void
.end method

.method static synthetic m()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 942
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 954
    return-void
.end method

.method private static o()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 956
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;-><init>()V

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 1339
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 1340
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    .line 1341
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1343
    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1447
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 1448
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    .line 1449
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1451
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 1119
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1120
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->b:I

    .line 1122
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 1315
    if-nez p1, :cond_0

    .line 1316
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1318
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1319
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    .line 1321
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 2

    .prologue
    .line 1027
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1064
    :goto_0
    return-object p0

    .line 1028
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1029
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->m()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    .line 1031
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1032
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->o()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    .line 1034
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1035
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1036
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->d:Ljava/lang/Object;

    .line 1039
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1040
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->t()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    .line 1042
    :cond_4
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1043
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1044
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    .line 1045
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1052
    :cond_5
    :goto_1
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1053
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1054
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    .line 1055
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1062
    :cond_6
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 1047
    :cond_7
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->p()V

    .line 1048
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1057
    :cond_8
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->q()V

    .line 1058
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 942
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 942
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 1167
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1168
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->c:I

    .line 1170
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 942
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1075
    const/4 v2, 0x0

    .line 1077
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1082
    if-eqz v0, :cond_0

    .line 1083
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    .line 1086
    :cond_0
    return-object p0

    .line 1078
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1079
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1080
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1082
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 1083
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    :cond_1
    throw v0

    .line 1082
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 942
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 2

    .prologue
    .line 977
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
    .locals 1

    .prologue
    .line 981
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->j()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
    .locals 2

    .prologue
    .line 985
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v0

    .line 986
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 987
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 989
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 993
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V

    .line 994
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 995
    const/4 v1, 0x0

    .line 996
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 999
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;I)I

    .line 1000
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1001
    or-int/lit8 v0, v0, 0x2

    .line 1003
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;I)I

    .line 1004
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1005
    or-int/lit8 v0, v0, 0x4

    .line 1007
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->d:Ljava/lang/Object;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 1009
    or-int/lit8 v0, v0, 0x8

    .line 1011
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    .line 1012
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 1013
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    .line 1014
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1016
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->f:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;Ljava/util/List;)Ljava/util/List;

    .line 1017
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 1018
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    .line 1019
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a:I

    .line 1021
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->g:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;Ljava/util/List;)Ljava/util/List;

    .line 1022
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;I)I

    .line 1023
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1068
    const/4 v0, 0x1

    return v0
.end method
