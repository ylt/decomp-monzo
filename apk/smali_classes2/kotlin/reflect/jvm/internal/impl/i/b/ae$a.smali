.class public final Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;
.super Lkotlin/reflect/jvm/internal/impl/i/b/ae;
.source "ProtoContainer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/e/a;

.field private final b:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

.field private final c:Z

.field private final d:Lkotlin/reflect/jvm/internal/impl/i/e$c;

.field private final e:Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/e$c;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)V
    .locals 2

    .prologue
    const-string v0, "classProto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p2, p3, p4, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/d/b/i;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->e:Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->r()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "nameResolver.getClassId(classProto.fqName)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->a:Lkotlin/reflect/jvm/internal/impl/e/a;

    .line 39
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->e:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 40
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->f:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "Flags.IS_INNER.get(classProto.flags)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->c:Z

    return-void

    .line 39
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    goto :goto_0
.end method


# virtual methods
.method public a()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->a:Lkotlin/reflect/jvm/internal/impl/e/a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v1, "classId.asSingleFqName()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->a:Lkotlin/reflect/jvm/internal/impl/e/a;

    return-object v0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/i/e$c$b;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->c:Z

    return v0
.end method

.method public final h()Lkotlin/reflect/jvm/internal/impl/i/e$c;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    return-object v0
.end method

.method public final i()Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->e:Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    return-object v0
.end method
