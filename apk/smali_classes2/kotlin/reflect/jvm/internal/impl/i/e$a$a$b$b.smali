.class public final enum Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;
.super Ljava/lang/Enum;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum h:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum i:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum j:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum k:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum l:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field public static final enum m:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field private static n:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic p:[Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;


# instance fields
.field private final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2361
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "BYTE"

    invoke-direct {v0, v1, v5, v5, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2365
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "CHAR"

    invoke-direct {v0, v1, v6, v6, v6}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2369
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "SHORT"

    invoke-direct {v0, v1, v7, v7, v7}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2373
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "INT"

    invoke-direct {v0, v1, v8, v8, v8}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2377
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v9, v9, v9}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->e:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2381
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "FLOAT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2385
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "DOUBLE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2389
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "BOOLEAN"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2393
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "STRING"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2397
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "CLASS"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->j:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2401
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "ENUM"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->k:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2405
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "ANNOTATION"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->l:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2409
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    const-string v1, "ARRAY"

    const/16 v2, 0xc

    const/16 v3, 0xc

    const/16 v4, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2356
    const/16 v0, 0xd

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v1, v0, v7

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v1, v0, v8

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->e:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->j:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->k:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->l:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    aput-object v2, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->p:[Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2492
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->n:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2501
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2502
    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->o:I

    .line 2503
    return-void
.end method

.method public static a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;
    .locals 1

    .prologue
    .line 2469
    packed-switch p0, :pswitch_data_0

    .line 2483
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2470
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2471
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2472
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2473
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2474
    :pswitch_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->e:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2475
    :pswitch_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2476
    :pswitch_6
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2477
    :pswitch_7
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2478
    :pswitch_8
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->i:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2479
    :pswitch_9
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->j:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2480
    :pswitch_a
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->k:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2481
    :pswitch_b
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->l:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2482
    :pswitch_c
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    goto :goto_0

    .line 2469
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;
    .locals 1

    .prologue
    .line 2356
    const-class v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;
    .locals 1

    .prologue
    .line 2356
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->p:[Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2466
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->o:I

    return v0
.end method
