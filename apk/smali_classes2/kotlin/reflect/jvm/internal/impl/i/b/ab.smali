.class public final Lkotlin/reflect/jvm/internal/impl/i/b/ab;
.super Ljava/lang/Object;
.source "NotFoundClasses.kt"


# direct methods
.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/b/h;I)Ljava/util/List;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ab;->b(Lkotlin/reflect/jvm/internal/impl/b/h;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1
    invoke-static {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ab;->b(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/b/h;I)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/h;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    const/4 v1, 0x1

    new-instance v0, Lkotlin/f/c;

    invoke-direct {v0, v1, p1}, Lkotlin/f/c;-><init>(II)V

    check-cast v0, Ljava/lang/Iterable;

    .line 192
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    move-object v6, v1

    check-cast v6, Ljava/util/Collection;

    .line 193
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v7

    check-cast v0, Lkotlin/a/aa;

    invoke-virtual {v0}, Lkotlin/a/aa;->b()I

    move-result v5

    move-object v0, p0

    .line 178
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "T"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/b/af;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/e/f;I)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v0

    .line 180
    invoke-interface {v6, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 195
    :cond_0
    check-cast v6, Ljava/util/List;

    .line 181
    return-object v6
.end method

.method private static final b(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ah;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/ab$b;

    invoke-direct {v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ab$b;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/ah;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p1, v0}, Lkotlin/g/h;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/ab$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ab$c;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/g/h;->e(Lkotlin/g/g;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    invoke-static {v0}, Lkotlin/g/h;->e(Lkotlin/g/g;)Ljava/util/List;

    move-result-object v1

    .line 185
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/ab$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ab$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {p0, v0}, Lkotlin/g/h;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Lkotlin/g/g;

    move-result-object v0

    invoke-static {v0}, Lkotlin/g/h;->f(Lkotlin/g/g;)I

    move-result v0

    .line 186
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v0, :cond_0

    .line 187
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 189
    :cond_0
    return-object v1
.end method
