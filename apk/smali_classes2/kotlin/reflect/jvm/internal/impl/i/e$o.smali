.class public final Lkotlin/reflect/jvm/internal/impl/i/e$o;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "o"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$p;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$o;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private j:I

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private m:I

.field private n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

.field private o:I

.field private p:I

.field private q:I

.field private r:B

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15639
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$o$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 17044
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->c:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    .line 17045
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->c:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->P()V

    .line 17046
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v5, 0x0

    const/16 v8, 0x20

    .line 15508
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 15925
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    .line 16013
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->s:I

    .line 15509
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->P()V

    .line 15511
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v6

    .line 15513
    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v7

    move v3, v2

    .line 15518
    :goto_0
    if-nez v2, :cond_4

    .line 15519
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 15520
    sparse-switch v0, :sswitch_data_0

    .line 15525
    invoke-virtual {p0, p1, v7, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    move v2, v3

    :goto_1
    move v3, v2

    move v2, v0

    .line 15619
    goto :goto_0

    :sswitch_0
    move v0, v1

    move v2, v3

    .line 15523
    goto :goto_1

    .line 15532
    :sswitch_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    .line 15533
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->g:I

    move v0, v2

    move v2, v3

    .line 15534
    goto :goto_1

    .line 15537
    :sswitch_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    .line 15538
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->h:I

    move v0, v2

    move v2, v3

    .line 15539
    goto :goto_1

    .line 15543
    :sswitch_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_a

    .line 15544
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v4, v0

    .line 15546
    :goto_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 15547
    if-eqz v4, :cond_0

    .line 15548
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 15549
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 15551
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    move v0, v2

    move v2, v3

    .line 15552
    goto :goto_1

    .line 15555
    :sswitch_4
    and-int/lit8 v0, v3, 0x20

    if-eq v0, v8, :cond_9

    .line 15556
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 15557
    or-int/lit8 v0, v3, 0x20

    .line 15559
    :goto_3
    :try_start_1
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move v9, v2

    move v2, v0

    move v0, v9

    .line 15560
    goto :goto_1

    .line 15564
    :sswitch_5
    :try_start_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x20

    if-ne v0, v8, :cond_8

    .line 15565
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v4, v0

    .line 15567
    :goto_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 15568
    if-eqz v4, :cond_1

    .line 15569
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 15570
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 15572
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    move v0, v2

    move v2, v3

    .line 15573
    goto/16 :goto_1

    .line 15577
    :sswitch_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_7

    .line 15578
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->C()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    move-object v4, v0

    .line 15580
    :goto_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 15581
    if-eqz v4, :cond_2

    .line 15582
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    .line 15583
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 15585
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    move v0, v2

    move v2, v3

    .line 15586
    goto/16 :goto_1

    .line 15589
    :sswitch_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    .line 15590
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->o:I

    move v0, v2

    move v2, v3

    .line 15591
    goto/16 :goto_1

    .line 15594
    :sswitch_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    .line 15595
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p:I

    move v0, v2

    move v2, v3

    .line 15596
    goto/16 :goto_1

    .line 15599
    :sswitch_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    .line 15600
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->j:I

    move v0, v2

    move v2, v3

    .line 15601
    goto/16 :goto_1

    .line 15604
    :sswitch_a
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    .line 15605
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->m:I

    move v0, v2

    move v2, v3

    .line 15606
    goto/16 :goto_1

    .line 15609
    :sswitch_b
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    .line 15610
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->f:I

    move v0, v2

    move v2, v3

    .line 15611
    goto/16 :goto_1

    .line 15614
    :sswitch_c
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    .line 15615
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->q:I
    :try_end_2
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    move v0, v2

    move v2, v3

    goto/16 :goto_1

    .line 15626
    :cond_4
    and-int/lit8 v0, v3, 0x20

    if-ne v0, v8, :cond_5

    .line 15627
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    .line 15630
    :cond_5
    :try_start_3
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 15634
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 15636
    :goto_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d()V

    .line 15638
    return-void

    .line 15631
    :catch_0
    move-exception v0

    .line 15634
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_6

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 15620
    :catch_1
    move-exception v0

    .line 15621
    :goto_7
    :try_start_4
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 15626
    :catchall_1
    move-exception v0

    :goto_8
    and-int/lit8 v1, v3, 0x20

    if-ne v1, v8, :cond_6

    .line 15627
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    .line 15630
    :cond_6
    :try_start_5
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 15634
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 15636
    :goto_9
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d()V

    throw v0

    .line 15622
    :catch_2
    move-exception v0

    .line 15623
    :goto_a
    :try_start_6
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 15631
    :catch_3
    move-exception v1

    .line 15634
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_9

    :catchall_2
    move-exception v0

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 15626
    :catchall_3
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_8

    .line 15622
    :catch_4
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_a

    .line 15620
    :catch_5
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_7

    :cond_7
    move-object v4, v5

    goto/16 :goto_5

    :cond_8
    move-object v4, v5

    goto/16 :goto_4

    :cond_9
    move v0, v3

    goto/16 :goto_3

    :cond_a
    move-object v4, v5

    goto/16 :goto_2

    .line 15520
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0xf8 -> :sswitch_c
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15483
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$o;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 15490
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 15925
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    .line 16013
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->s:I

    .line 15491
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 15492
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 15483
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15493
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 15925
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    .line 16013
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->s:I

    .line 15493
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method public static M()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16133
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->w()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method private P()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15912
    const/16 v0, 0x206

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->f:I

    .line 15913
    const/16 v0, 0x806

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->g:I

    .line 15914
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->h:I

    .line 15915
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 15916
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->j:I

    .line 15917
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    .line 15918
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 15919
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->m:I

    .line 15920
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 15921
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->o:I

    .line 15922
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p:I

    .line 15923
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->q:I

    .line 15924
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->f:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 15483
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 0

    .prologue
    .line 15483
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16136
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->M()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 15483
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Ljava/util/List;
    .locals 1

    .prologue
    .line 15483
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 15483
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->h:I

    return p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 15483
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->j:I

    return p1
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->m:I

    return p1
.end method

.method static synthetic f(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->o:I

    return p1
.end method

.method static synthetic g(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p:I

    return p1
.end method

.method static synthetic h(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->q:I

    return p1
.end method

.method static synthetic i(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I
    .locals 0

    .prologue
    .line 15483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    return p1
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 1

    .prologue
    .line 15497
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->c:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 15801
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 15807
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public C()Z
    .locals 2

    .prologue
    .line 15816
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()I
    .locals 1

    .prologue
    .line 15822
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->m:I

    return v0
.end method

.method public E()Z
    .locals 2

    .prologue
    .line 15831
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 15837
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object v0
.end method

.method public G()Z
    .locals 2

    .prologue
    .line 15855
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()I
    .locals 1

    .prologue
    .line 15870
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->o:I

    return v0
.end method

.method public I()Z
    .locals 2

    .prologue
    .line 15879
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()I
    .locals 1

    .prologue
    .line 15885
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p:I

    return v0
.end method

.method public K()Z
    .locals 2

    .prologue
    .line 15898
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L()I
    .locals 1

    .prologue
    .line 15908
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->q:I

    return v0
.end method

.method public N()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16134
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->M()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public O()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16138
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 15785
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x2

    const/4 v3, 0x1

    .line 15969
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i()I

    .line 15971
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v2

    .line 15973
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_0

    .line 15974
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->g:I

    invoke-virtual {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 15976
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 15977
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->h:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 15979
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_2

    .line 15980
    const/4 v0, 0x3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 15982
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 15983
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 15982
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 15985
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 15986
    const/4 v0, 0x5

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 15988
    :cond_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 15989
    const/4 v0, 0x6

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 15991
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_6

    .line 15992
    const/4 v0, 0x7

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->o:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 15994
    :cond_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_7

    .line 15995
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p:I

    invoke-virtual {p1, v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 15997
    :cond_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 15998
    const/16 v0, 0x9

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->j:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 16000
    :cond_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 16001
    const/16 v0, 0xa

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->m:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 16003
    :cond_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 16004
    const/16 v0, 0xb

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->f:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 16006
    :cond_a
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_b

    .line 16007
    const/16 v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->q:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 16009
    :cond_b
    const/16 v0, 0xc8

    invoke-virtual {v2, v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 16010
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 16011
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15651
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 15483
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 16015
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->s:I

    .line 16016
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 16070
    :goto_0
    return v0

    .line 16019
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_c

    .line 16020
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->g:I

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 16023
    :goto_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_1

    .line 16024
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->h:I

    invoke-static {v3, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 16027
    :cond_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_2

    .line 16028
    const/4 v2, 0x3

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    move v2, v0

    .line 16031
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 16032
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v0, v2

    .line 16031
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 16035
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 16036
    const/4 v0, 0x5

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v2, v0

    .line 16039
    :cond_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 16040
    const/4 v0, 0x6

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->n:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v2, v0

    .line 16043
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_6

    .line 16044
    const/4 v0, 0x7

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->o:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 16047
    :cond_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_7

    .line 16048
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p:I

    invoke-static {v6, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 16051
    :cond_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 16052
    const/16 v0, 0x9

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->j:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 16055
    :cond_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 16056
    const/16 v0, 0xa

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->m:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 16059
    :cond_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 16060
    const/16 v0, 0xb

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->f:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 16063
    :cond_a
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_b

    .line 16064
    const/16 v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->q:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 16067
    :cond_b
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->h()I

    move-result v0

    add-int/2addr v0, v2

    .line 16068
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 16069
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->s:I

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 15483
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->N()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 15483
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->O()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 15927
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    .line 15928
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 15964
    :cond_0
    :goto_0
    return v1

    .line 15929
    :cond_1
    if-eqz v0, :cond_0

    .line 15931
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->s()Z

    move-result v0

    if-nez v0, :cond_2

    .line 15932
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    goto :goto_0

    .line 15935
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 15936
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_3

    .line 15937
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    goto :goto_0

    :cond_3
    move v0, v1

    .line 15941
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->z()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 15942
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l()Z

    move-result v3

    if-nez v3, :cond_4

    .line 15943
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    goto :goto_0

    .line 15941
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 15947
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->A()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 15948
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->B()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_6

    .line 15949
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    goto :goto_0

    .line 15953
    :cond_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->E()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 15954
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->F()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l()Z

    move-result v0

    if-nez v0, :cond_7

    .line 15955
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    goto :goto_0

    .line 15959
    :cond_7
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->f()Z

    move-result v0

    if-nez v0, :cond_8

    .line 15960
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    goto :goto_0

    .line 15963
    :cond_8
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r:B

    move v1, v2

    .line 15964
    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 1

    .prologue
    .line 15501
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->c:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 15676
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 15697
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->f:I

    return v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 15706
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 15712
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->g:I

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 15721
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 15727
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->h:I

    return v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 15736
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 15742
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 15751
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 16077
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 15757
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->j:I

    return v0
.end method

.method public y()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15766
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    return-object v0
.end method

.method public z()I
    .locals 1

    .prologue
    .line 15779
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
