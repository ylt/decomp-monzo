.class final Lkotlin/reflect/jvm/internal/impl/i/b/a/h$f;
.super Lkotlin/d/b/m;
.source "DeserializedMemberScope.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/a/h;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/d/a/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/Map",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/e/f;",
        "+",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/i/b/a/h;

.field final synthetic b:Ljava/util/Collection;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/h;Ljava/util/Collection;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/h$f;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/h;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/h$f;->b:Ljava/util/Collection;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/h$f;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/h;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/h$f;->b:Ljava/util/Collection;

    .line 240
    check-cast v0, Ljava/lang/Iterable;

    .line 241
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 242
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    .line 243
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    .line 240
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/i/b/a/h;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v5

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    .line 49
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->t()I

    move-result v0

    invoke-interface {v5, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    .line 245
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 246
    if-nez v0, :cond_0

    .line 247
    nop

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 248
    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 252
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 49
    :cond_1
    return-object v1
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h$f;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
