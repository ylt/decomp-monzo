.class public final Lkotlin/reflect/jvm/internal/impl/i/c/d;
.super Ljava/lang/Object;
.source "JvmProtoBufUtil.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/c/d$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

.field private static final b:Lkotlin/reflect/jvm/internal/impl/protobuf/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    .line 29
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/i/c/d;

    .line 30
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/f;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    move-result-object v0

    .line 31
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    .line 32
    const-string v1, "registry"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    const-string v1, "run {\n        val regist\u2026y)\n        registry\n    }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->z()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "nameResolver.getClassId(type.className)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/b;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a([B[Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/a;
    .locals 4

    .prologue
    const-string v0, "bytes"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strings"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 40
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/n;

    move-object v0, v1

    check-cast v0, Ljava/io/InputStream;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c/d;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    invoke-static {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    const-string v3, "JvmProtoBuf.StringTableT\u2026nput, EXTENSION_REGISTRY)"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/n;-><init>(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;[Ljava/lang/String;)V

    .line 41
    check-cast v1, Ljava/io/InputStream;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v1

    .line 42
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/i/a;

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/x;

    const-string v2, "classProto"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/e$c;)V

    return-object v3
.end method

.method public static final a([Ljava/lang/String;[Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/a;
    .locals 2

    .prologue
    const-string v0, "data"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strings"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/a;->a([Ljava/lang/String;)[B

    move-result-object v0

    const-string v1, "BitEncoding.decodeBytes(data)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a([B[Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/a;

    move-result-object v0

    return-object v0
.end method

.method public static final b([B[Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/d;
    .locals 4

    .prologue
    const-string v0, "bytes"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strings"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 50
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/d/b/n;

    move-object v0, v1

    check-cast v0, Ljava/io/InputStream;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c/d;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    invoke-static {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    const-string v3, "JvmProtoBuf.StringTableT\u2026nput, EXTENSION_REGISTRY)"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v0, p1}, Lkotlin/reflect/jvm/internal/impl/d/b/n;-><init>(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;[Ljava/lang/String;)V

    .line 51
    check-cast v1, Ljava/io/InputStream;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v1

    .line 52
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/i/d;

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/x;

    const-string v2, "packageProto"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/d;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/e$m;)V

    return-object v3
.end method

.method public static final b([Ljava/lang/String;[Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/d;
    .locals 2

    .prologue
    const-string v0, "data"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strings"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/a;->a([Ljava/lang/String;)[B

    move-result-object v0

    const-string v1, "BitEncoding.decodeBytes(data)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->b([B[Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 85
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->o()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<init>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_0
    return-object v5

    :cond_1
    move-object v0, v5

    .line 84
    goto :goto_0

    .line 89
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->q()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 129
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 130
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 131
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 90
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    invoke-static {v0, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-direct {v3, v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 132
    :cond_3
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "("

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, ")V"

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/16 v7, 0x38

    move-object v6, v5

    move-object v8, v5

    .line 91
    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/lang/String;
    .locals 12

    .prologue
    const/16 v4, 0xa

    const/4 v5, 0x0

    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-object v1, v0

    .line 63
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->m()I

    move-result v0

    move v9, v0

    .line 64
    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 65
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->o()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2, v9}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_0
    return-object v5

    :cond_1
    move-object v1, v5

    .line 62
    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->t()I

    move-result v0

    move v9, v0

    goto :goto_1

    .line 68
    :cond_3
    invoke-static {p1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->b(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->E()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 121
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v1, v4}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 122
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 123
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 68
    invoke-static {v1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 124
    :cond_4
    check-cast v2, Ljava/util/List;

    .line 68
    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v0, v2}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 70
    check-cast v0, Ljava/lang/Iterable;

    .line 125
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v4}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 126
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 127
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 70
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a:Lkotlin/reflect/jvm/internal/impl/i/c/d;

    invoke-direct {v3, v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 128
    :cond_5
    check-cast v1, Ljava/util/List;

    .line 71
    invoke-static {p1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 73
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "("

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, ")"

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/16 v7, 0x38

    move-object v6, v5

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/c/d$a;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    .line 104
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->m()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    move-object v3, v0

    .line 106
    :goto_0
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->m()I

    move-result v0

    move v2, v0

    .line 108
    :goto_1
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->o()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    :cond_0
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/c/d$a;

    invoke-interface {p2, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nameResolver.getString(name)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "desc"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/d$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_1
    move-object v0, v1

    .line 102
    goto :goto_2

    :cond_2
    move-object v3, v1

    .line 104
    goto :goto_0

    .line 106
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->t()I

    move-result v0

    move v2, v0

    goto :goto_1

    .line 109
    :cond_4
    invoke-static {p1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/d;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_2
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/protobuf/f;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/d;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    return-object v0
.end method
