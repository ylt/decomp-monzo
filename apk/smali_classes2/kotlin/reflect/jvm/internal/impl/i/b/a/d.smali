.class public final Lkotlin/reflect/jvm/internal/impl/i/b/a/d;
.super Lkotlin/reflect/jvm/internal/impl/b/b/f;
.source "DeserializedMemberDescriptor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/a/c;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/i/e$e;

.field private final e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

.field private final f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

.field private final g:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

.field private final h:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/i/e$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/b/al;)V
    .locals 8

    .prologue
    const-string v1, "containingDeclaration"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annotations"

    invoke-static {p3, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "kind"

    invoke-static {p5, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "proto"

    invoke-static {p6, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "nameResolver"

    invoke-static {p7, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "typeTable"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "sinceKotlinInfoTable"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    if-eqz p11, :cond_0

    move-object/from16 v7, p11

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    .line 149
    invoke-direct/range {v1 .. v7}, Lkotlin/reflect/jvm/internal/impl/b/b/f;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->d:Lkotlin/reflect/jvm/internal/impl/i/e$e;

    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-object/from16 v0, p8

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-object/from16 v0, p9

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->g:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-object/from16 v0, p10

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    return-void

    .line 136
    :cond_0
    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    goto :goto_0
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/i/e$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/b/al;ILkotlin/d/b/i;)V
    .locals 13

    .prologue
    move/from16 v0, p12

    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_0

    .line 147
    const/4 v1, 0x0

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object v12, v1

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v12}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/i/e$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    return-void

    :cond_0
    move-object/from16 v12, p11

    goto :goto_0
.end method


# virtual methods
.method public C()Z
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public I()Lkotlin/reflect/jvm/internal/impl/i/e$e;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->d:Lkotlin/reflect/jvm/internal/impl/i/e$e;

    return-object v0
.end method

.method public synthetic J()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->I()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    return-object v0
.end method

.method public K()Lkotlin/reflect/jvm/internal/impl/i/b/x;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->e:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    return-object v0
.end method

.method public L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    return-object v0
.end method

.method public M()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->g:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    return-object v0
.end method

.method public N()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/o;
    .locals 1

    .prologue
    .line 136
    invoke-virtual/range {p0 .. p6}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->c(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/i/b/a/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/o;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/f;
    .locals 1

    .prologue
    .line 136
    invoke-virtual/range {p0 .. p6}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->c(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/i/b/a/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/f;

    return-object v0
.end method

.method protected c(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/i/b/a/d;
    .locals 13

    .prologue
    const-string v1, "newOwner"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "kind"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annotations"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "source"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;

    if-nez p1, :cond_0

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v2, p1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v3, p2

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/l;

    iget-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->a:Z

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->I()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v7

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->K()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v8

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v9

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->M()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v10

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->N()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v11

    move-object/from16 v4, p5

    move-object/from16 v6, p3

    move-object/from16 v12, p6

    invoke-direct/range {v1 .. v12}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/i/e$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    return-object v1
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method
