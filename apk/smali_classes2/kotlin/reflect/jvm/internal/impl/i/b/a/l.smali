.class public final Lkotlin/reflect/jvm/internal/impl/i/b/a/l;
.super Lkotlin/reflect/jvm/internal/impl/b/b/d;
.source "DeserializedMemberDescriptor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/a/g;


# instance fields
.field private a:Lkotlin/reflect/jvm/internal/impl/k/v;

.field private b:Lkotlin/reflect/jvm/internal/impl/k/v;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lkotlin/reflect/jvm/internal/impl/k/v;

.field private final e:Lkotlin/reflect/jvm/internal/impl/i/e$z;

.field private final f:Lkotlin/reflect/jvm/internal/impl/i/b/x;

.field private final g:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

.field private final h:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

.field private final i:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)V
    .locals 6

    .prologue
    const-string v0, "containingDeclaration"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "visibility"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sinceKotlinInfoTable"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const-string v0, "SourceElement.NO_SOURCE"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 184
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/b/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ay;)V

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->e:Lkotlin/reflect/jvm/internal/impl/i/e$z;

    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->f:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->g:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    iput-object p8, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    iput-object p9, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->i:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    return-void
.end method

.method private a(Lkotlin/reflect/jvm/internal/impl/k/v;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method

.method private b(Lkotlin/reflect/jvm/internal/impl/k/v;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    return-void
.end method


# virtual methods
.method public E_()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->d:Lkotlin/reflect/jvm/internal/impl/k/v;

    if-nez v0, :cond_0

    const-string v1, "defaultTypeImpl"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public synthetic J()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->o()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    return-object v0
.end method

.method public K()Lkotlin/reflect/jvm/internal/impl/i/b/x;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->f:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    return-object v0
.end method

.method public L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->g:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/ap;
    .locals 10

    .prologue
    const-string v0, "substitutor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/k/am;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    .line 219
    :goto_0
    return-object p0

    .line 212
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    const-string v2, "containingDeclaration"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    const-string v3, "annotations"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    const-string v4, "name"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->p()Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v4

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->o()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v5

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->K()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v6

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v7

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->q()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v8

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->s()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)V

    .line 215
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->y()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->a()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {p1, v1, v3}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/al;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v3

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->n()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-virtual {p1, v1, v4}, Lkotlin/reflect/jvm/internal/impl/k/am;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/ap;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/k/al;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    .line 219
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-object p0, v0

    goto :goto_0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->a:Lkotlin/reflect/jvm/internal/impl/k/v;

    if-nez v0, :cond_0

    const-string v1, "underlyingType"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            "Lkotlin/reflect/jvm/internal/impl/k/v;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "declaredTypeParameters"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "underlyingType"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expandedType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->a(Ljava/util/List;)V

    .line 198
    invoke-direct {p0, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/v;)V

    .line 199
    invoke-direct {p0, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->b(Lkotlin/reflect/jvm/internal/impl/k/v;)V

    .line 200
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/b/ar;->a(Lkotlin/reflect/jvm/internal/impl/b/i;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->c:Ljava/util/List;

    .line 201
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->l()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->d:Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 202
    return-void
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 205
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->n()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/v;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->n()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v1

    instance-of v2, v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_1

    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method protected k()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->c:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "typeConstructorParameters"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->b:Lkotlin/reflect/jvm/internal/impl/k/v;

    if-nez v0, :cond_0

    const-string v1, "expandedType"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->e:Lkotlin/reflect/jvm/internal/impl/i/e$z;

    return-object v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    return-object v0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->i:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    return-object v0
.end method
