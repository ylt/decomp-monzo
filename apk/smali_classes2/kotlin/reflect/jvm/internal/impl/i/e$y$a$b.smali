.class public final enum Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;
.super Ljava/lang/Enum;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

.field private static e:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic g:[Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4854
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    const-string v1, "IN"

    invoke-direct {v0, v1, v2, v2, v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 4858
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    const-string v1, "OUT"

    invoke-direct {v0, v1, v3, v3, v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 4862
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    const-string v1, "INV"

    invoke-direct {v0, v1, v4, v4, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 4866
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    const-string v1, "STAR"

    invoke-direct {v0, v1, v5, v5, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 4849
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->g:[Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 4904
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 4913
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4914
    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->f:I

    .line 4915
    return-void
.end method

.method public static a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;
    .locals 1

    .prologue
    .line 4890
    packed-switch p0, :pswitch_data_0

    .line 4895
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4891
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    goto :goto_0

    .line 4892
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    goto :goto_0

    .line 4893
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    goto :goto_0

    .line 4894
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    goto :goto_0

    .line 4890
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;
    .locals 1

    .prologue
    .line 4849
    const-class v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;
    .locals 1

    .prologue
    .line 4849
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->g:[Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 4887
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->f:I

    return v0
.end method
