.class public interface abstract Lkotlin/reflect/jvm/internal/impl/i/b/c;
.super Ljava/lang/Object;
.source "AnnotationAndConstantLoader.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "C:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")TC;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/i/e$g;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;ILkotlin/reflect/jvm/internal/impl/i/e$ag;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            "I",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation
.end method

.method public abstract a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation
.end method

.method public abstract b(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            ")",
            "Ljava/util/List",
            "<TA;>;"
        }
    .end annotation
.end method
