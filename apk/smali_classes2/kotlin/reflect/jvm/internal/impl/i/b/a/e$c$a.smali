.class final Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;
.super Lkotlin/d/b/m;
.source "DeserializedClassDescriptor.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/e/f;",
        "Lkotlin/reflect/jvm/internal/impl/b/b/n;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 304
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/e/f;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/b/n;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/b/n;
    .locals 7

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;

    if-eqz v0, :cond_0

    move-object v2, v0

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/e$g;

    .line 311
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v3

    new-instance v5, Lkotlin/reflect/jvm/internal/impl/i/b/a/a;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    iget-object v4, v4, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v6

    new-instance v4, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a$a;

    invoke-direct {v4, v2, p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/e$g;Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c$a;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    move-object v2, v4

    check-cast v2, Lkotlin/d/a/a;

    invoke-direct {v5, v6, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    move-object v4, v5

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/b/n;->a(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/j/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/n;

    move-result-object v0

    .line 310
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/n;

    .line 318
    :goto_0
    return-object v0

    .line 310
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
