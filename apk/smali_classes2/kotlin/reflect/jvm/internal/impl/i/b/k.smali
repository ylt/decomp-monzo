.class public final Lkotlin/reflect/jvm/internal/impl/i/b/k;
.super Ljava/lang/Object;
.source "protoEnumMapping.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/i/b/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/k;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/k;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/i/b/k;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a:Lkotlin/reflect/jvm/internal/impl/i/b/k;

    return-void
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$ai;)Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 2

    .prologue
    .line 47
    if-nez p0, :cond_0

    .line 54
    :goto_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    .line 55
    :goto_1
    return-object v0

    .line 47
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/l;->c:[I

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 48
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->d:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_1

    .line 49
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->a:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_1

    .line 50
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->b:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_1

    .line 51
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->c:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_1

    .line 52
    :pswitch_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_1

    .line 53
    :pswitch_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->f:Lkotlin/reflect/jvm/internal/impl/b/ay;

    goto :goto_1

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$k;)Lkotlin/reflect/jvm/internal/impl/b/b$a;
    .locals 2

    .prologue
    .line 29
    if-nez p0, :cond_0

    .line 34
    :goto_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    .line 35
    :goto_1
    return-object v0

    .line 29
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/l;->a:[I

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 30
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    goto :goto_1

    .line 31
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->b:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    goto :goto_1

    .line 32
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->c:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    goto :goto_1

    .line 33
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/b$a;->d:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    goto :goto_1

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$c$b;)Lkotlin/reflect/jvm/internal/impl/b/f;
    .locals 2

    .prologue
    .line 58
    if-nez p0, :cond_0

    .line 65
    :goto_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    .line 66
    :goto_1
    return-object v0

    .line 58
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/l;->d:[I

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 59
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->a:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto :goto_1

    .line 60
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto :goto_1

    .line 61
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto :goto_1

    .line 62
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->d:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto :goto_1

    .line 63
    :pswitch_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->e:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto :goto_1

    .line 64
    :pswitch_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->f:Lkotlin/reflect/jvm/internal/impl/b/f;

    goto :goto_1

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$l;)Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 2

    .prologue
    .line 38
    if-nez p0, :cond_0

    .line 43
    :goto_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    .line 44
    :goto_1
    return-object v0

    .line 38
    :cond_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/l;->b:[I

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$l;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 39
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->a:Lkotlin/reflect/jvm/internal/impl/b/u;

    goto :goto_1

    .line 40
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->c:Lkotlin/reflect/jvm/internal/impl/b/u;

    goto :goto_1

    .line 41
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    goto :goto_1

    .line 42
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->b:Lkotlin/reflect/jvm/internal/impl/b/u;

    goto :goto_1

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;)Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 2

    .prologue
    const-string v0, "variance"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/l;->e:[I

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    .line 74
    :goto_0
    return-object v0

    .line 70
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_0

    .line 71
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_0

    .line 72
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;)Lkotlin/reflect/jvm/internal/impl/k/ap;
    .locals 3

    .prologue
    const-string v0, "variance"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/l;->f:[I

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 83
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    .line 84
    :goto_0
    return-object v0

    .line 78
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_0

    .line 79
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_0

    .line 80
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    goto :goto_0

    .line 82
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only IN, OUT and INV are supported. Actual argument: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
