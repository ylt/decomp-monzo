.class public final Lkotlin/reflect/jvm/internal/impl/i/b/e;
.super Ljava/lang/Object;
.source "AnnotationDeserializer.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

.field private final b:Lkotlin/reflect/jvm/internal/impl/b/w;

.field private final c:Lkotlin/reflect/jvm/internal/impl/i/b/aa;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/aa;)V
    .locals 2

    .prologue
    const-string v0, "module"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notFoundClasses"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->b:Lkotlin/reflect/jvm/internal/impl/b/w;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->c:Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    .line 44
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/h;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a;",
            "Ljava/util/Map",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "+",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")",
            "Lkotlin/h",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->m()I

    move-result v0

    invoke-interface {p3, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    if-eqz v0, :cond_0

    .line 67
    new-instance v1, Lkotlin/h;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    const-string v3, "parameter.type"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v3

    const-string v4, "proto.value"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a()Lkotlin/reflect/jvm/internal/impl/a/l;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->b:Lkotlin/reflect/jvm/internal/impl/b/w;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->b:Lkotlin/reflect/jvm/internal/impl/b/w;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->c:Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    invoke-static {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/s;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b/aa;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 138
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->l()Lkotlin/reflect/jvm/internal/impl/b/f;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->c:Lkotlin/reflect/jvm/internal/impl/b/f;

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->B()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->r:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    .line 140
    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Lkotlin/reflect/jvm/internal/impl/h/b/i;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    .line 144
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unresolved enum entry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 3

    .prologue
    .line 148
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/l;

    .line 149
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    move-result-object v1

    if-nez v1, :cond_0

    .line 163
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 149
    :cond_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/b/f;->b:[I

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 150
    :pswitch_0
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->A()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getByteType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    .line 165
    return-object v0

    .line 151
    :pswitch_1
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->G()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getCharType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 152
    :pswitch_2
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->B()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getShortType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 153
    :pswitch_3
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->C()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getIntType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 154
    :pswitch_4
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->D()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getLongType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 155
    :pswitch_5
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->E()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getFloatType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 156
    :pswitch_6
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->F()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getDoubleType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 157
    :pswitch_7
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->H()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getBooleanType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 158
    :pswitch_8
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->J()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "getStringType()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 159
    :pswitch_9
    const-string v1, "Arrays of class literals are not supported yet"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 160
    :pswitch_a
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->w()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "nameResolver.getClassId(value.classId)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "resolveClass(nameResolve\u2026lue.classId)).defaultType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 161
    :pswitch_b
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->A()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->m()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "nameResolver.getClassId(value.annotation.id)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "resolveClass(nameResolve\u2026notation.id)).defaultType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 162
    :pswitch_c
    const-string v1, "Array of arrays is impossible"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 149
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;
    .locals 6

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->m()I

    move-result v0

    invoke-interface {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "nameResolver.getClassId(proto.id)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v1

    .line 49
    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v2

    .line 50
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->o()I

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->n(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 51
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    .line 52
    if-eqz v0, :cond_3

    .line 53
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 172
    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-static {v2}, Lkotlin/a/ab;->a(I)I

    move-result v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lkotlin/f/d;->c(II)I

    move-result v3

    .line 173
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 174
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    .line 175
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 53
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 54
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->n()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 178
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 187
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 186
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    .line 54
    const-string v5, "it"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v2, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/h;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 186
    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 189
    :cond_2
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 54
    invoke-static {v3}, Lkotlin/a/ab;->a(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v0

    move-object v3, v0

    .line 58
    :goto_2
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/a/d;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v2, v0, v3, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    return-object v0

    :cond_3
    move-object v3, v2

    goto :goto_2
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "expectedType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    move-result-object v0

    if-nez v0, :cond_0

    .line 123
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported annotation argument type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (expected "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 75
    :cond_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/b/f;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 76
    :pswitch_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o()J

    move-result-wide v2

    long-to-int v1, v2

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(B)Lkotlin/reflect/jvm/internal/impl/h/b/d;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    .line 126
    :goto_1
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/b/f;->a()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 131
    :goto_2
    return-object v0

    .line 77
    :pswitch_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o()J

    move-result-wide v2

    long-to-int v1, v2

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(C)Lkotlin/reflect/jvm/internal/impl/h/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_1

    .line 78
    :pswitch_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o()J

    move-result-wide v2

    long-to-int v1, v2

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(S)Lkotlin/reflect/jvm/internal/impl/h/b/r;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_1

    .line 79
    :pswitch_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(I)Lkotlin/reflect/jvm/internal/impl/h/b/l;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_1

    .line 80
    :pswitch_4
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(J)Lkotlin/reflect/jvm/internal/impl/h/b/p;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_1

    .line 81
    :pswitch_5
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->q()F

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(F)Lkotlin/reflect/jvm/internal/impl/h/b/k;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_1

    .line 82
    :pswitch_6
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->s()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(D)Lkotlin/reflect/jvm/internal/impl/h/b/h;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_1

    .line 83
    :pswitch_7
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    move v0, v2

    :goto_3
    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Z)Lkotlin/reflect/jvm/internal/impl/h/b/c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto :goto_1

    :cond_1
    move v0, v3

    goto :goto_3

    .line 85
    :pswitch_8
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->u()I

    move-result v1

    invoke-interface {p3, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "nameResolver.getString(value.stringValue)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/s;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto/16 :goto_1

    .line 89
    :pswitch_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Class literal annotation arguments are not supported yet ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->w()I

    move-result v1

    invoke-interface {p3, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 92
    :pswitch_a
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->w()I

    move-result v0

    invoke-interface {p3, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "nameResolver.getClassId(value.classId)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->y()I

    move-result v1

    invoke-interface {p3, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v2, "nameResolver.getName(value.enumValueId)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    goto/16 :goto_1

    .line 95
    :pswitch_b
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/b/a;

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->A()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v1

    const-string v2, "value.annotation"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/a;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto/16 :goto_1

    .line 98
    :pswitch_c
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/a/l;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v4, v2

    .line 99
    :goto_4
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->B()Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 102
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_5
    if-eqz v2, :cond_6

    .line 103
    invoke-static {v1}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "arrayElements.first()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-direct {p0, v0, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    .line 104
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v3

    move-object v0, v2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    :goto_6
    move-object v2, v0

    .line 114
    :goto_7
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v3

    if-eqz v4, :cond_8

    move-object v0, p1

    :goto_8
    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    .line 116
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    check-cast v1, Ljava/lang/Iterable;

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v1, v5}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 191
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 192
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 118
    const-string v6, "expectedElementType"

    invoke-static {v3, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "it"

    invoke-static {v1, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v3, v1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_3
    move v4, v3

    .line 98
    goto :goto_4

    :cond_4
    move v2, v3

    .line 102
    goto :goto_5

    .line 105
    :cond_5
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0, v3, v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v2, "builtIns.getArrayType(Va\u2026RIANT, actualElementType)"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_6

    .line 111
    :cond_6
    if-eqz v4, :cond_7

    move-object v2, p1

    goto :goto_7

    :cond_7
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/k/ap;->a:Lkotlin/reflect/jvm/internal/impl/k/ap;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->x()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v2, v3, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v2, "builtIns.getArrayType(Va\u2026T, builtIns.getAnyType())"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v2, v0

    goto :goto_7

    :cond_8
    move-object v0, v2

    .line 114
    goto :goto_8

    .line 193
    :cond_9
    check-cast v0, Ljava/util/List;

    .line 116
    invoke-virtual {v4, v0, v2}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/h/b/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto/16 :goto_1

    .line 131
    :cond_a
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a:Lkotlin/reflect/jvm/internal/impl/h/b/g;

    const-string v1, "Unexpected argument value"

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/j;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/f;

    goto/16 :goto_2

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
