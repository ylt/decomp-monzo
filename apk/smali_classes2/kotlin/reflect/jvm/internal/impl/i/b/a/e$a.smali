.class final Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;
.super Lkotlin/reflect/jvm/internal/impl/i/b/a/h;
.source "DeserializedClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

.field private final d:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 213
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->C()Ljava/util/List;

    move-result-object v2

    const-string v0, "classProto.functionList"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->E()Ljava/util/List;

    move-result-object v3

    const-string v0, "classProto.propertyList"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/util/Collection;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->G()Ljava/util/List;

    move-result-object v4

    const-string v0, "classProto.typeAliasList"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/util/Collection;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->z()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v6

    .line 357
    new-instance v5, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v0, v7}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 358
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 359
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v8

    move-object v0, v6

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/x;

    .line 215
    invoke-interface {v0, v8}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 360
    :cond_0
    check-cast v5, Ljava/util/List;

    check-cast v5, Ljava/util/List;

    .line 215
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a$a;

    invoke-direct {v0, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a$a;-><init>(Ljava/util/List;)V

    check-cast v0, Lkotlin/d/a/a;

    move-object v5, v0

    check-cast v5, Lkotlin/d/a/a;

    move-object v0, p0

    .line 213
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/d/a/a;)V

    .line 219
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a$b;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lkotlin/reflect/jvm/internal/impl/b/b;",
            ">(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<+TD;>;",
            "Ljava/util/Collection",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 260
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 261
    check-cast v0, Ljava/util/Collection;

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a$d;

    invoke-direct {v2, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a$d;-><init>(Ljava/util/Collection;)V

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/h/i;

    invoke-static {p1, p2, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/j;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/h/i;)V

    .line 273
    return-void
.end method

.method private final d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)V
    .locals 2

    .prologue
    .line 300
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->j()Lkotlin/reflect/jvm/internal/impl/c/a/c;

    move-result-object v1

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    invoke-static {v1, p2, v0, p1}, Lkotlin/reflect/jvm/internal/impl/c/a;->a(Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/c/a/b;Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    .line 301
    return-void
.end method

.method private final g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)V

    .line 234
    invoke-super {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/h/e/d;Lkotlin/d/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/h/e/d;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "kindFilter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->d:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->c(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/Collection;Lkotlin/d/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameFilter"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a()Ljava/util/Collection;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    invoke-interface {p1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 297
    return-void

    .line 296
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_1
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "functions"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 239
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 240
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->l:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v3, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    move-object v0, p2

    .line 243
    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a$c;

    invoke-direct {v1, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a$c;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Lkotlin/d/a/b;)Z

    .line 247
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->n()Lkotlin/reflect/jvm/internal/impl/i/b/a;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-interface {v0, p1, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-object v0, v2

    .line 248
    check-cast v0, Ljava/util/Collection;

    invoke-direct {p0, p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 249
    return-void
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ak;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)V

    .line 229
    invoke-super {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;->b(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/ag;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptors"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 253
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->G_()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 254
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v3

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->l:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v3, p1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 256
    check-cast v0, Ljava/util/Collection;

    invoke-direct {p0, p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->a(Lkotlin/reflect/jvm/internal/impl/e/f;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 257
    return-void
.end method

.method protected c()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 276
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->j()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 347
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 348
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 277
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->A_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 349
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 278
    check-cast v0, Ljava/util/LinkedHashSet;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->n()Lkotlin/reflect/jvm/internal/impl/i/b/a;

    move-result-object v2

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-interface {v2, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/a;->c(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    check-cast v1, Ljava/util/Set;

    return-object v1
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->d(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)V

    .line 289
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->b(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$c;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    .line 290
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    goto :goto_0
.end method

.method protected d()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->j()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 352
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 353
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 283
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->B_()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 354
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 356
    :cond_0
    check-cast v1, Ljava/util/Set;

    return-object v1
.end method
