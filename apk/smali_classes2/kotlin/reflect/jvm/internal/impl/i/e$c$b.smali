.class public final enum Lkotlin/reflect/jvm/internal/impl/i/e$c$b;
.super Ljava/lang/Enum;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$c$b;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

.field public static final enum g:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

.field private static h:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic j:[Lkotlin/reflect/jvm/internal/impl/i/e$c$b;


# instance fields
.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 8437
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    const-string v1, "CLASS"

    invoke-direct {v0, v1, v5, v5, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 8441
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    const-string v1, "INTERFACE"

    invoke-direct {v0, v1, v6, v6, v6}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 8445
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    const-string v1, "ENUM_CLASS"

    invoke-direct {v0, v1, v7, v7, v7}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 8449
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    const-string v1, "ENUM_ENTRY"

    invoke-direct {v0, v1, v8, v8, v8}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 8453
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    const-string v1, "ANNOTATION_CLASS"

    invoke-direct {v0, v1, v9, v9, v9}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->e:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 8457
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    const-string v1, "OBJECT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 8461
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    const-string v1, "COMPANION_OBJECT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->g:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 8428
    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    aput-object v1, v0, v7

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    aput-object v1, v0, v8

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->e:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->g:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    aput-object v2, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->j:[Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    .line 8518
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$b$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->h:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 8527
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 8528
    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->i:I

    .line 8529
    return-void
.end method

.method public static a(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$b;
    .locals 1

    .prologue
    .line 8501
    packed-switch p0, :pswitch_data_0

    .line 8509
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 8502
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    goto :goto_0

    .line 8503
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    goto :goto_0

    .line 8504
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    goto :goto_0

    .line 8505
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    goto :goto_0

    .line 8506
    :pswitch_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->e:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    goto :goto_0

    .line 8507
    :pswitch_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    goto :goto_0

    .line 8508
    :pswitch_6
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->g:Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    goto :goto_0

    .line 8501
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/e$c$b;
    .locals 1

    .prologue
    .line 8428
    const-class v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/i/e$c$b;
    .locals 1

    .prologue
    .line 8428
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->j:[Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/i/e$c$b;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 8498
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$b;->i:I

    return v0
.end method
