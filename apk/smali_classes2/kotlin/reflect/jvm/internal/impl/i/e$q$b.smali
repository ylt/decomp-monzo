.class public final Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$q$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$q$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$q$b;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 970
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 1457
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    .line 1458
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->u()V

    .line 1459
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 907
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 1109
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->i:B

    .line 1138
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->j:I

    .line 908
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->u()V

    .line 910
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v2

    .line 912
    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v3

    .line 916
    const/4 v0, 0x0

    .line 917
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 918
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v4

    .line 919
    sparse-switch v4, :sswitch_data_0

    .line 924
    invoke-virtual {p0, p1, v3, p2, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v1

    .line 926
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 922
    goto :goto_0

    .line 931
    :sswitch_1
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    .line 932
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->f:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 954
    :catch_0
    move-exception v0

    .line 955
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 960
    :catchall_0
    move-exception v0

    .line 961
    :try_start_2
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 965
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 967
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d()V

    throw v0

    .line 936
    :sswitch_2
    :try_start_3
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    .line 937
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->g:I
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 956
    :catch_1
    move-exception v0

    .line 957
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 941
    :sswitch_3
    :try_start_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->n()I

    move-result v5

    .line 942
    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    move-result-object v6

    .line 943
    if-nez v6, :cond_1

    .line 944
    invoke-virtual {v3, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 945
    invoke-virtual {v3, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    goto :goto_0

    .line 947
    :cond_1
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    .line 948
    iput-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;
    :try_end_5
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 961
    :cond_2
    :try_start_6
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 965
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 967
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d()V

    .line 969
    return-void

    .line 962
    :catch_2
    move-exception v0

    .line 965
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 962
    :catch_3
    move-exception v1

    .line 965
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 919
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 883
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 889
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 1109
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->i:B

    .line 1138
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->j:I

    .line 890
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 891
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 883
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 892
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 1109
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->i:B

    .line 1138
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->j:I

    .line 892
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;I)I
    .locals 0

    .prologue
    .line 883
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->f:I

    return p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1224
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->r()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;
    .locals 0

    .prologue
    .line 883
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;I)I
    .locals 0

    .prologue
    .line 883
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;I)I
    .locals 0

    .prologue
    .line 883
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    return p1
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    .locals 1

    .prologue
    .line 896
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    return-object v0
.end method

.method public static r()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1221
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 1105
    const/4 v0, -0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->f:I

    .line 1106
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->g:I

    .line 1107
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    .line 1108
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1125
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->i()I

    .line 1126
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1127
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 1129
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1130
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->g:I

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 1132
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1133
    const/4 v0, 0x3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(II)V

    .line 1135
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 1136
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$q$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 883
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->g()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    .locals 1

    .prologue
    .line 900
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1057
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1140
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->j:I

    .line 1141
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1158
    :goto_0
    return v0

    .line 1143
    :cond_0
    const/4 v0, 0x0

    .line 1144
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 1145
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1148
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 1149
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->g:I

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1152
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 1153
    const/4 v1, 0x3

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;->a()I

    move-result v2

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1156
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 1157
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->j:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 883
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->s()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 883
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->t()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1111
    iget-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->i:B

    .line 1112
    if-ne v2, v0, :cond_0

    .line 1120
    :goto_0
    return v0

    .line 1113
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 1115
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->n()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1116
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->i:B

    move v0, v1

    .line 1117
    goto :goto_0

    .line 1119
    :cond_2
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->i:B

    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 1063
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->f:I

    return v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 1076
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 1086
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->g:I

    return v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 1095
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;
    .locals 1

    .prologue
    .line 1101
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->h:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    return-object v0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1222
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->r()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public t()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1226
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1165
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
