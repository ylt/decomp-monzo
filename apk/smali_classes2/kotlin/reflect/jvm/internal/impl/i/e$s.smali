.class public final Lkotlin/reflect/jvm/internal/impl/i/e$s;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "s"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$s$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$s$b;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$s;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$s;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

.field private i:I

.field private j:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19985
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 20706
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->c:Lkotlin/reflect/jvm/internal/impl/i/e$s;

    .line 20707
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->c:Lkotlin/reflect/jvm/internal/impl/i/e$s;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->y()V

    .line 20708
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 19912
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 20194
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->k:B

    .line 20225
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->l:I

    .line 19913
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->y()V

    .line 19915
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v2

    .line 19917
    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v3

    .line 19921
    const/4 v0, 0x0

    .line 19922
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 19923
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v4

    .line 19924
    sparse-switch v4, :sswitch_data_0

    .line 19929
    invoke-virtual {p0, p1, v3, p2, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v1

    .line 19931
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 19927
    goto :goto_0

    .line 19936
    :sswitch_1
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    .line 19937
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->f:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 19969
    :catch_0
    move-exception v0

    .line 19970
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 19975
    :catchall_0
    move-exception v0

    .line 19976
    :try_start_2
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 19980
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 19982
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d()V

    throw v0

    .line 19941
    :sswitch_2
    :try_start_3
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    .line 19942
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->g:I
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 19971
    :catch_1
    move-exception v0

    .line 19972
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 19946
    :sswitch_3
    :try_start_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->n()I

    move-result v5

    .line 19947
    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    move-result-object v6

    .line 19948
    if-nez v6, :cond_1

    .line 19949
    invoke-virtual {v3, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 19950
    invoke-virtual {v3, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    goto :goto_0

    .line 19952
    :cond_1
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    .line 19953
    iput-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->h:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    goto :goto_0

    .line 19958
    :sswitch_4
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    .line 19959
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->i:I

    goto :goto_0

    .line 19963
    :sswitch_5
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    .line 19964
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->j:I
    :try_end_5
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 19976
    :cond_2
    :try_start_6
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 19980
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 19982
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d()V

    .line 19984
    return-void

    .line 19977
    :catch_2
    move-exception v0

    .line 19980
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 19977
    :catch_3
    move-exception v1

    .line 19980
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 19924
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 19888
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$s;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19894
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 20194
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->k:B

    .line 20225
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->l:I

    .line 19895
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 19896
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 19888
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19897
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 20194
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->k:B

    .line 20225
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->l:I

    .line 19897
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I
    .locals 0

    .prologue
    .line 19888
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->f:I

    return p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20319
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->v()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$s;Lkotlin/reflect/jvm/internal/impl/i/e$s$b;)Lkotlin/reflect/jvm/internal/impl/i/e$s$b;
    .locals 0

    .prologue
    .line 19888
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->h:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I
    .locals 0

    .prologue
    .line 19888
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 19888
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I
    .locals 0

    .prologue
    .line 19888
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->i:I

    return p1
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I
    .locals 0

    .prologue
    .line 19888
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->j:I

    return p1
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I
    .locals 0

    .prologue
    .line 19888
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    return p1
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$s;
    .locals 1

    .prologue
    .line 19901
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->c:Lkotlin/reflect/jvm/internal/impl/i/e$s;

    return-object v0
.end method

.method public static v()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20316
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method private y()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20188
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->f:I

    .line 20189
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->g:I

    .line 20190
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->h:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    .line 20191
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->i:I

    .line 20192
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->j:I

    .line 20193
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 20206
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->i()I

    .line 20207
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 20208
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 20210
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 20211
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->g:I

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 20213
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 20214
    const/4 v0, 0x3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->h:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(II)V

    .line 20216
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 20217
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->i:I

    invoke-virtual {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 20219
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 20220
    const/4 v0, 0x5

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->j:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 20222
    :cond_4
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 20223
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19997
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 19888
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->g()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$s;
    .locals 1

    .prologue
    .line 19905
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->c:Lkotlin/reflect/jvm/internal/impl/i/e$s;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 20078
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 20227
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->l:I

    .line 20228
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 20253
    :goto_0
    return v0

    .line 20230
    :cond_0
    const/4 v0, 0x0

    .line 20231
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 20232
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20235
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 20236
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->g:I

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20239
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 20240
    const/4 v1, 0x3

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->h:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->a()I

    move-result v2

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20243
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 20244
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->i:I

    invoke-static {v4, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20247
    :cond_4
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 20248
    const/4 v1, 0x5

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->j:I

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20251
    :cond_5
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 20252
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->l:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 19888
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->w()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 19888
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->x()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 20196
    iget-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->k:B

    .line 20197
    if-ne v1, v0, :cond_0

    .line 20201
    :goto_0
    return v0

    .line 20198
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 20200
    :cond_1
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->k:B

    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 20090
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->f:I

    return v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 20104
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 20115
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->g:I

    return v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 20128
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$s$b;
    .locals 1

    .prologue
    .line 20138
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->h:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    return-object v0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 20151
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 20161
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->i:I

    return v0
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 20174
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 20184
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->j:I

    return v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20317
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->v()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 20260
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20321
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method
