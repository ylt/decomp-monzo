.class public final Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$i$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$j;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

.field private l:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x6

    .line 14386
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 14609
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->b:I

    .line 14689
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->c:I

    .line 14753
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 14845
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    .line 14970
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 15062
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    .line 15187
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 14387
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->y()V

    .line 14388
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 14848
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 14849
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    .line 14850
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14852
    :cond_0
    return-void
.end method

.method private B()V
    .locals 2

    .prologue
    .line 15065
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_0

    .line 15066
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    .line 15067
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 15069
    :cond_0
    return-void
.end method

.method static synthetic x()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14380
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->z()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method private y()V
    .locals 0

    .prologue
    .line 14391
    return-void
.end method

.method private static z()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14393
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14662
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14663
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->b:I

    .line 14665
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 2

    .prologue
    .line 15226
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 15228
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 15234
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 15235
    return-object p0

    .line 15231
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 2

    .prologue
    .line 14494
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->m()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 14545
    :goto_0
    return-object p0

    .line 14495
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14496
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14498
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 14499
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->r()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14501
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 14502
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->t()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14504
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 14505
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14507
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->w()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 14508
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->x()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->d(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14510
    :cond_5
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 14511
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 14512
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    .line 14513
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14520
    :cond_6
    :goto_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->A()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 14521
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->B()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14523
    :cond_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->C()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 14524
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->D()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->f(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14526
    :cond_8
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->c(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 14527
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 14528
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->c(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    .line 14529
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14536
    :cond_9
    :goto_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->G()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 14537
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->H()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14539
    :cond_a
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->I()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 14540
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->J()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->h(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14542
    :cond_b
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 14543
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 14515
    :cond_c
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->A()V

    .line 14516
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 14531
    :cond_d
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->B()V

    .line 14532
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->c(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 2

    .prologue
    .line 14792
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 14794
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 14800
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14801
    return-object p0

    .line 14797
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 14380
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14380
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 14380
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14706
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14707
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->c:I

    .line 14709
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 2

    .prologue
    .line 15009
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 15011
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 15017
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 15018
    return-object p0

    .line 15014
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 14380
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14380
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14738
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14739
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->d:I

    .line 14741
    return-object p0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14594
    const/4 v2, 0x0

    .line 14596
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 14601
    if-eqz v0, :cond_0

    .line 14602
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    .line 14605
    :cond_0
    return-object p0

    .line 14597
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 14598
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 14599
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 14601
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 14602
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    :cond_1
    throw v0

    .line 14601
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 14380
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14380
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14830
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14831
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->f:I

    .line 14833
    return-object p0
.end method

.method public e(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 14870
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 14380
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v0

    return-object v0
.end method

.method public f(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 15047
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 15048
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->i:I

    .line 15050
    return-object p0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 14380
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public g(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 15087
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object v0
.end method

.method public h(I)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 15276
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 15277
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->l:I

    .line 15279
    return-object p0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 14380
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 14380
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 2

    .prologue
    .line 14424
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->z()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 1

    .prologue
    .line 14428
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->m()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 14549
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 14587
    :cond_0
    :goto_0
    return v1

    .line 14553
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 14554
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    move v0, v1

    .line 14559
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->r()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 14560
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->e(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 14559
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 14565
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 14566
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->t()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v0, v1

    .line 14571
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->u()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 14572
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 14571
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14577
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->v()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 14578
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->w()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14583
    :cond_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14587
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 2

    .prologue
    .line 14432
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v0

    .line 14433
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 14434
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 14436
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 14440
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 14441
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14442
    const/4 v1, 0x0

    .line 14443
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    .line 14446
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I

    .line 14447
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 14448
    or-int/lit8 v0, v0, 0x2

    .line 14450
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I

    .line 14451
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 14452
    or-int/lit8 v0, v0, 0x4

    .line 14454
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->d:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->c(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I

    .line 14455
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 14456
    or-int/lit8 v0, v0, 0x8

    .line 14458
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 14459
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 14460
    or-int/lit8 v0, v0, 0x10

    .line 14462
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I

    .line 14463
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 14464
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    .line 14465
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14467
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Ljava/util/List;)Ljava/util/List;

    .line 14468
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 14469
    or-int/lit8 v0, v0, 0x20

    .line 14471
    :cond_5
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 14472
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 14473
    or-int/lit8 v0, v0, 0x40

    .line 14475
    :cond_6
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->i:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I

    .line 14476
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 14477
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    .line 14478
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    .line 14480
    :cond_7
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(Lkotlin/reflect/jvm/internal/impl/i/e$i;Ljava/util/List;)Ljava/util/List;

    .line 14481
    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 14482
    or-int/lit16 v0, v0, 0x80

    .line 14484
    :cond_8
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 14485
    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    .line 14486
    or-int/lit16 v0, v0, 0x100

    .line 14488
    :cond_9
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->l:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->f(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I

    .line 14489
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->g(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I

    .line 14490
    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 14726
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 14758
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 14764
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 14864
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 14975
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 14981
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 15081
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public v()Z
    .locals 2

    .prologue
    .line 15192
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 1

    .prologue
    .line 15198
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->k:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object v0
.end method
