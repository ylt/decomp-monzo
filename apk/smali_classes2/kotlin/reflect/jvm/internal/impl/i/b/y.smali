.class public final Lkotlin/reflect/jvm/internal/impl/i/b/y;
.super Ljava/lang/Object;
.source "NameResolverImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/x;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/i/e$w;

.field private final b:Lkotlin/reflect/jvm/internal/impl/i/e$q;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/e$w;Lkotlin/reflect/jvm/internal/impl/i/e$q;)V
    .locals 1

    .prologue
    const-string v0, "strings"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qualifiedNames"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/y;->a:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/y;->b:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    return-void
.end method

.method private final d(I)Lkotlin/k;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lkotlin/k",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    .line 47
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 48
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 49
    const/4 v0, 0x0

    .line 51
    :goto_0
    const/4 v3, -0x1

    if-eq p1, v3, :cond_1

    .line 52
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/y;->b:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-virtual {v3, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v3

    .line 53
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/y;->a:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->o()I

    move-result v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 54
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->q()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/b/z;->a:[I

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    .line 63
    :goto_1
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->m()I

    move-result p1

    goto :goto_0

    .line 55
    :pswitch_0
    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_1

    .line 56
    :pswitch_1
    invoke-virtual {v1, v4}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_1

    .line 58
    :pswitch_2
    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 59
    const/4 v0, 0x1

    goto :goto_1

    .line 65
    :cond_1
    new-instance v3, Lkotlin/k;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {v3, v1, v2, v0}, Lkotlin/k;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/y;->a:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "strings.getString(index)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/y;->a:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->d(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 4

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/y;->d(I)Lkotlin/k;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 37
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/e/a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-direct {v3, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/e/a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/b;Z)V

    return-object v3
.end method
