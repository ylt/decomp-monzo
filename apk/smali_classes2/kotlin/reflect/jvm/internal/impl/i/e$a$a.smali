.class public final Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;,
        Lkotlin/reflect/jvm/internal/impl/i/e$a$a$c;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2079
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 3867
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    .line 3868
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->s()V

    .line 3869
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2020
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 3542
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h:B

    .line 3576
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->i:I

    .line 2021
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->s()V

    .line 2023
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v4

    .line 2025
    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v5

    .line 2029
    const/4 v2, 0x0

    .line 2030
    :goto_0
    if-nez v2, :cond_1

    .line 2031
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 2032
    sparse-switch v0, :sswitch_data_0

    .line 2037
    invoke-virtual {p0, p1, v5, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    move v2, v0

    .line 2062
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2035
    goto :goto_1

    .line 2044
    :sswitch_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    .line 2045
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->f:I

    move v0, v2

    .line 2046
    goto :goto_1

    .line 2049
    :sswitch_2
    const/4 v0, 0x0

    .line 2050
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    and-int/lit8 v3, v3, 0x2

    const/4 v6, 0x2

    if-ne v3, v6, :cond_2

    .line 2051
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->F()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    move-object v3, v0

    .line 2053
    :goto_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 2054
    if-eqz v3, :cond_0

    .line 2055
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    .line 2056
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 2058
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v0, v2

    .line 2059
    goto :goto_1

    .line 2070
    :cond_1
    :try_start_1
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2074
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2076
    :goto_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d()V

    .line 2078
    return-void

    .line 2071
    :catch_0
    move-exception v0

    .line 2074
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 2063
    :catch_1
    move-exception v0

    .line 2064
    :try_start_2
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2069
    :catchall_1
    move-exception v0

    .line 2070
    :try_start_3
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2074
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2076
    :goto_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d()V

    throw v0

    .line 2065
    :catch_2
    move-exception v0

    .line 2066
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2071
    :catch_3
    move-exception v1

    .line 2074
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_4

    :catchall_2
    move-exception v0

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    :cond_2
    move-object v3, v0

    goto :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_1

    .line 2032
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1996
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2002
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 3542
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h:B

    .line 3576
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->i:I

    .line 2003
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2004
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 1996
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2005
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 3542
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h:B

    .line 3576
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->i:I

    .line 2005
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;I)I
    .locals 0

    .prologue
    .line 1996
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->f:I

    return p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 1

    .prologue
    .line 3658
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 0

    .prologue
    .line 1996
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;I)I
    .locals 0

    .prologue
    .line 1996
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 1996
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
    .locals 1

    .prologue
    .line 2009
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    return-object v0
.end method

.method public static p()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 1

    .prologue
    .line 3655
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 3539
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->f:I

    .line 3540
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 3541
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3566
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->i()I

    .line 3567
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3568
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 3570
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3571
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 3573
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 3574
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2091
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1996
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
    .locals 1

    .prologue
    .line 2013
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3514
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3578
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->i:I

    .line 3579
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3592
    :goto_0
    return v0

    .line 3581
    :cond_0
    const/4 v0, 0x0

    .line 3582
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 3583
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3586
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 3587
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3590
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 3591
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->i:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 1996
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 1996
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3544
    iget-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h:B

    .line 3545
    if-ne v2, v0, :cond_0

    .line 3561
    :goto_0
    return v0

    .line 3546
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 3548
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3549
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h:B

    move v0, v1

    .line 3550
    goto :goto_0

    .line 3552
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->n()Z

    move-result v2

    if-nez v2, :cond_3

    .line 3553
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h:B

    move v0, v1

    .line 3554
    goto :goto_0

    .line 3556
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l()Z

    move-result v2

    if-nez v2, :cond_4

    .line 3557
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h:B

    move v0, v1

    .line 3558
    goto :goto_0

    .line 3560
    :cond_4
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h:B

    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 3520
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->f:I

    return v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 3529
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 1

    .prologue
    .line 3535
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    return-object v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 1

    .prologue
    .line 3656
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 1

    .prologue
    .line 3660
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 3599
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
