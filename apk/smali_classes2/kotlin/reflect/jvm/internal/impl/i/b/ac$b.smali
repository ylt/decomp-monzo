.class public final Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;
.super Ljava/lang/Object;
.source "PlatformDependentDeclarationFilter.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/ac;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/ac;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ac$b;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;Lkotlin/reflect/jvm/internal/impl/b/ak;)Z
    .locals 2

    .prologue
    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "functionDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/b/ak;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/b/ad;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
