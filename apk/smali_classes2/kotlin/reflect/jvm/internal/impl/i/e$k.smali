.class public final enum Lkotlin/reflect/jvm/internal/impl/i/e$k;
.super Ljava/lang/Enum;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "k"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$k;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/i/e$k;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/i/e$k;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/i/e$k;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/i/e$k;

.field private static e:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$k;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic g:[Lkotlin/reflect/jvm/internal/impl/i/e$k;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 205
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;

    const-string v1, "DECLARATION"

    invoke-direct {v0, v1, v2, v2, v2}, Lkotlin/reflect/jvm/internal/impl/i/e$k;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->a:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    .line 209
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;

    const-string v1, "FAKE_OVERRIDE"

    invoke-direct {v0, v1, v3, v3, v3}, Lkotlin/reflect/jvm/internal/impl/i/e$k;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->b:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    .line 213
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;

    const-string v1, "DELEGATION"

    invoke-direct {v0, v1, v4, v4, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$k;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->c:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    .line 217
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;

    const-string v1, "SYNTHESIZED"

    invoke-direct {v0, v1, v5, v5, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$k;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->d:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    .line 196
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/i/e$k;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$k;->a:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$k;->b:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$k;->c:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$k;->d:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->g:[Lkotlin/reflect/jvm/internal/impl/i/e$k;

    .line 259
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$k$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$k$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 268
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 269
    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->f:I

    .line 270
    return-void
.end method

.method public static a(I)Lkotlin/reflect/jvm/internal/impl/i/e$k;
    .locals 1

    .prologue
    .line 245
    packed-switch p0, :pswitch_data_0

    .line 250
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 246
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->a:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    goto :goto_0

    .line 247
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->b:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    goto :goto_0

    .line 248
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->c:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    goto :goto_0

    .line 249
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->d:Lkotlin/reflect/jvm/internal/impl/i/e$k;

    goto :goto_0

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/e$k;
    .locals 1

    .prologue
    .line 196
    const-class v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/i/e$k;
    .locals 1

    .prologue
    .line 196
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->g:[Lkotlin/reflect/jvm/internal/impl/i/e$k;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/i/e$k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/i/e$k;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$k;->f:I

    return v0
.end method
