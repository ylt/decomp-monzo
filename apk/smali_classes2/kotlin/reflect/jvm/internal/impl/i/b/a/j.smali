.class public final Lkotlin/reflect/jvm/internal/impl/i/b/a/j;
.super Lkotlin/reflect/jvm/internal/impl/b/b/y;
.source "DeserializedMemberDescriptor.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/a/c;


# instance fields
.field private final e:Lkotlin/reflect/jvm/internal/impl/i/e$o;

.field private final f:Lkotlin/reflect/jvm/internal/impl/i/b/x;

.field private final g:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

.field private final h:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

.field private final i:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;ZZZZLkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)V
    .locals 18

    .prologue
    const-string v2, "containingDeclaration"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "annotations"

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "modality"

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "visibility"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "name"

    move-object/from16 v0, p7

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "kind"

    move-object/from16 v0, p8

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "proto"

    move-object/from16 v0, p13

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "nameResolver"

    move-object/from16 v0, p14

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "typeTable"

    move-object/from16 v0, p15

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "sinceKotlinInfoTable"

    move-object/from16 v0, p16

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    sget-object v11, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v12, p9

    move/from16 v13, p10

    move/from16 v16, p11

    move/from16 v17, p12

    .line 115
    invoke-direct/range {v2 .. v17}, Lkotlin/reflect/jvm/internal/impl/b/b/y;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;ZZZZZZ)V

    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->e:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-object/from16 v0, p14

    move-object/from16 v1, p0

    iput-object v0, v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->f:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-object/from16 v0, p15

    move-object/from16 v1, p0

    iput-object v0, v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->g:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->i:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    return-void
.end method


# virtual methods
.method public F()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->w:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->G()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public G()Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->e:Lkotlin/reflect/jvm/internal/impl/i/e$o;

    return-object v0
.end method

.method public H()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    return-object v0
.end method

.method public I()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->i:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    return-object v0
.end method

.method public synthetic J()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->G()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    return-object v0
.end method

.method public K()Lkotlin/reflect/jvm/internal/impl/i/b/x;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->f:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    return-object v0
.end method

.method public L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->g:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/b$a;)Lkotlin/reflect/jvm/internal/impl/b/b/y;
    .locals 19

    .prologue
    const-string v1, "newOwner"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newModality"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newVisibility"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "kind"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->y()Z

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v8

    const-string v2, "name"

    invoke-static {v8, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->r()Z

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->A()Z

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->F()Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "isExternal"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->B()Z

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->G()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->K()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->L()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->H()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->I()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v18

    move-object/from16 v2, p1

    move-object/from16 v3, p4

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v9, p5

    invoke-direct/range {v1 .. v18}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;ZZZZLkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/b/y;

    return-object v1
.end method

.method public synthetic v()Z
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->F()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
