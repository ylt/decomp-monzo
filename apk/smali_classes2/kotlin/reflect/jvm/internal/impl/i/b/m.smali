.class public final Lkotlin/reflect/jvm/internal/impl/i/b/m;
.super Ljava/lang/Object;
.source "context.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/i/b/j;

.field private final b:Lkotlin/reflect/jvm/internal/impl/j/i;

.field private final c:Lkotlin/reflect/jvm/internal/impl/b/w;

.field private final d:Lkotlin/reflect/jvm/internal/impl/i/b/n;

.field private final e:Lkotlin/reflect/jvm/internal/impl/i/b/h;

.field private final f:Lkotlin/reflect/jvm/internal/impl/i/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/i/b/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lkotlin/reflect/jvm/internal/impl/b/z;

.field private final h:Lkotlin/reflect/jvm/internal/impl/i/b/v;

.field private final i:Lkotlin/reflect/jvm/internal/impl/i/b/r;

.field private final j:Lkotlin/reflect/jvm/internal/impl/c/a/c;

.field private final k:Lkotlin/reflect/jvm/internal/impl/i/b/t;

.field private final l:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/b/i;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lkotlin/reflect/jvm/internal/impl/i/b/aa;

.field private final n:Lkotlin/reflect/jvm/internal/impl/i/b/a;

.field private final o:Lkotlin/reflect/jvm/internal/impl/i/b/ac;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/n;Lkotlin/reflect/jvm/internal/impl/i/b/h;Lkotlin/reflect/jvm/internal/impl/i/b/c;Lkotlin/reflect/jvm/internal/impl/b/z;Lkotlin/reflect/jvm/internal/impl/i/b/v;Lkotlin/reflect/jvm/internal/impl/i/b/r;Lkotlin/reflect/jvm/internal/impl/c/a/c;Lkotlin/reflect/jvm/internal/impl/i/b/t;Ljava/lang/Iterable;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/i/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ac;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/j/i;",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/n;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/h;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/c",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            "+",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/b/z;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/v;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/r;",
            "Lkotlin/reflect/jvm/internal/impl/c/a/c;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/t;",
            "Ljava/lang/Iterable",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/i/b/i;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/aa;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ac;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moduleDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classDataFinder"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotationAndConstantLoader"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageFragmentProvider"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localClassifierTypeSettings"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorReporter"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lookupTracker"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flexibleTypeDeserializer"

    invoke-static {p10, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fictitiousClassDescriptorFactories"

    invoke-static {p11, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notFoundClasses"

    invoke-static {p12, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalClassPartsProvider"

    invoke-static {p13, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "platformDependentDeclarationFilter"

    invoke-static {p14, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->b:Lkotlin/reflect/jvm/internal/impl/j/i;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->c:Lkotlin/reflect/jvm/internal/impl/b/w;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->d:Lkotlin/reflect/jvm/internal/impl/i/b/n;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->e:Lkotlin/reflect/jvm/internal/impl/i/b/h;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->f:Lkotlin/reflect/jvm/internal/impl/i/b/c;

    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->g:Lkotlin/reflect/jvm/internal/impl/b/z;

    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->h:Lkotlin/reflect/jvm/internal/impl/i/b/v;

    iput-object p8, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->i:Lkotlin/reflect/jvm/internal/impl/i/b/r;

    iput-object p9, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->j:Lkotlin/reflect/jvm/internal/impl/c/a/c;

    iput-object p10, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->k:Lkotlin/reflect/jvm/internal/impl/i/b/t;

    iput-object p11, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->l:Ljava/lang/Iterable;

    iput-object p12, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->m:Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    iput-object p13, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->n:Lkotlin/reflect/jvm/internal/impl/i/b/a;

    iput-object p14, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->o:Lkotlin/reflect/jvm/internal/impl/i/b/ac;

    .line 46
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/j;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/j;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/m;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->a:Lkotlin/reflect/jvm/internal/impl/i/b/j;

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->a:Lkotlin/reflect/jvm/internal/impl/i/b/j;

    const/4 v1, 0x2

    invoke-static {v0, p1, v2, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/j;->a(Lkotlin/reflect/jvm/internal/impl/i/b/j;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lkotlin/reflect/jvm/internal/impl/i/b/j;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->a:Lkotlin/reflect/jvm/internal/impl/i/b/j;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)Lkotlin/reflect/jvm/internal/impl/i/b/o;
    .locals 9

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sinceKotlinInfoTable"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-object v3, p1

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/m;

    const/4 v7, 0x0

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v8

    move-object v1, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lkotlin/reflect/jvm/internal/impl/i/b/o;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/m;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/i/b/ag;Ljava/util/List;)V

    .line 58
    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/j/i;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->b:Lkotlin/reflect/jvm/internal/impl/j/i;

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->c:Lkotlin/reflect/jvm/internal/impl/b/w;

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/i/b/n;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->d:Lkotlin/reflect/jvm/internal/impl/i/b/n;

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/i/b/h;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->e:Lkotlin/reflect/jvm/internal/impl/i/b/h;

    return-object v0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/i/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/i/b/c",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->f:Lkotlin/reflect/jvm/internal/impl/i/b/c;

    return-object v0
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/b/z;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->g:Lkotlin/reflect/jvm/internal/impl/b/z;

    return-object v0
.end method

.method public final h()Lkotlin/reflect/jvm/internal/impl/i/b/v;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->h:Lkotlin/reflect/jvm/internal/impl/i/b/v;

    return-object v0
.end method

.method public final i()Lkotlin/reflect/jvm/internal/impl/i/b/r;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->i:Lkotlin/reflect/jvm/internal/impl/i/b/r;

    return-object v0
.end method

.method public final j()Lkotlin/reflect/jvm/internal/impl/c/a/c;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->j:Lkotlin/reflect/jvm/internal/impl/c/a/c;

    return-object v0
.end method

.method public final k()Lkotlin/reflect/jvm/internal/impl/i/b/t;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->k:Lkotlin/reflect/jvm/internal/impl/i/b/t;

    return-object v0
.end method

.method public final l()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/b/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->l:Ljava/lang/Iterable;

    return-object v0
.end method

.method public final m()Lkotlin/reflect/jvm/internal/impl/i/b/aa;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->m:Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    return-object v0
.end method

.method public final n()Lkotlin/reflect/jvm/internal/impl/i/b/a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->n:Lkotlin/reflect/jvm/internal/impl/i/b/a;

    return-object v0
.end method

.method public final o()Lkotlin/reflect/jvm/internal/impl/i/b/ac;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/m;->o:Lkotlin/reflect/jvm/internal/impl/i/b/ac;

    return-object v0
.end method
