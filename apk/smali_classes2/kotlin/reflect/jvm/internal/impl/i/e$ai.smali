.class public final enum Lkotlin/reflect/jvm/internal/impl/i/e$ai;
.super Ljava/lang/Enum;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ai"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ai;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

.field public static final enum e:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

.field public static final enum f:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

.field private static g:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ai;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic i:[Lkotlin/reflect/jvm/internal/impl/i/e$ai;


# instance fields
.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 105
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    const-string v1, "INTERNAL"

    invoke-direct {v0, v1, v5, v5, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$ai;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->a:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    .line 109
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    const-string v1, "PRIVATE"

    invoke-direct {v0, v1, v6, v6, v6}, Lkotlin/reflect/jvm/internal/impl/i/e$ai;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->b:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    .line 113
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    const-string v1, "PROTECTED"

    invoke-direct {v0, v1, v7, v7, v7}, Lkotlin/reflect/jvm/internal/impl/i/e$ai;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    .line 117
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v8, v8, v8}, Lkotlin/reflect/jvm/internal/impl/i/e$ai;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->d:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    .line 121
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    const-string v1, "PRIVATE_TO_THIS"

    invoke-direct {v0, v1, v9, v9, v9}, Lkotlin/reflect/jvm/internal/impl/i/e$ai;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    .line 125
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    const-string v1, "LOCAL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$ai;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->f:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    .line 96
    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->a:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    aput-object v1, v0, v5

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->b:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    aput-object v1, v0, v6

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    aput-object v1, v0, v7

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->d:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    aput-object v1, v0, v8

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->f:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    aput-object v2, v0, v1

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->i:[Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    .line 177
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ai$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 187
    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->h:I

    .line 188
    return-void
.end method

.method public static a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ai;
    .locals 1

    .prologue
    .line 161
    packed-switch p0, :pswitch_data_0

    .line 168
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 162
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->a:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    goto :goto_0

    .line 163
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->b:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    goto :goto_0

    .line 164
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    goto :goto_0

    .line 165
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->d:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    goto :goto_0

    .line 166
    :pswitch_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    goto :goto_0

    .line 167
    :pswitch_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->f:Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/e$ai;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/i/e$ai;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->i:[Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/i/e$ai;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;->h:I

    return v0
.end method
