.class public final Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$aa;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$z;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$z$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$aa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private f:I

.field private g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private j:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 18647
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 18846
    const/4 v0, 0x6

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->b:I

    .line 18930
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    .line 19055
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 19147
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 19239
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    .line 18648
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->w()V

    .line 18649
    return-void
.end method

.method static synthetic v()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 18641
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->x()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .locals 0

    .prologue
    .line 18652
    return-void
.end method

.method private static x()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 18654
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;-><init>()V

    return-object v0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 18933
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 18934
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    .line 18935
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 18937
    :cond_0
    return-void
.end method

.method private z()V
    .locals 2

    .prologue
    .line 19242
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 19243
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    .line 19244
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 19246
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 18878
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 18879
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->b:I

    .line 18881
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 2

    .prologue
    .line 19094
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 19096
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 19102
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 19103
    return-object p0

    .line 19099
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 2

    .prologue
    .line 18743
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 18788
    :goto_0
    return-object p0

    .line 18744
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18745
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    .line 18747
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 18748
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->r()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    .line 18750
    :cond_2
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 18751
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 18752
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    .line 18753
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 18760
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 18761
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    .line 18763
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->w()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 18764
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->x()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    .line 18766
    :cond_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 18767
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->z()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    .line 18769
    :cond_6
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->A()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 18770
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->B()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->e(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    .line 18772
    :cond_7
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->c(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 18773
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 18774
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->c(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    .line 18775
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 18782
    :cond_8
    :goto_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->E()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 18783
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->F()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    .line 18785
    :cond_9
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 18786
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 18755
    :cond_a
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->y()V

    .line 18756
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 18777
    :cond_b
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->z()V

    .line 18778
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->c(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 18641
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18641
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 18641
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$z;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 18915
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 18916
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->c:I

    .line 18918
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 2

    .prologue
    .line 19186
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 19188
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 19194
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 19195
    return-object p0

    .line 19191
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 18641
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18641
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 18955
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18831
    const/4 v2, 0x0

    .line 18833
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 18838
    if-eqz v0, :cond_0

    .line 18839
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    .line 18842
    :cond_0
    return-object p0

    .line 18834
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 18835
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 18836
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 18838
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 18839
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    :cond_1
    throw v0

    .line 18838
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 18641
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 18641
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 19132
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 19133
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->f:I

    .line 19135
    return-object p0
.end method

.method public e(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 19224
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 19225
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->h:I

    .line 19227
    return-object p0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 18641
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    return-object v0
.end method

.method public f(I)Lkotlin/reflect/jvm/internal/impl/i/e$a;
    .locals 1

    .prologue
    .line 19264
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 18641
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public g(I)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 1

    .prologue
    .line 19393
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 19394
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->j:I

    .line 19396
    return-object p0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 18641
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 18641
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;
    .locals 2

    .prologue
    .line 18681
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->x()Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/i/e$z$a;

    move-result-object v0

    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 1

    .prologue
    .line 18685
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->m()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 18792
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 18824
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 18796
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->p()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 18797
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 18796
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 18802
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 18803
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18808
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 18809
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->t()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v0, v1

    .line 18814
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->u()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 18815
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->f(I)Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 18814
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 18820
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18824
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 2

    .prologue
    .line 18689
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v0

    .line 18690
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18691
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 18693
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 18697
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$z;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 18698
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 18699
    const/4 v1, 0x0

    .line 18700
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    .line 18703
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I

    .line 18704
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 18705
    or-int/lit8 v0, v0, 0x2

    .line 18707
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I

    .line 18708
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 18709
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    .line 18710
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 18712
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;Ljava/util/List;)Ljava/util/List;

    .line 18713
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 18714
    or-int/lit8 v0, v0, 0x4

    .line 18716
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 18717
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 18718
    or-int/lit8 v0, v0, 0x8

    .line 18720
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->c(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I

    .line 18721
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 18722
    or-int/lit8 v0, v0, 0x10

    .line 18724
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b(Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 18725
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 18726
    or-int/lit8 v0, v0, 0x20

    .line 18728
    :cond_5
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->h:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->d(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I

    .line 18729
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 18730
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    .line 18731
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    .line 18733
    :cond_6
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->b(Lkotlin/reflect/jvm/internal/impl/i/e$z;Ljava/util/List;)Ljava/util/List;

    .line 18734
    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    .line 18735
    or-int/lit8 v0, v0, 0x40

    .line 18737
    :cond_7
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->j:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->e(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I

    .line 18738
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->f(Lkotlin/reflect/jvm/internal/impl/i/e$z;I)I

    .line 18739
    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 18903
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 18949
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 19060
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 19066
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 19152
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 19158
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 19258
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$z$a;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
