.class public final Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$y$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y$a;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y$b;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

.field private c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private d:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5110
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 5212
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 5247
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 5111
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->p()V

    .line 5112
    return-void
.end method

.method static synthetic o()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 1

    .prologue
    .line 5103
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 0

    .prologue
    .line 5115
    return-void
.end method

.method private static q()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 1

    .prologue
    .line 5117
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 1

    .prologue
    .line 5348
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    .line 5349
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->d:I

    .line 5351
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 1

    .prologue
    .line 5229
    if-nez p1, :cond_0

    .line 5230
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5232
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    .line 5233
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 5235
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 2

    .prologue
    .line 5168
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 5180
    :goto_0
    return-object p0

    .line 5169
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5170
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    .line 5172
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5173
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    .line 5175
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5176
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->q()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    .line 5178
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 2

    .prologue
    .line 5306
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5308
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 5314
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    .line 5315
    return-object p0

    .line 5311
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 5103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5103
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 5103
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 5103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5103
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5197
    const/4 v2, 0x0

    .line 5199
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5204
    if-eqz v0, :cond_0

    .line 5205
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    .line 5208
    :cond_0
    return-object p0

    .line 5200
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 5201
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 5202
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5204
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 5205
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    :cond_1
    throw v0

    .line 5204
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 5103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 5103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 5103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 2

    .prologue
    .line 5132
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    .locals 1

    .prologue
    .line 5136
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 5103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 5103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    .locals 2

    .prologue
    .line 5140
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v0

    .line 5141
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5142
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 5144
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5148
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 5149
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    .line 5150
    const/4 v1, 0x0

    .line 5151
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 5154
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 5155
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 5156
    or-int/lit8 v0, v0, 0x2

    .line 5158
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 5159
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 5160
    or-int/lit8 v0, v0, 0x4

    .line 5162
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->d:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;I)I

    .line 5163
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;I)I

    .line 5164
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 5184
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5185
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5187
    const/4 v0, 0x0

    .line 5190
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 5256
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 5266
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method
