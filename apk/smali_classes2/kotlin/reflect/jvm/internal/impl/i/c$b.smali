.class Lkotlin/reflect/jvm/internal/impl/i/c$b;
.super Lkotlin/reflect/jvm/internal/impl/i/c$c;
.source "Flags.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;",
        ">",
        "Lkotlin/reflect/jvm/internal/impl/i/c$c",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final c:[Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I[Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[TE;)V"
        }
    .end annotation

    .prologue
    .line 339
    invoke-static {p2}, Lkotlin/reflect/jvm/internal/impl/i/c$b;->a([Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$c;-><init>(IILkotlin/reflect/jvm/internal/impl/i/c$1;)V

    .line 340
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/c$b;->c:[Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;

    .line 341
    return-void
.end method

.method private static a([Ljava/lang/Object;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "enumEntries"

    aput-object v5, v3, v4

    const-string v4, "kotlin/reflect/jvm/internal/impl/serialization/Flags$EnumLiteFlagField"

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const-string v4, "bitWidth"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 344
    :cond_0
    array-length v1, p0

    add-int/lit8 v2, v1, -0x1

    .line 345
    if-nez v2, :cond_1

    .line 347
    :goto_0
    return v0

    .line 346
    :cond_1
    const/16 v1, 0x1f

    :goto_1
    if-ltz v1, :cond_3

    .line 347
    shl-int v3, v0, v1

    and-int/2addr v3, v2

    if-eqz v3, :cond_2

    add-int/lit8 v0, v1, 0x1

    goto :goto_0

    .line 346
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 349
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Empty enum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 355
    const/4 v0, 0x1

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c$b;->b:I

    shl-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 356
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c$b;->a:I

    shl-int/2addr v0, v1

    .line 357
    and-int/2addr v0, p1

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c$b;->a:I

    shr-int v2, v0, v1

    .line 358
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/c$b;->c:[Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 359
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;->a()I

    move-result v5

    if-ne v5, v2, :cond_0

    .line 363
    :goto_1
    return-object v0

    .line 358
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 363
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public synthetic b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 335
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c$b;->a(I)Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;

    move-result-object v0

    return-object v0
.end method
