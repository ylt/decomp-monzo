.class public final Lkotlin/reflect/jvm/internal/impl/i/b/a/m;
.super Lkotlin/reflect/jvm/internal/impl/b/b/b;
.source "DeserializedTypeParameterDescriptor.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/i/b/a/b;

.field private final b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

.field private final c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/i/e$ac;I)V
    .locals 9

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->r()I

    move-result v3

    invoke-interface {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->v()Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    move-result-object v0

    const-string v4, "proto.variance"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v4

    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->t()Z

    move-result v5

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/b/ao$a;->a:Lkotlin/reflect/jvm/internal/impl/b/ao$a;

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/b/ao;

    move-object v0, p0

    move v6, p3

    .line 35
    invoke-direct/range {v0 .. v8}, Lkotlin/reflect/jvm/internal/impl/b/b/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/ap;ZILkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ao;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    .line 39
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/b;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/m$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/m;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/b;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/a/m;)Lkotlin/reflect/jvm/internal/impl/i/b/o;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/i/b/a/m;)Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method


# virtual methods
.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/k/r;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Void;

    return-void
.end method

.method protected b(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Void;
    .locals 3

    .prologue
    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There should be no cycles for deserialized type parameters, but found for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method protected m()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;

    move-result-object v0

    .line 47
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->z()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 62
    :goto_0
    return-object v1

    .line 50
    :cond_0
    check-cast v0, Ljava/lang/Iterable;

    .line 59
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 60
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 61
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 51
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 62
    :cond_1
    check-cast v1, Ljava/util/List;

    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/b/a/b;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/b;

    return-object v0
.end method

.method public synthetic w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;->n()Lkotlin/reflect/jvm/internal/impl/i/b/a/b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method
