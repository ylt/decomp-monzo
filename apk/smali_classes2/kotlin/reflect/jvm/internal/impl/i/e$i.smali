.class public final Lkotlin/reflect/jvm/internal/impl/i/e$i;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$j;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$i;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private j:I

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private m:I

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

.field private p:I

.field private q:B

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13897
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$i$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 15299
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->c:Lkotlin/reflect/jvm/internal/impl/i/e$i;

    .line 15300
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->c:Lkotlin/reflect/jvm/internal/impl/i/e$i;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->N()V

    .line 15301
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    const/4 v4, 0x0

    const/16 v8, 0x100

    const/16 v7, 0x20

    .line 13765
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 14163
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    .line 14254
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->r:I

    .line 13766
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->N()V

    .line 13768
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v5

    .line 13770
    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v6

    move v2, v1

    .line 13775
    :goto_0
    if-nez v1, :cond_4

    .line 13776
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 13777
    sparse-switch v0, :sswitch_data_0

    .line 13782
    invoke-virtual {p0, p1, v6, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 13784
    const/4 v0, 0x1

    move v1, v2

    :goto_1
    move v2, v1

    move v1, v0

    .line 13874
    goto :goto_0

    .line 13779
    :sswitch_0
    const/4 v0, 0x1

    move v1, v2

    .line 13780
    goto :goto_1

    .line 13789
    :sswitch_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    .line 13790
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->g:I

    move v0, v1

    move v1, v2

    .line 13791
    goto :goto_1

    .line 13794
    :sswitch_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    .line 13795
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->h:I

    move v0, v1

    move v1, v2

    .line 13796
    goto :goto_1

    .line 13800
    :sswitch_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_d

    .line 13801
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v3, v0

    .line 13803
    :goto_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 13804
    if-eqz v3, :cond_0

    .line 13805
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 13806
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 13808
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    move v0, v1

    move v1, v2

    .line 13809
    goto :goto_1

    .line 13812
    :sswitch_4
    and-int/lit8 v0, v2, 0x20

    if-eq v0, v7, :cond_c

    .line 13813
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 13814
    or-int/lit8 v0, v2, 0x20

    .line 13816
    :goto_3
    :try_start_1
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v3, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move v9, v1

    move v1, v0

    move v0, v9

    .line 13817
    goto :goto_1

    .line 13821
    :sswitch_5
    :try_start_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x20

    if-ne v0, v7, :cond_b

    .line 13822
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v3, v0

    .line 13824
    :goto_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 13825
    if-eqz v3, :cond_1

    .line 13826
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 13827
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 13829
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    move v0, v1

    move v1, v2

    .line 13830
    goto/16 :goto_1

    .line 13833
    :sswitch_6
    and-int/lit16 v0, v2, 0x100

    if-eq v0, v8, :cond_a

    .line 13834
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;
    :try_end_2
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 13835
    or-int/lit16 v0, v2, 0x100

    .line 13837
    :goto_5
    :try_start_3
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v3, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move v9, v1

    move v1, v0

    move v0, v9

    .line 13838
    goto/16 :goto_1

    .line 13841
    :sswitch_7
    :try_start_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    .line 13842
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->j:I

    move v0, v1

    move v1, v2

    .line 13843
    goto/16 :goto_1

    .line 13846
    :sswitch_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    .line 13847
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->m:I

    move v0, v1

    move v1, v2

    .line 13848
    goto/16 :goto_1

    .line 13851
    :sswitch_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    .line 13852
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->f:I

    move v0, v1

    move v1, v2

    .line 13853
    goto/16 :goto_1

    .line 13857
    :sswitch_a
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_9

    .line 13858
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->r()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    move-object v3, v0

    .line 13860
    :goto_6
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 13861
    if-eqz v3, :cond_2

    .line 13862
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    .line 13863
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 13865
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    move v0, v1

    move v1, v2

    .line 13866
    goto/16 :goto_1

    .line 13869
    :sswitch_b
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    .line 13870
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->p:I
    :try_end_4
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_3
    move v0, v1

    move v1, v2

    goto/16 :goto_1

    .line 13881
    :cond_4
    and-int/lit8 v0, v2, 0x20

    if-ne v0, v7, :cond_5

    .line 13882
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    .line 13884
    :cond_5
    and-int/lit16 v0, v2, 0x100

    if-ne v0, v8, :cond_6

    .line 13885
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    .line 13888
    :cond_6
    :try_start_5
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 13892
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 13894
    :goto_7
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d()V

    .line 13896
    return-void

    .line 13889
    :catch_0
    move-exception v0

    .line 13892
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_7

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 13875
    :catch_1
    move-exception v0

    .line 13876
    :goto_8
    :try_start_6
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 13881
    :catchall_1
    move-exception v0

    :goto_9
    and-int/lit8 v1, v2, 0x20

    if-ne v1, v7, :cond_7

    .line 13882
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    .line 13884
    :cond_7
    and-int/lit16 v1, v2, 0x100

    if-ne v1, v8, :cond_8

    .line 13885
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    .line 13888
    :cond_8
    :try_start_7
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 13892
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 13894
    :goto_a
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d()V

    throw v0

    .line 13877
    :catch_2
    move-exception v0

    .line 13878
    :goto_b
    :try_start_8
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 13889
    :catch_3
    move-exception v1

    .line 13892
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_a

    :catchall_2
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 13881
    :catchall_3
    move-exception v1

    move v2, v0

    move-object v0, v1

    goto :goto_9

    .line 13877
    :catch_4
    move-exception v1

    move v2, v0

    move-object v0, v1

    goto :goto_b

    .line 13875
    :catch_5
    move-exception v1

    move v2, v0

    move-object v0, v1

    goto :goto_8

    :cond_9
    move-object v3, v4

    goto/16 :goto_6

    :cond_a
    move v0, v2

    goto/16 :goto_5

    :cond_b
    move-object v3, v4

    goto/16 :goto_4

    :cond_c
    move v0, v2

    goto/16 :goto_3

    :cond_d
    move-object v3, v4

    goto/16 :goto_2

    .line 13777
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0xf2 -> :sswitch_a
        0xf8 -> :sswitch_b
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13740
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$i;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 13747
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 14163
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    .line 14254
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->r:I

    .line 13748
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 13749
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 13740
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 13750
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 14163
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    .line 14254
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->r:I

    .line 13750
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method public static K()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14370
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->x()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method private N()V
    .locals 2

    .prologue
    const/4 v0, 0x6

    const/4 v1, 0x0

    .line 14151
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->f:I

    .line 14152
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->g:I

    .line 14153
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->h:I

    .line 14154
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 14155
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->j:I

    .line 14156
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    .line 14157
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 14158
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->m:I

    .line 14159
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    .line 14160
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 14161
    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->p:I

    .line 14162
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I
    .locals 0

    .prologue
    .line 13740
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->f:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 13740
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 0

    .prologue
    .line 13740
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14373
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->K()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14346
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->f(Ljava/io/InputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 13740
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I
    .locals 0

    .prologue
    .line 13740
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Ljava/util/List;
    .locals 1

    .prologue
    .line 13740
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$i;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 13740
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 13740
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I
    .locals 0

    .prologue
    .line 13740
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->h:I

    return p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Ljava/util/List;
    .locals 1

    .prologue
    .line 13740
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I
    .locals 0

    .prologue
    .line 13740
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->j:I

    return p1
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 13740
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I
    .locals 0

    .prologue
    .line 13740
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->m:I

    return p1
.end method

.method static synthetic f(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I
    .locals 0

    .prologue
    .line 13740
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->p:I

    return p1
.end method

.method static synthetic g(Lkotlin/reflect/jvm/internal/impl/i/e$i;I)I
    .locals 0

    .prologue
    .line 13740
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    return p1
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 1

    .prologue
    .line 13754
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->c:Lkotlin/reflect/jvm/internal/impl/i/e$i;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 14053
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 14059
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public C()Z
    .locals 2

    .prologue
    .line 14068
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()I
    .locals 1

    .prologue
    .line 14074
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->m:I

    return v0
.end method

.method public E()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14083
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    return-object v0
.end method

.method public F()I
    .locals 1

    .prologue
    .line 14096
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public G()Z
    .locals 2

    .prologue
    .line 14118
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 1

    .prologue
    .line 14124
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object v0
.end method

.method public I()Z
    .locals 2

    .prologue
    .line 14137
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()I
    .locals 1

    .prologue
    .line 14147
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->p:I

    return v0
.end method

.method public L()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14371
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->K()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public M()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;
    .locals 1

    .prologue
    .line 14375
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 14037
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 14213
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i()I

    .line 14215
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v3

    .line 14217
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_0

    .line 14218
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->g:I

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 14220
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 14221
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->h:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 14223
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 14224
    const/4 v0, 0x3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    :cond_2
    move v1, v2

    .line 14226
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 14227
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 14226
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 14229
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 14230
    const/4 v0, 0x5

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 14232
    :cond_4
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 14233
    const/4 v1, 0x6

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 14232
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 14235
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 14236
    const/4 v0, 0x7

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->j:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 14238
    :cond_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 14239
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->m:I

    invoke-virtual {p1, v6, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 14241
    :cond_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 14242
    const/16 v0, 0x9

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->f:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 14244
    :cond_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 14245
    const/16 v0, 0x1e

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 14247
    :cond_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 14248
    const/16 v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->p:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 14250
    :cond_a
    const/16 v0, 0xc8

    invoke-virtual {v3, v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 14251
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 14252
    return-void
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 14102
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13909
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 13740
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n()Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 14256
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->r:I

    .line 14257
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 14307
    :goto_0
    return v0

    .line 14260
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_b

    .line 14261
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->g:I

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 14264
    :goto_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_1

    .line 14265
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->h:I

    invoke-static {v3, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 14268
    :cond_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_2

    .line 14269
    const/4 v2, 0x3

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    move v2, v1

    move v3, v0

    .line 14272
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 14273
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 14272
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 14276
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_4

    .line 14277
    const/4 v0, 0x5

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 14280
    :cond_4
    :goto_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 14281
    const/4 v2, 0x6

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 14280
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 14284
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 14285
    const/4 v0, 0x7

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->j:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v3, v0

    .line 14288
    :cond_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 14289
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->m:I

    invoke-static {v6, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v3, v0

    .line 14292
    :cond_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 14293
    const/16 v0, 0x9

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->f:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v3, v0

    .line 14296
    :cond_8
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 14297
    const/16 v0, 0x1e

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 14300
    :cond_9
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 14301
    const/16 v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->p:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v3, v0

    .line 14304
    :cond_a
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->h()I

    move-result v0

    add-int/2addr v0, v3

    .line 14305
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 14306
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->r:I

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 13740
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->L()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 13740
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->M()Lkotlin/reflect/jvm/internal/impl/i/e$i$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 14165
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    .line 14166
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 14208
    :cond_0
    :goto_0
    return v1

    .line 14167
    :cond_1
    if-eqz v0, :cond_0

    .line 14169
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->s()Z

    move-result v0

    if-nez v0, :cond_2

    .line 14170
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    goto :goto_0

    .line 14173
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 14174
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_3

    .line 14175
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    goto :goto_0

    :cond_3
    move v0, v1

    .line 14179
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->z()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 14180
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l()Z

    move-result v3

    if-nez v3, :cond_4

    .line 14181
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    goto :goto_0

    .line 14179
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 14185
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->A()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 14186
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->B()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-nez v0, :cond_6

    .line 14187
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    goto :goto_0

    :cond_6
    move v0, v1

    .line 14191
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->F()I

    move-result v3

    if-ge v0, v3, :cond_8

    .line 14192
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l()Z

    move-result v3

    if-nez v3, :cond_7

    .line 14193
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    goto :goto_0

    .line 14191
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14197
    :cond_8
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->G()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 14198
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->H()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->l()Z

    move-result v0

    if-nez v0, :cond_9

    .line 14199
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    goto :goto_0

    .line 14203
    :cond_9
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->f()Z

    move-result v0

    if-nez v0, :cond_a

    .line 14204
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    goto :goto_0

    .line 14207
    :cond_a
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->q:B

    move v1, v2

    .line 14208
    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 1

    .prologue
    .line 13758
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->c:Lkotlin/reflect/jvm/internal/impl/i/e$i;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 13931
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 13949
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->f:I

    return v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 13958
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 13964
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->g:I

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 13973
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 13979
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->h:I

    return v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 13988
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 13994
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->i:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 14003
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 14314
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 14009
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->j:I

    return v0
.end method

.method public y()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14018
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    return-object v0
.end method

.method public z()I
    .locals 1

    .prologue
    .line 14031
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$i;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
