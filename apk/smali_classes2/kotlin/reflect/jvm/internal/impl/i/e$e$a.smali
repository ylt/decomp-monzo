.class public final Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$e$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$f;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 13236
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 13351
    const/4 v0, 0x6

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->b:I

    .line 13407
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    .line 13237
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->q()V

    .line 13238
    return-void
.end method

.method static synthetic p()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 1

    .prologue
    .line 13230
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 0

    .prologue
    .line 13241
    return-void
.end method

.method private static r()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 1

    .prologue
    .line 13243
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;-><init>()V

    return-object v0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 13410
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 13411
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    .line 13412
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    .line 13414
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 1

    .prologue
    .line 13386
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    .line 13387
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->b:I

    .line 13389
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 2

    .prologue
    .line 13295
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->m()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 13315
    :goto_0
    return-object p0

    .line 13296
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13297
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    .line 13299
    :cond_1
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 13300
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 13301
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    .line 13302
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    .line 13309
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 13310
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->t()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    .line 13312
    :cond_3
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 13313
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->c(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0

    .line 13304
    :cond_4
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->s()V

    .line 13305
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 13230
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13230
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 13230
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 13432
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 13230
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13230
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 1

    .prologue
    .line 13561
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    .line 13562
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->d:I

    .line 13564
    return-object p0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13336
    const/4 v2, 0x0

    .line 13338
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 13343
    if-eqz v0, :cond_0

    .line 13344
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    .line 13347
    :cond_0
    return-object p0

    .line 13339
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 13340
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 13341
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 13343
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 13344
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    :cond_1
    throw v0

    .line 13343
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 13230
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 13230
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 13230
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 13230
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 13230
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 13230
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;
    .locals 2

    .prologue
    .line 13258
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;)Lkotlin/reflect/jvm/internal/impl/i/e$e$a;

    move-result-object v0

    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$e;
    .locals 1

    .prologue
    .line 13262
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->m()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 13319
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->o()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 13320
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l()Z

    move-result v2

    if-nez v2, :cond_1

    .line 13329
    :cond_0
    :goto_1
    return v1

    .line 13319
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13325
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13329
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$e;
    .locals 2

    .prologue
    .line 13266
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v0

    .line 13267
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13268
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 13270
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$e;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 13274
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 13275
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    .line 13276
    const/4 v1, 0x0

    .line 13277
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 13280
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;I)I

    .line 13281
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 13282
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    .line 13283
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->a:I

    .line 13285
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$e;Ljava/util/List;)Ljava/util/List;

    .line 13286
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 13287
    or-int/lit8 v0, v0, 0x2

    .line 13289
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->d:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b(Lkotlin/reflect/jvm/internal/impl/i/e$e;I)I

    .line 13290
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->c(Lkotlin/reflect/jvm/internal/impl/i/e$e;I)I

    .line 13291
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 13426
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$e$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
