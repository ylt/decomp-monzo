.class public final Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$y$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

.field private g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private h:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 4831
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 5367
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    .line 5368
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->u()V

    .line 5369
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 4760
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 4979
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->i:B

    .line 5010
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->j:I

    .line 4761
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->u()V

    .line 4763
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v4

    .line 4765
    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v5

    .line 4769
    const/4 v2, 0x0

    .line 4770
    :goto_0
    if-nez v2, :cond_3

    .line 4771
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 4772
    sparse-switch v0, :sswitch_data_0

    .line 4777
    invoke-virtual {p0, p1, v5, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    move v2, v0

    .line 4814
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4775
    goto :goto_1

    .line 4784
    :sswitch_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->n()I

    move-result v3

    .line 4785
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    move-result-object v6

    .line 4786
    if-nez v6, :cond_0

    .line 4787
    invoke-virtual {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 4788
    invoke-virtual {v5, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    move v0, v2

    goto :goto_1

    .line 4790
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    .line 4791
    iput-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    move v0, v2

    .line 4793
    goto :goto_1

    .line 4796
    :sswitch_2
    const/4 v0, 0x0

    .line 4797
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v3, v3, 0x2

    const/4 v6, 0x2

    if-ne v3, v6, :cond_4

    .line 4798
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    move-object v3, v0

    .line 4800
    :goto_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4801
    if-eqz v3, :cond_1

    .line 4802
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 4803
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4805
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    move v0, v2

    .line 4806
    goto :goto_1

    .line 4809
    :sswitch_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    .line 4810
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->h:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_2
    move v0, v2

    goto :goto_1

    .line 4822
    :cond_3
    :try_start_1
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4826
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 4828
    :goto_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d()V

    .line 4830
    return-void

    .line 4823
    :catch_0
    move-exception v0

    .line 4826
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 4815
    :catch_1
    move-exception v0

    .line 4816
    :try_start_2
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4821
    :catchall_1
    move-exception v0

    .line 4822
    :try_start_3
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 4826
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 4828
    :goto_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d()V

    throw v0

    .line 4817
    :catch_2
    move-exception v0

    .line 4818
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 4823
    :catch_3
    move-exception v1

    .line 4826
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_4

    :catchall_2
    move-exception v0

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    :cond_4
    move-object v3, v0

    goto :goto_2

    .line 4772
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 4736
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4742
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 4979
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->i:B

    .line 5010
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->j:I

    .line 4743
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 4744
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 4736
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4745
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 4979
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->i:B

    .line 5010
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->j:I

    .line 4745
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;I)I
    .locals 0

    .prologue
    .line 4736
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->h:I

    return p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 1

    .prologue
    .line 5096
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;
    .locals 0

    .prologue
    .line 4736
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 0

    .prologue
    .line 4736
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;I)I
    .locals 0

    .prologue
    .line 4736
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 4736
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    .locals 1

    .prologue
    .line 4749
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    return-object v0
.end method

.method public static r()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 1

    .prologue
    .line 5093
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 4975
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    .line 4976
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 4977
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->h:I

    .line 4978
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4997
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->i()I

    .line 4998
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4999
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->a()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(II)V

    .line 5001
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 5002
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 5004
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 5005
    const/4 v0, 0x3

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->h:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 5007
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 5008
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4843
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 4736
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    .locals 1

    .prologue
    .line 4753
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4927
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5012
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->j:I

    .line 5013
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 5030
    :goto_0
    return v0

    .line 5015
    :cond_0
    const/4 v0, 0x0

    .line 5016
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 5017
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->a()I

    move-result v1

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5020
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 5021
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5024
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 5025
    const/4 v1, 0x3

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->h:I

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5028
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 5029
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->j:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 4736
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->s()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 4736
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->t()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4981
    iget-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->i:B

    .line 4982
    if-ne v2, v0, :cond_0

    .line 4992
    :goto_0
    return v0

    .line 4983
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 4985
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4986
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4987
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->i:B

    move v0, v1

    .line 4988
    goto :goto_0

    .line 4991
    :cond_2
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->i:B

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;
    .locals 1

    .prologue
    .line 4933
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    return-object v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 4946
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 4956
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->g:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 4965
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 4971
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->h:I

    return v0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 1

    .prologue
    .line 5094
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method public t()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;
    .locals 1

    .prologue
    .line 5098
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/i/e$y$a$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 5037
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
