.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$a;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$b;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2840
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 2841
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->n()V

    .line 2842
    return-void
.end method

.method static synthetic m()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;
    .locals 1

    .prologue
    .line 2833
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 2845
    return-void
.end method

.method private static o()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;
    .locals 1

    .prologue
    .line 2847
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;
    .locals 1

    .prologue
    .line 2944
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a:I

    .line 2945
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->b:I

    .line 2947
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;
    .locals 2

    .prologue
    .line 2892
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2901
    :goto_0
    return-object p0

    .line 2893
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2894
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->m()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    .line 2896
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2897
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->o()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    .line 2899
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 2833
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2833
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 2833
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;
    .locals 1

    .prologue
    .line 2988
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a:I

    .line 2989
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->c:I

    .line 2991
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 2833
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2833
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2912
    const/4 v2, 0x0

    .line 2914
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2919
    if-eqz v0, :cond_0

    .line 2920
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    .line 2923
    :cond_0
    return-object p0

    .line 2915
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2916
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2917
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2919
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2920
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    :cond_1
    throw v0

    .line 2919
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 2833
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2833
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2833
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;
    .locals 2

    .prologue
    .line 2860
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;
    .locals 1

    .prologue
    .line 2864
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2833
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->j()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2833
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;
    .locals 2

    .prologue
    .line 2868
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    .line 2869
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2870
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2872
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2876
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V

    .line 2877
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a:I

    .line 2878
    const/4 v1, 0x0

    .line 2879
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2882
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;I)I

    .line 2883
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2884
    or-int/lit8 v0, v0, 0x2

    .line 2886
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;I)I

    .line 2887
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;I)I

    .line 2888
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2905
    const/4 v0, 0x1

    return v0
.end method
