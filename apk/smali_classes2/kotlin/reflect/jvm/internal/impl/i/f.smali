.class public Lkotlin/reflect/jvm/internal/impl/i/f;
.super Ljava/lang/Object;
.source "SerializerExtensionProtocol.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

.field private final b:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/f;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "extensionRegistry"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "constructorAnnotation"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classAnnotation"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "functionAnnotation"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "propertyAnnotation"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enumEntryAnnotation"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compileTimeValue"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parameterAnnotation"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeAnnotation"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameterAnnotation"

    invoke-static {p10, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->f:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    iput-object p7, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    iput-object p8, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->h:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    iput-object p9, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->i:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    iput-object p10, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->j:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/protobuf/f;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->f:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method

.method public final h()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->h:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method

.method public final i()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->i:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method

.method public final j()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$f",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/f;->j:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    return-object v0
.end method
