.class public final Lkotlin/reflect/jvm/internal/impl/i/b/d;
.super Ljava/lang/Object;
.source "AnnotationAndConstantLoaderImpl.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/reflect/jvm/internal/impl/i/b/c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
        "Lkotlin/reflect/jvm/internal/impl/h/b/f",
        "<*>;",
        "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

.field private final b:Lkotlin/reflect/jvm/internal/impl/i/f;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/aa;Lkotlin/reflect/jvm/internal/impl/i/f;)V
    .locals 1

    .prologue
    const-string v0, "module"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notFoundClasses"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "protocol"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    .line 33
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/e;

    invoke-direct {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/aa;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    return-void
.end method


# virtual methods
.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->h()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/f;->c()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 37
    :goto_0
    check-cast v0, Ljava/lang/Iterable;

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 99
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 100
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 37
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 36
    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/i/e$g;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 59
    :goto_0
    check-cast v0, Ljava/lang/Iterable;

    .line 106
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 107
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 108
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 59
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 58
    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 109
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    if-eqz v0, :cond_0

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 50
    :goto_0
    if-eqz v0, :cond_3

    .line 51
    :goto_1
    check-cast v0, Ljava/lang/Iterable;

    .line 102
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 103
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 104
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 51
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lkotlin/reflect/jvm/internal/impl/b/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 47
    :cond_0
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    if-eqz v0, :cond_1

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0

    .line 48
    :cond_1
    instance-of v0, p2, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    if-eqz v0, :cond_2

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->e()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0

    .line 49
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 50
    :cond_3
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 105
    :cond_4
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;ILkotlin/reflect/jvm/internal/impl/i/e$ag;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            "I",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callableProto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->h()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p5, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 70
    :goto_0
    check-cast v0, Ljava/lang/Iterable;

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 111
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 112
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 70
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 69
    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 113
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->j()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Ljava/lang/Iterable;

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 119
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 120
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 84
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    invoke-virtual {v3, v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    :goto_0
    check-cast v0, Ljava/lang/Iterable;

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 115
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 116
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 80
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    invoke-virtual {v3, v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/h/b/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ae;",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/h/b/f",
            "<*>;"
        }
    .end annotation

    .prologue
    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "proto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expectedType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    .line 93
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->b:Lkotlin/reflect/jvm/internal/impl/i/f;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/f;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    move-result-object v0

    invoke-virtual {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 94
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/d;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    const-string v2, "value"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ae;->b()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    invoke-virtual {v1, p3, v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/h/b/f;

    move-result-object v0

    goto :goto_0
.end method
