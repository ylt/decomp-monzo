.class public final Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$q$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$q$b;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$q$c;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1238
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 1338
    const/4 v0, -0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->b:I

    .line 1418
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    .line 1239
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->o()V

    .line 1240
    return-void
.end method

.method static synthetic n()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1231
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 0

    .prologue
    .line 1243
    return-void
.end method

.method private static p()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1245
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1355
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a:I

    .line 1356
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->b:I

    .line 1358
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1435
    if-nez p1, :cond_0

    .line 1436
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1438
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a:I

    .line 1439
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    .line 1441
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 2

    .prologue
    .line 1296
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1308
    :goto_0
    return-object p0

    .line 1297
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1298
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->m()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    .line 1300
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1301
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->o()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    .line 1303
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1304
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->q()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    .line 1306
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->b(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 1231
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1231
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 1231
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 1

    .prologue
    .line 1399
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a:I

    .line 1400
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->c:I

    .line 1402
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 1231
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1231
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1323
    const/4 v2, 0x0

    .line 1325
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1330
    if-eqz v0, :cond_0

    .line 1331
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    .line 1334
    :cond_0
    return-object p0

    .line 1326
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1327
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1328
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1330
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 1331
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    :cond_1
    throw v0

    .line 1330
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 1231
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1231
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1231
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;
    .locals 2

    .prologue
    .line 1260
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    .locals 1

    .prologue
    .line 1264
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1231
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1231
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    .locals 2

    .prologue
    .line 1268
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v0

    .line 1269
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1270
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1272
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1276
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 1277
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a:I

    .line 1278
    const/4 v1, 0x0

    .line 1279
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 1282
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;I)I

    .line 1283
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1284
    or-int/lit8 v0, v0, 0x2

    .line 1286
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->b(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;I)I

    .line 1287
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 1288
    or-int/lit8 v0, v0, 0x4

    .line 1290
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;)Lkotlin/reflect/jvm/internal/impl/i/e$q$b$b;

    .line 1291
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->c(Lkotlin/reflect/jvm/internal/impl/i/e$q$b;I)I

    .line 1292
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1312
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1314
    const/4 v0, 0x0

    .line 1316
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 1379
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
