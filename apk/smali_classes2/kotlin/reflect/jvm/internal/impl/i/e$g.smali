.class public final Lkotlin/reflect/jvm/internal/impl/i/e$g;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$h;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$g;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19511
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$g$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 19783
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$g;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->c:Lkotlin/reflect/jvm/internal/impl/i/e$g;

    .line 19784
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->c:Lkotlin/reflect/jvm/internal/impl/i/e$g;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->t()V

    .line 19785
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 19465
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 19545
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->g:B

    .line 19572
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->h:I

    .line 19466
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->t()V

    .line 19468
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v2

    .line 19470
    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v3

    .line 19474
    const/4 v0, 0x0

    .line 19475
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 19476
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v4

    .line 19477
    sparse-switch v4, :sswitch_data_0

    .line 19482
    invoke-virtual {p0, p1, v3, p2, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v1

    .line 19484
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 19480
    goto :goto_0

    .line 19489
    :sswitch_1
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->e:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->e:I

    .line 19490
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->f:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 19495
    :catch_0
    move-exception v0

    .line 19496
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 19501
    :catchall_0
    move-exception v0

    .line 19502
    :try_start_2
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 19506
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 19508
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d()V

    throw v0

    .line 19502
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 19506
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 19508
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d()V

    .line 19510
    return-void

    .line 19503
    :catch_1
    move-exception v0

    .line 19506
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 19497
    :catch_2
    move-exception v0

    .line 19498
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 19503
    :catch_3
    move-exception v1

    .line 19506
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 19477
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 19440
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$g;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 19447
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 19545
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->g:B

    .line 19572
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->h:I

    .line 19448
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 19449
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 19440
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$g;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19450
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 19545
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->g:B

    .line 19572
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->h:I

    .line 19450
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$g;I)I
    .locals 0

    .prologue
    .line 19440
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->f:I

    return p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 1

    .prologue
    .line 19651
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->q()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$g;I)I
    .locals 0

    .prologue
    .line 19440
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->e:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 19440
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$g;
    .locals 1

    .prologue
    .line 19454
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->c:Lkotlin/reflect/jvm/internal/impl/i/e$g;

    return-object v0
.end method

.method public static q()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 1

    .prologue
    .line 19648
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method private t()V
    .locals 1

    .prologue
    .line 19543
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->f:I

    .line 19544
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 19561
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->i()I

    .line 19563
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v0

    .line 19565
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 19566
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->f:I

    invoke-virtual {p1, v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 19568
    :cond_0
    const/16 v1, 0xc8

    invoke-virtual {v0, v1, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 19569
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 19570
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19523
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 19440
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->n()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 19574
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->h:I

    .line 19575
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 19585
    :goto_0
    return v0

    .line 19577
    :cond_0
    const/4 v0, 0x0

    .line 19578
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 19579
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 19582
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->h()I

    move-result v1

    add-int/2addr v0, v1

    .line 19583
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 19584
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->h:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 19440
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->r()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 19440
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->s()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 19547
    iget-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->g:B

    .line 19548
    if-ne v2, v0, :cond_0

    .line 19556
    :goto_0
    return v0

    .line 19549
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 19551
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->f()Z

    move-result v2

    if-nez v2, :cond_2

    .line 19552
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->g:B

    move v0, v1

    .line 19553
    goto :goto_0

    .line 19555
    :cond_2
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->g:B

    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$g;
    .locals 1

    .prologue
    .line 19458
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->c:Lkotlin/reflect/jvm/internal/impl/i/e$g;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 19533
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 19539
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->f:I

    return v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 1

    .prologue
    .line 19649
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->q()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 1

    .prologue
    .line 19653
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->a(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 19592
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
