.class public final Lkotlin/reflect/jvm/internal/impl/i/e$ac;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$ad;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ac"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$c",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ad;"
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7054
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 7917
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    .line 7918
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->C()V

    .line 7919
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/16 v9, 0x10

    const/16 v8, 0x20

    .line 6951
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 7260
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    .line 7316
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->m:I

    .line 6952
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->C()V

    .line 6954
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v3

    .line 6956
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v4

    move v2, v0

    .line 6961
    :cond_0
    :goto_0
    if-nez v0, :cond_8

    .line 6962
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v5

    .line 6963
    sparse-switch v5, :sswitch_data_0

    .line 6968
    invoke-virtual {p0, p1, v4, p2, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 6970
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 6966
    goto :goto_0

    .line 6975
    :sswitch_1
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    .line 6976
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v5

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->f:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 7032
    :catch_0
    move-exception v0

    .line 7033
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7038
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v2, 0x10

    if-ne v1, v9, :cond_1

    .line 7039
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    .line 7041
    :cond_1
    and-int/lit8 v1, v2, 0x20

    if-ne v1, v8, :cond_2

    .line 7042
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    .line 7045
    :cond_2
    :try_start_2
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 7049
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 7051
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d()V

    throw v0

    .line 6980
    :sswitch_2
    :try_start_3
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    .line 6981
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v5

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->g:I
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 7034
    :catch_1
    move-exception v0

    .line 7035
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 6985
    :sswitch_3
    :try_start_5
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    .line 6986
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->i()Z

    move-result v5

    iput-boolean v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->h:Z

    goto :goto_0

    .line 6990
    :sswitch_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->n()I

    move-result v6

    .line 6991
    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    move-result-object v7

    .line 6992
    if-nez v7, :cond_3

    .line 6993
    invoke-virtual {v4, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 6994
    invoke-virtual {v4, v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    goto/16 :goto_0

    .line 6996
    :cond_3
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    .line 6997
    iput-object v7, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    goto/16 :goto_0

    .line 7002
    :sswitch_5
    and-int/lit8 v5, v2, 0x10

    if-eq v5, v9, :cond_4

    .line 7003
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    .line 7004
    or-int/lit8 v2, v2, 0x10

    .line 7006
    :cond_4
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v6, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 7010
    :sswitch_6
    and-int/lit8 v5, v2, 0x20

    if-eq v5, v8, :cond_5

    .line 7011
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    .line 7012
    or-int/lit8 v2, v2, 0x20

    .line 7014
    :cond_5
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 7018
    :sswitch_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->s()I

    move-result v5

    .line 7019
    invoke-virtual {p1, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->c(I)I

    move-result v5

    .line 7020
    and-int/lit8 v6, v2, 0x20

    if-eq v6, v8, :cond_6

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v6

    if-lez v6, :cond_6

    .line 7021
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    .line 7022
    or-int/lit8 v2, v2, 0x20

    .line 7024
    :cond_6
    :goto_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v6

    if-lez v6, :cond_7

    .line 7025
    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 7027
    :cond_7
    invoke-virtual {p1, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->d(I)V
    :try_end_5
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 7038
    :cond_8
    and-int/lit8 v0, v2, 0x10

    if-ne v0, v9, :cond_9

    .line 7039
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    .line 7041
    :cond_9
    and-int/lit8 v0, v2, 0x20

    if-ne v0, v8, :cond_a

    .line 7042
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    .line 7045
    :cond_a
    :try_start_6
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 7049
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 7051
    :goto_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d()V

    .line 7053
    return-void

    .line 7046
    :catch_2
    move-exception v0

    .line 7049
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_3

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 7046
    :catch_3
    move-exception v1

    .line 7049
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto/16 :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 6963
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6926
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 6933
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    .line 7260
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    .line 7316
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->m:I

    .line 6934
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 6935
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 6926
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6936
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;-><init>()V

    .line 7260
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    .line 7316
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->m:I

    .line 6936
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method private C()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7253
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->f:I

    .line 7254
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->g:I

    .line 7255
    iput-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->h:Z

    .line 7256
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    .line 7257
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    .line 7258
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    .line 7259
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;I)I
    .locals 0

    .prologue
    .line 6926
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->f:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 6926
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7420
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->z()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;
    .locals 0

    .prologue
    .line 6926
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Z)Z
    .locals 0

    .prologue
    .line 6926
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->h:Z

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$ac;I)I
    .locals 0

    .prologue
    .line 6926
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Ljava/util/List;
    .locals 1

    .prologue
    .line 6926
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 6926
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$ac;I)I
    .locals 0

    .prologue
    .line 6926
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    return p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Ljava/util/List;
    .locals 1

    .prologue
    .line 6926
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 6926
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 6940
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public static z()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7417
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7418
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->z()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public B()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7422
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 7220
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 7290
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->i()I

    .line 7292
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->g()Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;

    move-result-object v3

    .line 7294
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 7295
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 7297
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 7298
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->g:I

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 7300
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 7301
    const/4 v0, 0x3

    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->h:Z

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(IZ)V

    .line 7303
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 7304
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;->a()I

    move-result v0

    invoke-virtual {p1, v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(II)V

    :cond_3
    move v1, v2

    .line 7306
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 7307
    const/4 v4, 0x5

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 7306
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 7309
    :cond_4
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 7310
    const/4 v1, 0x6

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 7309
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7312
    :cond_5
    const/16 v0, 0x3e8

    invoke-virtual {v3, v0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c$a;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V

    .line 7313
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 7314
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7066
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 6926
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 7318
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->m:I

    .line 7319
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 7354
    :goto_0
    return v0

    .line 7322
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 7323
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->f:I

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 7326
    :goto_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 7327
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->g:I

    invoke-static {v4, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7330
    :cond_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 7331
    const/4 v2, 0x3

    iget-boolean v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->h:Z

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7334
    :cond_2
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 7335
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;->a()I

    move-result v2

    invoke-static {v5, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v0

    .line 7338
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 7339
    const/4 v4, 0x5

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v3, v0

    .line 7338
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move v2, v1

    .line 7344
    :goto_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 7345
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->g(I)I

    move-result v0

    add-int/2addr v0, v2

    .line 7344
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_3

    .line 7348
    :cond_5
    add-int v0, v3, v2

    .line 7349
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->y()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7351
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->h()I

    move-result v1

    add-int/2addr v0, v1

    .line 7352
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 7353
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->m:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto/16 :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 6926
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->A()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 6926
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->B()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 7262
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    .line 7263
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 7285
    :cond_0
    :goto_0
    return v1

    .line 7264
    :cond_1
    if-eqz v0, :cond_0

    .line 7266
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->o()Z

    move-result v0

    if-nez v0, :cond_2

    .line 7267
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    goto :goto_0

    .line 7270
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->q()Z

    move-result v0

    if-nez v0, :cond_3

    .line 7271
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    goto :goto_0

    :cond_3
    move v0, v1

    .line 7274
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->x()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 7275
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v3

    if-nez v3, :cond_4

    .line 7276
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    goto :goto_0

    .line 7274
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7280
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->f()Z

    move-result v0

    if-nez v0, :cond_6

    .line 7281
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    goto :goto_0

    .line 7284
    :cond_6
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l:B

    move v1, v2

    .line 7285
    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 6944
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 7141
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 7147
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->f:I

    return v0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 7156
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 7162
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->g:I

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 7171
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 7177
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->h:Z

    return v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 7186
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;
    .locals 1

    .prologue
    .line 7192
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->i:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    return-object v0
.end method

.method public w()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7201
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 7361
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 7214
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public y()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7237
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->k:Ljava/util/List;

    return-object v0
.end method
