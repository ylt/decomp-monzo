.class public final Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$g$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19664
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 19665
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->p()V

    .line 19666
    return-void
.end method

.method static synthetic o()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 1

    .prologue
    .line 19658
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 0

    .prologue
    .line 19669
    return-void
.end method

.method private static q()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 1

    .prologue
    .line 19671
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 1

    .prologue
    .line 19764
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a:I

    .line 19765
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->b:I

    .line 19767
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 2

    .prologue
    .line 19710
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->m()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 19717
    :goto_0
    return-object p0

    .line 19711
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19712
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    .line 19714
    :cond_1
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 19715
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->b(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 19658
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19658
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 19658
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$g;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 19658
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19658
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19732
    const/4 v2, 0x0

    .line 19734
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 19739
    if-eqz v0, :cond_0

    .line 19740
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    .line 19743
    :cond_0
    return-object p0

    .line 19735
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 19736
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 19737
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 19739
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 19740
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    :cond_1
    throw v0

    .line 19739
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 19658
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 19658
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 19658
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 19658
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 19658
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 19658
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;
    .locals 2

    .prologue
    .line 19682
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$g;)Lkotlin/reflect/jvm/internal/impl/i/e$g$a;

    move-result-object v0

    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$g;
    .locals 1

    .prologue
    .line 19686
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->m()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 19721
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 19723
    const/4 v0, 0x0

    .line 19725
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$g;
    .locals 2

    .prologue
    .line 19690
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v0

    .line 19691
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 19692
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 19694
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$g;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 19698
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$g;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$g;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 19699
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->a:I

    .line 19700
    const/4 v1, 0x0

    .line 19701
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    .line 19704
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$g$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->a(Lkotlin/reflect/jvm/internal/impl/i/e$g;I)I

    .line 19705
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->b(Lkotlin/reflect/jvm/internal/impl/i/e$g;I)I

    .line 19706
    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method
