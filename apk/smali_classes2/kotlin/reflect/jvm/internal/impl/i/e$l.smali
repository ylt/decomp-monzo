.class public final enum Lkotlin/reflect/jvm/internal/impl/i/e$l;
.super Ljava/lang/Enum;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "l"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$l;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/protobuf/i$a;"
    }
.end annotation


# static fields
.field public static final enum a:Lkotlin/reflect/jvm/internal/impl/i/e$l;

.field public static final enum b:Lkotlin/reflect/jvm/internal/impl/i/e$l;

.field public static final enum c:Lkotlin/reflect/jvm/internal/impl/i/e$l;

.field public static final enum d:Lkotlin/reflect/jvm/internal/impl/i/e$l;

.field private static e:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/i$b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$l;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic g:[Lkotlin/reflect/jvm/internal/impl/i/e$l;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    const-string v1, "FINAL"

    invoke-direct {v0, v1, v2, v2, v2}, Lkotlin/reflect/jvm/internal/impl/i/e$l;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->a:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    .line 27
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v3, v3, v3}, Lkotlin/reflect/jvm/internal/impl/i/e$l;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->b:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    .line 31
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    const-string v1, "ABSTRACT"

    invoke-direct {v0, v1, v4, v4, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$l;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->c:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    const-string v1, "SEALED"

    invoke-direct {v0, v1, v5, v5, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$l;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->d:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    .line 14
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/jvm/internal/impl/i/e$l;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$l;->a:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    aput-object v1, v0, v2

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$l;->b:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    aput-object v1, v0, v3

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$l;->c:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    aput-object v1, v0, v4

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$l;->d:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    aput-object v1, v0, v5

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->g:[Lkotlin/reflect/jvm/internal/impl/i/e$l;

    .line 77
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$l$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$l$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 87
    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->f:I

    .line 88
    return-void
.end method

.method public static a(I)Lkotlin/reflect/jvm/internal/impl/i/e$l;
    .locals 1

    .prologue
    .line 63
    packed-switch p0, :pswitch_data_0

    .line 68
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->a:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    goto :goto_0

    .line 65
    :pswitch_1
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->b:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    goto :goto_0

    .line 66
    :pswitch_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->c:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    goto :goto_0

    .line 67
    :pswitch_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->d:Lkotlin/reflect/jvm/internal/impl/i/e$l;

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/i/e$l;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    return-object v0
.end method

.method public static values()[Lkotlin/reflect/jvm/internal/impl/i/e$l;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->g:[Lkotlin/reflect/jvm/internal/impl/i/e$l;

    invoke-virtual {v0}, [Lkotlin/reflect/jvm/internal/impl/i/e$l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkotlin/reflect/jvm/internal/impl/i/e$l;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$l;->f:I

    return v0
.end method
