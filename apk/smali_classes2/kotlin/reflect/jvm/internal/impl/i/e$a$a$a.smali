.class public final Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$a$a;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$a$b;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3672
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 3803
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 3673
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->q()V

    .line 3674
    return-void
.end method

.method static synthetic p()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 1

    .prologue
    .line 3665
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 0

    .prologue
    .line 3677
    return-void
.end method

.method private static r()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 1

    .prologue
    .line 3679
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 1

    .prologue
    .line 3788
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a:I

    .line 3789
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->b:I

    .line 3791
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 2

    .prologue
    .line 3842
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 3844
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 3850
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a:I

    .line 3851
    return-object p0

    .line 3847
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 2

    .prologue
    .line 3724
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 3733
    :goto_0
    return-object p0

    .line 3725
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3726
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->m()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    .line 3728
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3729
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    .line 3731
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 3665
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3665
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 3665
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 3665
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3665
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3756
    const/4 v2, 0x0

    .line 3758
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3763
    if-eqz v0, :cond_0

    .line 3764
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    .line 3767
    :cond_0
    return-object p0

    .line 3759
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 3760
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3761
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3763
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 3764
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    :cond_1
    throw v0

    .line 3763
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 3665
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3665
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 3665
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;
    .locals 2

    .prologue
    .line 3692
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
    .locals 1

    .prologue
    .line 3696
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 3665
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 3665
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
    .locals 2

    .prologue
    .line 3700
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v0

    .line 3701
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3702
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 3704
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3708
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 3709
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a:I

    .line 3710
    const/4 v1, 0x0

    .line 3711
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 3714
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;I)I

    .line 3715
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 3716
    or-int/lit8 v0, v0, 0x2

    .line 3718
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 3719
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a;I)I

    .line 3720
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3737
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->m()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3749
    :cond_0
    :goto_0
    return v0

    .line 3741
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3745
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3749
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3776
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 3808
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 1

    .prologue
    .line 3814
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    return-object v0
.end method
