.class public final Lkotlin/reflect/jvm/internal/impl/i/b/p;
.super Ljava/lang/Object;
.source "DeserializedClassDataFinder.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/h;


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/b/z;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/z;)V
    .locals 1

    .prologue
    const-string v0, "packageFragmentProvider"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/p;->a:Lkotlin/reflect/jvm/internal/impl/b/z;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/i/b;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/p;->a:Lkotlin/reflect/jvm/internal/impl/b/z;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    const-string v3, "classId.packageFqName"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/b/z;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->j(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/i/b/q;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/q;

    if-eqz v0, :cond_1

    .line 28
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/q;->b()Lkotlin/reflect/jvm/internal/impl/i/b/h;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/h;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/i/b;

    move-result-object v1

    :cond_1
    return-object v1
.end method
