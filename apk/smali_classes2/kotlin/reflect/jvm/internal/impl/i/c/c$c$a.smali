.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$c;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2378
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 2379
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->n()V

    .line 2380
    return-void
.end method

.method static synthetic m()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 1

    .prologue
    .line 2371
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 2383
    return-void
.end method

.method private static o()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 1

    .prologue
    .line 2385
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 1

    .prologue
    .line 2482
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a:I

    .line 2483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->b:I

    .line 2485
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 2

    .prologue
    .line 2430
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2439
    :goto_0
    return-object p0

    .line 2431
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->m()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    .line 2434
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2435
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->o()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    .line 2437
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 2371
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2371
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 2371
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 1

    .prologue
    .line 2526
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a:I

    .line 2527
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->c:I

    .line 2529
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 2371
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2371
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2450
    const/4 v2, 0x0

    .line 2452
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2457
    if-eqz v0, :cond_0

    .line 2458
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    .line 2461
    :cond_0
    return-object p0

    .line 2453
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2454
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2455
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2457
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2458
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    :cond_1
    throw v0

    .line 2457
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 2371
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2371
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2371
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 2

    .prologue
    .line 2398
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 1

    .prologue
    .line 2402
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2371
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->j()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2371
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 2

    .prologue
    .line 2406
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    .line 2407
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2408
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2410
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2414
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V

    .line 2415
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a:I

    .line 2416
    const/4 v1, 0x0

    .line 2417
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2420
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;I)I

    .line 2421
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2422
    or-int/lit8 v0, v0, 0x2

    .line 2424
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;I)I

    .line 2425
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;I)I

    .line 2426
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2443
    const/4 v0, 0x1

    return v0
.end method
