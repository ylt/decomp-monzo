.class public final Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "BuiltInsProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/a/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/a/a$a;",
        "Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/a/a$b;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lkotlin/reflect/jvm/internal/impl/i/e$w;

.field private c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

.field private d:Lkotlin/reflect/jvm/internal/impl/i/e$m;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 436
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 567
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    .line 627
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    .line 687
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->m()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    .line 747
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    .line 437
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->s()V

    .line 438
    return-void
.end method

.method static synthetic r()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 1

    .prologue
    .line 429
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->t()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 0

    .prologue
    .line 441
    return-void
.end method

.method private static t()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 1

    .prologue
    .line 443
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;-><init>()V

    return-object v0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 750
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 751
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    .line 752
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    .line 754
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 2

    .prologue
    .line 501
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 523
    :goto_0
    return-object p0

    .line 502
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    .line 505
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 506
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    .line 508
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 509
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    .line 511
    :cond_3
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->b(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 512
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 513
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->b(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    .line 514
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    .line 521
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->c(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0

    .line 516
    :cond_5
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->u()V

    .line 517
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->b(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 2

    .prologue
    .line 726
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->m()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 728
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    .line 734
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    .line 735
    return-object p0

    .line 731
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 2

    .prologue
    .line 666
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 668
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    .line 674
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    .line 675
    return-object p0

    .line 671
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 2

    .prologue
    .line 606
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 608
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    .line 614
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    .line 615
    return-object p0

    .line 611
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    goto :goto_0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$c;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c;

    return-object v0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 429
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 552
    const/4 v2, 0x0

    .line 554
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 559
    if-eqz v0, :cond_0

    .line 560
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    .line 563
    :cond_0
    return-object p0

    .line 555
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 556
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 557
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 559
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 560
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    :cond_1
    throw v0

    .line 559
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->g()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;
    .locals 2

    .prologue
    .line 460
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->t()Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;)Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    .locals 1

    .prologue
    .line 464
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->f()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->j()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    .locals 2

    .prologue
    .line 468
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    move-result-object v0

    .line 469
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 470
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 472
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/a/a$a;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 476
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/a/a$1;)V

    .line 477
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    .line 478
    const/4 v1, 0x0

    .line 479
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 482
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w;

    .line 483
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 484
    or-int/lit8 v0, v0, 0x2

    .line 486
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q;

    .line 487
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 488
    or-int/lit8 v0, v0, 0x4

    .line 490
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Lkotlin/reflect/jvm/internal/impl/i/e$m;)Lkotlin/reflect/jvm/internal/impl/i/e$m;

    .line 491
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 492
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    .line 493
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    .line 495
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;Ljava/util/List;)Ljava/util/List;

    .line 496
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/a/a$a;I)I

    .line 497
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 527
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 528
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 545
    :cond_0
    :goto_0
    return v1

    .line 533
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 534
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$m;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    move v0, v1

    .line 539
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->q()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 540
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 539
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 545
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 632
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$q;
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 692
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/i/e$m;
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$m;

    return-object v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 766
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a/a$a$a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
