.class public final Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$ad;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$ac;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ad;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Z

.field private e:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 7433
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 7687
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    .line 7722
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    .line 7847
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    .line 7434
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->s()V

    .line 7435
    return-void
.end method

.method static synthetic r()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7427
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->t()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 0

    .prologue
    .line 7438
    return-void
.end method

.method private static t()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7440
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;-><init>()V

    return-object v0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 7725
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 7726
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    .line 7727
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7729
    :cond_0
    return-void
.end method

.method private v()V
    .locals 2

    .prologue
    .line 7849
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 7850
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    .line 7851
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7853
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7608
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7609
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->b:I

    .line 7611
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7704
    if-nez p1, :cond_0

    .line 7705
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7707
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7708
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    .line 7710
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 2

    .prologue
    .line 7511
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 7547
    :goto_0
    return-object p0

    .line 7512
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7513
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    .line 7515
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7516
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->r()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    .line 7518
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7519
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->t()Z

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Z)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    .line 7521
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 7522
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->v()Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    .line 7524
    :cond_4
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 7525
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 7526
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    .line 7527
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7534
    :cond_5
    :goto_1
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->c(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 7535
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 7536
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->c(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    .line 7537
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7544
    :cond_6
    :goto_2
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 7545
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->d(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 7529
    :cond_7
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->u()V

    .line 7530
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 7539
    :cond_8
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->v()V

    .line 7540
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->c(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public a(Z)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7672
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7673
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->d:Z

    .line 7675
    return-object p0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 7427
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7427
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 7427
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 1

    .prologue
    .line 7640
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7641
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->c:I

    .line 7643
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 7427
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7427
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7576
    const/4 v2, 0x0

    .line 7578
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 7583
    if-eqz v0, :cond_0

    .line 7584
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    .line 7587
    :cond_0
    return-object p0

    .line 7579
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 7580
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 7581
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7583
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 7584
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    :cond_1
    throw v0

    .line 7583
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 7747
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 7427
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7427
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 7427
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 7427
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 7427
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 7427
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;
    .locals 2

    .prologue
    .line 7461
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->t()Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;

    move-result-object v0

    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 7465
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7551
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 7569
    :cond_0
    :goto_0
    return v1

    .line 7555
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 7559
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->q()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 7560
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7559
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7565
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7569
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 2

    .prologue
    .line 7469
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v0

    .line 7470
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7471
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 7473
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 7477
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 7478
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7479
    const/4 v1, 0x0

    .line 7480
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 7483
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;I)I

    .line 7484
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 7485
    or-int/lit8 v0, v0, 0x2

    .line 7487
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ac;I)I

    .line 7488
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 7489
    or-int/lit8 v0, v0, 0x4

    .line 7491
    :cond_1
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->d:Z

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Z)Z

    .line 7492
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 7493
    or-int/lit8 v0, v0, 0x8

    .line 7495
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;)Lkotlin/reflect/jvm/internal/impl/i/e$ac$b;

    .line 7496
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 7497
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    .line 7498
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7500
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Ljava/util/List;)Ljava/util/List;

    .line 7501
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 7502
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    .line 7503
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    .line 7505
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->g:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Ljava/util/List;)Ljava/util/List;

    .line 7506
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->c(Lkotlin/reflect/jvm/internal/impl/i/e$ac;I)I

    .line 7507
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 7596
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 7628
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 7741
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ac$a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
