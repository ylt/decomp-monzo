.class public final Lkotlin/reflect/jvm/internal/impl/i/a;
.super Ljava/lang/Object;
.source "ProtoDatas.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/i/b/x;

.field private final b:Lkotlin/reflect/jvm/internal/impl/i/e$c;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/e$c;)V
    .locals 1

    .prologue
    const-string v0, "nameResolver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classProto"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/i/b/x;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/i/e$c;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/a;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/a;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/i/a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    iget-object v1, p1, Lkotlin/reflect/jvm/internal/impl/i/a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClassData(nameResolver="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->a:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", classProto="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/a;->b:Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
