.class public final Lkotlin/reflect/jvm/internal/impl/i/b/j;
.super Ljava/lang/Object;
.source "ClassDeserializer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/b/j$a;,
        Lkotlin/reflect/jvm/internal/impl/i/b/j$b;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/i/b/j$b;

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/b/j$a;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/reflect/jvm/internal/impl/i/b/m;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/j$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/j$b;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->a:Lkotlin/reflect/jvm/internal/impl/i/b/j$b;

    .line 90
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->c:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/ah;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->d:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/m;)V
    .locals 2

    .prologue
    const-string v0, "components"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    .line 28
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/j$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/j$c;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/j;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    check-cast v0, Lkotlin/d/a/b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->b:Lkotlin/d/a/b;

    return-void
.end method

.method public static final synthetic a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->d:Ljava/util/Set;

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/b/j$a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 36
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/j$a;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v3

    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->l()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/i;

    .line 38
    invoke-interface {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/i;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v5, v0

    .line 73
    :cond_1
    :goto_0
    return-object v5

    .line 40
    :cond_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->a:Lkotlin/reflect/jvm/internal/impl/i/b/j$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/j$b;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/j$a;->b()Lkotlin/reflect/jvm/internal/impl/i/b;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 43
    :goto_1
    if-eqz v0, :cond_1

    .line 44
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b;->a()Lkotlin/reflect/jvm/internal/impl/i/a;

    move-result-object v4

    .line 42
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b;->b()Lkotlin/reflect/jvm/internal/impl/b/al;

    move-result-object v6

    .line 45
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/a;->a()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v2

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/a;->b()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v7

    .line 47
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 48
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->e()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    const-string v1, "classId.outerClassId"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-static {p0, v0, v5, v1, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/j;->a(Lkotlin/reflect/jvm/internal/impl/i/b/j;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    if-nez v1, :cond_3

    move-object v0, v5

    :cond_3
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    if-eqz v0, :cond_1

    .line 51
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->c()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    const-string v3, "classId.shortClassName"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    move-object v1, v0

    .line 73
    :goto_2
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-direct {v0, v1, v7, v2, v6}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/i/e$c;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    move-object v5, v0

    goto :goto_0

    .line 43
    :cond_4
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->e()Lkotlin/reflect/jvm/internal/impl/i/b/h;

    move-result-object v0

    invoke-interface {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/h;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/i/b;

    move-result-object v0

    goto :goto_1

    .line 56
    :cond_5
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->g()Lkotlin/reflect/jvm/internal/impl/b/z;

    move-result-object v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v4

    const-string v8, "classId.packageFqName"

    invoke-static {v4, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v4}, Lkotlin/reflect/jvm/internal/impl/b/z;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/List;

    move-result-object v4

    .line 57
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_3
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_7

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "There should be exactly one package: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", class id is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 59
    :cond_7
    invoke-static {v4}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/y;

    .line 60
    instance-of v0, v1, Lkotlin/reflect/jvm/internal/impl/i/b/q;

    if-eqz v0, :cond_8

    move-object v0, v1

    .line 62
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/q;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/a;->c()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    const-string v4, "classId.shortClassName"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/q;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    :cond_8
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->M()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v4

    const-string v8, "classProto.typeTable"

    invoke-static {v4, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;-><init>(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)V

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/b/a/n;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/n$a;

    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->Q()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v8

    const-string v9, "classProto.sinceKotlinInfoTable"

    invoke-static {v8, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Lkotlin/reflect/jvm/internal/impl/i/b/a/n$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->a(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/j;Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b;

    :goto_0
    invoke-virtual {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/j;->a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/j;Lkotlin/reflect/jvm/internal/impl/i/b/j$a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/j;->a(Lkotlin/reflect/jvm/internal/impl/i/b/j$a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 2

    .prologue
    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/j;->b:Lkotlin/d/a/b;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/j$a;

    invoke-direct {v1, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/j$a;-><init>(Lkotlin/reflect/jvm/internal/impl/e/a;Lkotlin/reflect/jvm/internal/impl/i/b;)V

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method
