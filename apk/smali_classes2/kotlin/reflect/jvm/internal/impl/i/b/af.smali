.class public final Lkotlin/reflect/jvm/internal/impl/i/b/af;
.super Ljava/lang/Object;
.source "protoTypeTableUtil.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$ac;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ah;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->w()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 111
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->y()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 112
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 113
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 114
    check-cast v0, Ljava/lang/Integer;

    .line 43
    const-string v3, "it"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 43
    check-cast v1, Ljava/util/Collection;

    move-object v0, v1

    :cond_1
    const-string v1, "upperBoundList.ifEmpty {\u2026t.map { typeTable[it] } }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$c;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ah;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->w()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 106
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 23
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->y()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 107
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 108
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 109
    check-cast v0, Ljava/lang/Integer;

    .line 23
    const-string v3, "it"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 23
    check-cast v1, Ljava/util/Collection;

    move-object v0, v1

    :cond_1
    const-string v1, "supertypeList.ifEmpty { \u2026t.map { typeTable[it] } }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->t()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    const-string v1, "type"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->v()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    const-string v1, "returnType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->x()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    const-string v1, "returnType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->x()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 27
    :goto_0
    return-object v0

    .line 29
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->q()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0

    .line 30
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    .line 37
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->x()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0

    .line 38
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->x()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    const-string v1, "underlyingType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->A()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->A()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->x()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 81
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->z()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->B()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    .line 55
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->D()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0

    .line 56
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->B()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .line 69
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->D()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->H()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    .line 89
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->I()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->J()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->B()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->z()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    const-string v1, "expandedType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->L()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    .line 97
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->N()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    goto :goto_0

    .line 98
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
