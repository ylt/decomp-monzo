.class public final Lkotlin/reflect/jvm/internal/impl/i/e$ae;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$af;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ae"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ae;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$ae;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12351
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 12837
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 12838
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->s()V

    .line 12839
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 12294
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 12431
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->h:B

    .line 12459
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->i:I

    .line 12295
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->s()V

    .line 12297
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v3

    .line 12299
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v4

    move v2, v0

    .line 12304
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 12305
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v5

    .line 12306
    sparse-switch v5, :sswitch_data_0

    .line 12311
    invoke-virtual {p0, p1, v4, p2, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 12313
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 12309
    goto :goto_0

    .line 12318
    :sswitch_1
    and-int/lit8 v5, v2, 0x1

    if-eq v5, v1, :cond_1

    .line 12319
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    .line 12320
    or-int/lit8 v2, v2, 0x1

    .line 12322
    :cond_1
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v6, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 12332
    :catch_0
    move-exception v0

    .line 12333
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12338
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_2

    .line 12339
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    .line 12342
    :cond_2
    :try_start_2
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 12346
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 12348
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d()V

    throw v0

    .line 12326
    :sswitch_2
    :try_start_3
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->e:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->e:I

    .line 12327
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v5

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->g:I
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 12334
    :catch_1
    move-exception v0

    .line 12335
    :try_start_4
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 12338
    :cond_3
    and-int/lit8 v0, v2, 0x1

    if-ne v0, v1, :cond_4

    .line 12339
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    .line 12342
    :cond_4
    :try_start_5
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 12346
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 12348
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d()V

    .line 12350
    return-void

    .line 12343
    :catch_2
    move-exception v0

    .line 12346
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 12343
    :catch_3
    move-exception v1

    .line 12346
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 12306
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12270
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12276
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 12431
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->h:B

    .line 12459
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->i:I

    .line 12277
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 12278
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 12270
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 12279
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 12431
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->h:B

    .line 12459
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->i:I

    .line 12279
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;I)I
    .locals 0

    .prologue
    .line 12270
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->g:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 12270
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 1

    .prologue
    .line 12541
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->p()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$ae;I)I
    .locals 0

    .prologue
    .line 12270
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->e:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Ljava/util/List;
    .locals 1

    .prologue
    .line 12270
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 12270
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 1

    .prologue
    .line 12283
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object v0
.end method

.method public static p()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 1

    .prologue
    .line 12538
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 12428
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    .line 12429
    const/4 v0, -0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->g:I

    .line 12430
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 12392
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 12449
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->i()I

    .line 12450
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 12451
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 12450
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 12453
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 12454
    const/4 v0, 0x2

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->g:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 12456
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 12457
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12363
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 12270
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->g()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 1

    .prologue
    .line 12287
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->c:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12373
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    return-object v0
.end method

.method public i()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 12461
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->i:I

    .line 12462
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 12475
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 12465
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 12466
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v2, v0

    .line 12465
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 12469
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 12470
    const/4 v0, 0x2

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->g:I

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 12473
    :cond_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v0

    add-int/2addr v0, v2

    .line 12474
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->i:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 12270
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->q()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 12270
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->r()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 12433
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->h:B

    .line 12434
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 12444
    :cond_0
    :goto_0
    return v1

    .line 12435
    :cond_1
    if-eqz v0, :cond_0

    move v0, v1

    .line 12437
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->m()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 12438
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v3

    if-nez v3, :cond_2

    .line 12439
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->h:B

    goto :goto_0

    .line 12437
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 12443
    :cond_3
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->h:B

    move v1, v2

    .line 12444
    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 12386
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public n()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 12413
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 12424
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->g:I

    return v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 1

    .prologue
    .line 12539
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->p()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 1

    .prologue
    .line 12543
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 12482
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
