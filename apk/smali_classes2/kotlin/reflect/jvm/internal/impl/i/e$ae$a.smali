.class public final Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$af;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ae;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$af;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 12555
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 12656
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    .line 12781
    const/4 v0, -0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->c:I

    .line 12556
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->o()V

    .line 12557
    return-void
.end method

.method static synthetic n()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 1

    .prologue
    .line 12548
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 0

    .prologue
    .line 12560
    return-void
.end method

.method private static p()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 1

    .prologue
    .line 12562
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;-><init>()V

    return-object v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 12659
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 12660
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    .line 12661
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    .line 12663
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 2

    .prologue
    .line 12608
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 12624
    :goto_0
    return-object p0

    .line 12609
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 12610
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 12611
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    .line 12612
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    .line 12619
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 12620
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->o()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    .line 12622
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->c(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0

    .line 12614
    :cond_3
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->q()V

    .line 12615
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 12681
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 12548
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12548
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 12548
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 1

    .prologue
    .line 12813
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    .line 12814
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->c:I

    .line 12816
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 12548
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12548
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12641
    const/4 v2, 0x0

    .line 12643
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 12648
    if-eqz v0, :cond_0

    .line 12649
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    .line 12652
    :cond_0
    return-object p0

    .line 12644
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 12645
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 12646
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 12648
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 12649
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    :cond_1
    throw v0

    .line 12648
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 12548
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 12548
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 12548
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;
    .locals 2

    .prologue
    .line 12575
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 1

    .prologue
    .line 12579
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 12548
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 12548
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 2

    .prologue
    .line 12583
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    .line 12584
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 12585
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 12587
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 12591
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 12592
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    .line 12593
    const/4 v1, 0x0

    .line 12594
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 12595
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    .line 12596
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a:I

    .line 12598
    :cond_0
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    invoke-static {v2, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;Ljava/util/List;)Ljava/util/List;

    .line 12599
    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 12602
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;I)I

    .line 12603
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ae;I)I

    .line 12604
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 12628
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->m()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 12629
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v2

    if-nez v2, :cond_0

    .line 12634
    :goto_1
    return v1

    .line 12628
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12634
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public m()I
    .locals 1

    .prologue
    .line 12675
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
