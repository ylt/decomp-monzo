.class public final Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$ab;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$y$c;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ab;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y$a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:I

.field private e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private l:I

.field private m:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private n:I

.field private o:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5899
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 6131
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    .line 6340
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 6612
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 6704
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 5900
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->w()V

    .line 5901
    return-void
.end method

.method static synthetic v()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 5893
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->x()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .locals 0

    .prologue
    .line 5904
    return-void
.end method

.method private static x()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 5906
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;-><init>()V

    return-object v0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 6134
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 6135
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    .line 6136
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6138
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$a;
    .locals 1

    .prologue
    .line 6156
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 2

    .prologue
    .line 6024
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 6077
    :goto_0
    return-object p0

    .line 6025
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6026
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 6027
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    .line 6028
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6035
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6036
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r()Z

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Z)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6038
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6039
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6041
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6042
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6044
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->w()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6045
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->x()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6047
    :cond_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 6048
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->z()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->d(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6050
    :cond_6
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->A()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 6051
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->B()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->e(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6053
    :cond_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->C()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 6054
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->D()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->f(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6056
    :cond_8
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->E()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 6057
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->F()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->g(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6059
    :cond_9
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->G()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 6060
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->H()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->c(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6062
    :cond_a
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->I()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 6063
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->J()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->h(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6065
    :cond_b
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->K()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 6066
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->L()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->d(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6068
    :cond_c
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->M()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 6069
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->N()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->i(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6071
    :cond_d
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->O()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 6072
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->P()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->j(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6074
    :cond_e
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 6075
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->c(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 6030
    :cond_f
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->y()V

    .line 6031
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1
.end method

.method public a(Z)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6273
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6274
    iput-boolean p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->c:Z

    .line 6276
    return-object p0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 5893
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->j()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5893
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 5893
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6320
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6321
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->d:I

    .line 6323
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 2

    .prologue
    .line 6379
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 6381
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 6387
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6388
    return-object p0

    .line 6384
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 5893
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->j()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5893
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6417
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6418
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->f:I

    .line 6420
    return-object p0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 2

    .prologue
    .line 6651
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 6653
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 6659
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6660
    return-object p0

    .line 6656
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6116
    const/4 v2, 0x0

    .line 6118
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 6123
    if-eqz v0, :cond_0

    .line 6124
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    .line 6127
    :cond_0
    return-object p0

    .line 6119
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 6120
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 6121
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 6123
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 6124
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    :cond_1
    throw v0

    .line 6123
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 5893
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 5893
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->j()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6449
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6450
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->g:I

    .line 6452
    return-object p0
.end method

.method public d(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 2

    .prologue
    .line 6743
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 6745
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 6751
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6752
    return-object p0

    .line 6748
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public e(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6493
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6494
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->h:I

    .line 6496
    return-object p0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 5893
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    return-object v0
.end method

.method public f(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6541
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6542
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->i:I

    .line 6544
    return-object p0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 5893
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->j()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public g(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6592
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6593
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->j:I

    .line 6595
    return-object p0
.end method

.method public h(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6689
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6690
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->l:I

    .line 6692
    return-object p0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 5893
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    return-object v0
.end method

.method public i(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6781
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6782
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n:I

    .line 6784
    return-object p0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 5893
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 2

    .prologue
    .line 5943
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->x()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    return-object v0
.end method

.method public j(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;
    .locals 1

    .prologue
    .line 6825
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 6826
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->o:I

    .line 6828
    return-object p0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 5947
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6081
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->o()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 6082
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->l()Z

    move-result v2

    if-nez v2, :cond_1

    .line 6109
    :cond_0
    :goto_1
    return v1

    .line 6081
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6087
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6088
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->q()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6093
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6094
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->s()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6099
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6100
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->u()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6105
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6109
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 2

    .prologue
    .line 5951
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 5952
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5953
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 5955
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 5959
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 5960
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 5961
    const/4 v1, 0x0

    .line 5962
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 5963
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    .line 5964
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    .line 5966
    :cond_0
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    invoke-static {v2, v4}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Ljava/util/List;)Ljava/util/List;

    .line 5967
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_d

    .line 5970
    :goto_0
    iget-boolean v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->c:Z

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Z)Z

    .line 5971
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 5972
    or-int/lit8 v0, v0, 0x2

    .line 5974
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->d:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 5975
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 5976
    or-int/lit8 v0, v0, 0x4

    .line 5978
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 5979
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 5980
    or-int/lit8 v0, v0, 0x8

    .line 5982
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 5983
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 5984
    or-int/lit8 v0, v0, 0x10

    .line 5986
    :cond_4
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->g:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->c(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 5987
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 5988
    or-int/lit8 v0, v0, 0x20

    .line 5990
    :cond_5
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->h:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->d(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 5991
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 5992
    or-int/lit8 v0, v0, 0x40

    .line 5994
    :cond_6
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->i:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->e(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 5995
    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 5996
    or-int/lit16 v0, v0, 0x80

    .line 5998
    :cond_7
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->j:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->f(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 5999
    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 6000
    or-int/lit16 v0, v0, 0x100

    .line 6002
    :cond_8
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 6003
    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    .line 6004
    or-int/lit16 v0, v0, 0x200

    .line 6006
    :cond_9
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->l:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->g(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 6007
    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    .line 6008
    or-int/lit16 v0, v0, 0x400

    .line 6010
    :cond_a
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->c(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 6011
    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    .line 6012
    or-int/lit16 v0, v0, 0x800

    .line 6014
    :cond_b
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->h(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 6015
    and-int/lit16 v1, v3, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_c

    .line 6016
    or-int/lit16 v0, v0, 0x1000

    .line 6018
    :cond_c
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->o:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->i(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 6019
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->j(Lkotlin/reflect/jvm/internal/impl/i/e$y;I)I

    .line 6020
    return-object v2

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 6150
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 6345
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 6351
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 6617
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 6623
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->k:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 6709
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 6715
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method
