.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$c;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2202
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 2549
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 2550
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->s()V

    .line 2551
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2151
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 2260
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->h:B

    .line 2282
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->i:I

    .line 2152
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->s()V

    .line 2154
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v2

    .line 2156
    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v3

    .line 2160
    const/4 v0, 0x0

    .line 2161
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2162
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v4

    .line 2163
    sparse-switch v4, :sswitch_data_0

    .line 2168
    invoke-virtual {p0, p1, v3, p2, v4}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v1

    .line 2170
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2166
    goto :goto_0

    .line 2175
    :sswitch_1
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    .line 2176
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2186
    :catch_0
    move-exception v0

    .line 2187
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2192
    :catchall_0
    move-exception v0

    .line 2193
    :try_start_2
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2197
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2199
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d()V

    throw v0

    .line 2180
    :sswitch_2
    :try_start_3
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    .line 2181
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v4

    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->g:I
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2188
    :catch_1
    move-exception v0

    .line 2189
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2193
    :cond_1
    :try_start_5
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2197
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2199
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d()V

    .line 2201
    return-void

    .line 2194
    :catch_2
    move-exception v0

    .line 2197
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 2194
    :catch_3
    move-exception v1

    .line 2197
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 2163
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2127
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2133
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 2260
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->h:B

    .line 2282
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->i:I

    .line 2134
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2135
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V
    .locals 0

    .prologue
    .line 2127
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2136
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 2260
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->h:B

    .line 2282
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->i:I

    .line 2136
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;I)I
    .locals 0

    .prologue
    .line 2127
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f:I

    return p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 1

    .prologue
    .line 2364
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->p()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;I)I
    .locals 0

    .prologue
    .line 2127
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 2127
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;I)I
    .locals 0

    .prologue
    .line 2127
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    return p1
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 1

    .prologue
    .line 2140
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    return-object v0
.end method

.method public static p()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 1

    .prologue
    .line 2361
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->m()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2257
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f:I

    .line 2258
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->g:I

    .line 2259
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2272
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->i()I

    .line 2273
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2274
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 2276
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2277
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->g:I

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 2279
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 2280
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2214
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2127
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 1

    .prologue
    .line 2144
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2224
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2284
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->i:I

    .line 2285
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2298
    :goto_0
    return v0

    .line 2287
    :cond_0
    const/4 v0, 0x0

    .line 2288
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2289
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2292
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2293
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->g:I

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2296
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 2297
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->i:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 2127
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->q()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 2127
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->r()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2262
    iget-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->h:B

    .line 2263
    if-ne v1, v0, :cond_0

    .line 2267
    :goto_0
    return v0

    .line 2264
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2266
    :cond_1
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->h:B

    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 2230
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f:I

    return v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 2243
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 2253
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->g:I

    return v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 1

    .prologue
    .line 2362
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->p()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;
    .locals 1

    .prologue
    .line 2366
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 2305
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
