.class public final Lkotlin/reflect/jvm/internal/impl/i/e$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$a$c;,
        Lkotlin/reflect/jvm/internal/impl/i/e$a$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$a$b;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$a;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a;",
            ">;"
        }
    .end annotation
.end field

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1956
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 4323
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 4324
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->s()V

    .line 4325
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v7, 0x2

    .line 1899
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 3929
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->h:B

    .line 3961
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->i:I

    .line 1900
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->s()V

    .line 1902
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v3

    .line 1904
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v4

    move v2, v0

    .line 1909
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 1910
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v5

    .line 1911
    sparse-switch v5, :sswitch_data_0

    .line 1916
    invoke-virtual {p0, p1, v4, p2, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 1918
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1914
    goto :goto_0

    .line 1923
    :sswitch_1
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->e:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->e:I

    .line 1924
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v5

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1937
    :catch_0
    move-exception v0

    .line 1938
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1943
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v2, 0x2

    if-ne v1, v7, :cond_1

    .line 1944
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    .line 1947
    :cond_1
    :try_start_2
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1951
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 1953
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d()V

    throw v0

    .line 1928
    :sswitch_2
    and-int/lit8 v5, v2, 0x2

    if-eq v5, v7, :cond_2

    .line 1929
    :try_start_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    .line 1930
    or-int/lit8 v2, v2, 0x2

    .line 1932
    :cond_2
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v6, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1939
    :catch_1
    move-exception v0

    .line 1940
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1943
    :cond_3
    and-int/lit8 v0, v2, 0x2

    if-ne v0, v7, :cond_4

    .line 1944
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    .line 1947
    :cond_4
    :try_start_5
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1951
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 1953
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d()V

    .line 1955
    return-void

    .line 1948
    :catch_2
    move-exception v0

    .line 1951
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 1948
    :catch_3
    move-exception v1

    .line 1951
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 1911
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1875
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1881
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 3929
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->h:B

    .line 3961
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->i:I

    .line 1882
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 1883
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 1875
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1884
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 3929
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->h:B

    .line 3961
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->i:I

    .line 1884
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a;I)I
    .locals 0

    .prologue
    .line 1875
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 1875
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$c;
    .locals 1

    .prologue
    .line 4043
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$a;I)I
    .locals 0

    .prologue
    .line 1875
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->e:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1875
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 1875
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$a;
    .locals 1

    .prologue
    .line 1888
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    return-object v0
.end method

.method public static p()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;
    .locals 1

    .prologue
    .line 4040
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$c;->o()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 3926
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f:I

    .line 3927
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    .line 3928
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
    .locals 1

    .prologue
    .line 3915
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 3951
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->i()I

    .line 3952
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3953
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 3955
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 3956
    const/4 v2, 0x2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 3955
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3958
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 3959
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1968
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1875
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$a;
    .locals 1

    .prologue
    .line 1892
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3881
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3963
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->i:I

    .line 3964
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3977
    :goto_0
    return v0

    .line 3967
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 3968
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f:I

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v2, v0

    .line 3971
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 3972
    const/4 v3, 0x2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v0, v2

    .line 3971
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 3975
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v0

    add-int/2addr v0, v2

    .line 3976
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->i:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 1875
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 1875
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3931
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->h:B

    .line 3932
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 3946
    :cond_0
    :goto_0
    return v1

    .line 3933
    :cond_1
    if-eqz v0, :cond_0

    .line 3935
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3936
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->h:B

    goto :goto_0

    :cond_2
    move v0, v1

    .line 3939
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->o()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 3940
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a;->l()Z

    move-result v3

    if-nez v3, :cond_3

    .line 3941
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->h:B

    goto :goto_0

    .line 3939
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3945
    :cond_4
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->h:B

    move v1, v2

    .line 3946
    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 3887
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f:I

    return v0
.end method

.method public n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3896
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 3909
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;
    .locals 1

    .prologue
    .line 4041
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    return-object v0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;
    .locals 1

    .prologue
    .line 4045
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 3984
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
