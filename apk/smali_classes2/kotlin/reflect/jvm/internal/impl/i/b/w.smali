.class public final Lkotlin/reflect/jvm/internal/impl/i/b/w;
.super Ljava/lang/Object;
.source "MemberDeserializer.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

.field private final b:Lkotlin/reflect/jvm/internal/impl/i/b/o;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;)V
    .locals 3

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/e;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->m()Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/i/b/aa;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    return-void
.end method

.method private final a(I)I
    .locals 2

    .prologue
    .line 146
    and-int/lit8 v0, p1, 0x3f

    .line 147
    shr-int/lit8 v1, p1, 0x8

    shl-int/lit8 v1, v1, 0x6

    .line 148
    add-int/2addr v0, v1

    return v0
.end method

.method private final a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/at;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    move-object/from16 v0, p0

    iget-object v1, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v6

    if-nez v6, :cond_0

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.CallableDescriptor"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast v6, Lkotlin/reflect/jvm/internal/impl/b/a;

    .line 251
    invoke-interface {v6}, Lkotlin/reflect/jvm/internal/impl/b/a;->y_()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/i/b/ae;

    move-result-object v7

    .line 253
    check-cast p1, Ljava/lang/Iterable;

    .line 284
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v20, v1

    check-cast v20, Ljava/util/Collection;

    .line 285
    const/4 v3, 0x0

    .line 286
    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 287
    add-int/lit8 v21, v3, 0x1

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 254
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->p()I

    move-result v1

    move v11, v1

    .line 255
    :goto_1
    new-instance v13, Lkotlin/reflect/jvm/internal/impl/b/b/ag;

    const/4 v12, 0x0

    if-eqz v7, :cond_2

    move-object v2, v7

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/b/ae;

    .line 258
    new-instance v10, Lkotlin/reflect/jvm/internal/impl/i/b/a/a;

    move-object/from16 v0, p0

    iget-object v1, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v14

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/w$d;

    move-object/from16 v5, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    invoke-direct/range {v1 .. v9}, Lkotlin/reflect/jvm/internal/impl/i/b/w$d;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/ae;ILkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)V

    check-cast v1, Lkotlin/d/a/a;

    invoke-direct {v10, v14, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/a;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    move-object v1, v10

    .line 255
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/a;

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-object v2, v1

    move-object v5, v12

    move-object v8, v6

    move-object v9, v13

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v1

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->r()I

    move-result v10

    invoke-interface {v1, v10}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v10

    const-string v1, "c.nameResolver.getName(proto.name)"

    invoke-static {v10, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v12}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v12

    invoke-static {v4, v12}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v12

    const/4 v14, 0x0

    const/4 v15, 0x2

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-static {v1, v12, v14, v15, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v14

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/c;->y:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v1, v11}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v1

    const-string v12, "Flags.DECLARES_DEFAULT_VALUE.get(flags)"

    invoke-static {v1, v12}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/c;->z:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v1, v11}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v1

    const-string v12, "Flags.IS_CROSSINLINE.get(flags)"

    invoke-static {v1, v12}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v16

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/c;->A:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v1, v11}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v1

    const-string v11, "Flags.IS_NOINLINE.get(flags)"

    invoke-static {v1, v11}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    move-object/from16 v0, p0

    iget-object v1, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-static {v4, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eqz v1, :cond_3

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 267
    move-object/from16 v0, p0

    iget-object v4, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v4

    const/4 v11, 0x0

    const/4 v12, 0x2

    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-static {v4, v1, v11, v12, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    .line 255
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object/from16 v18, v1

    move-object v12, v2

    move v11, v3

    move-object v1, v13

    move-object v13, v10

    move-object v10, v5

    move-object/from16 v23, v9

    move-object v9, v8

    move-object/from16 v8, v23

    :goto_3
    sget-object v19, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const-string v2, "SourceElement.NO_SOURCE"

    move-object/from16 v0, v19

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {v8 .. v19}, Lkotlin/reflect/jvm/internal/impl/b/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/b/at;ILkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/k/r;ZZZLkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 269
    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move/from16 v3, v21

    .line 286
    goto/16 :goto_0

    .line 254
    :cond_1
    const/4 v1, 0x0

    move v11, v1

    goto/16 :goto_1

    .line 255
    :cond_2
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    move-object v2, v1

    move-object v5, v12

    move-object v8, v6

    move-object v9, v13

    goto/16 :goto_2

    :cond_3
    const/16 v18, 0x0

    move-object v12, v2

    move v11, v3

    move-object v1, v13

    move-object v13, v10

    move-object v10, v5

    move-object/from16 v23, v9

    move-object v9, v8

    move-object/from16 v8, v23

    goto :goto_3

    .line 288
    :cond_4
    check-cast v20, Ljava/util/List;

    check-cast v20, Ljava/util/Collection;

    .line 270
    invoke-static/range {v20 .. v20}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;Lkotlin/reflect/jvm/internal/impl/i/b/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    move-object p3, p2

    .line 234
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;ILkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 3

    .prologue
    .line 221
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->b:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v0, p2}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    .line 224
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/b;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/w$a;

    invoke-direct {v0, p0, p1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/w$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 3

    .prologue
    .line 236
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/b;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;

    invoke-direct {v0, p0, p1, p3}, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    return-object v0
.end method

.method private final a()Lkotlin/reflect/jvm/internal/impl/b/aj;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->C()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/i/b/ae;
    .locals 5

    .prologue
    .line 273
    .line 274
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/b/y;

    if-eqz v0, :cond_0

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/ae$b;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-interface {p1}, Lkotlin/reflect/jvm/internal/impl/b/y;->f()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v4

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->i()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v1, v2, v3, v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ae$b;-><init>(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae;

    .line 277
    :goto_0
    return-object v0

    .line 275
    :cond_0
    instance-of v0, p1, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->c()Lkotlin/reflect/jvm/internal/impl/i/b/ae$a;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae;

    goto :goto_0

    .line 276
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/i/b/ae;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/i/b/ae;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/w;)Lkotlin/reflect/jvm/internal/impl/i/b/o;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    return-object v0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/b/ag;
    .locals 21

    .prologue
    const-string v2, "proto"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->o()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p()I

    move-result v2

    move/from16 v20, v2

    .line 39
    :goto_0
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;

    move-object/from16 v0, p0

    iget-object v3, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v5, p1

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/b/b;->b:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v5, v1, v6}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;ILkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/c;->d:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$l;)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v6

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/i/c;->c:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-static {v7}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ai;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v7

    const-string v8, "Deserialization.visibili\u2026gs.VISIBILITY.get(flags))"

    invoke-static {v7, v8}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/i/c;->q:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v8

    const-string v9, "Flags.IS_VAR.get(flags)"

    invoke-static {v8, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v9}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->t()I

    move-result v10

    invoke-interface {v9, v10}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v9

    const-string v10, "c.nameResolver.getName(proto.name)"

    invoke-static {v9, v10}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/i/c;->j:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lkotlin/reflect/jvm/internal/impl/i/e$k;

    invoke-static {v10}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$k;)Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v10

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/i/c;->u:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v11

    const-string v12, "Flags.IS_LATEINIT.get(flags)"

    invoke-static {v11, v12}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    sget-object v12, Lkotlin/reflect/jvm/internal/impl/i/c;->t:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v12

    const-string v13, "Flags.IS_CONST.get(flags)"

    invoke-static {v12, v13}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    sget-object v13, Lkotlin/reflect/jvm/internal/impl/i/c;->w:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v13

    const-string v14, "Flags.IS_EXTERNAL_PROPERTY.get(flags)"

    invoke-static {v13, v14}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    sget-object v14, Lkotlin/reflect/jvm/internal/impl/i/c;->x:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v14

    const-string v15, "Flags.IS_DELEGATED.get(flags)"

    invoke-static {v14, v15}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v15}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v15, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v15}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v15, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v15}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->h()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v15, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v15}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->i()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v19

    move-object/from16 v15, p1

    invoke-direct/range {v2 .. v19}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;ZZZZLkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)V

    .line 58
    move-object/from16 v0, p0

    iget-object v3, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-object v4, v2

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->y()Ljava/util/List;

    move-result-object v5

    const-string v6, "proto.typeParameterList"

    invoke-static {v5, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v15

    .line 60
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->r:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v9

    .line 61
    const-string v3, "hasGetter"

    invoke-static {v9, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v4, p1

    .line 62
    check-cast v4, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/i/b/b;->c:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v8}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;Lkotlin/reflect/jvm/internal/impl/i/b/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    move-object v4, v3

    .line 66
    :goto_1
    invoke-virtual {v15}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static {v3, v5, v6, v7, v8}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v7

    invoke-virtual {v15}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a()Ljava/util/List;

    move-result-object v6

    invoke-direct/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->b(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v3

    if-eqz v3, :cond_3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 70
    invoke-virtual {v15}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    .line 66
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v2

    :goto_2
    invoke-virtual {v7, v6, v5, v4, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/aj;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 73
    const-string v3, "hasGetter"

    invoke-static {v9, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 74
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->H()I

    move-result v7

    .line 75
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->G()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->B:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v3, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "Flags.IS_NOT_DEFAULT.get(getterFlags)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    move v8, v3

    .line 76
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->G()Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->C:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v3, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "Flags.IS_EXTERNAL_ACCESSOR.get(getterFlags)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v9, 0x1

    .line 77
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->G()Z

    move-result v3

    if-eqz v3, :cond_6

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->D:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v3, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "Flags.IS_INLINE_ACCESSOR.get(getterFlags)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v10, 0x1

    .line 78
    :goto_5
    if-eqz v8, :cond_8

    .line 79
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-object v4, v2

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-object/from16 v5, p1

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/b/b;->c:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v7, v6}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;ILkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/c;->d:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {v6, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$l;)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v6

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/i/c;->c:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {v11, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-static {v7}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ai;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v7

    if-nez v8, :cond_7

    const/4 v8, 0x1

    :goto_6
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v11

    const/4 v12, 0x0

    sget-object v13, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct/range {v3 .. v13}, Lkotlin/reflect/jvm/internal/impl/b/b/z;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZZZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/ah;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 93
    :goto_7
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->g()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/z;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v14, v3

    .line 100
    :goto_8
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->s:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "Flags.HAS_SETTER.get(flags)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 101
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->J()I

    move-result v7

    .line 102
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->I()Z

    move-result v3

    if-eqz v3, :cond_a

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->B:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v3, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "Flags.IS_NOT_DEFAULT.get(setterFlags)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    move v8, v3

    .line 103
    :goto_9
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->I()Z

    move-result v3

    if-eqz v3, :cond_b

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->C:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v3, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "Flags.IS_EXTERNAL_ACCESSOR.get(setterFlags)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v9, 0x1

    .line 104
    :goto_a
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->G()Z

    move-result v3

    if-eqz v3, :cond_c

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->D:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v3, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "Flags.IS_INLINE_ACCESSOR.get(setterFlags)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_c

    const/4 v10, 0x1

    .line 105
    :goto_b
    if-eqz v8, :cond_e

    .line 106
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    move-object v4, v2

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/ag;

    move-object/from16 v5, p1

    check-cast v5, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/b/b;->d:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v7, v6}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;ILkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/c;->d:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {v6, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$l;)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v6

    sget-object v11, Lkotlin/reflect/jvm/internal/impl/i/c;->c:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {v11, v7}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-static {v7}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ai;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v7

    if-nez v8, :cond_d

    const/4 v8, 0x1

    :goto_c
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->n()Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v11

    const/4 v12, 0x0

    sget-object v13, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct/range {v3 .. v13}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;-><init>(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;ZZZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/ai;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v5, v3

    .line 116
    check-cast v5, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    move-object v4, v15

    invoke-static/range {v4 .. v10}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v4

    .line 117
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->b()Lkotlin/reflect/jvm/internal/impl/i/b/w;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->F()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v4

    invoke-static {v4}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    move-object/from16 v4, p1

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/i/b/b;->d:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    invoke-direct {v5, v6, v4, v7}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;

    move-result-object v4

    .line 120
    invoke-static {v4}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/b/at;

    invoke-virtual {v3, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/b/at;)V

    :goto_d
    move-object v4, v3

    .line 131
    :goto_e
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/c;->v:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v3

    const-string v5, "Flags.HAS_CONSTANT.get(flags)"

    invoke-static {v3, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 132
    move-object/from16 v0, p0

    iget-object v3, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v5

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/i/b/w$c;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/w$c;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/b/a/j;)V

    check-cast v3, Lkotlin/d/a/a;

    invoke-interface {v5, v3}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/g;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->a(Lkotlin/reflect/jvm/internal/impl/j/g;)V

    :cond_0
    move-object v3, v4

    .line 140
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/ai;

    invoke-virtual {v2, v14, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/a/j;->a(Lkotlin/reflect/jvm/internal/impl/b/b/z;Lkotlin/reflect/jvm/internal/impl/b/ai;)V

    .line 142
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/ag;

    return-object v2

    .line 37
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(I)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_0

    .line 64
    :cond_2
    sget-object v3, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_1

    .line 66
    :cond_3
    const/4 v3, 0x0

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v2

    goto/16 :goto_2

    .line 75
    :cond_4
    const/4 v3, 0x0

    move v8, v3

    goto/16 :goto_3

    .line 76
    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_4

    .line 77
    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_5

    .line 79
    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_6

    :cond_8
    move-object v3, v2

    .line 91
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/ag;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/reflect/jvm/internal/impl/h/b;->b(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/b/z;

    move-result-object v3

    const-string v4, "DescriptorFactory.create\u2026perty, Annotations.EMPTY)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 97
    :cond_9
    const/4 v3, 0x0

    move-object v14, v3

    goto/16 :goto_8

    .line 102
    :cond_a
    const/4 v3, 0x0

    move v8, v3

    goto/16 :goto_9

    .line 103
    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_a

    .line 104
    :cond_c
    const/4 v10, 0x0

    goto/16 :goto_b

    .line 106
    :cond_d
    const/4 v8, 0x0

    goto/16 :goto_c

    :cond_e
    move-object v3, v2

    .line 124
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/ag;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/reflect/jvm/internal/impl/h/b;->a(Lkotlin/reflect/jvm/internal/impl/b/ag;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/b/b/aa;

    move-result-object v3

    const-string v4, "DescriptorFactory.create\u2026perty, Annotations.EMPTY)"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 128
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_e
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Lkotlin/reflect/jvm/internal/impl/b/ak;
    .locals 17

    .prologue
    const-string v1, "proto"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->p()I

    move-result v1

    move v15, v1

    :goto_0
    move-object/from16 v1, p1

    .line 153
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/b/b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v15, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;ILkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v7

    .line 154
    invoke-static/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v2, p1

    .line 155
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/b/b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v6}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;Lkotlin/reflect/jvm/internal/impl/i/b/b;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    move-object/from16 v16, v1

    .line 157
    :goto_1
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;

    move-object/from16 v0, p0

    iget-object v2, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->t()I

    move-result v5

    invoke-interface {v4, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    const-string v4, "c.nameResolver.getName(proto.name)"

    invoke-static {v5, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/c;->j:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {v4, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlin/reflect/jvm/internal/impl/i/e$k;

    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$k;)Lkotlin/reflect/jvm/internal/impl/b/b$a;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->h()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v4, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->i()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v11

    const/4 v12, 0x0

    const/16 v13, 0x400

    const/4 v14, 0x0

    move-object v4, v7

    move-object/from16 v7, p1

    invoke-direct/range {v1 .. v14}, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/b/al;ILkotlin/d/b/i;)V

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-object v3, v1

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->y()Ljava/util/List;

    move-result-object v4

    const-string v5, "proto.typeParameterList"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v7

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->b(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 164
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    .line 163
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    move-object v3, v2

    move-object v2, v1

    :goto_2
    invoke-direct/range {p0 .. p0}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a()Lkotlin/reflect/jvm/internal/impl/b/aj;

    move-result-object v4

    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->b()Lkotlin/reflect/jvm/internal/impl/i/b/w;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->E()Ljava/util/List;

    move-result-object v9

    const-string v6, "proto.valueParameterList"

    invoke-static {v9, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v6, p1

    check-cast v6, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/i/b/b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    invoke-direct {v8, v9, v6, v10}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v8}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$i;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-static {v7, v8, v9, v10, v11}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v7

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/i/c;->d:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {v8, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lkotlin/reflect/jvm/internal/impl/i/e$l;

    invoke-static {v8}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$l;)Lkotlin/reflect/jvm/internal/impl/b/u;

    move-result-object v8

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/i/c;->c:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {v9, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-static {v9}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ai;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v9

    invoke-virtual/range {v2 .. v9}, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;->b(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/aj;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/u;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/ac;

    .line 172
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c;->k:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v2, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "Flags.IS_OPERATOR.get(flags)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;->a(Z)V

    .line 173
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c;->l:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v2, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "Flags.IS_INFIX.get(flags)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;->b(Z)V

    .line 174
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c;->o:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v2, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "Flags.IS_EXTERNAL_FUNCTION.get(flags)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;->c(Z)V

    .line 175
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c;->m:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v2, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "Flags.IS_INLINE.get(flags)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;->d(Z)V

    .line 176
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c;->n:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v2, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "Flags.IS_TAILREC.get(flags)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;->e(Z)V

    .line 177
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c;->p:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {v2, v15}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "Flags.IS_SUSPEND.get(flags)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/k;->i(Z)V

    .line 178
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/ak;

    return-object v1

    .line 152
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->r()I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(I)I

    move-result v1

    move v15, v1

    goto/16 :goto_0

    .line 156
    :cond_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    move-object/from16 v16, v1

    goto/16 :goto_1

    .line 163
    :cond_2
    const/4 v3, 0x0

    move-object v2, v1

    goto/16 :goto_2
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$z;)Lkotlin/reflect/jvm/internal/impl/b/ap;
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/b/a/i;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->C()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 280
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 281
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 282
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 182
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a:Lkotlin/reflect/jvm/internal/impl/i/b/e;

    const-string v5, "it"

    invoke-static {v0, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/e;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;Lkotlin/reflect/jvm/internal/impl/i/b/x;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 283
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 182
    invoke-direct {v2, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/i;-><init>(Ljava/util/List;)V

    .line 184
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->c:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ai;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v4

    .line 185
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v3

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->r()I

    move-result v5

    invoke-interface {v3, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->b(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v3

    const-string v5, "c.nameResolver.getName(proto.name)"

    invoke-static {v3, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "visibility"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v6

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v7

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->h()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v8

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->i()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v9

    move-object v5, p1

    invoke-direct/range {v0 .. v9}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/ay;Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;)V

    .line 190
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-object v2, v0

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->s()Ljava/util/List;

    move-result-object v3

    const-string v4, "proto.typeParameterList"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v6, 0xc

    move-object v4, v10

    move-object v5, v10

    move-object v7, v10

    invoke-static/range {v1 .. v7}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v1

    .line 191
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v3

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v4

    invoke-static {p1, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v4

    invoke-static {v3, v4, v10, v11, v10}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v3

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v1

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v4

    invoke-static {p1, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->b(Lkotlin/reflect/jvm/internal/impl/i/e$z;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v4

    invoke-static {v1, v4, v10, v11, v10}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/l;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)V

    .line 197
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ap;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$e;Z)Lkotlin/reflect/jvm/internal/impl/b/d;
    .locals 14

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 206
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;

    const/4 v2, 0x0

    move-object v3, p1

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->p()I

    move-result v4

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/i/b/b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    invoke-direct {p0, v3, v4, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;ILkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/b/b$a;->a:Lkotlin/reflect/jvm/internal/impl/b/b$a;

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v7

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v8

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->h()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    move-result-object v9

    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->i()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x400

    const/4 v13, 0x0

    move/from16 v4, p2

    move-object v6, p1

    invoke-direct/range {v0 .. v13}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/e;Lkotlin/reflect/jvm/internal/impl/b/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/i/e$e;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/b/al;ILkotlin/d/b/i;)V

    .line 211
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w;->b:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-object v3, v0

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v2

    .line 212
    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->b()Lkotlin/reflect/jvm/internal/impl/i/b/w;

    move-result-object v3

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->q()Ljava/util/List;

    move-result-object v4

    const-string v2, "proto.valueParameterList"

    invoke-static {v4, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/i/b/b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    invoke-direct {v3, v4, v2, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;

    move-result-object v3

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/c;->c:Lkotlin/reflect/jvm/internal/impl/i/c$c;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->p()I

    move-result v4

    invoke-virtual {v2, v4}, Lkotlin/reflect/jvm/internal/impl/i/c$c;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/i/e$ai;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ai;)Lkotlin/reflect/jvm/internal/impl/b/ay;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->a(Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/b/ay;)Lkotlin/reflect/jvm/internal/impl/b/b/f;

    .line 216
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/d;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    .line 217
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    return-object v0
.end method
