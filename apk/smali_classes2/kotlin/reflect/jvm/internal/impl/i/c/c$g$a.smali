.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$g;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1763
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 1864
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    .line 1989
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    .line 1764
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->n()V

    .line 1765
    return-void
.end method

.method static synthetic m()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 1

    .prologue
    .line 1756
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 1768
    return-void
.end method

.method private static o()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 1

    .prologue
    .line 1770
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;-><init>()V

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 1867
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1868
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    .line 1869
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    .line 1871
    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1991
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1992
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    .line 1993
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    .line 1995
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 2

    .prologue
    .line 1815
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1838
    :goto_0
    return-object p0

    .line 1816
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1817
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1818
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    .line 1819
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    .line 1826
    :cond_1
    :goto_1
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1827
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1828
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    .line 1829
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    .line 1836
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->d(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0

    .line 1821
    :cond_3
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->p()V

    .line 1822
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1831
    :cond_4
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->q()V

    .line 1832
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 1756
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1756
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 1756
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 1756
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1756
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1849
    const/4 v2, 0x0

    .line 1851
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856
    if-eqz v0, :cond_0

    .line 1857
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    .line 1860
    :cond_0
    return-object p0

    .line 1852
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1853
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1854
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1856
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 1857
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    :cond_1
    throw v0

    .line 1856
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 1756
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1756
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1756
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;
    .locals 2

    .prologue
    .line 1783
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
    .locals 1

    .prologue
    .line 1787
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1756
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->j()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 1756
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
    .locals 2

    .prologue
    .line 1791
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    move-result-object v0

    .line 1792
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1793
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1795
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
    .locals 3

    .prologue
    .line 1799
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V

    .line 1800
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    .line 1801
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1802
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    .line 1803
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    .line 1805
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;Ljava/util/List;)Ljava/util/List;

    .line 1806
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1807
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    .line 1808
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->a:I

    .line 1810
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$a;->c:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g;Ljava/util/List;)Ljava/util/List;

    .line 1811
    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1842
    const/4 v0, 0x1

    return v0
.end method
