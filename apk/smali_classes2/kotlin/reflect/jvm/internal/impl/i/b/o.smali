.class public final Lkotlin/reflect/jvm/internal/impl/i/b/o;
.super Ljava/lang/Object;
.source "context.kt"


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

.field private final b:Lkotlin/reflect/jvm/internal/impl/i/b/w;

.field private final c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

.field private final d:Lkotlin/reflect/jvm/internal/impl/i/b/x;

.field private final e:Lkotlin/reflect/jvm/internal/impl/b/m;

.field private final f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

.field private final g:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

.field private final h:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/m;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/i/b/ag;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/m;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ah;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a/n;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/a/f;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ag;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "components"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingDeclaration"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sinceKotlinInfoTable"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameters"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e:Lkotlin/reflect/jvm/internal/impl/b/m;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    iput-object p6, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    .line 72
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deserializer for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e:Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-interface {v2}, Lkotlin/reflect/jvm/internal/impl/b/m;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p7, p8, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/i/b/ag;Ljava/util/List;Ljava/lang/String;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    .line 75
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/w;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/w;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->b:Lkotlin/reflect/jvm/internal/impl/i/b/w;

    return-void
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/i/b/o;
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_0

    .line 82
    iget-object p3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    :cond_0
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_1

    .line 83
    iget-object p4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/m;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/b/o;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/x;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ah;",
            ")",
            "Lkotlin/reflect/jvm/internal/impl/i/b/o;"
        }
    .end annotation

    .prologue
    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameterProtos"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameResolver"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeTable"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/o;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    iget-object v7, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-object v2, p3

    move-object v3, p1

    move-object v4, p4

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lkotlin/reflect/jvm/internal/impl/i/b/o;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/m;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/i/b/ah;Lkotlin/reflect/jvm/internal/impl/i/b/a/n;Lkotlin/reflect/jvm/internal/impl/i/b/a/f;Lkotlin/reflect/jvm/internal/impl/i/b/ag;Ljava/util/List;)V

    .line 87
    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/i/b/w;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->b:Lkotlin/reflect/jvm/internal/impl/i/b/w;

    return-object v0
.end method

.method public final c()Lkotlin/reflect/jvm/internal/impl/j/i;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->b()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lkotlin/reflect/jvm/internal/impl/i/b/m;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c:Lkotlin/reflect/jvm/internal/impl/i/b/m;

    return-object v0
.end method

.method public final e()Lkotlin/reflect/jvm/internal/impl/i/b/x;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d:Lkotlin/reflect/jvm/internal/impl/i/b/x;

    return-object v0
.end method

.method public final f()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e:Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public final g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f:Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    return-object v0
.end method

.method public final h()Lkotlin/reflect/jvm/internal/impl/i/b/a/n;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g:Lkotlin/reflect/jvm/internal/impl/i/b/a/n;

    return-object v0
.end method

.method public final i()Lkotlin/reflect/jvm/internal/impl/i/b/a/f;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/o;->h:Lkotlin/reflect/jvm/internal/impl/i/b/a/f;

    return-object v0
.end method
