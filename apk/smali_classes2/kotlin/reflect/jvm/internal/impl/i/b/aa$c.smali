.class final Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;
.super Lkotlin/reflect/jvm/internal/impl/b/b/d;
.source "NotFoundClasses.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/aa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "constructorTypeParameters"

    const-string v5, "getConstructorTypeParameters()Ljava/util/List;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/e/f;ZI)V
    .locals 6

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingDeclaration"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const-string v0, "SourceElement.NO_SOURCE"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    const-string v0, "Visibilities.PUBLIC"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    .line 124
    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/b/b/d;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;Lkotlin/reflect/jvm/internal/impl/b/ay;)V

    iput-boolean p4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->c:Z

    move-object v0, p0

    .line 126
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    invoke-static {v0, p5}, Lkotlin/reflect/jvm/internal/impl/i/b/ab;->a(Lkotlin/reflect/jvm/internal/impl/b/h;I)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->a(Ljava/util/List;)V

    .line 129
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method private final o()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public E_()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 140
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "builtIns.nullableAnyType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;
    .locals 1

    .prologue
    const-string v0, "substitutor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    return-object p0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 136
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "builtIns.nullableAnyType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->a(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->n()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/v;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method protected k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->o()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 2

    .prologue
    .line 138
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->d(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "builtIns.nullableAnyType"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$c;->c:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MockTypeAliasDescriptor["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
