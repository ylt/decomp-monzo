.class public final Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$s;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$s$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$t;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 20333
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 20555
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$b;->b:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    .line 20334
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->n()V

    .line 20335
    return-void
.end method

.method static synthetic m()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20326
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 20338
    return-void
.end method

.method private static o()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20340
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20482
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    .line 20483
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->b:I

    .line 20485
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$s$b;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20584
    if-nez p1, :cond_0

    .line 20585
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20587
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    .line 20588
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    .line 20590
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 2

    .prologue
    .line 20403
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->f()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 20421
    :goto_0
    return-object p0

    .line 20404
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20405
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->m()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    .line 20407
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20408
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->o()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    .line 20410
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 20411
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->q()Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s$b;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    .line 20413
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 20414
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->s()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    .line 20416
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 20417
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->u()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->d(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    .line 20419
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->b(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 20326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20326
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 20326
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$s;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20535
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    .line 20536
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->c:I

    .line 20538
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 20326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20326
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20635
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    .line 20636
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->e:I

    .line 20638
    return-object p0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20432
    const/4 v2, 0x0

    .line 20434
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 20439
    if-eqz v0, :cond_0

    .line 20440
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    .line 20443
    :cond_0
    return-object p0

    .line 20435
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 20436
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 20437
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 20439
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 20440
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    :cond_1
    throw v0

    .line 20439
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 20326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 1

    .prologue
    .line 20683
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    .line 20684
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->f:I

    .line 20686
    return-object p0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 20326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;
    .locals 2

    .prologue
    .line 20359
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s;)Lkotlin/reflect/jvm/internal/impl/i/e$s$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$s;
    .locals 1

    .prologue
    .line 20363
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->f()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 20326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 20326
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$s;
    .locals 2

    .prologue
    .line 20367
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$s;

    move-result-object v0

    .line 20368
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20369
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 20371
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$s;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 20375
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$s;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 20376
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->a:I

    .line 20377
    const/4 v1, 0x0

    .line 20378
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 20381
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I

    .line 20382
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 20383
    or-int/lit8 v0, v0, 0x2

    .line 20385
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->b(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I

    .line 20386
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 20387
    or-int/lit8 v0, v0, 0x4

    .line 20389
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->a(Lkotlin/reflect/jvm/internal/impl/i/e$s;Lkotlin/reflect/jvm/internal/impl/i/e$s$b;)Lkotlin/reflect/jvm/internal/impl/i/e$s$b;

    .line 20390
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 20391
    or-int/lit8 v0, v0, 0x8

    .line 20393
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->e:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->c(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I

    .line 20394
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 20395
    or-int/lit8 v0, v0, 0x10

    .line 20397
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$s$a;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->d(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I

    .line 20398
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$s;->e(Lkotlin/reflect/jvm/internal/impl/i/e$s;I)I

    .line 20399
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 20425
    const/4 v0, 0x1

    return v0
.end method
