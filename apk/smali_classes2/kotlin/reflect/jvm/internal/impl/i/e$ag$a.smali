.class public final Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$ah;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ag;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$ah;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private e:I

.field private f:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private g:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 17537
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 17773
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17865
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17538
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->u()V

    .line 17539
    return-void
.end method

.method static synthetic t()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17531
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->v()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method private u()V
    .locals 0

    .prologue
    .line 17542
    return-void
.end method

.method private static v()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17544
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17719
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    .line 17720
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->b:I

    .line 17722
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 2

    .prologue
    .line 17613
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 17635
    :goto_0
    return-object p0

    .line 17614
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17615
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    .line 17617
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 17618
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->r()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    .line 17620
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 17621
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->t()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    .line 17623
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 17624
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->v()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    .line 17626
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->w()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 17627
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->x()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    .line 17629
    :cond_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 17630
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->z()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    .line 17632
    :cond_6
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 17633
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 2

    .prologue
    .line 17812
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 17814
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17820
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    .line 17821
    return-object p0

    .line 17817
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 17531
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17531
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 17531
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17758
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    .line 17759
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->c:I

    .line 17761
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 2

    .prologue
    .line 17904
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 17906
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17912
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    .line 17913
    return-object p0

    .line 17909
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 17531
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17531
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17850
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    .line 17851
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->e:I

    .line 17853
    return-object p0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17666
    const/4 v2, 0x0

    .line 17668
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 17673
    if-eqz v0, :cond_0

    .line 17674
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    .line 17677
    :cond_0
    return-object p0

    .line 17669
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 17670
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 17671
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 17673
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 17674
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    :cond_1
    throw v0

    .line 17673
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 17531
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 17531
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 1

    .prologue
    .line 17942
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    .line 17943
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->g:I

    .line 17945
    return-object p0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 17531
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 17531
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 17531
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 17531
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;
    .locals 2

    .prologue
    .line 17565
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->v()Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 17569
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 17639
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->o()Z

    move-result v1

    if-nez v1, :cond_1

    .line 17659
    :cond_0
    :goto_0
    return v0

    .line 17643
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 17644
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17649
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 17650
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->s()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17655
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17659
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 2

    .prologue
    .line 17573
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    .line 17574
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 17575
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 17577
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 17581
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 17582
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    .line 17583
    const/4 v1, 0x0

    .line 17584
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 17587
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I

    .line 17588
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 17589
    or-int/lit8 v0, v0, 0x2

    .line 17591
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I

    .line 17592
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 17593
    or-int/lit8 v0, v0, 0x4

    .line 17595
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17596
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 17597
    or-int/lit8 v0, v0, 0x8

    .line 17599
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->e:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->c(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I

    .line 17600
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 17601
    or-int/lit8 v0, v0, 0x10

    .line 17603
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 17604
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 17605
    or-int/lit8 v0, v0, 0x20

    .line 17607
    :cond_4
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->g:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->d(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I

    .line 17608
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->e(Lkotlin/reflect/jvm/internal/impl/i/e$ag;I)I

    .line 17609
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 17746
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 17778
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 17784
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 17870
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 17876
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->f:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method
