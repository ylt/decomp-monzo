.class public final Lkotlin/reflect/jvm/internal/impl/i/e$q;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$r;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "q"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$q$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$q$b;,
        Lkotlin/reflect/jvm/internal/impl/i/e$q$c;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$q;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$q;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$q$b;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 826
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 1838
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    .line 1839
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->p()V

    .line 1840
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 774
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 1502
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f:B

    .line 1527
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->g:I

    .line 775
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->p()V

    .line 777
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v3

    .line 779
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v4

    move v2, v0

    .line 784
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 785
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v5

    .line 786
    sparse-switch v5, :sswitch_data_0

    .line 791
    invoke-virtual {p0, p1, v4, p2, v5}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 793
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 789
    goto :goto_0

    .line 798
    :sswitch_1
    and-int/lit8 v5, v2, 0x1

    if-eq v5, v1, :cond_1

    .line 799
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    .line 800
    or-int/lit8 v2, v2, 0x1

    .line 802
    :cond_1
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v6, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 807
    :catch_0
    move-exception v0

    .line 808
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 813
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_2

    .line 814
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    .line 817
    :cond_2
    :try_start_2
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 821
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 823
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d()V

    throw v0

    .line 813
    :cond_3
    and-int/lit8 v0, v2, 0x1

    if-ne v0, v1, :cond_4

    .line 814
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    .line 817
    :cond_4
    :try_start_3
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 821
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 823
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d()V

    .line 825
    return-void

    .line 818
    :catch_1
    move-exception v0

    .line 821
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 809
    :catch_2
    move-exception v0

    .line 810
    :try_start_4
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 818
    :catch_3
    move-exception v1

    .line 821
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 786
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 750
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$q;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 756
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 1502
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f:B

    .line 1527
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->g:I

    .line 757
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 758
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 750
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$q;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 759
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 1502
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f:B

    .line 1527
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->g:I

    .line 759
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$q;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 750
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 1

    .prologue
    .line 1605
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->m()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Ljava/util/List;
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$q;
    .locals 1

    .prologue
    .line 763
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    return-object v0
.end method

.method public static m()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 1

    .prologue
    .line 1602
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 1500
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    .line 1501
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b;
    .locals 1

    .prologue
    .line 1489
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1520
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->i()I

    .line 1521
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1522
    const/4 v2, 0x1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 1521
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1524
    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 1525
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 838
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 750
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->g()Lkotlin/reflect/jvm/internal/impl/i/e$q;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$q;
    .locals 1

    .prologue
    .line 767
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->c:Lkotlin/reflect/jvm/internal/impl/i/e$q;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 1483
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1529
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->g:I

    .line 1530
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 1539
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 1533
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1534
    const/4 v3, 0x1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v2, v0

    .line 1533
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1537
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v0

    add-int/2addr v0, v2

    .line 1538
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->g:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 750
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->n()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 750
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->o()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1504
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f:B

    .line 1505
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 1515
    :cond_0
    :goto_0
    return v1

    .line 1506
    :cond_1
    if-eqz v0, :cond_0

    move v0, v1

    .line 1508
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->h()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 1509
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$q$b;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$q$b;->l()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1510
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f:B

    goto :goto_0

    .line 1508
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1514
    :cond_3
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$q;->f:B

    move v1, v2

    .line 1515
    goto :goto_0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 1

    .prologue
    .line 1603
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->m()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/e$q$a;
    .locals 1

    .prologue
    .line 1607
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$q;->a(Lkotlin/reflect/jvm/internal/impl/i/e$q;)Lkotlin/reflect/jvm/internal/impl/i/e$q$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1546
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
