.class final Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;
.super Lkotlin/reflect/jvm/internal/impl/k/b;
.source "DeserializedClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

.field private final b:Lkotlin/reflect/jvm/internal/impl/j/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/j/f",
            "<",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 174
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/k/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V

    .line 175
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method


# virtual methods
.method protected F_()Lkotlin/reflect/jvm/internal/impl/b/ao;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ao$a;->a:Lkotlin/reflect/jvm/internal/impl/b/ao$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/ao;

    return-object v0
.end method

.method protected a()Ljava/util/Collection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v6, 0xa

    const/4 v3, 0x0

    .line 180
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->E()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 347
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 348
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 349
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 181
    iget-object v4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->a()Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v0, v3, v5, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 350
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->n()Lkotlin/reflect/jvm/internal/impl/i/b/a;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-interface {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/a;->a(Lkotlin/reflect/jvm/internal/impl/i/b/a/e;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 182
    invoke-static {v1, v0}, Lkotlin/a/m;->b(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 184
    check-cast v0, Ljava/lang/Iterable;

    .line 351
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 360
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 359
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 185
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v5, v0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$b;

    if-nez v5, :cond_2

    move-object v0, v3

    :cond_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/aa$b;

    if-eqz v0, :cond_1

    .line 359
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 362
    :cond_3
    check-cast v2, Ljava/util/List;

    move-object v0, v2

    .line 188
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_7

    .line 189
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->a()Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->i()Lkotlin/reflect/jvm/internal/impl/i/b/r;

    move-result-object v5

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    check-cast v2, Ljava/lang/Iterable;

    .line 363
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v2, v6}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 364
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 365
    check-cast v4, Lkotlin/reflect/jvm/internal/impl/i/b/aa$b;

    move-object v2, v4

    .line 191
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/a;->g()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    :goto_4
    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 188
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 191
    :cond_5
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/b/aa$b;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 366
    :cond_6
    check-cast v3, Ljava/util/List;

    .line 189
    invoke-interface {v5, v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/r;->a(Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;)V

    .line 195
    :cond_7
    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->b:Lkotlin/reflect/jvm/internal/impl/j/f;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/j/f;->v_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->h()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/b/v;->a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z

    move-result v0

    return v0
.end method

.method public h()Lkotlin/reflect/jvm/internal/impl/i/b/a/e;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/a/e$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/a/e;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/e;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
