.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$e;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

.field private g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

.field private h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

.field private i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 3186
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 3781
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    .line 3782
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->w()V

    .line 3783
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 3093
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 3276
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->j:B

    .line 3304
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->k:I

    .line 3094
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->w()V

    .line 3096
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v5

    .line 3098
    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v6

    .line 3102
    const/4 v2, 0x0

    .line 3103
    :goto_0
    if-nez v2, :cond_4

    .line 3104
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 3105
    sparse-switch v0, :sswitch_data_0

    .line 3110
    invoke-virtual {p0, p1, v6, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    move v2, v0

    .line 3169
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 3108
    goto :goto_1

    .line 3118
    :sswitch_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_8

    .line 3119
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->r()Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    move-object v3, v0

    .line 3121
    :goto_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    .line 3122
    if-eqz v3, :cond_0

    .line 3123
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    .line 3124
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    .line 3126
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    move v0, v2

    .line 3127
    goto :goto_1

    .line 3131
    :sswitch_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_7

    .line 3132
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->r()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    move-object v3, v0

    .line 3134
    :goto_3
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3135
    if-eqz v3, :cond_1

    .line 3136
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    .line 3137
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3139
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    move v0, v2

    .line 3140
    goto :goto_1

    .line 3144
    :sswitch_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    .line 3145
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->r()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    move-object v3, v0

    .line 3147
    :goto_4
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3148
    if-eqz v3, :cond_2

    .line 3149
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    .line 3150
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3152
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    move v0, v2

    .line 3153
    goto/16 :goto_1

    .line 3157
    :sswitch_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_5

    .line 3158
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->r()Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    move-object v3, v0

    .line 3160
    :goto_5
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3161
    if-eqz v3, :cond_3

    .line 3162
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    .line 3163
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3165
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v0, v2

    .line 3166
    goto/16 :goto_1

    .line 3177
    :cond_4
    :try_start_1
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3181
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 3183
    :goto_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d()V

    .line 3185
    return-void

    .line 3178
    :catch_0
    move-exception v0

    .line 3181
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_6

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 3170
    :catch_1
    move-exception v0

    .line 3171
    :try_start_2
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3176
    :catchall_1
    move-exception v0

    .line 3177
    :try_start_3
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 3181
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 3183
    :goto_7
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d()V

    throw v0

    .line 3172
    :catch_2
    move-exception v0

    .line 3173
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3178
    :catch_3
    move-exception v1

    .line 3181
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_7

    :catchall_2
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    :cond_5
    move-object v3, v4

    goto :goto_5

    :cond_6
    move-object v3, v4

    goto/16 :goto_4

    :cond_7
    move-object v3, v4

    goto/16 :goto_3

    :cond_8
    move-object v3, v4

    goto/16 :goto_2

    :cond_9
    move v0, v2

    goto/16 :goto_1

    .line 3105
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3069
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3075
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 3276
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->j:B

    .line 3304
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->k:I

    .line 3076
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 3077
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V
    .locals 0

    .prologue
    .line 3069
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3078
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 3276
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->j:B

    .line 3304
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->k:I

    .line 3078
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;I)I
    .locals 0

    .prologue
    .line 3069
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a;
    .locals 0

    .prologue
    .line 3069
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 0

    .prologue
    .line 3069
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 1

    .prologue
    .line 3394
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->t()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 0

    .prologue
    .line 3069
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 3069
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 0

    .prologue
    .line 3069
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    return-object p1
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
    .locals 1

    .prologue
    .line 3082
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    return-object v0
.end method

.method public static t()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 1

    .prologue
    .line 3391
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->m()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 3271
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    .line 3272
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3273
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3274
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3275
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3288
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i()I

    .line 3289
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3290
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 3292
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3293
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 3295
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 3296
    const/4 v0, 0x3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 3298
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 3299
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-virtual {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 3301
    :cond_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 3302
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3198
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 3069
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
    .locals 1

    .prologue
    .line 3086
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3208
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3306
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->k:I

    .line 3307
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3328
    :goto_0
    return v0

    .line 3309
    :cond_0
    const/4 v0, 0x0

    .line 3310
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 3311
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3314
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 3315
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v3, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3318
    :cond_2
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 3319
    const/4 v1, 0x3

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3322
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 3323
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v4, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3326
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 3327
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->k:I

    goto :goto_0
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 3069
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->u()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 3069
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->v()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3278
    iget-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->j:B

    .line 3279
    if-ne v1, v0, :cond_0

    .line 3283
    :goto_0
    return v0

    .line 3280
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3282
    :cond_1
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->j:B

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;
    .locals 1

    .prologue
    .line 3214
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    return-object v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 3227
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 1

    .prologue
    .line 3237
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->g:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    return-object v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 3246
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 1

    .prologue
    .line 3252
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    return-object v0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 3261
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;
    .locals 1

    .prologue
    .line 3267
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    return-object v0
.end method

.method public u()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 1

    .prologue
    .line 3392
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->t()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 1

    .prologue
    .line 3396
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 3335
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
