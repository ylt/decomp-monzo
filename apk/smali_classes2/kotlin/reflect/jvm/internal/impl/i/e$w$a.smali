.class public final Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$x;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$w;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$w$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$x;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 541
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 625
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/l;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 542
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->n()V

    .line 543
    return-void
.end method

.method static synthetic m()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 1

    .prologue
    .line 534
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 546
    return-void
.end method

.method private static o()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 1

    .prologue
    .line 548
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;-><init>()V

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 627
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 628
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/protobuf/l;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/l;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/m;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 629
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    .line 631
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 2

    .prologue
    .line 586
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 599
    :goto_0
    return-object p0

    .line 587
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->b(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 588
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 589
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->b(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 590
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    .line 597
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->c(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0

    .line 592
    :cond_2
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->p()V

    .line 593
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->b(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 534
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 534
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$w;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 534
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 610
    const/4 v2, 0x0

    .line 612
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 617
    if-eqz v0, :cond_0

    .line 618
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    .line 621
    :cond_0
    return-object p0

    .line 613
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 614
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 615
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 617
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 618
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    :cond_1
    throw v0

    .line 617
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 534
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;
    .locals 2

    .prologue
    .line 559
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;)Lkotlin/reflect/jvm/internal/impl/i/e$w$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$w;
    .locals 1

    .prologue
    .line 563
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->f()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$w;
    .locals 2

    .prologue
    .line 567
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$w;

    move-result-object v0

    .line 568
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 569
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 571
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$w;
    .locals 3

    .prologue
    .line 575
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 576
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    .line 577
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 578
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/m;->b()Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 579
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->a:I

    .line 581
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$w$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$w;->a(Lkotlin/reflect/jvm/internal/impl/i/e$w;Lkotlin/reflect/jvm/internal/impl/protobuf/m;)Lkotlin/reflect/jvm/internal/impl/protobuf/m;

    .line 582
    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 603
    const/4 v0, 0x1

    return v0
.end method
