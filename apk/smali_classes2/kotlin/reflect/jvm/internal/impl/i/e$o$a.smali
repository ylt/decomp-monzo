.class public final Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$o$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$p;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

.field private i:I

.field private j:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

.field private k:I

.field private l:I

.field private m:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16149
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 16367
    const/16 v0, 0x206

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->b:I

    .line 16459
    const/16 v0, 0x806

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->c:I

    .line 16523
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 16615
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    .line 16740
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 16832
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 16150
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->x()V

    .line 16151
    return-void
.end method

.method static synthetic w()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16143
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->y()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method private x()V
    .locals 0

    .prologue
    .line 16154
    return-void
.end method

.method private static y()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16156
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;-><init>()V

    return-object v0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 16618
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 16619
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    .line 16620
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16622
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16429
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16430
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->b:I

    .line 16432
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 2

    .prologue
    .line 16871
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->m()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 16873
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 16879
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16880
    return-object p0

    .line 16876
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 2

    .prologue
    .line 16262
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->m()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 16309
    :goto_0
    return-object p0

    .line 16263
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16264
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16266
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16267
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->r()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16269
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 16270
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->t()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16272
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 16273
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->v()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16275
    :cond_4
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->w()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 16276
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->x()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->d(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16278
    :cond_5
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 16279
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 16280
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    .line 16281
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16288
    :cond_6
    :goto_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->A()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 16289
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->B()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16291
    :cond_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->C()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 16292
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->D()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->f(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16294
    :cond_8
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->E()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 16295
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->F()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16297
    :cond_9
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->G()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 16298
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->H()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16300
    :cond_a
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->I()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 16301
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->J()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->h(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16303
    :cond_b
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->K()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 16304
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->L()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->i(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16306
    :cond_c
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 16307
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->c(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 16283
    :cond_d
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->z()V

    .line 16284
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 2

    .prologue
    .line 16562
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 16564
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 16570
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16571
    return-object p0

    .line 16567
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 16143
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16143
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 16143
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16476
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16477
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->c:I

    .line 16479
    return-object p0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 2

    .prologue
    .line 16779
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 16781
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->n()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 16787
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16788
    return-object p0

    .line 16784
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    goto :goto_0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 16143
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16143
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16508
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16509
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->d:I

    .line 16511
    return-object p0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16352
    const/4 v2, 0x0

    .line 16354
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 16359
    if-eqz v0, :cond_0

    .line 16360
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    .line 16363
    :cond_0
    return-object p0

    .line 16355
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 16356
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 16357
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 16359
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 16360
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    :cond_1
    throw v0

    .line 16359
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 16143
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 16143
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16600
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16601
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->f:I

    .line 16603
    return-object p0
.end method

.method public e(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 16640
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 16143
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    return-object v0
.end method

.method public f(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16817
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16818
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->i:I

    .line 16820
    return-object p0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 16143
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public g(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16936
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16937
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->k:I

    .line 16939
    return-object p0
.end method

.method public h(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 16977
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16978
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->l:I

    .line 16980
    return-object p0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 16143
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    return-object v0
.end method

.method public i(I)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 1

    .prologue
    .line 17021
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 17022
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->m:I

    .line 17024
    return-object p0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 16143
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;
    .locals 2

    .prologue
    .line 16189
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->y()Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;)Lkotlin/reflect/jvm/internal/impl/i/e$o$a;

    move-result-object v0

    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 1

    .prologue
    .line 16193
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->m()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 16313
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 16345
    :cond_0
    :goto_0
    return v1

    .line 16317
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16318
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->q()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    move v0, v1

    .line 16323
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->r()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 16324
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->e(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 16323
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 16329
    :cond_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 16330
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->t()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16335
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 16336
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->v()Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ag;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16341
    :cond_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16345
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 2

    .prologue
    .line 16197
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v0

    .line 16198
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 16199
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 16201
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 16205
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 16206
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16207
    const/4 v1, 0x0

    .line 16208
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_b

    .line 16211
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16212
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 16213
    or-int/lit8 v0, v0, 0x2

    .line 16215
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16216
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 16217
    or-int/lit8 v0, v0, 0x4

    .line 16219
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->d:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->c(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16220
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 16221
    or-int/lit8 v0, v0, 0x8

    .line 16223
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 16224
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 16225
    or-int/lit8 v0, v0, 0x10

    .line 16227
    :cond_3
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->f:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->d(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16228
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 16229
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    .line 16230
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    .line 16232
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Ljava/util/List;)Ljava/util/List;

    .line 16233
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 16234
    or-int/lit8 v0, v0, 0x20

    .line 16236
    :cond_5
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->b(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 16237
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 16238
    or-int/lit8 v0, v0, 0x40

    .line 16240
    :cond_6
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->i:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->e(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16241
    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 16242
    or-int/lit16 v0, v0, 0x80

    .line 16244
    :cond_7
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->a(Lkotlin/reflect/jvm/internal/impl/i/e$o;Lkotlin/reflect/jvm/internal/impl/i/e$ag;)Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    .line 16245
    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 16246
    or-int/lit16 v0, v0, 0x100

    .line 16248
    :cond_8
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->k:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->f(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16249
    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    .line 16250
    or-int/lit16 v0, v0, 0x200

    .line 16252
    :cond_9
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->l:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->g(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16253
    and-int/lit16 v1, v3, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_a

    .line 16254
    or-int/lit16 v0, v0, 0x400

    .line 16256
    :cond_a
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->m:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->h(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16257
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->i(Lkotlin/reflect/jvm/internal/impl/i/e$o;I)I

    .line 16258
    return-object v2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 16496
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 16528
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 16534
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->e:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 16634
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 16745
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 16751
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->h:Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 16837
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Lkotlin/reflect/jvm/internal/impl/i/e$ag;
    .locals 1

    .prologue
    .line 16843
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$o$a;->j:Lkotlin/reflect/jvm/internal/impl/i/e$ag;

    return-object v0
.end method
