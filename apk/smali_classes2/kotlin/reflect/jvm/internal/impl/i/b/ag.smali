.class public final Lkotlin/reflect/jvm/internal/impl/i/b/ag;
.super Ljava/lang/Object;
.source "TypeDeserializer.kt"


# instance fields
.field private final a:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/b/h;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

.field private final e:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/i/b/ag;Ljava/util/List;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/i/b/o;",
            "Lkotlin/reflect/jvm/internal/impl/i/b/ag;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "c"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "typeParameterProtos"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "debugName"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->e:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->f:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag$a;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/ag;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    check-cast v0, Lkotlin/d/a/b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a:Lkotlin/d/a/b;

    .line 45
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v1

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$e;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag$e;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/ag;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->b(Lkotlin/d/a/b;)Lkotlin/reflect/jvm/internal/impl/j/d;

    move-result-object v0

    check-cast v0, Lkotlin/d/a/b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b:Lkotlin/d/a/b;

    .line 50
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v2

    .line 50
    :goto_0
    iput-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->c:Ljava/util/Map;

    return-void

    .line 54
    :cond_0
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 55
    check-cast p3, Ljava/lang/Iterable;

    invoke-static {p3}, Lkotlin/a/m;->n(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v3, p0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/a/x;

    invoke-virtual {v0}, Lkotlin/a/x;->c()I

    move-result v5

    invoke-virtual {v0}, Lkotlin/a/x;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-object v1, v2

    .line 56
    check-cast v1, Ljava/util/Map;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->p()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;

    iget-object v8, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-direct {v7, v8, v0, v5}, Lkotlin/reflect/jvm/internal/impl/i/b/a/m;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/o;Lkotlin/reflect/jvm/internal/impl/i/e$ac;I)V

    invoke-interface {v1, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 58
    :cond_1
    check-cast v2, Ljava/util/Map;

    move-object p0, v3

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;I)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(I)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;)Lkotlin/reflect/jvm/internal/impl/i/b/o;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    return-object v0
.end method

.method private final a(I)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->e:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    if-eqz v0, :cond_1

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(I)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 5

    .prologue
    .line 116
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a:Lkotlin/d/a/b;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->z()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-object v0

    .line 118
    :cond_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->m()Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->A()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->B()I

    move-result v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(I)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown type parameter "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->B()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->e(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorTy\u2026 ${proto.typeParameter}\")"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->C()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 124
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v2

    .line 125
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->D()I

    move-result v1

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 126
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    :goto_1
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 127
    if-eqz v0, :cond_5

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/aq;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-nez v0, :cond_0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Deserialized type parameter "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->e(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorTy\u2026ter $name in $container\")"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 126
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 129
    :cond_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->E()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 130
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b:Lkotlin/d/a/b;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->F()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    if-eqz v0, :cond_8

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/h;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    if-nez v0, :cond_0

    .line 131
    :cond_8
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->m()Lkotlin/reflect/jvm/internal/impl/i/b/aa;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/aa;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/x;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    goto/16 :goto_0

    .line 133
    :cond_9
    const-string v0, "Unknown type"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->e(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorTy\u2026nstructor(\"Unknown type\")"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/k/ah;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 194
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;->d:Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    if-nez p1, :cond_0

    .line 196
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/ab;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v2, "c.components.moduleDescr\u2026.builtIns.nullableAnyType"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/ab;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 205
    :goto_0
    return-object v0

    .line 198
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/y;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/k/y;-><init>(Lkotlin/reflect/jvm/internal/impl/b/aq;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0

    .line 201
    :cond_1
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;

    move-result-object v0

    const-string v1, "typeArgumentProto.projection"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/k;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a$b;)Lkotlin/reflect/jvm/internal/impl/k/ap;

    move-result-object v1

    .line 202
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v0

    invoke-static {p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y$a;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 205
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/k/aj;

    const/4 v3, 0x2

    invoke-static {p0, v2, v4, v3, v4}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/k/r;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0

    .line 203
    :cond_2
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/k/aj;

    const-string v0, "No type recorded"

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->c(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/k/aj;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    goto :goto_0
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 65
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;Z)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;",
            "Lkotlin/reflect/jvm/internal/impl/k/ad;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;Z)",
            "Lkotlin/reflect/jvm/internal/impl/k/v;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x10

    const/4 v4, 0x0

    .line 142
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v0, v4

    .line 159
    :goto_0
    if-eqz v0, :cond_2

    :goto_1
    return-object v0

    :pswitch_0
    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v6, v4

    .line 144
    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    .line 145
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/ag$b;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/p;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/v;

    goto :goto_0

    :cond_0
    move-object v0, v4

    goto :goto_0

    .line 149
    :pswitch_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 150
    if-ltz v0, :cond_1

    .line 151
    invoke-interface {p2}, Lkotlin/reflect/jvm/internal/impl/k/ad;->f()Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->d(I)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    const-string v0, "functionTypeConstructor.\u2026on(arity).typeConstructor"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    move-object v2, p3

    move v3, p4

    move-object v6, v4

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v4

    :cond_1
    move-object v0, v4

    .line 150
    goto :goto_0

    .line 159
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Bad suspend function in metadata with constructor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorTy\u2026eConstructor\", arguments)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final b(I)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v1

    const-string v2, "id"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    .line 172
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    const-string v2, "id"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/s;->a(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    goto :goto_0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/i/b/ag;I)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d(I)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 76
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method private final c(I)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->h()Lkotlin/reflect/jvm/internal/impl/i/b/v;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/v;->a()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    .line 179
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d(I)Lkotlin/reflect/jvm/internal/impl/b/h;
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->c(I)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 189
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->c()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v1

    const-string v2, "id"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/s;->b(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/ap;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/h;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 5

    .prologue
    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalAnnotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->e()Lkotlin/reflect/jvm/internal/impl/i/b/x;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->t()I

    move-result v1

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/x;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 68
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v2

    invoke-static {p1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {p0, v2, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v2

    .line 70
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->k()Lkotlin/reflect/jvm/internal/impl/i/b/t;

    move-result-object v3

    const-string v4, "id"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, p1, v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/i/b/t;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    goto :goto_0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const-string v0, "proto"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalAnnotations"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->z()I

    move-result v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->c(I)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    .line 83
    :goto_0
    if-eqz v0, :cond_3

    .line 111
    :cond_0
    :goto_1
    return-object v0

    .line 79
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->E()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->F()I

    move-result v0

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->c(I)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v4

    .line 80
    goto :goto_0

    .line 85
    :cond_3
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    .line 86
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 87
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/k/ad;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorTy\u2026.toString(), constructor)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 90
    :cond_4
    new-instance v5, Lkotlin/reflect/jvm/internal/impl/i/b/a/b;

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->c()Lkotlin/reflect/jvm/internal/impl/j/i;

    move-result-object v2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;

    invoke-direct {v0, p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag$d;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/ag;Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v5, v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/a/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/d/a/a;)V

    .line 96
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/b/ag$c;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag$c;-><init>(Lkotlin/reflect/jvm/internal/impl/i/b/ag;)V

    .line 99
    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/b/ag$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$y;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 211
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 212
    const/4 v3, 0x0

    .line 213
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 214
    add-int/lit8 v6, v3, 0x1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y$a;

    .line 100
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/k/ad;->b()Ljava/util/List;

    move-result-object v8

    invoke-static {v8, v3}, Lkotlin/a/m;->c(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/aq;

    invoke-direct {p0, v3, v0}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/aq;Lkotlin/reflect/jvm/internal/impl/i/e$y$a;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v3, v6

    .line 213
    goto :goto_2

    .line 215
    :cond_5
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    .line 101
    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 103
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c;->a:Lkotlin/reflect/jvm/internal/impl/i/c$a;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->P()I

    move-result v3

    invoke-virtual {v0, v3}, Lkotlin/reflect/jvm/internal/impl/i/c$a;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    const-string v3, "Flags.SUSPEND_TYPE.get(proto.flags)"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v5

    .line 104
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    .line 110
    :goto_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->d:Lkotlin/reflect/jvm/internal/impl/i/b/o;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->g()Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    move-result-object v1

    invoke-static {p1, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/af;->c(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/i/b/ah;)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p0, v1, p2}, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->b(Lkotlin/reflect/jvm/internal/impl/i/e$y;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/x;->a(Lkotlin/reflect/jvm/internal/impl/k/v;Lkotlin/reflect/jvm/internal/impl/k/v;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    move-object v0, v5

    .line 107
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->r()Z

    move-result v3

    const/16 v5, 0x10

    move-object v6, v4

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->e:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ". Child of "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->e:Lkotlin/reflect/jvm/internal/impl/i/b/ag;

    iget-object v2, v2, Lkotlin/reflect/jvm/internal/impl/i/b/ag;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
