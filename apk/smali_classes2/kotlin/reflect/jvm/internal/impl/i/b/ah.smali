.class public final Lkotlin/reflect/jvm/internal/impl/i/b/ah;
.super Ljava/lang/Object;
.source "TypeTable.kt"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)V
    .locals 6

    .prologue
    const-string v0, "typeTable"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p0

    .line 22
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ah;

    .line 23
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->h()Ljava/util/List;

    move-result-object v0

    .line 24
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->o()I

    move-result v4

    .line 26
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->h()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 39
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 40
    const/4 v2, 0x0

    .line 41
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 42
    add-int/lit8 v3, v2, 0x1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    .line 27
    if-lt v2, v4, :cond_0

    .line 28
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->S()Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->a(Z)Lkotlin/reflect/jvm/internal/impl/i/e$y$c;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$y$c;->m()Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v0

    .line 30
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 41
    goto :goto_0

    .line 43
    :cond_1
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    .line 22
    :goto_1
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a:Ljava/util/List;

    return-void

    .line 33
    :cond_2
    const-string v1, "originalTypes"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/ah;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method
