.class public final Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$b",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$c;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$c$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$d;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$ac;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$y;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$i;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$o;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$z;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$g;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

.field private p:I

.field private q:Lkotlin/reflect/jvm/internal/impl/i/e$u;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9301
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;-><init>()V

    .line 9651
    const/4 v0, 0x6

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->b:I

    .line 9783
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    .line 9908
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    .line 10033
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    .line 10099
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    .line 10165
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    .line 10290
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    .line 10415
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    .line 10540
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    .line 10665
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    .line 10790
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    .line 10856
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 10964
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->q:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 9302
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->z()V

    .line 9303
    return-void
.end method

.method private static A()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 1

    .prologue
    .line 9308
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;-><init>()V

    return-object v0
.end method

.method private B()V
    .locals 2

    .prologue
    .line 9786
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 9787
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    .line 9788
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9790
    :cond_0
    return-void
.end method

.method private C()V
    .locals 2

    .prologue
    .line 9911
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 9912
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    .line 9913
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9915
    :cond_0
    return-void
.end method

.method private D()V
    .locals 2

    .prologue
    .line 10035
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 10036
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    .line 10037
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10039
    :cond_0
    return-void
.end method

.method private E()V
    .locals 2

    .prologue
    .line 10101
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_0

    .line 10102
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    .line 10103
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10105
    :cond_0
    return-void
.end method

.method private F()V
    .locals 2

    .prologue
    .line 10168
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 10169
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    .line 10170
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10172
    :cond_0
    return-void
.end method

.method private G()V
    .locals 2

    .prologue
    .line 10293
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_0

    .line 10294
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    .line 10295
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10297
    :cond_0
    return-void
.end method

.method private H()V
    .locals 2

    .prologue
    .line 10418
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    .line 10419
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    .line 10420
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10422
    :cond_0
    return-void
.end method

.method private I()V
    .locals 2

    .prologue
    .line 10543
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-eq v0, v1, :cond_0

    .line 10544
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    .line 10545
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10547
    :cond_0
    return-void
.end method

.method private J()V
    .locals 2

    .prologue
    .line 10668
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-eq v0, v1, :cond_0

    .line 10669
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    .line 10670
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10672
    :cond_0
    return-void
.end method

.method private K()V
    .locals 2

    .prologue
    .line 10792
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-eq v0, v1, :cond_0

    .line 10793
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    .line 10794
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10796
    :cond_0
    return-void
.end method

.method static synthetic y()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 1

    .prologue
    .line 9295
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->A()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method private z()V
    .locals 0

    .prologue
    .line 9306
    return-void
.end method


# virtual methods
.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 1

    .prologue
    .line 9695
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9696
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->b:I

    .line 9698
    return-object p0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 2

    .prologue
    .line 10895
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->f()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 10897
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 10903
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10904
    return-object p0

    .line 10900
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 2

    .prologue
    .line 9447
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->m()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 9569
    :goto_0
    return-object p0

    .line 9448
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9449
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    .line 9451
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 9452
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->r()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->b(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    .line 9454
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 9455
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->t()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->c(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    .line 9457
    :cond_3
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 9458
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 9459
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    .line 9460
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9467
    :cond_4
    :goto_1
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->c(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 9468
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 9469
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->c(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    .line 9470
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9477
    :cond_5
    :goto_2
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->d(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 9478
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 9479
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->d(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    .line 9480
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9487
    :cond_6
    :goto_3
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->e(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 9488
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 9489
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->e(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    .line 9490
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9497
    :cond_7
    :goto_4
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->f(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 9498
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 9499
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->f(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    .line 9500
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9507
    :cond_8
    :goto_5
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->g(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 9508
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 9509
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->g(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    .line 9510
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9517
    :cond_9
    :goto_6
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->h(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 9518
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 9519
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->h(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    .line 9520
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9527
    :cond_a
    :goto_7
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->i(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 9528
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 9529
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->i(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    .line 9530
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9537
    :cond_b
    :goto_8
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->j(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 9538
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 9539
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->j(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    .line 9540
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9547
    :cond_c
    :goto_9
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->k(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 9548
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 9549
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->k(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    .line 9550
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9557
    :cond_d
    :goto_a
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->L()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 9558
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->M()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    .line 9560
    :cond_e
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->N()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 9561
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->O()I

    move-result v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    .line 9563
    :cond_f
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->P()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 9564
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->Q()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    .line 9566
    :cond_10
    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/h$c;)V

    .line 9567
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->l(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto/16 :goto_0

    .line 9462
    :cond_11
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->B()V

    .line 9463
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 9472
    :cond_12
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->C()V

    .line 9473
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->c(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 9482
    :cond_13
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->D()V

    .line 9483
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->d(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 9492
    :cond_14
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->E()V

    .line 9493
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->e(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    .line 9502
    :cond_15
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->F()V

    .line 9503
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->f(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    .line 9512
    :cond_16
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->G()V

    .line 9513
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->g(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    .line 9522
    :cond_17
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->H()V

    .line 9523
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->h(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    .line 9532
    :cond_18
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->I()V

    .line 9533
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->i(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    .line 9542
    :cond_19
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->J()V

    .line 9543
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->j(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_9

    .line 9552
    :cond_1a
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->K()V

    .line 9553
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->k(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 3

    .prologue
    const v2, 0x8000

    .line 11003
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->q:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 11005
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->q:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->q:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 11011
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/2addr v0, v2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 11012
    return-object p0

    .line 11008
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->q:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 9295
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9295
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 9295
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$c;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 1

    .prologue
    .line 9736
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9737
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->c:I

    .line 9739
    return-object p0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 9295
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9295
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 1

    .prologue
    .line 9768
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9769
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->d:I

    .line 9771
    return-object p0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9636
    const/4 v2, 0x0

    .line 9638
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 9643
    if-eqz v0, :cond_0

    .line 9644
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    .line 9647
    :cond_0
    return-object p0

    .line 9639
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 9640
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 9641
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 9643
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 9644
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    :cond_1
    throw v0

    .line 9643
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 9295
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9295
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;
    .locals 1

    .prologue
    .line 9808
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    return-object v0
.end method

.method public e(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;
    .locals 1

    .prologue
    .line 9933
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$y;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 9295
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    return-object v0
.end method

.method public f(I)Lkotlin/reflect/jvm/internal/impl/i/e$e;
    .locals 1

    .prologue
    .line 10190
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$e;

    return-object v0
.end method

.method public synthetic f()Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;
    .locals 1

    .prologue
    .line 9295
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method public g(I)Lkotlin/reflect/jvm/internal/impl/i/e$i;
    .locals 1

    .prologue
    .line 10315
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$i;

    return-object v0
.end method

.method public h(I)Lkotlin/reflect/jvm/internal/impl/i/e$o;
    .locals 1

    .prologue
    .line 10440
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$o;

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 9295
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    return-object v0
.end method

.method public i(I)Lkotlin/reflect/jvm/internal/impl/i/e$z;
    .locals 1

    .prologue
    .line 10565
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$z;

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 9295
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 2

    .prologue
    .line 9349
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->A()Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;

    move-result-object v0

    return-object v0
.end method

.method public j(I)Lkotlin/reflect/jvm/internal/impl/i/e$g;
    .locals 1

    .prologue
    .line 10690
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$g;

    return-object v0
.end method

.method public k(I)Lkotlin/reflect/jvm/internal/impl/i/e$c$a;
    .locals 1

    .prologue
    .line 10945
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 10946
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->p:I

    .line 10948
    return-object p0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$c;
    .locals 1

    .prologue
    .line 9353
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->m()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9573
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 9629
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 9577
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->p()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 9578
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->d(I)Lkotlin/reflect/jvm/internal/impl/i/e$ac;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$ac;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9577
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 9583
    :goto_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->q()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 9584
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e(I)Lkotlin/reflect/jvm/internal/impl/i/e$y;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$y;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9583
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    .line 9589
    :goto_3
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->r()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 9590
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f(I)Lkotlin/reflect/jvm/internal/impl/i/e$e;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$e;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9589
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    move v0, v1

    .line 9595
    :goto_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->s()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 9596
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g(I)Lkotlin/reflect/jvm/internal/impl/i/e$i;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$i;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9595
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    move v0, v1

    .line 9601
    :goto_5
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->t()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 9602
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h(I)Lkotlin/reflect/jvm/internal/impl/i/e$o;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$o;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9601
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_6
    move v0, v1

    .line 9607
    :goto_6
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->u()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 9608
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i(I)Lkotlin/reflect/jvm/internal/impl/i/e$z;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$z;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9607
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_7
    move v0, v1

    .line 9613
    :goto_7
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->v()I

    move-result v2

    if-ge v0, v2, :cond_8

    .line 9614
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j(I)Lkotlin/reflect/jvm/internal/impl/i/e$g;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/i/e$g;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9613
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 9619
    :cond_8
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->w()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 9620
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->x()Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$ae;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9625
    :cond_9
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9629
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$c;
    .locals 2

    .prologue
    .line 9357
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n()Lkotlin/reflect/jvm/internal/impl/i/e$c;

    move-result-object v0

    .line 9358
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 9359
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 9361
    :cond_0
    return-object v0
.end method

.method public n()Lkotlin/reflect/jvm/internal/impl/i/e$c;
    .locals 6

    .prologue
    const v5, 0x8000

    const/4 v0, 0x1

    .line 9365
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/e$c;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$b;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 9366
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9367
    const/4 v1, 0x0

    .line 9368
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_f

    .line 9371
    :goto_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->b:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;I)I

    .line 9372
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 9373
    or-int/lit8 v0, v0, 0x2

    .line 9375
    :cond_0
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->c:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b(Lkotlin/reflect/jvm/internal/impl/i/e$c;I)I

    .line 9376
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 9377
    or-int/lit8 v0, v0, 0x4

    .line 9379
    :cond_1
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->d:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->c(Lkotlin/reflect/jvm/internal/impl/i/e$c;I)I

    .line 9380
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 9381
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    .line 9382
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9384
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9385
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 9386
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    .line 9387
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9389
    :cond_3
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->b(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9390
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 9391
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    .line 9392
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9394
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->g:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->c(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9395
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 9396
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    .line 9397
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9399
    :cond_5
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->h:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->d(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9400
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 9401
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    .line 9402
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9404
    :cond_6
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->e(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9405
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    .line 9406
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    .line 9407
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9409
    :cond_7
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->f(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9410
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    .line 9411
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    .line 9412
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9414
    :cond_8
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->g(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9415
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    .line 9416
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    .line 9417
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, -0x401

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9419
    :cond_9
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->h(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9420
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    .line 9421
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    .line 9422
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, -0x801

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9424
    :cond_a
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->i(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9425
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    .line 9426
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    .line 9427
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v1, v1, -0x1001

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    .line 9429
    :cond_b
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->n:Ljava/util/List;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->j(Lkotlin/reflect/jvm/internal/impl/i/e$c;Ljava/util/List;)Ljava/util/List;

    .line 9430
    and-int/lit16 v1, v3, 0x2000

    const/16 v4, 0x2000

    if-ne v1, v4, :cond_c

    .line 9431
    or-int/lit8 v0, v0, 0x8

    .line 9433
    :cond_c
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;Lkotlin/reflect/jvm/internal/impl/i/e$ae;)Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    .line 9434
    and-int/lit16 v1, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v1, v4, :cond_d

    .line 9435
    or-int/lit8 v0, v0, 0x10

    .line 9437
    :cond_d
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->p:I

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->d(Lkotlin/reflect/jvm/internal/impl/i/e$c;I)I

    .line 9438
    and-int v1, v3, v5

    if-ne v1, v5, :cond_e

    .line 9439
    or-int/lit8 v0, v0, 0x20

    .line 9441
    :cond_e
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->q:Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$c;Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u;

    .line 9442
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$c;->e(Lkotlin/reflect/jvm/internal/impl/i/e$c;I)I

    .line 9443
    return-object v2

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 9724
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 9802
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 9927
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 10184
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 10309
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 10434
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 10559
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public v()I
    .locals 1

    .prologue
    .line 10684
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 10861
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/i/e$ae;
    .locals 1

    .prologue
    .line 10867
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$c$a;->o:Lkotlin/reflect/jvm/internal/impl/i/e$ae;

    return-object v0
.end method
