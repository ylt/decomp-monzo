.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$e;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/c/c$f;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

.field private c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

.field private d:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

.field private e:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3408
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 3513
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->b:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    .line 3573
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3657
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3717
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3409
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->n()V

    .line 3410
    return-void
.end method

.method static synthetic m()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 1

    .prologue
    .line 3401
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 3413
    return-void
.end method

.method private static o()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 1

    .prologue
    .line 3415
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 2

    .prologue
    .line 3552
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->b:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 3554
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->b:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$a$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->b:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    .line 3560
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    .line 3561
    return-object p0

    .line 3557
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->b:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 2

    .prologue
    .line 3632
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 3634
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3640
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    .line 3641
    return-object p0

    .line 3637
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 2

    .prologue
    .line 3472
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 3487
    :goto_0
    return-object p0

    .line 3473
    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3474
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->m()Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    .line 3476
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3477
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    .line 3479
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3480
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->q()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    .line 3482
    :cond_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3483
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->s()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    .line 3485
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 3401
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3401
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 3401
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public b(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 2

    .prologue
    .line 3696
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 3698
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3704
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    .line 3705
    return-object p0

    .line 3701
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    goto :goto_0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 3401
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3401
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 2

    .prologue
    .line 3756
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 3758
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$c$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3764
    :goto_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    .line 3765
    return-object p0

    .line 3761
    :cond_0
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    goto :goto_0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3498
    const/4 v2, 0x0

    .line 3500
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3505
    if-eqz v0, :cond_0

    .line 3506
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    .line 3509
    :cond_0
    return-object p0

    .line 3501
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 3502
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3503
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3505
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 3506
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    :cond_1
    throw v0

    .line 3505
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 3401
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3401
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 3401
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;
    .locals 2

    .prologue
    .line 3432
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->o()Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;)Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
    .locals 1

    .prologue
    .line 3436
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->f()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 3401
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->j()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 3401
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
    .locals 2

    .prologue
    .line 3440
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->k()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    move-result-object v0

    .line 3441
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3442
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 3444
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/c/c$e;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3448
    new-instance v2, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V

    .line 3449
    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->a:I

    .line 3450
    const/4 v1, 0x0

    .line 3451
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 3454
    :goto_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->b:Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/c/c$a;)Lkotlin/reflect/jvm/internal/impl/i/c/c$a;

    .line 3455
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 3456
    or-int/lit8 v0, v0, 0x2

    .line 3458
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3459
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 3460
    or-int/lit8 v0, v0, 0x4

    .line 3462
    :cond_1
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->d:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->b(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3463
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 3464
    or-int/lit8 v0, v0, 0x8

    .line 3466
    :cond_2
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$e$a;->e:Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    invoke-static {v2, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->c(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;Lkotlin/reflect/jvm/internal/impl/i/c/c$c;)Lkotlin/reflect/jvm/internal/impl/i/c/c$c;

    .line 3467
    invoke-static {v2, v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$e;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$e;I)I

    .line 3468
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 3491
    const/4 v0, 0x1

    return v0
.end method
