.class final Lkotlin/reflect/jvm/internal/impl/i/b/w$b;
.super Lkotlin/d/b/m;
.source "MemberDeserializer.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Lkotlin/reflect/jvm/internal/impl/b/a/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/List",
        "<+",
        "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/i/b/w;

.field final synthetic b:Lkotlin/reflect/jvm/internal/impl/protobuf/o;

.field final synthetic c:Lkotlin/reflect/jvm/internal/impl/i/b/b;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)V
    .locals 1

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/w;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->c:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/w;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/w;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/i/b/w;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->f()Lkotlin/reflect/jvm/internal/impl/b/m;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/i/b/w;Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/i/b/ae;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/b/ae;

    .line 238
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->a:Lkotlin/reflect/jvm/internal/impl/i/b/w;

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/w;->a(Lkotlin/reflect/jvm/internal/impl/i/b/w;)Lkotlin/reflect/jvm/internal/impl/i/b/o;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/o;->d()Lkotlin/reflect/jvm/internal/impl/i/b/m;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/b/m;->f()Lkotlin/reflect/jvm/internal/impl/i/b/c;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->c:Lkotlin/reflect/jvm/internal/impl/i/b/b;

    .line 239
    invoke-interface {v1, v0, v2, v3}, Lkotlin/reflect/jvm/internal/impl/i/b/c;->b(Lkotlin/reflect/jvm/internal/impl/i/b/ae;Lkotlin/reflect/jvm/internal/impl/protobuf/o;Lkotlin/reflect/jvm/internal/impl/i/b/b;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 240
    nop

    .line 280
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 281
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 282
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/c;

    .line 240
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/a/g;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/b/a/e;->f:Lkotlin/reflect/jvm/internal/impl/b/a/e;

    invoke-direct {v3, v0, v4}, Lkotlin/reflect/jvm/internal/impl/b/a/g;-><init>(Lkotlin/reflect/jvm/internal/impl/b/a/c;Lkotlin/reflect/jvm/internal/impl/b/a/e;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 283
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 237
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    .line 241
    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    return-object v0

    .line 237
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 241
    :cond_2
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_2
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/b/w$b;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
