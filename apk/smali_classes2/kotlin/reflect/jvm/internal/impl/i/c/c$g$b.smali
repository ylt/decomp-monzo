.class public final Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "JvmProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/c/c$g$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/c/c$g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;,
        Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/lang/Object;

.field private i:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m:I

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 464
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 1550
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    .line 1551
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->B()V

    .line 1552
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/16 v9, 0x20

    const/16 v8, 0x10

    const/4 v2, -0x1

    .line 347
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 727
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->k:I

    .line 765
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->m:I

    .line 775
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->n:B

    .line 817
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->o:I

    .line 348
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->B()V

    .line 350
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v3

    .line 352
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v4

    move v2, v0

    .line 357
    :cond_0
    :goto_0
    if-nez v0, :cond_a

    .line 358
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v5

    .line 359
    sparse-switch v5, :sswitch_data_0

    .line 364
    invoke-virtual {p0, p1, v4, p2, v5}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 366
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 362
    goto :goto_0

    .line 371
    :sswitch_1
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    .line 372
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v5

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->f:I
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 442
    :catch_0
    move-exception v0

    .line 443
    :try_start_1
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v2, 0x10

    if-ne v1, v8, :cond_1

    .line 449
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    .line 451
    :cond_1
    and-int/lit8 v1, v2, 0x20

    if-ne v1, v9, :cond_2

    .line 452
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    .line 455
    :cond_2
    :try_start_2
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 459
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 461
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d()V

    throw v0

    .line 376
    :sswitch_2
    :try_start_3
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    .line 377
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v5

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->g:I
    :try_end_3
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 444
    :catch_1
    move-exception v0

    .line 445
    :try_start_4
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 381
    :sswitch_3
    :try_start_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->n()I

    move-result v6

    .line 382
    invoke-static {v6}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;->a(I)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    move-result-object v7

    .line 383
    if-nez v7, :cond_3

    .line 384
    invoke-virtual {v4, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 385
    invoke-virtual {v4, v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    goto :goto_0

    .line 387
    :cond_3
    iget v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    .line 388
    iput-object v7, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    goto/16 :goto_0

    .line 393
    :sswitch_4
    and-int/lit8 v5, v2, 0x10

    if-eq v5, v8, :cond_4

    .line 394
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    .line 395
    or-int/lit8 v2, v2, 0x10

    .line 397
    :cond_4
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 401
    :sswitch_5
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->s()I

    move-result v5

    .line 402
    invoke-virtual {p1, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->c(I)I

    move-result v5

    .line 403
    and-int/lit8 v6, v2, 0x10

    if-eq v6, v8, :cond_5

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v6

    if-lez v6, :cond_5

    .line 404
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    .line 405
    or-int/lit8 v2, v2, 0x10

    .line 407
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v6

    if-lez v6, :cond_6

    .line 408
    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 410
    :cond_6
    invoke-virtual {p1, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->d(I)V

    goto/16 :goto_0

    .line 414
    :sswitch_6
    and-int/lit8 v5, v2, 0x20

    if-eq v5, v9, :cond_7

    .line 415
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    .line 416
    or-int/lit8 v2, v2, 0x20

    .line 418
    :cond_7
    iget-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 422
    :sswitch_7
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->s()I

    move-result v5

    .line 423
    invoke-virtual {p1, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->c(I)I

    move-result v5

    .line 424
    and-int/lit8 v6, v2, 0x20

    if-eq v6, v9, :cond_8

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v6

    if-lez v6, :cond_8

    .line 425
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    .line 426
    or-int/lit8 v2, v2, 0x20

    .line 428
    :cond_8
    :goto_3
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->x()I

    move-result v6

    if-lez v6, :cond_9

    .line 429
    iget-object v6, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 431
    :cond_9
    invoke-virtual {p1, v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->d(I)V

    goto/16 :goto_0

    .line 435
    :sswitch_8
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->l()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v5

    .line 436
    iget v6, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    .line 437
    iput-object v5, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h:Ljava/lang/Object;
    :try_end_5
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 448
    :cond_a
    and-int/lit8 v0, v2, 0x10

    if-ne v0, v8, :cond_b

    .line 449
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    .line 451
    :cond_b
    and-int/lit8 v0, v2, 0x20

    if-ne v0, v9, :cond_c

    .line 452
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    .line 455
    :cond_c
    :try_start_6
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 459
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 461
    :goto_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d()V

    .line 463
    return-void

    .line 456
    :catch_2
    move-exception v0

    .line 459
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_4

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 456
    :catch_3
    move-exception v1

    .line 459
    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto/16 :goto_1

    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 359
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x2a -> :sswitch_7
        0x32 -> :sswitch_8
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 323
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 329
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 727
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->k:I

    .line 765
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->m:I

    .line 775
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->n:B

    .line 817
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->o:I

    .line 330
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 331
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/c/c$1;)V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 332
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 727
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->k:I

    .line 765
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->m:I

    .line 775
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->n:B

    .line 817
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->o:I

    .line 332
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method private B()V
    .locals 1

    .prologue
    .line 768
    const/4 v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->f:I

    .line 769
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->g:I

    .line 770
    const-string v0, ""

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h:Ljava/lang/Object;

    .line 771
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;->a:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    .line 772
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    .line 773
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    .line 774
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;I)I
    .locals 0

    .prologue
    .line 323
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->f:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 935
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->y()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;I)I
    .locals 0

    .prologue
    .line 323
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->g:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;I)I
    .locals 0

    .prologue
    .line 323
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    return p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/util/List;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Ljava/util/List;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    return-object v0
.end method

.method public static y()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 932
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;->m()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 937
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->a(Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;)Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 787
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->i()I

    .line 788
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 789
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->f:I

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 791
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 792
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->g:I

    invoke-virtual {p1, v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 794
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 795
    const/4 v0, 0x3

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(II)V

    .line 797
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 798
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 799
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->k:I

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    :cond_3
    move v1, v2

    .line 801
    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 802
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(I)V

    .line 801
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 804
    :cond_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->w()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 805
    const/16 v0, 0x2a

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 806
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->m:I

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 808
    :cond_5
    :goto_1
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 809
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(I)V

    .line 808
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 811
    :cond_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    .line 812
    const/4 v0, 0x6

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->r()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(ILkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 814
    :cond_7
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 815
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 476
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;
    .locals 1

    .prologue
    .line 340
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->c:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 575
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 819
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->o:I

    .line 820
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 869
    :goto_0
    return v0

    .line 823
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 824
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->f:I

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 827
    :goto_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 828
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->g:I

    invoke-static {v4, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 831
    :cond_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_7

    .line 832
    const/4 v2, 0x3

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;->a()I

    move-result v3

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    :goto_2
    move v3, v1

    move v4, v1

    .line 837
    :goto_3
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 838
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->g(I)I

    move-result v0

    add-int/2addr v4, v0

    .line 837
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 841
    :cond_2
    add-int v0, v2, v4

    .line 842
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->u()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 843
    add-int/lit8 v0, v0, 0x1

    .line 844
    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->g(I)I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    .line 847
    :goto_4
    iput v4, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->k:I

    move v3, v1

    .line 851
    :goto_5
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 852
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->g(I)I

    move-result v0

    add-int/2addr v0, v3

    .line 851
    add-int/lit8 v1, v1, 0x1

    move v3, v0

    goto :goto_5

    .line 855
    :cond_3
    add-int v0, v2, v3

    .line 856
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->w()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 857
    add-int/lit8 v0, v0, 0x1

    .line 858
    invoke-static {v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->g(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 861
    :cond_4
    iput v3, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->m:I

    .line 863
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    .line 864
    const/4 v1, 0x6

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->r()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 867
    :cond_5
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 868
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->o:I

    goto/16 :goto_0

    :cond_6
    move v2, v0

    goto :goto_4

    :cond_7
    move v2, v0

    goto/16 :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->z()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->A()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 777
    iget-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->n:B

    .line 778
    if-ne v1, v0, :cond_0

    .line 782
    :goto_0
    return v0

    .line 779
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 781
    :cond_1
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->n:B

    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 585
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->f:I

    return v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 598
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 608
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->g:I

    return v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 621
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 631
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h:Ljava/lang/Object;

    .line 632
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 633
    check-cast v0, Ljava/lang/String;

    .line 641
    :goto_0
    return-object v0

    .line 635
    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 637
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->f()Ljava/lang/String;

    move-result-object v1

    .line 638
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 639
    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 641
    goto :goto_0
.end method

.method public r()Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 2

    .prologue
    .line 653
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h:Ljava/lang/Object;

    .line 654
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 655
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    .line 658
    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->h:Ljava/lang/Object;

    .line 661
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 675
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->i:Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$b;

    return-object v0
.end method

.method public u()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 701
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    return-object v0
.end method

.method public v()I
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public w()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 741
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 876
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public z()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;
    .locals 1

    .prologue
    .line 933
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b;->y()Lkotlin/reflect/jvm/internal/impl/i/c/c$g$b$a;

    move-result-object v0

    return-object v0
.end method
