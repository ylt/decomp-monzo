.class public final Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$a$a$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$a$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;,
        Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;
    }
.end annotation


# static fields
.field public static b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;


# instance fields
.field private final d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

.field private e:I

.field private f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

.field private g:J

.field private h:F

.field private i:D

.field private j:I

.field private k:I

.field private l:I

.field private m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            ">;"
        }
    .end annotation
.end field

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2338
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$1;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$1;-><init>()V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    .line 3500
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;-><init>(Z)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    .line 3501
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->G()V

    .line 3502
    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    const/16 v10, 0x100

    .line 2231
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 2695
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o:B

    .line 2750
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->p:I

    .line 2232
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->G()V

    .line 2234
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->i()Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;

    move-result-object v5

    .line 2236
    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;)Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;

    move-result-object v6

    move v3, v2

    .line 2241
    :goto_0
    if-nez v2, :cond_2

    .line 2242
    :try_start_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a()I

    move-result v0

    .line 2243
    sparse-switch v0, :sswitch_data_0

    .line 2248
    invoke-virtual {p0, p1, v6, p2, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;Lkotlin/reflect/jvm/internal/impl/protobuf/f;I)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    move v2, v3

    :goto_1
    move v3, v2

    move v2, v0

    .line 2318
    goto :goto_0

    :sswitch_0
    move v0, v1

    move v2, v3

    .line 2246
    goto :goto_1

    .line 2255
    :sswitch_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->n()I

    move-result v4

    .line 2256
    invoke-static {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    move-result-object v7

    .line 2257
    if-nez v7, :cond_0

    .line 2258
    invoke-virtual {v6, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    .line 2259
    invoke-virtual {v6, v4}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->o(I)V

    move v0, v2

    move v2, v3

    goto :goto_1

    .line 2261
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    .line 2262
    iput-object v7, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    move v0, v2

    move v2, v3

    .line 2264
    goto :goto_1

    .line 2267
    :sswitch_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    .line 2268
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->r()J

    move-result-wide v8

    iput-wide v8, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->g:J

    move v0, v2

    move v2, v3

    .line 2269
    goto :goto_1

    .line 2272
    :sswitch_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    .line 2273
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->c()F

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->h:F

    move v0, v2

    move v2, v3

    .line 2274
    goto :goto_1

    .line 2277
    :sswitch_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    .line 2278
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->b()D

    move-result-wide v8

    iput-wide v8, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->i:D

    move v0, v2

    move v2, v3

    .line 2279
    goto :goto_1

    .line 2282
    :sswitch_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    .line 2283
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->j:I

    move v0, v2

    move v2, v3

    .line 2284
    goto :goto_1

    .line 2287
    :sswitch_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    .line 2288
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->k:I

    move v0, v2

    move v2, v3

    .line 2289
    goto :goto_1

    .line 2292
    :sswitch_7
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    .line 2293
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->f()I

    move-result v0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l:I

    move v0, v2

    move v2, v3

    .line 2294
    goto :goto_1

    .line 2297
    :sswitch_8
    const/4 v0, 0x0

    .line 2298
    iget v4, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit16 v4, v4, 0x80

    const/16 v7, 0x80

    if-ne v4, v7, :cond_6

    .line 2299
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->r()Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    move-result-object v0

    move-object v4, v0

    .line 2301
    :goto_2
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v0, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 2302
    if-eqz v4, :cond_1

    .line 2303
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    invoke-virtual {v4, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$c;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a$c;

    .line 2304
    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/i/e$a$c;->k()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 2306
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    move v0, v2

    move v2, v3

    .line 2307
    goto/16 :goto_1

    .line 2310
    :sswitch_9
    and-int/lit16 v0, v3, 0x100

    if-eq v0, v10, :cond_5

    .line 2311
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2312
    or-int/lit16 v0, v3, 0x100

    .line 2314
    :goto_3
    :try_start_1
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-virtual {p1, v4, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/e;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/q;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move v11, v2

    move v2, v0

    move v0, v11

    goto/16 :goto_1

    .line 2325
    :cond_2
    and-int/lit16 v0, v3, 0x100

    if-ne v0, v10, :cond_3

    .line 2326
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    .line 2329
    :cond_3
    :try_start_2
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2333
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2335
    :goto_4
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d()V

    .line 2337
    return-void

    .line 2330
    :catch_0
    move-exception v0

    .line 2333
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_4

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 2319
    :catch_1
    move-exception v0

    .line 2320
    :goto_5
    :try_start_3
    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2325
    :catchall_1
    move-exception v0

    :goto_6
    and-int/lit16 v1, v3, 0x100

    if-ne v1, v10, :cond_4

    .line 2326
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    .line 2329
    :cond_4
    :try_start_4
    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2333
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2335
    :goto_7
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d()V

    throw v0

    .line 2321
    :catch_2
    move-exception v0

    .line 2322
    :goto_8
    :try_start_5
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2330
    :catch_3
    move-exception v1

    .line 2333
    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    goto :goto_7

    :catchall_2
    move-exception v0

    invoke-virtual {v5}, Lkotlin/reflect/jvm/internal/impl/protobuf/d$b;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    throw v0

    .line 2325
    :catchall_3
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_6

    .line 2321
    :catch_4
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_8

    .line 2319
    :catch_5
    move-exception v1

    move v3, v0

    move-object v0, v1

    goto :goto_5

    :cond_5
    move v0, v3

    goto/16 :goto_3

    :cond_6
    move-object v4, v0

    goto/16 :goto_2

    :cond_7
    move v0, v2

    move v2, v3

    goto/16 :goto_1

    .line 2243
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x21 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2207
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2213
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    .line 2695
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o:B

    .line 2750
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->p:I

    .line 2214
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    .line 2215
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V
    .locals 0

    .prologue
    .line 2207
    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2216
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;-><init>()V

    .line 2695
    iput-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o:B

    .line 2750
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->p:I

    .line 2216
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-void
.end method

.method public static D()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 2857
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->p()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method private G()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2685
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->a:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    .line 2686
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->g:J

    .line 2687
    const/4 v0, 0x0

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->h:F

    .line 2688
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->i:D

    .line 2689
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->j:I

    .line 2690
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->k:I

    .line 2691
    iput v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l:I

    .line 2692
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    .line 2693
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    .line 2694
    return-void
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;D)D
    .locals 1

    .prologue
    .line 2207
    iput-wide p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->i:D

    return-wide p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;F)F
    .locals 0

    .prologue
    .line 2207
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->h:F

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;I)I
    .locals 0

    .prologue
    .line 2207
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->j:I

    return p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;J)J
    .locals 1

    .prologue
    .line 2207
    iput-wide p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->g:J

    return-wide p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 2207
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    return-object p1
.end method

.method public static a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 2860
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->D()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;
    .locals 0

    .prologue
    .line 2207
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    return-object p1
.end method

.method static synthetic a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;Lkotlin/reflect/jvm/internal/impl/i/e$a;)Lkotlin/reflect/jvm/internal/impl/i/e$a;
    .locals 0

    .prologue
    .line 2207
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    return-object p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;I)I
    .locals 0

    .prologue
    .line 2207
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->k:I

    return p1
.end method

.method static synthetic b(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Ljava/util/List;
    .locals 1

    .prologue
    .line 2207
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;I)I
    .locals 0

    .prologue
    .line 2207
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l:I

    return p1
.end method

.method static synthetic c(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;
    .locals 1

    .prologue
    .line 2207
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    return-object v0
.end method

.method static synthetic d(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;I)I
    .locals 0

    .prologue
    .line 2207
    iput p1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    return p1
.end method

.method public static f()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 1

    .prologue
    .line 2220
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    return-object v0
.end method


# virtual methods
.method public A()Lkotlin/reflect/jvm/internal/impl/i/e$a;
    .locals 1

    .prologue
    .line 2646
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    return-object v0
.end method

.method public B()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2655
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    return-object v0
.end method

.method public C()I
    .locals 1

    .prologue
    .line 2668
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public E()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 2858
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->D()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public F()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;
    .locals 1

    .prologue
    .line 2862
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 1

    .prologue
    .line 2674
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2719
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->i()I

    .line 2720
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2721
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->a()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(II)V

    .line 2723
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2724
    iget-wide v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->g:J

    invoke-virtual {p1, v2, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(IJ)V

    .line 2726
    :cond_1
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2727
    const/4 v0, 0x3

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->h:F

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(IF)V

    .line 2729
    :cond_2
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 2730
    iget-wide v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->i:D

    invoke-virtual {p1, v3, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(ID)V

    .line 2732
    :cond_3
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2733
    const/4 v0, 0x5

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->j:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 2735
    :cond_4
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 2736
    const/4 v0, 0x6

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->k:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 2738
    :cond_5
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 2739
    const/4 v0, 0x7

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l:I

    invoke-virtual {p1, v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->a(II)V

    .line 2741
    :cond_6
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 2742
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    invoke-virtual {p1, v4, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 2744
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 2745
    const/16 v2, 0x9

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-virtual {p1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)V

    .line 2744
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2747
    :cond_8
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {p1, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)V

    .line 2748
    return-void
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/protobuf/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/jvm/internal/impl/protobuf/q",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2350
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 2207
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;
    .locals 1

    .prologue
    .line 2224
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->c:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2521
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2752
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->p:I

    .line 2753
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2794
    :goto_0
    return v0

    .line 2756
    :cond_0
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 2757
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;->a()I

    move-result v0

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->e(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 2760
    :goto_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2761
    iget-wide v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->g:J

    invoke-static {v4, v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2764
    :cond_1
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 2765
    const/4 v2, 0x3

    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->h:F

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(IF)I

    move-result v2

    add-int/2addr v0, v2

    .line 2768
    :cond_2
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 2769
    iget-wide v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->i:D

    invoke-static {v5, v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->b(ID)I

    move-result v2

    add-int/2addr v0, v2

    .line 2772
    :cond_3
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 2773
    const/4 v2, 0x5

    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->j:I

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2776
    :cond_4
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 2777
    const/4 v2, 0x6

    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->k:I

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2780
    :cond_5
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_6

    .line 2781
    const/4 v2, 0x7

    iget v3, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l:I

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2784
    :cond_6
    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_7

    .line 2785
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->m:Lkotlin/reflect/jvm/internal/impl/i/e$a;

    invoke-static {v6, v2}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    move v2, v0

    .line 2788
    :goto_2
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 2789
    const/16 v3, 0x9

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    invoke-static {v3, v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/CodedOutputStream;->d(ILkotlin/reflect/jvm/internal/impl/protobuf/o;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2788
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2792
    :cond_8
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a()I

    move-result v0

    add-int/2addr v0, v2

    .line 2793
    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->p:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public synthetic j()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 2207
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->E()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1

    .prologue
    .line 2207
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->F()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$a;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2697
    iget-byte v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o:B

    .line 2698
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 2714
    :cond_0
    :goto_0
    return v1

    .line 2699
    :cond_1
    if-eqz v0, :cond_0

    .line 2701
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2702
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->A()Lkotlin/reflect/jvm/internal/impl/i/e$a;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a;->l()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2703
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o:B

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2707
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->C()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 2708
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->a(I)Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;

    move-result-object v3

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2709
    iput-byte v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o:B

    goto :goto_0

    .line 2707
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2713
    :cond_4
    iput-byte v2, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->o:B

    move v1, v2

    .line 2714
    goto :goto_0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;
    .locals 1

    .prologue
    .line 2533
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->f:Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b$b;

    return-object v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 2542
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()J
    .locals 2

    .prologue
    .line 2548
    iget-wide v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->g:J

    return-wide v0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 2557
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()F
    .locals 1

    .prologue
    .line 2563
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->h:F

    return v0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 2572
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()D
    .locals 2

    .prologue
    .line 2578
    iget-wide v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->i:D

    return-wide v0
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 2587
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()I
    .locals 1

    .prologue
    .line 2593
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->j:I

    return v0
.end method

.method public v()Z
    .locals 2

    .prologue
    .line 2606
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()I
    .locals 1

    .prologue
    .line 2616
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->k:I

    return v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 2801
    invoke-super {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public x()Z
    .locals 2

    .prologue
    .line 2625
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()I
    .locals 1

    .prologue
    .line 2631
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->l:I

    return v0
.end method

.method public z()Z
    .locals 2

    .prologue
    .line 2640
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$a$a$b;->e:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
