.class public final Lkotlin/reflect/jvm/internal/impl/i/e$u$a;
.super Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
.source "ProtoBuf.java"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/e$v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/i/e$u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/reflect/jvm/internal/impl/protobuf/h$a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/i/e$u;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$u$a;",
        ">;",
        "Lkotlin/reflect/jvm/internal/impl/i/e$v;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/i/e$s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 20974
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;-><init>()V

    .line 21058
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    .line 20975
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->n()V

    .line 20976
    return-void
.end method

.method static synthetic m()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;
    .locals 1

    .prologue
    .line 20967
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 0

    .prologue
    .line 20979
    return-void
.end method

.method private static o()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;
    .locals 1

    .prologue
    .line 20981
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;-><init>()V

    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 21061
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 21062
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    .line 21063
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    .line 21065
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;
    .locals 2

    .prologue
    .line 21019
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 21032
    :goto_0
    return-object p0

    .line 21020
    :cond_0
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->b(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 21021
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 21022
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->b(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    .line 21023
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    .line 21030
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->d()Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->c(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/d;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/d;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;

    goto :goto_0

    .line 21025
    :cond_2
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->p()V

    .line 21026
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->b(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public synthetic a()Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1

    .prologue
    .line 20967
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20967
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/protobuf/h;)Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 20967
    check-cast p1, Lkotlin/reflect/jvm/internal/impl/i/e$u;

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;
    .locals 1

    .prologue
    .line 20967
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/protobuf/o$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20967
    invoke-virtual {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    return-object v0
.end method

.method public c(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21043
    const/4 v2, 0x0

    .line 21045
    :try_start_0
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/i/e$u;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/q;

    invoke-interface {v0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/protobuf/q;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/e;Lkotlin/reflect/jvm/internal/impl/protobuf/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$u;
    :try_end_0
    .catch Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 21050
    if-eqz v0, :cond_0

    .line 21051
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    .line 21054
    :cond_0
    return-object p0

    .line 21046
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 21047
    :try_start_1
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/protobuf/InvalidProtocolBufferException;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/o;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/i/e$u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 21048
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 21050
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 21051
    invoke-virtual {p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    :cond_1
    throw v0

    .line 21050
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public synthetic c()Lkotlin/reflect/jvm/internal/impl/protobuf/h;
    .locals 1

    .prologue
    .line 20967
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20967
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 20967
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->g()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;
    .locals 2

    .prologue
    .line 20992
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->o()Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;)Lkotlin/reflect/jvm/internal/impl/i/e$u$a;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/reflect/jvm/internal/impl/i/e$u;
    .locals 1

    .prologue
    .line 20996
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->f()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 20967
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->j()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/protobuf/o;
    .locals 1

    .prologue
    .line 20967
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    return-object v0
.end method

.method public j()Lkotlin/reflect/jvm/internal/impl/i/e$u;
    .locals 2

    .prologue
    .line 21000
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->k()Lkotlin/reflect/jvm/internal/impl/i/e$u;

    move-result-object v0

    .line 21001
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 21002
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/o;)Lkotlin/reflect/jvm/internal/impl/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 21004
    :cond_0
    return-object v0
.end method

.method public k()Lkotlin/reflect/jvm/internal/impl/i/e$u;
    .locals 3

    .prologue
    .line 21008
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/i/e$u;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$u;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/h$a;Lkotlin/reflect/jvm/internal/impl/i/e$1;)V

    .line 21009
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    .line 21010
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 21011
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    .line 21012
    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->a:I

    .line 21014
    :cond_0
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/i/e$u$a;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/i/e$u;->a(Lkotlin/reflect/jvm/internal/impl/i/e$u;Ljava/util/List;)Ljava/util/List;

    .line 21015
    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 21036
    const/4 v0, 0x1

    return v0
.end method
