.class Lkotlin/reflect/jvm/internal/impl/a/l$5;
.super Lkotlin/reflect/jvm/internal/impl/b/b/w;
.source "KotlinBuiltIns.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/b/z;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/e/b;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lkotlin/reflect/jvm/internal/impl/a/l;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/l$5;->c:Lkotlin/reflect/jvm/internal/impl/a/l;

    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/a/l$5;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    iput-object p5, p0, Lkotlin/reflect/jvm/internal/impl/a/l$5;->b:Ljava/util/List;

    invoke-direct {p0, p2, p3}, Lkotlin/reflect/jvm/internal/impl/b/b/w;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    return-void
.end method


# virtual methods
.method public x_()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 5

    .prologue
    .line 203
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/h/e/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "built-in package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/l$5;->a:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/l$5;->b:Ljava/util/List;

    new-instance v3, Lkotlin/reflect/jvm/internal/impl/a/l$5$1;

    invoke-direct {v3, p0}, Lkotlin/reflect/jvm/internal/impl/a/l$5$1;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l$5;)V

    invoke-static {v2, v3}, Lkotlin/a/m;->c(Ljava/lang/Iterable;Lkotlin/d/a/b;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/h/e/b;-><init>(Ljava/lang/String;Ljava/util/List;)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "@NotNull method %s.%s must not return null"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns$6"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "getMemberScope"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-object v0
.end method
