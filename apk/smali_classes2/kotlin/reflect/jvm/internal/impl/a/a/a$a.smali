.class public final Lkotlin/reflect/jvm/internal/impl/a/a/a$a;
.super Ljava/lang/Object;
.source "BuiltInFictitiousFunctionClassFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;-><init>()V

    return-void
.end method

.method private final a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 64
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v4

    :goto_0
    if-eqz v0, :cond_1

    move-object v0, v2

    .line 72
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 64
    goto :goto_0

    .line 67
    :cond_1
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/h/j;->c(Ljava/lang/CharSequence;)Lkotlin/a/l;

    move-result-object v5

    move v0, v1

    :goto_2
    invoke-virtual {v5}, Lkotlin/a/l;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v5}, Lkotlin/a/l;->b()C

    move-result v3

    .line 68
    add-int/lit8 v6, v3, -0x30

    .line 69
    if-ltz v6, :cond_2

    const/16 v3, 0x9

    if-gt v6, v3, :cond_2

    move v3, v1

    :goto_3
    if-eqz v3, :cond_3

    move-object v0, v2

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_3

    .line 70
    :cond_3
    mul-int/lit8 v0, v0, 0xa

    add-int/2addr v0, v6

    .line 67
    goto :goto_2

    .line 72
    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/a/a/a$a;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/a/a/a$b;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;->b(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/a/a/a$b;

    move-result-object v0

    return-object v0
.end method

.method private final b(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/a/a/a$b;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 41
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->d:Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;

    invoke-virtual {v1, p2, p1}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 43
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->b()Ljava/lang/String;

    move-result-object v2

    .line 45
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez p1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 48
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;

    invoke-direct {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;-><init>(Lkotlin/reflect/jvm/internal/impl/a/a/b$b;I)V

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 1

    .prologue
    const-string v0, "className"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageFqName"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    check-cast p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;

    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;->b(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/a/a/a$b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->a()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
