.class public final Lkotlin/reflect/jvm/internal/impl/a/i;
.super Lkotlin/reflect/jvm/internal/impl/a/l;
.source "DefaultBuiltIns.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/i$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/a/i$a;

.field private static final k:Lkotlin/reflect/jvm/internal/impl/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/jvm/internal/impl/a/d",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/a/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/i$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/i;->a:Lkotlin/reflect/jvm/internal/impl/a/i$a;

    .line 27
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/a/d;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/i$b;->a:Lkotlin/reflect/jvm/internal/impl/a/i$b;

    check-cast v0, Lkotlin/d/a/a;

    invoke-direct {v1, v0}, Lkotlin/reflect/jvm/internal/impl/a/d;-><init>(Lkotlin/d/a/a;)V

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/a/i;->k:Lkotlin/reflect/jvm/internal/impl/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/j/b;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/j/b;-><init>()V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/j/i;

    invoke-direct {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V

    .line 23
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/i;->c()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/a/i;-><init>()V

    return-void
.end method

.method public static final synthetic a()Lkotlin/reflect/jvm/internal/impl/a/d;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/i;->k:Lkotlin/reflect/jvm/internal/impl/a/d;

    return-object v0
.end method

.method public static final b()Lkotlin/reflect/jvm/internal/impl/a/i;
    .locals 1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/i;->a:Lkotlin/reflect/jvm/internal/impl/a/i$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/i$a;->a()Lkotlin/reflect/jvm/internal/impl/a/i;

    move-result-object v0

    return-object v0
.end method
