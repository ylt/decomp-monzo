.class public final Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;
.super Ljava/lang/Object;
.source "FunctionClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/e/b;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const-string v0, "packageFqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "className"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->values()[Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    move v3, v4

    .line 167
    :goto_0
    array-length v1, v0

    if-ge v3, v1, :cond_2

    aget-object v2, v0, v3

    move-object v1, v2

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    .line 60
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v6

    invoke-static {v6, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x2

    invoke-static {p2, v1, v4, v6, v5}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1

    move-object v0, v2

    .line 168
    :goto_2
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    .line 60
    return-object v0

    :cond_0
    move v1, v4

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move-object v0, v5

    .line 168
    goto :goto_2
.end method
