.class public final Lkotlin/reflect/jvm/internal/impl/a/a/e;
.super Lkotlin/reflect/jvm/internal/impl/b/b/ac;
.source "FunctionInvokeDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/a/e$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/a/a/e$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/a/e$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/a/e;->a:Lkotlin/reflect/jvm/internal/impl/a/a/e$a;

    return-void
.end method

.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/a/a/e;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)V
    .locals 7

    .prologue
    .line 30
    move-object v2, p2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/ak;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/l/j;->g:Lkotlin/reflect/jvm/internal/impl/e/f;

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    move-object v0, p0

    move-object v1, p1

    move-object v5, p3

    .line 35
    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/ak;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 44
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->a(Z)V

    .line 45
    invoke-virtual {p0, p4}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->i(Z)V

    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->j(Z)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/a/a/e;Lkotlin/reflect/jvm/internal/impl/b/b$a;ZLkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/a/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/a/a/e;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)V

    return-void
.end method

.method private final a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;)",
            "Lkotlin/reflect/jvm/internal/impl/b/s;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 74
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v6, v0, v1

    .line 75
    if-eqz v6, :cond_0

    if-ne v6, v4, :cond_1

    :cond_0
    move v0, v4

    :goto_0
    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    const-string v1, "Assertion failed"

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    move v0, v3

    goto :goto_0

    .line 77
    :cond_2
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->i()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 154
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 155
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 156
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 78
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    .line 79
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/at;->c()I

    move-result v8

    .line 80
    sub-int v2, v8, v6

    .line 81
    if-ltz v2, :cond_3

    .line 82
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 83
    if-eqz v2, :cond_3

    move-object v5, v2

    :cond_3
    move-object v2, p0

    .line 87
    check-cast v2, Lkotlin/reflect/jvm/internal/impl/b/a;

    const-string v9, "newName"

    invoke-static {v5, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v2, v5, v8}, Lkotlin/reflect/jvm/internal/impl/b/at;->a(Lkotlin/reflect/jvm/internal/impl/b/a;Lkotlin/reflect/jvm/internal/impl/e/f;I)Lkotlin/reflect/jvm/internal/impl/b/at;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 157
    :cond_4
    check-cast v1, Ljava/util/List;

    .line 90
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/k/am;->a:Lkotlin/reflect/jvm/internal/impl/k/am;

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->e(Lkotlin/reflect/jvm/internal/impl/k/am;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v2

    check-cast p1, Ljava/lang/Iterable;

    .line 158
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 91
    if-nez v0, :cond_7

    move v0, v4

    :goto_2
    if-eqz v0, :cond_5

    :goto_3
    invoke-virtual {v2, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->c(Z)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    .line 92
    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->b(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->o()Lkotlin/reflect/jvm/internal/impl/b/ak;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 93
    invoke-virtual {v1, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/o$a;->a(Lkotlin/reflect/jvm/internal/impl/b/s;)Lkotlin/reflect/jvm/internal/impl/b/b/o$a;

    move-result-object v0

    .line 95
    invoke-super {p0, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;->a(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_6
    return-object v0

    :cond_7
    move v0, v3

    .line 91
    goto :goto_2

    :cond_8
    move v4, v3

    .line 159
    goto :goto_3
.end method


# virtual methods
.method protected a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/s;Lkotlin/reflect/jvm/internal/impl/b/b$a;Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/al;)Lkotlin/reflect/jvm/internal/impl/b/b/o;
    .locals 2

    .prologue
    const-string v0, "newOwner"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/e;

    check-cast p2, Lkotlin/reflect/jvm/internal/impl/a/a/e;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->C()Z

    move-result v1

    invoke-direct {v0, p1, p2, p3, v1}, Lkotlin/reflect/jvm/internal/impl/a/a/e;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/a/a/e;Lkotlin/reflect/jvm/internal/impl/b/b$a;Z)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/o;

    return-object v0
.end method

.method protected a(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/s;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-super {p0, p1}, Lkotlin/reflect/jvm/internal/impl/b/b/ac;->a(Lkotlin/reflect/jvm/internal/impl/b/b/o$a;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/a/e;

    if-eqz v0, :cond_1

    .line 51
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->i()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 148
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 51
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/a/j;->h(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    if-eqz v1, :cond_2

    move v1, v3

    :goto_0
    if-eqz v1, :cond_0

    move v1, v2

    .line 149
    :goto_1
    if-eqz v1, :cond_4

    .line 51
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/s;

    .line 53
    :goto_2
    return-object v0

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move v1, v2

    .line 51
    goto :goto_0

    :cond_3
    move v1, v3

    .line 149
    goto :goto_1

    .line 52
    :cond_4
    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->i()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 150
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 151
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 152
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/at;

    .line 52
    invoke-interface {v1}, Lkotlin/reflect/jvm/internal/impl/b/at;->s()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    invoke-static {v1}, Lkotlin/reflect/jvm/internal/impl/a/j;->h(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 153
    :cond_5
    check-cast v2, Ljava/util/List;

    .line 53
    invoke-direct {v0, v2}, Lkotlin/reflect/jvm/internal/impl/a/a/e;->a(Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/b/s;

    move-result-object v0

    goto :goto_2
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method
