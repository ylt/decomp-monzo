.class public final Lkotlin/reflect/jvm/internal/impl/a/a;
.super Lkotlin/reflect/jvm/internal/impl/i/f;
.source "BuiltInSerializerProtocol.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/a/a; = null

.field private static final b:Ljava/lang/String; = "kotlin_builtins"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/a/a;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 11

    .prologue
    .line 24
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/protobuf/f;->a()Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    .line 25
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/i/a/a;->a(Lkotlin/reflect/jvm/internal/impl/protobuf/f;)V

    const-string v0, "ExtensionRegistryLite.ne\u2026sterAllExtensions(this) }"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/protobuf/f;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/i/a/a;->c:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.constructorAnnotation"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/i/a/a;->b:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.classAnnotation"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/i/a/a;->d:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.functionAnnotation"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/i/a/a;->e:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.propertyAnnotation"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/i/a/a;->g:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.enumEntryAnnotation"

    invoke-static {v6, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lkotlin/reflect/jvm/internal/impl/i/a/a;->f:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.compileTimeValue"

    invoke-static {v7, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/i/a/a;->h:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.parameterAnnotation"

    invoke-static {v8, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v9, Lkotlin/reflect/jvm/internal/impl/i/a/a;->i:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.typeAnnotation"

    invoke-static {v9, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v10, Lkotlin/reflect/jvm/internal/impl/i/a/a;->j:Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;

    const-string v0, "BuiltInsProtoBuf.typeParameterAnnotation"

    invoke-static {v10, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 24
    invoke-direct/range {v0 .. v10}, Lkotlin/reflect/jvm/internal/impl/i/f;-><init>(Lkotlin/reflect/jvm/internal/impl/protobuf/f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;Lkotlin/reflect/jvm/internal/impl/protobuf/h$f;)V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/a/a;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/a/a;->a:Lkotlin/reflect/jvm/internal/impl/a/a;

    .line 30
    const-string v0, "kotlin_builtins"

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/a;->b:Ljava/lang/String;

    return-void
.end method

.method private final c(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "default-package"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fqName.shortName().asString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/lang/String;
    .locals 7

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    const/16 v2, 0x2f

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->a(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/a;->b(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/lang/String;
    .locals 2

    .prologue
    const-string v0, "fqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/a;->c(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
