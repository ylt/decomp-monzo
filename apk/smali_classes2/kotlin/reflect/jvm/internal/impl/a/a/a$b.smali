.class final Lkotlin/reflect/jvm/internal/impl/a/a/a$b;
.super Ljava/lang/Object;
.source "BuiltInFictitiousFunctionClassFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

.field private final b:I


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/a/b$b;I)V
    .locals 1

    .prologue
    const-string v0, "kind"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    iput p2, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->b:I

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    return-object v0
.end method

.method public final b()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 1

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->b:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;

    if-eqz v2, :cond_1

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;

    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    iget-object v3, p1, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->b:I

    iget v3, p1, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->b:I

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->b:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KindWithArity(kind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", arity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
