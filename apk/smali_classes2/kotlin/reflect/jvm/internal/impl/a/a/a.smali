.class public final Lkotlin/reflect/jvm/internal/impl/a/a/a;
.super Ljava/lang/Object;
.source "BuiltInFictitiousFunctionClassFactory.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/a/a$b;,
        Lkotlin/reflect/jvm/internal/impl/a/a/a$a;
    }
.end annotation


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/a/a/a$a;


# instance fields
.field private final b:Lkotlin/reflect/jvm/internal/impl/j/i;

.field private final c:Lkotlin/reflect/jvm/internal/impl/b/w;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/a$a;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;)V
    .locals 1

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "module"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a;->b:Lkotlin/reflect/jvm/internal/impl/j/i;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a;->c:Lkotlin/reflect/jvm/internal/impl/b/w;

    return-void
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "packageFqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v3

    .line 96
    :goto_0
    return-object v0

    .line 85
    :cond_1
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->b()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 86
    check-cast v0, Ljava/lang/CharSequence;

    const-string v2, "Function"

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v0, v2, v4, v5, v3}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move-object v0, v3

    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {p1}, Lkotlin/reflect/jvm/internal/impl/e/a;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 89
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/a$a;

    const-string v4, "className"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "packageFqName"

    invoke-static {v0, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v1, v0}, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/a/a/a$a;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/a/a/a$b;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->b()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v2

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/a/a/a$b;->c()I

    move-result v4

    .line 92
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->b:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v0, v3

    goto :goto_0

    :cond_3
    move-object v0, v3

    .line 89
    goto :goto_0

    .line 94
    :cond_4
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a;->c:Lkotlin/reflect/jvm/internal/impl/b/w;

    const-string v3, "packageFqName"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lkotlin/reflect/jvm/internal/impl/b/w;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/ac;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/ac;->f()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 106
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 113
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    instance-of v5, v3, Lkotlin/reflect/jvm/internal/impl/a/e;

    if-eqz v5, :cond_5

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 114
    :cond_6
    check-cast v1, Ljava/util/List;

    .line 94
    invoke-static {v1}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/e;

    .line 96
    new-instance v1, Lkotlin/reflect/jvm/internal/impl/a/a/b;

    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/a/a/a;->b:Lkotlin/reflect/jvm/internal/impl/j/i;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/y;

    invoke-direct {v1, v3, v0, v2, v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/a/a/b$b;I)V

    move-object v0, v1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    goto/16 :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v0, 0x0

    const-string v1, "packageFqName"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "name"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-virtual {p2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v1

    .line 78
    const-string v2, "Function"

    invoke-static {v1, v2, v0, v3, v4}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "KFunction"

    invoke-static {v1, v2, v0, v3, v4}, Lkotlin/h/j;->a(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/a$a;

    const-string v3, "string"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {v2, v1, p1}, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;->a(Lkotlin/reflect/jvm/internal/impl/a/a/a$a;Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/a/a/a$b;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method
