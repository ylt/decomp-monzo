.class public final Lkotlin/reflect/jvm/internal/impl/a/j;
.super Ljava/lang/Object;
.source "functionTypes.kt"


# direct methods
.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/a/l;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Lkotlin/reflect/jvm/internal/impl/a/l;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const-string v0, "parameterTypes"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnType"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builtIns"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, v1

    .line 167
    check-cast v0, Ljava/util/Collection;

    if-eqz p0, :cond_3

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v3

    :goto_1
    invoke-static {v0, v3}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 169
    check-cast p1, Ljava/lang/Iterable;

    .line 225
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v2

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v1

    .line 226
    check-cast v0, Ljava/util/Collection;

    add-int/lit8 v6, v3, 0x1

    check-cast v2, Lkotlin/reflect/jvm/internal/impl/k/r;

    .line 170
    if-eqz p2, :cond_4

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/e/f;

    if-eqz v3, :cond_4

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/a/j$b;->a:Lkotlin/reflect/jvm/internal/impl/a/j$b;

    check-cast v4, Lkotlin/d/a/b;

    invoke-static {v3, v4}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 171
    :goto_3
    if-eqz v3, :cond_1

    .line 172
    sget-object v4, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v4, v4, Lkotlin/reflect/jvm/internal/impl/a/l$a;->A:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v4

    invoke-virtual {p4, v4}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v4

    .line 173
    new-instance v8, Lkotlin/reflect/jvm/internal/impl/h/b/g;

    invoke-direct {v8, p4}, Lkotlin/reflect/jvm/internal/impl/h/b/g;-><init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V

    invoke-virtual {v3}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v3

    const-string v9, "name.asString()"

    invoke-static {v3, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Lkotlin/reflect/jvm/internal/impl/h/b/g;->b(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/h/b/s;

    move-result-object v8

    .line 174
    new-instance v9, Lkotlin/reflect/jvm/internal/impl/b/a/d;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v3

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/e;->o()Lkotlin/reflect/jvm/internal/impl/b/d;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {v4}, Lkotlin/reflect/jvm/internal/impl/b/d;->i()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4, v8}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v4

    invoke-static {v4}, Lkotlin/a/ab;->a(Lkotlin/h;)Ljava/util/Map;

    move-result-object v4

    sget-object v8, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v9, v3, v4, v8}, Lkotlin/reflect/jvm/internal/impl/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 179
    new-instance v4, Lkotlin/reflect/jvm/internal/impl/b/a/i;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3, v9}, Lkotlin/a/m;->d(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v4, v3}, Lkotlin/reflect/jvm/internal/impl/b/a/i;-><init>(Ljava/util/List;)V

    move-object v3, v4

    check-cast v3, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    invoke-static {v2, v3}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/b/a/h;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    .line 184
    :cond_1
    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v3, v6

    .line 225
    goto/16 :goto_2

    :cond_2
    move v0, v2

    .line 165
    goto/16 :goto_0

    :cond_3
    move-object v3, v5

    .line 167
    goto/16 :goto_1

    :cond_4
    move-object v3, v5

    .line 170
    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 227
    check-cast v0, Ljava/util/Collection;

    .line 187
    invoke-static {p3}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    instance-of v0, p0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 119
    :goto_0
    return-object v0

    .line 118
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/c;

    move-result-object v0

    .line 119
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/e/c;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 4

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/c;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    .line 124
    :cond_1
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 126
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/a;->a:Lkotlin/reflect/jvm/internal/impl/a/a/a$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "fqName.shortName().asString()"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->d()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    const-string v3, "fqName.parent()"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0}, Lkotlin/reflect/jvm/internal/impl/a/a/a$a;->a(Ljava/lang/String;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Z)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/a/l;",
            "Lkotlin/reflect/jvm/internal/impl/b/a/h;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Ljava/util/List",
            "<+",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/f;",
            ">;",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            "Z)",
            "Lkotlin/reflect/jvm/internal/impl/k/v;"
        }
    .end annotation

    .prologue
    const-string v0, "builtIns"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parameterTypes"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnType"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    invoke-static {p2, p3, p4, p5, p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Lkotlin/reflect/jvm/internal/impl/a/l;)Ljava/util/List;

    move-result-object v2

    .line 203
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    .line 204
    if-nez p2, :cond_1

    .line 205
    :goto_0
    if-eqz p6, :cond_2

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->d(I)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    move-object v1, v0

    .line 208
    :goto_1
    if-eqz p2, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->z:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "KotlinBuiltIns.FQ_NAMES.extensionFunctionType"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    move-object v0, p1

    .line 221
    :goto_2
    const-string v3, "classDescriptor"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/b/e;Ljava/util/List;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0

    .line 204
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_2
    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(I)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 212
    :cond_3
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/b/a/d;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->z:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/e;->E_()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/r;

    invoke-static {}, Lkotlin/a/ab;->a()Ljava/util/Map;

    move-result-object v4

    sget-object v5, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    invoke-direct {v3, v0, v4, v5}, Lkotlin/reflect/jvm/internal/impl/b/a/d;-><init>(Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    .line 218
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/a/i;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1, v3}, Lkotlin/a/m;->d(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3}, Lkotlin/reflect/jvm/internal/impl/b/a/i;-><init>(Ljava/util/List;)V

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;

    goto :goto_2
.end method

.method public static bridge synthetic a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 7

    .prologue
    and-int/lit8 v0, p7, 0x40

    if-eqz v0, :cond_0

    .line 200
    const/4 v6, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0

    :cond_0
    move v6, p6

    goto :goto_0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    :goto_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    :goto_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->b:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    .line 80
    :goto_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->a:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->b:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 78
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static final d(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 1

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->i(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not a function type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 132
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->i(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->e(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final f(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;
    .locals 2

    .prologue
    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not a function type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 137
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->g(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    const-string v1, "arguments.last().type"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final g(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/k/r;",
            ")",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/k/ah;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->c(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    sget-boolean v3, Lkotlin/p;->a:Z

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not a function type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 142
    :cond_0
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v3

    .line 143
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->d(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 144
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 145
    if-gt v0, v4, :cond_2

    :goto_1
    sget-boolean v2, Lkotlin/p;->a:Z

    if-eqz v2, :cond_3

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not an exact function type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    move v0, v2

    .line 143
    goto :goto_0

    :cond_2
    move v1, v2

    .line 145
    goto :goto_1

    .line 146
    :cond_3
    invoke-interface {v3, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static final h(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const-string v0, "$receiver"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/a/l$a;->A:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v3, "KotlinBuiltIns.FQ_NAMES.parameterName"

    invoke-static {v1, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 151
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/c;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/b/s;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/h/b/s;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/j$a;->a:Lkotlin/reflect/jvm/internal/impl/a/j$a;

    check-cast v1, Lkotlin/d/a/b;

    .line 153
    invoke-static {v0, v1}, Lkotlin/reflect/jvm/internal/impl/utils/a/a;->a(Ljava/lang/Object;Lkotlin/d/a/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 155
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v2

    :cond_1
    return-object v2
.end method

.method private static final i(Lkotlin/reflect/jvm/internal/impl/k/r;)Z
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/a/l$a;->z:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v2, "KotlinBuiltIns.FQ_NAMES.extensionFunctionType"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
