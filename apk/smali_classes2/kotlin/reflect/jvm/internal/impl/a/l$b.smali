.class Lkotlin/reflect/jvm/internal/impl/a/l$b;
.super Ljava/lang/Object;
.source "KotlinBuiltIns.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkotlin/reflect/jvm/internal/impl/a/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field public final a:Lkotlin/reflect/jvm/internal/impl/b/y;

.field public final b:Lkotlin/reflect/jvm/internal/impl/b/y;

.field public final c:Lkotlin/reflect/jvm/internal/impl/b/y;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/b/y;Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/y;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "builtInsPackageFragment"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns$PackageFragments"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "collectionsPackageFragment"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns$PackageFragments"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "annotationPackageFragment"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns$PackageFragments"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument for @NotNull parameter \'%s\' of %s.%s must not be null"

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "allImportedByDefaultBuiltInsPackageFragments"

    aput-object v3, v2, v4

    const-string v3, "kotlin/reflect/jvm/internal/impl/builtins/KotlinBuiltIns$PackageFragments"

    aput-object v3, v2, v5

    const-string v3, "<init>"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/l$b;->a:Lkotlin/reflect/jvm/internal/impl/b/y;

    .line 256
    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/a/l$b;->b:Lkotlin/reflect/jvm/internal/impl/b/y;

    .line 257
    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/a/l$b;->c:Lkotlin/reflect/jvm/internal/impl/b/y;

    .line 258
    iput-object p4, p0, Lkotlin/reflect/jvm/internal/impl/a/l$b;->d:Ljava/util/Set;

    .line 259
    return-void
.end method

.method synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/b/y;Ljava/util/Set;Lkotlin/reflect/jvm/internal/impl/a/l$1;)V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0, p1, p2, p3, p4}, Lkotlin/reflect/jvm/internal/impl/a/l$b;-><init>(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/b/y;Ljava/util/Set;)V

    return-void
.end method
