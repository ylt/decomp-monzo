.class Lkotlin/reflect/jvm/internal/impl/a/l$1;
.super Ljava/lang/Object;
.source "KotlinBuiltIns.java"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/d/a/a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/a/l$b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/a/l;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/l$1;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b()Lkotlin/reflect/jvm/internal/impl/a/l$b;
    .locals 6

    .prologue
    .line 90
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/l$1;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/l;)Lkotlin/reflect/jvm/internal/impl/b/b/u;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/b/u;->g()Lkotlin/reflect/jvm/internal/impl/b/z;

    move-result-object v0

    .line 92
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    .line 93
    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/l$1;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v1, v0, v5, v2}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/z;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v1

    .line 94
    iget-object v2, p0, Lkotlin/reflect/jvm/internal/impl/a/l$1;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    sget-object v3, Lkotlin/reflect/jvm/internal/impl/a/l;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v2, v0, v5, v3}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/z;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v2

    .line 95
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/a/l$1;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/a/l;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v3, v0, v5, v4}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/z;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/y;

    .line 96
    iget-object v3, p0, Lkotlin/reflect/jvm/internal/impl/a/l$1;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/a/l;->K()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v4

    invoke-static {v3, v0, v5, v4}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/z;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v3

    .line 97
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 99
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/l$b;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lkotlin/reflect/jvm/internal/impl/a/l$b;-><init>(Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/b/y;Ljava/util/Set;Lkotlin/reflect/jvm/internal/impl/a/l$1;)V

    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l$1;->b()Lkotlin/reflect/jvm/internal/impl/a/l$b;

    move-result-object v0

    return-object v0
.end method
