.class public final Lkotlin/reflect/jvm/internal/impl/a/h;
.super Ljava/lang/Object;
.source "CompanionObjectMapping.kt"


# static fields
.field public static final a:Lkotlin/reflect/jvm/internal/impl/a/h;

.field private static final b:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/h;

    invoke-direct {v0}, Lkotlin/reflect/jvm/internal/impl/a/h;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lkotlin/reflect/jvm/internal/impl/a/h;

    sput-object p0, Lkotlin/reflect/jvm/internal/impl/a/h;->a:Lkotlin/reflect/jvm/internal/impl/a/h;

    .line 27
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/m;->i:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 38
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 39
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 40
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    .line 27
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->c(Lkotlin/reflect/jvm/internal/impl/a/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 41
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->g:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    .line 27
    invoke-static {v1, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v1, v1, Lkotlin/reflect/jvm/internal/impl/a/l$a;->r:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v1

    .line 28
    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 29
    nop

    .line 42
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 43
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 29
    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 44
    :cond_1
    check-cast v1, Ljava/util/LinkedHashSet;

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/a/h;->b:Ljava/util/LinkedHashSet;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/e/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/h;->b:Ljava/util/LinkedHashSet;

    check-cast v0, Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    const-string v1, "Collections.unmodifiableSet(classIds)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lkotlin/reflect/jvm/internal/impl/b/e;)Z
    .locals 2

    .prologue
    const-string v0, "classDescriptor"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 35
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c;->i(Lkotlin/reflect/jvm/internal/impl/b/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/h;->b:Ljava/util/LinkedHashSet;

    check-cast v0, Ljava/lang/Iterable;

    check-cast p1, Lkotlin/reflect/jvm/internal/impl/b/i;

    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/i;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/e/a;->e()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v1

    :goto_0
    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
