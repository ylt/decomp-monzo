.class public final Lkotlin/reflect/jvm/internal/impl/a/p;
.super Ljava/lang/Object;
.source "suspendFunctionTypes.kt"


# static fields
.field private static final a:Lkotlin/reflect/jvm/internal/impl/b/b/v;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 36
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/b/b/v;

    new-instance v1, Lkotlin/reflect/jvm/internal/impl/b/b/m;

    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/k/l;->a()Lkotlin/reflect/jvm/internal/impl/b/w;

    move-result-object v2

    const-string v4, "ErrorUtils.getErrorModule()"

    invoke-static {v2, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/c;->d:Lkotlin/reflect/jvm/internal/impl/e/b;

    const-string v5, "DescriptorUtils.COROUTINES_PACKAGE_FQ_NAME"

    invoke-static {v4, v5}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, v4}, Lkotlin/reflect/jvm/internal/impl/b/b/m;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/reflect/jvm/internal/impl/e/b;)V

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/h/c;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-virtual {v4}, Lkotlin/reflect/jvm/internal/impl/e/b;->e()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    sget-object v6, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/b/b/v;-><init>(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/f;ZZLkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/b/al;)V

    move-object v7, v0

    .line 40
    check-cast v7, Lkotlin/reflect/jvm/internal/impl/b/b/v;

    .line 41
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    invoke-virtual {v7, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/v;->a(Lkotlin/reflect/jvm/internal/impl/b/u;)V

    .line 42
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    invoke-virtual {v7, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/v;->a(Lkotlin/reflect/jvm/internal/impl/b/ay;)V

    move-object v1, v7

    .line 43
    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/m;

    sget-object v2, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v2}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v2

    sget-object v4, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    const-string v5, "T"

    invoke-static {v5}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v5

    move v6, v3

    invoke-static/range {v1 .. v6}, Lkotlin/reflect/jvm/internal/impl/b/b/af;->a(Lkotlin/reflect/jvm/internal/impl/b/m;Lkotlin/reflect/jvm/internal/impl/b/a/h;ZLkotlin/reflect/jvm/internal/impl/k/ap;Lkotlin/reflect/jvm/internal/impl/e/f;I)Lkotlin/reflect/jvm/internal/impl/b/aq;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/jvm/internal/impl/b/aq;

    .line 46
    invoke-static {v1}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 43
    check-cast v1, Ljava/util/List;

    invoke-virtual {v7, v1}, Lkotlin/reflect/jvm/internal/impl/b/b/v;->b(Ljava/util/List;)V

    .line 48
    invoke-virtual {v7}, Lkotlin/reflect/jvm/internal/impl/b/b/v;->c()V

    .line 49
    nop

    .line 40
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/v;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/p;->a:Lkotlin/reflect/jvm/internal/impl/b/b/v;

    return-void
.end method

.method public static final a()Lkotlin/reflect/jvm/internal/impl/b/b/v;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/p;->a:Lkotlin/reflect/jvm/internal/impl/b/b/v;

    return-object v0
.end method

.method public static final a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 14

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v0, "suspendFunType"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->b(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "This type should be suspend function type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 53
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 57
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v9

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v11

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v7

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->g(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 101
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 102
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 103
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 61
    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_1
    check-cast v1, Ljava/util/List;

    move-object v8, v1

    .line 57
    check-cast v8, Ljava/util/Collection;

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/p;->a:Lkotlin/reflect/jvm/internal/impl/b/b/v;

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/b/b/v;->e()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v1

    const-string v2, "FAKE_CONTINUATION_CLASS_DESCRIPTOR.typeConstructor"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->f(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-static {v2}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/ah;

    move-result-object v2

    invoke-static {v2}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/16 v5, 0x10

    move-object v6, v4

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/k/s;->a(Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/ad;Ljava/util/List;ZLkotlin/reflect/jvm/internal/impl/h/e/h;ILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-static {v8, v0}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/l;->y()Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v10

    const-string v0, "suspendFunType.builtIns.nullableAnyType"

    invoke-static {v10, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v10, Lkotlin/reflect/jvm/internal/impl/k/r;

    const/16 v12, 0x40

    move-object v5, v9

    move-object v6, v11

    move-object v9, v4

    move v11, v3

    move-object v13, v4

    invoke-static/range {v5 .. v13}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;ZILjava/lang/Object;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v1

    .line 73
    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/v;
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const-string v0, "funType"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Z

    move-result v0

    sget-boolean v1, Lkotlin/p;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "This type should be function type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 77
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 81
    :cond_0
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->g(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->h(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 83
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->i()Lkotlin/reflect/jvm/internal/impl/k/ad;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ad;->d()Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/h/c/a;->b(Lkotlin/reflect/jvm/internal/impl/b/m;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    :goto_0
    sget-object v2, Lkotlin/reflect/jvm/internal/impl/h/c;->e:Lkotlin/reflect/jvm/internal/impl/e/b;

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v6, :cond_3

    .line 99
    :cond_1
    :goto_1
    return-object v4

    :cond_2
    move-object v0, v4

    .line 83
    goto :goto_0

    .line 88
    :cond_3
    invoke-virtual {v1}, Lkotlin/reflect/jvm/internal/impl/k/r;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ah;

    invoke-interface {v0}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v5

    .line 90
    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/k/c/a;->a(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/a/l;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->w()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v1

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->e(Lkotlin/reflect/jvm/internal/impl/k/r;)Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v2

    invoke-static {p0}, Lkotlin/reflect/jvm/internal/impl/a/j;->g(Lkotlin/reflect/jvm/internal/impl/k/r;)Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v6}, Lkotlin/a/m;->d(Ljava/util/List;I)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 105
    new-instance v7, Ljava/util/ArrayList;

    const/16 v8, 0xa

    invoke-static {v3, v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 106
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 107
    check-cast v3, Lkotlin/reflect/jvm/internal/impl/k/ah;

    .line 94
    invoke-interface {v3}, Lkotlin/reflect/jvm/internal/impl/k/ah;->c()Lkotlin/reflect/jvm/internal/impl/k/r;

    move-result-object v3

    invoke-interface {v7, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move-object v3, v7

    .line 108
    check-cast v3, Ljava/util/List;

    .line 90
    const-string v7, "suspendReturnType"

    invoke-static {v5, v7}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static/range {v0 .. v6}, Lkotlin/reflect/jvm/internal/impl/a/j;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Lkotlin/reflect/jvm/internal/impl/b/a/h;Lkotlin/reflect/jvm/internal/impl/k/r;Ljava/util/List;Ljava/util/List;Lkotlin/reflect/jvm/internal/impl/k/r;Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v0

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/k/r;->c()Z

    move-result v1

    .line 99
    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/k/v;->a(Z)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v4

    goto :goto_1
.end method
