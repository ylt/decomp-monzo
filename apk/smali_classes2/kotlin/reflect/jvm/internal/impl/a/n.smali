.class public final Lkotlin/reflect/jvm/internal/impl/a/n;
.super Ljava/lang/Object;
.source "ReflectionTypes.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/n$a;,
        Lkotlin/reflect/jvm/internal/impl/a/n$b;
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/a/n$b;


# instance fields
.field private final c:Lkotlin/c;

.field private final d:Lkotlin/reflect/jvm/internal/impl/a/n$a;

.field private final e:Lkotlin/reflect/jvm/internal/impl/a/n$a;

.field private final f:Lkotlin/reflect/jvm/internal/impl/a/n$a;

.field private final g:Lkotlin/reflect/jvm/internal/impl/a/n$a;

.field private final h:Lkotlin/reflect/jvm/internal/impl/a/n$a;

.field private final i:Lkotlin/reflect/jvm/internal/impl/a/n$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/n$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/n$b;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/n;->b:Lkotlin/reflect/jvm/internal/impl/a/n$b;

    const/4 v0, 0x7

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/a/n;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kotlinReflectScope"

    const-string v5, "getKotlinReflectScope()Lorg/jetbrains/kotlin/resolve/scopes/MemberScope;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/a/n;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kClass"

    const-string v5, "getKClass()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/a/n;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kProperty0"

    const-string v5, "getKProperty0()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x3

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/a/n;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kProperty1"

    const-string v5, "getKProperty1()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x4

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/a/n;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kProperty2"

    const-string v5, "getKProperty2()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x5

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/a/n;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kMutableProperty0"

    const-string v5, "getKMutableProperty0()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x6

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/a/n;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "kMutableProperty1"

    const-string v5, "getKMutableProperty1()Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/a/n;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/b/w;)V
    .locals 2

    .prologue
    const-string v0, "module"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget-object v1, Lkotlin/f;->b:Lkotlin/f;

    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/n$c;

    invoke-direct {v0, p1}, Lkotlin/reflect/jvm/internal/impl/a/n$c;-><init>(Lkotlin/reflect/jvm/internal/impl/b/w;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v1, v0}, Lkotlin/d;->a(Lkotlin/f;Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->c:Lkotlin/c;

    .line 55
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/n$a;->a:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->d:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    .line 56
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/n$a;->a:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->e:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    .line 57
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/n$a;->a:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->f:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    .line 58
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/n$a;->a:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->g:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    .line 59
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/n$a;->a:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->h:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    .line 60
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/n$a;->a:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->i:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    return-void
.end method

.method private final b()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->c:Lkotlin/c;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/n;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method


# virtual methods
.method public final a()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/n;->d:Lkotlin/reflect/jvm/internal/impl/a/n$a;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/n;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/a/n$a;->a(Lkotlin/reflect/jvm/internal/impl/a/n;Lkotlin/reflect/l;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 3

    .prologue
    const-string v0, "className"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {p1}, Lkotlin/reflect/jvm/internal/impl/e/f;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v1

    .line 43
    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/a/n;->b()Lkotlin/reflect/jvm/internal/impl/h/e/h;

    move-result-object v2

    const-string v0, "name"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/c/a/d;->h:Lkotlin/reflect/jvm/internal/impl/c/a/d;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/c/a/b;

    invoke-interface {v2, v1, v0}, Lkotlin/reflect/jvm/internal/impl/h/e/h;->c(Lkotlin/reflect/jvm/internal/impl/e/f;Lkotlin/reflect/jvm/internal/impl/c/a/b;)Lkotlin/reflect/jvm/internal/impl/b/h;

    move-result-object v0

    instance-of v2, v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    if-eqz v0, :cond_1

    .line 44
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/a/o;->a()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lkotlin/reflect/jvm/internal/impl/e/b;->a(Lkotlin/reflect/jvm/internal/impl/e/f;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/k/l;->a(Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    const-string v1, "ErrorUtils.createErrorCl\u2026E.child(name).asString())"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method
