.class public final Lkotlin/reflect/jvm/internal/impl/a/a/b;
.super Lkotlin/reflect/jvm/internal/impl/b/b/a;
.source "FunctionClassDescriptor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/a/b$b;,
        Lkotlin/reflect/jvm/internal/impl/a/a/b$a;
    }
.end annotation


# instance fields
.field private final c:Lkotlin/reflect/jvm/internal/impl/a/a/b$a;

.field private final d:Lkotlin/reflect/jvm/internal/impl/a/a/c;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/reflect/jvm/internal/impl/j/i;

.field private final g:Lkotlin/reflect/jvm/internal/impl/b/y;

.field private final h:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

.field private final i:I


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/y;Lkotlin/reflect/jvm/internal/impl/a/a/b$b;I)V
    .locals 8

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingDeclaration"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "functionKind"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p3, p4}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->a(I)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    .line 49
    invoke-direct {p0, p1, v0}, Lkotlin/reflect/jvm/internal/impl/b/b/a;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/e/f;)V

    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->f:Lkotlin/reflect/jvm/internal/impl/j/i;

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->g:Lkotlin/reflect/jvm/internal/impl/b/y;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->h:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    iput p4, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->i:I

    .line 64
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;

    invoke-direct {v0, p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b$a;-><init>(Lkotlin/reflect/jvm/internal/impl/a/a/b;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->c:Lkotlin/reflect/jvm/internal/impl/a/a/b$a;

    .line 65
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/a/c;

    iget-object v1, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->f:Lkotlin/reflect/jvm/internal/impl/j/i;

    invoke-direct {v0, v1, p0}, Lkotlin/reflect/jvm/internal/impl/a/a/c;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/a/b;)V

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->d:Lkotlin/reflect/jvm/internal/impl/a/a/c;

    .line 70
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 72
    new-instance v3, Lkotlin/reflect/jvm/internal/impl/a/a/b$1;

    invoke-direct {v3, p0, v4}, Lkotlin/reflect/jvm/internal/impl/a/a/b$1;-><init>(Lkotlin/reflect/jvm/internal/impl/a/a/b;Ljava/util/ArrayList;)V

    .line 78
    const/4 v1, 0x1

    new-instance v0, Lkotlin/f/c;

    iget v2, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->i:I

    invoke-direct {v0, v1, v2}, Lkotlin/f/c;-><init>(II)V

    check-cast v0, Ljava/lang/Iterable;

    .line 167
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 168
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    check-cast v0, Lkotlin/a/aa;

    invoke-virtual {v0}, Lkotlin/a/aa;->b()I

    move-result v0

    .line 79
    sget-object v5, Lkotlin/reflect/jvm/internal/impl/k/ap;->b:Lkotlin/reflect/jvm/internal/impl/k/ap;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "P"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b$1;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 170
    :cond_0
    check-cast v1, Ljava/util/List;

    move-object v0, v3

    .line 82
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/a/b$1;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/k/ap;->c:Lkotlin/reflect/jvm/internal/impl/k/ap;

    const-string v2, "R"

    invoke-virtual {v0, v1, v2}, Lkotlin/reflect/jvm/internal/impl/a/a/b$1;->a(Lkotlin/reflect/jvm/internal/impl/k/ap;Ljava/lang/String;)V

    move-object v0, v4

    .line 84
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/utils/a;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->e:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/a/a/b;)Lkotlin/reflect/jvm/internal/impl/b/y;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->g:Lkotlin/reflect/jvm/internal/impl/b/y;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/a/a/b;)Ljava/util/List;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->e:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic c(Lkotlin/reflect/jvm/internal/impl/a/a/b;)Lkotlin/reflect/jvm/internal/impl/j/i;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->f:Lkotlin/reflect/jvm/internal/impl/j/i;

    return-object v0
.end method


# virtual methods
.method public final A()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->i:I

    return v0
.end method

.method public C_()Lkotlin/reflect/jvm/internal/impl/a/a/c;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->d:Lkotlin/reflect/jvm/internal/impl/a/a/c;

    return-object v0
.end method

.method public a()Lkotlin/reflect/jvm/internal/impl/b/y;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->g:Lkotlin/reflect/jvm/internal/impl/b/y;

    return-object v0
.end method

.method public c()Lkotlin/reflect/jvm/internal/impl/h/e/h$b;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/h/e/h$b;->a:Lkotlin/reflect/jvm/internal/impl/h/e/h$b;

    return-object v0
.end method

.method public synthetic d()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->c()Lkotlin/reflect/jvm/internal/impl/h/e/h$b;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public e()Lkotlin/reflect/jvm/internal/impl/k/ad;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->c:Lkotlin/reflect/jvm/internal/impl/a/a/b$a;

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/k/ad;

    return-object v0
.end method

.method public synthetic g()Lkotlin/reflect/jvm/internal/impl/h/e/h;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->C_()Lkotlin/reflect/jvm/internal/impl/a/a/c;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/h/e/h;

    return-object v0
.end method

.method public h()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic i()Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->h()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    return-object v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->j()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public l()Lkotlin/reflect/jvm/internal/impl/b/f;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/f;->b:Lkotlin/reflect/jvm/internal/impl/b/f;

    return-object v0
.end method

.method public m()Lkotlin/reflect/jvm/internal/impl/b/u;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/u;->d:Lkotlin/reflect/jvm/internal/impl/b/u;

    return-object v0
.end method

.method public n()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic o()Lkotlin/reflect/jvm/internal/impl/b/d;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->n()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/d;

    return-object v0
.end method

.method public p()Lkotlin/reflect/jvm/internal/impl/b/ay;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/ax;->e:Lkotlin/reflect/jvm/internal/impl/b/ay;

    return-object v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->D_()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public w()Lkotlin/reflect/jvm/internal/impl/b/a/h;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/a/h;->a:Lkotlin/reflect/jvm/internal/impl/b/a/h$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/b/a/h$a;->a()Lkotlin/reflect/jvm/internal/impl/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public x()Lkotlin/reflect/jvm/internal/impl/b/al;
    .locals 2

    .prologue
    .line 108
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/b/al;->a:Lkotlin/reflect/jvm/internal/impl/b/al;

    const-string v1, "SourceElement.NO_SOURCE"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public y()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->e:Ljava/util/List;

    return-object v0
.end method

.method public synthetic y_()Lkotlin/reflect/jvm/internal/impl/b/m;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->a()Lkotlin/reflect/jvm/internal/impl/b/y;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/m;

    return-object v0
.end method

.method public final z()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/a/b;->h:Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    return-object v0
.end method
