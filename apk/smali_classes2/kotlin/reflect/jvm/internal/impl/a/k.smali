.class public final Lkotlin/reflect/jvm/internal/impl/a/k;
.super Ljava/lang/Object;
.source "JvmBuiltInClassDescriptorFactory.kt"

# interfaces
.implements Lkotlin/reflect/jvm/internal/impl/i/b/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlin/reflect/jvm/internal/impl/a/k$a;
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final b:Lkotlin/reflect/jvm/internal/impl/a/k$a;

.field private static final f:Lkotlin/reflect/jvm/internal/impl/e/b;

.field private static final g:Lkotlin/reflect/jvm/internal/impl/e/f;

.field private static final h:Lkotlin/reflect/jvm/internal/impl/e/a;


# instance fields
.field private final c:Lkotlin/reflect/jvm/internal/impl/j/f;

.field private final d:Lkotlin/reflect/jvm/internal/impl/b/w;

.field private final e:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/k$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkotlin/reflect/jvm/internal/impl/a/k$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->b:Lkotlin/reflect/jvm/internal/impl/a/k$a;

    .line 61
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->c:Lkotlin/reflect/jvm/internal/impl/e/b;

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    .line 62
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->c:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->f()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->g:Lkotlin/reflect/jvm/internal/impl/e/f;

    .line 63
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/l;->h:Lkotlin/reflect/jvm/internal/impl/a/l$a;

    iget-object v0, v0, Lkotlin/reflect/jvm/internal/impl/a/l$a;->c:Lkotlin/reflect/jvm/internal/impl/e/c;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/e/c;->c()Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/e/a;->a(Lkotlin/reflect/jvm/internal/impl/e/b;)Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    sput-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->h:Lkotlin/reflect/jvm/internal/impl/e/a;

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lkotlin/reflect/jvm/internal/impl/a/k;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "cloneable"

    const-string v5, "getCloneable()Lorg/jetbrains/kotlin/descriptors/impl/ClassDescriptorImpl;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lkotlin/reflect/jvm/internal/impl/a/k;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/d/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/j/i;",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            "Lkotlin/d/a/b",
            "<-",
            "Lkotlin/reflect/jvm/internal/impl/b/w;",
            "+",
            "Lkotlin/reflect/jvm/internal/impl/b/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moduleDescriptor"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "computeContainingDeclaration"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkotlin/reflect/jvm/internal/impl/a/k;->d:Lkotlin/reflect/jvm/internal/impl/b/w;

    iput-object p3, p0, Lkotlin/reflect/jvm/internal/impl/a/k;->e:Lkotlin/d/a/b;

    .line 35
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/k$b;

    invoke-direct {v0, p0, p1}, Lkotlin/reflect/jvm/internal/impl/a/k$b;-><init>(Lkotlin/reflect/jvm/internal/impl/a/k;Lkotlin/reflect/jvm/internal/impl/j/i;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-interface {p1, v0}, Lkotlin/reflect/jvm/internal/impl/j/i;->a(Lkotlin/d/a/a;)Lkotlin/reflect/jvm/internal/impl/j/f;

    move-result-object v0

    iput-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/k;->c:Lkotlin/reflect/jvm/internal/impl/j/f;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/d/a/b;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_0

    .line 31
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/k$1;->a:Lkotlin/reflect/jvm/internal/impl/a/k$1;

    check-cast v0, Lkotlin/d/a/b;

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lkotlin/reflect/jvm/internal/impl/a/k;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/w;Lkotlin/d/a/b;)V

    return-void

    :cond_0
    move-object v0, p3

    goto :goto_0
.end method

.method public static final synthetic a(Lkotlin/reflect/jvm/internal/impl/a/k;)Lkotlin/d/a/b;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/k;->e:Lkotlin/d/a/b;

    return-object v0
.end method

.method public static final synthetic a()Lkotlin/reflect/jvm/internal/impl/e/b;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->f:Lkotlin/reflect/jvm/internal/impl/e/b;

    return-object v0
.end method

.method public static final synthetic b(Lkotlin/reflect/jvm/internal/impl/a/k;)Lkotlin/reflect/jvm/internal/impl/b/w;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/k;->d:Lkotlin/reflect/jvm/internal/impl/b/w;

    return-object v0
.end method

.method public static final synthetic b()Lkotlin/reflect/jvm/internal/impl/e/f;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->g:Lkotlin/reflect/jvm/internal/impl/e/f;

    return-object v0
.end method

.method public static final synthetic c()Lkotlin/reflect/jvm/internal/impl/e/a;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->h:Lkotlin/reflect/jvm/internal/impl/e/a;

    return-object v0
.end method

.method private final d()Lkotlin/reflect/jvm/internal/impl/b/b/h;
    .locals 3

    iget-object v0, p0, Lkotlin/reflect/jvm/internal/impl/a/k;->c:Lkotlin/reflect/jvm/internal/impl/j/f;

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/k;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lkotlin/reflect/jvm/internal/impl/j/h;->a(Lkotlin/reflect/jvm/internal/impl/j/f;Ljava/lang/Object;Lkotlin/reflect/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/b/h;

    return-object v0
.end method


# virtual methods
.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/jvm/internal/impl/e/b;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/e;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "packageFqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->b:Lkotlin/reflect/jvm/internal/impl/a/k$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/k$a;->b(Lkotlin/reflect/jvm/internal/impl/a/k$a;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/a/k;->d()Lkotlin/reflect/jvm/internal/impl/b/b/h;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/ah;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 58
    :goto_0
    return-object v0

    .line 57
    :cond_0
    invoke-static {}, Lkotlin/a/ah;->a()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/a;)Lkotlin/reflect/jvm/internal/impl/b/e;
    .locals 1

    .prologue
    const-string v0, "classId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->b:Lkotlin/reflect/jvm/internal/impl/a/k$a;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/k$a;->a()Lkotlin/reflect/jvm/internal/impl/e/a;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lkotlin/reflect/jvm/internal/impl/a/k;->d()Lkotlin/reflect/jvm/internal/impl/b/b/h;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 52
    :goto_0
    return-object v0

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lkotlin/reflect/jvm/internal/impl/e/b;Lkotlin/reflect/jvm/internal/impl/e/f;)Z
    .locals 1

    .prologue
    const-string v0, "packageFqName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->b:Lkotlin/reflect/jvm/internal/impl/a/k$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/k$a;->a(Lkotlin/reflect/jvm/internal/impl/a/k$a;)Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v0

    invoke-static {p2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lkotlin/reflect/jvm/internal/impl/a/k;->b:Lkotlin/reflect/jvm/internal/impl/a/k$a;

    invoke-static {v0}, Lkotlin/reflect/jvm/internal/impl/a/k$a;->b(Lkotlin/reflect/jvm/internal/impl/a/k$a;)Lkotlin/reflect/jvm/internal/impl/e/b;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
