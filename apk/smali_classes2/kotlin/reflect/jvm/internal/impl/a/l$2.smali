.class Lkotlin/reflect/jvm/internal/impl/a/l$2;
.super Ljava/lang/Object;
.source "KotlinBuiltIns.java"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkotlin/reflect/jvm/internal/impl/a/l;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/d/a/a",
        "<",
        "Lkotlin/reflect/jvm/internal/impl/a/l$c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lkotlin/reflect/jvm/internal/impl/a/l;


# direct methods
.method constructor <init>(Lkotlin/reflect/jvm/internal/impl/a/l;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lkotlin/reflect/jvm/internal/impl/a/l$2;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b()Lkotlin/reflect/jvm/internal/impl/a/l$c;
    .locals 10

    .prologue
    .line 106
    new-instance v1, Ljava/util/EnumMap;

    const-class v0, Lkotlin/reflect/jvm/internal/impl/a/m;

    invoke-direct {v1, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 107
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 108
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 109
    invoke-static {}, Lkotlin/reflect/jvm/internal/impl/a/m;->values()[Lkotlin/reflect/jvm/internal/impl/a/m;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    .line 110
    iget-object v7, p0, Lkotlin/reflect/jvm/internal/impl/a/l$2;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/a/m;->a()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v8

    invoke-virtual {v8}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v7

    .line 111
    iget-object v8, p0, Lkotlin/reflect/jvm/internal/impl/a/l$2;->a:Lkotlin/reflect/jvm/internal/impl/a/l;

    invoke-virtual {v6}, Lkotlin/reflect/jvm/internal/impl/a/m;->b()Lkotlin/reflect/jvm/internal/impl/e/f;

    move-result-object v9

    invoke-virtual {v9}, Lkotlin/reflect/jvm/internal/impl/e/f;->a()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lkotlin/reflect/jvm/internal/impl/a/l;->a(Lkotlin/reflect/jvm/internal/impl/a/l;Ljava/lang/String;)Lkotlin/reflect/jvm/internal/impl/k/v;

    move-result-object v8

    .line 113
    invoke-interface {v1, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-interface {v3, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_0
    new-instance v0, Lkotlin/reflect/jvm/internal/impl/a/l$c;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lkotlin/reflect/jvm/internal/impl/a/l$c;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lkotlin/reflect/jvm/internal/impl/a/l$1;)V

    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/l$2;->b()Lkotlin/reflect/jvm/internal/impl/a/l$c;

    move-result-object v0

    return-object v0
.end method
