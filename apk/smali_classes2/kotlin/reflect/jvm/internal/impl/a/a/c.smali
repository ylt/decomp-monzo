.class public final Lkotlin/reflect/jvm/internal/impl/a/a/c;
.super Lkotlin/reflect/jvm/internal/impl/h/e/e;
.source "FunctionClassScope.kt"


# direct methods
.method public constructor <init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/a/a/b;)V
    .locals 1

    .prologue
    const-string v0, "storageManager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containingClass"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    check-cast p2, Lkotlin/reflect/jvm/internal/impl/b/e;

    .line 26
    invoke-direct {p0, p1, p2}, Lkotlin/reflect/jvm/internal/impl/h/e/e;-><init>(Lkotlin/reflect/jvm/internal/impl/j/i;Lkotlin/reflect/jvm/internal/impl/b/e;)V

    return-void
.end method


# virtual methods
.method protected a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkotlin/reflect/jvm/internal/impl/b/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/c;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.jetbrains.kotlin.builtins.functions.FunctionClassDescriptor"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/a/b;

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b;->z()Lkotlin/reflect/jvm/internal/impl/a/a/b$b;

    move-result-object v0

    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/d;->a:[I

    invoke-virtual {v0}, Lkotlin/reflect/jvm/internal/impl/a/a/b$b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 31
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    .line 32
    :goto_0
    return-object v0

    .line 29
    :pswitch_0
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/e;->a:Lkotlin/reflect/jvm/internal/impl/a/a/e$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/c;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/a/b;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/a/a/e$a;->a(Lkotlin/reflect/jvm/internal/impl/a/a/b;Z)Lkotlin/reflect/jvm/internal/impl/a/a/e;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 30
    :pswitch_1
    sget-object v1, Lkotlin/reflect/jvm/internal/impl/a/a/e;->a:Lkotlin/reflect/jvm/internal/impl/a/a/e$a;

    invoke-virtual {p0}, Lkotlin/reflect/jvm/internal/impl/a/a/c;->c()Lkotlin/reflect/jvm/internal/impl/b/e;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/jvm/internal/impl/a/a/b;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lkotlin/reflect/jvm/internal/impl/a/a/e$a;->a(Lkotlin/reflect/jvm/internal/impl/a/a/b;Z)Lkotlin/reflect/jvm/internal/impl/a/a/e;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
